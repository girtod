// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/Atk-1.0.gir"

module Atk;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;

// package: "atk";

// c:symbol-prefixes: ["atk"]
// c:identifier-prefixes: ["Atk"]

// module Atk;


// This is a singly-linked list (a #GSList) of #AtkAttribute. It is
// used by atk_text_get_run_attributes(), atk_text_get_default_attributes()
// and atk_editable_text_set_run_attributes()
alias GLib2.SList AttributeSet;
alias ulong State;
struct Action /* Interface */ {
   mixin template __interface__() {
      // Perform the specified action on the object.
      // RETURNS: %TRUE if success, %FALSE otherwise
      // <i>: the action index corresponding to the action to be performed
      int do_action()(int i) nothrow {
         return atk_action_do_action(cast(Action*)&this, i);
      }

      // Returns a description of the specified action of the object.
      // Returns a description string, or %NULL
      // if @action does not implement this interface.
      // <i>: the action index corresponding to the action to be performed
      char* get_description()(int i) nothrow {
         return atk_action_get_description(cast(Action*)&this, i);
      }

      // Returns a keybinding associated with this action, if one exists.
      // The returned string is in the format "<a>;<b>;<c>"
      // (i.e. semicolon-delimited), where <a> is the keybinding which
      // activates the object if it is presently enabled onscreen, 
      // <b> corresponds to the keybinding or sequence of keys
      // which invokes the action even if the relevant element is not
      // currently posted on screen (for instance, for a menu item it
      // posts the parent menus before invoking).  The last token in the
      // above string, if non-empty, represents a keyboard shortcut which
      // invokes the same action without posting the component or its
      // enclosing menus or dialogs. 
      // Returns a string representing the available keybindings, or %NULL
      // if there is no keybinding for this action.
      // <i>: the action index corresponding to the action to be performed
      char* get_keybinding()(int i) nothrow {
         return atk_action_get_keybinding(cast(Action*)&this, i);
      }

      // Returns the localized name of the specified action of the object.
      // Returns a name string, or %NULL
      // if @action does not implement this interface.
      // <i>: the action index corresponding to the action to be performed
      char* get_localized_name()(int i) nothrow {
         return atk_action_get_localized_name(cast(Action*)&this, i);
      }

      // Gets the number of accessible actions available on the object.
      // If there are more than one, the first one is considered the
      // "default" action of the object.
      // implement this interface.
      // RETURNS: a the number of actions, or 0 if @action does not
      int get_n_actions()() nothrow {
         return atk_action_get_n_actions(cast(Action*)&this);
      }

      // Returns a non-localized string naming the specified action of the 
      // object. This name is generally not descriptive of the end result 
      // of the action, but instead names the 'interaction type' which the 
      // object supports. By convention, the above strings should be used to 
      // represent the actions which correspond to the common point-and-click 
      // "click", "press", "release", "drag", "drop", "popup", etc.
      // The "popup" action should be used to pop up a context menu for the 
      // object, if one exists.
      // For technical reasons, some toolkits cannot guarantee that the 
      // reported action is actually 'bound' to a nontrivial user event;
      // i.e. the result of some actions via atk_action_do_action() may be
      // NIL.
      // Returns a name string, or %NULL
      // if @action does not implement this interface.
      // <i>: the action index corresponding to the action to be performed
      char* get_name()(int i) nothrow {
         return atk_action_get_name(cast(Action*)&this, i);
      }

      // Sets a description of the specified action of the object.
      // RETURNS: a gboolean representing if the description was successfully set;
      // <i>: the action index corresponding to the action to be performed
      // <desc>: the description to be assigned to this action
      int set_description(AT0)(int i, AT0 /*char*/ desc) nothrow {
         return atk_action_set_description(cast(Action*)&this, i, toCString!(char*)(desc));
      }
   }
   mixin __interface__;
}

struct ActionIface {
   GObject2.TypeInterface parent;

   // RETURNS: %TRUE if success, %FALSE otherwise
   // <i>: the action index corresponding to the action to be performed
   extern (C) int function (Action* action, int i) nothrow do_action;
   // RETURNS: a the number of actions, or 0 if @action does not
   extern (C) int function (Action* action) nothrow get_n_actions;
   // <i>: the action index corresponding to the action to be performed
   extern (C) char* function (Action* action, int i) nothrow get_description;
   // <i>: the action index corresponding to the action to be performed
   extern (C) char* function (Action* action, int i) nothrow get_name;
   // <i>: the action index corresponding to the action to be performed
   extern (C) char* function (Action* action, int i) nothrow get_keybinding;

   // RETURNS: a gboolean representing if the description was successfully set;
   // <i>: the action index corresponding to the action to be performed
   // <desc>: the description to be assigned to this action
   extern (C) int function (Action* action, int i, char* desc) nothrow set_description;
   // <i>: the action index corresponding to the action to be performed
   extern (C) char* function (Action* action, int i) nothrow get_localized_name;
   Function pad2;
}

// A string name/value pair representing a text attribute.
struct Attribute {
   char* name, value;
}

struct Component /* Interface */ {
   mixin template __interface__() {
      // Unintrospectable method: add_focus_handler() / atk_component_add_focus_handler()
      // Add the specified handler to the set of functions to be called 
      // when this object receives focus events (in or out). If the handler is
      // already added it is not added again
      // or zero if the handler was already added.
      // RETURNS: a handler id which can be used in atk_component_remove_focus_handler
      // <handler>: The #AtkFocusHandler to be attached to @component
      uint add_focus_handler()(FocusHandler handler) nothrow {
         return atk_component_add_focus_handler(cast(Component*)&this, handler);
      }

      // Checks whether the specified point is within the extent of the @component.
      // the extent of the @component or not
      // RETURNS: %TRUE or %FALSE indicating whether the specified point is within
      // <x>: x coordinate
      // <y>: y coordinate
      // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
      int contains()(int x, int y, CoordType coord_type) nothrow {
         return atk_component_contains(cast(Component*)&this, x, y, coord_type);
      }

      // VERSION: 1.12
      // Returns the alpha value (i.e. the opacity) for this
      // (fully opaque).
      // RETURNS: An alpha value from 0 to 1.0, inclusive.
      double get_alpha()() nothrow {
         return atk_component_get_alpha(cast(Component*)&this);
      }

      // Gets the rectangle which gives the extent of the @component.
      // <x>: address of #gint to put x coordinate
      // <y>: address of #gint to put y coordinate
      // <width>: address of #gint to put width
      // <height>: address of #gint to put height
      // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
      void get_extents()(int* x, int* y, int* width, int* height, CoordType coord_type) nothrow {
         atk_component_get_extents(cast(Component*)&this, x, y, width, height, coord_type);
      }

      // Gets the layer of the component.
      // RETURNS: an #AtkLayer which is the layer of the component
      Layer get_layer()() nothrow {
         return atk_component_get_layer(cast(Component*)&this);
      }

      // Gets the zorder of the component. The value G_MININT will be returned 
      // if the layer of the component is not ATK_LAYER_MDI or ATK_LAYER_WINDOW.
      // which the component is shown in relation to other components in the same 
      // container.
      // RETURNS: a gint which is the zorder of the component, i.e. the depth at
      int get_mdi_zorder()() nothrow {
         return atk_component_get_mdi_zorder(cast(Component*)&this);
      }

      // Gets the position of @component in the form of 
      // a point specifying @component's top-left corner.
      // <x>: address of #gint to put x coordinate position
      // <y>: address of #gint to put y coordinate position
      // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
      void get_position()(int* x, int* y, CoordType coord_type) nothrow {
         atk_component_get_position(cast(Component*)&this, x, y, coord_type);
      }

      // Gets the size of the @component in terms of width and height.
      // <width>: address of #gint to put width of @component
      // <height>: address of #gint to put height of @component
      void get_size()(int* width, int* height) nothrow {
         atk_component_get_size(cast(Component*)&this, width, height);
      }

      // Grabs focus for this @component.
      // RETURNS: %TRUE if successful, %FALSE otherwise.
      int grab_focus()() nothrow {
         return atk_component_grab_focus(cast(Component*)&this);
      }

      // Gets a reference to the accessible child, if one exists, at the
      // coordinate point specified by @x and @y.
      // RETURNS: a reference to the accessible child, if one exists
      // <x>: x coordinate
      // <y>: y coordinate
      // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
      Object* /*new*/ ref_accessible_at_point()(int x, int y, CoordType coord_type) nothrow {
         return atk_component_ref_accessible_at_point(cast(Component*)&this, x, y, coord_type);
      }

      // Remove the handler specified by @handler_id from the list of
      // functions to be executed when this object receives focus events 
      // (in or out).
      // <handler_id>: the handler id of the focus handler to be removed from @component
      void remove_focus_handler()(uint handler_id) nothrow {
         atk_component_remove_focus_handler(cast(Component*)&this, handler_id);
      }

      // Sets the extents of @component.
      // RETURNS: %TRUE or %FALSE whether the extents were set or not
      // <x>: x coordinate
      // <y>: y coordinate
      // <width>: width to set for @component
      // <height>: height to set for @component
      // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
      int set_extents()(int x, int y, int width, int height, CoordType coord_type) nothrow {
         return atk_component_set_extents(cast(Component*)&this, x, y, width, height, coord_type);
      }

      // Sets the postition of @component.
      // RETURNS: %TRUE or %FALSE whether or not the position was set or not
      // <x>: x coordinate
      // <y>: y coordinate
      // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
      int set_position()(int x, int y, CoordType coord_type) nothrow {
         return atk_component_set_position(cast(Component*)&this, x, y, coord_type);
      }

      // Set the size of the @component in terms of width and height.
      // RETURNS: %TRUE or %FALSE whether the size was set or not
      // <width>: width to set for @component
      // <height>: height to set for @component
      int set_size()(int width, int height) nothrow {
         return atk_component_set_size(cast(Component*)&this, width, height);
      }
      extern (C) alias static void function (Component* this_, Rectangle* object, void* user_data=null) nothrow signal_bounds_changed;

      ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
         return super_.signal_connect!name(cb, data, cf);
      }

      ulong signal_connect(string name:"bounds-changed", CB/*:signal_bounds_changed*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_bounds_changed)||_ttmm!(CB, signal_bounds_changed)()) {
         return signal_connect_data!()(&this, cast(char*)"bounds-changed",
         cast(GObject2.Callback)cb, data, null, cf);
      }
   }
   mixin __interface__;
}

struct ComponentIface {
   GObject2.TypeInterface parent;

   // Unintrospectable functionp: add_focus_handler() / ()
   // 
   // RETURNS: a handler id which can be used in atk_component_remove_focus_handler
   // <handler>: The #AtkFocusHandler to be attached to @component
   extern (C) uint function (Component* component, FocusHandler handler) nothrow add_focus_handler;

   // RETURNS: %TRUE or %FALSE indicating whether the specified point is within
   // <x>: x coordinate
   // <y>: y coordinate
   // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
   extern (C) int function (Component* component, int x, int y, CoordType coord_type) nothrow contains;

   // RETURNS: a reference to the accessible child, if one exists
   // <x>: x coordinate
   // <y>: y coordinate
   // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
   extern (C) Object* /*new*/ function (Component* component, int x, int y, CoordType coord_type) nothrow ref_accessible_at_point;

   // <x>: address of #gint to put x coordinate
   // <y>: address of #gint to put y coordinate
   // <width>: address of #gint to put width
   // <height>: address of #gint to put height
   // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
   extern (C) void function (Component* component, int* x, int* y, int* width, int* height, CoordType coord_type) nothrow get_extents;

   // <x>: address of #gint to put x coordinate position
   // <y>: address of #gint to put y coordinate position
   // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
   extern (C) void function (Component* component, int* x, int* y, CoordType coord_type) nothrow get_position;

   // <width>: address of #gint to put width of @component
   // <height>: address of #gint to put height of @component
   extern (C) void function (Component* component, int* width, int* height) nothrow get_size;
   // RETURNS: %TRUE if successful, %FALSE otherwise.
   extern (C) int function (Component* component) nothrow grab_focus;
   // <handler_id>: the handler id of the focus handler to be removed from @component
   extern (C) void function (Component* component, uint handler_id) nothrow remove_focus_handler;

   // RETURNS: %TRUE or %FALSE whether the extents were set or not
   // <x>: x coordinate
   // <y>: y coordinate
   // <width>: width to set for @component
   // <height>: height to set for @component
   // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
   extern (C) int function (Component* component, int x, int y, int width, int height, CoordType coord_type) nothrow set_extents;

   // RETURNS: %TRUE or %FALSE whether or not the position was set or not
   // <x>: x coordinate
   // <y>: y coordinate
   // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
   extern (C) int function (Component* component, int x, int y, CoordType coord_type) nothrow set_position;

   // RETURNS: %TRUE or %FALSE whether the size was set or not
   // <width>: width to set for @component
   // <height>: height to set for @component
   extern (C) int function (Component* component, int width, int height) nothrow set_size;
   // RETURNS: an #AtkLayer which is the layer of the component
   extern (C) Layer function (Component* component) nothrow get_layer;
   // RETURNS: a gint which is the zorder of the component, i.e. the depth at
   extern (C) int function (Component* component) nothrow get_mdi_zorder;
   extern (C) void function (Component* component, Rectangle* bounds) nothrow bounds_changed;
   // RETURNS: An alpha value from 0 to 1.0, inclusive.
   extern (C) double function (Component* component) nothrow get_alpha;
}

enum CoordType {
   SCREEN = 0,
   WINDOW = 1
}
struct Document /* Interface */ {
   mixin template __interface__() {
      // VERSION: 1.12
      // document, or NULL if a value for #attribute_name has not been specified
      // for this document.
      // RETURNS: a string value associated with the named attribute for this
      // <attribute_name>: a character string representing the name of the attribute whose value is being queried.
      char* get_attribute_value(AT0)(AT0 /*char*/ attribute_name) nothrow {
         return atk_document_get_attribute_value(cast(Document*)&this, toCString!(char*)(attribute_name));
      }

      // VERSION: 1.12
      // Gets an AtkAttributeSet which describes document-wide
      // attributes as name-value pairs.
      // set name-value-pair attributes associated with this document
      // as a whole.
      // RETURNS: An AtkAttributeSet containing the explicitly
      AttributeSet* get_attributes()() nothrow {
         return atk_document_get_attributes(cast(Document*)&this);
      }

      // Gets a %gpointer that points to an instance of the DOM.  It is
      // up to the caller to check atk_document_get_type to determine
      // how to cast this pointer.
      // RETURNS: a %gpointer that points to an instance of the DOM.
      void* get_document()() nothrow {
         return atk_document_get_document(cast(Document*)&this);
      }

      // Gets a string indicating the document type.
      // RETURNS: a string indicating the document type
      char* get_document_type()() nothrow {
         return atk_document_get_document_type(cast(Document*)&this);
      }

      // Gets a UTF-8 string indicating the POSIX-style LC_MESSAGES locale
      // of the content of this document instance.  Individual
      // text substrings or images within this document may have
      // a different locale, see atk_text_get_attributes and
      // atk_image_get_image_locale.
      // locale of the document content as a whole, or NULL if
      // the document content does not specify a locale.
      // RETURNS: a UTF-8 string indicating the POSIX-style LC_MESSAGES
      char* get_locale()() nothrow {
         return atk_document_get_locale(cast(Document*)&this);
      }

      // VERSION: 1.12
      // for this document, FALSE otherwise (e.g. if the document does not
      // allow the attribute to be modified).
      // RETURNS: TRUE if #value is successfully associated with #attribute_name
      // <attribute_name>: a character string representing the name of the attribute whose value is being set.
      // <attribute_value>: a string value to be associated with #attribute_name.
      int set_attribute_value(AT0, AT1)(AT0 /*char*/ attribute_name, AT1 /*char*/ attribute_value) nothrow {
         return atk_document_set_attribute_value(cast(Document*)&this, toCString!(char*)(attribute_name), toCString!(char*)(attribute_value));
      }
      extern (C) alias static void function (Document* this_, void* user_data=null) nothrow signal_load_complete;

      ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
         return super_.signal_connect!name(cb, data, cf);
      }

      ulong signal_connect(string name:"load-complete", CB/*:signal_load_complete*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_load_complete)||_ttmm!(CB, signal_load_complete)()) {
         return signal_connect_data!()(&this, cast(char*)"load-complete",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Document* this_, void* user_data=null) nothrow signal_load_stopped;
      ulong signal_connect(string name:"load-stopped", CB/*:signal_load_stopped*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_load_stopped)||_ttmm!(CB, signal_load_stopped)()) {
         return signal_connect_data!()(&this, cast(char*)"load-stopped",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Document* this_, void* user_data=null) nothrow signal_reload;
      ulong signal_connect(string name:"reload", CB/*:signal_reload*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_reload)||_ttmm!(CB, signal_reload)()) {
         return signal_connect_data!()(&this, cast(char*)"reload",
         cast(GObject2.Callback)cb, data, null, cf);
      }
   }
   mixin __interface__;
}

struct DocumentIface {
   GObject2.TypeInterface parent;
   // RETURNS: a string indicating the document type
   extern (C) char* function (Document* document) nothrow get_document_type;
   // RETURNS: a %gpointer that points to an instance of the DOM.
   extern (C) void* function (Document* document) nothrow get_document;
   extern (C) char* function (Document* document) nothrow get_document_locale;
   // Unintrospectable functionp: get_document_attributes() / ()
   extern (C) AttributeSet* function (Document* document) nothrow get_document_attributes;
   extern (C) char* function (Document* document, char* attribute_name) nothrow get_document_attribute_value;
   extern (C) int function (Document* document, char* attribute_name, char* attribute_value) nothrow set_document_attribute;
   Function pad1, pad2, pad3, pad4;
}

struct EditableText /* Interface */ {
   mixin template __interface__() {
      // Copy text from @start_pos up to, but not including @end_pos 
      // to the clipboard.
      // <start_pos>: start position
      // <end_pos>: end position
      void copy_text()(int start_pos, int end_pos) nothrow {
         atk_editable_text_copy_text(cast(EditableText*)&this, start_pos, end_pos);
      }

      // Copy text from @start_pos up to, but not including @end_pos
      // to the clipboard and then delete from the widget.
      // <start_pos>: start position
      // <end_pos>: end position
      void cut_text()(int start_pos, int end_pos) nothrow {
         atk_editable_text_cut_text(cast(EditableText*)&this, start_pos, end_pos);
      }

      // Delete text @start_pos up to, but not including @end_pos.
      // <start_pos>: start position
      // <end_pos>: end position
      void delete_text()(int start_pos, int end_pos) nothrow {
         atk_editable_text_delete_text(cast(EditableText*)&this, start_pos, end_pos);
      }

      // Insert text at a given position.
      // <string>: the text to insert
      // <length>: the length of text to insert, in bytes
      // <position>: The caller initializes this to the position at which to insert the text. After the call it points at the position after the newly inserted text.
      void insert_text(AT0)(AT0 /*char*/ string_, int length, int* position) nothrow {
         atk_editable_text_insert_text(cast(EditableText*)&this, toCString!(char*)(string_), length, position);
      }

      // Paste text from clipboard to specified @position.
      // <position>: position to paste
      void paste_text()(int position) nothrow {
         atk_editable_text_paste_text(cast(EditableText*)&this, position);
      }
      int set_run_attributes(AT0)(AT0 /*AttributeSet*/ attrib_set, int start_offset, int end_offset) nothrow {
         return atk_editable_text_set_run_attributes(cast(EditableText*)&this, UpCast!(AttributeSet*)(attrib_set), start_offset, end_offset);
      }

      // Set text contents of @text.
      // <string>: string to set for text contents of @text
      void set_text_contents(AT0)(AT0 /*char*/ string_) nothrow {
         atk_editable_text_set_text_contents(cast(EditableText*)&this, toCString!(char*)(string_));
      }
   }
   mixin __interface__;
}

struct EditableTextIface {
   GObject2.TypeInterface parent_interface;
   extern (C) int function (EditableText* text, AttributeSet* attrib_set, int start_offset, int end_offset) nothrow set_run_attributes;
   // <string>: string to set for text contents of @text
   extern (C) void function (EditableText* text, char* string_) nothrow set_text_contents;

   // <string>: the text to insert
   // <length>: the length of text to insert, in bytes
   // <position>: The caller initializes this to the position at which to insert the text. After the call it points at the position after the newly inserted text.
   extern (C) void function (EditableText* text, char* string_, int length, int* position) nothrow insert_text;

   // <start_pos>: start position
   // <end_pos>: end position
   extern (C) void function (EditableText* text, int start_pos, int end_pos) nothrow copy_text;

   // <start_pos>: start position
   // <end_pos>: end position
   extern (C) void function (EditableText* text, int start_pos, int end_pos) nothrow cut_text;

   // <start_pos>: start position
   // <end_pos>: end position
   extern (C) void function (EditableText* text, int start_pos, int end_pos) nothrow delete_text;
   // <position>: position to paste
   extern (C) void function (EditableText* text, int position) nothrow paste_text;
   Function pad1, pad2;
}


// A function which is called when an object emits a matching event,
// as used in #atk_add_focus_tracker.
// Currently the only events for which object-specific handlers are
// supported are events of type "focus:".  Most clients of ATK will prefer to 
// attach signal handlers for the various ATK signals instead.
// see atk_add_focus_tracker.
// <obj>: An #AtkObject instance for whom the callback will be called when the specified event (e.g. 'focus:') takes place.
extern (C) alias void function (Object* obj) nothrow EventListener;


// An #AtkEventListenerInit function is a special function that is
// called in order to initialize the per-object event registration system
// used by #AtkEventListener, if any preparation is required.  
// see atk_focus_tracker_init.
extern (C) alias void function () nothrow EventListenerInit;

extern (C) alias void function (Object* arg_a, int arg_b) nothrow FocusHandler;

extern (C) alias int function (void* data) nothrow Function;

struct GObjectAccessible /* : Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   Object parent;


   // Gets the accessible object for the specified @obj.
   // the @obj
   // RETURNS: a #AtkObject which is the accessible object for
   // <obj>: a #GObject
   static Object* for_object(AT0)(AT0 /*GObject2.Object*/ obj) nothrow {
      return atk_gobject_accessible_for_object(UpCast!(GObject2.Object*)(obj));
   }

   // Gets the GObject for which @obj is the accessible object.
   // the accessible object
   // RETURNS: a #GObject which is the object for which @obj is
   GObject2.Object* get_object()() nothrow {
      return atk_gobject_accessible_get_object(&this);
   }
}

struct GObjectAccessibleClass {
   ObjectClass parent_class;
   Function pad1, pad2;
}

struct Hyperlink /* : GObject.Object */ {
   mixin Action.__interface__;
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;


   // Gets the index with the hypertext document at which this link ends.
   // RETURNS: the index with the hypertext document at which this link ends
   int get_end_index()() nothrow {
      return atk_hyperlink_get_end_index(&this);
   }

   // Gets the number of anchors associated with this hyperlink.
   // RETURNS: the number of anchors associated with this hyperlink
   int get_n_anchors()() nothrow {
      return atk_hyperlink_get_n_anchors(&this);
   }

   // Returns the item associated with this hyperlinks nth anchor.
   // For instance, the returned #AtkObject will implement #AtkText
   // if @link_ is a text hyperlink, #AtkImage if @link_ is an image
   // hyperlink etc. 
   // Multiple anchors are primarily used by client-side image maps.
   // i-th anchor
   // RETURNS: an #AtkObject associated with this hyperlinks
   // <i>: a (zero-index) integer specifying the desired anchor
   Object* get_object()(int i) nothrow {
      return atk_hyperlink_get_object(&this, i);
   }

   // Gets the index with the hypertext document at which this link begins.
   // RETURNS: the index with the hypertext document at which this link begins
   int get_start_index()() nothrow {
      return atk_hyperlink_get_start_index(&this);
   }

   // Get a the URI associated with the anchor specified 
   // by @i of @link_. 
   // Multiple anchors are primarily used by client-side image maps.
   // RETURNS: a string specifying the URI
   // <i>: a (zero-index) integer specifying the desired anchor
   char* /*new*/ get_uri()(int i) nothrow {
      return atk_hyperlink_get_uri(&this, i);
   }

   // Indicates whether the link currently displays some or all of its
   // content inline.  Ordinary HTML links will usually return
   // %FALSE, but an inline &lt;src&gt; HTML element will return
   // %TRUE.
   // RETURNS: whether or not this link displays its content inline.
   int is_inline()() nothrow {
      return atk_hyperlink_is_inline(&this);
   }

   // Since the document that a link is associated with may have changed
   // this method returns %TRUE if the link is still valid (with
   // respect to the document it references) and %FALSE otherwise.
   // RETURNS: whether or not this link is still valid
   int is_valid()() nothrow {
      return atk_hyperlink_is_valid(&this);
   }
   extern (C) alias static void function (Hyperlink* this_, void* user_data=null) nothrow signal_link_activated;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"link-activated", CB/*:signal_link_activated*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_link_activated)||_ttmm!(CB, signal_link_activated)()) {
      return signal_connect_data!()(&this, cast(char*)"link-activated",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct HyperlinkClass {
   GObject2.ObjectClass parent;

   // RETURNS: a string specifying the URI
   // <i>: a (zero-index) integer specifying the desired anchor
   extern (C) char* /*new*/ function (Hyperlink* link_, int i) nothrow get_uri;

   // RETURNS: an #AtkObject associated with this hyperlinks
   // <i>: a (zero-index) integer specifying the desired anchor
   extern (C) Object* function (Hyperlink* link_, int i) nothrow get_object;
   // RETURNS: the index with the hypertext document at which this link ends
   extern (C) int function (Hyperlink* link_) nothrow get_end_index;
   // RETURNS: the index with the hypertext document at which this link begins
   extern (C) int function (Hyperlink* link_) nothrow get_start_index;
   // RETURNS: whether or not this link is still valid
   extern (C) int function (Hyperlink* link_) nothrow is_valid;
   // RETURNS: the number of anchors associated with this hyperlink
   extern (C) int function (Hyperlink* link_) nothrow get_n_anchors;
   extern (C) uint function (Hyperlink* link_) nothrow link_state;
   extern (C) int function (Hyperlink* link_) nothrow is_selected_link;
   extern (C) void function (Hyperlink* link_) nothrow link_activated;
   Function pad1;
}

struct HyperlinkImpl /* Interface */ {
   mixin template __interface__() {
      // VERSION: 1.12
      // Gets the hyperlink associated with this object.
      // implementing AtkObject.
      // RETURNS: an AtkHyperlink object which points to this
      Hyperlink* /*new*/ get_hyperlink()() nothrow {
         return atk_hyperlink_impl_get_hyperlink(cast(HyperlinkImpl*)&this);
      }
   }
   mixin __interface__;
}

struct HyperlinkImplIface {
   GObject2.TypeInterface parent;
   // RETURNS: an AtkHyperlink object which points to this
   extern (C) Hyperlink* /*new*/ function (HyperlinkImpl* impl) nothrow get_hyperlink;
   Function pad1;
}

enum HyperlinkStateFlags {
   INLINE = 1
}
struct Hypertext /* Interface */ {
   mixin template __interface__() {
      // Gets the link in this hypertext document at index 
      // index @link_index
      // RETURNS: the link in this hypertext document at
      // <link_index>: an integer specifying the desired link
      Hyperlink* get_link()(int link_index) nothrow {
         return atk_hypertext_get_link(cast(Hypertext*)&this, link_index);
      }

      // Gets the index into the array of hyperlinks that is associated with
      // the character specified by @char_index.
      // or -1 if there is no hyperlink associated with this character.
      // RETURNS: an index into the array of hyperlinks in @hypertext,
      // <char_index>: a character index
      int get_link_index()(int char_index) nothrow {
         return atk_hypertext_get_link_index(cast(Hypertext*)&this, char_index);
      }

      // Gets the number of links within this hypertext document.
      // RETURNS: the number of links within this hypertext document
      int get_n_links()() nothrow {
         return atk_hypertext_get_n_links(cast(Hypertext*)&this);
      }
      extern (C) alias static void function (Hypertext* this_, int object, void* user_data=null) nothrow signal_link_selected;

      ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
         return super_.signal_connect!name(cb, data, cf);
      }

      ulong signal_connect(string name:"link-selected", CB/*:signal_link_selected*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_link_selected)||_ttmm!(CB, signal_link_selected)()) {
         return signal_connect_data!()(&this, cast(char*)"link-selected",
         cast(GObject2.Callback)cb, data, null, cf);
      }
   }
   mixin __interface__;
}

struct HypertextIface {
   GObject2.TypeInterface parent;

   // RETURNS: the link in this hypertext document at
   // <link_index>: an integer specifying the desired link
   extern (C) Hyperlink* function (Hypertext* hypertext, int link_index) nothrow get_link;
   // RETURNS: the number of links within this hypertext document
   extern (C) int function (Hypertext* hypertext) nothrow get_n_links;

   // RETURNS: an index into the array of hyperlinks in @hypertext,
   // <char_index>: a character index
   extern (C) int function (Hypertext* hypertext, int char_index) nothrow get_link_index;
   extern (C) void function (Hypertext* hypertext, int link_index) nothrow link_selected;
   Function pad1, pad2, pad3;
}

struct Image /* Interface */ {
   mixin template __interface__() {
      // Get a textual description of this image.
      // RETURNS: a string representing the image description
      char* get_image_description()() nothrow {
         return atk_image_get_image_description(cast(Image*)&this);
      }

      // Since ATK 1.12
      // Returns a string corresponding to the POSIX LC_MESSAGES locale used by the image description, or NULL if the image does not specify a locale.
      char* get_image_locale()() nothrow {
         return atk_image_get_image_locale(cast(Image*)&this);
      }

      // Gets the position of the image in the form of a point specifying the
      // images top-left corner.
      // <x>: address of #gint to put x coordinate position; otherwise, -1 if value cannot be obtained.
      // <y>: address of #gint to put y coordinate position; otherwise, -1 if value cannot be obtained.
      // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
      void get_image_position()(int* x, int* y, CoordType coord_type) nothrow {
         atk_image_get_image_position(cast(Image*)&this, x, y, coord_type);
      }

      // Get the width and height in pixels for the specified image.
      // The values of @width and @height are returned as -1 if the
      // values cannot be obtained (for instance, if the object is not onscreen).
      // <width>: filled with the image width, or -1 if the value cannot be obtained.
      // <height>: filled with the image height, or -1 if the value cannot be obtained.
      void get_image_size()(int* width, int* height) nothrow {
         atk_image_get_image_size(cast(Image*)&this, width, height);
      }

      // Sets the textual description for this image.
      // not be completed.
      // RETURNS: boolean TRUE, or FALSE if operation could
      // <description>: a string description to set for @image
      int set_image_description(AT0)(AT0 /*char*/ description) nothrow {
         return atk_image_set_image_description(cast(Image*)&this, toCString!(char*)(description));
      }
   }
   mixin __interface__;
}

struct ImageIface {
   GObject2.TypeInterface parent;

   // <x>: address of #gint to put x coordinate position; otherwise, -1 if value cannot be obtained.
   // <y>: address of #gint to put y coordinate position; otherwise, -1 if value cannot be obtained.
   // <coord_type>: specifies whether the coordinates are relative to the screen or to the components top level window
   extern (C) void function (Image* image, int* x, int* y, CoordType coord_type) nothrow get_image_position;
   // RETURNS: a string representing the image description
   extern (C) char* function (Image* image) nothrow get_image_description;

   // <width>: filled with the image width, or -1 if the value cannot be obtained.
   // <height>: filled with the image height, or -1 if the value cannot be obtained.
   extern (C) void function (Image* image, int* width, int* height) nothrow get_image_size;

   // RETURNS: boolean TRUE, or FALSE if operation could
   // <description>: a string description to set for @image
   extern (C) int function (Image* image, char* description) nothrow set_image_description;
   extern (C) char* function (Image* image) nothrow get_image_locale;
   Function pad1;
}

struct Implementor {

   // Gets a reference to an object's #AtkObject implementation, if
   // the object implements #AtkObjectIface
   // implementation
   // RETURNS: a reference to an object's #AtkObject
   Object* /*new*/ ref_accessible()() nothrow {
      return atk_implementor_ref_accessible(&this);
   }
}

struct ImplementorIface /* Interface */ {
   mixin template __interface__() {   }
   mixin __interface__;
}

// Encapsulates information about a key event.
struct KeyEventStruct {
   int type;
   uint state, keyval;
   int length;
   char* string_;
   ushort keycode;
   uint timestamp;
}

enum KeyEventType {
   PRESS = 0,
   RELEASE = 1,
   LAST_DEFINED = 2
}

// An #AtkKeySnoopFunc is a type of callback which is called whenever a key event occurs, 
// if registered via atk_add_key_event_listener.  It allows for pre-emptive 
// interception of key events via the return code as described below.
// discarded without being passed to the normal GUI recipient; FALSE (zero) if the 
// event dispatch to the client application should proceed as normal.
// see atk_add_key_event_listener.
// RETURNS: TRUE (nonzero) if the event emission should be stopped and the event
// <event>: an AtkKeyEventStruct containing information about the key event for which notification is being given.
// <func_data>: a block of data which will be passed to the event listener, on notification.
extern (C) alias int function (KeyEventStruct* event, void* func_data) nothrow KeySnoopFunc;

enum Layer {
   INVALID = 0,
   BACKGROUND = 1,
   CANVAS = 2,
   WIDGET = 3,
   MDI = 4,
   POPUP = 5,
   OVERLAY = 6,
   WINDOW = 7
}
struct Misc /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;


   // VERSION: 1.13
   // Obtain the singleton instance of AtkMisc for this application.
   // RETURNS: The singleton instance of AtkMisc for this application.
   static Misc* get_instance()() nothrow {
      return atk_misc_get_instance();
   }

   // VERSION: 1.13
   // Take the thread mutex for the GUI toolkit, 
   // if one exists. 
   // (This method is implemented by the toolkit ATK implementation layer;
   // for instance, for GTK+, GAIL implements this via GDK_THREADS_ENTER).
   void threads_enter()() nothrow {
      atk_misc_threads_enter(&this);
   }

   // VERSION: 1.13
   // Release the thread mutex for the GUI toolkit, 
   // if one exists. This method, and atk_misc_threads_enter, 
   // are needed in some situations by threaded application code which 
   // services ATK requests, since fulfilling ATK requests often
   // requires calling into the GUI toolkit.  If a long-running or
   // potentially blocking call takes place inside such a block, it should
   // be bracketed by atk_misc_threads_leave/atk_misc_threads_enter calls.
   // (This method is implemented by the toolkit ATK implementation layer;
   // for instance, for GTK+, GAIL implements this via GDK_THREADS_LEAVE).
   void threads_leave()() nothrow {
      atk_misc_threads_leave(&this);
   }
}

struct MiscClass {
   GObject2.ObjectClass parent;
   extern (C) void function (Misc* misc) nothrow threads_enter;
   extern (C) void function (Misc* misc) nothrow threads_leave;
   void*[32] vfuncs;
}

struct NoOpObject /* : Object */ {
   mixin Action.__interface__;
   mixin Component.__interface__;
   mixin Document.__interface__;
   mixin EditableText.__interface__;
   mixin Hypertext.__interface__;
   mixin Image.__interface__;
   mixin Selection.__interface__;
   mixin Table.__interface__;
   mixin Text.__interface__;
   mixin Value.__interface__;
   mixin Window.__interface__;
   alias parent this;
   alias parent super_;
   alias parent object;
   Object parent;


   // Provides a default (non-functioning stub) #AtkObject.
   // Application maintainers should not use this method.
   // RETURNS: a default (non-functioning stub) #AtkObject
   // <obj>: a #GObject
   static NoOpObject* /*new*/ new_(AT0)(AT0 /*GObject2.Object*/ obj) nothrow {
      return atk_no_op_object_new(UpCast!(GObject2.Object*)(obj));
   }
   static auto opCall(AT0)(AT0 /*GObject2.Object*/ obj) {
      return atk_no_op_object_new(UpCast!(GObject2.Object*)(obj));
   }
}

struct NoOpObjectClass {
   ObjectClass parent_class;
}

struct NoOpObjectFactory /* : ObjectFactory */ {
   alias parent this;
   alias parent super_;
   alias parent objectfactory;
   ObjectFactory parent;


   // Creates an instance of an #AtkObjectFactory which generates primitive
   // (non-functioning) #AtkObjects.
   // RETURNS: an instance of an #AtkObjectFactory
   static NoOpObjectFactory* /*new*/ new_()() nothrow {
      return atk_no_op_object_factory_new();
   }
   static auto opCall()() {
      return atk_no_op_object_factory_new();
   }
}

struct NoOpObjectFactoryClass {
   ObjectFactoryClass parent_class;
}

struct Object /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   char* description, name;
   Object* accessible_parent;
   Role role;
   RelationSet* relation_set;
   Layer layer;


   // Adds a relationship of the specified type with the specified target.
   // Returns TRUE if the relationship is added.
   // <relationship>: The #AtkRelationType of the relation
   // <target>: The #AtkObject which is to be the target of the relation.
   int add_relationship(AT0)(RelationType relationship, AT0 /*Object*/ target) nothrow {
      return atk_object_add_relationship(&this, relationship, UpCast!(Object*)(target));
   }

   // Unintrospectable method: connect_property_change_handler() / atk_object_connect_property_change_handler()
   // Specifies a function to be called when a property changes value.
   // atk_object_remove_property_change_handler()
   // RETURNS: a #guint which is the handler id used in
   // <handler>: a function to be called when a property changes its value
   uint connect_property_change_handler(AT0)(AT0 /*PropertyChangeHandler*/ handler) nothrow {
      return atk_object_connect_property_change_handler(&this, UpCast!(PropertyChangeHandler*)(handler));
   }

   // VERSION: 1.12
   // Get a list of properties applied to this object as a whole, as an #AtkAttributeSet consisting of 
   // name-value pairs. As such these attributes may be considered weakly-typed properties or annotations, 
   // as distinct from strongly-typed object data available via other get/set methods.
   // Not all objects have explicit "name-value pair" #AtkAttributeSet properties.
   // properties/annotations applied to the object, or an empty set if the object
   // has no name-value pair attributes assigned to it.
   // RETURNS: an #AtkAttributeSet consisting of all explicit
   AttributeSet* get_attributes()() nothrow {
      return atk_object_get_attributes(&this);
   }

   // Gets the accessible description of the accessible.
   // of the accessible.
   // RETURNS: a character string representing the accessible description
   char* get_description()() nothrow {
      return atk_object_get_description(&this);
   }

   // Gets the 0-based index of this accessible in its parent; returns -1 if the
   // accessible does not have an accessible parent.
   // RETURNS: an integer which is the index of the accessible in its parent
   int get_index_in_parent()() nothrow {
      return atk_object_get_index_in_parent(&this);
   }

   // Gets the number of accessible children of the accessible.
   // of the accessible.
   // RETURNS: an integer representing the number of accessible children
   int get_n_accessible_children()() nothrow {
      return atk_object_get_n_accessible_children(&this);
   }

   // Gets the accessible name of the accessible.
   // RETURNS: a character string representing the accessible name of the object.
   char* get_name()() nothrow {
      return atk_object_get_name(&this);
   }

   // Gets the accessible parent of the accessible.
   // of the accessible
   // RETURNS: a #AtkObject representing the accessible parent
   Object* get_parent()() nothrow {
      return atk_object_get_parent(&this);
   }

   // Gets the role of the accessible.
   // RETURNS: an #AtkRole which is the role of the accessible
   Role get_role()() nothrow {
      return atk_object_get_role(&this);
   }

   // This function is called when implementing subclasses of #AtkObject.
   // It does initialization required for the new object. It is intended
   // that this function should called only in the ..._new() functions used
   // to create an instance of a subclass of #AtkObject
   // <data>: a #gpointer which identifies the object for which the AtkObject was created.
   void initialize(AT0)(AT0 /*void*/ data) nothrow {
      atk_object_initialize(&this, UpCast!(void*)(data));
   }

   // Emits a state-change signal for the specified state.
   // <state>: an #AtkState whose state is changed
   // <value>: a gboolean which indicates whether the state is being set on or off
   void notify_state_change()(State state, int value) nothrow {
      atk_object_notify_state_change(&this, state, value);
   }

   // Gets a reference to the specified accessible child of the object.
   // The accessible children are 0-based so the first accessible child is
   // at index 0, the second at index 1 and so on.
   // accessible child of the accessible.
   // RETURNS: an #AtkObject representing the specified
   // <i>: a gint representing the position of the child, starting from 0
   Object* /*new*/ ref_accessible_child()(int i) nothrow {
      return atk_object_ref_accessible_child(&this, i);
   }

   // Gets the #AtkRelationSet associated with the object.
   // of the object.
   // RETURNS: an #AtkRelationSet representing the relation set
   RelationSet* /*new*/ ref_relation_set()() nothrow {
      return atk_object_ref_relation_set(&this);
   }

   // Gets a reference to the state set of the accessible; the caller must
   // unreference it when it is no longer needed.
   // set of the accessible
   // RETURNS: a reference to an #AtkStateSet which is the state
   StateSet* /*new*/ ref_state_set()() nothrow {
      return atk_object_ref_state_set(&this);
   }

   // Removes a property change handler.
   // <handler_id>: a guint which identifies the handler to be removed.
   void remove_property_change_handler()(uint handler_id) nothrow {
      atk_object_remove_property_change_handler(&this, handler_id);
   }

   // Removes a relationship of the specified type with the specified target.
   // Returns TRUE if the relationship is removed.
   // <relationship>: The #AtkRelationType of the relation
   // <target>: The #AtkObject which is the target of the relation to be removed.
   int remove_relationship(AT0)(RelationType relationship, AT0 /*Object*/ target) nothrow {
      return atk_object_remove_relationship(&this, relationship, UpCast!(Object*)(target));
   }

   // Sets the accessible description of the accessible.
   // <description>: a character string to be set as the accessible description
   void set_description(AT0)(AT0 /*char*/ description) nothrow {
      atk_object_set_description(&this, toCString!(char*)(description));
   }

   // Sets the accessible name of the accessible.
   // <name>: a character string to be set as the accessible name
   void set_name(AT0)(AT0 /*char*/ name) nothrow {
      atk_object_set_name(&this, toCString!(char*)(name));
   }

   // Sets the accessible parent of the accessible.
   // <parent>: an #AtkObject to be set as the accessible parent
   void set_parent(AT0)(AT0 /*Object*/ parent) nothrow {
      atk_object_set_parent(&this, UpCast!(Object*)(parent));
   }

   // Sets the role of the accessible.
   // <role>: an #AtkRole to be set as the role
   void set_role()(Role role) nothrow {
      atk_object_set_role(&this, role);
   }
   extern (C) alias static void function (Object* this_, void* object, void* user_data=null) nothrow signal_active_descendant_changed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"active-descendant-changed", CB/*:signal_active_descendant_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_active_descendant_changed)||_ttmm!(CB, signal_active_descendant_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"active-descendant-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
   extern (C) alias static void function (Object* this_, c_uint object, void* p0, void* user_data=null) nothrow signal_children_changed;
   ulong signal_connect(string name:"children-changed", CB/*:signal_children_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_children_changed)||_ttmm!(CB, signal_children_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"children-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
   extern (C) alias static void function (Object* this_, c_int object, void* user_data=null) nothrow signal_focus_event;
   ulong signal_connect(string name:"focus-event", CB/*:signal_focus_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_focus_event)||_ttmm!(CB, signal_focus_event)()) {
      return signal_connect_data!()(&this, cast(char*)"focus-event",
      cast(GObject2.Callback)cb, data, null, cf);
   }
   extern (C) alias static void function (Object* this_, void* object, void* user_data=null) nothrow signal_property_change;
   ulong signal_connect(string name:"property-change", CB/*:signal_property_change*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_property_change)||_ttmm!(CB, signal_property_change)()) {
      return signal_connect_data!()(&this, cast(char*)"property-change",
      cast(GObject2.Callback)cb, data, null, cf);
   }
   extern (C) alias static void function (Object* this_, char* object, c_int p0, void* user_data=null) nothrow signal_state_change;
   ulong signal_connect(string name:"state-change", CB/*:signal_state_change*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_state_change)||_ttmm!(CB, signal_state_change)()) {
      return signal_connect_data!()(&this, cast(char*)"state-change",
      cast(GObject2.Callback)cb, data, null, cf);
   }
   extern (C) alias static void function (Object* this_, void* user_data=null) nothrow signal_visible_data_changed;
   ulong signal_connect(string name:"visible-data-changed", CB/*:signal_visible_data_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_visible_data_changed)||_ttmm!(CB, signal_visible_data_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"visible-data-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct ObjectClass {
   GObject2.ObjectClass parent;
   // RETURNS: a character string representing the accessible name of the object.
   extern (C) char* function (Object* accessible) nothrow get_name;
   // RETURNS: a character string representing the accessible description
   extern (C) char* function (Object* accessible) nothrow get_description;
   // RETURNS: a #AtkObject representing the accessible parent
   extern (C) Object* function (Object* accessible) nothrow get_parent;
   extern (C) int function (Object* accessible) nothrow get_n_children;
   // Unintrospectable functionp: ref_child() / ()
   extern (C) Object* function (Object* accessible, int i) nothrow ref_child;
   // RETURNS: an integer which is the index of the accessible in its parent
   extern (C) int function (Object* accessible) nothrow get_index_in_parent;
   // RETURNS: an #AtkRelationSet representing the relation set
   extern (C) RelationSet* /*new*/ function (Object* accessible) nothrow ref_relation_set;
   // RETURNS: an #AtkRole which is the role of the accessible
   extern (C) Role function (Object* accessible) nothrow get_role;
   extern (C) Layer function (Object* accessible) nothrow get_layer;
   extern (C) int function (Object* accessible) nothrow get_mdi_zorder;
   // RETURNS: a reference to an #AtkStateSet which is the state
   extern (C) StateSet* /*new*/ function (Object* accessible) nothrow ref_state_set;
   // <name>: a character string to be set as the accessible name
   extern (C) void function (Object* accessible, char* name) nothrow set_name;
   // <description>: a character string to be set as the accessible description
   extern (C) void function (Object* accessible, char* description) nothrow set_description;
   // <parent>: an #AtkObject to be set as the accessible parent
   extern (C) void function (Object* accessible, Object* parent) nothrow set_parent;
   // <role>: an #AtkRole to be set as the role
   extern (C) void function (Object* accessible, Role role) nothrow set_role;

   // Unintrospectable functionp: connect_property_change_handler() / ()
   // 
   // RETURNS: a #guint which is the handler id used in
   // <handler>: a function to be called when a property changes its value
   extern (C) uint function (Object* accessible, PropertyChangeHandler* handler) nothrow connect_property_change_handler;
   // <handler_id>: a guint which identifies the handler to be removed.
   extern (C) void function (Object* accessible, uint handler_id) nothrow remove_property_change_handler;
   // <data>: a #gpointer which identifies the object for which the AtkObject was created.
   extern (C) void function (Object* accessible, void* data) nothrow initialize;
   extern (C) void function (Object* accessible, uint change_index, void* changed_child) nothrow children_changed;
   extern (C) void function (Object* accessible, int focus_in) nothrow focus_event;
   // Unintrospectable functionp: property_change() / ()
   extern (C) void function (Object* accessible, _PropertyValues* values) nothrow property_change;
   extern (C) void function (Object* accessible, char* name, int state_set) nothrow state_change;
   extern (C) void function (Object* accessible) nothrow visible_data_changed;
   extern (C) void function (Object* accessible, void** child) nothrow active_descendant_changed;
   // RETURNS: an #AtkAttributeSet consisting of all explicit
   extern (C) AttributeSet* function (Object* accessible) nothrow get_attributes;
   Function pad1, pad2;
}

struct ObjectFactory /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;


   // Provides an #AtkObject that implements an accessibility interface 
   // on behalf of @obj
   // interface on behalf of @obj
   // RETURNS: an #AtkObject that implements an accessibility
   // <obj>: a #GObject
   Object* /*new*/ create_accessible(AT0)(AT0 /*GObject2.Object*/ obj) nothrow {
      return atk_object_factory_create_accessible(&this, UpCast!(GObject2.Object*)(obj));
   }

   // Gets the GType of the accessible which is created by the factory. 
   // The value G_TYPE_INVALID is returned if no type if found.
   // RETURNS: the type of the accessible which is created by the @factory.
   Type get_accessible_type()() nothrow {
      return atk_object_factory_get_accessible_type(&this);
   }

   // Inform @factory that it is no longer being used to create
   // accessibles. When called, @factory may need to inform
   // #AtkObjects which it has created that they need to be re-instantiated.
   // in object registries.
   void invalidate()() nothrow {
      atk_object_factory_invalidate(&this);
   }
}

struct ObjectFactoryClass {
   GObject2.ObjectClass parent_class;
   // Unintrospectable functionp: create_accessible() / ()
   extern (C) Object* function (GObject2.Object* obj) nothrow create_accessible;
   extern (C) void function (ObjectFactory* factory) nothrow invalidate;
   extern (C) Type function () nothrow get_accessible_type;
   Function pad1, pad2;
}

struct Plug /* : Object */ {
   mixin Component.__interface__;
   alias parent this;
   alias parent super_;
   alias parent object;
   Object parent;

   static Plug* /*new*/ new_()() nothrow {
      return atk_plug_new();
   }
   static auto opCall()() {
      return atk_plug_new();
   }
   char* /*new*/ get_id()() nothrow {
      return atk_plug_get_id(&this);
   }
}

struct PlugClass {
   ObjectClass parent_class;
   extern (C) char* /*new*/ function (Plug* obj) nothrow get_object_id;
}

// Unintrospectable callback: PropertyChangeHandler() / ()
extern (C) alias void function (Object* arg_a, _PropertyValues* arg_b) nothrow PropertyChangeHandler;

struct Rectangle {
   int x, y, width, height;
}

struct Registry /* : GObject.Object */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // Gets an #AtkObjectFactory appropriate for creating #AtkObjects
   // appropriate for @type.
   // #AtkObjects appropriate for @type.
   // RETURNS: an #AtkObjectFactory appropriate for creating
   // <type>: a #GType with which to look up the associated #AtkObjectFactory
   ObjectFactory* get_factory()(Type type) nothrow {
      return atk_registry_get_factory(&this, type);
   }

   // Provides a #GType indicating the #AtkObjectFactory subclass
   // associated with @type.
   // RETURNS: a #GType associated with type @type
   // <type>: a #GType with which to look up the associated #AtkObjectFactory subclass
   Type get_factory_type()(Type type) nothrow {
      return atk_registry_get_factory_type(&this, type);
   }

   // Associate an #AtkObjectFactory subclass with a #GType. Note:
   // The associated @factory_type will thereafter be responsible for
   // the creation of new #AtkObject implementations for instances
   // appropriate for @type.
   // <type>: an #AtkObject type
   // <factory_type>: an #AtkObjectFactory type to associate with @type.  Must implement AtkObject appropriate for @type.
   void set_factory_type()(Type type, Type factory_type) nothrow {
      atk_registry_set_factory_type(&this, type, factory_type);
   }
}

struct Relation /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   void*[666] target;
   RelationType relationship;


   // Create a new relation for the specified key and the specified list
   // of targets.  See also atk_object_add_relationship().
   // RETURNS: a pointer to a new #AtkRelation
   // <targets>: an array of pointers to #AtkObjects
   // <n_targets>: number of #AtkObjects pointed to by @targets
   // <relationship>: an #AtkRelationType with which to create the new #AtkRelation
   static Relation* /*new*/ new_(AT0)(AT0 /*Object**/ targets, int n_targets, RelationType relationship) nothrow {
      return atk_relation_new(UpCast!(Object**)(targets), n_targets, relationship);
   }
   static auto opCall(AT0)(AT0 /*Object**/ targets, int n_targets, RelationType relationship) {
      return atk_relation_new(UpCast!(Object**)(targets), n_targets, relationship);
   }

   // VERSION: 1.9
   // Adds the specified AtkObject to the target for the relation, if it is
   // not already present.  See also atk_object_add_relationship().
   // <target>: an #AtkObject
   void add_target(AT0)(AT0 /*Object*/ target) nothrow {
      atk_relation_add_target(&this, UpCast!(Object*)(target));
   }

   // Gets the type of @relation
   // RETURNS: the type of @relation
   RelationType get_relation_type()() nothrow {
      return atk_relation_get_relation_type(&this);
   }

   // Gets the target list of @relation
   // RETURNS: the target list of @relation
   PtrArray* get_target()() nothrow {
      return atk_relation_get_target(&this);
   }

   // Remove the specified AtkObject from the target for the relation.
   // Returns TRUE if the removal is successful.
   // <target>: an #AtkObject
   int remove_target(AT0)(AT0 /*Object*/ target) nothrow {
      return atk_relation_remove_target(&this, UpCast!(Object*)(target));
   }
}

struct RelationClass {
   GObject2.ObjectClass parent;
}

struct RelationSet /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   void*[666] relations;


   // Creates a new empty relation set.
   // RETURNS: a new #AtkRelationSet
   static RelationSet* /*new*/ new_()() nothrow {
      return atk_relation_set_new();
   }
   static auto opCall()() {
      return atk_relation_set_new();
   }

   // Add a new relation to the current relation set if it is not already
   // present.
   // This function ref's the AtkRelation so the caller of this function
   // should unref it to ensure that it will be destroyed when the AtkRelationSet
   // is destroyed.
   // <relation>: an #AtkRelation
   void add(AT0)(AT0 /*Relation*/ relation) nothrow {
      atk_relation_set_add(&this, UpCast!(Relation*)(relation));
   }

   // VERSION: 1.9
   // Add a new relation of the specified type with the specified target to 
   // the current relation set if the relation set does not contain a relation
   // of that type. If it is does contain a relation of that typea the target
   // is added to the relation.
   // <relationship>: an #AtkRelationType
   // <target>: an #AtkObject
   void add_relation_by_type(AT0)(RelationType relationship, AT0 /*Object*/ target) nothrow {
      atk_relation_set_add_relation_by_type(&this, relationship, UpCast!(Object*)(target));
   }

   // Determines whether the relation set contains a relation that matches the
   // specified type.
   // in @set, %FALSE otherwise
   // RETURNS: %TRUE if @relationship is the relationship type of a relation
   // <relationship>: an #AtkRelationType
   int contains()(RelationType relationship) nothrow {
      return atk_relation_set_contains(&this, relationship);
   }

   // Determines the number of relations in a relation set.
   // RETURNS: an integer representing the number of relations in the set.
   int get_n_relations()() nothrow {
      return atk_relation_set_get_n_relations(&this);
   }

   // Determines the relation at the specified position in the relation set.
   // position i in the set.
   // RETURNS: a #AtkRelation, which is the relation at
   // <i>: a gint representing a position in the set, starting from 0.
   Relation* get_relation()(int i) nothrow {
      return atk_relation_set_get_relation(&this, i);
   }

   // Finds a relation that matches the specified type.
   // specified type.
   // RETURNS: an #AtkRelation, which is a relation matching the
   // <relationship>: an #AtkRelationType
   Relation* get_relation_by_type()(RelationType relationship) nothrow {
      return atk_relation_set_get_relation_by_type(&this, relationship);
   }

   // Removes a relation from the relation set.
   // This function unref's the #AtkRelation so it will be deleted unless there
   // is another reference to it.
   // <relation>: an #AtkRelation
   void remove(AT0)(AT0 /*Relation*/ relation) nothrow {
      atk_relation_set_remove(&this, UpCast!(Relation*)(relation));
   }
}

struct RelationSetClass {
   GObject2.ObjectClass parent;
   Function pad1, pad2;
}

enum RelationType {
   NULL = 0,
   CONTROLLED_BY = 1,
   CONTROLLER_FOR = 2,
   LABEL_FOR = 3,
   LABELLED_BY = 4,
   MEMBER_OF = 5,
   NODE_CHILD_OF = 6,
   FLOWS_TO = 7,
   FLOWS_FROM = 8,
   SUBWINDOW_OF = 9,
   EMBEDS = 10,
   EMBEDDED_BY = 11,
   POPUP_FOR = 12,
   PARENT_WINDOW_OF = 13,
   DESCRIBED_BY = 14,
   DESCRIPTION_FOR = 15,
   NODE_PARENT_OF = 16,
   LAST_DEFINED = 17
}
enum Role {
   INVALID = 0,
   ACCEL_LABEL = 1,
   ALERT = 2,
   ANIMATION = 3,
   ARROW = 4,
   CALENDAR = 5,
   CANVAS = 6,
   CHECK_BOX = 7,
   CHECK_MENU_ITEM = 8,
   COLOR_CHOOSER = 9,
   COLUMN_HEADER = 10,
   COMBO_BOX = 11,
   DATE_EDITOR = 12,
   DESKTOP_ICON = 13,
   DESKTOP_FRAME = 14,
   DIAL = 15,
   DIALOG = 16,
   DIRECTORY_PANE = 17,
   DRAWING_AREA = 18,
   FILE_CHOOSER = 19,
   FILLER = 20,
   FONT_CHOOSER = 21,
   FRAME = 22,
   GLASS_PANE = 23,
   HTML_CONTAINER = 24,
   ICON = 25,
   IMAGE = 26,
   INTERNAL_FRAME = 27,
   LABEL = 28,
   LAYERED_PANE = 29,
   LIST = 30,
   LIST_ITEM = 31,
   MENU = 32,
   MENU_BAR = 33,
   MENU_ITEM = 34,
   OPTION_PANE = 35,
   PAGE_TAB = 36,
   PAGE_TAB_LIST = 37,
   PANEL = 38,
   PASSWORD_TEXT = 39,
   POPUP_MENU = 40,
   PROGRESS_BAR = 41,
   PUSH_BUTTON = 42,
   RADIO_BUTTON = 43,
   RADIO_MENU_ITEM = 44,
   ROOT_PANE = 45,
   ROW_HEADER = 46,
   SCROLL_BAR = 47,
   SCROLL_PANE = 48,
   SEPARATOR = 49,
   SLIDER = 50,
   SPLIT_PANE = 51,
   SPIN_BUTTON = 52,
   STATUSBAR = 53,
   TABLE = 54,
   TABLE_CELL = 55,
   TABLE_COLUMN_HEADER = 56,
   TABLE_ROW_HEADER = 57,
   TEAR_OFF_MENU_ITEM = 58,
   TERMINAL = 59,
   TEXT = 60,
   TOGGLE_BUTTON = 61,
   TOOL_BAR = 62,
   TOOL_TIP = 63,
   TREE = 64,
   TREE_TABLE = 65,
   UNKNOWN = 66,
   VIEWPORT = 67,
   WINDOW = 68,
   HEADER = 69,
   FOOTER = 70,
   PARAGRAPH = 71,
   RULER = 72,
   APPLICATION = 73,
   AUTOCOMPLETE = 74,
   EDITBAR = 75,
   EMBEDDED = 76,
   ENTRY = 77,
   CHART = 78,
   CAPTION = 79,
   DOCUMENT_FRAME = 80,
   HEADING = 81,
   PAGE = 82,
   SECTION = 83,
   REDUNDANT_OBJECT = 84,
   FORM = 85,
   LINK = 86,
   INPUT_METHOD_WINDOW = 87,
   TABLE_ROW = 88,
   TREE_ITEM = 89,
   DOCUMENT_SPREADSHEET = 90,
   DOCUMENT_PRESENTATION = 91,
   DOCUMENT_TEXT = 92,
   DOCUMENT_WEB = 93,
   DOCUMENT_EMAIL = 94,
   COMMENT = 95,
   LIST_BOX = 96,
   GROUPING = 97,
   IMAGE_MAP = 98,
   NOTIFICATION = 99,
   INFO_BAR = 100,
   LAST_DEFINED = 101
}
struct Selection /* Interface */ {
   mixin template __interface__() {
      // Adds the specified accessible child of the object to the
      // object's selection.
      // RETURNS: TRUE if success, FALSE otherwise.
      // <i>: a #gint specifying the child index.
      int add_selection()(int i) nothrow {
         return atk_selection_add_selection(cast(Selection*)&this, i);
      }

      // Clears the selection in the object so that no children in the object
      // are selected.
      // RETURNS: TRUE if success, FALSE otherwise.
      int clear_selection()() nothrow {
         return atk_selection_clear_selection(cast(Selection*)&this);
      }

      // Gets the number of accessible children currently selected.
      // indication of whether AtkSelectionIface is implemented, they should
      // use type checking/interface checking macros or the
      // atk_get_accessible_value() convenience method.
      // if @selection does not implement this interface.
      // RETURNS: a gint representing the number of items selected, or 0
      int get_selection_count()() nothrow {
         return atk_selection_get_selection_count(cast(Selection*)&this);
      }

      // Determines if the current child of this object is selected
      // indication of whether AtkSelectionIface is implemented, they should
      // use type checking/interface checking macros or the
      // atk_get_accessible_value() convenience method.
      // if @selection does not implement this interface.
      // RETURNS: a gboolean representing the specified child is selected, or 0
      // <i>: a #gint specifying the child index.
      int is_child_selected()(int i) nothrow {
         return atk_selection_is_child_selected(cast(Selection*)&this, i);
      }

      // Gets a reference to the accessible object representing the specified 
      // selected child of the object.
      // indication of whether AtkSelectionIface is implemented, they should
      // use type checking/interface checking macros or the
      // atk_get_accessible_value() convenience method.
      // accessible , or %NULL if @selection does not implement this interface.
      // RETURNS: an #AtkObject representing the selected
      // <i>: a #gint specifying the index in the selection set.  (e.g. the ith selection as opposed to the ith child).
      Object* /*new*/ ref_selection()(int i) nothrow {
         return atk_selection_ref_selection(cast(Selection*)&this, i);
      }

      // Removes the specified child of the object from the object's selection.
      // RETURNS: TRUE if success, FALSE otherwise.
      // <i>: a #gint specifying the index in the selection set.  (e.g. the ith selection as opposed to the ith child).
      int remove_selection()(int i) nothrow {
         return atk_selection_remove_selection(cast(Selection*)&this, i);
      }

      // Causes every child of the object to be selected if the object
      // supports multiple selections.
      // RETURNS: TRUE if success, FALSE otherwise.
      int select_all_selection()() nothrow {
         return atk_selection_select_all_selection(cast(Selection*)&this);
      }
      extern (C) alias static void function (Selection* this_, void* user_data=null) nothrow signal_selection_changed;

      ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
         return super_.signal_connect!name(cb, data, cf);
      }

      ulong signal_connect(string name:"selection-changed", CB/*:signal_selection_changed*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_selection_changed)||_ttmm!(CB, signal_selection_changed)()) {
         return signal_connect_data!()(&this, cast(char*)"selection-changed",
         cast(GObject2.Callback)cb, data, null, cf);
      }
   }
   mixin __interface__;
}

struct SelectionIface {
   GObject2.TypeInterface parent;

   // RETURNS: TRUE if success, FALSE otherwise.
   // <i>: a #gint specifying the child index.
   extern (C) int function (Selection* selection, int i) nothrow add_selection;
   // RETURNS: TRUE if success, FALSE otherwise.
   extern (C) int function (Selection* selection) nothrow clear_selection;

   // RETURNS: an #AtkObject representing the selected
   // <i>: a #gint specifying the index in the selection set.  (e.g. the ith selection as opposed to the ith child).
   extern (C) Object* /*new*/ function (Selection* selection, int i) nothrow ref_selection;
   // RETURNS: a gint representing the number of items selected, or 0
   extern (C) int function (Selection* selection) nothrow get_selection_count;

   // RETURNS: a gboolean representing the specified child is selected, or 0
   // <i>: a #gint specifying the child index.
   extern (C) int function (Selection* selection, int i) nothrow is_child_selected;

   // RETURNS: TRUE if success, FALSE otherwise.
   // <i>: a #gint specifying the index in the selection set.  (e.g. the ith selection as opposed to the ith child).
   extern (C) int function (Selection* selection, int i) nothrow remove_selection;
   // RETURNS: TRUE if success, FALSE otherwise.
   extern (C) int function (Selection* selection) nothrow select_all_selection;
   extern (C) void function (Selection* selection) nothrow selection_changed;
   Function pad1, pad2;
}

struct Socket /* : Object */ {
   mixin Component.__interface__;
   alias parent this;
   alias parent super_;
   alias parent object;
   Object parent;
   private char* embedded_plug_id;

   static Socket* /*new*/ new_()() nothrow {
      return atk_socket_new();
   }
   static auto opCall()() {
      return atk_socket_new();
   }

   // VERSION: 1.30
   // Embeds the children of an #AtkPlug as the children of the #AtkSocket.  The
   // plug may be in the same process or in a different process.
   // THe class item used by this function should be filled in by the IPC layer
   // (ie, at-spi2-atk).  The implementor of the AtkSocket should call this
   // function and pass the id for the plug as returned by atk_plug_get_id.
   // It is the responsibility of the application to pass the plug id on to
   // the process implementing the AtkSocket as needed.
   // <plug_id>: the ID of an #AtkPlug
   void embed(AT0)(AT0 /*char*/ plug_id) nothrow {
      atk_socket_embed(&this, toCString!(char*)(plug_id));
   }

   // VERSION: 1.30
   // Determines whether or not the socket has an embedded plug.
   // RETURNS: TRUE if a plug is embedded in the socket
   int is_occupied()() nothrow {
      return atk_socket_is_occupied(&this);
   }
}

struct SocketClass {
   ObjectClass parent_class;
   // <plug_id>: the ID of an #AtkPlug
   extern (C) void function (Socket* obj, char* plug_id) nothrow embed;
}

struct StateSet /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;


   // Creates a new empty state set.
   // RETURNS: a new #AtkStateSet
   static StateSet* /*new*/ new_()() nothrow {
      return atk_state_set_new();
   }
   static auto opCall()() {
      return atk_state_set_new();
   }

   // Add a new state for the specified type to the current state set if
   // it is not already present.
   // RETURNS: %TRUE if  the state for @type is not already in @set.
   // <type>: an #AtkStateType
   int add_state()(StateType type) nothrow {
      return atk_state_set_add_state(&this, type);
   }

   // Add the states for the specified types to the current state set.
   // <types>: an array of #AtkStateType
   // <n_types>: The number of elements in the array
   void add_states(AT0)(AT0 /*StateType*/ types, int n_types) nothrow {
      atk_state_set_add_states(&this, UpCast!(StateType*)(types), n_types);
   }

   // Constructs the intersection of the two sets, returning %NULL if the
   // intersection is empty.
   // the two sets.
   // RETURNS: a new #AtkStateSet which is the intersection of
   // <compare_set>: another #AtkStateSet
   StateSet* /*new*/ and_sets(AT0)(AT0 /*StateSet*/ compare_set) nothrow {
      return atk_state_set_and_sets(&this, UpCast!(StateSet*)(compare_set));
   }
   // Removes all states from the state set.
   void clear_states()() nothrow {
      atk_state_set_clear_states(&this);
   }

   // Checks whether the state for the specified type is in the specified set.
   // RETURNS: %TRUE if @type is the state type is in @set.
   // <type>: an #AtkStateType
   int contains_state()(StateType type) nothrow {
      return atk_state_set_contains_state(&this, type);
   }

   // Checks whether the states for all the specified types are in the 
   // specified set.
   // RETURNS: %TRUE if all the states for @type are in @set.
   // <types>: an array of #AtkStateType
   // <n_types>: The number of elements in the array
   int contains_states(AT0)(AT0 /*StateType*/ types, int n_types) nothrow {
      return atk_state_set_contains_states(&this, UpCast!(StateType*)(types), n_types);
   }

   // Checks whether the state set is empty, i.e. has no states set.
   // RETURNS: %TRUE if @set has no states set, otherwise %FALSE
   int is_empty()() nothrow {
      return atk_state_set_is_empty(&this);
   }

   // Constructs the union of the two sets.
   // sets, returning %NULL is empty.
   // RETURNS: a new #AtkStateSet which is the union of the two
   // <compare_set>: another #AtkStateSet
   StateSet* /*new*/ or_sets(AT0)(AT0 /*StateSet*/ compare_set) nothrow {
      return atk_state_set_or_sets(&this, UpCast!(StateSet*)(compare_set));
   }

   // Removes the state for the specified type from the state set.
   // RETURNS: %TRUE if @type was the state type is in @set.
   // <type>: an #AtkType
   int remove_state()(StateType type) nothrow {
      return atk_state_set_remove_state(&this, type);
   }

   // Constructs the exclusive-or of the two sets, returning %NULL is empty.
   // The set returned by this operation contains the states in exactly
   // one of the two sets.
   // which are in exactly one of the two sets.
   // RETURNS: a new #AtkStateSet which contains the states
   // <compare_set>: another #AtkStateSet
   StateSet* /*new*/ xor_sets(AT0)(AT0 /*StateSet*/ compare_set) nothrow {
      return atk_state_set_xor_sets(&this, UpCast!(StateSet*)(compare_set));
   }
}

struct StateSetClass {
   GObject2.ObjectClass parent;
}

enum StateType {
   INVALID = 0,
   ACTIVE = 1,
   ARMED = 2,
   BUSY = 3,
   CHECKED = 4,
   DEFUNCT = 5,
   EDITABLE = 6,
   ENABLED = 7,
   EXPANDABLE = 8,
   EXPANDED = 9,
   FOCUSABLE = 10,
   FOCUSED = 11,
   HORIZONTAL = 12,
   ICONIFIED = 13,
   MODAL = 14,
   MULTI_LINE = 15,
   MULTISELECTABLE = 16,
   OPAQUE = 17,
   PRESSED = 18,
   RESIZABLE = 19,
   SELECTABLE = 20,
   SELECTED = 21,
   SENSITIVE = 22,
   SHOWING = 23,
   SINGLE_LINE = 24,
   STALE = 25,
   TRANSIENT = 26,
   VERTICAL = 27,
   VISIBLE = 28,
   MANAGES_DESCENDANTS = 29,
   INDETERMINATE = 30,
   TRUNCATED = 31,
   REQUIRED = 32,
   INVALID_ENTRY = 33,
   SUPPORTS_AUTOCOMPLETION = 34,
   SELECTABLE_TEXT = 35,
   DEFAULT = 36,
   ANIMATED = 37,
   VISITED = 38,
   LAST_DEFINED = 39
}
struct StreamableContent /* Interface */ {
   mixin template __interface__() {
      // Gets the character string of the specified mime type. The first mime
      // type is at position 0, the second at position 1, and so on.
      // should not free the character string.
      // <i>: a gint representing the position of the mime type starting from 0
      char* get_mime_type()(int i) nothrow {
         return atk_streamable_content_get_mime_type(cast(StreamableContent*)&this, i);
      }

      // Gets the number of mime types supported by this object.
      // RETURNS: a gint which is the number of mime types supported by the object.
      int get_n_mime_types()() nothrow {
         return atk_streamable_content_get_n_mime_types(cast(StreamableContent*)&this);
      }

      // Unintrospectable method: get_stream() / atk_streamable_content_get_stream()
      // Gets the content in the specified mime type.
      // specified mime type.
      // RETURNS: A #GIOChannel which contains the content in the
      // <mime_type>: a gchar* representing the mime type
      GLib2.IOChannel* /*new*/ get_stream(AT0)(AT0 /*char*/ mime_type) nothrow {
         return atk_streamable_content_get_stream(cast(StreamableContent*)&this, toCString!(char*)(mime_type));
      }

      // VERSION: 1.12
      // Get a string representing a URI in IETF standard format
      // (see http://www.ietf.org/rfc/rfc2396.txt) from which the object's content
      // may be streamed in the specified mime-type, if one is available.
      // If mime_type is NULL, the URI for the default (and possibly only) mime-type is
      // returned. 
      // Note that it is possible for get_uri to return NULL but for
      // get_stream to work nonetheless, since not all GIOChannels connect to URIs.
      // can be constructed.
      // RETURNS: Returns a string representing a URI, or NULL if no corresponding URI
      // <mime_type>: a gchar* representing the mime type, or NULL to request a URI for the default mime type.
      char* get_uri(AT0)(AT0 /*char*/ mime_type) nothrow {
         return atk_streamable_content_get_uri(cast(StreamableContent*)&this, toCString!(char*)(mime_type));
      }
   }
   mixin __interface__;
}

struct StreamableContentIface {
   GObject2.TypeInterface parent;
   // RETURNS: a gint which is the number of mime types supported by the object.
   extern (C) int function (StreamableContent* streamable) nothrow get_n_mime_types;
   // <i>: a gint representing the position of the mime type starting from 0
   extern (C) char* function (StreamableContent* streamable, int i) nothrow get_mime_type;

   // Unintrospectable functionp: get_stream() / ()
   // 
   // RETURNS: A #GIOChannel which contains the content in the
   // <mime_type>: a gchar* representing the mime type
   extern (C) GLib2.IOChannel* /*new*/ function (StreamableContent* streamable, char* mime_type) nothrow get_stream;

   // RETURNS: Returns a string representing a URI, or NULL if no corresponding URI
   // <mime_type>: a gchar* representing the mime type, or NULL to request a URI for the default mime type.
   extern (C) char* function (StreamableContent* streamable, char* mime_type) nothrow get_uri;
   Function pad1, pad2, pad3;
}

struct Table /* Interface */ {
   mixin template __interface__() {
      // Adds the specified @column to the selection. 
      // the selection, or 0 if value does not implement this interface.
      // RETURNS: a gboolean representing if the column was successfully added to
      // <column>: a #gint representing a column in @table
      int add_column_selection()(int column) nothrow {
         return atk_table_add_column_selection(cast(Table*)&this, column);
      }

      // Adds the specified @row to the selection. 
      // or 0 if value does not implement this interface.
      // RETURNS: a gboolean representing if row was successfully added to selection,
      // <row>: a #gint representing a row in @table
      int add_row_selection()(int row) nothrow {
         return atk_table_add_row_selection(cast(Table*)&this, row);
      }

      // Gets the caption for the @table.
      // %NULL if value does not implement this interface.
      // RETURNS: a AtkObject* representing the table caption, or
      Object* get_caption()() nothrow {
         return atk_table_get_caption(cast(Table*)&this);
      }

      // Gets a #gint representing the column at the specified @index_. 
      // or -1 if the table does not implement this interface
      // RETURNS: a gint representing the column at the specified index,
      // <index_>: a #gint representing an index in @table
      int get_column_at_index()(int index_) nothrow {
         return atk_table_get_column_at_index(cast(Table*)&this, index_);
      }

      // Gets the description text of the specified @column in the table
      // if value does not implement this interface.
      // RETURNS: a gchar* representing the column description, or %NULL
      // <column>: a #gint representing a column in @table
      char* get_column_description()(int column) nothrow {
         return atk_table_get_column_description(cast(Table*)&this, column);
      }

      // Gets the number of columns occupied by the accessible object
      // at the specified @row and @column in the @table.
      // if value does not implement this interface.
      // RETURNS: a gint representing the column extent at specified position, or 0
      // <row>: a #gint representing a row in @table
      // <column>: a #gint representing a column in @table
      int get_column_extent_at()(int row, int column) nothrow {
         return atk_table_get_column_extent_at(cast(Table*)&this, row, column);
      }

      // Gets the column header of a specified column in an accessible table.
      // header, or %NULL if value does not implement this interface.
      // RETURNS: a AtkObject* representing the specified column
      // <column>: a #gint representing a column in the table
      Object* get_column_header()(int column) nothrow {
         return atk_table_get_column_header(cast(Table*)&this, column);
      }

      // Gets a #gint representing the index at the specified @row and @column.
      // The value -1 is returned if the object at row,column is not a child
      // of table or table does not implement this interface.
      // RETURNS: a #gint representing the index at specified position.
      // <row>: a #gint representing a row in @table
      // <column>: a #gint representing a column in @table
      int get_index_at()(int row, int column) nothrow {
         return atk_table_get_index_at(cast(Table*)&this, row, column);
      }

      // Gets the number of columns in the table.
      // if value does not implement this interface.
      // RETURNS: a gint representing the number of columns, or 0
      int get_n_columns()() nothrow {
         return atk_table_get_n_columns(cast(Table*)&this);
      }

      // Gets the number of rows in the table.
      // if value does not implement this interface.
      // RETURNS: a gint representing the number of rows, or 0
      int get_n_rows()() nothrow {
         return atk_table_get_n_rows(cast(Table*)&this);
      }

      // Gets a #gint representing the row at the specified @index_.
      // or -1 if the table does not implement this interface
      // RETURNS: a gint representing the row at the specified index,
      // <index_>: a #gint representing an index in @table
      int get_row_at_index()(int index_) nothrow {
         return atk_table_get_row_at_index(cast(Table*)&this, index_);
      }

      // Gets the description text of the specified row in the table
      // if value does not implement this interface.
      // RETURNS: a gchar* representing the row description, or %NULL
      // <row>: a #gint representing a row in @table
      char* get_row_description()(int row) nothrow {
         return atk_table_get_row_description(cast(Table*)&this, row);
      }

      // Gets the number of rows occupied by the accessible object
      // at a specified @row and @column in the @table.
      // if value does not implement this interface.
      // RETURNS: a gint representing the row extent at specified position, or 0
      // <row>: a #gint representing a row in @table
      // <column>: a #gint representing a column in @table
      int get_row_extent_at()(int row, int column) nothrow {
         return atk_table_get_row_extent_at(cast(Table*)&this, row, column);
      }

      // Gets the row header of a specified row in an accessible table.
      // header, or %NULL if value does not implement this interface.
      // RETURNS: a AtkObject* representing the specified row
      // <row>: a #gint representing a row in the table
      Object* get_row_header()(int row) nothrow {
         return atk_table_get_row_header(cast(Table*)&this, row);
      }

      // Gets the selected columns of the table by initializing **selected with 
      // the selected column numbers. This array should be freed by the caller.
      // or %0 if value does not implement this interface.
      // RETURNS: a gint representing the number of selected columns,
      // <selected>: a #gint** that is to contain the selected columns numbers
      int get_selected_columns()(int** selected) nothrow {
         return atk_table_get_selected_columns(cast(Table*)&this, selected);
      }

      // Gets the selected rows of the table by initializing **selected with 
      // the selected row numbers. This array should be freed by the caller.
      // or zero if value does not implement this interface.
      // RETURNS: a gint representing the number of selected rows,
      // <selected>: a #gint** that is to contain the selected row numbers
      int get_selected_rows()(int** selected) nothrow {
         return atk_table_get_selected_rows(cast(Table*)&this, selected);
      }

      // Gets the summary description of the table.
      // of the table, or zero if value does not implement this interface.
      // RETURNS: a AtkObject* representing a summary description
      Object* /*new*/ get_summary()() nothrow {
         return atk_table_get_summary(cast(Table*)&this);
      }

      // Gets a boolean value indicating whether the specified @column
      // is selected
      // if value does not implement this interface.
      // RETURNS: a gboolean representing if the column is selected, or 0
      // <column>: a #gint representing a column in @table
      int is_column_selected()(int column) nothrow {
         return atk_table_is_column_selected(cast(Table*)&this, column);
      }

      // Gets a boolean value indicating whether the specified @row
      // is selected
      // if value does not implement this interface.
      // RETURNS: a gboolean representing if the row is selected, or 0
      // <row>: a #gint representing a row in @table
      int is_row_selected()(int row) nothrow {
         return atk_table_is_row_selected(cast(Table*)&this, row);
      }

      // Gets a boolean value indicating whether the accessible object
      // at the specified @row and @column is selected
      // if value does not implement this interface.
      // RETURNS: a gboolean representing if the cell is selected, or 0
      // <row>: a #gint representing a row in @table
      // <column>: a #gint representing a column in @table
      int is_selected()(int row, int column) nothrow {
         return atk_table_is_selected(cast(Table*)&this, row, column);
      }

      // Get a reference to the table cell at @row, @column.
      // accessible
      // RETURNS: a AtkObject* representing the referred to
      // <row>: a #gint representing a row in @table
      // <column>: a #gint representing a column in @table
      Object* /*new*/ ref_at()(int row, int column) nothrow {
         return atk_table_ref_at(cast(Table*)&this, row, column);
      }

      // Adds the specified @column to the selection. 
      // the selection, or 0 if value does not implement this interface.
      // RETURNS: a gboolean representing if the column was successfully removed from
      // <column>: a #gint representing a column in @table
      int remove_column_selection()(int column) nothrow {
         return atk_table_remove_column_selection(cast(Table*)&this, column);
      }

      // Removes the specified @row from the selection. 
      // the selection, or 0 if value does not implement this interface.
      // RETURNS: a gboolean representing if the row was successfully removed from
      // <row>: a #gint representing a row in @table
      int remove_row_selection()(int row) nothrow {
         return atk_table_remove_row_selection(cast(Table*)&this, row);
      }

      // Sets the caption for the table.
      // <caption>: a #AtkObject representing the caption to set for @table
      void set_caption(AT0)(AT0 /*Object*/ caption) nothrow {
         atk_table_set_caption(cast(Table*)&this, UpCast!(Object*)(caption));
      }

      // Sets the description text for the specified @column of the @table.
      // <column>: a #gint representing a column in @table
      // <description>: a #gchar representing the description text to set for the specified @column of the @table
      void set_column_description(AT0)(int column, AT0 /*char*/ description) nothrow {
         atk_table_set_column_description(cast(Table*)&this, column, toCString!(char*)(description));
      }

      // Sets the specified column header to @header.
      // <column>: a #gint representing a column in @table
      // <header>: an #AtkTable
      void set_column_header(AT0)(int column, AT0 /*Object*/ header) nothrow {
         atk_table_set_column_header(cast(Table*)&this, column, UpCast!(Object*)(header));
      }

      // Sets the description text for the specified @row of @table.
      // <row>: a #gint representing a row in @table
      // <description>: a #gchar representing the description text to set for the specified @row of @table
      void set_row_description(AT0)(int row, AT0 /*char*/ description) nothrow {
         atk_table_set_row_description(cast(Table*)&this, row, toCString!(char*)(description));
      }

      // Sets the specified row header to @header.
      // <row>: a #gint representing a row in @table
      // <header>: an #AtkTable
      void set_row_header(AT0)(int row, AT0 /*Object*/ header) nothrow {
         atk_table_set_row_header(cast(Table*)&this, row, UpCast!(Object*)(header));
      }

      // Sets the summary description of the table.
      // <accessible>: an #AtkObject representing the summary description to set for @table
      void set_summary(AT0)(AT0 /*Object*/ accessible) nothrow {
         atk_table_set_summary(cast(Table*)&this, UpCast!(Object*)(accessible));
      }
      extern (C) alias static void function (Table* this_, int object, int p0, void* user_data=null) nothrow signal_column_deleted;

      ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
         return super_.signal_connect!name(cb, data, cf);
      }

      ulong signal_connect(string name:"column-deleted", CB/*:signal_column_deleted*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_column_deleted)||_ttmm!(CB, signal_column_deleted)()) {
         return signal_connect_data!()(&this, cast(char*)"column-deleted",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Table* this_, int object, int p0, void* user_data=null) nothrow signal_column_inserted;
      ulong signal_connect(string name:"column-inserted", CB/*:signal_column_inserted*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_column_inserted)||_ttmm!(CB, signal_column_inserted)()) {
         return signal_connect_data!()(&this, cast(char*)"column-inserted",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Table* this_, void* user_data=null) nothrow signal_column_reordered;
      ulong signal_connect(string name:"column-reordered", CB/*:signal_column_reordered*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_column_reordered)||_ttmm!(CB, signal_column_reordered)()) {
         return signal_connect_data!()(&this, cast(char*)"column-reordered",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Table* this_, void* user_data=null) nothrow signal_model_changed;
      ulong signal_connect(string name:"model-changed", CB/*:signal_model_changed*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_model_changed)||_ttmm!(CB, signal_model_changed)()) {
         return signal_connect_data!()(&this, cast(char*)"model-changed",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Table* this_, int object, int p0, void* user_data=null) nothrow signal_row_deleted;
      ulong signal_connect(string name:"row-deleted", CB/*:signal_row_deleted*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_row_deleted)||_ttmm!(CB, signal_row_deleted)()) {
         return signal_connect_data!()(&this, cast(char*)"row-deleted",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Table* this_, int object, int p0, void* user_data=null) nothrow signal_row_inserted;
      ulong signal_connect(string name:"row-inserted", CB/*:signal_row_inserted*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_row_inserted)||_ttmm!(CB, signal_row_inserted)()) {
         return signal_connect_data!()(&this, cast(char*)"row-inserted",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Table* this_, void* user_data=null) nothrow signal_row_reordered;
      ulong signal_connect(string name:"row-reordered", CB/*:signal_row_reordered*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_row_reordered)||_ttmm!(CB, signal_row_reordered)()) {
         return signal_connect_data!()(&this, cast(char*)"row-reordered",
         cast(GObject2.Callback)cb, data, null, cf);
      }
   }
   mixin __interface__;
}

struct TableIface {
   GObject2.TypeInterface parent;

   // RETURNS: a AtkObject* representing the referred to
   // <row>: a #gint representing a row in @table
   // <column>: a #gint representing a column in @table
   extern (C) Object* /*new*/ function (Table* table, int row, int column) nothrow ref_at;

   // RETURNS: a #gint representing the index at specified position.
   // <row>: a #gint representing a row in @table
   // <column>: a #gint representing a column in @table
   extern (C) int function (Table* table, int row, int column) nothrow get_index_at;

   // RETURNS: a gint representing the column at the specified index,
   // <index_>: a #gint representing an index in @table
   extern (C) int function (Table* table, int index_) nothrow get_column_at_index;

   // RETURNS: a gint representing the row at the specified index,
   // <index_>: a #gint representing an index in @table
   extern (C) int function (Table* table, int index_) nothrow get_row_at_index;
   // RETURNS: a gint representing the number of columns, or 0
   extern (C) int function (Table* table) nothrow get_n_columns;
   // RETURNS: a gint representing the number of rows, or 0
   extern (C) int function (Table* table) nothrow get_n_rows;

   // RETURNS: a gint representing the column extent at specified position, or 0
   // <row>: a #gint representing a row in @table
   // <column>: a #gint representing a column in @table
   extern (C) int function (Table* table, int row, int column) nothrow get_column_extent_at;

   // RETURNS: a gint representing the row extent at specified position, or 0
   // <row>: a #gint representing a row in @table
   // <column>: a #gint representing a column in @table
   extern (C) int function (Table* table, int row, int column) nothrow get_row_extent_at;
   // RETURNS: a AtkObject* representing the table caption, or
   extern (C) Object* function (Table* table) nothrow get_caption;

   // RETURNS: a gchar* representing the column description, or %NULL
   // <column>: a #gint representing a column in @table
   extern (C) char* function (Table* table, int column) nothrow get_column_description;

   // RETURNS: a AtkObject* representing the specified column
   // <column>: a #gint representing a column in the table
   extern (C) Object* function (Table* table, int column) nothrow get_column_header;

   // RETURNS: a gchar* representing the row description, or %NULL
   // <row>: a #gint representing a row in @table
   extern (C) char* function (Table* table, int row) nothrow get_row_description;

   // RETURNS: a AtkObject* representing the specified row
   // <row>: a #gint representing a row in the table
   extern (C) Object* function (Table* table, int row) nothrow get_row_header;
   // RETURNS: a AtkObject* representing a summary description
   extern (C) Object* /*new*/ function (Table* table) nothrow get_summary;
   // <caption>: a #AtkObject representing the caption to set for @table
   extern (C) void function (Table* table, Object* caption) nothrow set_caption;

   // <column>: a #gint representing a column in @table
   // <description>: a #gchar representing the description text to set for the specified @column of the @table
   extern (C) void function (Table* table, int column, char* description) nothrow set_column_description;

   // <column>: a #gint representing a column in @table
   // <header>: an #AtkTable
   extern (C) void function (Table* table, int column, Object* header) nothrow set_column_header;

   // <row>: a #gint representing a row in @table
   // <description>: a #gchar representing the description text to set for the specified @row of @table
   extern (C) void function (Table* table, int row, char* description) nothrow set_row_description;

   // <row>: a #gint representing a row in @table
   // <header>: an #AtkTable
   extern (C) void function (Table* table, int row, Object* header) nothrow set_row_header;
   // <accessible>: an #AtkObject representing the summary description to set for @table
   extern (C) void function (Table* table, Object* accessible) nothrow set_summary;

   // RETURNS: a gint representing the number of selected columns,
   // <selected>: a #gint** that is to contain the selected columns numbers
   extern (C) int function (Table* table, int** selected) nothrow get_selected_columns;

   // RETURNS: a gint representing the number of selected rows,
   // <selected>: a #gint** that is to contain the selected row numbers
   extern (C) int function (Table* table, int** selected) nothrow get_selected_rows;

   // RETURNS: a gboolean representing if the column is selected, or 0
   // <column>: a #gint representing a column in @table
   extern (C) int function (Table* table, int column) nothrow is_column_selected;

   // RETURNS: a gboolean representing if the row is selected, or 0
   // <row>: a #gint representing a row in @table
   extern (C) int function (Table* table, int row) nothrow is_row_selected;

   // RETURNS: a gboolean representing if the cell is selected, or 0
   // <row>: a #gint representing a row in @table
   // <column>: a #gint representing a column in @table
   extern (C) int function (Table* table, int row, int column) nothrow is_selected;

   // RETURNS: a gboolean representing if row was successfully added to selection,
   // <row>: a #gint representing a row in @table
   extern (C) int function (Table* table, int row) nothrow add_row_selection;

   // RETURNS: a gboolean representing if the row was successfully removed from
   // <row>: a #gint representing a row in @table
   extern (C) int function (Table* table, int row) nothrow remove_row_selection;

   // RETURNS: a gboolean representing if the column was successfully added to
   // <column>: a #gint representing a column in @table
   extern (C) int function (Table* table, int column) nothrow add_column_selection;

   // RETURNS: a gboolean representing if the column was successfully removed from
   // <column>: a #gint representing a column in @table
   extern (C) int function (Table* table, int column) nothrow remove_column_selection;
   extern (C) void function (Table* table, int row, int num_inserted) nothrow row_inserted;
   extern (C) void function (Table* table, int column, int num_inserted) nothrow column_inserted;
   extern (C) void function (Table* table, int row, int num_deleted) nothrow row_deleted;
   extern (C) void function (Table* table, int column, int num_deleted) nothrow column_deleted;
   extern (C) void function (Table* table) nothrow row_reordered;
   extern (C) void function (Table* table) nothrow column_reordered;
   extern (C) void function (Table* table) nothrow model_changed;
   Function pad1, pad2, pad3, pad4;
}

struct Text /* Interface */ {
   mixin template __interface__() {
      // Adds a selection bounded by the specified offsets.
      // RETURNS: %TRUE if success, %FALSE otherwise
      // <start_offset>: the start position of the selected region
      // <end_offset>: the offset of the first character after the selected region.
      int add_selection()(int start_offset, int end_offset) nothrow {
         return atk_text_add_selection(cast(Text*)&this, start_offset, end_offset);
      }

      // VERSION: 1.3
      // Get the ranges of text in the specified bounding box.
      // element of the array returned by this function will be NULL.
      // RETURNS: Array of AtkTextRange. The last
      // <rect>: An AtkTextRectangle giving the dimensions of the bounding box.
      // <coord_type>: Specify whether coordinates are relative to the screen or widget window.
      // <x_clip_type>: Specify the horizontal clip type.
      // <y_clip_type>: Specify the vertical clip type.
      TextRange** /*new*/ get_bounded_ranges(AT0)(AT0 /*TextRectangle*/ rect, CoordType coord_type, TextClipType x_clip_type, TextClipType y_clip_type) nothrow {
         return atk_text_get_bounded_ranges(cast(Text*)&this, UpCast!(TextRectangle*)(rect), coord_type, x_clip_type, y_clip_type);
      }

      // Gets the offset position of the caret (cursor).
      // RETURNS: the offset position of the caret (cursor).
      int get_caret_offset()() nothrow {
         return atk_text_get_caret_offset(cast(Text*)&this);
      }

      // Gets the specified text.
      // RETURNS: the character at @offset.
      // <offset>: position
      dchar get_character_at_offset()(int offset) nothrow {
         return atk_text_get_character_at_offset(cast(Text*)&this, offset);
      }

      // Gets the character count.
      // RETURNS: the number of characters.
      int get_character_count()() nothrow {
         return atk_text_get_character_count(cast(Text*)&this);
      }

      // Get the bounding box containing the glyph representing the character at 
      // a particular text offset.
      // <offset>: The offset of the text character for which bounding information is required.
      // <x>: Pointer for the x cordinate of the bounding box
      // <y>: Pointer for the y cordinate of the bounding box
      // <width>: Pointer for the width of the bounding box
      // <height>: Pointer for the height of the bounding box
      // <coords>: specify whether coordinates are relative to the screen or widget window
      void get_character_extents()(int offset, int* x, int* y, int* width, int* height, CoordType coords) nothrow {
         atk_text_get_character_extents(cast(Text*)&this, offset, x, y, width, height, coords);
      }

      // Creates an #AtkAttributeSet which consists of the default values of
      // attributes for the text. See the enum AtkTextAttribute for types of text 
      // attributes that can be returned. Note that other attributes may also be 
      // returned.
      // values of attributes.  at @offset. this #atkattributeset should be freed by
      // a call to atk_attribute_set_free().
      // RETURNS: an #AtkAttributeSet which contains the default
      AttributeSet* /*new*/ get_default_attributes()() nothrow {
         return atk_text_get_default_attributes(cast(Text*)&this);
      }

      // Gets the number of selected regions.
      // occurred.
      // RETURNS: The number of selected regions, or -1 if a failure
      int get_n_selections()() nothrow {
         return atk_text_get_n_selections(cast(Text*)&this);
      }

      // Gets the offset of the character located at coordinates @x and @y. @x and @y
      // are interpreted as being relative to the screen or this widget's window
      // depending on @coords.
      // the specified @x and @y coordinates.
      // RETURNS: the offset to the character which is located at
      // <x>: screen x-position of character
      // <y>: screen y-position of character
      // <coords>: specify whether coordinates are relative to the screen or widget window
      int get_offset_at_point()(int x, int y, CoordType coords) nothrow {
         return atk_text_get_offset_at_point(cast(Text*)&this, x, y, coords);
      }

      // VERSION: 1.3
      // Get the bounding box for text within the specified range.
      // <start_offset>: The offset of the first text character for which boundary information is required.
      // <end_offset>: The offset of the text character after the last character for which boundary information is required.
      // <coord_type>: Specify whether coordinates are relative to the screen or widget window.
      // <rect>: A pointer to a AtkTextRectangle which is filled in by this function.
      void get_range_extents(AT0)(int start_offset, int end_offset, CoordType coord_type, AT0 /*TextRectangle*/ rect) nothrow {
         atk_text_get_range_extents(cast(Text*)&this, start_offset, end_offset, coord_type, UpCast!(TextRectangle*)(rect));
      }

      // Creates an #AtkAttributeSet which consists of the attributes explicitly
      // set at the position @offset in the text. @start_offset and @end_offset are
      // set to the start and end of the range around @offset where the attributes are
      // invariant. Note that @end_offset is the offset of the first character
      // after the range.  See the enum AtkTextAttribute for types of text 
      // attributes that can be returned. Note that other attributes may also be 
      // returned.
      // explicitly set at @offset. This #AtkAttributeSet should be freed by a call
      // to atk_attribute_set_free().
      // RETURNS: an #AtkAttributeSet which contains the attributes
      // <offset>: the offset at which to get the attributes, -1 means the offset of the character to be inserted at the caret location.
      // <start_offset>: the address to put the start offset of the range
      // <end_offset>: the address to put the end offset of the range
      AttributeSet* /*new*/ get_run_attributes()(int offset, int* start_offset, int* end_offset) nothrow {
         return atk_text_get_run_attributes(cast(Text*)&this, offset, start_offset, end_offset);
      }

      // Gets the text from the specified selection.
      // to free the returned string.
      // RETURNS: a newly allocated string containing the selected text. Use g_free()
      // <selection_num>: The selection number.  The selected regions are assigned numbers that correspond to how far the region is from the start of the text.  The selected region closest to the beginning of the text region is assigned the number 0, etc.  Note that adding, moving or deleting a selected region can change the numbering.
      // <start_offset>: passes back the start position of the selected region
      // <end_offset>: passes back the end position of (e.g. offset immediately past) the selected region
      char* /*new*/ get_selection()(int selection_num, int* start_offset, int* end_offset) nothrow {
         return atk_text_get_selection(cast(Text*)&this, selection_num, start_offset, end_offset);
      }

      // Gets the specified text.
      // to, but not including @end_offset. Use g_free() to free the returned string.
      // RETURNS: a newly allocated string containing the text from @start_offset up
      // <start_offset>: start position
      // <end_offset>: end position
      char* /*new*/ get_text()(int start_offset, int end_offset) nothrow {
         return atk_text_get_text(cast(Text*)&this, start_offset, end_offset);
      }

      // Gets the specified text.
      // If the boundary_type if ATK_TEXT_BOUNDARY_CHAR the character after the 
      // offset is returned.
      // If the boundary_type is ATK_TEXT_BOUNDARY_WORD_START the returned string
      // is from the word start after the offset to the next word start.
      // The returned string will contain the word after the offset if the offset 
      // is inside a word or if the offset is not inside a word.
      // If the boundary_type is ATK_TEXT_BOUNDARY_WORD_END the returned string
      // is from the word end at or after the offset to the next work end.
      // The returned string will contain the word after the offset if the offset
      // is inside a word and will contain the word after the word after the offset
      // if the offset is not inside a word.
      // If the boundary type is ATK_TEXT_BOUNDARY_SENTENCE_START the returned
      // string is from the sentence start after the offset to the next sentence
      // start.
      // The returned string will contain the sentence after the offset if the offset
      // is inside a sentence or if the offset is not inside a sentence.
      // If the boundary_type is ATK_TEXT_BOUNDARY_SENTENCE_END the returned string
      // is from the sentence end at or after the offset to the next sentence end.
      // The returned string will contain the sentence after the offset if the offset
      // is inside a sentence and will contain the sentence after the sentence
      // after the offset if the offset is not inside a sentence.
      // If the boundary type is ATK_TEXT_BOUNDARY_LINE_START the returned
      // string is from the line start after the offset to the next line start.
      // If the boundary_type is ATK_TEXT_BOUNDARY_LINE_END the returned string
      // is from the line end at or after the offset to the next line end.
      // by the specified @boundary_type. Use g_free() to free the returned string.
      // RETURNS: a newly allocated string containing the text after @offset bounded
      // <offset>: position
      // <boundary_type>: An #AtkTextBoundary
      // <start_offset>: the start offset of the returned string
      // <end_offset>: the offset of the first character after the returned substring
      char* /*new*/ get_text_after_offset()(int offset, TextBoundary boundary_type, int* start_offset, int* end_offset) nothrow {
         return atk_text_get_text_after_offset(cast(Text*)&this, offset, boundary_type, start_offset, end_offset);
      }

      // Gets the specified text.
      // If the boundary_type if ATK_TEXT_BOUNDARY_CHAR the character at the
      // offset is returned.
      // If the boundary_type is ATK_TEXT_BOUNDARY_WORD_START the returned string
      // is from the word start at or before the offset to the word start after 
      // the offset.
      // The returned string will contain the word at the offset if the offset
      // is inside a word and will contain the word before the offset if the 
      // offset is not inside a word.
      // If the boundary_type is ATK_TEXT_BOUNDARY_WORD_END the returned string
      // is from the word end before the offset to the word end at or after the
      // offset.
      // The returned string will contain the word at the offset if the offset
      // is inside a word and will contain the word after to the offset if the 
      // offset is not inside a word.
      // If the boundary type is ATK_TEXT_BOUNDARY_SENTENCE_START the returned
      // string is from the sentence start at or before the offset to the sentence
      // start after the offset.
      // The returned string will contain the sentence at the offset if the offset
      // is inside a sentence and will contain the sentence before the offset 
      // if the offset is not inside a sentence.
      // If the boundary_type is ATK_TEXT_BOUNDARY_SENTENCE_END the returned string
      // is from the sentence end before the offset to the sentence end at or
      // after the offset.
      // The returned string will contain the sentence at the offset if the offset
      // is inside a sentence and will contain the sentence after the offset 
      // if the offset is not inside a sentence.
      // If the boundary type is ATK_TEXT_BOUNDARY_LINE_START the returned
      // string is from the line start at or before the offset to the line
      // start after the offset.
      // If the boundary_type is ATK_TEXT_BOUNDARY_LINE_END the returned string
      // is from the line end before the offset to the line end at or after
      // the offset.
      // the specified @boundary_type. Use g_free() to free the returned string.
      // RETURNS: a newly allocated string containing the text at @offset bounded by
      // <offset>: position
      // <boundary_type>: An #AtkTextBoundary
      // <start_offset>: the start offset of the returned string
      // <end_offset>: the offset of the first character after the returned substring
      char* /*new*/ get_text_at_offset()(int offset, TextBoundary boundary_type, int* start_offset, int* end_offset) nothrow {
         return atk_text_get_text_at_offset(cast(Text*)&this, offset, boundary_type, start_offset, end_offset);
      }

      // Gets the specified text.
      // If the boundary_type if ATK_TEXT_BOUNDARY_CHAR the character before the
      // offset is returned.
      // If the boundary_type is ATK_TEXT_BOUNDARY_WORD_START the returned string
      // is from the word start before the word start before or at the offset to 
      // the word start before or at the offset.
      // The returned string will contain the word before the offset if the offset
      // is inside a word and will contain the word before the word before the 
      // offset if the offset is not inside a word.
      // If the boundary_type is ATK_TEXT_BOUNDARY_WORD_END the returned string
      // is from the word end before the word end before the offset to the word
      // end before the offset.
      // The returned string will contain the word before the offset if the offset
      // is inside a word or if the offset is not inside a word.
      // If the boundary type is ATK_TEXT_BOUNDARY_SENTENCE_START the returned
      // string is from the sentence start before the sentence start before 
      // the offset to the sentence start before the offset.
      // The returned string will contain the sentence before the offset if the 
      // offset is inside a sentence and will contain the sentence before the 
      // sentence before the offset if the offset is not inside a sentence.
      // If the boundary_type is ATK_TEXT_BOUNDARY_SENTENCE_END the returned string
      // is from the sentence end before the sentence end at or before the offset to 
      // the sentence end at or before the offset.
      // The returned string will contain the sentence before the offset if the 
      // offset is inside a sentence or if the offset is not inside a sentence.
      // If the boundary type is ATK_TEXT_BOUNDARY_LINE_START the returned
      // string is from the line start before the line start ar or before the offset 
      // to the line start ar or before the offset.
      // If the boundary_type is ATK_TEXT_BOUNDARY_LINE_END the returned string
      // is from the line end before the line end before the offset to the 
      // line end before the offset.
      // by the specified @boundary_type. Use g_free() to free the returned string.
      // RETURNS: a newly allocated string containing the text before @offset bounded
      // <offset>: position
      // <boundary_type>: An #AtkTextBoundary
      // <start_offset>: the start offset of the returned string
      // <end_offset>: the offset of the first character after the returned substring
      char* /*new*/ get_text_before_offset()(int offset, TextBoundary boundary_type, int* start_offset, int* end_offset) nothrow {
         return atk_text_get_text_before_offset(cast(Text*)&this, offset, boundary_type, start_offset, end_offset);
      }

      // Removes the specified selection.
      // RETURNS: %TRUE if success, %FALSE otherwise
      // <selection_num>: The selection number.  The selected regions are assigned numbers that correspond to how far the region is from the start of the text.  The selected region closest to the beginning of the text region is assigned the number 0, etc.  Note that adding, moving or deleting a selected region can change the numbering.
      int remove_selection()(int selection_num) nothrow {
         return atk_text_remove_selection(cast(Text*)&this, selection_num);
      }

      // Sets the caret (cursor) position to the specified @offset.
      // RETURNS: %TRUE if success, %FALSE otherwise.
      // <offset>: position
      int set_caret_offset()(int offset) nothrow {
         return atk_text_set_caret_offset(cast(Text*)&this, offset);
      }

      // Changes the start and end offset of the specified selection.
      // RETURNS: %TRUE if success, %FALSE otherwise
      // <selection_num>: The selection number.  The selected regions are assigned numbers that correspond to how far the region is from the start of the text.  The selected region closest to the beginning of the text region is assigned the number 0, etc.  Note that adding, moving or deleting a selected region can change the numbering.
      // <start_offset>: the new start position of the selection
      // <end_offset>: the new end position of (e.g. offset immediately past) the selection
      int set_selection()(int selection_num, int start_offset, int end_offset) nothrow {
         return atk_text_set_selection(cast(Text*)&this, selection_num, start_offset, end_offset);
      }
      extern (C) alias static void function (Text* this_, void* user_data=null) nothrow signal_text_attributes_changed;

      ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
         return super_.signal_connect!name(cb, data, cf);
      }

      ulong signal_connect(string name:"text-attributes-changed", CB/*:signal_text_attributes_changed*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_text_attributes_changed)||_ttmm!(CB, signal_text_attributes_changed)()) {
         return signal_connect_data!()(&this, cast(char*)"text-attributes-changed",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Text* this_, int object, void* user_data=null) nothrow signal_text_caret_moved;
      ulong signal_connect(string name:"text-caret-moved", CB/*:signal_text_caret_moved*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_text_caret_moved)||_ttmm!(CB, signal_text_caret_moved)()) {
         return signal_connect_data!()(&this, cast(char*)"text-caret-moved",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Text* this_, int object, int p0, void* user_data=null) nothrow signal_text_changed;
      ulong signal_connect(string name:"text-changed", CB/*:signal_text_changed*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_text_changed)||_ttmm!(CB, signal_text_changed)()) {
         return signal_connect_data!()(&this, cast(char*)"text-changed",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Text* this_, int object, int p0, char* p1, void* user_data=null) nothrow signal_text_insert;
      ulong signal_connect(string name:"text-insert", CB/*:signal_text_insert*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_text_insert)||_ttmm!(CB, signal_text_insert)()) {
         return signal_connect_data!()(&this, cast(char*)"text-insert",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Text* this_, int object, int p0, char* p1, void* user_data=null) nothrow signal_text_remove;
      ulong signal_connect(string name:"text-remove", CB/*:signal_text_remove*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_text_remove)||_ttmm!(CB, signal_text_remove)()) {
         return signal_connect_data!()(&this, cast(char*)"text-remove",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Text* this_, void* user_data=null) nothrow signal_text_selection_changed;
      ulong signal_connect(string name:"text-selection-changed", CB/*:signal_text_selection_changed*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_text_selection_changed)||_ttmm!(CB, signal_text_selection_changed)()) {
         return signal_connect_data!()(&this, cast(char*)"text-selection-changed",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Text* this_, int object, int p0, int p1, char* p2, void* user_data=null) nothrow signal_text_update;
      ulong signal_connect(string name:"text-update", CB/*:signal_text_update*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_text_update)||_ttmm!(CB, signal_text_update)()) {
         return signal_connect_data!()(&this, cast(char*)"text-update",
         cast(GObject2.Callback)cb, data, null, cf);
      }
   }
   mixin __interface__;
}

enum TextAttribute {
   INVALID = 0,
   LEFT_MARGIN = 1,
   RIGHT_MARGIN = 2,
   INDENT = 3,
   INVISIBLE = 4,
   EDITABLE = 5,
   PIXELS_ABOVE_LINES = 6,
   PIXELS_BELOW_LINES = 7,
   PIXELS_INSIDE_WRAP = 8,
   BG_FULL_HEIGHT = 9,
   RISE = 10,
   UNDERLINE = 11,
   STRIKETHROUGH = 12,
   SIZE = 13,
   SCALE = 14,
   WEIGHT = 15,
   LANGUAGE = 16,
   FAMILY_NAME = 17,
   BG_COLOR = 18,
   FG_COLOR = 19,
   BG_STIPPLE = 20,
   FG_STIPPLE = 21,
   WRAP_MODE = 22,
   DIRECTION = 23,
   JUSTIFICATION = 24,
   STRETCH = 25,
   VARIANT = 26,
   STYLE = 27,
   LAST_DEFINED = 28
}
enum TextBoundary {
   CHAR = 0,
   WORD_START = 1,
   WORD_END = 2,
   SENTENCE_START = 3,
   SENTENCE_END = 4,
   LINE_START = 5,
   LINE_END = 6
}
enum TextClipType {
   NONE = 0,
   MIN = 1,
   MAX = 2,
   BOTH = 3
}
struct TextIface {
   GObject2.TypeInterface parent;

   // RETURNS: a newly allocated string containing the text from @start_offset up
   // <start_offset>: start position
   // <end_offset>: end position
   extern (C) char* /*new*/ function (Text* text, int start_offset, int end_offset) nothrow get_text;

   // RETURNS: a newly allocated string containing the text after @offset bounded
   // <offset>: position
   // <boundary_type>: An #AtkTextBoundary
   // <start_offset>: the start offset of the returned string
   // <end_offset>: the offset of the first character after the returned substring
   extern (C) char* /*new*/ function (Text* text, int offset, TextBoundary boundary_type, int* start_offset, int* end_offset) nothrow get_text_after_offset;

   // RETURNS: a newly allocated string containing the text at @offset bounded by
   // <offset>: position
   // <boundary_type>: An #AtkTextBoundary
   // <start_offset>: the start offset of the returned string
   // <end_offset>: the offset of the first character after the returned substring
   extern (C) char* /*new*/ function (Text* text, int offset, TextBoundary boundary_type, int* start_offset, int* end_offset) nothrow get_text_at_offset;

   // RETURNS: the character at @offset.
   // <offset>: position
   extern (C) dchar function (Text* text, int offset) nothrow get_character_at_offset;

   // RETURNS: a newly allocated string containing the text before @offset bounded
   // <offset>: position
   // <boundary_type>: An #AtkTextBoundary
   // <start_offset>: the start offset of the returned string
   // <end_offset>: the offset of the first character after the returned substring
   extern (C) char* /*new*/ function (Text* text, int offset, TextBoundary boundary_type, int* start_offset, int* end_offset) nothrow get_text_before_offset;
   // RETURNS: the offset position of the caret (cursor).
   extern (C) int function (Text* text) nothrow get_caret_offset;

   // RETURNS: an #AtkAttributeSet which contains the attributes
   // <offset>: the offset at which to get the attributes, -1 means the offset of the character to be inserted at the caret location.
   // <start_offset>: the address to put the start offset of the range
   // <end_offset>: the address to put the end offset of the range
   extern (C) AttributeSet* /*new*/ function (Text* text, int offset, int* start_offset, int* end_offset) nothrow get_run_attributes;
   // RETURNS: an #AtkAttributeSet which contains the default
   extern (C) AttributeSet* /*new*/ function (Text* text) nothrow get_default_attributes;

   // <offset>: The offset of the text character for which bounding information is required.
   // <x>: Pointer for the x cordinate of the bounding box
   // <y>: Pointer for the y cordinate of the bounding box
   // <width>: Pointer for the width of the bounding box
   // <height>: Pointer for the height of the bounding box
   // <coords>: specify whether coordinates are relative to the screen or widget window
   extern (C) void function (Text* text, int offset, int* x, int* y, int* width, int* height, CoordType coords) nothrow get_character_extents;
   // RETURNS: the number of characters.
   extern (C) int function (Text* text) nothrow get_character_count;

   // RETURNS: the offset to the character which is located at
   // <x>: screen x-position of character
   // <y>: screen y-position of character
   // <coords>: specify whether coordinates are relative to the screen or widget window
   extern (C) int function (Text* text, int x, int y, CoordType coords) nothrow get_offset_at_point;
   // RETURNS: The number of selected regions, or -1 if a failure
   extern (C) int function (Text* text) nothrow get_n_selections;

   // RETURNS: a newly allocated string containing the selected text. Use g_free()
   // <selection_num>: The selection number.  The selected regions are assigned numbers that correspond to how far the region is from the start of the text.  The selected region closest to the beginning of the text region is assigned the number 0, etc.  Note that adding, moving or deleting a selected region can change the numbering.
   // <start_offset>: passes back the start position of the selected region
   // <end_offset>: passes back the end position of (e.g. offset immediately past) the selected region
   extern (C) char* /*new*/ function (Text* text, int selection_num, int* start_offset, int* end_offset) nothrow get_selection;

   // RETURNS: %TRUE if success, %FALSE otherwise
   // <start_offset>: the start position of the selected region
   // <end_offset>: the offset of the first character after the selected region.
   extern (C) int function (Text* text, int start_offset, int end_offset) nothrow add_selection;

   // RETURNS: %TRUE if success, %FALSE otherwise
   // <selection_num>: The selection number.  The selected regions are assigned numbers that correspond to how far the region is from the start of the text.  The selected region closest to the beginning of the text region is assigned the number 0, etc.  Note that adding, moving or deleting a selected region can change the numbering.
   extern (C) int function (Text* text, int selection_num) nothrow remove_selection;

   // RETURNS: %TRUE if success, %FALSE otherwise
   // <selection_num>: The selection number.  The selected regions are assigned numbers that correspond to how far the region is from the start of the text.  The selected region closest to the beginning of the text region is assigned the number 0, etc.  Note that adding, moving or deleting a selected region can change the numbering.
   // <start_offset>: the new start position of the selection
   // <end_offset>: the new end position of (e.g. offset immediately past) the selection
   extern (C) int function (Text* text, int selection_num, int start_offset, int end_offset) nothrow set_selection;

   // RETURNS: %TRUE if success, %FALSE otherwise.
   // <offset>: position
   extern (C) int function (Text* text, int offset) nothrow set_caret_offset;
   extern (C) void function (Text* text, int position, int length) nothrow text_changed;
   extern (C) void function (Text* text, int location) nothrow text_caret_moved;
   extern (C) void function (Text* text) nothrow text_selection_changed;
   extern (C) void function (Text* text) nothrow text_attributes_changed;

   // <start_offset>: The offset of the first text character for which boundary information is required.
   // <end_offset>: The offset of the text character after the last character for which boundary information is required.
   // <coord_type>: Specify whether coordinates are relative to the screen or widget window.
   // <rect>: A pointer to a AtkTextRectangle which is filled in by this function.
   extern (C) void function (Text* text, int start_offset, int end_offset, CoordType coord_type, TextRectangle* rect) nothrow get_range_extents;
   extern (C) TextRange** /*new*/ function (Text* text, TextRectangle* rect, CoordType coord_type, TextClipType x_clip_type, TextClipType y_clip_type) nothrow get_bounded_ranges;
   Function pad4;
}

// A structure used to describe a text range.
struct TextRange {
   TextRectangle bounds;
   int start_offset, end_offset;
   char* content;
}

// A structure used to store a rectangle used by AtkText.
struct TextRectangle {
   int x, y, width, height;
}

struct Util /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
}

struct UtilClass {
   GObject2.ObjectClass parent;
   // Unintrospectable functionp: add_global_event_listener() / ()
   extern (C) uint function (GObject2.SignalEmissionHook listener, char* event_type) nothrow add_global_event_listener;
   extern (C) void function (uint listener_id) nothrow remove_global_event_listener;
   // Unintrospectable functionp: add_key_event_listener() / ()
   extern (C) uint function (KeySnoopFunc listener, void* data) nothrow add_key_event_listener;
   extern (C) void function (uint listener_id) nothrow remove_key_event_listener;
   // Unintrospectable functionp: get_root() / ()
   extern (C) Object* function () nothrow get_root;
   extern (C) char* function () nothrow get_toolkit_name;
   extern (C) char* function () nothrow get_toolkit_version;
}

struct Value /* Interface */ {
   mixin template __interface__() {
      // Gets the value of this object.
      // <value>: a #GValue representing the current accessible value
      void get_current_value(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
         atk_value_get_current_value(cast(Value*)&this, UpCast!(GObject2.Value*)(value));
      }

      // Gets the maximum value of this object.
      // <value>: a #GValue representing the maximum accessible value
      void get_maximum_value(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
         atk_value_get_maximum_value(cast(Value*)&this, UpCast!(GObject2.Value*)(value));
      }

      // VERSION: 1.12
      // Gets the minimum increment by which the value of this object may be changed.  If zero,
      // the minimum increment is undefined, which may mean that it is limited only by the 
      // floating point precision of the platform.
      // <value>: a #GValue representing the minimum increment by which the accessible value may be changed
      void get_minimum_increment(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
         atk_value_get_minimum_increment(cast(Value*)&this, UpCast!(GObject2.Value*)(value));
      }

      // Gets the minimum value of this object.
      // <value>: a #GValue representing the minimum accessible value
      void get_minimum_value(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
         atk_value_get_minimum_value(cast(Value*)&this, UpCast!(GObject2.Value*)(value));
      }

      // Sets the value of this object.
      // RETURNS: %TRUE if new value is successfully set, %FALSE otherwise.
      // <value>: a #GValue which is the desired new accessible value.
      int set_current_value(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
         return atk_value_set_current_value(cast(Value*)&this, UpCast!(GObject2.Value*)(value));
      }
   }
   mixin __interface__;
}

struct ValueIface {
   GObject2.TypeInterface parent;
   // <value>: a #GValue representing the current accessible value
   extern (C) void function (Value* obj, GObject2.Value* value) nothrow get_current_value;
   // <value>: a #GValue representing the maximum accessible value
   extern (C) void function (Value* obj, GObject2.Value* value) nothrow get_maximum_value;
   // <value>: a #GValue representing the minimum accessible value
   extern (C) void function (Value* obj, GObject2.Value* value) nothrow get_minimum_value;

   // RETURNS: %TRUE if new value is successfully set, %FALSE otherwise.
   // <value>: a #GValue which is the desired new accessible value.
   extern (C) int function (Value* obj, GObject2.Value* value) nothrow set_current_value;
   // <value>: a #GValue representing the minimum increment by which the accessible value may be changed
   extern (C) void function (Value* obj, GObject2.Value* value) nothrow get_minimum_increment;
   Function pad1;
}

struct Window /* Interface */ {
   mixin template __interface__() {      extern (C) alias static void function (Window* this_, void* user_data=null) nothrow signal_activate;

      ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
         return super_.signal_connect!name(cb, data, cf);
      }

      ulong signal_connect(string name:"activate", CB/*:signal_activate*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_activate)||_ttmm!(CB, signal_activate)()) {
         return signal_connect_data!()(&this, cast(char*)"activate",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Window* this_, void* user_data=null) nothrow signal_create;
      ulong signal_connect(string name:"create", CB/*:signal_create*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_create)||_ttmm!(CB, signal_create)()) {
         return signal_connect_data!()(&this, cast(char*)"create",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Window* this_, void* user_data=null) nothrow signal_deactivate;
      ulong signal_connect(string name:"deactivate", CB/*:signal_deactivate*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_deactivate)||_ttmm!(CB, signal_deactivate)()) {
         return signal_connect_data!()(&this, cast(char*)"deactivate",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Window* this_, void* user_data=null) nothrow signal_destroy;
      ulong signal_connect(string name:"destroy", CB/*:signal_destroy*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_destroy)||_ttmm!(CB, signal_destroy)()) {
         return signal_connect_data!()(&this, cast(char*)"destroy",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Window* this_, void* user_data=null) nothrow signal_maximize;
      ulong signal_connect(string name:"maximize", CB/*:signal_maximize*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_maximize)||_ttmm!(CB, signal_maximize)()) {
         return signal_connect_data!()(&this, cast(char*)"maximize",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Window* this_, void* user_data=null) nothrow signal_minimize;
      ulong signal_connect(string name:"minimize", CB/*:signal_minimize*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_minimize)||_ttmm!(CB, signal_minimize)()) {
         return signal_connect_data!()(&this, cast(char*)"minimize",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Window* this_, void* user_data=null) nothrow signal_move;
      ulong signal_connect(string name:"move", CB/*:signal_move*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_move)||_ttmm!(CB, signal_move)()) {
         return signal_connect_data!()(&this, cast(char*)"move",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Window* this_, void* user_data=null) nothrow signal_resize;
      ulong signal_connect(string name:"resize", CB/*:signal_resize*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_resize)||_ttmm!(CB, signal_resize)()) {
         return signal_connect_data!()(&this, cast(char*)"resize",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Window* this_, void* user_data=null) nothrow signal_restore;
      ulong signal_connect(string name:"restore", CB/*:signal_restore*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_restore)||_ttmm!(CB, signal_restore)()) {
         return signal_connect_data!()(&this, cast(char*)"restore",
         cast(GObject2.Callback)cb, data, null, cf);
      }
   }
   mixin __interface__;
}

struct WindowIface {
   GObject2.TypeInterface parent;
   void*[16] _padding_dummy;
}

struct _PropertyValues {
   char* property_name;
   GObject2.Value old_value, new_value;
}

struct _Registry {
   GObject2.Object parent;
   GLib2.HashTable* factory_type_registry, factory_singleton_cache;
}

struct _RegistryClass {
   GObject2.ObjectClass parent_class;
}


// Unintrospectable function: add_focus_tracker() / atk_add_focus_tracker()
// Adds the specified function to the list of functions to be called
// when an object receives focus.
// RETURNS: added focus tracker id, or 0 on failure.
// <focus_tracker>: Function to be added to the list of functions to be called when an object receives focus.
static uint add_focus_tracker()(EventListener focus_tracker) nothrow {
   return atk_add_focus_tracker(focus_tracker);
}


// Unintrospectable function: add_global_event_listener() / atk_add_global_event_listener()
// Adds the specified function to the list of functions to be called
// when an ATK event of type event_type occurs.
// The format of event_type is the following:
// "ATK:<atk_type>:<atk_event>
// Where "ATK" works as the namespace, <atk_interface> is the name of
// the ATK type (interface or object) and <atk_event> is the name of
// the signal defined on that interface.
// For example:
// ATK:AtkObject:state-change
// ATK:AtkText:text-selection-changed
// RETURNS: added event listener id, or 0 on failure.
// <listener>: the listener to notify
// <event_type>: the type of event for which notification is requested
static uint add_global_event_listener(AT0)(GObject2.SignalEmissionHook listener, AT0 /*char*/ event_type) nothrow {
   return atk_add_global_event_listener(listener, toCString!(char*)(event_type));
}


// Unintrospectable function: add_key_event_listener() / atk_add_key_event_listener()
// Adds the specified function to the list of functions to be called
// when a key event occurs.  The @data element will be passed to the
// #AtkKeySnoopFunc (@listener) as the @func_data param, on notification.
// RETURNS: added event listener id, or 0 on failure.
// <listener>: the listener to notify
// <data>: a #gpointer that points to a block of data that should be sent to the registered listeners, along with the event notification, when it occurs.
static uint add_key_event_listener(AT0)(KeySnoopFunc listener, AT0 /*void*/ data) nothrow {
   return atk_add_key_event_listener(listener, UpCast!(void*)(data));
}


// Frees the memory used by an #AtkAttributeSet, including all its
// #AtkAttributes.
// <attrib_set>: The #AtkAttributeSet to free
static void attribute_set_free(AT0)(AT0 /*AttributeSet*/ attrib_set) nothrow {
   atk_attribute_set_free(UpCast!(AttributeSet*)(attrib_set));
}


// Unintrospectable function: focus_tracker_init() / atk_focus_tracker_init()
// Specifies the function to be called for focus tracker initialization.
// This function should be called by an implementation of the
// ATK interface if any specific work needs to be done to enable
// focus tracking.
// <init>: Function to be called for focus tracker initialization
static void focus_tracker_init()(EventListenerInit init) nothrow {
   atk_focus_tracker_init(init);
}


// Cause the focus tracker functions which have been specified to be
// executed for the object.
// <object>: an #AtkObject
static void focus_tracker_notify(AT0)(AT0 /*Object*/ object) nothrow {
   atk_focus_tracker_notify(UpCast!(Object*)(object));
}


// Gets a default implementation of the #AtkObjectFactory/type
// registry.
// registry for registering new #AtkObject factories. Following
// a call to this function, maintainers may call atk_registry_set_factory_type()
// to associate an #AtkObjectFactory subclass with the GType of objects
// for whom accessibility information will be provided.
// #AtkObjectFactory/type registry
// RETURNS: a default implementation of the
static Registry* /*new*/ get_default_registry()() nothrow {
   return atk_get_default_registry();
}


// VERSION: 1.6
// Gets the currently focused object.
// application
// RETURNS: the currently focused object for the current
static Object* get_focus_object()() nothrow {
   return atk_get_focus_object();
}


// Gets the root accessible container for the current application.
// application
// RETURNS: the root accessible container for the current
static Object* get_root()() nothrow {
   return atk_get_root();
}


// Gets name string for the GUI toolkit implementing ATK for this application.
// RETURNS: name string for the GUI toolkit implementing ATK for this application
static char* get_toolkit_name()() nothrow {
   return atk_get_toolkit_name();
}


// Gets version string for the GUI toolkit implementing ATK for this application.
// RETURNS: version string for the GUI toolkit implementing ATK for this application
static char* get_toolkit_version()() nothrow {
   return atk_get_toolkit_version();
}


// VERSION: 1.20
// Gets the current version for ATK.
// RETURNS: version string for ATK
static char* get_version()() nothrow {
   return atk_get_version();
}


// Get the #AtkRelationType type corresponding to a relation name.
// or #ATK_RELATION_NULL if no matching relation type is found.
// RETURNS: the #AtkRelationType enumerated type corresponding to the specified name,
// <name>: a string which is the (non-localized) name of an ATK relation type.
static RelationType relation_type_for_name(AT0)(AT0 /*char*/ name) nothrow {
   return atk_relation_type_for_name(toCString!(char*)(name));
}


// Gets the description string describing the #AtkRelationType @type.
// RETURNS: the string describing the AtkRelationType
// <type>: The #AtkRelationType whose name is required
static char* relation_type_get_name()(RelationType type) nothrow {
   return atk_relation_type_get_name(type);
}


// Associate @name with a new #AtkRelationType
// RETURNS: an #AtkRelationType associated with @name
// <name>: a name string
static RelationType relation_type_register(AT0)(AT0 /*char*/ name) nothrow {
   return atk_relation_type_register(toCString!(char*)(name));
}


// Removes the specified focus tracker from the list of functions
// to be called when any object receives focus.
// <tracker_id>: the id of the focus tracker to remove
static void remove_focus_tracker()(uint tracker_id) nothrow {
   atk_remove_focus_tracker(tracker_id);
}


// Removes the specified event listener
// <listener_id>: the id of the event listener to remove
static void remove_global_event_listener()(uint listener_id) nothrow {
   atk_remove_global_event_listener(listener_id);
}


// Removes the specified event listener
// <listener_id>: the id of the event listener to remove
static void remove_key_event_listener()(uint listener_id) nothrow {
   atk_remove_key_event_listener(listener_id);
}


// Get the #AtkRole type corresponding to a rolew name.
// or #ATK_ROLE_INVALID if no matching role is found.
// RETURNS: the #AtkRole enumerated type corresponding to the specified
// <name>: a string which is the (non-localized) name of an ATK role.
static Role role_for_name(AT0)(AT0 /*char*/ name) nothrow {
   return atk_role_for_name(toCString!(char*)(name));
}


// Gets the localized description string describing the #AtkRole @role.
// RETURNS: the localized string describing the AtkRole
// <role>: The #AtkRole whose localized name is required
static char* role_get_localized_name()(Role role) nothrow {
   return atk_role_get_localized_name(role);
}


// Gets the description string describing the #AtkRole @role.
// RETURNS: the string describing the AtkRole
// <role>: The #AtkRole whose name is required
static char* role_get_name()(Role role) nothrow {
   return atk_role_get_name(role);
}


// Registers the role specified by @name.
// RETURNS: an #AtkRole for the new role.
// <name>: a character string describing the new role.
static Role role_register(AT0)(AT0 /*char*/ name) nothrow {
   return atk_role_register(toCString!(char*)(name));
}


// Gets the #AtkStateType corresponding to the description string @name.
// RETURNS: an #AtkStateType corresponding to @name
// <name>: a character string state name
static StateType state_type_for_name(AT0)(AT0 /*char*/ name) nothrow {
   return atk_state_type_for_name(toCString!(char*)(name));
}


// Gets the description string describing the #AtkStateType @type.
// RETURNS: the string describing the AtkStateType
// <type>: The #AtkStateType whose name is required
static char* state_type_get_name()(StateType type) nothrow {
   return atk_state_type_get_name(type);
}


// Register a new object state.
// RETURNS: an #AtkState value for the new state.
// <name>: a character string describing the new state.
static StateType state_type_register(AT0)(AT0 /*char*/ name) nothrow {
   return atk_state_type_register(toCString!(char*)(name));
}


// Get the #AtkTextAttribute type corresponding to a text attribute name.
// or #ATK_TEXT_ATTRIBUTE_INVALID if no matching text attribute is found.
// RETURNS: the #AtkTextAttribute enumerated type corresponding to the specified
// <name>: a string which is the (non-localized) name of an ATK text attribute.
static TextAttribute text_attribute_for_name(AT0)(AT0 /*char*/ name) nothrow {
   return atk_text_attribute_for_name(toCString!(char*)(name));
}


// Gets the name corresponding to the #AtkTextAttribute
// RETURNS: a string containing the name; this string should not be freed
// <attr>: The #AtkTextAttribute whose name is required
static char* text_attribute_get_name()(TextAttribute attr) nothrow {
   return atk_text_attribute_get_name(attr);
}


// Gets the value for the index of the #AtkTextAttribute
// NULL is returned if there are no values maintained for the attr value.
// RETURNS: a string containing the value; this string should not be freed;
// <attr>: The #AtkTextAttribute for which a value is required
// <index_>: The index of the required value
static char* text_attribute_get_value()(TextAttribute attr, int index_) nothrow {
   return atk_text_attribute_get_value(attr, index_);
}


// Associate @name with a new #AtkTextAttribute
// RETURNS: an #AtkTextAttribute associated with @name
// <name>: a name string
static TextAttribute text_attribute_register(AT0)(AT0 /*char*/ name) nothrow {
   return atk_text_attribute_register(toCString!(char*)(name));
}


// VERSION: 1.3
// Frees the memory associated with an array of AtkTextRange. It is assumed
// that the array was returned by the function atk_text_get_bounded_ranges
// and is NULL terminated.
// <ranges>: A pointer to an array of  #AtkTextRange which is to be freed.
static void text_free_ranges(AT0)(AT0 /*TextRange**/ ranges) nothrow {
   atk_text_free_ranges(UpCast!(TextRange**)(ranges));
}


// C prototypes:

extern (C) {
int atk_action_do_action(Action* this_, int i) nothrow;
char* atk_action_get_description(Action* this_, int i) nothrow;
char* atk_action_get_keybinding(Action* this_, int i) nothrow;
char* atk_action_get_localized_name(Action* this_, int i) nothrow;
int atk_action_get_n_actions(Action* this_) nothrow;
char* atk_action_get_name(Action* this_, int i) nothrow;
int atk_action_set_description(Action* this_, int i, char* desc) nothrow;
uint atk_component_add_focus_handler(Component* this_, FocusHandler handler) nothrow;
int atk_component_contains(Component* this_, int x, int y, CoordType coord_type) nothrow;
double atk_component_get_alpha(Component* this_) nothrow;
void atk_component_get_extents(Component* this_, int* x, int* y, int* width, int* height, CoordType coord_type) nothrow;
Layer atk_component_get_layer(Component* this_) nothrow;
int atk_component_get_mdi_zorder(Component* this_) nothrow;
void atk_component_get_position(Component* this_, int* x, int* y, CoordType coord_type) nothrow;
void atk_component_get_size(Component* this_, int* width, int* height) nothrow;
int atk_component_grab_focus(Component* this_) nothrow;
Object* /*new*/ atk_component_ref_accessible_at_point(Component* this_, int x, int y, CoordType coord_type) nothrow;
void atk_component_remove_focus_handler(Component* this_, uint handler_id) nothrow;
int atk_component_set_extents(Component* this_, int x, int y, int width, int height, CoordType coord_type) nothrow;
int atk_component_set_position(Component* this_, int x, int y, CoordType coord_type) nothrow;
int atk_component_set_size(Component* this_, int width, int height) nothrow;
char* atk_document_get_attribute_value(Document* this_, char* attribute_name) nothrow;
AttributeSet* atk_document_get_attributes(Document* this_) nothrow;
void* atk_document_get_document(Document* this_) nothrow;
char* atk_document_get_document_type(Document* this_) nothrow;
char* atk_document_get_locale(Document* this_) nothrow;
int atk_document_set_attribute_value(Document* this_, char* attribute_name, char* attribute_value) nothrow;
void atk_editable_text_copy_text(EditableText* this_, int start_pos, int end_pos) nothrow;
void atk_editable_text_cut_text(EditableText* this_, int start_pos, int end_pos) nothrow;
void atk_editable_text_delete_text(EditableText* this_, int start_pos, int end_pos) nothrow;
void atk_editable_text_insert_text(EditableText* this_, char* string_, int length, int* position) nothrow;
void atk_editable_text_paste_text(EditableText* this_, int position) nothrow;
int atk_editable_text_set_run_attributes(EditableText* this_, AttributeSet* attrib_set, int start_offset, int end_offset) nothrow;
void atk_editable_text_set_text_contents(EditableText* this_, char* string_) nothrow;
Object* atk_gobject_accessible_for_object(GObject2.Object* obj) nothrow;
GObject2.Object* atk_gobject_accessible_get_object(GObjectAccessible* this_) nothrow;
int atk_hyperlink_get_end_index(Hyperlink* this_) nothrow;
int atk_hyperlink_get_n_anchors(Hyperlink* this_) nothrow;
Object* atk_hyperlink_get_object(Hyperlink* this_, int i) nothrow;
int atk_hyperlink_get_start_index(Hyperlink* this_) nothrow;
char* /*new*/ atk_hyperlink_get_uri(Hyperlink* this_, int i) nothrow;
int atk_hyperlink_is_inline(Hyperlink* this_) nothrow;
int atk_hyperlink_is_valid(Hyperlink* this_) nothrow;
Hyperlink* /*new*/ atk_hyperlink_impl_get_hyperlink(HyperlinkImpl* this_) nothrow;
Hyperlink* atk_hypertext_get_link(Hypertext* this_, int link_index) nothrow;
int atk_hypertext_get_link_index(Hypertext* this_, int char_index) nothrow;
int atk_hypertext_get_n_links(Hypertext* this_) nothrow;
char* atk_image_get_image_description(Image* this_) nothrow;
char* atk_image_get_image_locale(Image* this_) nothrow;
void atk_image_get_image_position(Image* this_, int* x, int* y, CoordType coord_type) nothrow;
void atk_image_get_image_size(Image* this_, int* width, int* height) nothrow;
int atk_image_set_image_description(Image* this_, char* description) nothrow;
Object* /*new*/ atk_implementor_ref_accessible(Implementor* this_) nothrow;
Misc* atk_misc_get_instance() nothrow;
void atk_misc_threads_enter(Misc* this_) nothrow;
void atk_misc_threads_leave(Misc* this_) nothrow;
NoOpObject* /*new*/ atk_no_op_object_new(GObject2.Object* obj) nothrow;
NoOpObjectFactory* /*new*/ atk_no_op_object_factory_new() nothrow;
int atk_object_add_relationship(Object* this_, RelationType relationship, Object* target) nothrow;
uint atk_object_connect_property_change_handler(Object* this_, PropertyChangeHandler* handler) nothrow;
AttributeSet* atk_object_get_attributes(Object* this_) nothrow;
char* atk_object_get_description(Object* this_) nothrow;
int atk_object_get_index_in_parent(Object* this_) nothrow;
int atk_object_get_n_accessible_children(Object* this_) nothrow;
char* atk_object_get_name(Object* this_) nothrow;
Object* atk_object_get_parent(Object* this_) nothrow;
Role atk_object_get_role(Object* this_) nothrow;
void atk_object_initialize(Object* this_, void* data) nothrow;
void atk_object_notify_state_change(Object* this_, State state, int value) nothrow;
Object* /*new*/ atk_object_ref_accessible_child(Object* this_, int i) nothrow;
RelationSet* /*new*/ atk_object_ref_relation_set(Object* this_) nothrow;
StateSet* /*new*/ atk_object_ref_state_set(Object* this_) nothrow;
void atk_object_remove_property_change_handler(Object* this_, uint handler_id) nothrow;
int atk_object_remove_relationship(Object* this_, RelationType relationship, Object* target) nothrow;
void atk_object_set_description(Object* this_, char* description) nothrow;
void atk_object_set_name(Object* this_, char* name) nothrow;
void atk_object_set_parent(Object* this_, Object* parent) nothrow;
void atk_object_set_role(Object* this_, Role role) nothrow;
Object* /*new*/ atk_object_factory_create_accessible(ObjectFactory* this_, GObject2.Object* obj) nothrow;
Type atk_object_factory_get_accessible_type(ObjectFactory* this_) nothrow;
void atk_object_factory_invalidate(ObjectFactory* this_) nothrow;
Plug* /*new*/ atk_plug_new() nothrow;
char* /*new*/ atk_plug_get_id(Plug* this_) nothrow;
ObjectFactory* atk_registry_get_factory(Registry* this_, Type type) nothrow;
Type atk_registry_get_factory_type(Registry* this_, Type type) nothrow;
void atk_registry_set_factory_type(Registry* this_, Type type, Type factory_type) nothrow;
Relation* /*new*/ atk_relation_new(Object** targets, int n_targets, RelationType relationship) nothrow;
void atk_relation_add_target(Relation* this_, Object* target) nothrow;
RelationType atk_relation_get_relation_type(Relation* this_) nothrow;
PtrArray* atk_relation_get_target(Relation* this_) nothrow;
int atk_relation_remove_target(Relation* this_, Object* target) nothrow;
RelationSet* /*new*/ atk_relation_set_new() nothrow;
void atk_relation_set_add(RelationSet* this_, Relation* relation) nothrow;
void atk_relation_set_add_relation_by_type(RelationSet* this_, RelationType relationship, Object* target) nothrow;
int atk_relation_set_contains(RelationSet* this_, RelationType relationship) nothrow;
int atk_relation_set_get_n_relations(RelationSet* this_) nothrow;
Relation* atk_relation_set_get_relation(RelationSet* this_, int i) nothrow;
Relation* atk_relation_set_get_relation_by_type(RelationSet* this_, RelationType relationship) nothrow;
void atk_relation_set_remove(RelationSet* this_, Relation* relation) nothrow;
int atk_selection_add_selection(Selection* this_, int i) nothrow;
int atk_selection_clear_selection(Selection* this_) nothrow;
int atk_selection_get_selection_count(Selection* this_) nothrow;
int atk_selection_is_child_selected(Selection* this_, int i) nothrow;
Object* /*new*/ atk_selection_ref_selection(Selection* this_, int i) nothrow;
int atk_selection_remove_selection(Selection* this_, int i) nothrow;
int atk_selection_select_all_selection(Selection* this_) nothrow;
Socket* /*new*/ atk_socket_new() nothrow;
void atk_socket_embed(Socket* this_, char* plug_id) nothrow;
int atk_socket_is_occupied(Socket* this_) nothrow;
StateSet* /*new*/ atk_state_set_new() nothrow;
int atk_state_set_add_state(StateSet* this_, StateType type) nothrow;
void atk_state_set_add_states(StateSet* this_, StateType* types, int n_types) nothrow;
StateSet* /*new*/ atk_state_set_and_sets(StateSet* this_, StateSet* compare_set) nothrow;
void atk_state_set_clear_states(StateSet* this_) nothrow;
int atk_state_set_contains_state(StateSet* this_, StateType type) nothrow;
int atk_state_set_contains_states(StateSet* this_, StateType* types, int n_types) nothrow;
int atk_state_set_is_empty(StateSet* this_) nothrow;
StateSet* /*new*/ atk_state_set_or_sets(StateSet* this_, StateSet* compare_set) nothrow;
int atk_state_set_remove_state(StateSet* this_, StateType type) nothrow;
StateSet* /*new*/ atk_state_set_xor_sets(StateSet* this_, StateSet* compare_set) nothrow;
char* atk_streamable_content_get_mime_type(StreamableContent* this_, int i) nothrow;
int atk_streamable_content_get_n_mime_types(StreamableContent* this_) nothrow;
GLib2.IOChannel* /*new*/ atk_streamable_content_get_stream(StreamableContent* this_, char* mime_type) nothrow;
char* atk_streamable_content_get_uri(StreamableContent* this_, char* mime_type) nothrow;
int atk_table_add_column_selection(Table* this_, int column) nothrow;
int atk_table_add_row_selection(Table* this_, int row) nothrow;
Object* atk_table_get_caption(Table* this_) nothrow;
int atk_table_get_column_at_index(Table* this_, int index_) nothrow;
char* atk_table_get_column_description(Table* this_, int column) nothrow;
int atk_table_get_column_extent_at(Table* this_, int row, int column) nothrow;
Object* atk_table_get_column_header(Table* this_, int column) nothrow;
int atk_table_get_index_at(Table* this_, int row, int column) nothrow;
int atk_table_get_n_columns(Table* this_) nothrow;
int atk_table_get_n_rows(Table* this_) nothrow;
int atk_table_get_row_at_index(Table* this_, int index_) nothrow;
char* atk_table_get_row_description(Table* this_, int row) nothrow;
int atk_table_get_row_extent_at(Table* this_, int row, int column) nothrow;
Object* atk_table_get_row_header(Table* this_, int row) nothrow;
int atk_table_get_selected_columns(Table* this_, int** selected) nothrow;
int atk_table_get_selected_rows(Table* this_, int** selected) nothrow;
Object* /*new*/ atk_table_get_summary(Table* this_) nothrow;
int atk_table_is_column_selected(Table* this_, int column) nothrow;
int atk_table_is_row_selected(Table* this_, int row) nothrow;
int atk_table_is_selected(Table* this_, int row, int column) nothrow;
Object* /*new*/ atk_table_ref_at(Table* this_, int row, int column) nothrow;
int atk_table_remove_column_selection(Table* this_, int column) nothrow;
int atk_table_remove_row_selection(Table* this_, int row) nothrow;
void atk_table_set_caption(Table* this_, Object* caption) nothrow;
void atk_table_set_column_description(Table* this_, int column, char* description) nothrow;
void atk_table_set_column_header(Table* this_, int column, Object* header) nothrow;
void atk_table_set_row_description(Table* this_, int row, char* description) nothrow;
void atk_table_set_row_header(Table* this_, int row, Object* header) nothrow;
void atk_table_set_summary(Table* this_, Object* accessible) nothrow;
int atk_text_add_selection(Text* this_, int start_offset, int end_offset) nothrow;
TextRange** /*new*/ atk_text_get_bounded_ranges(Text* this_, TextRectangle* rect, CoordType coord_type, TextClipType x_clip_type, TextClipType y_clip_type) nothrow;
int atk_text_get_caret_offset(Text* this_) nothrow;
dchar atk_text_get_character_at_offset(Text* this_, int offset) nothrow;
int atk_text_get_character_count(Text* this_) nothrow;
void atk_text_get_character_extents(Text* this_, int offset, int* x, int* y, int* width, int* height, CoordType coords) nothrow;
AttributeSet* /*new*/ atk_text_get_default_attributes(Text* this_) nothrow;
int atk_text_get_n_selections(Text* this_) nothrow;
int atk_text_get_offset_at_point(Text* this_, int x, int y, CoordType coords) nothrow;
void atk_text_get_range_extents(Text* this_, int start_offset, int end_offset, CoordType coord_type, TextRectangle* rect) nothrow;
AttributeSet* /*new*/ atk_text_get_run_attributes(Text* this_, int offset, int* start_offset, int* end_offset) nothrow;
char* /*new*/ atk_text_get_selection(Text* this_, int selection_num, int* start_offset, int* end_offset) nothrow;
char* /*new*/ atk_text_get_text(Text* this_, int start_offset, int end_offset) nothrow;
char* /*new*/ atk_text_get_text_after_offset(Text* this_, int offset, TextBoundary boundary_type, int* start_offset, int* end_offset) nothrow;
char* /*new*/ atk_text_get_text_at_offset(Text* this_, int offset, TextBoundary boundary_type, int* start_offset, int* end_offset) nothrow;
char* /*new*/ atk_text_get_text_before_offset(Text* this_, int offset, TextBoundary boundary_type, int* start_offset, int* end_offset) nothrow;
int atk_text_remove_selection(Text* this_, int selection_num) nothrow;
int atk_text_set_caret_offset(Text* this_, int offset) nothrow;
int atk_text_set_selection(Text* this_, int selection_num, int start_offset, int end_offset) nothrow;
void atk_value_get_current_value(Value* this_, GObject2.Value* value) nothrow;
void atk_value_get_maximum_value(Value* this_, GObject2.Value* value) nothrow;
void atk_value_get_minimum_increment(Value* this_, GObject2.Value* value) nothrow;
void atk_value_get_minimum_value(Value* this_, GObject2.Value* value) nothrow;
int atk_value_set_current_value(Value* this_, GObject2.Value* value) nothrow;
uint atk_add_focus_tracker(EventListener focus_tracker) nothrow;
uint atk_add_global_event_listener(GObject2.SignalEmissionHook listener, char* event_type) nothrow;
uint atk_add_key_event_listener(KeySnoopFunc listener, void* data) nothrow;
void atk_attribute_set_free(AttributeSet* attrib_set) nothrow;
void atk_focus_tracker_init(EventListenerInit init) nothrow;
void atk_focus_tracker_notify(Object* object) nothrow;
Registry* /*new*/ atk_get_default_registry() nothrow;
Object* atk_get_focus_object() nothrow;
Object* atk_get_root() nothrow;
char* atk_get_toolkit_name() nothrow;
char* atk_get_toolkit_version() nothrow;
char* atk_get_version() nothrow;
RelationType atk_relation_type_for_name(char* name) nothrow;
char* atk_relation_type_get_name(RelationType type) nothrow;
RelationType atk_relation_type_register(char* name) nothrow;
void atk_remove_focus_tracker(uint tracker_id) nothrow;
void atk_remove_global_event_listener(uint listener_id) nothrow;
void atk_remove_key_event_listener(uint listener_id) nothrow;
Role atk_role_for_name(char* name) nothrow;
char* atk_role_get_localized_name(Role role) nothrow;
char* atk_role_get_name(Role role) nothrow;
Role atk_role_register(char* name) nothrow;
StateType atk_state_type_for_name(char* name) nothrow;
char* atk_state_type_get_name(StateType type) nothrow;
StateType atk_state_type_register(char* name) nothrow;
TextAttribute atk_text_attribute_for_name(char* name) nothrow;
char* atk_text_attribute_get_name(TextAttribute attr) nothrow;
char* atk_text_attribute_get_value(TextAttribute attr, int index_) nothrow;
TextAttribute atk_text_attribute_register(char* name) nothrow;
void atk_text_free_ranges(TextRange** ranges) nothrow;
}

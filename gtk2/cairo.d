// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/cairo-1.0.gir"


// package: "cairo-gobject";

// c:symbol-prefixes: ["cairo"]
// c:identifier-prefixes: ["cairo"]

// module cairo;

struct Context {
}

struct Surface {
}

struct Matrix {
}

struct Pattern {
}

struct Region {
}

enum Content {
   COLOR = 4096,
   ALPHA = 8192,
   COLOR_ALPHA = 12288
}
struct FontOptions {
}

struct FontType {
}

struct FontFace {
}

struct ScaledFont {
}

struct Path {
}

struct RectangleInt {
   int x, y, width, height;
}

static void image_surface_create()() nothrow {
   cairo_image_surface_create();
}


// C prototypes:

extern (C) {
void cairo_image_surface_create() nothrow;
}

// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/Cally-1.0.gir"

module Cally;
public import gtk2.atk;
alias gtk2.atk Atk;
public import gtk2.clutter;
alias gtk2.clutter Clutter;
public import gtk2.cogl;
alias gtk2.cogl Cogl;
public import gtk2.coglpango;
alias gtk2.coglpango CoglPango;
public import gtk2.gl;
alias gtk2.gl GL;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;
public import gtk2.gio2;
alias gtk2.gio2 Gio2;
public import gtk2.json;
alias gtk2.json Json;
public import gtk2.pango;
alias gtk2.pango Pango;
public import gtk2.pangocairo;
alias gtk2.pangocairo PangoCairo;
public import gtk2.pangoft2;
alias gtk2.pangoft2 PangoFT2;
public import gtk2.cairo;
alias gtk2.cairo cairo;
public import gtk2.fontconfig2;
alias gtk2.fontconfig2 fontconfig2;
public import gtk2.freetype2;
alias gtk2.freetype2 freetype2;

// package: "cally-1.0";
// C header: "cally/cally.h";

// c:symbol-prefixes: ["cally"]
// c:identifier-prefixes: ["Cally"]

// module Cally;


// VERSION: 1.6
// Action function, to be used on #AtkAction implementations as
// an individual action. Unlike #CallyActionFunc, this function
// uses the @user_data argument passed to cally_actor_add_action_full().
// <cally_actor>: a #CallyActor
// <user_data>: user data passed to the function
extern (C) alias void function (Actor* cally_actor, void* user_data) nothrow ActionCallback;


// VERSION: 1.4
// Action function, to be used on #AtkAction implementations as a individual
// action
// <cally_actor>: a #CallyActor
extern (C) alias void function (Actor* cally_actor) nothrow ActionFunc;


// The <structname>CallyActor</structname> structure contains only private
// data and should be accessed using the provided API
struct Actor /* : Atk.GObjectAccessible */ /* Version 1.4 */ {
   mixin Atk.Action.__interface__;
   mixin Atk.Component.__interface__;
   alias parent this;
   alias parent super_;
   alias parent gobjectaccessible;
   Atk.GObjectAccessible parent;
   private ActorPrivate* priv;


   // VERSION: 1.4
   // Creates a new #CallyActor for the given @actor
   // RETURNS: the newly created #AtkObject
   // <actor>: a #ClutterActor
   static Actor* /*new*/ new_(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
      return cally_actor_new(UpCast!(Clutter.Actor*)(actor));
   }
   static auto opCall(AT0)(AT0 /*Clutter.Actor*/ actor) {
      return cally_actor_new(UpCast!(Clutter.Actor*)(actor));
   }

   // Unintrospectable method: add_action() / cally_actor_add_action()
   // VERSION: 1.4
   // Adds a new action to be accessed with the #AtkAction interface.
   // RETURNS: added action id, or -1 if failure
   // <action_name>: the action name
   // <action_description>: the action description
   // <action_keybinding>: the action keybinding
   // <action_func>: the callback of the action, to be executed with do_action
   uint add_action(AT0, AT1, AT2)(AT0 /*char*/ action_name, AT1 /*char*/ action_description, AT2 /*char*/ action_keybinding, ActionFunc action_func) nothrow {
      return cally_actor_add_action(&this, toCString!(char*)(action_name), toCString!(char*)(action_description), toCString!(char*)(action_keybinding), action_func);
   }

   // VERSION: 1.6
   // Adds a new action to be accessed with the #AtkAction interface.
   // RETURNS: added action id, or -1 if failure
   // <action_name>: the action name
   // <action_description>: the action description
   // <action_keybinding>: the action keybinding
   // <callback>: the callback of the action
   // <user_data>: data to be passed to @callback
   // <notify>: function to be called when removing the action
   uint add_action_full(AT0, AT1, AT2, AT3)(AT0 /*char*/ action_name, AT1 /*char*/ action_description, AT2 /*char*/ action_keybinding, ActionCallback callback, AT3 /*void*/ user_data, GLib2.DestroyNotify notify) nothrow {
      return cally_actor_add_action_full(&this, toCString!(char*)(action_name), toCString!(char*)(action_description), toCString!(char*)(action_keybinding), callback, UpCast!(void*)(user_data), notify);
   }

   // VERSION: 1.4
   // Removes a action, using the @action_id returned by cally_actor_add_action()
   // RETURNS: %TRUE if the operation was succesful, %FALSE otherwise
   // <action_id>: the action id
   int remove_action()(int action_id) nothrow {
      return cally_actor_remove_action(&this, action_id);
   }

   // VERSION: 1.4
   // Removes an action, using the @action_name used when the action was added
   // with cally_actor_add_action()
   // RETURNS: %TRUE if the operation was succesful, %FALSE otherwise
   // <action_name>: the name of the action to remove
   int remove_action_by_name(AT0)(AT0 /*char*/ action_name) nothrow {
      return cally_actor_remove_action_by_name(&this, toCString!(char*)(action_name));
   }
}


// The <structname>CallyActorClass</structname> structure contains only
// private data
struct ActorClass /* Version 1.4 */ {
   private Atk.GObjectAccessibleClass parent_class;
   extern (C) void function (GObject2.Object* object, GObject2.ParamSpec* pspec) nothrow notify_clutter;
   extern (C) int function (Clutter.Actor* actor, void* data) nothrow focus_clutter;
   extern (C) int function (Clutter.Actor* container, Clutter.Actor* actor, void* data) nothrow add_actor;
   extern (C) int function (Clutter.Actor* container, Clutter.Actor* actor, void* data) nothrow remove_actor;
   private void*[32] _padding_dummy;
}

struct ActorPrivate {
}


// The <structname>CallyClone</structname> structure contains only private
// data and should be accessed using the provided API
struct Clone /* : Actor */ /* Version 1.4 */ {
   mixin Atk.Action.__interface__;
   mixin Atk.Component.__interface__;
   alias parent this;
   alias parent super_;
   alias parent actor;
   Actor parent;
   private ClonePrivate* priv;


   // VERSION: 1.4
   // Creates a new #CallyClone for the given @actor. @actor must be a
   // #ClutterClone.
   // RETURNS: the newly created #AtkObject
   // <actor>: a #ClutterActor
   static Clone* /*new*/ new_(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
      return cally_clone_new(UpCast!(Clutter.Actor*)(actor));
   }
   static auto opCall(AT0)(AT0 /*Clutter.Actor*/ actor) {
      return cally_clone_new(UpCast!(Clutter.Actor*)(actor));
   }
}


// The <structname>CallyCloneClass</structname> structure contains only
// private data
struct CloneClass /* Version 1.4 */ {
   private ActorClass parent_class;
   private void*[8] _padding_dummy;
}

struct ClonePrivate {
}


// The <structname>CallyGroup</structname> structure contains only
// private data and should be accessed using the provided API
struct Group /* : Actor */ /* Version 1.4 */ {
   mixin Atk.Action.__interface__;
   mixin Atk.Component.__interface__;
   alias parent this;
   alias parent super_;
   alias parent actor;
   Actor parent;
   private GroupPrivate* priv;


   // VERSION: 1.4
   // Creates a #CallyGroup for @actor
   // RETURNS: the newly created #CallyGroup
   // <actor>: a #ClutterGroup
   static Group* /*new*/ new_(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
      return cally_group_new(UpCast!(Clutter.Actor*)(actor));
   }
   static auto opCall(AT0)(AT0 /*Clutter.Actor*/ actor) {
      return cally_group_new(UpCast!(Clutter.Actor*)(actor));
   }
}


// The <structname>CallyGroupClass</structname> structure contains only
// private data
struct GroupClass /* Version 1.4 */ {
   private ActorClass parent_class;
   private void*[8] _padding_dummy;
}

struct GroupPrivate {
}


// The <structname>CallyRectangle</structname> structure contains only private
// data and should be accessed using the provided API
struct Rectangle /* : Actor */ /* Version 1.4 */ {
   mixin Atk.Action.__interface__;
   mixin Atk.Component.__interface__;
   alias parent this;
   alias parent super_;
   alias parent actor;
   Actor parent;
   private RectanglePrivate* priv;


   // VERSION: 1.4
   // Creates a new #CallyRectangle for the given @actor. @actor must be
   // a #ClutterRectangle.
   // RETURNS: the newly created #AtkObject
   // <actor>: a #ClutterActor
   static Rectangle* /*new*/ new_(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
      return cally_rectangle_new(UpCast!(Clutter.Actor*)(actor));
   }
   static auto opCall(AT0)(AT0 /*Clutter.Actor*/ actor) {
      return cally_rectangle_new(UpCast!(Clutter.Actor*)(actor));
   }
}


// The <structname>CallyRectangleClass</structname> structure contains
// only private data
struct RectangleClass /* Version 1.4 */ {
   private ActorClass parent_class;
   private void*[8] _padding_dummy;
}

struct RectanglePrivate {
}


// The <structname>CallyRoot</structname> structure contains only private
// data and should be accessed using the provided API
struct Root /* : Atk.GObjectAccessible */ /* Version 1.4 */ {
   alias parent this;
   alias parent super_;
   alias parent gobjectaccessible;
   Atk.GObjectAccessible parent;
   private RootPrivate* priv;


   // VERSION: 1.4
   // Creates a new #CallyRoot object.
   // RETURNS: the newly created #AtkObject
   static Root* /*new*/ new_()() nothrow {
      return cally_root_new();
   }
   static auto opCall()() {
      return cally_root_new();
   }
}


// The <structname>CallyRootClass</structname> structure contains only
// private data
struct RootClass /* Version 1.4 */ {
   private Atk.GObjectAccessibleClass parent_class;
   private void*[16] _padding_dummy;
}

struct RootPrivate {
}


// The <structname>CallyStage</structname> structure contains only
// private data and should be accessed using the provided API
struct Stage /* : Group */ /* Version 1.4 */ {
   mixin Atk.Action.__interface__;
   mixin Atk.Component.__interface__;
   mixin Atk.Window.__interface__;
   alias parent this;
   alias parent super_;
   alias parent group;
   Group parent;
   private StagePrivate* priv;


   // VERSION: 1.4
   // Creates a new #CallyStage for the given @actor. @actor should be a
   // #ClutterStage.
   // RETURNS: the newly created #AtkObject
   // <actor>: a #ClutterActor
   static Stage* /*new*/ new_(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
      return cally_stage_new(UpCast!(Clutter.Actor*)(actor));
   }
   static auto opCall(AT0)(AT0 /*Clutter.Actor*/ actor) {
      return cally_stage_new(UpCast!(Clutter.Actor*)(actor));
   }
}


// The <structname>CallyStageClass</structname> structure contains only
// private data
struct StageClass /* Version 1.4 */ {
   private GroupClass parent_class;
   private void*[16] _padding_dummy;
}

struct StagePrivate {
}


// The <structname>CallyText</structname> structure contains only private
// data and should be accessed using the provided API
struct Text /* : Actor */ /* Version 1.4 */ {
   mixin Atk.Action.__interface__;
   mixin Atk.Component.__interface__;
   mixin Atk.EditableText.__interface__;
   mixin Atk.Text.__interface__;
   alias parent this;
   alias parent super_;
   alias parent actor;
   Actor parent;
   private TextPrivate* priv;


   // VERSION: 1.4
   // Creates a new #CallyText for the given @actor. @actor must be a
   // #ClutterText.
   // RETURNS: the newly created #AtkObject
   // <actor>: a #ClutterActor
   static Text* /*new*/ new_(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
      return cally_text_new(UpCast!(Clutter.Actor*)(actor));
   }
   static auto opCall(AT0)(AT0 /*Clutter.Actor*/ actor) {
      return cally_text_new(UpCast!(Clutter.Actor*)(actor));
   }
}


// The <structname>CallyTextClass</structname> structure contains only
// private data
struct TextClass /* Version 1.4 */ {
   private ActorClass parent_class;
   private void*[8] _padding_dummy;
}

struct TextPrivate {
}


// The <structname>CallyTexture</structname> structure contains only
// private data and should be accessed using the provided API
struct Texture /* : Actor */ /* Version 1.4 */ {
   mixin Atk.Action.__interface__;
   mixin Atk.Component.__interface__;
   alias parent this;
   alias parent super_;
   alias parent actor;
   Actor parent;
   private TexturePrivate* priv;


   // VERSION: 1.4
   // Creates a new #CallyTexture for the given @actor. @actor must be
   // a #ClutterTexture.
   // RETURNS: the newly created #AtkObject
   // <actor>: a #ClutterActor
   static Texture* /*new*/ new_(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
      return cally_texture_new(UpCast!(Clutter.Actor*)(actor));
   }
   static auto opCall(AT0)(AT0 /*Clutter.Actor*/ actor) {
      return cally_texture_new(UpCast!(Clutter.Actor*)(actor));
   }
}


// The <structname>CallyTextureClass</structname> structure contains
// only private data
struct TextureClass /* Version 1.4 */ {
   private ActorClass parent_class;
   private void*[8] _padding_dummy;
}

struct TexturePrivate {
}


// The <structname>CallyUtil</structname> structure contains only
// private data and should be accessed using the provided API
struct Util /* : Atk.Util */ /* Version 1.4 */ {
   alias parent this;
   alias parent super_;
   alias parent util;
   Atk.Util parent;
   private UtilPrivate* priv;
}


// The <structname>CallyUtilClass</structname> structure contains only
// private data
struct UtilClass /* Version 1.4 */ {
   private Atk.UtilClass parent_class;
   private void*[8] _padding_dummy;
}

struct UtilPrivate {
}


// VERSION: 1.4
// Initializes the accessibility support.
// 
// initialized.
// RETURNS: %TRUE if accessibility support has been correctly
static int accessibility_init()() nothrow {
   return cally_accessibility_init();
}


// VERSION: 1.4
// Returns if the accessibility support using cally is enabled.
// 
// initialized.
// RETURNS: %TRUE if accessibility support has been correctly
static int get_cally_initialized()() nothrow {
   return cally_get_cally_initialized();
}


// C prototypes:

extern (C) {
Actor* /*new*/ cally_actor_new(Clutter.Actor* actor) nothrow;
uint cally_actor_add_action(Actor* this_, char* action_name, char* action_description, char* action_keybinding, ActionFunc action_func) nothrow;
uint cally_actor_add_action_full(Actor* this_, char* action_name, char* action_description, char* action_keybinding, ActionCallback callback, void* user_data, GLib2.DestroyNotify notify) nothrow;
int cally_actor_remove_action(Actor* this_, int action_id) nothrow;
int cally_actor_remove_action_by_name(Actor* this_, char* action_name) nothrow;
Clone* /*new*/ cally_clone_new(Clutter.Actor* actor) nothrow;
Group* /*new*/ cally_group_new(Clutter.Actor* actor) nothrow;
Rectangle* /*new*/ cally_rectangle_new(Clutter.Actor* actor) nothrow;
Root* /*new*/ cally_root_new() nothrow;
Stage* /*new*/ cally_stage_new(Clutter.Actor* actor) nothrow;
Text* /*new*/ cally_text_new(Clutter.Actor* actor) nothrow;
Texture* /*new*/ cally_texture_new(Clutter.Actor* actor) nothrow;
int cally_accessibility_init() nothrow;
int cally_get_cally_initialized() nothrow;
}

// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/Clutter-1.0.gir"

module Clutter;
public import gtk2.atk;
alias gtk2.atk Atk;
public import gtk2.cogl;
alias gtk2.cogl Cogl;
public import gtk2.coglpango;
alias gtk2.coglpango CoglPango;
public import gtk2.gl;
alias gtk2.gl GL;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;
public import gtk2.gio2;
alias gtk2.gio2 Gio2;
public import gtk2.json;
alias gtk2.json Json;
public import gtk2.pango;
alias gtk2.pango Pango;
public import gtk2.pangocairo;
alias gtk2.pangocairo PangoCairo;
public import gtk2.pangoft2;
alias gtk2.pangoft2 PangoFT2;
public import gtk2.cairo;
alias gtk2.cairo cairo;
public import gtk2.fontconfig2;
alias gtk2.fontconfig2 fontconfig2;
public import gtk2.freetype2;
alias gtk2.freetype2 freetype2;

// package: "clutter-1.0";
// C header: "clutter/clutter.h";

// c:symbol-prefixes: ["clutter"]
// c:identifier-prefixes: ["Clutter"]

// module Clutter;

enum int _0 = 48;
enum int _1 = 49;
enum int _2 = 50;
enum int _3 = 51;
enum int _3270_AltCursor = 64784;
enum int _3270_Attn = 64782;
enum int _3270_BackTab = 64773;
enum int _3270_ChangeScreen = 64793;
enum int _3270_Copy = 64789;
enum int _3270_CursorBlink = 64783;
enum int _3270_CursorSelect = 64796;
enum int _3270_DeleteWord = 64794;
enum int _3270_Duplicate = 64769;
enum int _3270_Enter = 64798;
enum int _3270_EraseEOF = 64774;
enum int _3270_EraseInput = 64775;
enum int _3270_ExSelect = 64795;
enum int _3270_FieldMark = 64770;
enum int _3270_Ident = 64787;
enum int _3270_Jump = 64786;
enum int _3270_KeyClick = 64785;
enum int _3270_Left2 = 64772;
enum int _3270_PA1 = 64778;
enum int _3270_PA2 = 64779;
enum int _3270_PA3 = 64780;
enum int _3270_Play = 64790;
enum int _3270_PrintScreen = 64797;
enum int _3270_Quit = 64777;
enum int _3270_Record = 64792;
enum int _3270_Reset = 64776;
enum int _3270_Right2 = 64771;
enum int _3270_Rule = 64788;
enum int _3270_Setup = 64791;
enum int _3270_Test = 64781;
enum int _4 = 52;
enum int _5 = 53;
enum int _6 = 54;
enum int _7 = 55;
enum int _8 = 56;
enum int _9 = 57;
enum int A = 65;
enum int AE = 198;
enum int Aacute = 193;
enum int Abelowdot = 16785056;
enum int Abreve = 451;
enum int Abreveacute = 16785070;
enum int Abrevebelowdot = 16785078;
enum int Abrevegrave = 16785072;
enum int Abrevehook = 16785074;
enum int Abrevetilde = 16785076;
enum int AccessX_Enable = 65136;
enum int AccessX_Feedback_Enable = 65137;
enum int Acircumflex = 194;
enum int Acircumflexacute = 16785060;
enum int Acircumflexbelowdot = 16785068;
enum int Acircumflexgrave = 16785062;
enum int Acircumflexhook = 16785064;
enum int Acircumflextilde = 16785066;

// The <structname>ClutterAction</structname> structure contains only
// private data and should be accessed using the provided API
struct Action /* : ActorMeta */ /* Version 1.4 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance actormeta;
   ActorMeta parent_instance;
}


// The <structname>ClutterActionClass</structname> structure contains
// only private data
struct ActionClass /* Version 1.4 */ {
   private ActorMetaClass parent_class;
   extern (C) void function () nothrow _clutter_action1;
   extern (C) void function () nothrow _clutter_action2;
   extern (C) void function () nothrow _clutter_action3;
   extern (C) void function () nothrow _clutter_action4;
   extern (C) void function () nothrow _clutter_action5;
   extern (C) void function () nothrow _clutter_action6;
   extern (C) void function () nothrow _clutter_action7;
   extern (C) void function () nothrow _clutter_action8;
}

// Base class for actors.
struct Actor /* : GObject.InitiallyUnowned */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Animatable.__interface__;
   mixin Container.__interface__;
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance initiallyunowned;
   GObject2.InitiallyUnowned parent_instance;
   uint flags;
   private uint private_flags;
   private ActorPrivate* priv;


   // VERSION: 1.10
   // Creates a new #ClutterActor.
   // 
   // A newly created actor has a floating reference, which will be sunk
   // when it is added to another actor.
   // RETURNS: the newly created #ClutterActor
   static Actor* /*new*/ new_()() nothrow {
      return clutter_actor_new();
   }
   static auto opCall()() {
      return clutter_actor_new();
   }

   // VERSION: 1.4
   // Adds @action to the list of actions applied to @self
   // 
   // A #ClutterAction can only belong to one actor at a time
   // 
   // The #ClutterActor will hold a reference on @action until either
   // clutter_actor_remove_action() or clutter_actor_clear_actions()
   // is called
   // <action>: a #ClutterAction
   void add_action(AT0)(AT0 /*Action*/ action) nothrow {
      clutter_actor_add_action(&this, UpCast!(Action*)(action));
   }

   // VERSION: 1.4
   // A convenience function for setting the name of a #ClutterAction
   // while adding it to the list of actions applied to @self
   // 
   // This function is the logical equivalent of:
   // 
   // |[
   // clutter_actor_meta_set_name (CLUTTER_ACTOR_META (action), name);
   // clutter_actor_add_action (self, action);
   // ]|
   // <name>: the name to set on the action
   // <action>: a #ClutterAction
   void add_action_with_name(AT0, AT1)(AT0 /*char*/ name, AT1 /*Action*/ action) nothrow {
      clutter_actor_add_action_with_name(&this, toCString!(char*)(name), UpCast!(Action*)(action));
   }

   // VERSION: 1.10
   // Adds @child to the children of @self.
   // 
   // This function will acquire a reference on @child that will only
   // be released when calling clutter_actor_remove_child().
   // 
   // This function will take into consideration the #ClutterActor:depth
   // of @child, and will keep the list of children sorted.
   // 
   // This function will emit the #ClutterContainer::actor-added signal
   // on @self.
   // <child>: a #ClutterActor
   void add_child(AT0)(AT0 /*Actor*/ child) nothrow {
      clutter_actor_add_child(&this, UpCast!(Actor*)(child));
   }

   // VERSION: 1.4
   // Adds @constraint to the list of #ClutterConstraint<!-- -->s applied
   // to @self
   // 
   // The #ClutterActor will hold a reference on the @constraint until
   // either clutter_actor_remove_constraint() or
   // clutter_actor_clear_constraints() is called.
   // <constraint>: a #ClutterConstraint
   void add_constraint(AT0)(AT0 /*Constraint*/ constraint) nothrow {
      clutter_actor_add_constraint(&this, UpCast!(Constraint*)(constraint));
   }

   // VERSION: 1.4
   // A convenience function for setting the name of a #ClutterConstraint
   // while adding it to the list of constraints applied to @self
   // 
   // This function is the logical equivalent of:
   // 
   // |[
   // clutter_actor_meta_set_name (CLUTTER_ACTOR_META (constraint), name);
   // clutter_actor_add_constraint (self, constraint);
   // ]|
   // <name>: the name to set on the constraint
   // <constraint>: a #ClutterConstraint
   void add_constraint_with_name(AT0, AT1)(AT0 /*char*/ name, AT1 /*Constraint*/ constraint) nothrow {
      clutter_actor_add_constraint_with_name(&this, toCString!(char*)(name), UpCast!(Constraint*)(constraint));
   }

   // VERSION: 1.4
   // Adds @effect to the list of #ClutterEffect<!-- -->s applied to @self
   // 
   // The #ClutterActor will hold a reference on the @effect until either
   // clutter_actor_remove_effect() or clutter_actor_clear_effects() is
   // called.
   // <effect>: a #ClutterEffect
   void add_effect(AT0)(AT0 /*Effect*/ effect) nothrow {
      clutter_actor_add_effect(&this, UpCast!(Effect*)(effect));
   }

   // VERSION: 1.4
   // A convenience function for setting the name of a #ClutterEffect
   // while adding it to the list of effectss applied to @self
   // 
   // This function is the logical equivalent of:
   // 
   // |[
   // clutter_actor_meta_set_name (CLUTTER_ACTOR_META (effect), name);
   // clutter_actor_add_effect (self, effect);
   // ]|
   // <name>: the name to set on the effect
   // <effect>: a #ClutterEffect
   void add_effect_with_name(AT0, AT1)(AT0 /*char*/ name, AT1 /*Effect*/ effect) nothrow {
      clutter_actor_add_effect_with_name(&this, toCString!(char*)(name), UpCast!(Effect*)(effect));
   }

   // VERSION: 0.8
   // Called by the parent of an actor to assign the actor its size.
   // Should never be called by applications (except when implementing
   // a container or layout manager).
   // 
   // Actors can know from their allocation box whether they have moved
   // with respect to their parent actor. The @flags parameter describes
   // additional information about the allocation, for instance whether
   // the parent has moved with respect to the stage, for example because
   // a grandparent's origin has moved.
   // <box>: new allocation of the actor, in parent-relative coordinates
   // <flags>: flags that control the allocation
   void allocate(AT0)(AT0 /*ActorBox*/ box, AllocationFlags flags) nothrow {
      clutter_actor_allocate(&this, UpCast!(ActorBox*)(box), flags);
   }

   // VERSION: 1.4
   // Allocates @self by taking into consideration the available allocation
   // area; an alignment factor on either axis; and whether the actor should
   // fill the allocation on either axis.
   // 
   // The @box should contain the available allocation width and height;
   // if the x1 and y1 members of #ClutterActorBox are not set to 0, the
   // allocation will be offset by their value.
   // 
   // This function takes into consideration the geometry request specified by
   // the #ClutterActor:request-mode property, and the text direction.
   // 
   // This function is useful for fluid layout managers, like #ClutterBinLayout
   // or #ClutterTableLayout
   // <box>: a #ClutterActorBox, containing the available width and height
   // <x_align>: the horizontal alignment, between 0 and 1
   // <y_align>: the vertical alignment, between 0 and 1
   // <x_fill>: whether the actor should fill horizontally
   // <y_fill>: whether the actor should fill vertically
   // <flags>: allocation flags to be passed to clutter_actor_allocate()
   void allocate_align_fill(AT0)(AT0 /*ActorBox*/ box, double x_align, double y_align, int x_fill, int y_fill, AllocationFlags flags) nothrow {
      clutter_actor_allocate_align_fill(&this, UpCast!(ActorBox*)(box), x_align, y_align, x_fill, y_fill, flags);
   }

   // VERSION: 1.0
   // Allocates @self taking into account the #ClutterActor<!-- -->'s
   // preferred size, but limiting it to the maximum available width
   // and height provided.
   // 
   // This function will do the right thing when dealing with the
   // actor's request mode.
   // 
   // The implementation of this function is equivalent to:
   // 
   // |[
   // if (request_mode == CLUTTER_REQUEST_HEIGHT_FOR_WIDTH)
   // {
   // clutter_actor_get_preferred_width (self, available_height,
   // &amp;min_width,
   // &amp;natural_width);
   // width = CLAMP (natural_width, min_width, available_width);
   // 
   // clutter_actor_get_preferred_height (self, width,
   // &amp;min_height,
   // &amp;natural_height);
   // height = CLAMP (natural_height, min_height, available_height);
   // }
   // else
   // {
   // clutter_actor_get_preferred_height (self, available_width,
   // &amp;min_height,
   // &amp;natural_height);
   // height = CLAMP (natural_height, min_height, available_height);
   // 
   // clutter_actor_get_preferred_width (self, height,
   // &amp;min_width,
   // &amp;natural_width);
   // width = CLAMP (natural_width, min_width, available_width);
   // }
   // 
   // box.x1 = x; box.y1 = y;
   // box.x2 = box.x1 + available_width;
   // box.y2 = box.y1 + available_height;
   // clutter_actor_allocate (self, &amp;box, flags);
   // ]|
   // 
   // This function can be used by fluid layout managers to allocate
   // an actor's preferred size without making it bigger than the area
   // available for the container.
   // <x>: the actor's X coordinate
   // <y>: the actor's Y coordinate
   // <available_width>: the maximum available width, or -1 to use the actor's natural width
   // <available_height>: the maximum available height, or -1 to use the actor's natural height
   // <flags>: flags controlling the allocation
   void allocate_available_size()(float x, float y, float available_width, float available_height, AllocationFlags flags) nothrow {
      clutter_actor_allocate_available_size(&this, x, y, available_width, available_height, flags);
   }

   // VERSION: 0.8
   // Allocates the natural size of @self.
   // 
   // This function is a utility call for #ClutterActor implementations
   // that allocates the actor's preferred natural size. It can be used
   // by fixed layout managers (like #ClutterGroup or so called
   // 'composite actors') inside the ClutterActor::allocate
   // implementation to give each child exactly how much space it
   // requires.
   // 
   // This function is not meant to be used by applications. It is also
   // not meant to be used outside the implementation of the
   // ClutterActor::allocate virtual function.
   // <flags>: flags controlling the allocation
   void allocate_preferred_size()(AllocationFlags flags) nothrow {
      clutter_actor_allocate_preferred_size(&this, flags);
   }

   // Unintrospectable method: animate() / clutter_actor_animate()
   // VERSION: 1.0
   // Animates the given list of properties of @actor between the current
   // value for each property and a new final value. The animation has a
   // definite duration and a speed given by the @mode.
   // 
   // For example, this:
   // 
   // |[
   // clutter_actor_animate (rectangle, CLUTTER_LINEAR, 250,
   // "width", 100.0,
   // "height", 100.0,
   // NULL);
   // ]|
   // 
   // will make width and height properties of the #ClutterActor "rectangle"
   // grow linearly between the current value and 100 pixels, in 250 milliseconds.
   // 
   // The animation @mode is a logical id, either from the #ClutterAnimationMode
   // enumeration of from clutter_alpha_register_func().
   // 
   // All the properties specified will be animated between the current value
   // and the final value. If a property should be set at the beginning of
   // the animation but not updated during the animation, it should be prefixed
   // by the "fixed::" string, for instance:
   // 
   // |[
   // clutter_actor_animate (actor, CLUTTER_EASE_IN_SINE, 100,
   // "rotation-angle-z", 360.0,
   // "fixed::rotation-center-z", &amp;center,
   // NULL);
   // ]|
   // 
   // Will animate the "rotation-angle-z" property between the current value
   // and 360 degrees, and set the "rotation-center-z" property to the fixed
   // value of the #ClutterVertex "center".
   // 
   // This function will implicitly create a #ClutterAnimation object which
   // will be assigned to the @actor and will be returned to the developer
   // to control the animation or to know when the animation has been
   // completed.
   // 
   // If a name argument starts with "signal::", "signal-after::",
   // "signal-swapped::" or "signal-swapped-after::" the two following arguments
   // are used as callback function and data for a signal handler installed on
   // the #ClutterAnimation object for the specified signal name, for instance:
   // 
   // |[
   // 
   // static void
   // on_animation_completed (ClutterAnimation *animation,
   // ClutterActor     *actor)
   // {
   // clutter_actor_hide (actor);
   // }
   // 
   // clutter_actor_animate (actor, CLUTTER_EASE_IN_CUBIC, 100,
   // "opacity", 0,
   // "signal::completed", on_animation_completed, actor,
   // NULL);
   // ]|
   // 
   // or, to automatically destroy an actor at the end of the animation:
   // 
   // |[
   // clutter_actor_animate (actor, CLUTTER_EASE_IN_CUBIC, 100,
   // "opacity", 0,
   // "signal-swapped-after::completed",
   // clutter_actor_destroy,
   // actor,
   // NULL);
   // ]|
   // 
   // The "signal::" modifier is the equivalent of using g_signal_connect();
   // the "signal-after::" modifier is the equivalent of using
   // g_signal_connect_after() or g_signal_connect_data() with the
   // %G_CONNECT_AFTER; the "signal-swapped::" modifier is the equivalent
   // of using g_signal_connect_swapped() or g_signal_connect_data() with the
   // %G_CONNECT_SWAPPED flah; finally, the "signal-swapped-after::" modifier
   // is the equivalent of using g_signal_connect_data() with both the
   // %G_CONNECT_AFTER and %G_CONNECT_SWAPPED flags. The clutter_actor_animate()
   // function will not keep track of multiple connections to the same signal,
   // so it is your responsability to avoid them when calling
   // clutter_actor_animate() multiple times on the same actor.
   // 
   // Calling this function on an actor that is already being animated
   // will cause the current animation to change with the new final values,
   // the new easing mode and the new duration - that is, this code:
   // 
   // |[
   // clutter_actor_animate (actor, CLUTTER_LINEAR, 250,
   // "width", 100.0,
   // "height", 100.0,
   // NULL);
   // clutter_actor_animate (actor, CLUTTER_EASE_IN_CUBIC, 500,
   // "x", 100.0,
   // "y", 100.0,
   // "width", 200.0,
   // NULL);
   // ]|
   // 
   // is the equivalent of:
   // 
   // |[
   // clutter_actor_animate (actor, CLUTTER_EASE_IN_CUBIC, 500,
   // "x", 100.0,
   // "y", 100.0,
   // "width", 200.0,
   // "height", 100.0,
   // NULL);
   // ]|
   // 
   // <note>Unless the animation is looping, the #ClutterAnimation created by
   // clutter_actor_animate() will become invalid as soon as it is
   // complete.</note>
   // 
   // Since the created #ClutterAnimation instance attached to @actor
   // is guaranteed to be valid throughout the #ClutterAnimation::completed
   // signal emission chain, you will not be able to create a new animation
   // using clutter_actor_animate() on the same @actor from within the
   // #ClutterAnimation::completed signal handler unless you use
   // g_signal_connect_after() to connect the callback function, for instance:
   // 
   // |[
   // static void
   // on_animation_completed (ClutterAnimation *animation,
   // ClutterActor     *actor)
   // {
   // clutter_actor_animate (actor, CLUTTER_EASE_OUT_CUBIC, 250,
   // "x", 500.0,
   // "y", 500.0,
   // NULL);
   // }
   // 
   // ...
   // animation = clutter_actor_animate (actor, CLUTTER_EASE_IN_CUBIC, 250,
   // "x", 100.0,
   // "y", 100.0,
   // NULL);
   // g_signal_connect (animation, "completed",
   // G_CALLBACK (on_animation_completed),
   // actor);
   // ...
   // ]|
   // 
   // owned by the #ClutterActor and should not be unreferenced with
   // g_object_unref()
   // RETURNS: a #ClutterAnimation object. The object is
   // <mode>: an animation mode logical id
   // <duration>: duration of the animation, in milliseconds
   // <first_property_name>: the name of a property
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_actor_animate animate; // Variadic
   +/

   // Unintrospectable method: animate_with_alpha() / clutter_actor_animate_with_alpha()
   // VERSION: 1.0
   // Animates the given list of properties of @actor between the current
   // value for each property and a new final value. The animation has a
   // definite behaviour given by the passed @alpha.
   // 
   // See clutter_actor_animate() for further details.
   // 
   // This function is useful if you want to use an existing #ClutterAlpha
   // to animate @actor.
   // 
   // #ClutterActor and should not be unreferenced with g_object_unref()
   // RETURNS: a #ClutterAnimation object. The object is owned by the
   // <alpha>: a #ClutterAlpha
   // <first_property_name>: the name of a property
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_actor_animate_with_alpha animate_with_alpha; // Variadic
   +/

   // VERSION: 1.0
   // Animates the given list of properties of @actor between the current
   // value for each property and a new final value. The animation has a
   // definite behaviour given by the passed @alpha.
   // 
   // See clutter_actor_animate() for further details.
   // 
   // This function is useful if you want to use an existing #ClutterAlpha
   // to animate @actor.
   // 
   // This is the vector-based variant of clutter_actor_animate_with_alpha(),
   // useful for language bindings.
   // 
   // <warning>Unlike clutter_actor_animate_with_alpha(), this function will
   // not allow you to specify "signal::" names and callbacks.</warning>
   // 
   // #ClutterActor and should not be unreferenced with g_object_unref()
   // RETURNS: a #ClutterAnimation object. The object is owned by the
   // <alpha>: a #ClutterAlpha
   // <n_properties>: number of property names and values
   // <properties>: a vector containing the property names to set
   // <values>: a vector containing the property values to set
   Animation* animate_with_alphav(AT0, AT1, AT2)(AT0 /*Alpha*/ alpha, int n_properties, AT1 /*char*/ properties, AT2 /*GObject2.Value*/ values) nothrow {
      return clutter_actor_animate_with_alphav(&this, UpCast!(Alpha*)(alpha), n_properties, toCString!(char*)(properties), UpCast!(GObject2.Value*)(values));
   }

   // Unintrospectable method: animate_with_timeline() / clutter_actor_animate_with_timeline()
   // VERSION: 1.0
   // Animates the given list of properties of @actor between the current
   // value for each property and a new final value. The animation has a
   // definite duration given by @timeline and a speed given by the @mode.
   // 
   // See clutter_actor_animate() for further details.
   // 
   // This function is useful if you want to use an existing timeline
   // to animate @actor.
   // 
   // owned by the #ClutterActor and should not be unreferenced with
   // g_object_unref()
   // RETURNS: a #ClutterAnimation object. The object is
   // <mode>: an animation mode logical id
   // <timeline>: a #ClutterTimeline
   // <first_property_name>: the name of a property
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_actor_animate_with_timeline animate_with_timeline; // Variadic
   +/

   // VERSION: 1.0
   // Animates the given list of properties of @actor between the current
   // value for each property and a new final value. The animation has a
   // definite duration given by @timeline and a speed given by the @mode.
   // 
   // See clutter_actor_animate() for further details.
   // 
   // This function is useful if you want to use an existing timeline
   // to animate @actor.
   // 
   // This is the vector-based variant of clutter_actor_animate_with_timeline(),
   // useful for language bindings.
   // 
   // <warning>Unlike clutter_actor_animate_with_timeline(), this function
   // will not allow you to specify "signal::" names and callbacks.</warning>
   // 
   // owned by the #ClutterActor and should not be unreferenced with
   // g_object_unref()
   // RETURNS: a #ClutterAnimation object. The object is
   // <mode>: an animation mode logical id
   // <timeline>: a #ClutterTimeline
   // <n_properties>: number of property names and values
   // <properties>: a vector containing the property names to set
   // <values>: a vector containing the property values to set
   Animation* animate_with_timelinev(AT0, AT1, AT2)(c_ulong mode, AT0 /*Timeline*/ timeline, int n_properties, AT1 /*char*/ properties, AT2 /*GObject2.Value*/ values) nothrow {
      return clutter_actor_animate_with_timelinev(&this, mode, UpCast!(Timeline*)(timeline), n_properties, toCString!(char*)(properties), UpCast!(GObject2.Value*)(values));
   }

   // VERSION: 1.0
   // Animates the given list of properties of @actor between the current
   // value for each property and a new final value. The animation has a
   // definite duration and a speed given by the @mode.
   // 
   // This is the vector-based variant of clutter_actor_animate(), useful
   // for language bindings.
   // 
   // <warning>Unlike clutter_actor_animate(), this function will not
   // allow you to specify "signal::" names and callbacks.</warning>
   // 
   // owned by the #ClutterActor and should not be unreferenced with
   // g_object_unref()
   // RETURNS: a #ClutterAnimation object. The object is
   // <mode>: an animation mode logical id
   // <duration>: duration of the animation, in milliseconds
   // <n_properties>: number of property names and values
   // <properties>: a vector containing the property names to set
   // <values>: a vector containing the property values to set
   Animation* animatev(AT0, AT1)(c_ulong mode, uint duration, int n_properties, AT0 /*char*/ properties, AT1 /*GObject2.Value*/ values) nothrow {
      return clutter_actor_animatev(&this, mode, duration, n_properties, toCString!(char*)(properties), UpCast!(GObject2.Value*)(values));
   }

   // VERSION: 0.6
   // Transforms @point in coordinates relative to the actor into
   // ancestor-relative coordinates using the relevant transform
   // stack (i.e. scale, rotation, etc).
   // 
   // If @ancestor is %NULL the ancestor will be the #ClutterStage. In
   // this case, the coordinates returned will be the coordinates on
   // the stage before the projection is applied. This is different from
   // the behaviour of clutter_actor_apply_transform_to_point().
   // <ancestor>: A #ClutterActor ancestor, or %NULL to use the default #ClutterStage
   // <point>: A point as #ClutterVertex
   // <vertex>: The translated #ClutterVertex
   void apply_relative_transform_to_point(AT0, AT1, AT2)(AT0 /*Actor*/ ancestor, AT1 /*Vertex*/ point, /*out*/ AT2 /*Vertex*/ vertex) nothrow {
      clutter_actor_apply_relative_transform_to_point(&this, UpCast!(Actor*)(ancestor), UpCast!(Vertex*)(point), UpCast!(Vertex*)(vertex));
   }

   // VERSION: 0.4
   // Transforms @point in coordinates relative to the actor
   // into screen-relative coordinates with the current actor
   // transformation (i.e. scale, rotation, etc)
   // <point>: A point as #ClutterVertex
   // <vertex>: The translated #ClutterVertex
   void apply_transform_to_point(AT0, AT1)(AT0 /*Vertex*/ point, /*out*/ AT1 /*Vertex*/ vertex) nothrow {
      clutter_actor_apply_transform_to_point(&this, UpCast!(Vertex*)(point), UpCast!(Vertex*)(vertex));
   }

   // VERSION: 1.4
   // Clears the list of actions applied to @self
   void clear_actions()() nothrow {
      clutter_actor_clear_actions(&this);
   }

   // VERSION: 1.4
   // Clears the list of constraints applied to @self
   void clear_constraints()() nothrow {
      clutter_actor_clear_constraints(&this);
   }

   // VERSION: 1.4
   // Clears the list of effects applied to @self
   void clear_effects()() nothrow {
      clutter_actor_clear_effects(&this);
   }

   // VERSION: 1.4
   // Determines if @descendant is contained inside @self (either as an
   // immediate child, or as a deeper descendant). If @self and
   // @descendant point to the same actor then it will also return %TRUE.
   // RETURNS: whether @descendent is contained within @self
   // <descendant>: A #ClutterActor, possibly contained in @self
   int contains(AT0)(AT0 /*Actor*/ descendant) nothrow {
      return clutter_actor_contains(&this, UpCast!(Actor*)(descendant));
   }

   // VERSION: 1.8
   // Run the next stage of the paint sequence. This function should only
   // be called within the implementation of the ‘run’ virtual of a
   // #ClutterEffect. It will cause the run method of the next effect to
   // be applied, or it will paint the actual actor if the current effect
   // is the last effect in the chain.
   void continue_paint()() nothrow {
      clutter_actor_continue_paint(&this);
   }

   // VERSION: 1.0
   // Creates a #PangoContext for the given actor. The #PangoContext
   // is already configured using the appropriate font map, resolution
   // and font options.
   // 
   // See also clutter_actor_get_pango_context().
   // 
   // Use g_object_unref() on the returned value to deallocate its
   // resources
   // RETURNS: the newly created #PangoContext.
   Pango.Context* /*new*/ create_pango_context()() nothrow {
      return clutter_actor_create_pango_context(&this);
   }

   // VERSION: 1.0
   // Creates a new #PangoLayout from the same #PangoContext used
   // by the #ClutterActor. The #PangoLayout is already configured
   // with the font map, resolution and font options, and the
   // given @text.
   // 
   // If you want to keep around a #PangoLayout created by this
   // function you will have to connect to the #ClutterBackend::font-changed
   // and #ClutterBackend::resolution-changed signals, and call
   // pango_layout_context_changed() in response to them.
   // 
   // Use g_object_unref() when done
   // RETURNS: the newly created #PangoLayout.
   // <text>: (allow-none) the text to set on the #PangoLayout, or %NULL
   Pango.Layout* /*new*/ create_pango_layout(AT0)(AT0 /*char*/ text) nothrow {
      return clutter_actor_create_pango_layout(&this, toCString!(char*)(text));
   }

   // Destroys an actor.  When an actor is destroyed, it will break any
   // references it holds to other objects.  If the actor is inside a
   // container, the actor will be removed.
   // 
   // When you destroy a container, its children will be destroyed as well.
   // 
   // Note: you cannot destroy the #ClutterStage returned by
   // clutter_stage_get_default().
   void destroy()() nothrow {
      clutter_actor_destroy(&this);
   }

   // VERSION: 1.4
   // Detaches the #ClutterAnimation used by @actor, if clutter_actor_animate()
   // has been called on @actor.
   // 
   // Once the animation has been detached, it loses a reference. If it was
   // the only reference then the #ClutterAnimation becomes invalid.
   // 
   // The #ClutterAnimation::completed signal will not be emitted.
   void detach_animation()() nothrow {
      clutter_actor_detach_animation(&this);
   }

   // VERSION: 0.6
   // This function is used to emit an event on the main stage.
   // You should rarely need to use this function, except for
   // synthetising events.
   // 
   // if the actor handled the event, or %FALSE if the event was
   // not handled
   // RETURNS: the return value from the signal emission: %TRUE
   // <event>: a #ClutterEvent
   // <capture>: TRUE if event in in capture phase, FALSE otherwise.
   int event(AT0)(AT0 /*Event*/ event, int capture) nothrow {
      return clutter_actor_event(&this, UpCast!(Event*)(event), capture);
   }

   // VERSION: 0.4
   // Calculates the transformed screen coordinates of the four corners of
   // the actor; the returned vertices relate to the #ClutterActorBox
   // coordinates  as follows:
   // <itemizedlist>
   // <listitem><para>v[0] contains (x1, y1)</para></listitem>
   // <listitem><para>v[1] contains (x2, y1)</para></listitem>
   // <listitem><para>v[2] contains (x1, y2)</para></listitem>
   // <listitem><para>v[3] contains (x2, y2)</para></listitem>
   // </itemizedlist>
   // <verts>: Pointer to a location of an array of 4 #ClutterVertex where to store the result.
   void get_abs_allocation_vertices()(/*out*/ Vertex verts) nothrow {
      clutter_actor_get_abs_allocation_vertices(&this, verts);
   }

   // Returns the accessible object that describes the actor to an
   // assistive technology.
   // 
   // If no class-specific #AtkObject implementation is available for the
   // actor instance in question, it will inherit an #AtkObject
   // implementation from the first ancestor class for which such an
   // implementation is defined.
   // 
   // The documentation of the <ulink
   // url="http://developer.gnome.org/doc/API/2.0/atk/index.html">ATK</ulink>
   // library contains more information about accessible objects and
   // their uses.
   // RETURNS: the #AtkObject associated with @actor
   Atk.Object* get_accessible()() nothrow {
      return clutter_actor_get_accessible(&this);
   }

   // VERSION: 1.4
   // Retrieves the #ClutterAction with the given name in the list
   // of actions applied to @self
   // 
   // name, or %NULL. The returned #ClutterAction is owned by the
   // actor and it should not be unreferenced directly
   // RETURNS: a #ClutterAction for the given
   // <name>: the name of the action to retrieve
   Action* get_action(AT0)(AT0 /*char*/ name) nothrow {
      return clutter_actor_get_action(&this, toCString!(char*)(name));
   }

   // VERSION: 1.4
   // Retrieves the list of actions applied to @self
   // 
   // of the list of #ClutterAction<!-- -->s. The contents of the list are
   // owned by the #ClutterActor. Use g_list_free() to free the resources
   // allocated by the returned #GList
   // RETURNS: a copy
   GLib2.List* /*new container*/ get_actions()() nothrow {
      return clutter_actor_get_actions(&this);
   }

   // VERSION: 0.8
   // Gets the layout box an actor has been assigned. The allocation can
   // only be assumed valid inside a paint() method; anywhere else, it
   // may be out-of-date.
   // 
   // An allocation does not incorporate the actor's scale or anchor point;
   // those transformations do not affect layout, only rendering.
   // 
   // <note>Do not call any of the clutter_actor_get_allocation_*() family
   // of functions inside the implementation of the get_preferred_width()
   // or get_preferred_height() virtual functions.</note>
   // <box>: the function fills this in with the actor's allocation
   void get_allocation_box(AT0)(/*out*/ AT0 /*ActorBox*/ box) nothrow {
      clutter_actor_get_allocation_box(&this, UpCast!(ActorBox*)(box));
   }

   // VERSION: 0.8
   // Gets the layout box an actor has been assigned.  The allocation can
   // only be assumed valid inside a paint() method; anywhere else, it
   // may be out-of-date.
   // 
   // An allocation does not incorporate the actor's scale or anchor point;
   // those transformations do not affect layout, only rendering.
   // 
   // The returned rectangle is in pixels.
   // <geom>: allocation geometry in pixels
   void get_allocation_geometry(AT0)(/*out*/ AT0 /*Geometry*/ geom) nothrow {
      clutter_actor_get_allocation_geometry(&this, UpCast!(Geometry*)(geom));
   }

   // VERSION: 0.6
   // Calculates the transformed coordinates of the four corners of the
   // actor in the plane of @ancestor. The returned vertices relate to
   // the #ClutterActorBox coordinates as follows:
   // <itemizedlist>
   // <listitem><para>@verts[0] contains (x1, y1)</para></listitem>
   // <listitem><para>@verts[1] contains (x2, y1)</para></listitem>
   // <listitem><para>@verts[2] contains (x1, y2)</para></listitem>
   // <listitem><para>@verts[3] contains (x2, y2)</para></listitem>
   // </itemizedlist>
   // 
   // If @ancestor is %NULL the ancestor will be the #ClutterStage. In
   // this case, the coordinates returned will be the coordinates on
   // the stage before the projection is applied. This is different from
   // the behaviour of clutter_actor_get_abs_allocation_vertices().
   // <ancestor>: A #ClutterActor to calculate the vertices against, or %NULL to use the #ClutterStage
   // <verts>: return location for an array of 4 #ClutterVertex in which to store the result
   void get_allocation_vertices(AT0)(AT0 /*Actor*/ ancestor, /*out*/ Vertex verts) nothrow {
      clutter_actor_get_allocation_vertices(&this, UpCast!(Actor*)(ancestor), verts);
   }

   // VERSION: 0.6
   // Gets the current anchor point of the @actor in pixels.
   // <anchor_x>: return location for the X coordinate of the anchor point
   // <anchor_y>: return location for the Y coordinate of the anchor point
   void get_anchor_point(AT0, AT1)(/*out*/ AT0 /*float*/ anchor_x, /*out*/ AT1 /*float*/ anchor_y) nothrow {
      clutter_actor_get_anchor_point(&this, UpCast!(float*)(anchor_x), UpCast!(float*)(anchor_y));
   }

   // VERSION: 1.0
   // Retrieves the anchor position expressed as a #ClutterGravity. If
   // the anchor point was specified using pixels or units this will
   // return %CLUTTER_GRAVITY_NONE.
   // RETURNS: the #ClutterGravity used by the anchor point
   Gravity get_anchor_point_gravity()() nothrow {
      return clutter_actor_get_anchor_point_gravity(&this);
   }

   // VERSION: 1.0
   // Retrieves the #ClutterAnimation used by @actor, if clutter_actor_animate()
   // has been called on @actor.
   // RETURNS: a #ClutterAnimation, or %NULL
   Animation* get_animation()() nothrow {
      return clutter_actor_get_animation(&this);
   }

   // VERSION: 1.10
   // Retrieves the color set using clutter_actor_set_background_color().
   // <color>: return location for a #ClutterColor
   void get_background_color(AT0)(/*out*/ AT0 /*Color*/ color) nothrow {
      clutter_actor_get_background_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.10
   // Retrieves the actor at the given @index_ inside the list of
   // children of @self.
   // RETURNS: a pointer to a #ClutterActor, or %NULL
   // <index_>: the position in the list of children
   Actor* get_child_at_index()(int index_) nothrow {
      return clutter_actor_get_child_at_index(&this, index_);
   }

   // VERSION: 1.10
   // Retrieves the list of children of @self.
   // 
   // allocated #GList of #ClutterActor<!-- -->s. Use g_list_free() when
   // done.
   // RETURNS: A newly
   GLib2.List* /*new container*/ get_children()() nothrow {
      return clutter_actor_get_children(&this);
   }

   // VERSION: 0.6
   // Gets the clip area for @self, if any is set
   // <xoff>: return location for the X offset of the clip rectangle, or %NULL
   // <yoff>: return location for the Y offset of the clip rectangle, or %NULL
   // <width>: return location for the width of the clip rectangle, or %NULL
   // <height>: return location for the height of the clip rectangle, or %NULL
   void get_clip(AT0, AT1, AT2, AT3)(/*out*/ AT0 /*float*/ xoff=null, /*out*/ AT1 /*float*/ yoff=null, /*out*/ AT2 /*float*/ width=null, /*out*/ AT3 /*float*/ height=null) nothrow {
      clutter_actor_get_clip(&this, UpCast!(float*)(xoff), UpCast!(float*)(yoff), UpCast!(float*)(width), UpCast!(float*)(height));
   }

   // VERSION: 1.4
   // Retrieves the value set using clutter_actor_set_clip_to_allocation()
   // RETURNS: %TRUE if the #ClutterActor is clipped to its allocation
   int get_clip_to_allocation()() nothrow {
      return clutter_actor_get_clip_to_allocation(&this);
   }

   // VERSION: 1.4
   // Retrieves the #ClutterConstraint with the given name in the list
   // of constraints applied to @self
   // 
   // name, or %NULL. The returned #ClutterConstraint is owned by the
   // actor and it should not be unreferenced directly
   // RETURNS: a #ClutterConstraint for the given
   // <name>: the name of the constraint to retrieve
   Constraint* get_constraint(AT0)(AT0 /*char*/ name) nothrow {
      return clutter_actor_get_constraint(&this, toCString!(char*)(name));
   }

   // VERSION: 1.4
   // Retrieves the list of constraints applied to @self
   // 
   // of the list of #ClutterConstraint<!-- -->s. The contents of the list are
   // owned by the #ClutterActor. Use g_list_free() to free the resources
   // allocated by the returned #GList
   // RETURNS: a copy
   GLib2.List* /*new container*/ get_constraints()() nothrow {
      return clutter_actor_get_constraints(&this);
   }

   // Retrieves the depth of @self.
   // RETURNS: the depth of the actor
   float get_depth()() nothrow {
      return clutter_actor_get_depth(&this);
   }

   // VERSION: 1.4
   // Retrieves the #ClutterEffect with the given name in the list
   // of effects applied to @self
   // 
   // name, or %NULL. The returned #ClutterEffect is owned by the
   // actor and it should not be unreferenced directly
   // RETURNS: a #ClutterEffect for the given
   // <name>: the name of the effect to retrieve
   Effect* get_effect(AT0)(AT0 /*char*/ name) nothrow {
      return clutter_actor_get_effect(&this, toCString!(char*)(name));
   }

   // VERSION: 1.4
   // Retrieves the #ClutterEffect<!-- -->s applied on @self, if any
   // 
   // of #ClutterEffect<!-- -->s, or %NULL. The elements of the returned
   // list are owned by Clutter and they should not be freed. You should
   // free the returned list using g_list_free() when done
   // RETURNS: a list
   GLib2.List* /*new container*/ get_effects()() nothrow {
      return clutter_actor_get_effects(&this);
   }

   // VERSION: 1.10
   // Retrieves the first child of @self.
   // 
   // The returned pointer is only valid until the scene graph changes; it
   // is not safe to modify the list of children of @self while iterating
   // it.
   // RETURNS: a pointer to a #ClutterActor, or %NULL
   Actor* get_first_child()() nothrow {
      return clutter_actor_get_first_child(&this);
   }

   // VERSION: 0.8
   // Checks whether an actor has a fixed position set (and will thus be
   // unaffected by any layout manager).
   // RETURNS: %TRUE if the fixed position is set on the actor
   int get_fixed_position_set()() nothrow {
      return clutter_actor_get_fixed_position_set(&this);
   }

   // VERSION: 1.0
   // Retrieves the flags set on @self
   // RETURNS: a bitwise or of #ClutterActorFlags or 0
   ActorFlags get_flags()() nothrow {
      return clutter_actor_get_flags(&this);
   }

   // DEPRECATED (v1.10) method: get_geometry - Use clutter_actor_get_position() and
   // Gets the size and position of an actor relative to its parent
   // actor. This is the same as calling clutter_actor_get_position() and
   // clutter_actor_get_size(). It tries to "do what you mean" and get the
   // requested size and position if the actor's allocation is invalid.
   // 
   // clutter_actor_get_size(), or clutter_actor_get_allocation_geometry()
   // instead.
   // <geometry>: A location to store actors #ClutterGeometry
   void get_geometry(AT0)(/*out*/ AT0 /*Geometry*/ geometry) nothrow {
      clutter_actor_get_geometry(&this, UpCast!(Geometry*)(geometry));
   }

   // VERSION: 0.6
   // DEPRECATED (v1.8) method: get_gid - The id is not used any longer.
   // Retrieves the unique id for @self.
   // RETURNS: Globally unique value for this object instance.
   uint get_gid()() nothrow {
      return clutter_actor_get_gid(&this);
   }

   // Retrieves the height of a #ClutterActor.
   // 
   // If the actor has a valid allocation, this function will return the
   // height of the allocated area given to the actor.
   // 
   // If the actor does not have a valid allocation, this function will
   // return the actor's natural height, that is the preferred height of
   // the actor.
   // 
   // If you care whether you get the preferred height or the height that
   // has been assigned to the actor, you should probably call a different
   // function like clutter_actor_get_allocation_box() to retrieve the
   // allocated size or clutter_actor_get_preferred_height() to retrieve the
   // preferred height.
   // 
   // If an actor has a fixed height, for instance a height that has been
   // assigned using clutter_actor_set_height(), the height returned will
   // be the same value.
   // RETURNS: the height of the actor, in pixels
   float get_height()() nothrow {
      return clutter_actor_get_height(&this);
   }

   // VERSION: 1.10
   // Retrieves the last child of @self.
   // 
   // The returned pointer is only valid until the scene graph changes; it
   // is not safe to modify the list of children of @self while iterating
   // it.
   // RETURNS: a pointer to a #ClutterActor, or %NULL
   Actor* get_last_child()() nothrow {
      return clutter_actor_get_last_child(&this);
   }

   // VERSION: 1.10
   // Retrieves the #ClutterLayoutManager used by @self.
   // 
   // or %NULL
   // RETURNS: a pointer to the #ClutterLayoutManager,
   LayoutManager* get_layout_manager()() nothrow {
      return clutter_actor_get_layout_manager(&this);
   }

   // VERSION: 1.10
   // Retrieves all the components of the margin of a #ClutterActor.
   // <margin>: return location for a #ClutterMargin
   void get_margin(AT0)(/*out*/ AT0 /*Margin*/ margin) nothrow {
      clutter_actor_get_margin(&this, UpCast!(Margin*)(margin));
   }

   // VERSION: 1.10
   // Retrieves the bottom margin of a #ClutterActor.
   // RETURNS: the bottom margin
   float get_margin_bottom()() nothrow {
      return clutter_actor_get_margin_bottom(&this);
   }

   // VERSION: 1.10
   // Retrieves the left margin of a #ClutterActor.
   // RETURNS: the left margin
   float get_margin_left()() nothrow {
      return clutter_actor_get_margin_left(&this);
   }

   // VERSION: 1.10
   // Retrieves the right margin of a #ClutterActor.
   // RETURNS: the right margin
   float get_margin_right()() nothrow {
      return clutter_actor_get_margin_right(&this);
   }

   // VERSION: 1.10
   // Retrieves the top margin of a #ClutterActor.
   // RETURNS: the top margin
   float get_margin_top()() nothrow {
      return clutter_actor_get_margin_top(&this);
   }

   // VERSION: 1.10
   // Retrieves the number of children of @self.
   // RETURNS: the number of children of an actor
   int get_n_children()() nothrow {
      return clutter_actor_get_n_children(&this);
   }

   // Retrieves the name of @self.
   // 
   // owned by the actor and should not be modified or freed.
   // RETURNS: the name of the actor, or %NULL. The returned string is
   char* get_name()() nothrow {
      return clutter_actor_get_name(&this);
   }

   // VERSION: 1.10
   // Retrieves the sibling of @self that comes after it in the list
   // of children of @self's parent.
   // 
   // The returned pointer is only valid until the scene graph changes; it
   // is not safe to modify the list of children of @self while iterating
   // it.
   // RETURNS: a pointer to a #ClutterActor, or %NULL
   Actor* get_next_sibling()() nothrow {
      return clutter_actor_get_next_sibling(&this);
   }

   // VERSION: 1.8
   // Retrieves whether to redirect the actor to an offscreen buffer, as
   // set by clutter_actor_set_offscreen_redirect().
   // RETURNS: the value of the offscreen-redirect property of the actor
   OffscreenRedirect get_offscreen_redirect()() nothrow {
      return clutter_actor_get_offscreen_redirect(&this);
   }

   // Retrieves the opacity value of an actor, as set by
   // clutter_actor_set_opacity().
   // 
   // For retrieving the absolute opacity of the actor inside a paint
   // virtual function, see clutter_actor_get_paint_opacity().
   // RETURNS: the opacity of the actor
   ubyte get_opacity()() nothrow {
      return clutter_actor_get_opacity(&this);
   }

   // VERSION: 1.6
   // Retrieves the paint volume of the passed #ClutterActor, and
   // transforms it into a 2D bounding box in stage coordinates.
   // 
   // This function is useful to determine the on screen area occupied by
   // the actor. The box is only an approximation and may often be
   // considerably larger due to the optimizations used to calculate the
   // box. The box is never smaller though, so it can reliably be used
   // for culling.
   // 
   // There are times when a 2D paint box can't be determined, e.g.
   // because the actor isn't yet parented under a stage or because
   // the actor is unable to determine a paint volume.
   // 
   // %FALSE.
   // RETURNS: %TRUE if a 2D paint box could be determined, else
   // <box>: return location for a #ClutterActorBox
   int get_paint_box(AT0)(/*out*/ AT0 /*ActorBox*/ box) nothrow {
      return clutter_actor_get_paint_box(&this, UpCast!(ActorBox*)(box));
   }

   // VERSION: 0.8
   // Retrieves the absolute opacity of the actor, as it appears on the stage.
   // 
   // This function traverses the hierarchy chain and composites the opacity of
   // the actor with that of its parents.
   // 
   // This function is intended for subclasses to use in the paint virtual
   // function, to paint themselves with the correct opacity.
   // RETURNS: The actor opacity value.
   ubyte get_paint_opacity()() nothrow {
      return clutter_actor_get_paint_opacity(&this);
   }

   // VERSION: 0.8.4
   // Retrieves the 'paint' visibility of an actor recursively checking for non
   // visible parents.
   // 
   // This is by definition the same as %CLUTTER_ACTOR_IS_MAPPED.
   // RETURNS: %TRUE if the actor is visibile and will be painted.
   int get_paint_visibility()() nothrow {
      return clutter_actor_get_paint_visibility(&this);
   }

   // VERSION: 1.6
   // Retrieves the paint volume of the passed #ClutterActor, or %NULL
   // when a paint volume can't be determined.
   // 
   // The paint volume is defined as the 3D space occupied by an actor
   // when being painted.
   // 
   // This function will call the <function>get_paint_volume()</function>
   // virtual function of the #ClutterActor class. Sub-classes of #ClutterActor
   // should not usually care about overriding the default implementation,
   // unless they are, for instance: painting outside their allocation, or
   // actors with a depth factor (not in terms of #ClutterActor:depth but real
   // 3D depth).
   // 
   // <note>2D actors overriding <function>get_paint_volume()</function>
   // ensure their volume has a depth of 0. (This will be true so long as
   // you don't call clutter_paint_volume_set_depth().)</note>
   // 
   // or %NULL if no volume could be determined.
   // RETURNS: a pointer to a #ClutterPaintVolume
   PaintVolume* get_paint_volume()() nothrow {
      return clutter_actor_get_paint_volume(&this);
   }

   // VERSION: 1.0
   // Retrieves the #PangoContext for @self. The actor's #PangoContext
   // is already configured using the appropriate font map, resolution
   // and font options.
   // 
   // Unlike clutter_actor_create_pango_context(), this context is owend
   // by the #ClutterActor and it will be updated each time the options
   // stored by the #ClutterBackend change.
   // 
   // You can use the returned #PangoContext to create a #PangoLayout
   // and render text using cogl_pango_render_layout() to reuse the
   // glyphs cache also used by Clutter.
   // 
   // The returned #PangoContext is owned by the actor and should not be
   // unreferenced by the application code
   // RETURNS: the #PangoContext for a #ClutterActor.
   Pango.Context* get_pango_context()() nothrow {
      return clutter_actor_get_pango_context(&this);
   }

   // Retrieves the parent of @self.
   // 
   // if no parent is set
   // RETURNS: The #ClutterActor parent, or %NULL
   Actor* get_parent()() nothrow {
      return clutter_actor_get_parent(&this);
   }

   // VERSION: 0.6
   // This function tries to "do what you mean" and tell you where the
   // actor is, prior to any transformations. Retrieves the fixed
   // position of an actor in pixels, if one has been set; otherwise, if
   // the allocation is valid, returns the actor's allocated position;
   // otherwise, returns 0,0.
   // 
   // The returned position is in pixels.
   // <x>: return location for the X coordinate, or %NULL
   // <y>: return location for the Y coordinate, or %NULL
   void get_position(AT0, AT1)(/*out*/ AT0 /*float*/ x=null, /*out*/ AT1 /*float*/ y=null) nothrow {
      clutter_actor_get_position(&this, UpCast!(float*)(x), UpCast!(float*)(y));
   }

   // VERSION: 0.8
   // Computes the requested minimum and natural heights for an actor,
   // or if they are already computed, returns the cached values.
   // 
   // An actor may not get its request - depending on the layout
   // manager that's in effect.
   // 
   // A request should not incorporate the actor's scale or anchor point;
   // those transformations do not affect layout, only rendering.
   // <for_width>: available width to assume in computing desired height, or a negative value to indicate that no width is defined
   // <min_height_p>: return location for minimum height, or %NULL
   // <natural_height_p>: return location for natural height, or %NULL
   void get_preferred_height(AT0, AT1)(float for_width, /*out*/ AT0 /*float*/ min_height_p=null, /*out*/ AT1 /*float*/ natural_height_p=null) nothrow {
      clutter_actor_get_preferred_height(&this, for_width, UpCast!(float*)(min_height_p), UpCast!(float*)(natural_height_p));
   }

   // VERSION: 0.8
   // Computes the preferred minimum and natural size of an actor, taking into
   // account the actor's geometry management (either height-for-width
   // or width-for-height).
   // 
   // The width and height used to compute the preferred height and preferred
   // width are the actor's natural ones.
   // 
   // If you need to control the height for the preferred width, or the width for
   // the preferred height, you should use clutter_actor_get_preferred_width()
   // and clutter_actor_get_preferred_height(), and check the actor's preferred
   // geometry management using the #ClutterActor:request-mode property.
   // <min_width_p>: return location for the minimum width, or %NULL
   // <min_height_p>: return location for the minimum height, or %NULL
   // <natural_width_p>: return location for the natural width, or %NULL
   // <natural_height_p>: return location for the natural height, or %NULL
   void get_preferred_size(AT0, AT1, AT2, AT3)(/*out*/ AT0 /*float*/ min_width_p=null, /*out*/ AT1 /*float*/ min_height_p=null, /*out*/ AT2 /*float*/ natural_width_p=null, /*out*/ AT3 /*float*/ natural_height_p=null) nothrow {
      clutter_actor_get_preferred_size(&this, UpCast!(float*)(min_width_p), UpCast!(float*)(min_height_p), UpCast!(float*)(natural_width_p), UpCast!(float*)(natural_height_p));
   }

   // VERSION: 0.8
   // Computes the requested minimum and natural widths for an actor,
   // optionally depending on the specified height, or if they are
   // already computed, returns the cached values.
   // 
   // An actor may not get its request - depending on the layout
   // manager that's in effect.
   // 
   // A request should not incorporate the actor's scale or anchor point;
   // those transformations do not affect layout, only rendering.
   // <for_height>: available height when computing the preferred width, or a negative value to indicate that no height is defined
   // <min_width_p>: return location for minimum width, or %NULL
   // <natural_width_p>: return location for the natural width, or %NULL
   void get_preferred_width(AT0, AT1)(float for_height, /*out*/ AT0 /*float*/ min_width_p=null, /*out*/ AT1 /*float*/ natural_width_p=null) nothrow {
      clutter_actor_get_preferred_width(&this, for_height, UpCast!(float*)(min_width_p), UpCast!(float*)(natural_width_p));
   }

   // VERSION: 1.10
   // Retrieves the sibling of @self that comes before it in the list
   // of children of @self's parent.
   // 
   // The returned pointer is only valid until the scene graph changes; it
   // is not safe to modify the list of children of @self while iterating
   // it.
   // RETURNS: a pointer to a #ClutterActor, or %NULL
   Actor* get_previous_sibling()() nothrow {
      return clutter_actor_get_previous_sibling(&this);
   }

   // VERSION: 0.6
   // Checks whether @actor is marked as reactive.
   // RETURNS: %TRUE if the actor is reactive
   int get_reactive()() nothrow {
      return clutter_actor_get_reactive(&this);
   }

   // VERSION: 1.2
   // Retrieves the geometry request mode of @self
   // RETURNS: the request mode for the actor
   RequestMode get_request_mode()() nothrow {
      return clutter_actor_get_request_mode(&this);
   }

   // VERSION: 0.8
   // Retrieves the angle and center of rotation on the given axis,
   // set using clutter_actor_set_rotation().
   // RETURNS: the angle of rotation
   // <axis>: the axis of rotation
   // <x>: return value for the X coordinate of the center of rotation
   // <y>: return value for the Y coordinate of the center of rotation
   // <z>: return value for the Z coordinate of the center of rotation
   double get_rotation(AT0, AT1, AT2)(RotateAxis axis, /*out*/ AT0 /*float*/ x, /*out*/ AT1 /*float*/ y, /*out*/ AT2 /*float*/ z) nothrow {
      return clutter_actor_get_rotation(&this, axis, UpCast!(float*)(x), UpCast!(float*)(y), UpCast!(float*)(z));
   }

   // VERSION: 0.2
   // Retrieves an actors scale factors.
   // <scale_x>: Location to store horizonal scale factor, or %NULL.
   // <scale_y>: Location to store vertical scale factor, or %NULL.
   void get_scale(AT0, AT1)(/*out*/ AT0 /*double*/ scale_x=null, /*out*/ AT1 /*double*/ scale_y=null) nothrow {
      clutter_actor_get_scale(&this, UpCast!(double*)(scale_x), UpCast!(double*)(scale_y));
   }

   // VERSION: 1.0
   // Retrieves the scale center coordinate in pixels relative to the top
   // left corner of the actor. If the scale center was specified using a
   // #ClutterGravity this will calculate the pixel offset using the
   // current size of the actor.
   // <center_x>: Location to store the X position of the scale center, or %NULL.
   // <center_y>: Location to store the Y position of the scale center, or %NULL.
   void get_scale_center(AT0, AT1)(/*out*/ AT0 /*float*/ center_x=null, /*out*/ AT1 /*float*/ center_y=null) nothrow {
      clutter_actor_get_scale_center(&this, UpCast!(float*)(center_x), UpCast!(float*)(center_y));
   }

   // VERSION: 1.0
   // Retrieves the scale center as a compass direction. If the scale
   // center was specified in pixels or units this will return
   // %CLUTTER_GRAVITY_NONE.
   // RETURNS: the scale gravity
   Gravity get_scale_gravity()() nothrow {
      return clutter_actor_get_scale_gravity(&this);
   }

   // VERSION: 0.6
   // DEPRECATED (v1.8) method: get_shader - Use clutter_actor_get_effect() instead.
   // Queries the currently set #ClutterShader on @self.
   // 
   // or %NULL if no shader is set.
   // RETURNS: The currently set #ClutterShader
   Shader* get_shader()() nothrow {
      return clutter_actor_get_shader(&this);
   }

   // VERSION: 0.2
   // This function tries to "do what you mean" and return
   // the size an actor will have. If the actor has a valid
   // allocation, the allocation will be returned; otherwise,
   // the actors natural size request will be returned.
   // 
   // If you care whether you get the request vs. the allocation, you
   // should probably call a different function like
   // clutter_actor_get_allocation_box() or
   // clutter_actor_get_preferred_width().
   // <width>: return location for the width, or %NULL.
   // <height>: return location for the height, or %NULL.
   void get_size(AT0, AT1)(/*out*/ AT0 /*float*/ width=null, /*out*/ AT1 /*float*/ height=null) nothrow {
      clutter_actor_get_size(&this, UpCast!(float*)(width), UpCast!(float*)(height));
   }

   // VERSION: 0.8
   // Retrieves the #ClutterStage where @actor is contained.
   // 
   // containing the actor, or %NULL
   // RETURNS: the stage
   Clutter.Stage* get_stage()() nothrow {
      return clutter_actor_get_stage(&this);
   }

   // VERSION: 1.2
   // Retrieves the value set using clutter_actor_set_text_direction()
   // 
   // If no text direction has been previously set, the default text
   // direction, as returned by clutter_get_default_text_direction(), will
   // be returned instead
   // RETURNS: the #ClutterTextDirection for the actor
   TextDirection get_text_direction()() nothrow {
      return clutter_actor_get_text_direction(&this);
   }

   // VERSION: 1.0
   // Retrieves the transformations applied to @self relative to its
   // parent.
   // <matrix>: the return location for a #CoglMatrix
   void get_transformation_matrix(AT0)(/*out*/ AT0 /*Cogl.Matrix*/ matrix) nothrow {
      clutter_actor_get_transformation_matrix(&this, UpCast!(Cogl.Matrix*)(matrix));
   }

   // VERSION: 1.6
   // Retrieves the 3D paint volume of an actor like
   // clutter_actor_get_paint_volume() does (Please refer to the
   // documentation of clutter_actor_get_paint_volume() for more
   // details.) and it additionally transforms the paint volume into the
   // coordinate space of @relative_to_ancestor. (Or the stage if %NULL
   // is passed for @relative_to_ancestor)
   // 
   // This can be used by containers that base their paint volume on
   // the volume of their children. Such containers can query the
   // transformed paint volume of all of its children and union them
   // together using clutter_paint_volume_union().
   // 
   // or %NULL if no volume could be determined.
   // RETURNS: a pointer to a #ClutterPaintVolume
   // <relative_to_ancestor>: A #ClutterActor that is an ancestor of @self (or %NULL for the stage)
   PaintVolume* get_transformed_paint_volume(AT0)(AT0 /*Actor*/ relative_to_ancestor) nothrow {
      return clutter_actor_get_transformed_paint_volume(&this, UpCast!(Actor*)(relative_to_ancestor));
   }

   // VERSION: 0.8
   // Gets the absolute position of an actor, in pixels relative to the stage.
   // <x>: return location for the X coordinate, or %NULL
   // <y>: return location for the Y coordinate, or %NULL
   void get_transformed_position(AT0, AT1)(/*out*/ AT0 /*float*/ x=null, /*out*/ AT1 /*float*/ y=null) nothrow {
      clutter_actor_get_transformed_position(&this, UpCast!(float*)(x), UpCast!(float*)(y));
   }

   // VERSION: 0.8
   // Gets the absolute size of an actor in pixels, taking into account the
   // scaling factors.
   // 
   // If the actor has a valid allocation, the allocated size will be used.
   // If the actor has not a valid allocation then the preferred size will
   // be transformed and returned.
   // 
   // If you want the transformed allocation, see
   // clutter_actor_get_abs_allocation_vertices() instead.
   // 
   // <note>When the actor (or one of its ancestors) is rotated around the
   // X or Y axis, it no longer appears as on the stage as a rectangle, but
   // as a generic quadrangle; in that case this function returns the size
   // of the smallest rectangle that encapsulates the entire quad. Please
   // note that in this case no assumptions can be made about the relative
   // position of this envelope to the absolute position of the actor, as
   // returned by clutter_actor_get_transformed_position(); if you need this
   // information, you need to use clutter_actor_get_abs_allocation_vertices()
   // to get the coords of the actual quadrangle.</note>
   // <width>: return location for the width, or %NULL
   // <height>: return location for the height, or %NULL
   void get_transformed_size(AT0, AT1)(/*out*/ AT0 /*float*/ width=null, /*out*/ AT1 /*float*/ height=null) nothrow {
      clutter_actor_get_transformed_size(&this, UpCast!(float*)(width), UpCast!(float*)(height));
   }

   // Retrieves the width of a #ClutterActor.
   // 
   // If the actor has a valid allocation, this function will return the
   // width of the allocated area given to the actor.
   // 
   // If the actor does not have a valid allocation, this function will
   // return the actor's natural width, that is the preferred width of
   // the actor.
   // 
   // If you care whether you get the preferred width or the width that
   // has been assigned to the actor, you should probably call a different
   // function like clutter_actor_get_allocation_box() to retrieve the
   // allocated size or clutter_actor_get_preferred_width() to retrieve the
   // preferred width.
   // 
   // If an actor has a fixed width, for instance a width that has been
   // assigned using clutter_actor_set_width(), the width returned will
   // be the same value.
   // RETURNS: the width of the actor, in pixels
   float get_width()() nothrow {
      return clutter_actor_get_width(&this);
   }

   // Retrieves the X coordinate of a #ClutterActor.
   // 
   // This function tries to "do what you mean", by returning the
   // correct value depending on the actor's state.
   // 
   // If the actor has a valid allocation, this function will return
   // the X coordinate of the origin of the allocation box.
   // 
   // If the actor has any fixed coordinate set using clutter_actor_set_x(),
   // clutter_actor_set_position() or clutter_actor_set_geometry(), this
   // function will return that coordinate.
   // 
   // If both the allocation and a fixed position are missing, this function
   // will return 0.
   // 
   // transformation (i.e. scaling, rotation)
   // RETURNS: the X coordinate, in pixels, ignoring any
   float get_x()() nothrow {
      return clutter_actor_get_x(&this);
   }

   // VERSION: 1.10
   // Retrieves the horizontal alignment policy set using
   // clutter_actor_set_x_align().
   // RETURNS: the horizontal alignment policy.
   ActorAlign get_x_align()() nothrow {
      return clutter_actor_get_x_align(&this);
   }

   // Retrieves the Y coordinate of a #ClutterActor.
   // 
   // This function tries to "do what you mean", by returning the
   // correct value depending on the actor's state.
   // 
   // If the actor has a valid allocation, this function will return
   // the Y coordinate of the origin of the allocation box.
   // 
   // If the actor has any fixed coordinate set using clutter_actor_set_y(),
   // clutter_actor_set_position() or clutter_actor_set_geometry(), this
   // function will return that coordinate.
   // 
   // If both the allocation and a fixed position are missing, this function
   // will return 0.
   // 
   // transformation (i.e. scaling, rotation)
   // RETURNS: the Y coordinate, in pixels, ignoring any
   float get_y()() nothrow {
      return clutter_actor_get_y(&this);
   }

   // VERSION: 1.10
   // Retrieves the vertical alignment policy set using
   // clutter_actor_set_y_align().
   // RETURNS: the vertical alignment policy.
   ActorAlign get_y_align()() nothrow {
      return clutter_actor_get_y_align(&this);
   }

   // VERSION: 1.0
   // Retrieves the center for the rotation around the Z axis as a
   // compass direction. If the center was specified in pixels or units
   // this will return %CLUTTER_GRAVITY_NONE.
   // RETURNS: the Z rotation center
   Gravity get_z_rotation_gravity()() nothrow {
      return clutter_actor_get_z_rotation_gravity(&this);
   }

   // VERSION: 1.0
   // Sets the key focus of the #ClutterStage including @self
   // to this #ClutterActor.
   void grab_key_focus()() nothrow {
      clutter_actor_grab_key_focus(&this);
   }

   // VERSION: 1.10
   // Returns whether the actor has any actions applied.
   // 
   // %FALSE otherwise
   // RETURNS: %TRUE if the actor has any actions,
   int has_actions()() nothrow {
      return clutter_actor_has_actions(&this);
   }

   // VERSION: 1.4
   // Checks if the actor has an up-to-date allocation assigned to
   // it. This means that the actor should have an allocation: it's
   // visible and has a parent. It also means that there is no
   // outstanding relayout request in progress for the actor or its
   // children (There might be other outstanding layout requests in
   // progress that will cause the actor to get a new allocation
   // when the stage is laid out, however).
   // 
   // If this function returns %FALSE, then the actor will normally
   // be allocated before it is next drawn on the screen.
   // RETURNS: %TRUE if the actor has an up-to-date allocation
   int has_allocation()() nothrow {
      return clutter_actor_has_allocation(&this);
   }

   // VERSION: 0.1.1
   // Determines whether the actor has a clip area set or not.
   // RETURNS: %TRUE if the actor has a clip area set.
   int has_clip()() nothrow {
      return clutter_actor_has_clip(&this);
   }

   // VERSION: 1.10
   // Returns whether the actor has any constraints applied.
   // 
   // %FALSE otherwise
   // RETURNS: %TRUE if the actor has any constraints,
   int has_constraints()() nothrow {
      return clutter_actor_has_constraints(&this);
   }

   // VERSION: 1.10
   // Returns whether the actor has any effects applied.
   // 
   // %FALSE otherwise
   // RETURNS: %TRUE if the actor has any effects,
   int has_effects()() nothrow {
      return clutter_actor_has_effects(&this);
   }

   // VERSION: 1.4
   // Checks whether @self is the #ClutterActor that has key focus
   // RETURNS: %TRUE if the actor has key focus, and %FALSE otherwise
   int has_key_focus()() nothrow {
      return clutter_actor_has_key_focus(&this);
   }

   // VERSION: 1.8
   // Asks the actor's implementation whether it may contain overlapping
   // primitives.
   // 
   // For example; Clutter may use this to determine whether the painting
   // should be redirected to an offscreen buffer to correctly implement
   // the opacity property.
   // 
   // Custom actors can override the default response by implementing the
   // #ClutterActor <function>has_overlaps</function> virtual function. See
   // clutter_actor_set_offscreen_redirect() for more information.
   // 
   // %FALSE otherwise
   // RETURNS: %TRUE if the actor may have overlapping primitives, and
   int has_overlaps()() nothrow {
      return clutter_actor_has_overlaps(&this);
   }

   // VERSION: 1.2
   // Checks whether an actor contains the pointer of a
   // #ClutterInputDevice
   // 
   // %FALSE otherwise
   // RETURNS: %TRUE if the actor contains the pointer, and
   int has_pointer()() nothrow {
      return clutter_actor_has_pointer(&this);
   }

   // Flags an actor to be hidden. A hidden actor will not be
   // rendered on the stage.
   // 
   // Actors are visible by default.
   // 
   // If this function is called on an actor without a parent, the
   // #ClutterActor:show-on-set-parent property will be set to %FALSE
   // as a side-effect.
   void hide()() nothrow {
      clutter_actor_hide(&this);
   }

   // VERSION: 0.2
   // DEPRECATED (v1.10) method: hide_all - Using clutter_actor_hide() on the actor will
   // Calls clutter_actor_hide() on all child actors (if any).
   // 
   // 
   // prevent its children from being painted as well.
   void hide_all()() nothrow {
      clutter_actor_hide_all(&this);
   }

   // VERSION: 1.10
   // Inserts @child into the list of children of @self, above another
   // child of @self or, if @sibling is %NULL, above all the children
   // of @self.
   // 
   // This function will acquire a reference on @child that will only
   // be released when calling clutter_actor_remove_child().
   // 
   // This function will not take into consideration the #ClutterActor:depth
   // of @child.
   // 
   // This function will emit the #ClutterContainer::actor-added signal
   // on @self.
   // <child>: a #ClutterActor
   // <sibling>: a child of @self, or %NULL
   void insert_child_above(AT0, AT1)(AT0 /*Actor*/ child, AT1 /*Actor*/ sibling=null) nothrow {
      clutter_actor_insert_child_above(&this, UpCast!(Actor*)(child), UpCast!(Actor*)(sibling));
   }

   // VERSION: 1.10
   // Inserts @child into the list of children of @self, using the
   // given @index_.
   // 
   // This function will acquire a reference on @child that will only
   // be released when calling clutter_actor_remove_child().
   // 
   // This function will not take into consideration the #ClutterActor:depth
   // of @child.
   // 
   // This function will emit the #ClutterContainer::actor-added signal
   // on @self.
   // <child>: a #ClutterActor
   // <index_>: the index
   void insert_child_at_index(AT0)(AT0 /*Actor*/ child, int index_) nothrow {
      clutter_actor_insert_child_at_index(&this, UpCast!(Actor*)(child), index_);
   }

   // VERSION: 1.10
   // Inserts @child into the list of children of @self, below another
   // child of @self or, if @sibling is %NULL, below all the children
   // of @self.
   // 
   // This function will acquire a reference on @child that will only
   // be released when calling clutter_actor_remove_child().
   // 
   // This function will not take into consideration the #ClutterActor:depth
   // of @child.
   // 
   // This function will emit the #ClutterContainer::actor-added signal
   // on @self.
   // <child>: a #ClutterActor
   // <sibling>: a child of @self, or %NULL
   void insert_child_below(AT0, AT1)(AT0 /*Actor*/ child, AT1 /*Actor*/ sibling=null) nothrow {
      clutter_actor_insert_child_below(&this, UpCast!(Actor*)(child), UpCast!(Actor*)(sibling));
   }

   // VERSION: 1.0
   // Checks whether @self is being currently painted by a #ClutterClone
   // 
   // This function is useful only inside the ::paint virtual function
   // implementations or within handlers for the #ClutterActor::paint
   // signal
   // 
   // This function should not be used by applications
   // 
   // by a #ClutterClone, and %FALSE otherwise
   // RETURNS: %TRUE if the #ClutterActor is currently being painted
   int is_in_clone_paint()() nothrow {
      return clutter_actor_is_in_clone_paint(&this);
   }

   // VERSION: 0.6
   // Checks whether any rotation is applied to the actor.
   // RETURNS: %TRUE if the actor is rotated.
   int is_rotated()() nothrow {
      return clutter_actor_is_rotated(&this);
   }

   // VERSION: 0.6
   // Checks whether the actor is scaled in either dimension.
   // RETURNS: %TRUE if the actor is scaled.
   int is_scaled()() nothrow {
      return clutter_actor_is_scaled(&this);
   }

   // DEPRECATED (v1.10) method: lower - Use clutter_actor_set_child_below_sibling() instead.
   // Puts @self below @above.
   // 
   // Both actors must have the same parent, and the parent must implement
   // the #ClutterContainer interface.
   // 
   // This function calls clutter_container_lower_child() internally.
   // <above>: A #ClutterActor to lower below
   void lower(AT0)(AT0 /*Actor*/ above=null) nothrow {
      clutter_actor_lower(&this, UpCast!(Actor*)(above));
   }

   // DEPRECATED (v1.10) method: lower_bottom - Use clutter_actor_set_child_below_sibling() with
   // Lowers @self to the bottom.
   // 
   // This function calls clutter_actor_lower() internally.
   // 
   // a %NULL sibling, instead.
   void lower_bottom()() nothrow {
      clutter_actor_lower_bottom(&this);
   }

   // VERSION: 1.0
   // Sets the %CLUTTER_ACTOR_MAPPED flag on the actor and possibly maps
   // and realizes its children if they are visible. Does nothing if the
   // actor is not visible.
   // 
   // Calling this function is strongly disencouraged: the default
   // implementation of #ClutterActorClass.map() will map all the children
   // of an actor when mapping its parent.
   // 
   // When overriding map, it is mandatory to chain up to the parent
   // implementation.
   void map()() nothrow {
      clutter_actor_map(&this);
   }

   // VERSION: 0.6
   // Sets an anchor point for the actor, and adjusts the actor postion so that
   // the relative position of the actor toward its parent remains the same.
   // <anchor_x>: X coordinate of the anchor point
   // <anchor_y>: Y coordinate of the anchor point
   void move_anchor_point()(float anchor_x, float anchor_y) nothrow {
      clutter_actor_move_anchor_point(&this, anchor_x, anchor_y);
   }

   // VERSION: 0.6
   // Sets an anchor point on the actor based on the given gravity, adjusting the
   // actor postion so that its relative position within its parent remains
   // unchanged.
   // 
   // Since version 1.0 the anchor point will be stored as a gravity so
   // that if the actor changes size then the anchor point will move. For
   // example, if you set the anchor point to %CLUTTER_GRAVITY_SOUTH_EAST
   // and later double the size of the actor, the anchor point will move
   // to the bottom right.
   // <gravity>: #ClutterGravity.
   void move_anchor_point_from_gravity()(Gravity gravity) nothrow {
      clutter_actor_move_anchor_point_from_gravity(&this, gravity);
   }

   // VERSION: 0.2
   // Moves an actor by the specified distance relative to its current
   // position in pixels.
   // 
   // This function modifies the fixed position of an actor and thus removes
   // it from any layout management. Another way to move an actor is with an
   // anchor point, see clutter_actor_set_anchor_point().
   // <dx>: Distance to move Actor on X axis.
   // <dy>: Distance to move Actor on Y axis.
   void move_by()(float dx, float dy) nothrow {
      clutter_actor_move_by(&this, dx, dy);
   }

   // Renders the actor to display.
   // 
   // This function should not be called directly by applications.
   // Call clutter_actor_queue_redraw() to queue paints, instead.
   // 
   // This function is context-aware, and will either cause a
   // regular paint or a pick paint.
   // 
   // This function will emit the #ClutterActor::paint signal or
   // the #ClutterActor::pick signal, depending on the context.
   // 
   // This function does not paint the actor if the actor is set to 0,
   // unless it is performing a pick paint.
   void paint()() nothrow {
      clutter_actor_paint(&this);
   }

   // VERSION: 1.2
   // DEPRECATED (v1.10) method: pop_internal - All children of an actor are accessible through
   // Disables the effects of clutter_actor_push_internal().
   // 
   // 
   // the #ClutterActor API. This function is only useful for legacy
   // containers overriding the default implementation of the
   // #ClutterContainer interface.
   void pop_internal()() nothrow {
      clutter_actor_pop_internal(&this);
   }

   // VERSION: 1.2
   // DEPRECATED (v1.10) method: push_internal - All children of an actor are accessible through
   // Should be used by actors implementing the #ClutterContainer and with
   // internal children added through clutter_actor_set_parent(), for instance:
   // 
   // |[
   // static void
   // my_actor_init (MyActor *self)
   // {
   // self->priv = SELF_ACTOR_GET_PRIVATE (self);
   // 
   // clutter_actor_push_internal (CLUTTER_ACTOR (self));
   // 
   // /&ast; calling clutter_actor_set_parent() now will result in
   // &ast; the internal flag being set on a child of MyActor
   // &ast;/
   // 
   // /&ast; internal child - a background texture &ast;/
   // self->priv->background_tex = clutter_texture_new ();
   // clutter_actor_set_parent (self->priv->background_tex,
   // CLUTTER_ACTOR (self));
   // 
   // /&ast; internal child - a label &ast;/
   // self->priv->label = clutter_text_new ();
   // clutter_actor_set_parent (self->priv->label,
   // CLUTTER_ACTOR (self));
   // 
   // clutter_actor_pop_internal (CLUTTER_ACTOR (self));
   // 
   // /&ast; calling clutter_actor_set_parent() now will not result in
   // &ast; the internal flag being set on a child of MyActor
   // &ast;/
   // }
   // ]|
   // 
   // This function will be used by Clutter to toggle an "internal child"
   // flag whenever clutter_actor_set_parent() is called; internal children
   // are handled differently by Clutter, specifically when destroying their
   // parent.
   // 
   // Call clutter_actor_pop_internal() when you finished adding internal
   // children.
   // 
   // Nested calls to clutter_actor_push_internal() are allowed, but each
   // one must by followed by a clutter_actor_pop_internal() call.
   // 
   // 
   // the #ClutterActor API, and #ClutterActor implements the
   // #ClutterContainer interface, so this function is only useful
   // for legacy containers overriding the default implementation.
   void push_internal()() nothrow {
      clutter_actor_push_internal(&this);
   }

   // Queues up a redraw of an actor and any children. The redraw occurs
   // once the main loop becomes idle (after the current batch of events
   // has been processed, roughly).
   // 
   // Applications rarely need to call this, as redraws are handled
   // automatically by modification functions.
   // 
   // This function will not do anything if @self is not visible, or
   // if the actor is inside an invisible part of the scenegraph.
   // 
   // Also be aware that painting is a NOP for actors with an opacity of
   // 0
   // 
   // When you are implementing a custom actor you must queue a redraw
   // whenever some private state changes that will affect painting or
   // picking of your actor.
   void queue_redraw()() nothrow {
      clutter_actor_queue_redraw(&this);
   }

   // VERSION: 1.10
   // Queues a redraw on @self limited to a specific, actor-relative
   // rectangular area.
   // 
   // If @clip is %NULL this function is equivalent to
   // clutter_actor_queue_redraw().
   // <clip>: a rectangular clip region, or %NULL
   void queue_redraw_with_clip(AT0)(AT0 /*cairo.RectangleInt*/ clip=null) nothrow {
      clutter_actor_queue_redraw_with_clip(&this, UpCast!(cairo.RectangleInt*)(clip));
   }

   // VERSION: 0.8
   // Indicates that the actor's size request or other layout-affecting
   // properties may have changed. This function is used inside #ClutterActor
   // subclass implementations, not by applications directly.
   // 
   // Queueing a new layout automatically queues a redraw as well.
   void queue_relayout()() nothrow {
      clutter_actor_queue_relayout(&this);
   }

   // DEPRECATED (v1.10) method: raise - Use clutter_actor_set_child_above_sibling() instead.
   // Puts @self above @below.
   // 
   // Both actors must have the same parent, and the parent must implement
   // the #ClutterContainer interface
   // 
   // This function calls clutter_container_raise_child() internally.
   // <below>: A #ClutterActor to raise above.
   void raise(AT0)(AT0 /*Actor*/ below=null) nothrow {
      clutter_actor_raise(&this, UpCast!(Actor*)(below));
   }

   // DEPRECATED (v1.10) method: raise_top - Use clutter_actor_set_child_above_sibling() with
   // Raises @self to the top.
   // 
   // This function calls clutter_actor_raise() internally.
   // 
   // a %NULL sibling, instead.
   void raise_top()() nothrow {
      clutter_actor_raise_top(&this);
   }

   // Realization informs the actor that it is attached to a stage. It
   // can use this to allocate resources if it wanted to delay allocation
   // until it would be rendered. However it is perfectly acceptable for
   // an actor to create resources before being realized because Clutter
   // only ever has a single rendering context so that actor is free to
   // be moved from one stage to another.
   // 
   // This function does nothing if the actor is already realized.
   // 
   // Because a realized actor must have realized parent actors, calling
   // clutter_actor_realize() will also realize all parents of the actor.
   // 
   // This function does not realize child actors, except in the special
   // case that realizing the stage, when the stage is visible, will
   // suddenly map (and thus realize) the children of the stage.
   void realize()() nothrow {
      clutter_actor_realize(&this);
   }

   // VERSION: 1.4
   // Removes @action from the list of actions applied to @self
   // 
   // The reference held by @self on the #ClutterAction will be released
   // <action>: a #ClutterAction
   void remove_action(AT0)(AT0 /*Action*/ action) nothrow {
      clutter_actor_remove_action(&this, UpCast!(Action*)(action));
   }

   // VERSION: 1.4
   // Removes the #ClutterAction with the given name from the list
   // of actions applied to @self
   // <name>: the name of the action to remove
   void remove_action_by_name(AT0)(AT0 /*char*/ name) nothrow {
      clutter_actor_remove_action_by_name(&this, toCString!(char*)(name));
   }

   // VERSION: 1.10
   // Removes all children of @self.
   // 
   // This function releases the reference added by inserting a child actor
   // in the list of children of @self.
   void remove_all_children()() nothrow {
      clutter_actor_remove_all_children(&this);
   }

   // VERSION: 1.10
   // Removes @child from the children of @self.
   // 
   // This function will release the reference added by
   // clutter_actor_add_child(), so if you want to keep using @child
   // you will have to acquire a referenced on it before calling this
   // function.
   // 
   // This function will emit the #ClutterContainer::actor-removed
   // signal on @self.
   // <child>: a #ClutterActor
   void remove_child(AT0)(AT0 /*Actor*/ child) nothrow {
      clutter_actor_remove_child(&this, UpCast!(Actor*)(child));
   }
   // Removes clip area from @self.
   void remove_clip()() nothrow {
      clutter_actor_remove_clip(&this);
   }

   // VERSION: 1.4
   // Removes @constraint from the list of constraints applied to @self
   // 
   // The reference held by @self on the #ClutterConstraint will be released
   // <constraint>: a #ClutterConstraint
   void remove_constraint(AT0)(AT0 /*Constraint*/ constraint) nothrow {
      clutter_actor_remove_constraint(&this, UpCast!(Constraint*)(constraint));
   }

   // VERSION: 1.4
   // Removes the #ClutterConstraint with the given name from the list
   // of constraints applied to @self
   // <name>: the name of the constraint to remove
   void remove_constraint_by_name(AT0)(AT0 /*char*/ name) nothrow {
      clutter_actor_remove_constraint_by_name(&this, toCString!(char*)(name));
   }

   // VERSION: 1.4
   // Removes @effect from the list of effects applied to @self
   // 
   // The reference held by @self on the #ClutterEffect will be released
   // <effect>: a #ClutterEffect
   void remove_effect(AT0)(AT0 /*Effect*/ effect) nothrow {
      clutter_actor_remove_effect(&this, UpCast!(Effect*)(effect));
   }

   // VERSION: 1.4
   // Removes the #ClutterEffect with the given name from the list
   // of effects applied to @self
   // <name>: the name of the effect to remove
   void remove_effect_by_name(AT0)(AT0 /*char*/ name) nothrow {
      clutter_actor_remove_effect_by_name(&this, toCString!(char*)(name));
   }

   // VERSION: 0.2
   // DEPRECATED (v1.10) method: reparent - Use clutter_actor_remove_child() and
   // Resets the parent actor of @self.
   // 
   // This function is logically equivalent to calling clutter_actor_unparent()
   // and clutter_actor_set_parent(), but more efficiently implemented, as it
   // ensures the child is not finalized when unparented, and emits the
   // #ClutterActor::parent-set signal only once.
   // 
   // In reality, calling this function is less useful than it sounds, as some
   // application code may rely on changes in the intermediate state between
   // removal and addition of the actor from its old parent to the @new_parent.
   // Thus, it is strongly encouraged to avoid using this function in application
   // code.
   // 
   // 
   // clutter_actor_add_child() instead; remember to take a reference on
   // the actor being removed before calling clutter_actor_remove_child()
   // to avoid the reference count dropping to zero and the actor being
   // destroyed.
   // <new_parent>: the new #ClutterActor parent
   void reparent(AT0)(AT0 /*Actor*/ new_parent) nothrow {
      clutter_actor_reparent(&this, UpCast!(Actor*)(new_parent));
   }

   // VERSION: 1.10
   // Replaces @old_child with @new_child in the list of children of @self.
   // <old_child>: the child of @self to replace
   // <new_child>: the #ClutterActor to replace @old_child
   void replace_child(AT0, AT1)(AT0 /*Actor*/ old_child, AT1 /*Actor*/ new_child) nothrow {
      clutter_actor_replace_child(&this, UpCast!(Actor*)(old_child), UpCast!(Actor*)(new_child));
   }

   // VERSION: 1.10
   // Stores the allocation of @self as defined by @box.
   // 
   // This function can only be called from within the implementation of
   // the #ClutterActorClass.allocate() virtual function.
   // 
   // The allocation should have been adjusted to take into account constraints,
   // alignment, and margin properties. If you are implementing a #ClutterActor
   // subclass that provides its own layout management policy for its children
   // instead of using a #ClutterLayoutManager delegate, you should not call
   // this function on the children of @self; instead, you should call
   // clutter_actor_allocate(), which will adjust the allocation box for
   // you.
   // 
   // This function should only be used by subclasses of #ClutterActor
   // that wish to store their allocation but cannot chain up to the
   // parent's implementation; the default implementation of the
   // #ClutterActorClass.allocate() virtual function will call this
   // function.
   // 
   // It is important to note that, while chaining up was the recommended
   // behaviour for #ClutterActor subclasses prior to the introduction of
   // this function, it is recommended to call clutter_actor_set_allocation()
   // instead.
   // 
   // If the #ClutterActor is using a #ClutterLayoutManager delegate object
   // to handle the allocation of its children, this function will call
   // the clutter_layout_manager_allocate() function only if the
   // %CLUTTER_DELEGATE_LAYOUT flag is set on @flags, otherwise it is
   // expected that the subclass will call clutter_layout_manager_allocate()
   // by itself. For instance, the following code:
   // 
   // |[
   // static void
   // my_actor_allocate (ClutterActor *actor,
   // const ClutterActorBox *allocation,
   // ClutterAllocationFlags flags)
   // {
   // ClutterActorBox new_alloc;
   // ClutterAllocationFlags new_flags;
   // 
   // adjust_allocation (allocation, &amp;new_alloc);
   // 
   // new_flags = flags | CLUTTER_DELEGATE_LAYOUT;
   // 
   // /&ast; this will use the layout manager set on the actor &ast;/
   // clutter_actor_set_allocation (actor, &amp;new_alloc, new_flags);
   // }
   // ]|
   // 
   // is equivalent to this:
   // 
   // |[
   // static void
   // my_actor_allocate (ClutterActor *actor,
   // const ClutterActorBox *allocation,
   // ClutterAllocationFlags flags)
   // {
   // ClutterLayoutManager *layout;
   // ClutterActorBox new_alloc;
   // 
   // adjust_allocation (allocation, &amp;new_alloc);
   // 
   // clutter_actor_set_allocation (actor, &amp;new_alloc, flags);
   // 
   // layout = clutter_actor_get_layout_manager (actor);
   // clutter_layout_manager_allocate (layout,
   // CLUTTER_CONTAINER (actor),
   // &amp;new_alloc,
   // flags);
   // }
   // ]|
   // <box>: a #ClutterActorBox
   // <flags>: allocation flags
   void set_allocation(AT0)(AT0 /*ActorBox*/ box, AllocationFlags flags) nothrow {
      clutter_actor_set_allocation(&this, UpCast!(ActorBox*)(box), flags);
   }

   // VERSION: 0.6
   // Sets an anchor point for @self. The anchor point is a point in the
   // coordinate space of an actor to which the actor position within its
   // parent is relative; the default is (0, 0), i.e. the top-left corner
   // of the actor.
   // <anchor_x>: X coordinate of the anchor point
   // <anchor_y>: Y coordinate of the anchor point
   void set_anchor_point()(float anchor_x, float anchor_y) nothrow {
      clutter_actor_set_anchor_point(&this, anchor_x, anchor_y);
   }

   // VERSION: 0.6
   // Sets an anchor point on the actor, based on the given gravity (this is a
   // convenience function wrapping clutter_actor_set_anchor_point()).
   // 
   // Since version 1.0 the anchor point will be stored as a gravity so
   // that if the actor changes size then the anchor point will move. For
   // example, if you set the anchor point to %CLUTTER_GRAVITY_SOUTH_EAST
   // and later double the size of the actor, the anchor point will move
   // to the bottom right.
   // <gravity>: #ClutterGravity.
   void set_anchor_point_from_gravity()(Gravity gravity) nothrow {
      clutter_actor_set_anchor_point_from_gravity(&this, gravity);
   }

   // VERSION: 1.10
   // Sets the background color of a #ClutterActor.
   // 
   // The background color will be used to cover the whole allocation of the
   // actor. The default background color of an actor is transparent.
   // 
   // To check whether an actor has a background color, you can use the
   // #ClutterActor:background-color-set actor property.
   // <color>: a #ClutterColor, or %NULL to unset a previously set color
   void set_background_color(AT0)(AT0 /*Color*/ color=null) nothrow {
      clutter_actor_set_background_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.10
   // Sets @child to be above @sibling in the list of children of @self.
   // 
   // If @sibling is %NULL, @child will be the new last child of @self.
   // 
   // This function is logically equivalent to removing @child and using
   // clutter_actor_insert_child_above(), but it will not emit signals
   // or change state on @child.
   // <child>: a #ClutterActor child of @self
   // <sibling>: a #ClutterActor child of @self, or %NULL
   void set_child_above_sibling(AT0, AT1)(AT0 /*Actor*/ child, AT1 /*Actor*/ sibling=null) nothrow {
      clutter_actor_set_child_above_sibling(&this, UpCast!(Actor*)(child), UpCast!(Actor*)(sibling));
   }

   // VERSION: 1.10
   // Changes the index of @child in the list of children of @self.
   // 
   // This function is logically equivalent to removing @child and
   // calling clutter_actor_insert_child_at_index(), but it will not
   // emit signals or change state on @child.
   // <child>: a #ClutterActor child of @self
   // <index_>: the new index for @child
   void set_child_at_index(AT0)(AT0 /*Actor*/ child, int index_) nothrow {
      clutter_actor_set_child_at_index(&this, UpCast!(Actor*)(child), index_);
   }

   // VERSION: 1.10
   // Sets @child to be below @sibling in the list of children of @self.
   // 
   // If @sibling is %NULL, @child will be the new first child of @self.
   // 
   // This function is logically equivalent to removing @self and using
   // clutter_actor_insert_child_below(), but it will not emit signals
   // or change state on @child.
   // <child>: a #ClutterActor child of @self
   // <sibling>: a #ClutterActor child of @self, or %NULL
   void set_child_below_sibling(AT0, AT1)(AT0 /*Actor*/ child, AT1 /*Actor*/ sibling=null) nothrow {
      clutter_actor_set_child_below_sibling(&this, UpCast!(Actor*)(child), UpCast!(Actor*)(sibling));
   }

   // VERSION: 0.6
   // Sets clip area for @self. The clip area is always computed from the
   // upper left corner of the actor, even if the anchor point is set
   // otherwise.
   // <xoff>: X offset of the clip rectangle
   // <yoff>: Y offset of the clip rectangle
   // <width>: Width of the clip rectangle
   // <height>: Height of the clip rectangle
   void set_clip()(float xoff, float yoff, float width, float height) nothrow {
      clutter_actor_set_clip(&this, xoff, yoff, width, height);
   }

   // VERSION: 1.4
   // Sets whether @self should be clipped to the same size as its
   // allocation
   // <clip_set>: %TRUE to apply a clip tracking the allocation
   void set_clip_to_allocation()(int clip_set) nothrow {
      clutter_actor_set_clip_to_allocation(&this, clip_set);
   }

   // Sets the Z coordinate of @self to @depth.
   // 
   // The unit used by @depth is dependant on the perspective setup. See
   // also clutter_stage_set_perspective().
   // <depth>: Z co-ord
   void set_depth()(float depth) nothrow {
      clutter_actor_set_depth(&this, depth);
   }

   // VERSION: 0.8
   // Sets whether an actor has a fixed position set (and will thus be
   // unaffected by any layout manager).
   // <is_set>: whether to use fixed position
   void set_fixed_position_set()(int is_set) nothrow {
      clutter_actor_set_fixed_position_set(&this, is_set);
   }

   // VERSION: 1.0
   // Sets @flags on @self
   // 
   // This function will emit notifications for the changed properties
   // <flags>: the flags to set
   void set_flags()(ActorFlags flags) nothrow {
      clutter_actor_set_flags(&this, flags);
   }

   // DEPRECATED (v1.10) method: set_geometry - Use clutter_actor_set_position() and
   // Sets the actor's fixed position and forces its minimum and natural
   // size, in pixels. This means the untransformed actor will have the
   // given geometry. This is the same as calling clutter_actor_set_position()
   // and clutter_actor_set_size().
   // 
   // clutter_actor_set_size() instead.
   // <geometry>: A #ClutterGeometry
   void set_geometry(AT0)(AT0 /*Geometry*/ geometry) nothrow {
      clutter_actor_set_geometry(&this, UpCast!(Geometry*)(geometry));
   }

   // VERSION: 0.2
   // Forces a height on an actor, causing the actor's preferred width
   // and height (if any) to be ignored.
   // 
   // If @height is -1 the actor will use its preferred height instead of
   // overriding it, i.e. you can "unset" the height with -1.
   // 
   // This function sets both the minimum and natural size of the actor.
   // <height>: Requested new height for the actor, in pixels, or -1
   void set_height()(float height) nothrow {
      clutter_actor_set_height(&this, height);
   }

   // VERSION: 1.10
   // Sets the #ClutterLayoutManager delegate object that will be used to
   // lay out the children of @self.
   // 
   // The #ClutterActor will take a reference on the passed @manager which
   // will be released either when the layout manager is removed, or when
   // the actor is destroyed.
   // <manager>: a #ClutterLayoutManager, or %NULL to unset it
   void set_layout_manager(AT0)(AT0 /*LayoutManager*/ manager=null) nothrow {
      clutter_actor_set_layout_manager(&this, UpCast!(LayoutManager*)(manager));
   }

   // VERSION: 1.10
   // Sets all the components of the margin of a #ClutterActor.
   // <margin>: a #ClutterMargin
   void set_margin(AT0)(AT0 /*Margin*/ margin) nothrow {
      clutter_actor_set_margin(&this, UpCast!(Margin*)(margin));
   }

   // VERSION: 1.10
   // Sets the margin from the bottom of a #ClutterActor.
   // <margin>: the bottom margin
   void set_margin_bottom()(float margin) nothrow {
      clutter_actor_set_margin_bottom(&this, margin);
   }

   // VERSION: 1.10
   // Sets the margin from the left of a #ClutterActor.
   // <margin>: the left margin
   void set_margin_left()(float margin) nothrow {
      clutter_actor_set_margin_left(&this, margin);
   }

   // VERSION: 1.10
   // Sets the margin from the right of a #ClutterActor.
   // <margin>: the right margin
   void set_margin_right()(float margin) nothrow {
      clutter_actor_set_margin_right(&this, margin);
   }

   // VERSION: 1.10
   // Sets the margin from the top of a #ClutterActor.
   // <margin>: the top margin
   void set_margin_top()(float margin) nothrow {
      clutter_actor_set_margin_top(&this, margin);
   }

   // Sets the given name to @self. The name can be used to identify
   // a #ClutterActor.
   // <name>: Textual tag to apply to actor
   void set_name(AT0)(AT0 /*char*/ name) nothrow {
      clutter_actor_set_name(&this, toCString!(char*)(name));
   }

   // VERSION: 1.8
   // Defines the circumstances where the actor should be redirected into
   // an offscreen image. The offscreen image is used to flatten the
   // actor into a single image while painting for two main reasons.
   // Firstly, when the actor is painted a second time without any of its
   // contents changing it can simply repaint the cached image without
   // descending further down the actor hierarchy. Secondly, it will make
   // the opacity look correct even if there are overlapping primitives
   // in the actor.
   // 
   // Caching the actor could in some cases be a performance win and in
   // some cases be a performance lose so it is important to determine
   // which value is right for an actor before modifying this value. For
   // example, there is never any reason to flatten an actor that is just
   // a single texture (such as a #ClutterTexture) because it is
   // effectively already cached in an image so the offscreen would be
   // redundant. Also if the actor contains primitives that are far apart
   // with a large transparent area in the middle (such as a large
   // CluterGroup with a small actor in the top left and a small actor in
   // the bottom right) then the cached image will contain the entire
   // image of the large area and the paint will waste time blending all
   // of the transparent pixels in the middle.
   // 
   // The default method of implementing opacity on a container simply
   // forwards on the opacity to all of the children. If the children are
   // overlapping then it will appear as if they are two separate glassy
   // objects and there will be a break in the color where they
   // overlap. By redirecting to an offscreen buffer it will be as if the
   // two opaque objects are combined into one and then made transparent
   // which is usually what is expected.
   // 
   // The image below demonstrates the difference between redirecting and
   // not. The image shows two Clutter groups, each containing a red and
   // a green rectangle which overlap. The opacity on the group is set to
   // 128 (which is 50%). When the offscreen redirect is not used, the
   // red rectangle can be seen through the blue rectangle as if the two
   // rectangles were separately transparent. When the redirect is used
   // the group as a whole is transparent instead so the red rectangle is
   // not visible where they overlap.
   // 
   // <figure id="offscreen-redirect">
   // <title>Sample of using an offscreen redirect for transparency</title>
   // <graphic fileref="offscreen-redirect.png" format="PNG"/>
   // </figure>
   // 
   // The default value for this property is 0, so we effectively will
   // never redirect an actor offscreen by default. This means that there
   // are times that transparent actors may look glassy as described
   // above. The reason this is the default is because there is a
   // performance trade off between quality and performance here. In many
   // cases the default form of glassy opacity looks good enough, but if
   // it's not you will need to set the
   // %CLUTTER_OFFSCREEN_REDIRECT_AUTOMATIC_FOR_OPACITY flag to enable
   // redirection for opacity.
   // 
   // Custom actors that don't contain any overlapping primitives are
   // recommended to override the has_overlaps() virtual to return %FALSE
   // for maximum efficiency.
   // <redirect>: New offscreen redirect flags for the actor.
   void set_offscreen_redirect()(OffscreenRedirect redirect) nothrow {
      clutter_actor_set_offscreen_redirect(&this, redirect);
   }

   // Sets the actor's opacity, with zero being completely transparent and
   // 255 (0xff) being fully opaque.
   // <opacity>: New opacity value for the actor.
   void set_opacity()(ubyte opacity) nothrow {
      clutter_actor_set_opacity(&this, opacity);
   }

   // DEPRECATED (v1.10) method: set_parent - Use clutter_actor_add_child() instead.
   // Sets the parent of @self to @parent.
   // 
   // This function will result in @parent acquiring a reference on @self,
   // eventually by sinking its floating reference first. The reference
   // will be released by clutter_actor_unparent().
   // 
   // This function should only be called by legacy #ClutterActor<!-- -->s
   // implementing the #ClutterContainer interface.
   // <parent>: A new #ClutterActor parent
   void set_parent(AT0)(AT0 /*Actor*/ parent) nothrow {
      clutter_actor_set_parent(&this, UpCast!(Actor*)(parent));
   }

   // Sets the actor's fixed position in pixels relative to any parent
   // actor.
   // 
   // If a layout manager is in use, this position will override the
   // layout manager and force a fixed position.
   // <x>: New left position of actor in pixels.
   // <y>: New top position of actor in pixels.
   void set_position()(float x, float y) nothrow {
      clutter_actor_set_position(&this, x, y);
   }

   // VERSION: 0.6
   // Sets @actor as reactive. Reactive actors will receive events.
   // <reactive>: whether the actor should be reactive to events
   void set_reactive()(int reactive) nothrow {
      clutter_actor_set_reactive(&this, reactive);
   }

   // VERSION: 1.2
   // Sets the geometry request mode of @self.
   // 
   // The @mode determines the order for invoking
   // clutter_actor_get_preferred_width() and
   // clutter_actor_get_preferred_height()
   // <mode>: the request mode
   void set_request_mode()(RequestMode mode) nothrow {
      clutter_actor_set_request_mode(&this, mode);
   }

   // VERSION: 0.8
   // Sets the rotation angle of @self around the given axis.
   // 
   // The rotation center coordinates used depend on the value of @axis:
   // <itemizedlist>
   // <listitem><para>%CLUTTER_X_AXIS requires @y and @z</para></listitem>
   // <listitem><para>%CLUTTER_Y_AXIS requires @x and @z</para></listitem>
   // <listitem><para>%CLUTTER_Z_AXIS requires @x and @y</para></listitem>
   // </itemizedlist>
   // 
   // The rotation coordinates are relative to the anchor point of the
   // actor, set using clutter_actor_set_anchor_point(). If no anchor
   // point is set, the upper left corner is assumed as the origin.
   // <axis>: the axis of rotation
   // <angle>: the angle of rotation
   // <x>: X coordinate of the rotation center
   // <y>: Y coordinate of the rotation center
   // <z>: Z coordinate of the rotation center
   void set_rotation()(RotateAxis axis, double angle, float x, float y, float z) nothrow {
      clutter_actor_set_rotation(&this, axis, angle, x, y, z);
   }

   // VERSION: 0.2
   // Scales an actor with the given factors. The scaling is relative to
   // the scale center and the anchor point. The scale center is
   // unchanged by this function and defaults to 0,0.
   // <scale_x>: double factor to scale actor by horizontally.
   // <scale_y>: double factor to scale actor by vertically.
   void set_scale()(double scale_x, double scale_y) nothrow {
      clutter_actor_set_scale(&this, scale_x, scale_y);
   }

   // VERSION: 1.0
   // Scales an actor with the given factors around the given center
   // point. The center point is specified in pixels relative to the
   // anchor point (usually the top left corner of the actor).
   // <scale_x>: double factor to scale actor by horizontally.
   // <scale_y>: double factor to scale actor by vertically.
   // <center_x>: X coordinate of the center of the scale.
   // <center_y>: Y coordinate of the center of the scale
   void set_scale_full()(double scale_x, double scale_y, float center_x, float center_y) nothrow {
      clutter_actor_set_scale_full(&this, scale_x, scale_y, center_x, center_y);
   }

   // VERSION: 1.0
   // Scales an actor with the given factors around the given
   // center point. The center point is specified as one of the compass
   // directions in #ClutterGravity. For example, setting it to north
   // will cause the top of the actor to remain unchanged and the rest of
   // the actor to expand left, right and downwards.
   // <scale_x>: double factor to scale actor by horizontally.
   // <scale_y>: double factor to scale actor by vertically.
   // <gravity>: the location of the scale center expressed as a compass direction.
   void set_scale_with_gravity()(double scale_x, double scale_y, Gravity gravity) nothrow {
      clutter_actor_set_scale_with_gravity(&this, scale_x, scale_y, gravity);
   }

   // VERSION: 0.6
   // DEPRECATED (v1.8) method: set_shader - Use #ClutterShaderEffect and
   // Sets the #ClutterShader to be used when rendering @self.
   // 
   // If @shader is %NULL this function will unset any currently set shader
   // for the actor.
   // 
   // <note>Any #ClutterEffect applied to @self will take the precedence
   // over the #ClutterShader set using this function.</note>
   // 
   // or removed
   // 
   // 
   // clutter_actor_add_effect() instead.
   // RETURNS: %TRUE if the shader was successfully applied
   // <shader>: a #ClutterShader or %NULL to unset the shader.
   int set_shader(AT0)(AT0 /*Shader*/ shader=null) nothrow {
      return clutter_actor_set_shader(&this, UpCast!(Shader*)(shader));
   }

   // VERSION: 1.0
   // DEPRECATED (v1.8) method: set_shader_param - Use clutter_shader_effect_set_uniform_value() instead
   // Sets the value for a named parameter of the shader applied
   // to @actor.
   // <param>: the name of the parameter
   // <value>: the value of the parameter
   void set_shader_param(AT0, AT1)(AT0 /*char*/ param, AT1 /*GObject2.Value*/ value) nothrow {
      clutter_actor_set_shader_param(&this, toCString!(char*)(param), UpCast!(GObject2.Value*)(value));
   }

   // VERSION: 0.8
   // DEPRECATED (v1.8) method: set_shader_param_float - Use clutter_shader_effect_set_uniform() instead
   // Sets the value for a named float parameter of the shader applied
   // to @actor.
   // <param>: the name of the parameter
   // <value>: the value of the parameter
   void set_shader_param_float(AT0)(AT0 /*char*/ param, float value) nothrow {
      clutter_actor_set_shader_param_float(&this, toCString!(char*)(param), value);
   }

   // VERSION: 0.8
   // DEPRECATED (v1.8) method: set_shader_param_int - Use clutter_shader_effect_set_uniform() instead
   // Sets the value for a named int parameter of the shader applied to
   // @actor.
   // <param>: the name of the parameter
   // <value>: the value of the parameter
   void set_shader_param_int(AT0)(AT0 /*char*/ param, int value) nothrow {
      clutter_actor_set_shader_param_int(&this, toCString!(char*)(param), value);
   }

   // Sets the actor's size request in pixels. This overrides any
   // "normal" size request the actor would have. For example
   // a text actor might normally request the size of the text;
   // this function would force a specific size instead.
   // 
   // If @width and/or @height are -1 the actor will use its
   // "normal" size request instead of overriding it, i.e.
   // you can "unset" the size with -1.
   // 
   // This function sets or unsets both the minimum and natural size.
   // <width>: New width of actor in pixels, or -1
   // <height>: New height of actor in pixels, or -1
   void set_size()(float width, float height) nothrow {
      clutter_actor_set_size(&this, width, height);
   }

   // VERSION: 1.2
   // Sets the #ClutterTextDirection for an actor
   // 
   // The passed text direction must not be %CLUTTER_TEXT_DIRECTION_DEFAULT
   // 
   // If @self implements #ClutterContainer then this function will recurse
   // inside all the children of @self (including the internal ones).
   // 
   // Composite actors not implementing #ClutterContainer, or actors requiring
   // special handling when the text direction changes, should connect to
   // the #GObject::notify signal for the #ClutterActor:text-direction property
   // <text_dir>: the text direction for @self
   void set_text_direction()(TextDirection text_dir) nothrow {
      clutter_actor_set_text_direction(&this, text_dir);
   }

   // VERSION: 0.2
   // Forces a width on an actor, causing the actor's preferred width
   // and height (if any) to be ignored.
   // 
   // If @width is -1 the actor will use its preferred width request
   // instead of overriding it, i.e. you can "unset" the width with -1.
   // 
   // This function sets both the minimum and natural size of the actor.
   // <width>: Requested new width for the actor, in pixels, or -1
   void set_width()(float width) nothrow {
      clutter_actor_set_width(&this, width);
   }

   // VERSION: 0.6
   // Sets the actor's X coordinate, relative to its parent, in pixels.
   // 
   // Overrides any layout manager and forces a fixed position for
   // the actor.
   // <x>: the actor's position on the X axis
   void set_x()(float x) nothrow {
      clutter_actor_set_x(&this, x);
   }

   // VERSION: 1.10
   // Sets the horizontal alignment policy of a #ClutterActor, in case the
   // actor received extra horizontal space.
   // 
   // See also the #ClutterActor:x-align property.
   // <x_align>: the horizontal alignment policy
   void set_x_align()(ActorAlign x_align) nothrow {
      clutter_actor_set_x_align(&this, x_align);
   }

   // VERSION: 0.6
   // Sets the actor's Y coordinate, relative to its parent, in pixels.#
   // 
   // Overrides any layout manager and forces a fixed position for
   // the actor.
   // <y>: the actor's position on the Y axis
   void set_y()(float y) nothrow {
      clutter_actor_set_y(&this, y);
   }

   // VERSION: 1.10
   // Sets the vertical alignment policy of a #ClutterActor, in case the
   // actor received extra vertical space.
   // 
   // See also the #ClutterActor:y-align property.
   // <y_align>: the vertical alignment policy
   void set_y_align()(ActorAlign y_align) nothrow {
      clutter_actor_set_y_align(&this, y_align);
   }

   // VERSION: 1.0
   // Sets the rotation angle of @self around the Z axis using the center
   // point specified as a compass point. For example to rotate such that
   // the center of the actor remains static you can use
   // %CLUTTER_GRAVITY_CENTER. If the actor changes size the center point
   // will move accordingly.
   // <angle>: the angle of rotation
   // <gravity>: the center point of the rotation
   void set_z_rotation_from_gravity()(double angle, Gravity gravity) nothrow {
      clutter_actor_set_z_rotation_from_gravity(&this, angle, gravity);
   }

   // Should be called inside the implementation of the
   // #ClutterActor::pick virtual function in order to check whether
   // the actor should paint itself in pick mode or not.
   // 
   // This function should never be called directly by applications.
   // 
   // %FALSE otherwise
   // RETURNS: %TRUE if the actor should paint its silhouette,
   int should_pick_paint()() nothrow {
      return clutter_actor_should_pick_paint(&this);
   }

   // Flags an actor to be displayed. An actor that isn't shown will not
   // be rendered on the stage.
   // 
   // Actors are visible by default.
   // 
   // If this function is called on an actor without a parent, the
   // #ClutterActor:show-on-set-parent will be set to %TRUE as a side
   // effect.
   void show()() nothrow {
      clutter_actor_show(&this);
   }

   // VERSION: 0.2
   // DEPRECATED (v1.10) method: show_all - Actors are visible by default
   // Calls clutter_actor_show() on all children of an actor (if any).
   void show_all()() nothrow {
      clutter_actor_show_all(&this);
   }

   // VERSION: 0.6
   // This function translates screen coordinates (@x, @y) to
   // coordinates relative to the actor. For example, it can be used to translate
   // screen events from global screen coordinates into actor-local coordinates.
   // 
   // The conversion can fail, notably if the transform stack results in the
   // actor being projected on the screen as a mere line.
   // 
   // The conversion should not be expected to be pixel-perfect due to the
   // nature of the operation. In general the error grows when the skewing
   // of the actor rectangle on screen increases.
   // 
   // <note><para>This function can be computationally intensive.</para></note>
   // 
   // <note><para>This function only works when the allocation is up-to-date,
   // i.e. inside of paint().</para></note>
   // RETURNS: %TRUE if conversion was successful.
   // <x>: x screen coordinate of the point to unproject
   // <y>: y screen coordinate of the point to unproject
   // <x_out>: return location for the unprojected x coordinance
   // <y_out>: return location for the unprojected y coordinance
   int transform_stage_point(AT0, AT1)(float x, float y, /*out*/ AT0 /*float*/ x_out, /*out*/ AT1 /*float*/ y_out) nothrow {
      return clutter_actor_transform_stage_point(&this, x, y, UpCast!(float*)(x_out), UpCast!(float*)(y_out));
   }

   // VERSION: 1.0
   // Unsets the %CLUTTER_ACTOR_MAPPED flag on the actor and possibly
   // unmaps its children if they were mapped.
   // 
   // Calling this function is not encouraged: the default #ClutterActor
   // implementation of #ClutterActorClass.unmap() will also unmap any
   // eventual children by default when their parent is unmapped.
   // 
   // When overriding #ClutterActorClass.unmap(), it is mandatory to
   // chain up to the parent implementation.
   // 
   // <note>It is important to note that the implementation of the
   // #ClutterActorClass.unmap() virtual function may be called after
   // the #ClutterActorClass.destroy() or the #GObjectClass.dispose()
   // implementation, but it is guaranteed to be called before the
   // #GObjectClass.finalize() implementation.</note>
   void unmap()() nothrow {
      clutter_actor_unmap(&this);
   }

   // VERSION: 0.1.1
   // DEPRECATED (v1.10) method: unparent - Use clutter_actor_remove_child() instead.
   // Removes the parent of @self.
   // 
   // This will cause the parent of @self to release the reference
   // acquired when calling clutter_actor_set_parent(), so if you
   // want to keep @self you will have to acquire a reference of
   // your own, through g_object_ref().
   // 
   // This function should only be called by legacy #ClutterActor<!-- -->s
   // implementing the #ClutterContainer interface.
   void unparent()() nothrow {
      clutter_actor_unparent(&this);
   }

   // Unrealization informs the actor that it may be being destroyed or
   // moved to another stage. The actor may want to destroy any
   // underlying graphics resources at this point. However it is
   // perfectly acceptable for it to retain the resources until the actor
   // is destroyed because Clutter only ever uses a single rendering
   // context and all of the graphics resources are valid on any stage.
   // 
   // Because mapped actors must be realized, actors may not be
   // unrealized if they are mapped. This function hides the actor to be
   // sure it isn't mapped, an application-visible side effect that you
   // may not be expecting.
   // 
   // This function should not be called by application code.
   void unrealize()() nothrow {
      clutter_actor_unrealize(&this);
   }

   // VERSION: 1.0
   // Unsets @flags on @self
   // 
   // This function will emit notifications for the changed properties
   // <flags>: the flags to unset
   void unset_flags()(ActorFlags flags) nothrow {
      clutter_actor_unset_flags(&this, flags);
   }

   // VERSION: 1.0
   // The ::allocation-changed signal is emitted when the
   // #ClutterActor:allocation property changes. Usually, application
   // code should just use the notifications for the :allocation property
   // but if you want to track the allocation flags as well, for instance
   // to know whether the absolute origin of @actor changed, then you might
   // want use this signal instead.
   // <box>: a #ClutterActorBox with the new allocation
   // <flags>: #ClutterAllocationFlags for the allocation
   extern (C) alias static void function (Actor* this_, ActorBox* box, AllocationFlags* flags, void* user_data=null) nothrow signal_allocation_changed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"allocation-changed", CB/*:signal_allocation_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_allocation_changed)||_ttmm!(CB, signal_allocation_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"allocation-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::button-press-event signal is emitted each time a mouse button
   // is pressed on @actor.
   // 
   // or %FALSE to continue the emission.
   // RETURNS: %TRUE if the event has been handled by the actor,
   // <event>: a #ClutterButtonEvent
   extern (C) alias static c_int function (Actor* this_, ButtonEvent* event, void* user_data=null) nothrow signal_button_press_event;
   ulong signal_connect(string name:"button-press-event", CB/*:signal_button_press_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_button_press_event)||_ttmm!(CB, signal_button_press_event)()) {
      return signal_connect_data!()(&this, cast(char*)"button-press-event",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::button-release-event signal is emitted each time a mouse button
   // is released on @actor.
   // 
   // or %FALSE to continue the emission.
   // RETURNS: %TRUE if the event has been handled by the actor,
   // <event>: a #ClutterButtonEvent
   extern (C) alias static c_int function (Actor* this_, ButtonEvent* event, void* user_data=null) nothrow signal_button_release_event;
   ulong signal_connect(string name:"button-release-event", CB/*:signal_button_release_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_button_release_event)||_ttmm!(CB, signal_button_release_event)()) {
      return signal_connect_data!()(&this, cast(char*)"button-release-event",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::captured-event signal is emitted when an event is captured
   // by Clutter. This signal will be emitted starting from the top-level
   // container (the #ClutterStage) to the actor which received the event
   // going down the hierarchy. This signal can be used to intercept every
   // event before the specialized events (like
   // ClutterActor::button-press-event or ::key-released-event) are
   // emitted.
   // 
   // or %FALSE to continue the emission.
   // RETURNS: %TRUE if the event has been handled by the actor,
   // <event>: a #ClutterEvent
   extern (C) alias static c_int function (Actor* this_, Event* event, void* user_data=null) nothrow signal_captured_event;
   ulong signal_connect(string name:"captured-event", CB/*:signal_captured_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_captured_event)||_ttmm!(CB, signal_captured_event)()) {
      return signal_connect_data!()(&this, cast(char*)"captured-event",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.2
   // The ::destroy signal notifies that all references held on the
   // actor which emitted it should be released.
   // 
   // The ::destroy signal should be used by all holders of a reference
   // on @actor.
   // 
   // This signal might result in the finalization of the #ClutterActor
   // if all references are released.
   // 
   // Composite actors and actors implementing the #ClutterContainer
   // interface should override the default implementation of the
   // class handler of this signal and call clutter_actor_destroy() on
   // their children. When overriding the default class handler, it is
   // required to chain up to the parent's implementation.
   extern (C) alias static void function (Actor* this_, void* user_data=null) nothrow signal_destroy;
   ulong signal_connect(string name:"destroy", CB/*:signal_destroy*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_destroy)||_ttmm!(CB, signal_destroy)()) {
      return signal_connect_data!()(&this, cast(char*)"destroy",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::enter-event signal is emitted when the pointer enters the @actor
   // 
   // or %FALSE to continue the emission.
   // RETURNS: %TRUE if the event has been handled by the actor,
   // <event>: a #ClutterCrossingEvent
   extern (C) alias static c_int function (Actor* this_, CrossingEvent* event, void* user_data=null) nothrow signal_enter_event;
   ulong signal_connect(string name:"enter-event", CB/*:signal_enter_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_enter_event)||_ttmm!(CB, signal_enter_event)()) {
      return signal_connect_data!()(&this, cast(char*)"enter-event",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::event signal is emitted each time an event is received
   // by the @actor. This signal will be emitted on every actor,
   // following the hierarchy chain, until it reaches the top-level
   // container (the #ClutterStage).
   // 
   // or %FALSE to continue the emission.
   // RETURNS: %TRUE if the event has been handled by the actor,
   // <event>: a #ClutterEvent
   extern (C) alias static c_int function (Actor* this_, Event* event, void* user_data=null) nothrow signal_event;
   ulong signal_connect(string name:"event", CB/*:signal_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_event)||_ttmm!(CB, signal_event)()) {
      return signal_connect_data!()(&this, cast(char*)"event",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.2
   // The ::hide signal is emitted when an actor is no longer rendered
   // on the stage.
   extern (C) alias static void function (Actor* this_, void* user_data=null) nothrow signal_hide;
   ulong signal_connect(string name:"hide", CB/*:signal_hide*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_hide)||_ttmm!(CB, signal_hide)()) {
      return signal_connect_data!()(&this, cast(char*)"hide",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::key-focus-in signal is emitted when @actor receives key focus.
   extern (C) alias static void function (Actor* this_, void* user_data=null) nothrow signal_key_focus_in;
   ulong signal_connect(string name:"key-focus-in", CB/*:signal_key_focus_in*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_key_focus_in)||_ttmm!(CB, signal_key_focus_in)()) {
      return signal_connect_data!()(&this, cast(char*)"key-focus-in",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::key-focus-out signal is emitted when @actor loses key focus.
   extern (C) alias static void function (Actor* this_, void* user_data=null) nothrow signal_key_focus_out;
   ulong signal_connect(string name:"key-focus-out", CB/*:signal_key_focus_out*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_key_focus_out)||_ttmm!(CB, signal_key_focus_out)()) {
      return signal_connect_data!()(&this, cast(char*)"key-focus-out",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::key-press-event signal is emitted each time a keyboard button
   // is pressed while @actor has key focus (see clutter_stage_set_key_focus()).
   // 
   // or %FALSE to continue the emission.
   // RETURNS: %TRUE if the event has been handled by the actor,
   // <event>: a #ClutterKeyEvent
   extern (C) alias static c_int function (Actor* this_, KeyEvent* event, void* user_data=null) nothrow signal_key_press_event;
   ulong signal_connect(string name:"key-press-event", CB/*:signal_key_press_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_key_press_event)||_ttmm!(CB, signal_key_press_event)()) {
      return signal_connect_data!()(&this, cast(char*)"key-press-event",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::key-release-event signal is emitted each time a keyboard button
   // is released while @actor has key focus (see
   // clutter_stage_set_key_focus()).
   // 
   // or %FALSE to continue the emission.
   // RETURNS: %TRUE if the event has been handled by the actor,
   // <event>: a #ClutterKeyEvent
   extern (C) alias static c_int function (Actor* this_, KeyEvent* event, void* user_data=null) nothrow signal_key_release_event;
   ulong signal_connect(string name:"key-release-event", CB/*:signal_key_release_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_key_release_event)||_ttmm!(CB, signal_key_release_event)()) {
      return signal_connect_data!()(&this, cast(char*)"key-release-event",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::leave-event signal is emitted when the pointer leaves the @actor.
   // 
   // or %FALSE to continue the emission.
   // RETURNS: %TRUE if the event has been handled by the actor,
   // <event>: a #ClutterCrossingEvent
   extern (C) alias static c_int function (Actor* this_, CrossingEvent* event, void* user_data=null) nothrow signal_leave_event;
   ulong signal_connect(string name:"leave-event", CB/*:signal_leave_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_leave_event)||_ttmm!(CB, signal_leave_event)()) {
      return signal_connect_data!()(&this, cast(char*)"leave-event",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::motion-event signal is emitted each time the mouse pointer is
   // moved over @actor.
   // 
   // or %FALSE to continue the emission.
   // RETURNS: %TRUE if the event has been handled by the actor,
   // <event>: a #ClutterMotionEvent
   extern (C) alias static c_int function (Actor* this_, MotionEvent* event, void* user_data=null) nothrow signal_motion_event;
   ulong signal_connect(string name:"motion-event", CB/*:signal_motion_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_motion_event)||_ttmm!(CB, signal_motion_event)()) {
      return signal_connect_data!()(&this, cast(char*)"motion-event",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.8
   // The ::paint signal is emitted each time an actor is being painted.
   // 
   // Subclasses of #ClutterActor should override the class signal handler
   // and paint themselves in that function.
   // 
   // It is possible to connect a handler to the ::paint signal in order
   // to set up some custom aspect of a paint.
   extern (C) alias static void function (Actor* this_, void* user_data=null) nothrow signal_paint;
   ulong signal_connect(string name:"paint", CB/*:signal_paint*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_paint)||_ttmm!(CB, signal_paint)()) {
      return signal_connect_data!()(&this, cast(char*)"paint",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.2
   // This signal is emitted when the parent of the actor changes.
   // <old_parent>: the previous parent of the actor, or %NULL
   extern (C) alias static void function (Actor* this_, Actor* old_parent=null, void* user_data=null) nothrow signal_parent_set;
   ulong signal_connect(string name:"parent-set", CB/*:signal_parent_set*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_parent_set)||_ttmm!(CB, signal_parent_set)()) {
      return signal_connect_data!()(&this, cast(char*)"parent-set",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.0
   // The ::pick signal is emitted each time an actor is being painted
   // in "pick mode". The pick mode is used to identify the actor during
   // the event handling phase, or by clutter_stage_get_actor_at_pos().
   // The actor should paint its shape using the passed @pick_color.
   // 
   // Subclasses of #ClutterActor should override the class signal handler
   // and paint themselves in that function.
   // 
   // It is possible to connect a handler to the ::pick signal in order
   // to set up some custom aspect of a paint in pick mode.
   // <color>: the #ClutterColor to be used when picking
   extern (C) alias static void function (Actor* this_, Color* color, void* user_data=null) nothrow signal_pick;
   ulong signal_connect(string name:"pick", CB/*:signal_pick*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_pick)||_ttmm!(CB, signal_pick)()) {
      return signal_connect_data!()(&this, cast(char*)"pick",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.0
   // The ::queue_redraw signal is emitted when clutter_actor_queue_redraw()
   // is called on @origin.
   // 
   // The default implementation for #ClutterActor chains up to the
   // parent actor and queues a redraw on the parent, thus "bubbling"
   // the redraw queue up through the actor graph. The default
   // implementation for #ClutterStage queues a clutter_stage_ensure_redraw()
   // in a main loop idle handler.
   // 
   // Note that the @origin actor may be the stage, or a container; it
   // does not have to be a leaf node in the actor graph.
   // 
   // Toolkits embedding a #ClutterStage which require a redraw and
   // relayout cycle can stop the emission of this signal using the
   // GSignal API, redraw the UI and then call clutter_stage_ensure_redraw()
   // themselves, like:
   // 
   // |[
   // static void
   // on_redraw_complete (gpointer data)
   // {
   // ClutterStage *stage = data;
   // 
   // /&ast; execute the Clutter drawing pipeline &ast;/
   // clutter_stage_ensure_redraw (stage);
   // }
   // 
   // static void
   // on_stage_queue_redraw (ClutterStage *stage)
   // {
   // /&ast; this prevents the default handler to run &ast;/
   // g_signal_stop_emission_by_name (stage, "queue-redraw");
   // 
   // /&ast; queue a redraw with the host toolkit and call
   // &ast; a function when the redraw has been completed
   // &ast;/
   // queue_a_redraw (G_CALLBACK (on_redraw_complete), stage);
   // }
   // ]|
   // 
   // <note><para>This signal is emitted before the Clutter paint
   // pipeline is executed. If you want to know when the pipeline has
   // been completed you should connect to the ::paint signal on the
   // Stage with g_signal_connect_after().</para></note>
   // <origin>: the actor which initiated the redraw request
   extern (C) alias static void function (Actor* this_, Actor* origin, void* user_data=null) nothrow signal_queue_redraw;
   ulong signal_connect(string name:"queue-redraw", CB/*:signal_queue_redraw*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_queue_redraw)||_ttmm!(CB, signal_queue_redraw)()) {
      return signal_connect_data!()(&this, cast(char*)"queue-redraw",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.2
   // The ::queue_layout signal is emitted when clutter_actor_queue_relayout()
   // is called on an actor.
   // 
   // The default implementation for #ClutterActor chains up to the
   // parent actor and queues a relayout on the parent, thus "bubbling"
   // the relayout queue up through the actor graph.
   // 
   // The main purpose of this signal is to allow relayout to be propagated
   // properly in the procense of #ClutterClone actors. Applications will
   // not normally need to connect to this signal.
   extern (C) alias static void function (Actor* this_, void* user_data=null) nothrow signal_queue_relayout;
   ulong signal_connect(string name:"queue-relayout", CB/*:signal_queue_relayout*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_queue_relayout)||_ttmm!(CB, signal_queue_relayout)()) {
      return signal_connect_data!()(&this, cast(char*)"queue-relayout",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.8
   // The ::realize signal is emitted each time an actor is being
   // realized.
   extern (C) alias static void function (Actor* this_, void* user_data=null) nothrow signal_realize;
   ulong signal_connect(string name:"realize", CB/*:signal_realize*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_realize)||_ttmm!(CB, signal_realize)()) {
      return signal_connect_data!()(&this, cast(char*)"realize",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::scroll-event signal is emitted each time the mouse is
   // scrolled on @actor
   // 
   // or %FALSE to continue the emission.
   // RETURNS: %TRUE if the event has been handled by the actor,
   // <event>: a #ClutterScrollEvent
   extern (C) alias static c_int function (Actor* this_, ScrollEvent* event, void* user_data=null) nothrow signal_scroll_event;
   ulong signal_connect(string name:"scroll-event", CB/*:signal_scroll_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_scroll_event)||_ttmm!(CB, signal_scroll_event)()) {
      return signal_connect_data!()(&this, cast(char*)"scroll-event",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.2
   // The ::show signal is emitted when an actor is visible and
   // rendered on the stage.
   extern (C) alias static void function (Actor* this_, void* user_data=null) nothrow signal_show;
   ulong signal_connect(string name:"show", CB/*:signal_show*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_show)||_ttmm!(CB, signal_show)()) {
      return signal_connect_data!()(&this, cast(char*)"show",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.8
   // The ::unrealize signal is emitted each time an actor is being
   // unrealized.
   extern (C) alias static void function (Actor* this_, void* user_data=null) nothrow signal_unrealize;
   ulong signal_connect(string name:"unrealize", CB/*:signal_unrealize*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_unrealize)||_ttmm!(CB, signal_unrealize)()) {
      return signal_connect_data!()(&this, cast(char*)"unrealize",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}


// Controls how a #ClutterActor should align itself inside the extra space
// assigned to it during the allocation.
// 
// Alignment only matters if the allocated space given to an actor is
// bigger than its natural size; for example, when the #ClutterActor:x-expand
// or the #ClutterActor:y-expand properties of #ClutterActor are set to %TRUE.
enum ActorAlign /* Version 1.10 */ {
   FILL = 0,
   START = 1,
   CENTER = 2,
   END = 3
}

// Bounding box of an actor. The coordinates of the top left and right bottom
// corners of an actor. The coordinates of the two points are expressed in
// pixels with sub-pixel precision
struct ActorBox {
   float x1, y1, x2, y2;


   // VERSION: 1.0
   // Allocates a new #ClutterActorBox using the passed coordinates
   // for the top left and bottom right points
   // 
   // clutter_actor_box_free() to free the resources
   // RETURNS: the newly allocated #ClutterActorBox. Use
   // <x_1>: X coordinate of the top left point
   // <y_1>: Y coordinate of the top left point
   // <x_2>: X coordinate of the bottom right point
   // <y_2>: Y coordinate of the bottom right point
   static ActorBox* /*new*/ new_()(float x_1, float y_1, float x_2, float y_2) nothrow {
      return clutter_actor_box_new(x_1, y_1, x_2, y_2);
   }
   static auto opCall()(float x_1, float y_1, float x_2, float y_2) {
      return clutter_actor_box_new(x_1, y_1, x_2, y_2);
   }

   // VERSION: 1.2
   // Clamps the components of @box to the nearest integer
   void clamp_to_pixel()() nothrow {
      clutter_actor_box_clamp_to_pixel(&this);
   }

   // VERSION: 1.0
   // Checks whether a point with @x, @y coordinates is contained
   // withing @box
   // RETURNS: %TRUE if the point is contained by the #ClutterActorBox
   // <x>: X coordinate of the point
   // <y>: Y coordinate of the point
   int contains()(float x, float y) nothrow {
      return clutter_actor_box_contains(&this, x, y);
   }

   // VERSION: 1.0
   // Copies @box
   // 
   // clutter_actor_box_free() to free the allocated resources
   // RETURNS: a newly allocated copy of #ClutterActorBox. Use
   ActorBox* /*new*/ copy()() nothrow {
      return clutter_actor_box_copy(&this);
   }

   // VERSION: 1.0
   // Checks @box_a and @box_b for equality
   // RETURNS: %TRUE if the passed #ClutterActorBox are equal
   // <box_b>: a #ClutterActorBox
   int equal(AT0)(AT0 /*ActorBox*/ box_b) nothrow {
      return clutter_actor_box_equal(&this, UpCast!(ActorBox*)(box_b));
   }

   // VERSION: 1.0
   // Frees a #ClutterActorBox allocated using clutter_actor_box_new()
   // or clutter_actor_box_copy()
   void free()() nothrow {
      clutter_actor_box_free(&this);
   }

   // VERSION: 1.0
   // Calculates the bounding box represented by the four vertices; for details
   // of the vertex array see clutter_actor_get_abs_allocation_vertices().
   // <verts>: array of four #ClutterVertex
   void from_vertices()(Vertex verts) nothrow {
      clutter_actor_box_from_vertices(&this, verts);
   }

   // VERSION: 1.0
   // Retrieves the area of @box
   // RETURNS: the area of a #ClutterActorBox, in pixels
   float get_area()() nothrow {
      return clutter_actor_box_get_area(&this);
   }

   // VERSION: 1.0
   // Retrieves the height of the @box
   // RETURNS: the height of the box
   float get_height()() nothrow {
      return clutter_actor_box_get_height(&this);
   }

   // VERSION: 1.0
   // Retrieves the origin of @box
   // <x>: return location for the X coordinate, or %NULL
   // <y>: return location for the Y coordinate, or %NULL
   void get_origin(AT0, AT1)(/*out*/ AT0 /*float*/ x=null, /*out*/ AT1 /*float*/ y=null) nothrow {
      clutter_actor_box_get_origin(&this, UpCast!(float*)(x), UpCast!(float*)(y));
   }

   // VERSION: 1.0
   // Retrieves the size of @box
   // <width>: return location for the width, or %NULL
   // <height>: return location for the height, or %NULL
   void get_size(AT0, AT1)(/*out*/ AT0 /*float*/ width=null, /*out*/ AT1 /*float*/ height=null) nothrow {
      clutter_actor_box_get_size(&this, UpCast!(float*)(width), UpCast!(float*)(height));
   }

   // VERSION: 1.0
   // Retrieves the width of the @box
   // RETURNS: the width of the box
   float get_width()() nothrow {
      return clutter_actor_box_get_width(&this);
   }

   // VERSION: 1.0
   // Retrieves the X coordinate of the origin of @box
   // RETURNS: the X coordinate of the origin
   float get_x()() nothrow {
      return clutter_actor_box_get_x(&this);
   }

   // VERSION: 1.0
   // Retrieves the Y coordinate of the origin of @box
   // RETURNS: the Y coordinate of the origin
   float get_y()() nothrow {
      return clutter_actor_box_get_y(&this);
   }

   // VERSION: 1.2
   // Interpolates between @initial and @final #ClutterActorBox<!-- -->es
   // using @progress
   // <final>: the final #ClutterActorBox
   // <progress>: the interpolation progress
   // <result>: return location for the interpolation
   void interpolate(AT0, AT1)(AT0 /*ActorBox*/ final_, double progress, /*out*/ AT1 /*ActorBox*/ result) nothrow {
      clutter_actor_box_interpolate(&this, UpCast!(ActorBox*)(final_), progress, UpCast!(ActorBox*)(result));
   }

   // VERSION: 1.6
   // Changes the origin of @box, maintaining the size of the #ClutterActorBox.
   // <x>: the X coordinate of the new origin
   // <y>: the Y coordinate of the new origin
   void set_origin()(float x, float y) nothrow {
      clutter_actor_box_set_origin(&this, x, y);
   }

   // VERSION: 1.6
   // Sets the size of @box, maintaining the origin of the #ClutterActorBox.
   // <width>: the new width
   // <height>: the new height
   void set_size()(float width, float height) nothrow {
      clutter_actor_box_set_size(&this, width, height);
   }

   // VERSION: 1.4
   // Unions the two boxes @a and @b and stores the result in @result.
   // <b>: the second #ClutterActorBox
   // <result>: the #ClutterActorBox representing a union of @a and @b
   void union_(AT0, AT1)(AT0 /*ActorBox*/ b, /*out*/ AT1 /*ActorBox*/ result) nothrow {
      clutter_actor_box_union(&this, UpCast!(ActorBox*)(b), UpCast!(ActorBox*)(result));
   }
}

// Base class for actors.
struct ActorClass {
   private GObject2.InitiallyUnownedClass parent_class;
   extern (C) void function (Actor* self) nothrow show;
   extern (C) void function (Actor* self) nothrow show_all;
   extern (C) void function (Actor* self) nothrow hide;
   extern (C) void function (Actor* self) nothrow hide_all;
   extern (C) void function (Actor* self) nothrow realize;
   extern (C) void function (Actor* self) nothrow unrealize;
   extern (C) void function (Actor* self) nothrow map;
   extern (C) void function (Actor* self) nothrow unmap;
   extern (C) void function (Actor* self) nothrow paint;
   extern (C) void function (Actor* actor, Actor* old_parent) nothrow parent_set;
   extern (C) void function (Actor* self) nothrow destroy;
   extern (C) void function (Actor* actor, Color* color) nothrow pick;
   extern (C) void function (Actor* actor, Actor* leaf_that_queued) nothrow queue_redraw;

   // <for_height>: available height when computing the preferred width, or a negative value to indicate that no height is defined
   // <min_width_p>: return location for minimum width, or %NULL
   // <natural_width_p>: return location for the natural width, or %NULL
   extern (C) void function (Actor* self, float for_height, /*out*/ float* min_width_p=null, /*out*/ float* natural_width_p=null) nothrow get_preferred_width;

   // <for_width>: available width to assume in computing desired height, or a negative value to indicate that no width is defined
   // <min_height_p>: return location for minimum height, or %NULL
   // <natural_height_p>: return location for natural height, or %NULL
   extern (C) void function (Actor* self, float for_width, /*out*/ float* min_height_p=null, /*out*/ float* natural_height_p=null) nothrow get_preferred_height;

   // <box>: new allocation of the actor, in parent-relative coordinates
   // <flags>: flags that control the allocation
   extern (C) void function (Actor* self, ActorBox* box, AllocationFlags flags) nothrow allocate;
   extern (C) void function (Actor* actor, Cogl.Matrix* matrix) nothrow apply_transform;
   extern (C) int function (Actor* actor, Event* event) nothrow event;
   extern (C) int function (Actor* actor, ButtonEvent* event) nothrow button_press_event;
   extern (C) int function (Actor* actor, ButtonEvent* event) nothrow button_release_event;
   extern (C) int function (Actor* actor, ScrollEvent* event) nothrow scroll_event;
   extern (C) int function (Actor* actor, KeyEvent* event) nothrow key_press_event;
   extern (C) int function (Actor* actor, KeyEvent* event) nothrow key_release_event;
   extern (C) int function (Actor* actor, MotionEvent* event) nothrow motion_event;
   extern (C) int function (Actor* actor, CrossingEvent* event) nothrow enter_event;
   extern (C) int function (Actor* actor, CrossingEvent* event) nothrow leave_event;
   extern (C) int function (Actor* actor, Event* event) nothrow captured_event;
   extern (C) void function (Actor* actor) nothrow key_focus_in;
   extern (C) void function (Actor* actor) nothrow key_focus_out;
   extern (C) void function (Actor* self) nothrow queue_relayout;
   // RETURNS: the #AtkObject associated with @actor
   extern (C) Atk.Object* function (Actor* self) nothrow get_accessible;
   extern (C) int function (Actor* actor, PaintVolume* volume) nothrow get_paint_volume;
   // RETURNS: %TRUE if the actor may have overlapping primitives, and
   extern (C) int function (Actor* self) nothrow has_overlaps;
   private void*[28] _padding_dummy;
}

// Flags used to signal the state of an actor.
enum ActorFlags {
   MAPPED = 2,
   REALIZED = 4,
   REACTIVE = 8,
   VISIBLE = 16,
   NO_LAYOUT = 32
}

// An iterator structure that allows to efficiently iterate over a
// section of the scene graph.
// 
// The contents of the <structname>ClutterActorIter</structname> structure
// are private and should only be accessed using the provided API.
struct ActorIter /* Version 1.10 */ {
   private void* dummy1, dummy2, dummy3;
   private int dummy4;
   private void* dummy5;


   // VERSION: 1.10
   // Initializes a #ClutterActorIter, which can then be used to iterate
   // efficiently over a section of the scene graph, and associates it
   // with @root.
   // 
   // Modifying the scene graph section that contains @root will invalidate
   // the iterator.
   // 
   // |[
   // ClutterActorIter iter;
   // ClutterActor *child;
   // 
   // clutter_actor_iter_init (&iter, container);
   // while (clutter_actor_iter_next (&iter, &child))
   // {
   // /&ast; do something with child &ast;/
   // }
   // ]|
   // <root>: a #ClutterActor
   void init(AT0)(AT0 /*Actor*/ root) nothrow {
      clutter_actor_iter_init(&this, UpCast!(Actor*)(root));
   }

   // VERSION: 1.10
   // Advances the @iter and retrieves the previous child of the root
   // #ClutterActor that was used to initialize the #ClutterActorIterator.
   // 
   // If the iterator can advance, this function returns %TRUE and sets the
   // @child argument.
   // 
   // If the iterator cannot advance, this function returns %FALSE, and
   // the contents of @child are undefined.
   // RETURNS: %TRUE if the iterator could advance, and %FALSE otherwise.
   // <child>: return location for a #ClutterActor
   int next(AT0)(/*out*/ AT0 /*Actor**/ child) nothrow {
      return clutter_actor_iter_next(&this, UpCast!(Actor**)(child));
   }
   int prev(AT0)(AT0 /*Actor**/ child) nothrow {
      return clutter_actor_iter_prev(&this, UpCast!(Actor**)(child));
   }

   // VERSION: 1.10
   // Safely removes the #ClutterActor currently pointer to by the iterator
   // from its parent.
   // 
   // This function can only be called after clutter_actor_iter_next() or
   // clutter_actor_iter_prev() returned %TRUE, and cannot be called more
   // than once for the same actor.
   // 
   // This function will call clutter_actor_remove_child() internally.
   void remove()() nothrow {
      clutter_actor_iter_remove(&this);
   }
}


// The <structname>ClutterActorMeta</structname> structure contains only
// private data and should be accessed using the provided API
struct ActorMeta /* : GObject.InitiallyUnowned */ /* Version 1.4 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance initiallyunowned;
   GObject2.InitiallyUnowned parent_instance;
   private ActorMetaPrivate* priv;


   // VERSION: 1.4
   // Retrieves a pointer to the #ClutterActor that owns @meta
   // RETURNS: a pointer to a #ClutterActor or %NULL
   Actor* get_actor()() nothrow {
      return clutter_actor_meta_get_actor(&this);
   }

   // VERSION: 1.4
   // Retrieves whether @meta is enabled
   // RETURNS: %TRUE if the #ClutterActorMeta instance is enabled
   int get_enabled()() nothrow {
      return clutter_actor_meta_get_enabled(&this);
   }

   // VERSION: 1.4
   // Retrieves the name set using clutter_actor_meta_set_name()
   // 
   // instance, or %NULL if none was set. The returned string is owned
   // by the #ClutterActorMeta instance and it should not be modified
   // or freed
   // RETURNS: the name of the #ClutterActorMeta
   char* get_name()() nothrow {
      return clutter_actor_meta_get_name(&this);
   }

   // VERSION: 1.4
   // Sets whether @meta should be enabled or not
   // <is_enabled>: whether @meta is enabled
   void set_enabled()(int is_enabled) nothrow {
      clutter_actor_meta_set_enabled(&this, is_enabled);
   }

   // VERSION: 1.4
   // Sets the name of @meta
   // 
   // The name can be used to identify the #ClutterActorMeta instance
   // <name>: the name of @meta
   void set_name(AT0)(AT0 /*char*/ name) nothrow {
      clutter_actor_meta_set_name(&this, toCString!(char*)(name));
   }
}


// The <structname>ClutterActorMetaClass</structname> structure contains
// only private data
struct ActorMetaClass /* Version 1.4 */ {
   private GObject2.InitiallyUnownedClass parent_class;
   extern (C) void function (ActorMeta* meta, Actor* actor) nothrow set_actor;
   extern (C) void function () nothrow _clutter_meta1;
   extern (C) void function () nothrow _clutter_meta2;
   extern (C) void function () nothrow _clutter_meta3;
   extern (C) void function () nothrow _clutter_meta4;
   extern (C) void function () nothrow _clutter_meta5;
   extern (C) void function () nothrow _clutter_meta6;
   extern (C) void function () nothrow _clutter_meta7;
}

struct ActorMetaPrivate {
}

struct ActorPrivate {
}

enum int AddFavorite = 269025081;
enum int Adiaeresis = 196;
enum int Agrave = 192;
enum int Ahook = 16785058;

// Specifies the axis on which #ClutterAlignConstraint should maintain
// the alignment.
enum AlignAxis /* Version 1.4 */ {
   X_AXIS = 0,
   Y_AXIS = 1,
   BOTH = 2
}

// <structname>ClutterAlignConstraint</structname> is an opaque structure
// whose members cannot be directly accesses
struct AlignConstraint /* : Constraint */ /* Version 1.4 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent constraint;
   Constraint method_parent;


   // VERSION: 1.4
   // Creates a new constraint, aligning a #ClutterActor's position with
   // regards of the size of the actor to @source, with the given
   // alignment @factor
   // RETURNS: the newly created #ClutterAlignConstraint
   // <source>: the #ClutterActor to use as the source of the alignment, or %NULL
   // <axis>: the axis to be used to compute the alignment
   // <factor>: the alignment factor, between 0.0 and 1.0
   static AlignConstraint* new_(AT0)(AT0 /*Actor*/ source, AlignAxis axis, float factor) nothrow {
      return clutter_align_constraint_new(UpCast!(Actor*)(source), axis, factor);
   }
   static auto opCall(AT0)(AT0 /*Actor*/ source, AlignAxis axis, float factor) {
      return clutter_align_constraint_new(UpCast!(Actor*)(source), axis, factor);
   }

   // VERSION: 1.4
   // Retrieves the value set using clutter_align_constraint_set_align_axis()
   // RETURNS: the alignment axis
   AlignAxis get_align_axis()() nothrow {
      return clutter_align_constraint_get_align_axis(&this);
   }

   // VERSION: 1.4
   // Retrieves the factor set using clutter_align_constraint_set_factor()
   // RETURNS: the alignment factor
   float get_factor()() nothrow {
      return clutter_align_constraint_get_factor(&this);
   }

   // VERSION: 1.4
   // Retrieves the source of the alignment
   // 
   // of the alignment
   // RETURNS: the #ClutterActor used as the source
   Actor* get_source()() nothrow {
      return clutter_align_constraint_get_source(&this);
   }

   // VERSION: 1.4
   // Sets the axis to which the alignment refers to
   // <axis>: the axis to which the alignment refers to
   void set_align_axis()(AlignAxis axis) nothrow {
      clutter_align_constraint_set_align_axis(&this, axis);
   }

   // VERSION: 1.4
   // Sets the alignment factor of the constraint
   // 
   // The factor depends on the #ClutterAlignConstraint:align-axis property
   // and it is a value between 0.0 (meaning left, when
   // #ClutterAlignConstraint:align-axis is set to %CLUTTER_ALIGN_X_AXIS; or
   // meaning top, when #ClutterAlignConstraint:align-axis is set to
   // %CLUTTER_ALIGN_Y_AXIS) and 1.0 (meaning right, when
   // #ClutterAlignConstraint:align-axis is set to %CLUTTER_ALIGN_X_AXIS; or
   // meaning bottom, when #ClutterAlignConstraint:align-axis is set to
   // %CLUTTER_ALIGN_Y_AXIS). A value of 0.5 aligns in the middle in either
   // cases
   // <factor>: the alignment factor, between 0.0 and 1.0
   void set_factor()(float factor) nothrow {
      clutter_align_constraint_set_factor(&this, factor);
   }

   // VERSION: 1.4
   // Sets the source of the alignment constraint
   // <source>: a #ClutterActor, or %NULL to unset the source
   void set_source(AT0)(AT0 /*Actor*/ source=null) nothrow {
      clutter_align_constraint_set_source(&this, UpCast!(Actor*)(source));
   }
}

struct AlignConstraintClass {
}


// Flags passed to the #ClutterActorClass.allocate() virtual function
// and to the clutter_actor_allocate() function.
enum AllocationFlags /* Version 1.0 */ {
   ALLOCATION_NONE = 0,
   ABSOLUTE_ORIGIN_CHANGED = 2,
   DELEGATE_LAYOUT = 4
}

// #ClutterAlpha combines a #ClutterTimeline and a function.
// The contents of the #ClutterAlpha structure are private and should
// only be accessed using the provided API.
struct Alpha /* : GObject.InitiallyUnowned */ /* Version 0.2 */ {
   mixin Scriptable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent initiallyunowned;
   GObject2.InitiallyUnowned parent;
   private AlphaPrivate* priv;


   // VERSION: 0.2
   // Creates a new #ClutterAlpha instance.  You must set a function
   // to compute the alpha value using clutter_alpha_set_func() and
   // bind a #ClutterTimeline object to the #ClutterAlpha instance
   // using clutter_alpha_set_timeline().
   // 
   // You should use the newly created #ClutterAlpha instance inside
   // a #ClutterBehaviour object.
   // RETURNS: the newly created empty #ClutterAlpha instance.
   static Alpha* new_()() nothrow {
      return clutter_alpha_new();
   }
   static auto opCall()() {
      return clutter_alpha_new();
   }

   // VERSION: 1.0
   // Creates a new #ClutterAlpha instance and sets the timeline
   // and animation mode.
   // 
   // See also clutter_alpha_set_timeline() and clutter_alpha_set_mode().
   // RETURNS: the newly created #ClutterAlpha
   // <timeline>: #ClutterTimeline timeline
   // <mode>: animation mode
   static Alpha* new_full(AT0)(AT0 /*Timeline*/ timeline, c_ulong mode) nothrow {
      return clutter_alpha_new_full(UpCast!(Timeline*)(timeline), mode);
   }
   static auto opCall(AT0)(AT0 /*Timeline*/ timeline, c_ulong mode) {
      return clutter_alpha_new_full(UpCast!(Timeline*)(timeline), mode);
   }

   // VERSION: 1.0
   // Creates a new #ClutterAlpha instances and sets the timeline
   // and the alpha function.
   // 
   // This function will not register @func as a global alpha function.
   // 
   // See also clutter_alpha_set_timeline() and clutter_alpha_set_func().
   // RETURNS: the newly created #ClutterAlpha
   // <timeline>: a #ClutterTimeline
   // <func>: a #ClutterAlphaFunc
   // <data>: data to pass to the function, or %NULL
   // <destroy>: function to call when removing the alpha function, or %NULL
   static Alpha* new_with_func(AT0, AT1)(AT0 /*Timeline*/ timeline, AlphaFunc func, AT1 /*void*/ data, GLib2.DestroyNotify destroy) nothrow {
      return clutter_alpha_new_with_func(UpCast!(Timeline*)(timeline), func, UpCast!(void*)(data), destroy);
   }
   static auto opCall(AT0, AT1)(AT0 /*Timeline*/ timeline, AlphaFunc func, AT1 /*void*/ data, GLib2.DestroyNotify destroy) {
      return clutter_alpha_new_with_func(UpCast!(Timeline*)(timeline), func, UpCast!(void*)(data), destroy);
   }

   // VERSION: 1.0
   // #GClosure variant of clutter_alpha_register_func().
   // 
   // Registers a global alpha function and returns its logical id
   // to be used by clutter_alpha_set_mode() or by #ClutterAnimation.
   // 
   // The logical id is always greater than %CLUTTER_ANIMATION_LAST.
   // RETURNS: the logical id of the alpha function
   // <closure>: a #GClosure
   static c_ulong register_closure(AT0)(AT0 /*GObject2.Closure*/ closure) nothrow {
      return clutter_alpha_register_closure(UpCast!(GObject2.Closure*)(closure));
   }

   // Unintrospectable function: register_func() / clutter_alpha_register_func()
   // VERSION: 1.0
   // Registers a global alpha function and returns its logical id
   // to be used by clutter_alpha_set_mode() or by #ClutterAnimation.
   // 
   // The logical id is always greater than %CLUTTER_ANIMATION_LAST.
   // RETURNS: the logical id of the alpha function
   // <func>: a #ClutterAlphaFunc
   // <data>: user data to pass to @func, or %NULL
   static c_ulong register_func(AT0)(AlphaFunc func, AT0 /*void*/ data) nothrow {
      return clutter_alpha_register_func(func, UpCast!(void*)(data));
   }

   // VERSION: 0.2
   // Query the current alpha value.
   // RETURNS: The current alpha value for the alpha
   double get_alpha()() nothrow {
      return clutter_alpha_get_alpha(&this);
   }

   // VERSION: 1.0
   // Retrieves the #ClutterAnimationMode used by @alpha.
   // RETURNS: the animation mode
   c_ulong get_mode()() nothrow {
      return clutter_alpha_get_mode(&this);
   }

   // VERSION: 0.2
   // Gets the #ClutterTimeline bound to @alpha.
   // RETURNS: a #ClutterTimeline instance
   Timeline* get_timeline()() nothrow {
      return clutter_alpha_get_timeline(&this);
   }

   // VERSION: 0.8
   // Sets the #GClosure used to compute the alpha value at each
   // frame of the #ClutterTimeline bound to @alpha.
   // <closure>: A #GClosure
   void set_closure(AT0)(AT0 /*GObject2.Closure*/ closure) nothrow {
      clutter_alpha_set_closure(&this, UpCast!(GObject2.Closure*)(closure));
   }

   // VERSION: 0.2
   // Sets the #ClutterAlphaFunc function used to compute
   // the alpha value at each frame of the #ClutterTimeline
   // bound to @alpha.
   // 
   // This function will not register @func as a global alpha function.
   // <func>: A #ClutterAlphaFunc
   // <data>: user data to be passed to the alpha function, or %NULL
   // <destroy>: notify function used when disposing the alpha function
   void set_func(AT0)(AlphaFunc func, AT0 /*void*/ data, GLib2.DestroyNotify destroy) nothrow {
      clutter_alpha_set_func(&this, func, UpCast!(void*)(data), destroy);
   }

   // VERSION: 1.0
   // Sets the progress function of @alpha using the symbolic value
   // of @mode, as taken by the #ClutterAnimationMode enumeration or
   // using the value returned by clutter_alpha_register_func().
   // <mode>: a #ClutterAnimationMode
   void set_mode()(c_ulong mode) nothrow {
      clutter_alpha_set_mode(&this, mode);
   }

   // VERSION: 0.2
   // Binds @alpha to @timeline.
   // <timeline>: A #ClutterTimeline
   void set_timeline(AT0)(AT0 /*Timeline*/ timeline) nothrow {
      clutter_alpha_set_timeline(&this, UpCast!(Timeline*)(timeline));
   }
}

// Base class for #ClutterAlpha
struct AlphaClass /* Version 0.2 */ {
   private GObject2.InitiallyUnownedClass parent_class;
   extern (C) void function () nothrow _clutter_alpha_1;
   extern (C) void function () nothrow _clutter_alpha_2;
   extern (C) void function () nothrow _clutter_alpha_3;
   extern (C) void function () nothrow _clutter_alpha_4;
   extern (C) void function () nothrow _clutter_alpha_5;
}


// VERSION: 0.2
// A function returning a value depending on the position of
// the #ClutterTimeline bound to @alpha.
// RETURNS: a floating point value
// <alpha>: a #ClutterAlpha
// <user_data>: user data passed to the function
extern (C) alias double function (Alpha* alpha, void* user_data) nothrow AlphaFunc;

struct AlphaPrivate {
}

enum int Alt_L = 65513;
enum int Alt_R = 65514;
enum int Amacron = 960;

// #ClutterAnimatable is an opaque structure whose members cannot be directly
// accessed
struct Animatable /* Interface */ /* Version 1.0 */ {
   mixin template __interface__() {
      // VERSION: 1.0
      // DEPRECATED (v1.8) method: animate_property - Use clutter_animatable_interpolate_value()
      // Calls the animate_property() virtual function for @animatable.
      // 
      // The @initial_value and @final_value #GValue<!-- -->s must contain
      // the same type; @value must have been initialized to the same
      // type of @initial_value and @final_value.
      // 
      // All implementation of the #ClutterAnimatable interface must
      // implement this function.
      // 
      // be applied to the #ClutterAnimatable, and %FALSE otherwise
      // 
      // 
      // instead
      // RETURNS: %TRUE if the value has been validated and can
      // <animation>: a #ClutterAnimation
      // <property_name>: the name of the animated property
      // <initial_value>: the initial value of the animation interval
      // <final_value>: the final value of the animation interval
      // <progress>: the progress factor
      // <value>: return location for the animation value
      int animate_property(AT0, AT1, AT2, AT3, AT4)(AT0 /*Animation*/ animation, AT1 /*char*/ property_name, AT2 /*GObject2.Value*/ initial_value, AT3 /*GObject2.Value*/ final_value, double progress, AT4 /*GObject2.Value*/ value) nothrow {
         return clutter_animatable_animate_property(cast(Animatable*)&this, UpCast!(Animation*)(animation), toCString!(char*)(property_name), UpCast!(GObject2.Value*)(initial_value), UpCast!(GObject2.Value*)(final_value), progress, UpCast!(GObject2.Value*)(value));
      }

      // VERSION: 1.4
      // Finds the #GParamSpec for @property_name
      // 
      // or %NULL
      // RETURNS: The #GParamSpec for the given property
      // <property_name>: the name of the animatable property to find
      GObject2.ParamSpec* find_property(AT0)(AT0 /*char*/ property_name) nothrow {
         return clutter_animatable_find_property(cast(Animatable*)&this, toCString!(char*)(property_name));
      }

      // VERSION: 1.4
      // Retrieves the current state of @property_name and sets @value with it
      // <property_name>: the name of the animatable property to retrieve
      // <value>: a #GValue initialized to the type of the property to retrieve
      void get_initial_state(AT0, AT1)(AT0 /*char*/ property_name, AT1 /*GObject2.Value*/ value) nothrow {
         clutter_animatable_get_initial_state(cast(Animatable*)&this, toCString!(char*)(property_name), UpCast!(GObject2.Value*)(value));
      }

      // VERSION: 1.8
      // Asks a #ClutterAnimatable implementation to interpolate a
      // a named property between the initial and final values of
      // a #ClutterInterval, using @progress as the interpolation
      // value, and store the result inside @value.
      // 
      // This function should be used for every property animation
      // involving #ClutterAnimatable<!-- -->s.
      // 
      // This function replaces clutter_animatable_animate_property().
      // 
      // and %FALSE otherwise
      // RETURNS: %TRUE if the interpolation was successful,
      // <property_name>: the name of the property to interpolate
      // <interval>: a #ClutterInterval with the animation range
      // <progress>: the progress to use to interpolate between the initial and final values of the @interval
      // <value>: return location for an initialized #GValue using the same type of the @interval
      int interpolate_value(AT0, AT1, AT2)(AT0 /*char*/ property_name, AT1 /*Interval*/ interval, double progress, /*out*/ AT2 /*GObject2.Value*/ value) nothrow {
         return clutter_animatable_interpolate_value(cast(Animatable*)&this, toCString!(char*)(property_name), UpCast!(Interval*)(interval), progress, UpCast!(GObject2.Value*)(value));
      }

      // VERSION: 1.4
      // Sets the current state of @property_name to @value
      // <property_name>: the name of the animatable property to set
      // <value>: the value of the animatable property to set
      void set_final_state(AT0, AT1)(AT0 /*char*/ property_name, AT1 /*GObject2.Value*/ value) nothrow {
         clutter_animatable_set_final_state(cast(Animatable*)&this, toCString!(char*)(property_name), UpCast!(GObject2.Value*)(value));
      }
   }
   mixin __interface__;
}


// Base interface for #GObject<!-- -->s that can be animated by a
// a #ClutterAnimation.
struct AnimatableIface /* Version 1.0 */ {
   private GObject2.TypeInterface parent_iface;

   // RETURNS: %TRUE if the value has been validated and can
   // <animation>: a #ClutterAnimation
   // <property_name>: the name of the animated property
   // <initial_value>: the initial value of the animation interval
   // <final_value>: the final value of the animation interval
   // <progress>: the progress factor
   // <value>: return location for the animation value
   extern (C) int function (Animatable* animatable, Animation* animation, char* property_name, GObject2.Value* initial_value, GObject2.Value* final_value, double progress, GObject2.Value* value) nothrow animate_property;

   // RETURNS: The #GParamSpec for the given property
   // <property_name>: the name of the animatable property to find
   extern (C) GObject2.ParamSpec* function (Animatable* animatable, char* property_name) nothrow find_property;

   // <property_name>: the name of the animatable property to retrieve
   // <value>: a #GValue initialized to the type of the property to retrieve
   extern (C) void function (Animatable* animatable, char* property_name, GObject2.Value* value) nothrow get_initial_state;

   // <property_name>: the name of the animatable property to set
   // <value>: the value of the animatable property to set
   extern (C) void function (Animatable* animatable, char* property_name, GObject2.Value* value) nothrow set_final_state;

   // RETURNS: %TRUE if the interpolation was successful,
   // <property_name>: the name of the property to interpolate
   // <interval>: a #ClutterInterval with the animation range
   // <progress>: the progress to use to interpolate between the initial and final values of the @interval
   // <value>: return location for an initialized #GValue using the same type of the @interval
   extern (C) int function (Animatable* animatable, char* property_name, Interval* interval, double progress, /*out*/ GObject2.Value* value) nothrow interpolate_value;
}


// The #ClutterAnimation structure contains only private data and should
// be accessed using the provided functions.
struct Animation /* : GObject.Object */ /* Version 1.0 */ {
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private AnimationPrivate* priv;


   // VERSION: 1.0
   // Creates a new #ClutterAnimation instance. You should set the
   // #GObject to be animated using clutter_animation_set_object(),
   // set the duration with clutter_animation_set_duration() and the
   // easing mode using clutter_animation_set_mode().
   // 
   // Use clutter_animation_bind() or clutter_animation_bind_interval()
   // to define the properties to be animated. The interval and the
   // animated properties can be updated at runtime.
   // 
   // The clutter_actor_animate() and relative family of functions provide
   // an easy way to animate a #ClutterActor and automatically manage the
   // lifetime of a #ClutterAnimation instance, so you should consider using
   // those functions instead of manually creating an animation.
   // 
   // to release the associated resources
   // RETURNS: the newly created #ClutterAnimation. Use g_object_unref()
   static Animation* /*new*/ new_()() nothrow {
      return clutter_animation_new();
   }
   static auto opCall()() {
      return clutter_animation_new();
   }

   // VERSION: 1.0
   // Adds a single property with name @property_name to the
   // animation @animation.  For more information about animations,
   // see clutter_actor_animate().
   // 
   // This method returns the animation primarily to make chained
   // calls convenient in language bindings.
   // RETURNS: The animation itself.
   // <property_name>: the property to control
   // <final>: The final value of the property
   Animation* bind(AT0, AT1)(AT0 /*char*/ property_name, AT1 /*GObject2.Value*/ final_) nothrow {
      return clutter_animation_bind(&this, toCString!(char*)(property_name), UpCast!(GObject2.Value*)(final_));
   }

   // VERSION: 1.0
   // Binds @interval to the @property_name of the #GObject
   // attached to @animation. The #ClutterAnimation will take
   // ownership of the passed #ClutterInterval.  For more information
   // about animations, see clutter_actor_animate().
   // 
   // If you need to update the interval instance use
   // clutter_animation_update_interval() instead.
   // RETURNS: The animation itself.
   // <property_name>: the property to control
   // <interval>: a #ClutterInterval
   Animation* bind_interval(AT0, AT1)(AT0 /*char*/ property_name, AT1 /*Interval*/ interval) nothrow {
      return clutter_animation_bind_interval(&this, toCString!(char*)(property_name), UpCast!(Interval*)(interval));
   }

   // VERSION: 1.0
   // Emits the ::completed signal on @animation
   // 
   // When using this function with a #ClutterAnimation created
   // by the clutter_actor_animate() family of functions, @animation
   // will be unreferenced and it will not be valid anymore,
   // unless g_object_ref() was called before calling this function
   // or unless a reference was taken inside a handler for the
   // #ClutterAnimation::completed signal
   void completed()() nothrow {
      clutter_animation_completed(&this);
   }

   // VERSION: 1.0
   // Retrieves the #ClutterAlpha used by @animation.
   // RETURNS: the alpha object used by the animation
   Alpha* get_alpha()() nothrow {
      return clutter_animation_get_alpha(&this);
   }

   // VERSION: 1.0
   // Retrieves the duration of @animation, in milliseconds.
   // RETURNS: the duration of the animation
   uint get_duration()() nothrow {
      return clutter_animation_get_duration(&this);
   }

   // VERSION: 1.0
   // Retrieves the #ClutterInterval associated to @property_name
   // inside @animation.
   // 
   // property with the same name was found. The returned interval is
   // owned by the #ClutterAnimation and should not be unreferenced
   // RETURNS: a #ClutterInterval or %NULL if no
   // <property_name>: name of the property
   Interval* get_interval(AT0)(AT0 /*char*/ property_name) nothrow {
      return clutter_animation_get_interval(&this, toCString!(char*)(property_name));
   }

   // VERSION: 1.0
   // Retrieves whether @animation is looping.
   // RETURNS: %TRUE if the animation is looping
   int get_loop()() nothrow {
      return clutter_animation_get_loop(&this);
   }

   // VERSION: 1.0
   // Retrieves the animation mode of @animation, as set by
   // clutter_animation_set_mode().
   // RETURNS: the mode for the animation
   c_ulong get_mode()() nothrow {
      return clutter_animation_get_mode(&this);
   }

   // VERSION: 1.0
   // Retrieves the #GObject attached to @animation.
   // RETURNS: a #GObject
   GObject2.Object* get_object()() nothrow {
      return clutter_animation_get_object(&this);
   }

   // VERSION: 1.0
   // Retrieves the #ClutterTimeline used by @animation
   // RETURNS: the timeline used by the animation
   Timeline* get_timeline()() nothrow {
      return clutter_animation_get_timeline(&this);
   }

   // VERSION: 1.0
   // Checks whether @animation is controlling @property_name.
   // 
   // #ClutterAnimation, %FALSE otherwise
   // RETURNS: %TRUE if the property is animated by the
   // <property_name>: name of the property
   int has_property(AT0)(AT0 /*char*/ property_name) nothrow {
      return clutter_animation_has_property(&this, toCString!(char*)(property_name));
   }

   // VERSION: 1.0
   // Sets @alpha as the #ClutterAlpha used by @animation.
   // 
   // If @alpha is not %NULL, the #ClutterAnimation will take ownership
   // of the #ClutterAlpha instance.
   // <alpha>: a #ClutterAlpha, or %NULL to unset the current #ClutterAlpha
   void set_alpha(AT0)(AT0 /*Alpha*/ alpha) nothrow {
      clutter_animation_set_alpha(&this, UpCast!(Alpha*)(alpha));
   }

   // VERSION: 1.0
   // Sets the duration of @animation in milliseconds.
   // 
   // This function will set #ClutterAnimation:alpha and
   // #ClutterAnimation:timeline if needed.
   // <msecs>: the duration in milliseconds
   void set_duration()(uint msecs) nothrow {
      clutter_animation_set_duration(&this, msecs);
   }

   // VERSION: 1.0
   // Sets whether @animation should loop over itself once finished.
   // 
   // A looping #ClutterAnimation will not emit the #ClutterAnimation::completed
   // signal when finished.
   // 
   // This function will set #ClutterAnimation:alpha and
   // #ClutterAnimation:timeline if needed.
   // <loop>: %TRUE if the animation should loop
   void set_loop()(int loop) nothrow {
      clutter_animation_set_loop(&this, loop);
   }

   // VERSION: 1.0
   // Sets the animation @mode of @animation. The animation @mode is
   // a logical id, either coming from the #ClutterAnimationMode enumeration
   // or the return value of clutter_alpha_register_func().
   // 
   // This function will also set #ClutterAnimation:alpha if needed.
   // <mode>: an animation mode logical id
   void set_mode()(c_ulong mode) nothrow {
      clutter_animation_set_mode(&this, mode);
   }

   // VERSION: 1.0
   // Attaches @animation to @object. The #ClutterAnimation will take a
   // reference on @object.
   // <object>: a #GObject
   void set_object(AT0)(AT0 /*GObject2.Object*/ object) nothrow {
      clutter_animation_set_object(&this, UpCast!(GObject2.Object*)(object));
   }

   // VERSION: 1.0
   // Sets the #ClutterTimeline used by @animation.
   // <timeline>: a #ClutterTimeline, or %NULL to unset the current #ClutterTimeline
   void set_timeline(AT0)(AT0 /*Timeline*/ timeline) nothrow {
      clutter_animation_set_timeline(&this, UpCast!(Timeline*)(timeline));
   }

   // VERSION: 1.0
   // Removes @property_name from the list of animated properties.
   // <property_name>: name of the property
   void unbind_property(AT0)(AT0 /*char*/ property_name) nothrow {
      clutter_animation_unbind_property(&this, toCString!(char*)(property_name));
   }

   // VERSION: 1.0
   // Updates the @final value of the interval for @property_name
   // RETURNS: The animation itself.
   // <property_name>: name of the property
   // <final>: The final value of the property
   Animation* update(AT0, AT1)(AT0 /*char*/ property_name, AT1 /*GObject2.Value*/ final_) nothrow {
      return clutter_animation_update(&this, toCString!(char*)(property_name), UpCast!(GObject2.Value*)(final_));
   }

   // VERSION: 1.0
   // Changes the @interval for @property_name. The #ClutterAnimation
   // will take ownership of the passed #ClutterInterval.
   // <property_name>: name of the property
   // <interval>: a #ClutterInterval
   void update_interval(AT0, AT1)(AT0 /*char*/ property_name, AT1 /*Interval*/ interval) nothrow {
      clutter_animation_update_interval(&this, toCString!(char*)(property_name), UpCast!(Interval*)(interval));
   }

   // VERSION: 1.0
   // The ::completed signal is emitted once the animation has
   // been completed.
   // 
   // The @animation instance is guaranteed to be valid for the entire
   // duration of the signal emission chain.
   extern (C) alias static void function (Animation* this_, void* user_data=null) nothrow signal_completed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"completed", CB/*:signal_completed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_completed)||_ttmm!(CB, signal_completed)()) {
      return signal_connect_data!()(&this, cast(char*)"completed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.0
   // The ::started signal is emitted once the animation has been
   // started
   extern (C) alias static void function (Animation* this_, void* user_data=null) nothrow signal_started;
   ulong signal_connect(string name:"started", CB/*:signal_started*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_started)||_ttmm!(CB, signal_started)()) {
      return signal_connect_data!()(&this, cast(char*)"started",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}


// The #ClutterAnimationClass structure contains only private data and
// should be accessed using the provided functions.
struct AnimationClass /* Version 1.0 */ {
   private GObject2.ObjectClass parent_class;
   extern (C) void function (Animation* animation) nothrow started;
   extern (C) void function (Animation* animation) nothrow completed;
   extern (C) void function () nothrow _clutter_reserved1;
   extern (C) void function () nothrow _clutter_reserved2;
   extern (C) void function () nothrow _clutter_reserved3;
   extern (C) void function () nothrow _clutter_reserved4;
   extern (C) void function () nothrow _clutter_reserved5;
   extern (C) void function () nothrow _clutter_reserved6;
   extern (C) void function () nothrow _clutter_reserved7;
   extern (C) void function () nothrow _clutter_reserved8;
}


// The animation modes used by #ClutterAlpha and #ClutterAnimation. This
// enumeration can be expanded in later versions of Clutter. See the
// #ClutterAlpha documentation for a graph of all the animation modes.
// 
// Every global alpha function registered using clutter_alpha_register_func()
// or clutter_alpha_register_closure() will have a logical id greater than
// %CLUTTER_ANIMATION_LAST.
enum AnimationMode /* Version 1.0 */ {
   CUSTOM_MODE = 0,
   LINEAR = 1,
   EASE_IN_QUAD = 2,
   EASE_OUT_QUAD = 3,
   EASE_IN_OUT_QUAD = 4,
   EASE_IN_CUBIC = 5,
   EASE_OUT_CUBIC = 6,
   EASE_IN_OUT_CUBIC = 7,
   EASE_IN_QUART = 8,
   EASE_OUT_QUART = 9,
   EASE_IN_OUT_QUART = 10,
   EASE_IN_QUINT = 11,
   EASE_OUT_QUINT = 12,
   EASE_IN_OUT_QUINT = 13,
   EASE_IN_SINE = 14,
   EASE_OUT_SINE = 15,
   EASE_IN_OUT_SINE = 16,
   EASE_IN_EXPO = 17,
   EASE_OUT_EXPO = 18,
   EASE_IN_OUT_EXPO = 19,
   EASE_IN_CIRC = 20,
   EASE_OUT_CIRC = 21,
   EASE_IN_OUT_CIRC = 22,
   EASE_IN_ELASTIC = 23,
   EASE_OUT_ELASTIC = 24,
   EASE_IN_OUT_ELASTIC = 25,
   EASE_IN_BACK = 26,
   EASE_OUT_BACK = 27,
   EASE_IN_OUT_BACK = 28,
   EASE_IN_BOUNCE = 29,
   EASE_OUT_BOUNCE = 30,
   EASE_IN_OUT_BOUNCE = 31,
   ANIMATION_LAST = 32
}
struct AnimationPrivate {
}


// The #ClutterAnimator structure contains only private data and
// should be accessed using the provided API
struct Animator /* : GObject.Object */ /* Version 1.2 */ {
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private AnimatorPrivate* priv;


   // VERSION: 1.2
   // Creates a new #ClutterAnimator instance
   // RETURNS: a new #ClutterAnimator.
   static Animator* /*new*/ new_()() nothrow {
      return clutter_animator_new();
   }
   static auto opCall()() {
      return clutter_animator_new();
   }

   // VERSION: 1.2
   // Compute the value for a managed property at a given progress.
   // 
   // If the property is an ease-in property, the current value of the property
   // on the object will be used as the starting point for computation.
   // 
   // an error occurs or the progress is before any of the keys) %FALSE is
   // returned and the #GValue is left untouched
   // RETURNS: %TRUE if the computation yields has a value, otherwise (when
   // <object>: a #GObject
   // <property_name>: the name of the property on object to check
   // <progress>: a value between 0.0 and 1.0
   // <value>: an initialized value to store the computed result
   int compute_value(AT0, AT1, AT2)(AT0 /*GObject2.Object*/ object, AT1 /*char*/ property_name, double progress, AT2 /*GObject2.Value*/ value) nothrow {
      return clutter_animator_compute_value(&this, UpCast!(GObject2.Object*)(object), toCString!(char*)(property_name), progress, UpCast!(GObject2.Value*)(value));
   }

   // VERSION: 1.2
   // Retrieves the current duration of an animator
   // RETURNS: the duration of the animation, in milliseconds
   uint get_duration()() nothrow {
      return clutter_animator_get_duration(&this);
   }

   // VERSION: 1.2
   // Returns a list of pointers to opaque structures with accessor functions
   // that describe the keys added to an animator.
   // 
   // list of #ClutterAnimatorKey<!-- -->s; the contents of the list are owned
   // by the #ClutterAnimator, but you should free the returned list when done,
   // using g_list_free()
   // RETURNS: a
   // <object>: a #GObject to search for, or %NULL for all objects
   // <property_name>: a specific property name to query for, or %NULL for all properties
   // <progress>: a specific progress to search for, or a negative value for all progresses
   GLib2.List* /*new container*/ get_keys(AT0, AT1)(AT0 /*GObject2.Object*/ object, AT1 /*char*/ property_name, double progress) nothrow {
      return clutter_animator_get_keys(&this, UpCast!(GObject2.Object*)(object), toCString!(char*)(property_name), progress);
   }

   // VERSION: 1.2
   // Get the timeline hooked up for driving the #ClutterAnimator
   // RETURNS: the #ClutterTimeline that drives the animator
   Timeline* get_timeline()() nothrow {
      return clutter_animator_get_timeline(&this);
   }

   // VERSION: 1.2
   // Checks if a property value is to be eased into the animation.
   // RETURNS: %TRUE if the property is eased in
   // <object>: a #GObject
   // <property_name>: the name of a property on object
   int property_get_ease_in(AT0, AT1)(AT0 /*GObject2.Object*/ object, AT1 /*char*/ property_name) nothrow {
      return clutter_animator_property_get_ease_in(&this, UpCast!(GObject2.Object*)(object), toCString!(char*)(property_name));
   }

   // VERSION: 1.2
   // Get the interpolation used by animator for a property on a particular
   // object.
   // RETURNS: a ClutterInterpolation value.
   // <object>: a #GObject
   // <property_name>: the name of a property on object
   Interpolation property_get_interpolation(AT0, AT1)(AT0 /*GObject2.Object*/ object, AT1 /*char*/ property_name) nothrow {
      return clutter_animator_property_get_interpolation(&this, UpCast!(GObject2.Object*)(object), toCString!(char*)(property_name));
   }

   // VERSION: 1.2
   // Sets whether a property value is to be eased into the animation.
   // <object>: a #GObject
   // <property_name>: the name of a property on object
   // <ease_in>: we are going to be easing in this property
   void property_set_ease_in(AT0, AT1)(AT0 /*GObject2.Object*/ object, AT1 /*char*/ property_name, int ease_in) nothrow {
      clutter_animator_property_set_ease_in(&this, UpCast!(GObject2.Object*)(object), toCString!(char*)(property_name), ease_in);
   }

   // VERSION: 1.2
   // Set the interpolation method to use, %CLUTTER_INTERPOLATION_LINEAR causes
   // the values to linearly change between the values, and
   // %CLUTTER_INTERPOLATION_CUBIC causes the values to smoothly change between
   // the values.
   // <object>: a #GObject
   // <property_name>: the name of a property on object
   // <interpolation>: the #ClutterInterpolation to use
   void property_set_interpolation(AT0, AT1)(AT0 /*GObject2.Object*/ object, AT1 /*char*/ property_name, Interpolation interpolation) nothrow {
      clutter_animator_property_set_interpolation(&this, UpCast!(GObject2.Object*)(object), toCString!(char*)(property_name), interpolation);
   }

   // VERSION: 1.2
   // Removes all keys matching the conditions specificed in the arguments.
   // <object>: a #GObject to search for, or %NULL for all
   // <property_name>: a specific property name to query for, or %NULL for all
   // <progress>: a specific progress to search for or a negative value for all
   void remove_key(AT0, AT1)(AT0 /*GObject2.Object*/ object, AT1 /*char*/ property_name, double progress) nothrow {
      clutter_animator_remove_key(&this, UpCast!(GObject2.Object*)(object), toCString!(char*)(property_name), progress);
   }

   // Unintrospectable method: set() / clutter_animator_set()
   // VERSION: 1.2
   // Adds multiple keys to a #ClutterAnimator, specifying the value a given
   // property should have at a given progress of the animation. The mode
   // specified is the mode used when going to this key from the previous key of
   // the @property_name
   // 
   // If a given (object, property, progress) tuple already exist the mode and
   // value will be replaced with the new values.
   // <first_object>: a #GObject
   // <first_property_name>: the property to specify a key for
   // <first_mode>: the id of the alpha function to use
   // <first_progress>: at which stage of the animation this value applies; the range is a normalized floating point value between 0 and 1
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_animator_set set; // Variadic
   +/

   // VERSION: 1.2
   // Runs the timeline of the #ClutterAnimator with a duration in msecs
   // as specified.
   // <duration>: milliseconds a run of the animator should last.
   void set_duration()(uint duration) nothrow {
      clutter_animator_set_duration(&this, duration);
   }

   // VERSION: 1.2
   // Sets a single key in the #ClutterAnimator for the @property_name of
   // @object at @progress.
   // 
   // See also: clutter_animator_set()
   // RETURNS: The animator instance
   // <object>: a #GObject
   // <property_name>: the property to specify a key for
   // <mode>: the id of the alpha function to use
   // <progress>: the normalized range at which stage of the animation this value applies
   // <value>: the value property_name should have at progress.
   Animator* set_key(AT0, AT1, AT2)(AT0 /*GObject2.Object*/ object, AT1 /*char*/ property_name, uint mode, double progress, AT2 /*GObject2.Value*/ value) nothrow {
      return clutter_animator_set_key(&this, UpCast!(GObject2.Object*)(object), toCString!(char*)(property_name), mode, progress, UpCast!(GObject2.Value*)(value));
   }

   // VERSION: 1.2
   // Sets an external timeline that will be used for driving the animation
   // <timeline>: a #ClutterTimeline
   void set_timeline(AT0)(AT0 /*Timeline*/ timeline) nothrow {
      clutter_animator_set_timeline(&this, UpCast!(Timeline*)(timeline));
   }

   // VERSION: 1.2
   // Start the ClutterAnimator, this is a thin wrapper that rewinds
   // and starts the animators current timeline.
   // 
   // the animator. The returned timeline is owned by the #ClutterAnimator
   // and it should not be unreferenced
   // RETURNS: the #ClutterTimeline that drives
   Timeline* start()() nothrow {
      return clutter_animator_start(&this);
   }
}

// The #ClutterAnimatorClass structure contains only private data
struct AnimatorClass /* Version 1.2 */ {
   private GObject2.ObjectClass parent_class;
   private void*[16] _padding_dummy;
}

// A key frame inside a #ClutterAnimator
struct AnimatorKey /* Version 1.2 */ {

   // VERSION: 1.2
   // Retrieves the mode of a #ClutterAnimator key, for the first key of a
   // property for an object this represents the whether the animation is
   // open ended and or curved for the remainding keys for the property it
   // represents the easing mode.
   // RETURNS: the mode of a #ClutterAnimatorKey
   c_ulong get_mode()() nothrow {
      return clutter_animator_key_get_mode(&this);
   }

   // VERSION: 1.2
   // Retrieves the object a key applies to.
   // RETURNS: the object an animator_key exist for.
   GObject2.Object* get_object()() nothrow {
      return clutter_animator_key_get_object(&this);
   }

   // VERSION: 1.2
   // Retrieves the progress of an clutter_animator_key
   // RETURNS: the progress defined for a #ClutterAnimator key.
   double get_progress()() nothrow {
      return clutter_animator_key_get_progress(&this);
   }

   // VERSION: 1.2
   // Retrieves the name of the property a key applies to.
   // RETURNS: the name of the property an animator_key exist for.
   char* get_property_name()() nothrow {
      return clutter_animator_key_get_property_name(&this);
   }

   // VERSION: 1.2
   // Retrieves the #GType of the property a key applies to
   // 
   // You can use this type to initialize the #GValue to pass to
   // clutter_animator_key_get_value()
   // RETURNS: the #GType of the property
   Type get_property_type()() nothrow {
      return clutter_animator_key_get_property_type(&this);
   }

   // VERSION: 1.2
   // Retrieves a copy of the value for a #ClutterAnimatorKey.
   // 
   // The passed in #GValue needs to be already initialized for the value
   // type of the key or to a type that allow transformation from the value
   // type of the key.
   // 
   // Use g_value_unset() when done.
   // 
   // %FALSE otherwise
   // RETURNS: %TRUE if the passed #GValue was successfully set, and
   // <value>: a #GValue initialized with the correct type for the animator key
   int get_value(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
      return clutter_animator_key_get_value(&this, UpCast!(GObject2.Value*)(value));
   }
}

struct AnimatorPrivate {
}

// Common members for a #ClutterEvent
struct AnyEvent /* Version 0.2 */ {
   EventType type;
   uint time;
   EventFlags flags;
   Stage* stage;
   Actor* source;
}

enum int Aogonek = 417;
enum int ApplicationLeft = 269025104;
enum int ApplicationRight = 269025105;
enum int Arabic_0 = 16778848;
enum int Arabic_1 = 16778849;
enum int Arabic_2 = 16778850;
enum int Arabic_3 = 16778851;
enum int Arabic_4 = 16778852;
enum int Arabic_5 = 16778853;
enum int Arabic_6 = 16778854;
enum int Arabic_7 = 16778855;
enum int Arabic_8 = 16778856;
enum int Arabic_9 = 16778857;
enum int Arabic_ain = 1497;
enum int Arabic_alef = 1479;
enum int Arabic_alefmaksura = 1513;
enum int Arabic_beh = 1480;
enum int Arabic_comma = 1452;
enum int Arabic_dad = 1494;
enum int Arabic_dal = 1487;
enum int Arabic_damma = 1519;
enum int Arabic_dammatan = 1516;
enum int Arabic_ddal = 16778888;
enum int Arabic_farsi_yeh = 16778956;
enum int Arabic_fatha = 1518;
enum int Arabic_fathatan = 1515;
enum int Arabic_feh = 1505;
enum int Arabic_fullstop = 16778964;
enum int Arabic_gaf = 16778927;
enum int Arabic_ghain = 1498;
enum int Arabic_ha = 1511;
enum int Arabic_hah = 1485;
enum int Arabic_hamza = 1473;
enum int Arabic_hamza_above = 16778836;
enum int Arabic_hamza_below = 16778837;
enum int Arabic_hamzaonalef = 1475;
enum int Arabic_hamzaonwaw = 1476;
enum int Arabic_hamzaonyeh = 1478;
enum int Arabic_hamzaunderalef = 1477;
enum int Arabic_heh = 1511;
enum int Arabic_heh_doachashmee = 16778942;
enum int Arabic_heh_goal = 16778945;
enum int Arabic_jeem = 1484;
enum int Arabic_jeh = 16778904;
enum int Arabic_kaf = 1507;
enum int Arabic_kasra = 1520;
enum int Arabic_kasratan = 1517;
enum int Arabic_keheh = 16778921;
enum int Arabic_khah = 1486;
enum int Arabic_lam = 1508;
enum int Arabic_madda_above = 16778835;
enum int Arabic_maddaonalef = 1474;
enum int Arabic_meem = 1509;
enum int Arabic_noon = 1510;
enum int Arabic_noon_ghunna = 16778938;
enum int Arabic_peh = 16778878;
enum int Arabic_percent = 16778858;
enum int Arabic_qaf = 1506;
enum int Arabic_question_mark = 1471;
enum int Arabic_ra = 1489;
enum int Arabic_rreh = 16778897;
enum int Arabic_sad = 1493;
enum int Arabic_seen = 1491;
enum int Arabic_semicolon = 1467;
enum int Arabic_shadda = 1521;
enum int Arabic_sheen = 1492;
enum int Arabic_sukun = 1522;
enum int Arabic_superscript_alef = 16778864;
enum int Arabic_switch = 65406;
enum int Arabic_tah = 1495;
enum int Arabic_tatweel = 1504;
enum int Arabic_tcheh = 16778886;
enum int Arabic_teh = 1482;
enum int Arabic_tehmarbuta = 1481;
enum int Arabic_thal = 1488;
enum int Arabic_theh = 1483;
enum int Arabic_tteh = 16778873;
enum int Arabic_veh = 16778916;
enum int Arabic_waw = 1512;
enum int Arabic_yeh = 1514;
enum int Arabic_yeh_baree = 16778962;
enum int Arabic_zah = 1496;
enum int Arabic_zain = 1490;
enum int Aring = 197;
enum int Armenian_AT = 16778552;
enum int Armenian_AYB = 16778545;
enum int Armenian_BEN = 16778546;
enum int Armenian_CHA = 16778569;
enum int Armenian_DA = 16778548;
enum int Armenian_DZA = 16778561;
enum int Armenian_E = 16778551;
enum int Armenian_FE = 16778582;
enum int Armenian_GHAT = 16778562;
enum int Armenian_GIM = 16778547;
enum int Armenian_HI = 16778565;
enum int Armenian_HO = 16778560;
enum int Armenian_INI = 16778555;
enum int Armenian_JE = 16778571;
enum int Armenian_KE = 16778580;
enum int Armenian_KEN = 16778559;
enum int Armenian_KHE = 16778557;
enum int Armenian_LYUN = 16778556;
enum int Armenian_MEN = 16778564;
enum int Armenian_NU = 16778566;
enum int Armenian_O = 16778581;
enum int Armenian_PE = 16778570;
enum int Armenian_PYUR = 16778579;
enum int Armenian_RA = 16778572;
enum int Armenian_RE = 16778576;
enum int Armenian_SE = 16778573;
enum int Armenian_SHA = 16778567;
enum int Armenian_TCHE = 16778563;
enum int Armenian_TO = 16778553;
enum int Armenian_TSA = 16778558;
enum int Armenian_TSO = 16778577;
enum int Armenian_TYUN = 16778575;
enum int Armenian_VEV = 16778574;
enum int Armenian_VO = 16778568;
enum int Armenian_VYUN = 16778578;
enum int Armenian_YECH = 16778549;
enum int Armenian_ZA = 16778550;
enum int Armenian_ZHE = 16778554;
enum int Armenian_accent = 16778587;
enum int Armenian_amanak = 16778588;
enum int Armenian_apostrophe = 16778586;
enum int Armenian_at = 16778600;
enum int Armenian_ayb = 16778593;
enum int Armenian_ben = 16778594;
enum int Armenian_but = 16778589;
enum int Armenian_cha = 16778617;
enum int Armenian_da = 16778596;
enum int Armenian_dza = 16778609;
enum int Armenian_e = 16778599;
enum int Armenian_exclam = 16778588;
enum int Armenian_fe = 16778630;
enum int Armenian_full_stop = 16778633;
enum int Armenian_ghat = 16778610;
enum int Armenian_gim = 16778595;
enum int Armenian_hi = 16778613;
enum int Armenian_ho = 16778608;
enum int Armenian_hyphen = 16778634;
enum int Armenian_ini = 16778603;
enum int Armenian_je = 16778619;
enum int Armenian_ke = 16778628;
enum int Armenian_ken = 16778607;
enum int Armenian_khe = 16778605;
enum int Armenian_ligature_ew = 16778631;
enum int Armenian_lyun = 16778604;
enum int Armenian_men = 16778612;
enum int Armenian_nu = 16778614;
enum int Armenian_o = 16778629;
enum int Armenian_paruyk = 16778590;
enum int Armenian_pe = 16778618;
enum int Armenian_pyur = 16778627;
enum int Armenian_question = 16778590;
enum int Armenian_ra = 16778620;
enum int Armenian_re = 16778624;
enum int Armenian_se = 16778621;
enum int Armenian_separation_mark = 16778589;
enum int Armenian_sha = 16778615;
enum int Armenian_shesht = 16778587;
enum int Armenian_tche = 16778611;
enum int Armenian_to = 16778601;
enum int Armenian_tsa = 16778606;
enum int Armenian_tso = 16778625;
enum int Armenian_tyun = 16778623;
enum int Armenian_verjaket = 16778633;
enum int Armenian_vev = 16778622;
enum int Armenian_vo = 16778616;
enum int Armenian_vyun = 16778626;
enum int Armenian_yech = 16778597;
enum int Armenian_yentamna = 16778634;
enum int Armenian_za = 16778598;
enum int Armenian_zhe = 16778602;
enum int Atilde = 195;
enum int AudibleBell_Enable = 65146;
enum int AudioCycleTrack = 269025179;
enum int AudioForward = 269025175;
enum int AudioLowerVolume = 269025041;
enum int AudioMedia = 269025074;
enum int AudioMute = 269025042;
enum int AudioNext = 269025047;
enum int AudioPause = 269025073;
enum int AudioPlay = 269025044;
enum int AudioPrev = 269025046;
enum int AudioRaiseVolume = 269025043;
enum int AudioRandomPlay = 269025177;
enum int AudioRecord = 269025052;
enum int AudioRepeat = 269025176;
enum int AudioRewind = 269025086;
enum int AudioStop = 269025045;
enum int Away = 269025165;
enum int B = 66;
enum int BUTTON_MIDDLE = 2;
enum int BUTTON_PRIMARY = 1;
enum int BUTTON_SECONDARY = 3;
enum int Babovedot = 16784898;
enum int Back = 269025062;
enum int BackForward = 269025087;
enum int BackSpace = 65288;

// <structname>ClutterBackend</structname> is an opaque structure whose
// members cannot be directly accessed.
struct Backend /* : GObject.Object */ /* Version 0.4 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // VERSION: 0.4
   // DEPRECATED (v1.4) method: get_double_click_distance - Use #ClutterSettings:double-click-distance instead
   // Retrieves the distance used to verify a double click event
   // RETURNS: a distance, in pixels.
   uint get_double_click_distance()() nothrow {
      return clutter_backend_get_double_click_distance(&this);
   }

   // VERSION: 0.4
   // DEPRECATED (v1.4) method: get_double_click_time - Use #ClutterSettings:double-click-time instead
   // Gets the maximum time between two button press events, as set
   // by clutter_backend_set_double_click_time().
   // RETURNS: a time in milliseconds
   uint get_double_click_time()() nothrow {
      return clutter_backend_get_double_click_time(&this);
   }

   // VERSION: 1.0
   // DEPRECATED (v1.4) method: get_font_name - Use #ClutterSettings:font-name instead
   // Retrieves the default font name as set by
   // clutter_backend_set_font_name().
   // 
   // owned by the #ClutterBackend and should never be modified or freed
   // RETURNS: the font name for the backend. The returned string is
   char* get_font_name()() nothrow {
      return clutter_backend_get_font_name(&this);
   }

   // VERSION: 0.8
   // Retrieves the font options for @backend.
   // 
   // The returned #cairo_font_options_t is owned by the backend and should
   // not be modified or freed
   // RETURNS: the font options of the #ClutterBackend.
   cairo.FontOptions* get_font_options()() nothrow {
      return clutter_backend_get_font_options(&this);
   }

   // VERSION: 0.4
   // Gets the resolution for font handling on the screen.
   // 
   // The resolution is a scale factor between points specified in a
   // #PangoFontDescription and cairo units. The default value is 96.0,
   // meaning that a 10 point font will be 13 units
   // high (10 * 96. / 72. = 13.3).
   // 
   // Clutter will set the resolution using the current backend when
   // initializing; the resolution is also stored in the
   // #ClutterSettings:font-dpi property.
   // 
   // has been set.
   // RETURNS: the current resolution, or -1 if no resolution
   double get_resolution()() nothrow {
      return clutter_backend_get_resolution(&this);
   }

   // VERSION: 0.4
   // DEPRECATED (v1.4) method: set_double_click_distance - Use #ClutterSettings:double-click-distance instead
   // Sets the maximum distance used to verify a double click event.
   // <distance>: a distance, in pixels
   void set_double_click_distance()(uint distance) nothrow {
      clutter_backend_set_double_click_distance(&this, distance);
   }

   // VERSION: 0.4
   // DEPRECATED (v1.4) method: set_double_click_time - Use #ClutterSettings:double-click-time instead
   // Sets the maximum time between two button press events, used to
   // verify whether it's a double click event or not.
   // <msec>: milliseconds between two button press events
   void set_double_click_time()(uint msec) nothrow {
      clutter_backend_set_double_click_time(&this, msec);
   }

   // VERSION: 1.0
   // DEPRECATED (v1.4) method: set_font_name - Use #ClutterSettings:font-name instead
   // Sets the default font to be used by Clutter. The @font_name string
   // must either be %NULL, which means that the font name from the
   // default #ClutterBackend will be used; or be something that can
   // be parsed by the pango_font_description_from_string() function.
   // <font_name>: the name of the font
   void set_font_name(AT0)(AT0 /*char*/ font_name) nothrow {
      clutter_backend_set_font_name(&this, toCString!(char*)(font_name));
   }

   // VERSION: 0.8
   // Sets the new font options for @backend. The #ClutterBackend will
   // copy the #cairo_font_options_t.
   // 
   // If @options is %NULL, the first following call to
   // clutter_backend_get_font_options() will return the default font
   // options for @backend.
   // 
   // This function is intended for actors creating a Pango layout
   // using the PangoCairo API.
   // <options>: Cairo font options for the backend, or %NULL
   void set_font_options(AT0)(AT0 /*cairo.FontOptions*/ options) nothrow {
      clutter_backend_set_font_options(&this, UpCast!(cairo.FontOptions*)(options));
   }

   // VERSION: 0.4
   // DEPRECATED method: set_resolution - Use #ClutterSettings:font-dpi instead
   // Sets the resolution for font handling on the screen. This is a
   // scale factor between points specified in a #PangoFontDescription
   // and cairo units. The default value is 96, meaning that a 10 point
   // font will be 13 units high. (10 * 96. / 72. = 13.3).
   // 
   // Applications should never need to call this function.
   // <dpi>: the resolution in "dots per inch" (Physical inches aren't actually involved; the terminology is conventional).
   void set_resolution()(double dpi) nothrow {
      clutter_backend_set_resolution(&this, dpi);
   }

   // VERSION: 1.0
   // The ::font-changed signal is emitted each time the font options
   // have been changed through #ClutterSettings.
   extern (C) alias static void function (Backend* this_, void* user_data=null) nothrow signal_font_changed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"font-changed", CB/*:signal_font_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_font_changed)||_ttmm!(CB, signal_font_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"font-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.0
   // The ::resolution-changed signal is emitted each time the font
   // resolutions has been changed through #ClutterSettings.
   extern (C) alias static void function (Backend* this_, void* user_data=null) nothrow signal_resolution_changed;
   ulong signal_connect(string name:"resolution-changed", CB/*:signal_resolution_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_resolution_changed)||_ttmm!(CB, signal_resolution_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"resolution-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.4
   // The ::settings-changed signal is emitted each time the #ClutterSettings
   // properties have been changed.
   extern (C) alias static void function (Backend* this_, void* user_data=null) nothrow signal_settings_changed;
   ulong signal_connect(string name:"settings-changed", CB/*:signal_settings_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_settings_changed)||_ttmm!(CB, signal_settings_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"settings-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct BackendClass {
}

enum int Battery = 269025171;
enum int Begin = 65368;

// #ClutterBehaviour-struct contains only private data and should
// be accessed with the functions below.
struct Behaviour /* : GObject.Object */ /* Version 0.2 */ {
   mixin Scriptable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   private BehaviourPrivate* priv;


   // VERSION: 0.2
   // DEPRECATED method: actors_foreach - 1.6
   // Calls @func for every actor driven by @behave.
   // <func>: a function called for each actor
   // <data>: optional data to be passed to the function, or %NULL
   void actors_foreach(AT0)(BehaviourForeachFunc func, AT0 /*void*/ data) nothrow {
      clutter_behaviour_actors_foreach(&this, func, UpCast!(void*)(data));
   }

   // VERSION: 0.2
   // DEPRECATED method: apply - 1.6
   // Applies @behave to @actor.  This function adds a reference on
   // the actor.
   // <actor>: a #ClutterActor
   void apply(AT0)(AT0 /*Actor*/ actor) nothrow {
      clutter_behaviour_apply(&this, UpCast!(Actor*)(actor));
   }

   // VERSION: 0.2
   // DEPRECATED method: get_actors - 1.6
   // Retrieves all the actors to which @behave applies. It is not recommended
   // for derived classes to use this in there alpha notify method but use 
   // #clutter_behaviour_actors_foreach as it avoids alot of needless allocations.
   // 
   // actors. You should free the returned list with g_slist_free() when
   // finished using it.
   // RETURNS: a list of
   GLib2.SList* /*new container*/ get_actors()() nothrow {
      return clutter_behaviour_get_actors(&this);
   }

   // VERSION: 0.2
   // DEPRECATED method: get_alpha - 1.6
   // Retrieves the #ClutterAlpha object bound to @behave.
   // 
   // object has been bound to this behaviour.
   // RETURNS: a #ClutterAlpha object, or %NULL if no alpha
   Alpha* get_alpha()() nothrow {
      return clutter_behaviour_get_alpha(&this);
   }

   // VERSION: 0.2
   // DEPRECATED method: get_n_actors - 1.6
   // Gets the number of actors this behaviour is applied too.
   // RETURNS: The number of applied actors
   int get_n_actors()() nothrow {
      return clutter_behaviour_get_n_actors(&this);
   }

   // VERSION: 0.2
   // DEPRECATED method: get_nth_actor - 1.6
   // Gets an actor the behaviour was applied to referenced by index num.
   // RETURNS: A Clutter actor or NULL if @index_ is invalid.
   // <index_>: the index of an actor this behaviour is applied too.
   Actor* get_nth_actor()(int index_) nothrow {
      return clutter_behaviour_get_nth_actor(&this, index_);
   }

   // VERSION: 0.4
   // DEPRECATED method: is_applied - 1.6
   // Check if @behave applied to  @actor.
   // RETURNS: TRUE if actor has behaviour. FALSE otherwise.
   // <actor>: a #ClutterActor
   int is_applied(AT0)(AT0 /*Actor*/ actor) nothrow {
      return clutter_behaviour_is_applied(&this, UpCast!(Actor*)(actor));
   }

   // VERSION: 0.2
   // DEPRECATED method: remove - 1.6
   // Removes @actor from the list of #ClutterActor<!-- -->s to which
   // @behave applies.  This function removes a reference on the actor.
   // <actor>: a #ClutterActor
   void remove(AT0)(AT0 /*Actor*/ actor) nothrow {
      clutter_behaviour_remove(&this, UpCast!(Actor*)(actor));
   }

   // VERSION: 0.4
   // DEPRECATED method: remove_all - 1.6
   // Removes every actor from the list that @behave holds.
   void remove_all()() nothrow {
      clutter_behaviour_remove_all(&this);
   }

   // VERSION: 0.2
   // DEPRECATED method: set_alpha - 1.6
   // Binds @alpha to a #ClutterBehaviour. The #ClutterAlpha object
   // is what makes a behaviour work: for each tick of the timeline
   // used by #ClutterAlpha a new value of the alpha parameter is
   // computed by the alpha function; the value should be used by
   // the #ClutterBehaviour to update one or more properties of the
   // actors to which the behaviour applies.
   // 
   // If @alpha is not %NULL, the #ClutterBehaviour will take ownership
   // of the #ClutterAlpha instance.
   // <alpha>: a #ClutterAlpha or %NULL to unset a previously set alpha
   void set_alpha(AT0)(AT0 /*Alpha*/ alpha) nothrow {
      clutter_behaviour_set_alpha(&this, UpCast!(Alpha*)(alpha));
   }

   // VERSION: 0.4
   // DEPRECATED glib:signal: applied - 1.6
   // The ::apply signal is emitted each time the behaviour is applied
   // to an actor.
   // <actor>: the actor the behaviour was applied to.
   extern (C) alias static void function (Behaviour* this_, Actor* actor, void* user_data=null) nothrow signal_applied;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"applied", CB/*:signal_applied*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_applied)||_ttmm!(CB, signal_applied)()) {
      return signal_connect_data!()(&this, cast(char*)"applied",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.4
   // DEPRECATED glib:signal: removed - 1.6
   // The ::removed signal is emitted each time a behaviour is not applied
   // to an actor anymore.
   // <actor>: the removed actor
   extern (C) alias static void function (Behaviour* this_, Actor* actor, void* user_data=null) nothrow signal_removed;
   ulong signal_connect(string name:"removed", CB/*:signal_removed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_removed)||_ttmm!(CB, signal_removed)()) {
      return signal_connect_data!()(&this, cast(char*)"removed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// Base class for behaviours.
struct BehaviourClass /* Version 0.2 */ {
   private GObject2.ObjectClass parent_class;
   extern (C) void function (Behaviour* behave, double alpha_value) nothrow alpha_notify;
   extern (C) void function (Behaviour* behave, Actor* actor) nothrow applied;
   extern (C) void function (Behaviour* behave, Actor* actor) nothrow removed;
   extern (C) void function () nothrow _clutter_behaviour1;
   extern (C) void function () nothrow _clutter_behaviour2;
   extern (C) void function () nothrow _clutter_behaviour3;
   extern (C) void function () nothrow _clutter_behaviour4;
   extern (C) void function () nothrow _clutter_behaviour5;
   extern (C) void function () nothrow _clutter_behaviour6;
}


// The #ClutterBehaviourDepth structure contains only private data
// and should be accessed using the provided API
// 
// 
// instead.
struct BehaviourDepth /* : Behaviour */ /* Version 0.2 */ {
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance behaviour;
   Behaviour parent_instance;
   private BehaviourDepthPrivate* priv;


   // VERSION: 0.4
   // DEPRECATED constructor: new - 1.6
   // Creates a new #ClutterBehaviourDepth which can be used to control
   // the ClutterActor:depth property of a set of #ClutterActor<!-- -->s.
   // 
   // If @alpha is not %NULL, the #ClutterBehaviour will take ownership
   // of the #ClutterAlpha instance. In the case when @alpha is %NULL,
   // it can be set later with clutter_behaviour_set_alpha().
   // RETURNS: the newly created behaviour
   // <alpha>: a #ClutterAlpha instance, or %NULL
   // <depth_start>: initial value of the depth
   // <depth_end>: final value of the depth
   static BehaviourDepth* /*new*/ new_(AT0)(AT0 /*Alpha*/ alpha, int depth_start, int depth_end) nothrow {
      return clutter_behaviour_depth_new(UpCast!(Alpha*)(alpha), depth_start, depth_end);
   }
   static auto opCall(AT0)(AT0 /*Alpha*/ alpha, int depth_start, int depth_end) {
      return clutter_behaviour_depth_new(UpCast!(Alpha*)(alpha), depth_start, depth_end);
   }

   // VERSION: 0.6
   // DEPRECATED method: get_bounds - 1.6
   // Gets the boundaries of the @behaviour
   // <depth_start>: return location for the initial depth value, or %NULL
   // <depth_end>: return location for the final depth value, or %NULL
   void get_bounds()(/*out*/ int* depth_start, /*out*/ int* depth_end) nothrow {
      clutter_behaviour_depth_get_bounds(&this, depth_start, depth_end);
   }

   // VERSION: 0.6
   // DEPRECATED method: set_bounds - 1.6
   // Sets the boundaries of the @behaviour.
   // <depth_start>: initial value of the depth
   // <depth_end>: final value of the depth
   void set_bounds()(int depth_start, int depth_end) nothrow {
      clutter_behaviour_depth_set_bounds(&this, depth_start, depth_end);
   }
}

// The #ClutterBehaviourDepthClass structure contains only private data
struct BehaviourDepthClass /* Version 0.2 */ {
   private BehaviourClass parent_class;
}

struct BehaviourDepthPrivate {
}


// The #ClutterBehaviourEllipse struct contains only private data
// and should be accessed using the provided API
struct BehaviourEllipse /* : Behaviour */ /* Version 0.4 */ {
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance behaviour;
   Behaviour parent_instance;
   private BehaviourEllipsePrivate* priv;


   // VERSION: 0.4
   // Creates a behaviour that drives actors along an elliptical path with
   // given center, width and height; the movement starts at @start
   // degrees (with 0 corresponding to 12 o'clock) and ends at @end
   // degrees. Angles greated than 360 degrees get clamped to the canonical
   // interval <0, 360); if @start is equal to @end, the behaviour will
   // rotate by exacly 360 degrees.
   // 
   // If @alpha is not %NULL, the #ClutterBehaviour will take ownership
   // of the #ClutterAlpha instance. In the case when @alpha is %NULL,
   // it can be set later with clutter_behaviour_set_alpha().
   // RETURNS: the newly created #ClutterBehaviourEllipse
   // <alpha>: a #ClutterAlpha instance, or %NULL
   // <x>: x coordinace of the center
   // <y>: y coordiance of the center
   // <width>: width of the ellipse
   // <height>: height of the ellipse
   // <direction>: #ClutterRotateDirection of rotation
   // <start>: angle in degrees at which movement starts, between 0 and 360
   // <end>: angle in degrees at which movement ends, between 0 and 360
   static BehaviourEllipse* /*new*/ new_(AT0)(AT0 /*Alpha*/ alpha, int x, int y, int width, int height, RotateDirection direction, double start, double end) nothrow {
      return clutter_behaviour_ellipse_new(UpCast!(Alpha*)(alpha), x, y, width, height, direction, start, end);
   }
   static auto opCall(AT0)(AT0 /*Alpha*/ alpha, int x, int y, int width, int height, RotateDirection direction, double start, double end) {
      return clutter_behaviour_ellipse_new(UpCast!(Alpha*)(alpha), x, y, width, height, direction, start, end);
   }

   // VERSION: 0.4
   // Gets the at which movements ends.
   // RETURNS: angle in degrees
   double get_angle_end()() nothrow {
      return clutter_behaviour_ellipse_get_angle_end(&this);
   }

   // VERSION: 0.6
   // Gets the angle at which movements starts.
   // RETURNS: angle in degrees
   double get_angle_start()() nothrow {
      return clutter_behaviour_ellipse_get_angle_start(&this);
   }

   // VERSION: 0.4
   // Gets the tilt of the ellipse around the center in the given axis.
   // RETURNS: angle in degrees.
   // <axis>: a #ClutterRotateAxis
   double get_angle_tilt()(RotateAxis axis) nothrow {
      return clutter_behaviour_ellipse_get_angle_tilt(&this, axis);
   }

   // VERSION: 0.4
   // Gets the center of the elliptical path path.
   // <x>: return location for the X coordinate of the center, or %NULL
   // <y>: return location for the Y coordinate of the center, or %NULL
   void get_center()(/*out*/ int* x, /*out*/ int* y) nothrow {
      clutter_behaviour_ellipse_get_center(&this, x, y);
   }

   // VERSION: 0.4
   // Retrieves the #ClutterRotateDirection used by the ellipse behaviour.
   // RETURNS: the rotation direction
   RotateDirection get_direction()() nothrow {
      return clutter_behaviour_ellipse_get_direction(&this);
   }

   // VERSION: 0.4
   // Gets the height of the elliptical path.
   // RETURNS: the height of the path
   int get_height()() nothrow {
      return clutter_behaviour_ellipse_get_height(&this);
   }

   // VERSION: 0.4
   // Gets the tilt of the ellipse around the center in Y axis.
   // <angle_tilt_x>: return location for tilt angle on the X axis, or %NULL.
   // <angle_tilt_y>: return location for tilt angle on the Y axis, or %NULL.
   // <angle_tilt_z>: return location for tilt angle on the Z axis, or %NULL.
   void get_tilt(AT0, AT1, AT2)(/*out*/ AT0 /*double*/ angle_tilt_x, /*out*/ AT1 /*double*/ angle_tilt_y, /*out*/ AT2 /*double*/ angle_tilt_z) nothrow {
      clutter_behaviour_ellipse_get_tilt(&this, UpCast!(double*)(angle_tilt_x), UpCast!(double*)(angle_tilt_y), UpCast!(double*)(angle_tilt_z));
   }

   // VERSION: 0.4
   // Gets the width of the elliptical path.
   // RETURNS: the width of the path
   int get_width()() nothrow {
      return clutter_behaviour_ellipse_get_width(&this);
   }

   // VERSION: 0.4
   // Sets the angle at which movement ends; angles >= 360 degress get clamped
   // to the canonical interval <0, 360).
   // <angle_end>: angle at which movement ends in degrees, between 0 and 360.
   void set_angle_end()(double angle_end) nothrow {
      clutter_behaviour_ellipse_set_angle_end(&this, angle_end);
   }

   // VERSION: 0.6
   // Sets the angle at which movement starts; angles >= 360 degress get clamped
   // to the canonical interval <0, 360).
   // <angle_start>: angle at which movement starts in degrees, between 0 and 360.
   void set_angle_start()(double angle_start) nothrow {
      clutter_behaviour_ellipse_set_angle_start(&this, angle_start);
   }

   // VERSION: 0.4
   // Sets the angle at which the ellipse should be tilted around it's center.
   // <axis>: a #ClutterRotateAxis
   // <angle_tilt>: tilt of the elipse around the center in the given axis in degrees.
   void set_angle_tilt()(RotateAxis axis, double angle_tilt) nothrow {
      clutter_behaviour_ellipse_set_angle_tilt(&this, axis, angle_tilt);
   }

   // VERSION: 0.4
   // Sets the center of the elliptical path to the point represented by knot.
   // <x>: x coordinace of centre
   // <y>: y coordinace of centre
   void set_center()(int x, int y) nothrow {
      clutter_behaviour_ellipse_set_center(&this, x, y);
   }

   // VERSION: 0.4
   // Sets the rotation direction used by the ellipse behaviour.
   // <direction>: the rotation direction
   void set_direction()(RotateDirection direction) nothrow {
      clutter_behaviour_ellipse_set_direction(&this, direction);
   }

   // VERSION: 0.4
   // Sets the height of the elliptical path.
   // <height>: height of the ellipse
   void set_height()(int height) nothrow {
      clutter_behaviour_ellipse_set_height(&this, height);
   }

   // VERSION: 0.4
   // Sets the angles at which the ellipse should be tilted around it's center.
   // <angle_tilt_x>: tilt of the elipse around the center in X axis in degrees.
   // <angle_tilt_y>: tilt of the elipse around the center in Y axis in degrees.
   // <angle_tilt_z>: tilt of the elipse around the center in Z axis in degrees.
   void set_tilt()(double angle_tilt_x, double angle_tilt_y, double angle_tilt_z) nothrow {
      clutter_behaviour_ellipse_set_tilt(&this, angle_tilt_x, angle_tilt_y, angle_tilt_z);
   }

   // VERSION: 0.4
   // Sets the width of the elliptical path.
   // <width>: width of the ellipse
   void set_width()(int width) nothrow {
      clutter_behaviour_ellipse_set_width(&this, width);
   }
}

// The #ClutterBehaviourEllipseClass struct contains only private data
struct BehaviourEllipseClass /* Version 0.4 */ {
   private BehaviourClass parent_class;
}

struct BehaviourEllipsePrivate {
}


// VERSION: 0.2
// DEPRECATED callback: BehaviourForeachFunc - 1.6
// This function is passed to clutter_behaviour_actors_foreach() and
// will be called for each actor driven by @behaviour.
// <behaviour>: the #ClutterBehaviour
// <actor>: an actor driven by @behaviour
// <data>: optional data passed to the function
extern (C) alias void function (Behaviour* behaviour, Actor* actor, void* data) nothrow BehaviourForeachFunc;


// The #ClutterBehaviourOpacity structure contains only private data and
// should be accessed using the provided API
// 
// 
// instead.
struct BehaviourOpacity /* : Behaviour */ /* Version 0.2 */ {
   mixin Scriptable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent behaviour;
   Behaviour parent;
   private BehaviourOpacityPrivate* priv;


   // VERSION: 0.2
   // DEPRECATED constructor: new - 1.6
   // Creates a new #ClutterBehaviourOpacity object, driven by @alpha
   // which controls the opacity property of every actor, making it
   // change in the interval between @opacity_start and @opacity_end.
   // 
   // If @alpha is not %NULL, the #ClutterBehaviour will take ownership
   // of the #ClutterAlpha instance. In the case when @alpha is %NULL,
   // it can be set later with clutter_behaviour_set_alpha().
   // RETURNS: the newly created #ClutterBehaviourOpacity
   // <alpha>: a #ClutterAlpha instance, or %NULL
   // <opacity_start>: minimum level of opacity
   // <opacity_end>: maximum level of opacity
   static BehaviourOpacity* /*new*/ new_(AT0)(AT0 /*Alpha*/ alpha, ubyte opacity_start, ubyte opacity_end) nothrow {
      return clutter_behaviour_opacity_new(UpCast!(Alpha*)(alpha), opacity_start, opacity_end);
   }
   static auto opCall(AT0)(AT0 /*Alpha*/ alpha, ubyte opacity_start, ubyte opacity_end) {
      return clutter_behaviour_opacity_new(UpCast!(Alpha*)(alpha), opacity_start, opacity_end);
   }

   // VERSION: 0.6
   // DEPRECATED method: get_bounds - 1.6
   // Gets the initial and final levels of the opacity applied by @behaviour
   // on each actor it controls.
   // <opacity_start>: return location for the minimum level of opacity, or %NULL
   // <opacity_end>: return location for the maximum level of opacity, or %NULL
   void get_bounds(AT0, AT1)(/*out*/ AT0 /*ubyte*/ opacity_start, /*out*/ AT1 /*ubyte*/ opacity_end) nothrow {
      clutter_behaviour_opacity_get_bounds(&this, UpCast!(ubyte*)(opacity_start), UpCast!(ubyte*)(opacity_end));
   }

   // VERSION: 0.6
   // DEPRECATED method: set_bounds - 1.6
   // Sets the initial and final levels of the opacity applied by @behaviour
   // on each actor it controls.
   // <opacity_start>: minimum level of opacity
   // <opacity_end>: maximum level of opacity
   void set_bounds()(ubyte opacity_start, ubyte opacity_end) nothrow {
      clutter_behaviour_opacity_set_bounds(&this, opacity_start, opacity_end);
   }
}

// The #ClutterBehaviourOpacityClass structure contains only private data
struct BehaviourOpacityClass /* Version 0.2 */ {
   private BehaviourClass parent_class;
}

struct BehaviourOpacityPrivate {
}


// The #ClutterBehaviourPath structure contains only private data
// and should be accessed using the provided API
// 
// 
// instead.
struct BehaviourPath /* : Behaviour */ /* Version 0.2 */ {
   mixin Scriptable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent behaviour;
   Behaviour parent;
   private BehaviourPathPrivate* priv;


   // VERSION: 0.2
   // DEPRECATED constructor: new - 1.6
   // Creates a new path behaviour. You can use this behaviour to drive
   // actors along the nodes of a path, described by @path.
   // 
   // This will claim the floating reference on the #ClutterPath so you
   // do not need to unref if it.
   // 
   // If @alpha is not %NULL, the #ClutterBehaviour will take ownership
   // of the #ClutterAlpha instance. In the case when @alpha is %NULL,
   // it can be set later with clutter_behaviour_set_alpha().
   // RETURNS: a #ClutterBehaviour
   // <alpha>: a #ClutterAlpha instance, or %NULL
   // <path>: a #ClutterPath or %NULL for an empty path
   static BehaviourPath* /*new*/ new_(AT0, AT1)(AT0 /*Alpha*/ alpha, AT1 /*Path*/ path) nothrow {
      return clutter_behaviour_path_new(UpCast!(Alpha*)(alpha), UpCast!(Path*)(path));
   }
   static auto opCall(AT0, AT1)(AT0 /*Alpha*/ alpha, AT1 /*Path*/ path) {
      return clutter_behaviour_path_new(UpCast!(Alpha*)(alpha), UpCast!(Path*)(path));
   }

   // VERSION: 1.0
   // DEPRECATED constructor: new_with_description - 1.6
   // Creates a new path behaviour using the path described by @desc. See
   // clutter_path_add_string() for a description of the format.
   // 
   // If @alpha is not %NULL, the #ClutterBehaviour will take ownership
   // of the #ClutterAlpha instance. In the case when @alpha is %NULL,
   // it can be set later with clutter_behaviour_set_alpha().
   // RETURNS: a #ClutterBehaviour
   // <alpha>: a #ClutterAlpha instance, or %NULL
   // <desc>: a string description of the path
   static BehaviourPath* /*new*/ new_with_description(AT0, AT1)(AT0 /*Alpha*/ alpha, AT1 /*char*/ desc) nothrow {
      return clutter_behaviour_path_new_with_description(UpCast!(Alpha*)(alpha), toCString!(char*)(desc));
   }
   static auto opCall(AT0, AT1)(AT0 /*Alpha*/ alpha, AT1 /*char*/ desc) {
      return clutter_behaviour_path_new_with_description(UpCast!(Alpha*)(alpha), toCString!(char*)(desc));
   }

   // VERSION: 1.0
   // DEPRECATED constructor: new_with_knots - 1.6
   // Creates a new path behaviour that will make the actors visit all of
   // the given knots in order with straight lines in between.
   // 
   // A path will be created where the first knot is used in a
   // %CLUTTER_PATH_MOVE_TO and the subsequent knots are used in
   // %CLUTTER_PATH_LINE_TO<!-- -->s.
   // 
   // If @alpha is not %NULL, the #ClutterBehaviour will take ownership
   // of the #ClutterAlpha instance. In the case when @alpha is %NULL,
   // it can be set later with clutter_behaviour_set_alpha().
   // RETURNS: a #ClutterBehaviour
   // <alpha>: a #ClutterAlpha instance, or %NULL
   // <knots>: an array of #ClutterKnot<!-- -->s
   // <n_knots>: number of entries in @knots
   static BehaviourPath* /*new*/ new_with_knots(AT0, AT1)(AT0 /*Alpha*/ alpha, AT1 /*Knot*/ knots, uint n_knots) nothrow {
      return clutter_behaviour_path_new_with_knots(UpCast!(Alpha*)(alpha), UpCast!(Knot*)(knots), n_knots);
   }
   static auto opCall(AT0, AT1)(AT0 /*Alpha*/ alpha, AT1 /*Knot*/ knots, uint n_knots) {
      return clutter_behaviour_path_new_with_knots(UpCast!(Alpha*)(alpha), UpCast!(Knot*)(knots), n_knots);
   }

   // VERSION: 1.0
   // DEPRECATED method: get_path - 1.6
   // Get the current path of the behaviour
   // RETURNS: the path
   Path* get_path()() nothrow {
      return clutter_behaviour_path_get_path(&this);
   }

   // VERSION: 1.0
   // DEPRECATED method: set_path - 1.6
   // Change the path that the actors will follow. This will take the
   // floating reference on the #ClutterPath so you do not need to unref
   // it.
   // <path>: the new path to follow
   void set_path(AT0)(AT0 /*Path*/ path) nothrow {
      clutter_behaviour_path_set_path(&this, UpCast!(Path*)(path));
   }

   // VERSION: 0.2
   // DEPRECATED glib:signal: knot-reached - 1.6
   // This signal is emitted each time a node defined inside the path
   // is reached.
   // <knot_num>: the index of the #ClutterKnot reached
   extern (C) alias static void function (BehaviourPath* this_, c_uint knot_num, void* user_data=null) nothrow signal_knot_reached;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"knot-reached", CB/*:signal_knot_reached*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_knot_reached)||_ttmm!(CB, signal_knot_reached)()) {
      return signal_connect_data!()(&this, cast(char*)"knot-reached",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// The #ClutterBehaviourPathClass struct contains only private data
struct BehaviourPathClass /* Version 0.2 */ {
   private BehaviourClass parent_class;
   extern (C) void function (BehaviourPath* pathb, uint knot_num) nothrow knot_reached;
   extern (C) void function () nothrow _clutter_path_1;
   extern (C) void function () nothrow _clutter_path_2;
   extern (C) void function () nothrow _clutter_path_3;
   extern (C) void function () nothrow _clutter_path_4;
}

struct BehaviourPathPrivate {
}

struct BehaviourPrivate {
}


// The #ClutterBehaviourRotate struct contains only private data and
// should be accessed using the provided API
struct BehaviourRotate /* : Behaviour */ /* Version 0.4 */ {
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance behaviour;
   Behaviour parent_instance;
   private BehaviourRotatePrivate* priv;


   // VERSION: 0.4
   // Creates a new #ClutterBehaviourRotate. This behaviour will rotate actors
   // bound to it on @axis, following @direction, between @angle_start and
   // @angle_end. Angles >= 360 degrees will be clamped to the canonical interval
   // <0, 360), if angle_start == angle_end, the behaviour will carry out a
   // single rotation of 360 degrees.
   // 
   // If @alpha is not %NULL, the #ClutterBehaviour will take ownership
   // of the #ClutterAlpha instance. In the case when @alpha is %NULL,
   // it can be set later with clutter_behaviour_set_alpha().
   // RETURNS: the newly created #ClutterBehaviourRotate.
   // <alpha>: a #ClutterAlpha instance, or %NULL
   // <axis>: the rotation axis
   // <direction>: the rotation direction
   // <angle_start>: the starting angle in degrees, between 0 and 360.
   // <angle_end>: the final angle in degrees, between 0 and 360.
   static BehaviourRotate* /*new*/ new_(AT0)(AT0 /*Alpha*/ alpha, RotateAxis axis, RotateDirection direction, double angle_start, double angle_end) nothrow {
      return clutter_behaviour_rotate_new(UpCast!(Alpha*)(alpha), axis, direction, angle_start, angle_end);
   }
   static auto opCall(AT0)(AT0 /*Alpha*/ alpha, RotateAxis axis, RotateDirection direction, double angle_start, double angle_end) {
      return clutter_behaviour_rotate_new(UpCast!(Alpha*)(alpha), axis, direction, angle_start, angle_end);
   }

   // VERSION: 0.4
   // Retrieves the #ClutterRotateAxis used by the rotate behaviour.
   // RETURNS: the rotation axis
   RotateAxis get_axis()() nothrow {
      return clutter_behaviour_rotate_get_axis(&this);
   }

   // VERSION: 0.4
   // Retrieves the rotation boundaries of the rotate behaviour.
   // <angle_start>: return value for the initial angle
   // <angle_end>: return value for the final angle
   void get_bounds(AT0, AT1)(/*out*/ AT0 /*double*/ angle_start, /*out*/ AT1 /*double*/ angle_end) nothrow {
      clutter_behaviour_rotate_get_bounds(&this, UpCast!(double*)(angle_start), UpCast!(double*)(angle_end));
   }

   // VERSION: 0.4
   // Retrieves the center of rotation set using
   // clutter_behaviour_rotate_set_center().
   // <x>: return location for the X center of rotation
   // <y>: return location for the Y center of rotation
   // <z>: return location for the Z center of rotation
   void get_center()(/*out*/ int* x, /*out*/ int* y, /*out*/ int* z) nothrow {
      clutter_behaviour_rotate_get_center(&this, x, y, z);
   }

   // VERSION: 0.4
   // Retrieves the #ClutterRotateDirection used by the rotate behaviour.
   // RETURNS: the rotation direction
   RotateDirection get_direction()() nothrow {
      return clutter_behaviour_rotate_get_direction(&this);
   }

   // VERSION: 0.4
   // Sets the axis used by the rotate behaviour.
   // <axis>: a #ClutterRotateAxis
   void set_axis()(RotateAxis axis) nothrow {
      clutter_behaviour_rotate_set_axis(&this, axis);
   }

   // VERSION: 0.4
   // Sets the initial and final angles of a rotation behaviour; angles >= 360
   // degrees get clamped to the canonical interval <0, 360).
   // <angle_start>: initial angle in degrees, between 0 and 360.
   // <angle_end>: final angle in degrees, between 0 and 360.
   void set_bounds()(double angle_start, double angle_end) nothrow {
      clutter_behaviour_rotate_set_bounds(&this, angle_start, angle_end);
   }

   // VERSION: 0.4
   // Sets the center of rotation. The coordinates are relative to the plane
   // normal to the rotation axis set with clutter_behaviour_rotate_set_axis().
   // <x>: X axis center of rotation
   // <y>: Y axis center of rotation
   // <z>: Z axis center of rotation
   void set_center()(int x, int y, int z) nothrow {
      clutter_behaviour_rotate_set_center(&this, x, y, z);
   }

   // VERSION: 0.4
   // Sets the rotation direction used by the rotate behaviour.
   // <direction>: the rotation direction
   void set_direction()(RotateDirection direction) nothrow {
      clutter_behaviour_rotate_set_direction(&this, direction);
   }
}

// The #ClutterBehaviourRotateClass struct contains only private data
struct BehaviourRotateClass /* Version 0.4 */ {
   private BehaviourClass parent_class;
}

struct BehaviourRotatePrivate {
}


// The #ClutterBehaviourScale struct contains only private data and
// should be accessed using the provided API
// 
// 
// and #ClutterActor:scale-y instead.
struct BehaviourScale /* : Behaviour */ /* Version 0.2 */ {
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance behaviour;
   Behaviour parent_instance;
   private BehaviourScalePrivate* priv;


   // VERSION: 0.2
   // DEPRECATED constructor: new - 1.6
   // Creates a new  #ClutterBehaviourScale instance.
   // 
   // If @alpha is not %NULL, the #ClutterBehaviour will take ownership
   // of the #ClutterAlpha instance. In the case when @alpha is %NULL,
   // it can be set later with clutter_behaviour_set_alpha().
   // RETURNS: the newly created #ClutterBehaviourScale
   // <alpha>: a #ClutterAlpha instance, or %NULL
   // <x_scale_start>: initial scale factor on the X axis
   // <y_scale_start>: initial scale factor on the Y axis
   // <x_scale_end>: final scale factor on the X axis
   // <y_scale_end>: final scale factor on the Y axis
   static BehaviourScale* /*new*/ new_(AT0)(AT0 /*Alpha*/ alpha, double x_scale_start, double y_scale_start, double x_scale_end, double y_scale_end) nothrow {
      return clutter_behaviour_scale_new(UpCast!(Alpha*)(alpha), x_scale_start, y_scale_start, x_scale_end, y_scale_end);
   }
   static auto opCall(AT0)(AT0 /*Alpha*/ alpha, double x_scale_start, double y_scale_start, double x_scale_end, double y_scale_end) {
      return clutter_behaviour_scale_new(UpCast!(Alpha*)(alpha), x_scale_start, y_scale_start, x_scale_end, y_scale_end);
   }

   // VERSION: 0.4
   // DEPRECATED method: get_bounds - 1.6
   // Retrieves the bounds used by scale behaviour.
   // <x_scale_start>: return location for the initial scale factor on the X axis, or %NULL
   // <y_scale_start>: return location for the initial scale factor on the Y axis, or %NULL
   // <x_scale_end>: return location for the final scale factor on the X axis, or %NULL
   // <y_scale_end>: return location for the final scale factor on the Y axis, or %NULL
   void get_bounds(AT0, AT1, AT2, AT3)(/*out*/ AT0 /*double*/ x_scale_start, /*out*/ AT1 /*double*/ y_scale_start, /*out*/ AT2 /*double*/ x_scale_end, /*out*/ AT3 /*double*/ y_scale_end) nothrow {
      clutter_behaviour_scale_get_bounds(&this, UpCast!(double*)(x_scale_start), UpCast!(double*)(y_scale_start), UpCast!(double*)(x_scale_end), UpCast!(double*)(y_scale_end));
   }

   // VERSION: 0.6
   // DEPRECATED method: set_bounds - 1.6
   // Sets the bounds used by scale behaviour.
   // <x_scale_start>: initial scale factor on the X axis
   // <y_scale_start>: initial scale factor on the Y axis
   // <x_scale_end>: final scale factor on the X axis
   // <y_scale_end>: final scale factor on the Y axis
   void set_bounds()(double x_scale_start, double y_scale_start, double x_scale_end, double y_scale_end) nothrow {
      clutter_behaviour_scale_set_bounds(&this, x_scale_start, y_scale_start, x_scale_end, y_scale_end);
   }
}

// The #ClutterBehaviourScaleClass struct contains only private data
struct BehaviourScaleClass /* Version 0.2 */ {
   private BehaviourClass parent_class;
}

struct BehaviourScalePrivate {
}

// The alignment policies available on each axis for #ClutterBinLayout
enum BinAlignment /* Version 1.2 */ {
   FIXED = 0,
   FILL = 1,
   START = 2,
   END = 3,
   CENTER = 4
}

// The #ClutterBinLayout structure contains only private data
// and should be accessed using the provided API
struct BinLayout /* : LayoutManager */ /* Version 1.2 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance layoutmanager;
   LayoutManager parent_instance;
   private BinLayoutPrivate* priv;


   // VERSION: 1.2
   // Creates a new #ClutterBinLayout layout manager
   // RETURNS: the newly created layout manager
   // <x_align>: the default alignment policy to be used on the horizontal axis
   // <y_align>: the default alignment policy to be used on the vertical axis
   static BinLayout* new_()(BinAlignment x_align, BinAlignment y_align) nothrow {
      return clutter_bin_layout_new(x_align, y_align);
   }
   static auto opCall()(BinAlignment x_align, BinAlignment y_align) {
      return clutter_bin_layout_new(x_align, y_align);
   }

   // VERSION: 1.2
   // Adds a #ClutterActor to the container using @self and
   // sets the alignment policies for it
   // 
   // This function is equivalent to clutter_container_add_actor()
   // and clutter_layout_manager_child_set_property() but it does not
   // require a pointer to the #ClutterContainer associated to the
   // #ClutterBinLayout
   // <child>: a #ClutterActor
   // <x_align>: horizontal alignment policy for @child
   // <y_align>: vertical alignment policy for @child
   void add(AT0)(AT0 /*Actor*/ child, BinAlignment x_align, BinAlignment y_align) nothrow {
      clutter_bin_layout_add(&this, UpCast!(Actor*)(child), x_align, y_align);
   }

   // VERSION: 1.2
   // Retrieves the horizontal and vertical alignment policies for
   // a child of @self
   // 
   // If @child is %NULL the default alignment policies will be returned
   // instead
   // <child>: a child of @container
   // <x_align>: return location for the horizontal alignment policy
   // <y_align>: return location for the vertical alignment policy
   void get_alignment(AT0, AT1, AT2)(AT0 /*Actor*/ child=null, /*out*/ AT1 /*BinAlignment*/ x_align=null, /*out*/ AT2 /*BinAlignment*/ y_align=null) nothrow {
      clutter_bin_layout_get_alignment(&this, UpCast!(Actor*)(child), UpCast!(BinAlignment*)(x_align), UpCast!(BinAlignment*)(y_align));
   }

   // VERSION: 1.2
   // Sets the horizontal and vertical alignment policies to be applied
   // to a @child of @self
   // 
   // If @child is %NULL then the @x_align and @y_align values will
   // be set as the default alignment policies
   // <child>: a child of @container
   // <x_align>: the horizontal alignment policy to be used for the @child inside @container
   // <y_align>: the vertical aligment policy to be used on the @child inside @container
   void set_alignment(AT0)(AT0 /*Actor*/ child, BinAlignment x_align, BinAlignment y_align) nothrow {
      clutter_bin_layout_set_alignment(&this, UpCast!(Actor*)(child), x_align, y_align);
   }
}


// The #ClutterBinLayoutClass structure contains only private
// data and should be accessed using the provided API
struct BinLayoutClass /* Version 1.2 */ {
   private LayoutManagerClass parent_class;
}

struct BinLayoutPrivate {
}


// <structname>ClutterBindConstraint</structname> is an opaque structure
// whose members cannot be directly accessed
struct BindConstraint /* : Constraint */ /* Version 1.4 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent constraint;
   Constraint method_parent;


   // VERSION: 1.4
   // Creates a new constraint, binding a #ClutterActor's position to
   // the given @coordinate of the position of @source
   // RETURNS: the newly created #ClutterBindConstraint
   // <source>: the #ClutterActor to use as the source of the binding, or %NULL
   // <coordinate>: the coordinate to bind
   // <offset>: the offset to apply to the binding, in pixels
   static BindConstraint* new_(AT0)(AT0 /*Actor*/ source, BindCoordinate coordinate, float offset) nothrow {
      return clutter_bind_constraint_new(UpCast!(Actor*)(source), coordinate, offset);
   }
   static auto opCall(AT0)(AT0 /*Actor*/ source, BindCoordinate coordinate, float offset) {
      return clutter_bind_constraint_new(UpCast!(Actor*)(source), coordinate, offset);
   }

   // VERSION: 1.4
   // Retrieves the bound coordinate of the constraint
   // RETURNS: the bound coordinate
   BindCoordinate get_coordinate()() nothrow {
      return clutter_bind_constraint_get_coordinate(&this);
   }

   // VERSION: 1.4
   // Retrieves the offset set using clutter_bind_constraint_set_offset()
   // RETURNS: the offset, in pixels
   float get_offset()() nothrow {
      return clutter_bind_constraint_get_offset(&this);
   }

   // VERSION: 1.4
   // Retrieves the #ClutterActor set using clutter_bind_constraint_set_source()
   // RETURNS: a pointer to the source actor
   Actor* get_source()() nothrow {
      return clutter_bind_constraint_get_source(&this);
   }

   // VERSION: 1.4
   // Sets the coordinate to bind in the constraint
   // <coordinate>: the coordinate to bind
   void set_coordinate()(BindCoordinate coordinate) nothrow {
      clutter_bind_constraint_set_coordinate(&this, coordinate);
   }

   // VERSION: 1.4
   // Sets the offset to be applied to the constraint
   // <offset>: the offset to apply, in pixels
   void set_offset()(float offset) nothrow {
      clutter_bind_constraint_set_offset(&this, offset);
   }

   // VERSION: 1.4
   // Sets the source #ClutterActor for the constraint
   // <source>: a #ClutterActor, or %NULL to unset the source
   void set_source(AT0)(AT0 /*Actor*/ source=null) nothrow {
      clutter_bind_constraint_set_source(&this, UpCast!(Actor*)(source));
   }
}

struct BindConstraintClass {
}

// Specifies which property should be used in a binding
enum BindCoordinate /* Version 1.4 */ {
   X = 0,
   Y = 1,
   WIDTH = 2,
   HEIGHT = 3,
   POSITION = 4,
   SIZE = 5
}

// VERSION: 1.0
// The prototype for the callback function registered with
// clutter_binding_pool_install_action() and invoked by
// clutter_binding_pool_activate().
// 
// binding has been handled, and return %FALSE otherwise
// RETURNS: the function should return %TRUE if the key
// <gobject>: a #GObject
// <action_name>: the name of the action
// <key_val>: the key symbol
// <modifiers>: bitmask of the modifier flags
extern (C) alias int function (GObject2.Object* gobject, char* action_name, uint key_val, ModifierType modifiers) nothrow BindingActionFunc;


// Container of key bindings. The #ClutterBindingPool struct is
// private.
struct BindingPool /* : GObject.Object */ /* Version 1.0 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // VERSION: 1.0
   // Creates a new #ClutterBindingPool that can be used to store
   // key bindings for an actor. The @name must be a unique identifier
   // for the binding pool, so that clutter_binding_pool_find() will
   // be able to return the correct binding pool.
   // 
   // name. Use g_object_unref() when done.
   // RETURNS: the newly created binding pool with the given
   // <name>: the name of the binding pool
   static BindingPool* /*new*/ new_(AT0)(AT0 /*char*/ name) nothrow {
      return clutter_binding_pool_new(toCString!(char*)(name));
   }
   static auto opCall(AT0)(AT0 /*char*/ name) {
      return clutter_binding_pool_new(toCString!(char*)(name));
   }

   // VERSION: 1.0
   // Finds the #ClutterBindingPool with @name.
   // RETURNS: a pointer to the #ClutterBindingPool, or %NULL
   // <name>: the name of the binding pool to find
   static BindingPool* find(AT0)(AT0 /*char*/ name) nothrow {
      return clutter_binding_pool_find(toCString!(char*)(name));
   }

   // VERSION: 1.0
   // Retrieves the #ClutterBindingPool for the given #GObject class
   // and, eventually, creates it. This function is a wrapper around
   // clutter_binding_pool_new() and uses the class type name as the
   // unique name for the binding pool.
   // 
   // Calling this function multiple times will return the same
   // #ClutterBindingPool.
   // 
   // A binding pool for a class can also be retrieved using
   // clutter_binding_pool_find() with the class type name:
   // 
   // |[
   // pool = clutter_binding_pool_find (G_OBJECT_TYPE_NAME (instance));
   // ]|
   // 
   // The returned #ClutterBindingPool is owned by Clutter and should not
   // be freed directly
   // RETURNS: the binding pool for the given class.
   // <klass>: a #GObjectClass pointer
   static BindingPool* get_for_class(AT0)(AT0 /*void*/ klass) nothrow {
      return clutter_binding_pool_get_for_class(UpCast!(void*)(klass));
   }

   // VERSION: 1.0
   // Activates the callback associated to the action that is
   // bound to the @key_val and @modifiers pair.
   // 
   // The callback has the following signature:
   // 
   // |[
   // void (* callback) (GObject             *gobject,
   // const gchar         *action_name,
   // guint                key_val,
   // ClutterModifierType  modifiers,
   // gpointer             user_data);
   // ]|
   // 
   // Where the #GObject instance is @gobject and the user data
   // is the one passed when installing the action with
   // clutter_binding_pool_install_action().
   // 
   // If the action bound to the @key_val, @modifiers pair has been
   // blocked using clutter_binding_pool_block_action(), the callback
   // will not be invoked, and this function will return %FALSE.
   // RETURNS: %TRUE if an action was found and was activated
   // <key_val>: the key symbol
   // <modifiers>: bitmask for the modifiers
   // <gobject>: a #GObject
   int activate(AT0)(uint key_val, ModifierType modifiers, AT0 /*GObject2.Object*/ gobject) nothrow {
      return clutter_binding_pool_activate(&this, key_val, modifiers, UpCast!(GObject2.Object*)(gobject));
   }

   // VERSION: 1.0
   // Blocks all the actions with name @action_name inside @pool.
   // <action_name>: an action name
   void block_action(AT0)(AT0 /*char*/ action_name) nothrow {
      clutter_binding_pool_block_action(&this, toCString!(char*)(action_name));
   }

   // VERSION: 1.0
   // Retrieves the name of the action matching the given key symbol
   // and modifiers bitmask.
   // 
   // returned string is owned by the binding pool and should never
   // be modified or freed
   // RETURNS: the name of the action, if found, or %NULL. The
   // <key_val>: a key symbol
   // <modifiers>: a bitmask for the modifiers
   char* find_action()(uint key_val, ModifierType modifiers) nothrow {
      return clutter_binding_pool_find_action(&this, key_val, modifiers);
   }

   // VERSION: 1.0
   // Installs a new action inside a #ClutterBindingPool. The action
   // is bound to @key_val and @modifiers.
   // 
   // The same action name can be used for multiple @key_val, @modifiers
   // pairs.
   // 
   // When an action has been activated using clutter_binding_pool_activate()
   // the passed @callback will be invoked (with @data).
   // 
   // Actions can be blocked with clutter_binding_pool_block_action()
   // and then unblocked using clutter_binding_pool_unblock_action().
   // <action_name>: the name of the action
   // <key_val>: key symbol
   // <modifiers>: bitmask of modifiers
   // <callback>: function to be called when the action is activated
   // <data>: data to be passed to @callback
   // <notify>: function to be called when the action is removed from the pool
   void install_action(AT0, AT1)(AT0 /*char*/ action_name, uint key_val, ModifierType modifiers, GObject2.Callback callback, AT1 /*void*/ data, GLib2.DestroyNotify notify) nothrow {
      clutter_binding_pool_install_action(&this, toCString!(char*)(action_name), key_val, modifiers, callback, UpCast!(void*)(data), notify);
   }

   // VERSION: 1.0
   // A #GClosure variant of clutter_binding_pool_install_action().
   // 
   // Installs a new action inside a #ClutterBindingPool. The action
   // is bound to @key_val and @modifiers.
   // 
   // The same action name can be used for multiple @key_val, @modifiers
   // pairs.
   // 
   // When an action has been activated using clutter_binding_pool_activate()
   // the passed @closure will be invoked.
   // 
   // Actions can be blocked with clutter_binding_pool_block_action()
   // and then unblocked using clutter_binding_pool_unblock_action().
   // <action_name>: the name of the action
   // <key_val>: key symbol
   // <modifiers>: bitmask of modifiers
   // <closure>: a #GClosure
   void install_closure(AT0, AT1)(AT0 /*char*/ action_name, uint key_val, ModifierType modifiers, AT1 /*GObject2.Closure*/ closure) nothrow {
      clutter_binding_pool_install_closure(&this, toCString!(char*)(action_name), key_val, modifiers, UpCast!(GObject2.Closure*)(closure));
   }

   // VERSION: 1.0
   // Allows overriding the action for @key_val and @modifiers inside a
   // #ClutterBindingPool. See clutter_binding_pool_install_action().
   // 
   // When an action has been activated using clutter_binding_pool_activate()
   // the passed @callback will be invoked (with @data).
   // 
   // Actions can be blocked with clutter_binding_pool_block_action()
   // and then unblocked using clutter_binding_pool_unblock_action().
   // <key_val>: key symbol
   // <modifiers>: bitmask of modifiers
   // <callback>: function to be called when the action is activated
   // <data>: data to be passed to @callback
   // <notify>: function to be called when the action is removed from the pool
   void override_action(AT0)(uint key_val, ModifierType modifiers, GObject2.Callback callback, AT0 /*void*/ data, GLib2.DestroyNotify notify) nothrow {
      clutter_binding_pool_override_action(&this, key_val, modifiers, callback, UpCast!(void*)(data), notify);
   }

   // VERSION: 1.0
   // A #GClosure variant of clutter_binding_pool_override_action().
   // 
   // Allows overriding the action for @key_val and @modifiers inside a
   // #ClutterBindingPool. See clutter_binding_pool_install_closure().
   // 
   // When an action has been activated using clutter_binding_pool_activate()
   // the passed @callback will be invoked (with @data).
   // 
   // Actions can be blocked with clutter_binding_pool_block_action()
   // and then unblocked using clutter_binding_pool_unblock_action().
   // <key_val>: key symbol
   // <modifiers>: bitmask of modifiers
   // <closure>: a #GClosure
   void override_closure(AT0)(uint key_val, ModifierType modifiers, AT0 /*GObject2.Closure*/ closure) nothrow {
      clutter_binding_pool_override_closure(&this, key_val, modifiers, UpCast!(GObject2.Closure*)(closure));
   }

   // VERSION: 1.0
   // Removes the action matching the given @key_val, @modifiers pair,
   // if any exists.
   // <key_val>: a key symbol
   // <modifiers>: a bitmask for the modifiers
   void remove_action()(uint key_val, ModifierType modifiers) nothrow {
      clutter_binding_pool_remove_action(&this, key_val, modifiers);
   }

   // VERSION: 1.0
   // Unblockes all the actions with name @action_name inside @pool.
   // 
   // Unblocking an action does not cause the callback bound to it to
   // be invoked in case clutter_binding_pool_activate() was called on
   // an action previously blocked with clutter_binding_pool_block_action().
   // <action_name>: an action name
   void unblock_action(AT0)(AT0 /*char*/ action_name) nothrow {
      clutter_binding_pool_unblock_action(&this, toCString!(char*)(action_name));
   }
}

struct BindingPoolClass {
}

enum int Blue = 269025190;
enum int Bluetooth = 269025172;

// <structname>ClutterBlurEffect</structname> is an opaque structure
// whose members cannot be accessed directly
struct BlurEffect /* : OffscreenEffect */ /* Version 1.4 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent offscreeneffect;
   OffscreenEffect method_parent;


   // VERSION: 1.4
   // Creates a new #ClutterBlurEffect to be used with
   // clutter_actor_add_effect()
   // RETURNS: the newly created #ClutterBlurEffect or %NULL
   static BlurEffect* new_()() nothrow {
      return clutter_blur_effect_new();
   }
   static auto opCall()() {
      return clutter_blur_effect_new();
   }
}

struct BlurEffectClass {
}

enum int Book = 269025106;
enum int BounceKeys_Enable = 65140;

// The #ClutterBox structure contains only private data and should
// be accessed using the provided API
struct Box /* : Actor */ /* Version 1.2 */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Animatable.__interface__;
   mixin Container.__interface__;
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance actor;
   Actor parent_instance;
   private BoxPrivate* priv;


   // VERSION: 1.2
   // DEPRECATED (v1.10) constructor: new - Use clutter_actor_new() instead.
   // Creates a new #ClutterBox. The children of the box will be layed
   // out by the passed @manager
   // RETURNS: the newly created #ClutterBox actor
   // <manager>: a #ClutterLayoutManager
   static Box* new_(AT0)(AT0 /*LayoutManager*/ manager) nothrow {
      return clutter_box_new(UpCast!(LayoutManager*)(manager));
   }
   static auto opCall(AT0)(AT0 /*LayoutManager*/ manager) {
      return clutter_box_new(UpCast!(LayoutManager*)(manager));
   }

   // VERSION: 1.2
   // DEPRECATED (v1.10) method: get_color - Use clutter_actor_get_background_color() instead.
   // Retrieves the background color of @box
   // 
   // If the #ClutterBox:color-set property is set to %FALSE the
   // returned #ClutterColor is undefined
   // <color>: return location for a #ClutterColor
   void get_color(AT0)(/*out*/ AT0 /*Color*/ color) nothrow {
      clutter_box_get_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.2
   // DEPRECATED (v1.10) method: get_layout_manager - Use clutter_actor_get_layout_manager() instead.
   // Retrieves the #ClutterLayoutManager instance used by @box
   // 
   // #ClutterLayoutManager is owned by the #ClutterBox and it should not
   // be unreferenced
   // RETURNS: a #ClutterLayoutManager. The returned
   LayoutManager* get_layout_manager()() nothrow {
      return clutter_box_get_layout_manager(&this);
   }

   // Unintrospectable method: pack() / clutter_box_pack()
   // VERSION: 1.2
   // DEPRECATED (v1.10) method: pack - Use clutter_actor_add_child() instead.
   // Adds @actor to @box and sets layout properties at the same time,
   // if the #ClutterLayoutManager used by @box has them
   // 
   // This function is a wrapper around clutter_container_add_actor()
   // and clutter_layout_manager_child_set()
   // 
   // Language bindings should use the vector-based clutter_box_packv()
   // variant instead
   // <actor>: a #ClutterActor
   // <first_property>: the name of the first property to set, or %NULL
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_box_pack pack; // Variadic
   +/

   // Unintrospectable method: pack_after() / clutter_box_pack_after()
   // VERSION: 1.2
   // DEPRECATED (v1.10) method: pack_after - Use clutter_actor_insert_child_above() instead.
   // Adds @actor to @box, placing it after @sibling, and sets layout
   // properties at the same time, if the #ClutterLayoutManager used by
   // @box supports them
   // 
   // If @sibling is %NULL then @actor is placed at the end of the
   // list of children, to be allocated and painted after every other child
   // 
   // This function is a wrapper around clutter_container_add_actor(),
   // clutter_container_raise_child() and clutter_layout_manager_child_set()
   // <actor>: a #ClutterActor
   // <sibling>: a #ClutterActor or %NULL
   // <first_property>: the name of the first property to set, or %NULL
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_box_pack_after pack_after; // Variadic
   +/

   // Unintrospectable method: pack_at() / clutter_box_pack_at()
   // VERSION: 1.2
   // DEPRECATED (v1.10) method: pack_at - Use clutter_actor_insert_child_at_index() instead.
   // Adds @actor to @box, placing it at @position, and sets layout
   // properties at the same time, if the #ClutterLayoutManager used by
   // @box supports them
   // 
   // If @position is a negative number, or is larger than the number of
   // children of @box, the new child is added at the end of the list of
   // children
   // <actor>: a #ClutterActor
   // <position>: the position to insert the @actor at
   // <first_property>: the name of the first property to set, or %NULL
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_box_pack_at pack_at; // Variadic
   +/

   // Unintrospectable method: pack_before() / clutter_box_pack_before()
   // VERSION: 1.2
   // DEPRECATED (v1.10) method: pack_before - Use clutter_actor_insert_child_below() instead.
   // Adds @actor to @box, placing it before @sibling, and sets layout
   // properties at the same time, if the #ClutterLayoutManager used by
   // @box supports them
   // 
   // If @sibling is %NULL then @actor is placed at the beginning of the
   // list of children, to be allocated and painted below every other child
   // 
   // This function is a wrapper around clutter_container_add_actor(),
   // clutter_container_lower_child() and clutter_layout_manager_child_set()
   // <actor>: a #ClutterActor
   // <sibling>: a #ClutterActor or %NULL
   // <first_property>: the name of the first property to set, or %NULL
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_box_pack_before pack_before; // Variadic
   +/

   // VERSION: 1.2
   // DEPRECATED (v1.10) method: packv - Use clutter_actor_add_child() instead.
   // Vector-based variant of clutter_box_pack(), intended for language
   // bindings to use
   // <actor>: a #ClutterActor
   // <n_properties>: the number of properties to set
   // <properties>: a vector containing the property names to set
   // <values>: a vector containing the property values to set
   void packv(AT0, AT1, AT2)(AT0 /*Actor*/ actor, uint n_properties, AT1 /*char*/ properties, AT2 /*GObject2.Value*/ values) nothrow {
      clutter_box_packv(&this, UpCast!(Actor*)(actor), n_properties, toCString!(char*)(properties), UpCast!(GObject2.Value*)(values));
   }

   // VERSION: 1.2
   // DEPRECATED (v1.10) method: set_color - Use clutter_actor_set_background_color() instead.
   // Sets (or unsets) the background color for @box
   // <color>: the background color, or %NULL to unset
   void set_color(AT0)(AT0 /*Color*/ color=null) nothrow {
      clutter_box_set_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.2
   // DEPRECATED (v1.10) method: set_layout_manager - Use clutter_actor_set_layout_manager() instead.
   // Sets the #ClutterLayoutManager for @box
   // 
   // A #ClutterLayoutManager is a delegate object that controls the
   // layout of the children of @box
   // <manager>: a #ClutterLayoutManager
   void set_layout_manager(AT0)(AT0 /*LayoutManager*/ manager) nothrow {
      clutter_box_set_layout_manager(&this, UpCast!(LayoutManager*)(manager));
   }
}

// The alignment policies available on each axis of the #ClutterBoxLayout
enum BoxAlignment /* Version 1.2 */ {
   START = 0,
   END = 1,
   CENTER = 2
}
// The #ClutterBoxClass structure contains only private data
struct BoxClass /* Version 1.2 */ {
   private ActorClass parent_class;
   extern (C) void function () nothrow clutter_padding_1;
   extern (C) void function () nothrow clutter_padding_2;
   extern (C) void function () nothrow clutter_padding_3;
   extern (C) void function () nothrow clutter_padding_4;
   extern (C) void function () nothrow clutter_padding_5;
   extern (C) void function () nothrow clutter_padding_6;
}


// The #ClutterBoxLayout structure contains only private data
// and should be accessed using the provided API
struct BoxLayout /* : LayoutManager */ /* Version 1.2 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance layoutmanager;
   LayoutManager parent_instance;
   private BoxLayoutPrivate* priv;


   // VERSION: 1.2
   // Creates a new #ClutterBoxLayout layout manager
   // RETURNS: the newly created #ClutterBoxLayout
   static BoxLayout* new_()() nothrow {
      return clutter_box_layout_new();
   }
   static auto opCall()() {
      return clutter_box_layout_new();
   }

   // VERSION: 1.2
   // Retrieves the horizontal and vertical alignment policies for @actor
   // as set using clutter_box_layout_pack() or clutter_box_layout_set_alignment()
   // <actor>: a #ClutterActor child of @layout
   // <x_align>: return location for the horizontal alignment policy
   // <y_align>: return location for the vertical alignment policy
   void get_alignment(AT0, AT1, AT2)(AT0 /*Actor*/ actor, /*out*/ AT1 /*BoxAlignment*/ x_align, /*out*/ AT2 /*BoxAlignment*/ y_align) nothrow {
      clutter_box_layout_get_alignment(&this, UpCast!(Actor*)(actor), UpCast!(BoxAlignment*)(x_align), UpCast!(BoxAlignment*)(y_align));
   }

   // VERSION: 1.2
   // Retrieves the duration set using clutter_box_layout_set_easing_duration()
   // RETURNS: the duration of the animations, in milliseconds
   uint get_easing_duration()() nothrow {
      return clutter_box_layout_get_easing_duration(&this);
   }

   // VERSION: 1.2
   // Retrieves the easing mode set using clutter_box_layout_set_easing_mode()
   // RETURNS: an easing mode
   c_ulong get_easing_mode()() nothrow {
      return clutter_box_layout_get_easing_mode(&this);
   }

   // VERSION: 1.2
   // Retrieves whether @actor should expand inside @layout
   // RETURNS: %TRUE if the #ClutterActor should expand, %FALSE otherwise
   // <actor>: a #ClutterActor child of @layout
   int get_expand(AT0)(AT0 /*Actor*/ actor) nothrow {
      return clutter_box_layout_get_expand(&this, UpCast!(Actor*)(actor));
   }

   // VERSION: 1.2
   // Retrieves the horizontal and vertical fill policies for @actor
   // as set using clutter_box_layout_pack() or clutter_box_layout_set_fill()
   // <actor>: a #ClutterActor child of @layout
   // <x_fill>: return location for the horizontal fill policy
   // <y_fill>: return location for the vertical fill policy
   void get_fill(AT0)(AT0 /*Actor*/ actor, /*out*/ int* x_fill, /*out*/ int* y_fill) nothrow {
      clutter_box_layout_get_fill(&this, UpCast!(Actor*)(actor), x_fill, y_fill);
   }

   // VERSION: 1.4
   // Retrieves if the children sizes are allocated homogeneously.
   // 
   // homogeneously, and %FALSE otherwise
   // RETURNS: %TRUE if the #ClutterBoxLayout is arranging its children
   int get_homogeneous()() nothrow {
      return clutter_box_layout_get_homogeneous(&this);
   }

   // VERSION: 1.2
   // Retrieves the value set using clutter_box_layout_set_pack_start()
   // 
   // at the beginning of the layout, and %FALSE otherwise
   // RETURNS: %TRUE if the #ClutterBoxLayout should pack children
   int get_pack_start()() nothrow {
      return clutter_box_layout_get_pack_start(&this);
   }

   // VERSION: 1.2
   // Retrieves the spacing set using clutter_box_layout_set_spacing()
   // RETURNS: the spacing between children of the #ClutterBoxLayout
   uint get_spacing()() nothrow {
      return clutter_box_layout_get_spacing(&this);
   }

   // VERSION: 1.2
   // Retrieves whether @layout should animate changes in the layout properties
   // 
   // Since clutter_box_layout_set_use_animations()
   // RETURNS: %TRUE if the animations should be used, %FALSE otherwise
   int get_use_animations()() nothrow {
      return clutter_box_layout_get_use_animations(&this);
   }

   // VERSION: 1.2
   // Retrieves the orientation of the @layout as set using the
   // clutter_box_layout_set_vertical() function
   // 
   // vertically, and %FALSE otherwise
   // RETURNS: %TRUE if the #ClutterBoxLayout is arranging its children
   int get_vertical()() nothrow {
      return clutter_box_layout_get_vertical(&this);
   }

   // VERSION: 1.2
   // Packs @actor inside the #ClutterContainer associated to @layout
   // and sets the layout properties
   // <actor>: a #ClutterActor
   // <expand>: whether the @actor should expand
   // <x_fill>: whether the @actor should fill horizontally
   // <y_fill>: whether the @actor should fill vertically
   // <x_align>: the horizontal alignment policy for @actor
   // <y_align>: the vertical alignment policy for @actor
   void pack(AT0)(AT0 /*Actor*/ actor, int expand, int x_fill, int y_fill, BoxAlignment x_align, BoxAlignment y_align) nothrow {
      clutter_box_layout_pack(&this, UpCast!(Actor*)(actor), expand, x_fill, y_fill, x_align, y_align);
   }

   // VERSION: 1.2
   // Sets the horizontal and vertical alignment policies for @actor
   // inside @layout
   // <actor>: a #ClutterActor child of @layout
   // <x_align>: Horizontal alignment policy for @actor
   // <y_align>: Vertical alignment policy for @actor
   void set_alignment(AT0)(AT0 /*Actor*/ actor, BoxAlignment x_align, BoxAlignment y_align) nothrow {
      clutter_box_layout_set_alignment(&this, UpCast!(Actor*)(actor), x_align, y_align);
   }

   // VERSION: 1.2
   // Sets the duration of the animations used by @layout when animating changes
   // in the layout properties
   // 
   // Use clutter_box_layout_set_use_animations() to enable and disable the
   // animations
   // <msecs>: the duration of the animations, in milliseconds
   void set_easing_duration()(uint msecs) nothrow {
      clutter_box_layout_set_easing_duration(&this, msecs);
   }

   // VERSION: 1.2
   // Sets the easing mode to be used by @layout when animating changes in layout
   // properties
   // 
   // Use clutter_box_layout_set_use_animations() to enable and disable the
   // animations
   // <mode>: an easing mode, either from #ClutterAnimationMode or a logical id from clutter_alpha_register_func()
   void set_easing_mode()(c_ulong mode) nothrow {
      clutter_box_layout_set_easing_mode(&this, mode);
   }

   // VERSION: 1.2
   // Sets whether @actor should expand inside @layout
   // <actor>: a #ClutterActor child of @layout
   // <expand>: whether @actor should expand
   void set_expand(AT0)(AT0 /*Actor*/ actor, int expand) nothrow {
      clutter_box_layout_set_expand(&this, UpCast!(Actor*)(actor), expand);
   }

   // VERSION: 1.2
   // Sets the horizontal and vertical fill policies for @actor
   // inside @layout
   // <actor>: a #ClutterActor child of @layout
   // <x_fill>: whether @actor should fill horizontally the allocated space
   // <y_fill>: whether @actor should fill vertically the allocated space
   void set_fill(AT0)(AT0 /*Actor*/ actor, int x_fill, int y_fill) nothrow {
      clutter_box_layout_set_fill(&this, UpCast!(Actor*)(actor), x_fill, y_fill);
   }

   // VERSION: 1.4
   // Sets whether the size of @layout children should be
   // homogeneous
   // <homogeneous>: %TRUE if the layout should be homogeneous
   void set_homogeneous()(int homogeneous) nothrow {
      clutter_box_layout_set_homogeneous(&this, homogeneous);
   }

   // VERSION: 1.2
   // Sets whether children of @layout should be layed out by appending
   // them or by prepending them
   // <pack_start>: %TRUE if the @layout should pack children at the beginning of the layout
   void set_pack_start()(int pack_start) nothrow {
      clutter_box_layout_set_pack_start(&this, pack_start);
   }

   // VERSION: 1.2
   // Sets the spacing between children of @layout
   // <spacing>: the spacing between children of the layout, in pixels
   void set_spacing()(uint spacing) nothrow {
      clutter_box_layout_set_spacing(&this, spacing);
   }

   // VERSION: 1.2
   // Sets whether @layout should animate changes in the layout properties
   // 
   // The duration of the animations is controlled by
   // clutter_box_layout_set_easing_duration(); the easing mode to be used
   // by the animations is controlled by clutter_box_layout_set_easing_mode()
   // <animate>: %TRUE if the @layout should use animations
   void set_use_animations()(int animate) nothrow {
      clutter_box_layout_set_use_animations(&this, animate);
   }

   // VERSION: 1.2
   // Sets whether @layout should arrange its children vertically alongside
   // the Y axis, instead of horizontally alongside the X axis
   // <vertical>: %TRUE if the layout should be vertical
   void set_vertical()(int vertical) nothrow {
      clutter_box_layout_set_vertical(&this, vertical);
   }
}


// The #ClutterBoxLayoutClass structure contains only private
// data and should be accessed using the provided API
struct BoxLayoutClass /* Version 1.2 */ {
   private LayoutManagerClass parent_class;
}

struct BoxLayoutPrivate {
}

struct BoxPrivate {
}

enum int Break = 65387;
enum int BrightnessAdjust = 269025083;

// Button event.
// 
// The event coordinates are relative to the stage that received the
// event, and can be transformed into actor-relative coordinates by
// using clutter_actor_transform_stage_point().
struct ButtonEvent /* Version 0.2 */ {
   EventType type;
   uint time;
   EventFlags flags;
   Stage* stage;
   Actor* source;
   float x, y;
   ModifierType modifier_state;
   uint button;
   uint click_count;
   double* axes;
   InputDevice* device;
}

enum int Byelorussian_SHORTU = 1726;
enum int Byelorussian_shortu = 1710;
enum int C = 67;
enum int CD = 269025107;
enum int CH = 65186;
enum COGL = "deprecated";
enum int CURRENT_TIME = 0;
enum int C_H = 65189;
enum int C_h = 65188;
enum int Cabovedot = 709;
enum int Cacute = 454;
// The #ClutterCairoTexture struct contains only private data.
struct CairoTexture /* : Texture */ /* Version 1.0 */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Animatable.__interface__;
   mixin Container.__interface__;
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance texture;
   Texture parent_instance;
   private CairoTexturePrivate* priv;


   // VERSION: 1.0
   // Creates a new #ClutterCairoTexture actor, with a surface of @width by
   // @height pixels.
   // RETURNS: the newly created #ClutterCairoTexture actor
   // <width>: the width of the surface
   // <height>: the height of the surface
   static CairoTexture* new_()(uint width, uint height) nothrow {
      return clutter_cairo_texture_new(width, height);
   }
   static auto opCall()(uint width, uint height) {
      return clutter_cairo_texture_new(width, height);
   }

   // VERSION: 1.0
   // Clears @self's internal drawing surface, so that the next upload
   // will replace the previous contents of the #ClutterCairoTexture
   // rather than adding to it.
   // 
   // Calling this function from within a #ClutterCairoTexture::draw
   // signal handler will clear the invalidated area.
   void clear()() nothrow {
      clutter_cairo_texture_clear(&this);
   }

   // VERSION: 1.0
   // DEPRECATED (v1.8) method: create - Use the #ClutterCairoTexture::draw signal and
   // Creates a new Cairo context for the @cairo texture. It is
   // similar to using clutter_cairo_texture_create_region() with @x_offset
   // and @y_offset of 0, @width equal to the @cairo texture surface width
   // and @height equal to the @cairo texture surface height.
   // 
   // <warning><para>Do not call this function within the paint virtual
   // function or from a callback to the #ClutterActor::paint
   // signal.</para></warning>
   // 
   // to upload the contents of the context when done drawing
   // 
   // 
   // the clutter_cairo_texture_invalidate() function to obtain a
   // Cairo context for 2D drawing.
   // RETURNS: a newly created Cairo context. Use cairo_destroy()
   cairo.Context* /*new*/ create()() nothrow {
      return clutter_cairo_texture_create(&this);
   }

   // VERSION: 1.0
   // DEPRECATED (v1.8) method: create_region - Use the #ClutterCairoTexture::draw signal and
   // Creates a new Cairo context that will updat the region defined
   // by @x_offset, @y_offset, @width and @height.
   // 
   // <warning><para>Do not call this function within the paint virtual
   // function or from a callback to the #ClutterActor::paint
   // signal.</para></warning>
   // 
   // to upload the contents of the context when done drawing
   // 
   // 
   // clutter_cairo_texture_invalidate_rectangle() to obtain a
   // clipped Cairo context for 2D drawing.
   // RETURNS: a newly created Cairo context. Use cairo_destroy()
   // <x_offset>: offset of the region on the X axis
   // <y_offset>: offset of the region on the Y axis
   // <width>: width of the region, or -1 for the full surface width
   // <height>: height of the region, or -1 for the full surface height
   cairo.Context* /*new*/ create_region()(int x_offset, int y_offset, int width, int height) nothrow {
      return clutter_cairo_texture_create_region(&this, x_offset, y_offset, width, height);
   }

   // VERSION: 1.8
   // Retrieves the value set using clutter_cairo_texture_set_auto_resize().
   // 
   // allocation, and %FALSE otherwise
   // RETURNS: %TRUE if the #ClutterCairoTexture should track the
   int get_auto_resize()() nothrow {
      return clutter_cairo_texture_get_auto_resize(&this);
   }

   // VERSION: 1.0
   // Retrieves the surface width and height for @self.
   // <width>: return location for the surface width, or %NULL
   // <height>: return location for the surface height, or %NULL
   void get_surface_size(AT0, AT1)(/*out*/ AT0 /*uint*/ width, /*out*/ AT1 /*uint*/ height) nothrow {
      clutter_cairo_texture_get_surface_size(&this, UpCast!(uint*)(width), UpCast!(uint*)(height));
   }

   // VERSION: 1.8
   // Invalidates the whole surface of a #ClutterCairoTexture.
   // 
   // This function will cause the #ClutterCairoTexture::draw signal
   // to be emitted.
   // 
   // See also: clutter_cairo_texture_invalidate_rectangle()
   void invalidate()() nothrow {
      clutter_cairo_texture_invalidate(&this);
   }

   // VERSION: 1.8
   // Invalidates a rectangular region of a #ClutterCairoTexture.
   // 
   // The invalidation will cause the #ClutterCairoTexture::draw signal
   // to be emitted.
   // 
   // See also: clutter_cairo_texture_invalidate()
   // <rect>: a rectangle with the area to invalida, or %NULL to perform an unbounded invalidation
   void invalidate_rectangle(AT0)(AT0 /*cairo.RectangleInt*/ rect=null) nothrow {
      clutter_cairo_texture_invalidate_rectangle(&this, UpCast!(cairo.RectangleInt*)(rect));
   }

   // VERSION: 1.8
   // Sets whether the #ClutterCairoTexture should ensure that the
   // backing Cairo surface used matches the allocation assigned to
   // the actor. If the allocation changes, the contents of the
   // #ClutterCairoTexture will also be invalidated automatically.
   // <value>: %TRUE if the #ClutterCairoTexture should bind the surface size to the allocation
   void set_auto_resize()(int value) nothrow {
      clutter_cairo_texture_set_auto_resize(&this, value);
   }

   // VERSION: 1.0
   // Resizes the Cairo surface used by @self to @width and @height.
   // 
   // This function will not invalidate the contents of the Cairo
   // texture: you will have to explicitly call either
   // clutter_cairo_texture_invalidate_rectangle() or
   // clutter_cairo_texture_invalidate().
   // <width>: the new width of the surface
   // <height>: the new height of the surface
   void set_surface_size()(uint width, uint height) nothrow {
      clutter_cairo_texture_set_surface_size(&this, width, height);
   }

   // VERSION: 1.6
   // The ::create-surface signal is emitted when a #ClutterCairoTexture
   // news its surface (re)created, which happens either when the Cairo
   // context is created with clutter_cairo_texture_create() or
   // clutter_cairo_texture_create_region(), or when the surface is resized
   // through clutter_cairo_texture_set_surface_size().
   // 
   // The first signal handler that returns a non-%NULL, valid surface will
   // stop any further signal emission, and the returned surface will be
   // the one used.
   // RETURNS: the newly created #cairo_surface_t for the texture
   // <width>: the width of the surface to create
   // <height>: the height of the surface to create
   extern (C) alias static cairo.Surface* /*new*/ function (CairoTexture* this_, c_uint width, c_uint height, void* user_data=null) nothrow signal_create_surface;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"create-surface", CB/*:signal_create_surface*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_create_surface)||_ttmm!(CB, signal_create_surface)()) {
      return signal_connect_data!()(&this, cast(char*)"create-surface",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.8
   // The ::draw signal is emitted each time a #ClutterCairoTexture has
   // been invalidated.
   // 
   // The passed Cairo context passed will be clipped to the invalidated
   // area.
   // 
   // It is safe to connect multiple callbacks to this signals; the state
   // of the Cairo context passed to each callback is automatically saved
   // and restored, so it's not necessary to call cairo_save() and
   // cairo_restore().
   // 
   // to continue
   // RETURNS: %TRUE if the signal emission should stop, and %FALSE
   // <cr>: the Cairo context to use to draw
   extern (C) alias static c_int function (CairoTexture* this_, cairo.Context* cr, void* user_data=null) nothrow signal_draw;
   ulong signal_connect(string name:"draw", CB/*:signal_draw*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_draw)||_ttmm!(CB, signal_draw)()) {
      return signal_connect_data!()(&this, cast(char*)"draw",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// The #ClutterCairoTextureClass struct contains only private data.
struct CairoTextureClass /* Version 1.0 */ {
   private TextureClass parent_class;
   extern (C) cairo.Surface* /*new*/ function (CairoTexture* texture, uint width, uint height) nothrow create_surface;
   extern (C) int function (CairoTexture* texture, cairo.Context* cr) nothrow draw;
   extern (C) void function () nothrow _clutter_cairo_3;
   extern (C) void function () nothrow _clutter_cairo_4;
}

struct CairoTexturePrivate {
}

enum int Calculator = 269025053;
enum int Calendar = 269025056;

// Generic callback
// <actor>: a #ClutterActor
// <data>: user data
extern (C) alias void function (Actor* actor, void* data) nothrow Callback;

enum int Cancel = 65385;
enum int Caps_Lock = 65509;
enum int Ccaron = 456;
enum int Ccedilla = 199;
enum int Ccircumflex = 710;
enum int Ch = 65185;

// Base interface for container specific state for child actors. A child
// data is meant to be used when you need to keep track of information
// about each individual child added to a container.
// 
// In order to use it you should create your own subclass of
// #ClutterChildMeta and set the #ClutterContainerIface child_meta_type
// interface member to your subclass type, like:
// 
// |[
// static void
// my_container_iface_init (ClutterContainerIface *iface)
// {
// /&ast; set the rest of the #ClutterContainer vtable &ast;/
// 
// container_iface->child_meta_type  = MY_TYPE_CHILD_META;
// }
// ]|
// 
// This will automatically create a #ClutterChildMeta of type
// MY_TYPE_CHILD_META for every actor that is added to the container.
// 
// The child data for an actor can be retrieved using the
// clutter_container_get_child_meta() function.
// 
// The properties of the data and your subclass can be manipulated with
// clutter_container_child_set() and clutter_container_child_get() which
// act like g_object_set() and g_object_get().
// 
// You can provide hooks for your own storage as well as control the
// instantiation by overriding the #ClutterContainerIface virtual functions
// <function>create_child_meta</function>,
// <function>destroy_child_meta</function>,
// and <function>get_child_meta</function>.
struct ChildMeta /* : GObject.Object */ /* Version 0.8 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   Container* container;
   Actor* actor;


   // VERSION: 0.8
   // Retrieves the actor wrapped by @data
   // RETURNS: a #ClutterActor
   Actor* get_actor()() nothrow {
      return clutter_child_meta_get_actor(&this);
   }

   // VERSION: 0.8
   // Retrieves the container using @data
   // RETURNS: a #ClutterContainer
   Container* get_container()() nothrow {
      return clutter_child_meta_get_container(&this);
   }
}

// The #ClutterChildMetaClass contains only private data
struct ChildMetaClass /* Version 0.8 */ {
   private GObject2.ObjectClass parent_class;
}

enum int Clear = 65291;
enum int ClearGrab = 269024801;

// The <structname>ClutterClickAction</structname> structure contains
// only private data and should be accessed using the provided API
struct ClickAction /* : Action */ /* Version 1.4 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance action;
   Action parent_instance;
   private ClickActionPrivate* priv;


   // VERSION: 1.4
   // Creates a new #ClutterClickAction instance
   // RETURNS: the newly created #ClutterClickAction
   static ClickAction* new_()() nothrow {
      return clutter_click_action_new();
   }
   static auto opCall()() {
      return clutter_click_action_new();
   }

   // VERSION: 1.4
   // Retrieves the button that was pressed.
   // RETURNS: the button value
   uint get_button()() nothrow {
      return clutter_click_action_get_button(&this);
   }

   // VERSION: 1.8
   // Retrieves the screen coordinates of the button press.
   // <press_x>: return location for the X coordinate, or %NULL
   // <press_y>: return location for the Y coordinate, or %NULL
   void get_coords(AT0, AT1)(/*out*/ AT0 /*float*/ press_x, /*out*/ AT1 /*float*/ press_y) nothrow {
      clutter_click_action_get_coords(&this, UpCast!(float*)(press_x), UpCast!(float*)(press_y));
   }

   // VERSION: 1.6
   // Retrieves the modifier state of the click action.
   // RETURNS: the modifier state parameter, or 0
   ModifierType get_state()() nothrow {
      return clutter_click_action_get_state(&this);
   }

   // VERSION: 1.4
   // Emulates a release of the pointer button, which ungrabs the pointer
   // and unsets the #ClutterClickAction:pressed state.
   // 
   // This function will also cancel the long press gesture if one was
   // initiated.
   // 
   // This function is useful to break a grab, for instance after a certain
   // amount of time has passed.
   void release()() nothrow {
      clutter_click_action_release(&this);
   }

   // VERSION: 1.4
   // The ::clicked signal is emitted when the #ClutterActor to which
   // a #ClutterClickAction has been applied should respond to a
   // pointer button press and release events
   // <actor>: the #ClutterActor attached to the @action
   extern (C) alias static void function (ClickAction* this_, Actor* actor, void* user_data=null) nothrow signal_clicked;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"clicked", CB/*:signal_clicked*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_clicked)||_ttmm!(CB, signal_clicked)()) {
      return signal_connect_data!()(&this, cast(char*)"clicked",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.8
   // The ::long-press signal is emitted during the long press gesture
   // handling.
   // 
   // This signal can be emitted multiple times with different states.
   // 
   // The %CLUTTER_LONG_PRESS_QUERY state will be emitted on button presses,
   // and its return value will determine whether the long press handling
   // should be initiated. If the signal handlers will return %TRUE, the
   // %CLUTTER_LONG_PRESS_QUERY state will be followed either by a signal
   // emission with the %CLUTTER_LONG_PRESS_ACTIVATE state if the long press
   // constraints were respected, or by a signal emission with the
   // %CLUTTER_LONG_PRESS_CANCEL state if the long press was cancelled.
   // 
   // It is possible to forcibly cancel a long press detection using
   // clutter_click_action_release().
   // 
   // returned value of the handler; other states will ignore it
   // RETURNS: Only the %CLUTTER_LONG_PRESS_QUERY state uses the
   // <actor>: the #ClutterActor attached to the @action
   // <state>: the long press state
   extern (C) alias static c_int function (ClickAction* this_, Actor* actor, LongPressState* state, void* user_data=null) nothrow signal_long_press;
   ulong signal_connect(string name:"long-press", CB/*:signal_long_press*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_long_press)||_ttmm!(CB, signal_long_press)()) {
      return signal_connect_data!()(&this, cast(char*)"long-press",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}


// The <structname>ClutterClickActionClass</structname> structure
// contains only private data
struct ClickActionClass /* Version 1.4 */ {
   private ActionClass parent_class;
   extern (C) void function (ClickAction* action, Actor* actor) nothrow clicked;
   extern (C) int function (ClickAction* action, Actor* actor, LongPressState state) nothrow long_press;
   extern (C) void function () nothrow _clutter_click_action1;
   extern (C) void function () nothrow _clutter_click_action2;
   extern (C) void function () nothrow _clutter_click_action3;
   extern (C) void function () nothrow _clutter_click_action4;
   extern (C) void function () nothrow _clutter_click_action5;
   extern (C) void function () nothrow _clutter_click_action6;
   extern (C) void function () nothrow _clutter_click_action7;
}

struct ClickActionPrivate {
}


// The #ClutterClone structure contains only private data
// and should be accessed using the provided API
struct Clone /* : Actor */ /* Version 1.0 */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Animatable.__interface__;
   mixin Container.__interface__;
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance actor;
   Actor parent_instance;
   private ClonePrivate* priv;


   // VERSION: 1.0
   // Creates a new #ClutterActor which clones @source/
   // RETURNS: the newly created #ClutterClone
   // <source>: a #ClutterActor, or %NULL
   static Clone* new_(AT0)(AT0 /*Actor*/ source) nothrow {
      return clutter_clone_new(UpCast!(Actor*)(source));
   }
   static auto opCall(AT0)(AT0 /*Actor*/ source) {
      return clutter_clone_new(UpCast!(Actor*)(source));
   }

   // VERSION: 1.0
   // Retrieves the source #ClutterActor being cloned by @self.
   // RETURNS: the actor source for the clone
   Actor* get_source()() nothrow {
      return clutter_clone_get_source(&this);
   }

   // VERSION: 1.0
   // Sets @source as the source actor to be cloned by @self.
   // <source>: a #ClutterActor, or %NULL
   void set_source(AT0)(AT0 /*Actor*/ source=null) nothrow {
      clutter_clone_set_source(&this, UpCast!(Actor*)(source));
   }
}

// The #ClutterCloneClass structure contains only private data
struct CloneClass /* Version 1.0 */ {
   private ActorClass parent_class;
   extern (C) void function () nothrow _clutter_actor_clone1;
   extern (C) void function () nothrow _clutter_actor_clone2;
   extern (C) void function () nothrow _clutter_actor_clone3;
   extern (C) void function () nothrow _clutter_actor_clone4;
}

struct ClonePrivate {
}

enum int Close = 269025110;
enum int Codeinput = 65335;
enum int ColonSign = 16785569;
// Color representation.
struct Color {
   ubyte red, green, blue, alpha;


   // VERSION: 0.8.4
   // Creates a new #ClutterColor with the given values.
   // 
   // Use clutter_color_free() when done
   // RETURNS: the newly allocated color.
   // <red>: red component of the color, between 0 and 255
   // <green>: green component of the color, between 0 and 255
   // <blue>: blue component of the color, between 0 and 255
   // <alpha>: alpha component of the color, between 0 and 255
   static Color* /*new*/ new_()(ubyte red, ubyte green, ubyte blue, ubyte alpha) nothrow {
      return clutter_color_new(red, green, blue, alpha);
   }
   static auto opCall()(ubyte red, ubyte green, ubyte blue, ubyte alpha) {
      return clutter_color_new(red, green, blue, alpha);
   }

   // Adds @a to @b and saves the resulting color inside @result.
   // 
   // The alpha channel of @result is set as as the maximum value
   // between the alpha channels of @a and @b.
   // <b>: a #ClutterColor
   // <result>: return location for the result
   void add(AT0, AT1)(AT0 /*Color*/ b, /*out*/ AT1 /*Color*/ result) nothrow {
      clutter_color_add(&this, UpCast!(Color*)(b), UpCast!(Color*)(result));
   }

   // VERSION: 0.2
   // Makes a copy of the color structure.  The result must be
   // freed using clutter_color_free().
   // RETURNS: an allocated copy of @color.
   Color* /*new*/ copy()() nothrow {
      return clutter_color_copy(&this);
   }

   // Darkens @color by a fixed amount, and saves the changed color
   // in @result.
   // <result>: return location for the darker color
   void darken(AT0)(/*out*/ AT0 /*Color*/ result) nothrow {
      clutter_color_darken(&this, UpCast!(Color*)(result));
   }

   // VERSION: 0.2
   // Frees a color structure created with clutter_color_copy().
   void free()() nothrow {
      clutter_color_free(&this);
   }

   // Converts a color expressed in HLS (hue, luminance and saturation)
   // values into a #ClutterColor.
   // <hue>: hue value, in the 0 .. 360 range
   // <luminance>: luminance value, in the 0 .. 1 range
   // <saturation>: saturation value, in the 0 .. 1 range
   void from_hls()(float hue, float luminance, float saturation) nothrow {
      clutter_color_from_hls(&this, hue, luminance, saturation);
   }

   // Converts @pixel from the packed representation of a four 8 bit channel
   // color to a #ClutterColor.
   // <pixel>: a 32 bit packed integer containing a color
   void from_pixel()(uint pixel) nothrow {
      clutter_color_from_pixel(&this, pixel);
   }

   // VERSION: 1.0
   // Parses a string definition of a color, filling the
   // <structfield>red</structfield>, <structfield>green</structfield>, 
   // <structfield>blue</structfield> and <structfield>alpha</structfield> 
   // channels of @color.
   // 
   // The @color is not allocated.
   // 
   // The format of @str can be either one of:
   // 
   // <itemizedlist>
   // <listitem>
   // <para>a standard name (as taken from the X11 rgb.txt file)</para>
   // </listitem>
   // <listitem>
   // <para>an hexadecimal value in the form: <literal>&num;rgb</literal>,
   // <literal>&num;rrggbb</literal>, <literal>&num;rgba</literal> or
   // <literal>&num;rrggbbaa</literal></para>
   // </listitem>
   // <listitem>
   // <para>a RGB color in the form: <literal>rgb(r, g, b)</literal></para>
   // </listitem>
   // <listitem>
   // <para>a RGB color in the form: <literal>rgba(r, g, b, a)</literal></para>
   // </listitem>
   // <listitem>
   // <para>a HSL color in the form: <literal>hsl(h, s, l)</literal></para>
   // </listitem>
   // <listitem>
   // <para>a HSL color in the form: <literal>hsla(h, s, l, a)</literal></para>
   // </listitem>
   // </itemizedlist>
   // 
   // where 'r', 'g', 'b' and 'a' are (respectively) the red, green, blue color
   // intensities and the opacity. The 'h', 's' and 'l' are (respectively) the
   // hue, saturation and luminance values.
   // 
   // In the rgb() and rgba() formats, the 'r', 'g', and 'b' values are either
   // integers between 0 and 255, or percentage values in the range between 0%
   // and 100%; the percentages require the '%' character. The 'a' value, if
   // specified, can only be a floating point value between 0.0 and 1.0.
   // 
   // In the hls() and hlsa() formats, the 'h' value (hue) it's an angle between
   // 0 and 360.0 degrees; the 'l' and 's' values (luminance and saturation) are
   // a floating point value between 0.0 and 1.0. The 'a' value, if specified,
   // can only be a floating point value between 0.0 and 1.0.
   // 
   // Whitespace inside the definitions is ignored; no leading whitespace
   // is allowed.
   // 
   // If the alpha component is not specified then it is assumed to be set to
   // be fully opaque.
   // RETURNS: %TRUE if parsing succeeded, and %FALSE otherwise
   // <str>: a string specifiying a color
   int from_string(AT0)(AT0 /*char*/ str) nothrow {
      return clutter_color_from_string(&this, toCString!(char*)(str));
   }

   // VERSION: 1.6
   // Interpolates between @initial and @final #ClutterColor<!-- -->s
   // using @progress
   // <final>: the final #ClutterColor
   // <progress>: the interpolation progress
   // <result>: return location for the interpolation
   void interpolate(AT0, AT1)(AT0 /*Color*/ final_, double progress, /*out*/ AT1 /*Color*/ result) nothrow {
      clutter_color_interpolate(&this, UpCast!(Color*)(final_), progress, UpCast!(Color*)(result));
   }

   // Lightens @color by a fixed amount, and saves the changed color
   // in @result.
   // <result>: return location for the lighter color
   void lighten(AT0)(/*out*/ AT0 /*Color*/ result) nothrow {
      clutter_color_lighten(&this, UpCast!(Color*)(result));
   }

   // Shades @color by @factor and saves the modified color into @result.
   // <factor>: the shade factor to apply
   // <result>: return location for the shaded color
   void shade(AT0)(double factor, /*out*/ AT0 /*Color*/ result) nothrow {
      clutter_color_shade(&this, factor, UpCast!(Color*)(result));
   }

   // Subtracts @b from @a and saves the resulting color inside @result.
   // 
   // This function assumes that the components of @a are greater than the
   // components of @b; the result is, otherwise, undefined.
   // 
   // The alpha channel of @result is set as the minimum value
   // between the alpha channels of @a and @b.
   // <b>: a #ClutterColor
   // <result>: return location for the result
   void subtract(AT0, AT1)(AT0 /*Color*/ b, /*out*/ AT1 /*Color*/ result) nothrow {
      clutter_color_subtract(&this, UpCast!(Color*)(b), UpCast!(Color*)(result));
   }

   // Converts @color to the HLS format.
   // 
   // The @hue value is in the 0 .. 360 range. The @luminance and
   // @saturation values are in the 0 .. 1 range.
   // <hue>: return location for the hue value or %NULL
   // <luminance>: return location for the luminance value or %NULL
   // <saturation>: return location for the saturation value or %NULL
   void to_hls(AT0, AT1, AT2)(/*out*/ AT0 /*float*/ hue, /*out*/ AT1 /*float*/ luminance, /*out*/ AT2 /*float*/ saturation) nothrow {
      clutter_color_to_hls(&this, UpCast!(float*)(hue), UpCast!(float*)(luminance), UpCast!(float*)(saturation));
   }

   // Converts @color into a packed 32 bit integer, containing
   // all the four 8 bit channels used by #ClutterColor.
   // RETURNS: a packed color
   uint to_pixel()() nothrow {
      return clutter_color_to_pixel(&this);
   }

   // VERSION: 0.2
   // Returns a textual specification of @color in the hexadecimal form
   // <literal>&num;rrggbbaa</literal>, where <literal>r</literal>,
   // <literal>g</literal>, <literal>b</literal> and <literal>a</literal> are
   // hexadecimal digits representing the red, green, blue and alpha components
   // respectively.
   // RETURNS: a newly-allocated text string
   char* /*new*/ to_string()() nothrow {
      return clutter_color_to_string(&this);
   }

   // VERSION: 0.2
   // Compares two #ClutterColor<!-- -->s and checks if they are the same.
   // 
   // This function can be passed to g_hash_table_new() as the @key_equal_func
   // parameter, when using #ClutterColor<!-- -->s as keys in a #GHashTable.
   // RETURNS: %TRUE if the two colors are the same.
   // <v1>: a #ClutterColor
   // <v2>: a #ClutterColor
   static int equal(AT0, AT1)(AT0 /*const(void)*/ v1, AT1 /*const(void)*/ v2) nothrow {
      return clutter_color_equal(UpCast!(const(void)*)(v1), UpCast!(const(void)*)(v2));
   }

   // VERSION: 1.6
   // Retrieves a static color for the given @color name
   // 
   // Static colors are created by Clutter and are guaranteed to always be
   // available and valid
   // 
   // is owned by Clutter and it should never be modified or freed
   // RETURNS: a pointer to a static color; the returned pointer
   // <color>: the named global color
   static Color* get_static()(StaticColor color) nothrow {
      return clutter_color_get_static(color);
   }

   // VERSION: 1.0
   // Converts a #ClutterColor to a hash value.
   // 
   // This function can be passed to g_hash_table_new() as the @hash_func
   // parameter, when using #ClutterColor<!-- -->s as keys in a #GHashTable.
   // RETURNS: a hash value corresponding to the color
   // <v>: a #ClutterColor
   static uint hash(AT0)(AT0 /*const(void)*/ v) nothrow {
      return clutter_color_hash(UpCast!(const(void)*)(v));
   }
}


// <structname>ClutterColorizeEffect</structname> is an opaque structure
// whose members cannot be directly accessed
struct ColorizeEffect /* : OffscreenEffect */ /* Version 1.4 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent offscreeneffect;
   OffscreenEffect method_parent;


   // VERSION: 1.4
   // Creates a new #ClutterColorizeEffect to be used with
   // clutter_actor_add_effect()
   // RETURNS: the newly created #ClutterColorizeEffect or %NULL
   // <tint>: the color to be used
   static ColorizeEffect* new_(AT0)(AT0 /*Color*/ tint) nothrow {
      return clutter_colorize_effect_new(UpCast!(Color*)(tint));
   }
   static auto opCall(AT0)(AT0 /*Color*/ tint) {
      return clutter_colorize_effect_new(UpCast!(Color*)(tint));
   }

   // VERSION: 1.4
   // Retrieves the tint used by @effect
   // <tint>: return location for the color used
   void get_tint(AT0)(/*out*/ AT0 /*Color*/ tint) nothrow {
      clutter_colorize_effect_get_tint(&this, UpCast!(Color*)(tint));
   }

   // VERSION: 1.4
   // Sets the tint to be used when colorizing
   // <tint>: the color to be used
   void set_tint(AT0)(AT0 /*Color*/ tint) nothrow {
      clutter_colorize_effect_set_tint(&this, UpCast!(Color*)(tint));
   }
}

struct ColorizeEffectClass {
}

enum int Community = 269025085;

// The <structname>ClutterConstraint</structname> structure contains only
// private data and should be accessed using the provided API
struct Constraint /* : ActorMeta */ /* Version 1.4 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance actormeta;
   ActorMeta parent_instance;
}


// The <structname>ClutterConstraintClass</structname> structure contains
// only private data
struct ConstraintClass /* Version 1.4 */ {
   private ActorMetaClass parent_class;
   extern (C) void function (Constraint* constraint, Actor* actor, ActorBox* allocation) nothrow update_allocation;
   extern (C) void function () nothrow _clutter_constraint1;
   extern (C) void function () nothrow _clutter_constraint2;
   extern (C) void function () nothrow _clutter_constraint3;
   extern (C) void function () nothrow _clutter_constraint4;
   extern (C) void function () nothrow _clutter_constraint5;
   extern (C) void function () nothrow _clutter_constraint6;
   extern (C) void function () nothrow _clutter_constraint7;
   extern (C) void function () nothrow _clutter_constraint8;
}


// #ClutterContainer is an opaque structure whose members cannot be directly
// accessed
struct Container /* Interface */ /* Version 0.4 */ {
   mixin template __interface__() {
      // VERSION: 0.8
      // Looks up the #GParamSpec for a child property of @klass.
      // 
      // if no such property exist.
      // RETURNS: The #GParamSpec for the property or %NULL
      // <klass>: a #GObjectClass implementing the #ClutterContainer interface.
      // <property_name>: a property name.
      static GObject2.ParamSpec* class_find_child_property(AT0, AT1)(AT0 /*GObject2.ObjectClass*/ klass, AT1 /*char*/ property_name) nothrow {
         return clutter_container_class_find_child_property(UpCast!(GObject2.ObjectClass*)(klass), toCString!(char*)(property_name));
      }

      // VERSION: 0.8
      // Returns an array of #GParamSpec for all child properties.
      // 
      // of #GParamSpec<!-- -->s which should be freed after use.
      // RETURNS: an array
      // <klass>: a #GObjectClass implementing the #ClutterContainer interface.
      // <n_properties>: return location for length of returned array.
      static GObject2.ParamSpec** /*new*/ class_list_child_properties(AT0, AT1)(AT0 /*GObject2.ObjectClass*/ klass, /*out*/ AT1 /*uint*/ n_properties) nothrow {
         return clutter_container_class_list_child_properties(UpCast!(GObject2.ObjectClass*)(klass), UpCast!(uint*)(n_properties));
      }

      // Unintrospectable method: add() / clutter_container_add()
      // VERSION: 0.4
      // DEPRECATED (v1.10) method: add - Use clutter_actor_add_child() instead.
      // Adds a list of #ClutterActor<!-- -->s to @container. Each time and
      // actor is added, the "actor-added" signal is emitted. Each actor should
      // be parented to @container, which takes a reference on the actor. You
      // cannot add a #ClutterActor to more than one #ClutterContainer.
      // <first_actor>: the first #ClutterActor to add
      /+ Not available -- variadic methods unsupported - use the C function directly.
         alias clutter_container_add add; // Variadic
      +/

      // VERSION: 0.4
      // DEPRECATED (v1.10) method: add_actor - Use clutter_actor_add_child() instead.
      // Adds a #ClutterActor to @container. This function will emit the
      // "actor-added" signal. The actor should be parented to
      // @container. You cannot add a #ClutterActor to more than one
      // #ClutterContainer.
      // <actor>: the first #ClutterActor to add
      void add_actor(AT0)(AT0 /*Actor*/ actor) nothrow {
         clutter_container_add_actor(cast(Container*)&this, UpCast!(Actor*)(actor));
      }

      // Unintrospectable method: add_valist() / clutter_container_add_valist()
      // VERSION: 0.4
      // DEPRECATED (v1.10) method: add_valist - Use clutter_actor_add_child() instead.
      // Alternative va_list version of clutter_container_add().
      // <first_actor>: the first #ClutterActor to add
      // <var_args>: list of actors to add, followed by %NULL
      void add_valist(AT0)(AT0 /*Actor*/ first_actor, va_list var_args) nothrow {
         clutter_container_add_valist(cast(Container*)&this, UpCast!(Actor*)(first_actor), var_args);
      }

      // Unintrospectable method: child_get() / clutter_container_child_get()
      // VERSION: 0.8
      // Gets @container specific properties of an actor.
      // 
      // In general, a copy is made of the property contents and the caller is
      // responsible for freeing the memory in the appropriate manner for the type, for
      // instance by calling g_free() or g_object_unref().
      // <actor>: a #ClutterActor that is a child of @container.
      // <first_prop>: name of the first property to be set.
      /+ Not available -- variadic methods unsupported - use the C function directly.
         alias clutter_container_child_get child_get; // Variadic
      +/

      // VERSION: 0.8
      // Gets a container specific property of a child of @container, In general,
      // a copy is made of the property contents and the caller is responsible for
      // freeing the memory by calling g_value_unset().
      // 
      // Note that clutter_container_child_set_property() is really intended for
      // language bindings, clutter_container_child_set() is much more convenient
      // for C programming.
      // <child>: a #ClutterActor that is a child of @container.
      // <property>: the name of the property to set.
      // <value>: the value.
      void child_get_property(AT0, AT1, AT2)(AT0 /*Actor*/ child, AT1 /*char*/ property, AT2 /*GObject2.Value*/ value) nothrow {
         clutter_container_child_get_property(cast(Container*)&this, UpCast!(Actor*)(child), toCString!(char*)(property), UpCast!(GObject2.Value*)(value));
      }

      // VERSION: 1.6
      // Calls the <function>child_notify()</function> virtual function of
      // #ClutterContainer. The default implementation will emit the
      // #ClutterContainer::child-notify signal.
      // <child>: a #ClutterActor
      // <pspec>: a #GParamSpec
      void child_notify(AT0, AT1)(AT0 /*Actor*/ child, AT1 /*GObject2.ParamSpec*/ pspec) nothrow {
         clutter_container_child_notify(cast(Container*)&this, UpCast!(Actor*)(child), UpCast!(GObject2.ParamSpec*)(pspec));
      }

      // Unintrospectable method: child_set() / clutter_container_child_set()
      // VERSION: 0.8
      // Sets container specific properties on the child of a container.
      // <actor>: a #ClutterActor that is a child of @container.
      // <first_prop>: name of the first property to be set.
      /+ Not available -- variadic methods unsupported - use the C function directly.
         alias clutter_container_child_set child_set; // Variadic
      +/

      // VERSION: 0.8
      // Sets a container-specific property on a child of @container.
      // <child>: a #ClutterActor that is a child of @container.
      // <property>: the name of the property to set.
      // <value>: the value.
      void child_set_property(AT0, AT1, AT2)(AT0 /*Actor*/ child, AT1 /*char*/ property, AT2 /*GObject2.Value*/ value) nothrow {
         clutter_container_child_set_property(cast(Container*)&this, UpCast!(Actor*)(child), toCString!(char*)(property), UpCast!(GObject2.Value*)(value));
      }

      // VERSION: 1.2
      // Creates the #ClutterChildMeta wrapping @actor inside the
      // @container, if the #ClutterContainerIface::child_meta_type
      // class member is not set to %G_TYPE_INVALID.
      // 
      // This function is only useful when adding a #ClutterActor to
      // a #ClutterContainer implementation outside of the
      // #ClutterContainer::add() virtual function implementation.
      // 
      // Applications should not call this function.
      // <actor>: a #ClutterActor
      void create_child_meta(AT0)(AT0 /*Actor*/ actor) nothrow {
         clutter_container_create_child_meta(cast(Container*)&this, UpCast!(Actor*)(actor));
      }

      // VERSION: 1.2
      // Destroys the #ClutterChildMeta wrapping @actor inside the
      // @container, if any.
      // 
      // This function is only useful when removing a #ClutterActor to
      // a #ClutterContainer implementation outside of the
      // #ClutterContainer::add() virtual function implementation.
      // 
      // Applications should not call this function.
      // <actor>: a #ClutterActor
      void destroy_child_meta(AT0)(AT0 /*Actor*/ actor) nothrow {
         clutter_container_destroy_child_meta(cast(Container*)&this, UpCast!(Actor*)(actor));
      }

      // VERSION: 0.6
      // Finds a child actor of a container by its name. Search recurses
      // into any child container.
      // 
      // or %NULL if no actor with that name was found.
      // RETURNS: The child actor with the requested name,
      // <child_name>: the name of the requested child.
      Actor* find_child_by_name(AT0)(AT0 /*char*/ child_name) nothrow {
         return clutter_container_find_child_by_name(cast(Container*)&this, toCString!(char*)(child_name));
      }

      // VERSION: 0.4
      // DEPRECATED (v1.10) method: foreach - Use clutter_actor_get_first_child() or
      // Calls @callback for each child of @container that was added
      // by the application (with clutter_container_add_actor()). Does
      // not iterate over "internal" children that are part of the
      // container's own implementation, if any.
      // 
      // 
      // clutter_actor_get_last_child() to retrieve the beginning of
      // the list of children, and clutter_actor_get_next_sibling()
      // and clutter_actor_get_previous_sibling() to iterate over it.
      // <callback>: a function to be called for each child
      // <user_data>: data to be passed to the function, or %NULL
      void foreach_(AT0)(Callback callback, AT0 /*void*/ user_data) nothrow {
         clutter_container_foreach(cast(Container*)&this, callback, UpCast!(void*)(user_data));
      }

      // VERSION: 1.0
      // DEPRECATED (v1.10) method: foreach_with_internals - See clutter_container_foreach().
      // Calls @callback for each child of @container, including "internal"
      // children built in to the container itself that were never added
      // by the application.
      // <callback>: a function to be called for each child
      // <user_data>: data to be passed to the function, or %NULL
      void foreach_with_internals(AT0)(Callback callback, AT0 /*void*/ user_data) nothrow {
         clutter_container_foreach_with_internals(cast(Container*)&this, callback, UpCast!(void*)(user_data));
      }

      // VERSION: 0.8
      // Retrieves the #ClutterChildMeta which contains the data about the
      // @container specific state for @actor.
      // 
      // of @container or %NULL if the specifiec actor does not exist or the
      // container is not configured to provide #ClutterChildMeta<!-- -->s
      // RETURNS: the #ClutterChildMeta for the @actor child
      // <actor>: a #ClutterActor that is a child of @container.
      ChildMeta* get_child_meta(AT0)(AT0 /*Actor*/ actor) nothrow {
         return clutter_container_get_child_meta(cast(Container*)&this, UpCast!(Actor*)(actor));
      }

      // VERSION: 0.4
      // DEPRECATED (v1.10) method: get_children - Use clutter_actor_get_children() instead.
      // Retrieves all the children of @container.
      // 
      // of #ClutterActor<!-- -->s. Use g_list_free() on the returned
      // list when done.
      // RETURNS: a list
      GLib2.List* /*new container*/ get_children()() nothrow {
         return clutter_container_get_children(cast(Container*)&this);
      }

      // VERSION: 0.6
      // DEPRECATED (v1.10) method: lower_child - Use clutter_actor_set_child_below_sibling() instead.
      // Lowers @actor to @sibling level, in the depth ordering.
      // <actor>: the actor to raise
      // <sibling>: the sibling to lower to, or %NULL to lower to the bottom
      void lower_child(AT0, AT1)(AT0 /*Actor*/ actor, AT1 /*Actor*/ sibling=null) nothrow {
         clutter_container_lower_child(cast(Container*)&this, UpCast!(Actor*)(actor), UpCast!(Actor*)(sibling));
      }

      // VERSION: 0.6
      // DEPRECATED (v1.10) method: raise_child - Use clutter_actor_set_child_above_sibling() instead.
      // Raises @actor to @sibling level, in the depth ordering.
      // <actor>: the actor to raise
      // <sibling>: the sibling to raise to, or %NULL to raise to the top
      void raise_child(AT0, AT1)(AT0 /*Actor*/ actor, AT1 /*Actor*/ sibling=null) nothrow {
         clutter_container_raise_child(cast(Container*)&this, UpCast!(Actor*)(actor), UpCast!(Actor*)(sibling));
      }

      // Unintrospectable method: remove() / clutter_container_remove()
      // VERSION: 0.4
      // DEPRECATED (v1.10) method: remove - Use clutter_actor_remove_child() instead.
      // Removes a %NULL terminated list of #ClutterActor<!-- -->s from
      // @container. Each actor should be unparented, so if you want to keep it
      // around you must hold a reference to it yourself, using g_object_ref().
      // Each time an actor is removed, the "actor-removed" signal is
      // emitted by @container.
      // <first_actor>: first #ClutterActor to remove
      /+ Not available -- variadic methods unsupported - use the C function directly.
         alias clutter_container_remove remove; // Variadic
      +/

      // VERSION: 0.4
      // DEPRECATED (v1.10) method: remove_actor - Use clutter_actor_remove_child() instead.
      // Removes @actor from @container. The actor should be unparented, so
      // if you want to keep it around you must hold a reference to it
      // yourself, using g_object_ref(). When the actor has been removed,
      // the "actor-removed" signal is emitted by @container.
      // <actor>: a #ClutterActor
      void remove_actor(AT0)(AT0 /*Actor*/ actor) nothrow {
         clutter_container_remove_actor(cast(Container*)&this, UpCast!(Actor*)(actor));
      }

      // Unintrospectable method: remove_valist() / clutter_container_remove_valist()
      // VERSION: 0.4
      // DEPRECATED (v1.10) method: remove_valist - Use clutter_actor_remove_child() instead.
      // Alternative va_list version of clutter_container_remove().
      // <first_actor>: the first #ClutterActor to add
      // <var_args>: list of actors to remove, followed by %NULL
      void remove_valist(AT0)(AT0 /*Actor*/ first_actor, va_list var_args) nothrow {
         clutter_container_remove_valist(cast(Container*)&this, UpCast!(Actor*)(first_actor), var_args);
      }

      // VERSION: 0.6
      // DEPRECATED (v1.10) method: sort_depth_order - The #ClutterContainerIface.sort_depth_order() virtual
      // Sorts a container's children using their depth. This function should not
      // be normally used by applications.
      // 
      // 
      // function should not be used any more; the default implementation in
      // #ClutterContainer does not do anything.
      void sort_depth_order()() nothrow {
         clutter_container_sort_depth_order(cast(Container*)&this);
      }

      // VERSION: 0.4
      // The ::actor-added signal is emitted each time an actor
      // has been added to @container.
      // <actor>: the new child that has been added to @container
      extern (C) alias static void function (Container* this_, Actor* actor, void* user_data=null) nothrow signal_actor_added;

      ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
         return super_.signal_connect!name(cb, data, cf);
      }

      ulong signal_connect(string name:"actor-added", CB/*:signal_actor_added*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_actor_added)||_ttmm!(CB, signal_actor_added)()) {
         return signal_connect_data!()(&this, cast(char*)"actor-added",
         cast(GObject2.Callback)cb, data, null, cf);
      }

      // VERSION: 0.4
      // The ::actor-removed signal is emitted each time an actor
      // is removed from @container.
      // <actor>: the child that has been removed from @container
      extern (C) alias static void function (Container* this_, Actor* actor, void* user_data=null) nothrow signal_actor_removed;
      ulong signal_connect(string name:"actor-removed", CB/*:signal_actor_removed*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_actor_removed)||_ttmm!(CB, signal_actor_removed)()) {
         return signal_connect_data!()(&this, cast(char*)"actor-removed",
         cast(GObject2.Callback)cb, data, null, cf);
      }

      // VERSION: 0.8
      // The ::child-notify signal is emitted each time a property is
      // being set through the clutter_container_child_set() and
      // clutter_container_child_set_property() calls.
      // <actor>: the child that has had a property set
      // <pspec>: the #GParamSpec of the property set
      extern (C) alias static void function (Container* this_, Actor* actor, GObject2.ParamSpec* pspec, void* user_data=null) nothrow signal_child_notify;
      ulong signal_connect(string name:"child-notify", CB/*:signal_child_notify*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_child_notify)||_ttmm!(CB, signal_child_notify)()) {
         return signal_connect_data!()(&this, cast(char*)"child-notify",
         cast(GObject2.Callback)cb, data, null, cf);
      }
   }
   mixin __interface__;
}


// Base interface for container actors. The @add, @remove and @foreach
// virtual functions must be provided by any implementation; the other
// virtual functions are optional.
struct ContainerIface /* Version 0.4 */ {
   private GObject2.TypeInterface g_iface;
   // <actor>: the first #ClutterActor to add
   extern (C) void function (Container* container, Actor* actor) nothrow add;
   // <actor>: a #ClutterActor
   extern (C) void function (Container* container, Actor* actor) nothrow remove;

   // <callback>: a function to be called for each child
   // <user_data>: data to be passed to the function, or %NULL
   extern (C) void function (Container* container, Callback callback, void* user_data) nothrow foreach_;

   // <callback>: a function to be called for each child
   // <user_data>: data to be passed to the function, or %NULL
   extern (C) void function (Container* container, Callback callback, void* user_data) nothrow foreach_with_internals;

   // <actor>: the actor to raise
   // <sibling>: the sibling to raise to, or %NULL to raise to the top
   extern (C) void function (Container* container, Actor* actor, Actor* sibling=null) nothrow raise;

   // <actor>: the actor to raise
   // <sibling>: the sibling to lower to, or %NULL to lower to the bottom
   extern (C) void function (Container* container, Actor* actor, Actor* sibling=null) nothrow lower;
   extern (C) void function (Container* container) nothrow sort_depth_order;
   Type child_meta_type;
   // <actor>: a #ClutterActor
   extern (C) void function (Container* container, Actor* actor) nothrow create_child_meta;
   // <actor>: a #ClutterActor
   extern (C) void function (Container* container, Actor* actor) nothrow destroy_child_meta;

   // RETURNS: the #ClutterChildMeta for the @actor child
   // <actor>: a #ClutterActor that is a child of @container.
   extern (C) ChildMeta* function (Container* container, Actor* actor) nothrow get_child_meta;
   extern (C) void function (Container* container, Actor* actor) nothrow actor_added;
   extern (C) void function (Container* container, Actor* actor) nothrow actor_removed;

   // <child>: a #ClutterActor
   // <pspec>: a #GParamSpec
   extern (C) void function (Container* container, Actor* child, GObject2.ParamSpec* pspec) nothrow child_notify;
}

enum int ContrastAdjust = 269025058;
enum int Control_L = 65507;
enum int Control_R = 65508;
enum int Copy = 269025111;
// Event for the movement of the pointer across different actors
struct CrossingEvent /* Version 0.2 */ {
   EventType type;
   uint time;
   EventFlags flags;
   Stage* stage;
   Actor* source;
   float x, y;
   InputDevice* device;
   Actor* related;
}

enum int CruzeiroSign = 16785570;
enum int Cut = 269025112;
enum int CycleAngle = 269025180;
enum int Cyrillic_A = 1761;
enum int Cyrillic_BE = 1762;
enum int Cyrillic_CHE = 1790;
enum int Cyrillic_CHE_descender = 16778422;
enum int Cyrillic_CHE_vertstroke = 16778424;
enum int Cyrillic_DE = 1764;
enum int Cyrillic_DZHE = 1727;
enum int Cyrillic_E = 1788;
enum int Cyrillic_EF = 1766;
enum int Cyrillic_EL = 1772;
enum int Cyrillic_EM = 1773;
enum int Cyrillic_EN = 1774;
enum int Cyrillic_EN_descender = 16778402;
enum int Cyrillic_ER = 1778;
enum int Cyrillic_ES = 1779;
enum int Cyrillic_GHE = 1767;
enum int Cyrillic_GHE_bar = 16778386;
enum int Cyrillic_HA = 1768;
enum int Cyrillic_HARDSIGN = 1791;
enum int Cyrillic_HA_descender = 16778418;
enum int Cyrillic_I = 1769;
enum int Cyrillic_IE = 1765;
enum int Cyrillic_IO = 1715;
enum int Cyrillic_I_macron = 16778466;
enum int Cyrillic_JE = 1720;
enum int Cyrillic_KA = 1771;
enum int Cyrillic_KA_descender = 16778394;
enum int Cyrillic_KA_vertstroke = 16778396;
enum int Cyrillic_LJE = 1721;
enum int Cyrillic_NJE = 1722;
enum int Cyrillic_O = 1775;
enum int Cyrillic_O_bar = 16778472;
enum int Cyrillic_PE = 1776;
enum int Cyrillic_SCHWA = 16778456;
enum int Cyrillic_SHA = 1787;
enum int Cyrillic_SHCHA = 1789;
enum int Cyrillic_SHHA = 16778426;
enum int Cyrillic_SHORTI = 1770;
enum int Cyrillic_SOFTSIGN = 1784;
enum int Cyrillic_TE = 1780;
enum int Cyrillic_TSE = 1763;
enum int Cyrillic_U = 1781;
enum int Cyrillic_U_macron = 16778478;
enum int Cyrillic_U_straight = 16778414;
enum int Cyrillic_U_straight_bar = 16778416;
enum int Cyrillic_VE = 1783;
enum int Cyrillic_YA = 1777;
enum int Cyrillic_YERU = 1785;
enum int Cyrillic_YU = 1760;
enum int Cyrillic_ZE = 1786;
enum int Cyrillic_ZHE = 1782;
enum int Cyrillic_ZHE_descender = 16778390;
enum int Cyrillic_a = 1729;
enum int Cyrillic_be = 1730;
enum int Cyrillic_che = 1758;
enum int Cyrillic_che_descender = 16778423;
enum int Cyrillic_che_vertstroke = 16778425;
enum int Cyrillic_de = 1732;
enum int Cyrillic_dzhe = 1711;
enum int Cyrillic_e = 1756;
enum int Cyrillic_ef = 1734;
enum int Cyrillic_el = 1740;
enum int Cyrillic_em = 1741;
enum int Cyrillic_en = 1742;
enum int Cyrillic_en_descender = 16778403;
enum int Cyrillic_er = 1746;
enum int Cyrillic_es = 1747;
enum int Cyrillic_ghe = 1735;
enum int Cyrillic_ghe_bar = 16778387;
enum int Cyrillic_ha = 1736;
enum int Cyrillic_ha_descender = 16778419;
enum int Cyrillic_hardsign = 1759;
enum int Cyrillic_i = 1737;
enum int Cyrillic_i_macron = 16778467;
enum int Cyrillic_ie = 1733;
enum int Cyrillic_io = 1699;
enum int Cyrillic_je = 1704;
enum int Cyrillic_ka = 1739;
enum int Cyrillic_ka_descender = 16778395;
enum int Cyrillic_ka_vertstroke = 16778397;
enum int Cyrillic_lje = 1705;
enum int Cyrillic_nje = 1706;
enum int Cyrillic_o = 1743;
enum int Cyrillic_o_bar = 16778473;
enum int Cyrillic_pe = 1744;
enum int Cyrillic_schwa = 16778457;
enum int Cyrillic_sha = 1755;
enum int Cyrillic_shcha = 1757;
enum int Cyrillic_shha = 16778427;
enum int Cyrillic_shorti = 1738;
enum int Cyrillic_softsign = 1752;
enum int Cyrillic_te = 1748;
enum int Cyrillic_tse = 1731;
enum int Cyrillic_u = 1749;
enum int Cyrillic_u_macron = 16778479;
enum int Cyrillic_u_straight = 16778415;
enum int Cyrillic_u_straight_bar = 16778417;
enum int Cyrillic_ve = 1751;
enum int Cyrillic_ya = 1745;
enum int Cyrillic_yeru = 1753;
enum int Cyrillic_yu = 1728;
enum int Cyrillic_ze = 1754;
enum int Cyrillic_zhe = 1750;
enum int Cyrillic_zhe_descender = 16778391;
enum int D = 68;
enum int DOS = 269025114;
enum int Dabovedot = 16784906;
enum int Dcaron = 463;

// The <structname>ClutterDeformEffect</structname> structure contains
// only private data and should be accessed using the provided API
struct DeformEffect /* : OffscreenEffect */ /* Version 1.4 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance offscreeneffect;
   OffscreenEffect parent_instance;
   private DeformEffectPrivate* priv;


   // VERSION: 1.4
   // Retrieves the handle to the back face material used by @effect
   // 
   // The returned material is owned by the #ClutterDeformEffect and it
   // should not be freed directly
   // RETURNS: a handle for the material, or %NULL.
   Cogl.Handle get_back_material()() nothrow {
      return clutter_deform_effect_get_back_material(&this);
   }

   // VERSION: 1.4
   // Retrieves the number of horizontal and vertical tiles used to sub-divide
   // the actor's geometry during the effect
   // <x_tiles>: return location for the number of horizontal tiles, or %NULL
   // <y_tiles>: return location for the number of vertical tiles, or %NULL
   void get_n_tiles(AT0, AT1)(/*out*/ AT0 /*uint*/ x_tiles, /*out*/ AT1 /*uint*/ y_tiles) nothrow {
      clutter_deform_effect_get_n_tiles(&this, UpCast!(uint*)(x_tiles), UpCast!(uint*)(y_tiles));
   }

   // VERSION: 1.4
   // Invalidates the @effect<!-- -->'s vertices and, if it is associated
   // to an actor, it will queue a redraw
   void invalidate()() nothrow {
      clutter_deform_effect_invalidate(&this);
   }

   // VERSION: 1.4
   // Sets the material that should be used when drawing the back face
   // of the actor during a deformation
   // 
   // The #ClutterDeformEffect will take a reference on the material's
   // handle
   // <material>: a handle to a Cogl material
   void set_back_material()(Cogl.Handle material=null) nothrow {
      clutter_deform_effect_set_back_material(&this, material);
   }

   // VERSION: 1.4
   // Sets the number of horizontal and vertical tiles to be used
   // when applying the effect
   // 
   // More tiles allow a finer grained deformation at the expenses
   // of computation
   // <x_tiles>: number of horizontal tiles
   // <y_tiles>: number of vertical tiles
   void set_n_tiles()(uint x_tiles, uint y_tiles) nothrow {
      clutter_deform_effect_set_n_tiles(&this, x_tiles, y_tiles);
   }
}


// The <structname>ClutterDeformEffectClass</structname> structure contains
// only private data
struct DeformEffectClass /* Version 1.4 */ {
   private OffscreenEffectClass parent_class;
   extern (C) void function (DeformEffect* effect, float width, float height, Cogl.TextureVertex* vertex) nothrow deform_vertex;
   extern (C) void function () nothrow _clutter_deform1;
   extern (C) void function () nothrow _clutter_deform2;
   extern (C) void function () nothrow _clutter_deform3;
   extern (C) void function () nothrow _clutter_deform4;
   extern (C) void function () nothrow _clutter_deform5;
   extern (C) void function () nothrow _clutter_deform6;
   extern (C) void function () nothrow _clutter_deform7;
}

struct DeformEffectPrivate {
}

enum int Delete = 65535;

// <structname>ClutterDesaturateEffect</structname> is an opaque structure
// whose members cannot be directly accessed
struct DesaturateEffect /* : OffscreenEffect */ /* Version 1.4 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent offscreeneffect;
   OffscreenEffect method_parent;


   // VERSION: 1.4
   // Creates a new #ClutterDesaturateEffect to be used with
   // clutter_actor_add_effect()
   // RETURNS: the newly created #ClutterDesaturateEffect or %NULL
   // <factor>: the desaturation factor, between 0.0 and 1.0
   static DesaturateEffect* new_()(double factor) nothrow {
      return clutter_desaturate_effect_new(factor);
   }
   static auto opCall()(double factor) {
      return clutter_desaturate_effect_new(factor);
   }

   // VERSION: 1.4
   // Retrieves the desaturation factor of @effect
   // RETURNS: the desaturation factor
   double get_factor()() nothrow {
      return clutter_desaturate_effect_get_factor(&this);
   }

   // VERSION: 1.4
   // Sets the desaturation factor for @effect, with 0.0 being "do not desaturate"
   // and 1.0 being "fully desaturate"
   // <factor>: the desaturation factor, between 0.0 and 1.0
   void set_factor()(double factor) nothrow {
      clutter_desaturate_effect_set_factor(&this, factor);
   }
}

struct DesaturateEffectClass {
}

// The #ClutterDeviceManager structure contains only private data
struct DeviceManager /* : GObject.Object */ /* Version 1.2 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private DeviceManagerPrivate* priv;


   // VERSION: 1.2
   // Retrieves the device manager singleton
   // 
   // The returned instance is owned by Clutter and it should not be
   // modified or freed
   // RETURNS: the #ClutterDeviceManager singleton.
   static DeviceManager* get_default()() nothrow {
      return clutter_device_manager_get_default();
   }

   // VERSION: 1.2
   // Retrieves the core #ClutterInputDevice of type @device_type
   // 
   // Core devices are devices created automatically by the default
   // Clutter backend
   // 
   // returned device is owned by the #ClutterDeviceManager and should
   // not be modified or freed
   // RETURNS: a #ClutterInputDevice or %NULL. The
   // <device_type>: the type of the core device
   InputDevice* get_core_device()(InputDeviceType device_type) nothrow {
      return clutter_device_manager_get_core_device(&this, device_type);
   }

   // VERSION: 1.2
   // Retrieves the #ClutterInputDevice with the given @device_id
   // 
   // returned device is owned by the #ClutterDeviceManager and should
   // never be modified or freed
   // RETURNS: a #ClutterInputDevice or %NULL. The
   // <device_id>: the integer id of a device
   InputDevice* get_device()(int device_id) nothrow {
      return clutter_device_manager_get_device(&this, device_id);
   }

   // VERSION: 1.2
   // Lists all currently registered input devices
   // 
   // a newly allocated list of #ClutterInputDevice objects. Use
   // g_slist_free() to deallocate it when done
   GLib2.SList* /*new container*/ list_devices()() nothrow {
      return clutter_device_manager_list_devices(&this);
   }

   // VERSION: 1.2
   // Lists all currently registered input devices
   // 
   // a pointer to the internal list of #ClutterInputDevice objects. The
   // returned list is owned by the #ClutterDeviceManager and should never
   // be modified or freed
   GLib2.SList* peek_devices()() nothrow {
      return clutter_device_manager_peek_devices(&this);
   }

   // VERSION: 1.2
   // The ::device-added signal is emitted each time a device has been
   // added to the #ClutterDeviceManager
   // <device>: the newly added #ClutterInputDevice
   extern (C) alias static void function (DeviceManager* this_, InputDevice* device, void* user_data=null) nothrow signal_device_added;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"device-added", CB/*:signal_device_added*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_device_added)||_ttmm!(CB, signal_device_added)()) {
      return signal_connect_data!()(&this, cast(char*)"device-added",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.2
   // The ::device-removed signal is emitted each time a device has been
   // removed from the #ClutterDeviceManager
   // <device>: the removed #ClutterInputDevice
   extern (C) alias static void function (DeviceManager* this_, InputDevice* device, void* user_data=null) nothrow signal_device_removed;
   ulong signal_connect(string name:"device-removed", CB/*:signal_device_removed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_device_removed)||_ttmm!(CB, signal_device_removed)()) {
      return signal_connect_data!()(&this, cast(char*)"device-removed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// The #ClutterDeviceManagerClass structure contains only private data
struct DeviceManagerClass /* Version 1.2 */ {
   private GObject2.ObjectClass parent_class;
   // Unintrospectable functionp: get_devices() / ()
   extern (C) GLib2.SList* function (DeviceManager* device_manager) nothrow get_devices;

   // RETURNS: a #ClutterInputDevice or %NULL. The
   // <device_type>: the type of the core device
   extern (C) InputDevice* function (DeviceManager* device_manager, InputDeviceType device_type) nothrow get_core_device;

   // RETURNS: a #ClutterInputDevice or %NULL. The
   // <device_id>: the integer id of a device
   extern (C) InputDevice* function (DeviceManager* device_manager, int device_id) nothrow get_device;
   extern (C) void function (DeviceManager* manager, InputDevice* device) nothrow add_device;
   extern (C) void function (DeviceManager* manager, InputDevice* device) nothrow remove_device;
   private void*[8] _padding;
}

struct DeviceManagerPrivate {
}

enum int Display = 269025113;
enum int Documents = 269025115;
enum int DongSign = 16785579;
enum int Down = 65364;

// The <structname>ClutterDragAction</structname> structure contains only
// private data and should be accessed using the provided API
struct DragAction /* : Action */ /* Version 1.4 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance action;
   Action parent_instance;
   private DragActionPrivate* priv;


   // VERSION: 1.4
   // Creates a new #ClutterDragAction instance
   // RETURNS: the newly created #ClutterDragAction
   static DragAction* new_()() nothrow {
      return clutter_drag_action_new();
   }
   static auto opCall()() {
      return clutter_drag_action_new();
   }

   // VERSION: 1.4
   // Retrieves the axis constraint set by clutter_drag_action_set_drag_axis()
   // RETURNS: the axis constraint
   DragAxis get_drag_axis()() nothrow {
      return clutter_drag_action_get_drag_axis(&this);
   }

   // VERSION: 1.4
   // Retrieves the drag handle set by clutter_drag_action_set_drag_handle()
   // 
   // handle, or %NULL if none was set
   // RETURNS: a #ClutterActor, used as the drag
   Actor* get_drag_handle()() nothrow {
      return clutter_drag_action_get_drag_handle(&this);
   }

   // VERSION: 1.4
   // Retrieves the values set by clutter_drag_action_set_drag_threshold().
   // 
   // If the #ClutterDragAction:x-drag-threshold property or the
   // #ClutterDragAction:y-drag-threshold property have been set to -1 then
   // this function will return the default drag threshold value as stored
   // by the #ClutterSettings:dnd-drag-threshold property of #ClutterSettings.
   // <x_threshold>: return location for the horizontal drag threshold value, in pixels
   // <y_threshold>: return location for the vertical drag threshold value, in pixels
   void get_drag_threshold(AT0, AT1)(/*out*/ AT0 /*uint*/ x_threshold, /*out*/ AT1 /*uint*/ y_threshold) nothrow {
      clutter_drag_action_get_drag_threshold(&this, UpCast!(uint*)(x_threshold), UpCast!(uint*)(y_threshold));
   }

   // VERSION: 1.4
   // Retrieves the coordinates, in stage space, of the latest motion
   // event during the dragging
   // <motion_x>: return location for the latest motion event's X coordinate
   // <motion_y>: return location for the latest motion event's Y coordinate
   void get_motion_coords(AT0, AT1)(/*out*/ AT0 /*float*/ motion_x, /*out*/ AT1 /*float*/ motion_y) nothrow {
      clutter_drag_action_get_motion_coords(&this, UpCast!(float*)(motion_x), UpCast!(float*)(motion_y));
   }

   // VERSION: 1.4
   // Retrieves the coordinates, in stage space, of the press event
   // that started the dragging
   // <press_x>: return location for the press event's X coordinate
   // <press_y>: return location for the press event's Y coordinate
   void get_press_coords(AT0, AT1)(/*out*/ AT0 /*float*/ press_x, /*out*/ AT1 /*float*/ press_y) nothrow {
      clutter_drag_action_get_press_coords(&this, UpCast!(float*)(press_x), UpCast!(float*)(press_y));
   }

   // VERSION: 1.4
   // Restricts the dragging action to a specific axis
   // <axis>: the axis to constraint the dragging to
   void set_drag_axis()(DragAxis axis) nothrow {
      clutter_drag_action_set_drag_axis(&this, axis);
   }

   // VERSION: 1.4
   // Sets the actor to be used as the drag handle.
   // <handle>: a #ClutterActor, or %NULL to unset
   void set_drag_handle(AT0)(AT0 /*Actor*/ handle=null) nothrow {
      clutter_drag_action_set_drag_handle(&this, UpCast!(Actor*)(handle));
   }

   // VERSION: 1.4
   // Sets the horizontal and vertical drag thresholds that must be
   // cleared by the pointer before @action can begin the dragging.
   // 
   // If @x_threshold or @y_threshold are set to -1 then the default
   // drag threshold stored in the #ClutterSettings:dnd-drag-threshold
   // property of #ClutterSettings will be used.
   // <x_threshold>: a distance on the horizontal axis, in pixels, or -1 to use the default drag threshold from #ClutterSettings
   // <y_threshold>: a distance on the vertical axis, in pixels, or -1 to use the default drag threshold from #ClutterSettings
   void set_drag_threshold()(int x_threshold, int y_threshold) nothrow {
      clutter_drag_action_set_drag_threshold(&this, x_threshold, y_threshold);
   }

   // VERSION: 1.4
   // The ::drag-begin signal is emitted when the #ClutterDragAction
   // starts the dragging
   // 
   // The emission of this signal can be delayed by using the
   // #ClutterDragAction:x-drag-threshold and
   // #ClutterDragAction:y-drag-threshold properties
   // <actor>: the #ClutterActor attached to the action
   // <event_x>: the X coordinate (in stage space) of the press event
   // <event_y>: the Y coordinate (in stage space) of the press event
   // <modifiers>: the modifiers of the press event
   extern (C) alias static void function (DragAction* this_, Actor* actor, float event_x, float event_y, ModifierType* modifiers, void* user_data=null) nothrow signal_drag_begin;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"drag-begin", CB/*:signal_drag_begin*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_drag_begin)||_ttmm!(CB, signal_drag_begin)()) {
      return signal_connect_data!()(&this, cast(char*)"drag-begin",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.4
   // The ::drag-end signal is emitted at the end of the dragging,
   // when the pointer button's is released
   // 
   // This signal is emitted if and only if the #ClutterDragAction::drag-begin
   // signal has been emitted first
   // <actor>: the #ClutterActor attached to the action
   // <event_x>: the X coordinate (in stage space) of the release event
   // <event_y>: the Y coordinate (in stage space) of the release event
   // <modifiers>: the modifiers of the release event
   extern (C) alias static void function (DragAction* this_, Actor* actor, float event_x, float event_y, ModifierType* modifiers, void* user_data=null) nothrow signal_drag_end;
   ulong signal_connect(string name:"drag-end", CB/*:signal_drag_end*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_drag_end)||_ttmm!(CB, signal_drag_end)()) {
      return signal_connect_data!()(&this, cast(char*)"drag-end",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.4
   // The ::drag-motion signal is emitted for each motion event after
   // the #ClutterDragAction::drag-begin signal has been emitted.
   // 
   // The components of the distance between the press event and the
   // latest motion event are computed in the actor's coordinate space,
   // to take into account eventual transformations. If you want the
   // stage coordinates of the latest motion event you can use
   // clutter_drag_action_get_motion_coords().
   // 
   // The default handler of the signal will call clutter_actor_move_by()
   // either on @actor or, if set, of #ClutterDragAction:drag-handle using
   // the @delta_x and @delta_y components of the dragging motion. If you
   // want to override the default behaviour, you can connect to this
   // signal and call g_signal_stop_emission_by_name() from within your
   // callback.
   // <actor>: the #ClutterActor attached to the action
   // <delta_x>: the X component of the distance between the press event that began the dragging and the current position of the pointer, as of the latest motion event
   // <delta_y>: the Y component of the distance between the press event that began the dragging and the current position of the pointer, as of the latest motion event
   extern (C) alias static void function (DragAction* this_, Actor* actor, float delta_x, float delta_y, void* user_data=null) nothrow signal_drag_motion;
   ulong signal_connect(string name:"drag-motion", CB/*:signal_drag_motion*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_drag_motion)||_ttmm!(CB, signal_drag_motion)()) {
      return signal_connect_data!()(&this, cast(char*)"drag-motion",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}


// The <structname>ClutterDragActionClass</structname> structure contains
// only private data
struct DragActionClass /* Version 1.4 */ {
   private ActionClass parent_class;
   extern (C) void function (DragAction* action, Actor* actor, float event_x, float event_y, ModifierType modifiers) nothrow drag_begin;
   extern (C) void function (DragAction* action, Actor* actor, float delta_x, float delta_y) nothrow drag_motion;
   extern (C) void function (DragAction* action, Actor* actor, float event_x, float event_y, ModifierType modifiers) nothrow drag_end;
   extern (C) void function () nothrow _clutter_drag_action1;
   extern (C) void function () nothrow _clutter_drag_action2;
   extern (C) void function () nothrow _clutter_drag_action3;
   extern (C) void function () nothrow _clutter_drag_action4;
   extern (C) void function () nothrow _clutter_drag_action5;
}

struct DragActionPrivate {
}


// The axis of the constraint that should be applied on the
// dragging action
enum DragAxis /* Version 1.4 */ {
   AXIS_NONE = 0,
   X_AXIS = 1,
   Y_AXIS = 2
}

// The <structname>ClutterDropAction</structname> structure contains only
// private data and should be accessed using the provided API.
struct DropAction /* : Action */ /* Version 1.8 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance action;
   Action parent_instance;
   private DropActionPrivate* priv;


   // VERSION: 1.8
   // Creates a new #ClutterDropAction.
   // 
   // Use clutter_actor_add_action() to add the action to a #ClutterActor.
   // RETURNS: the newly created #ClutterDropAction
   static DropAction* new_()() nothrow {
      return clutter_drop_action_new();
   }
   static auto opCall()() {
      return clutter_drop_action_new();
   }

   // VERSION: 1.8
   // The ::can-drop signal is emitted when the dragged actor is dropped
   // on @actor. The return value of the ::can-drop signal will determine
   // whether or not the #ClutterDropAction::drop signal is going to be
   // emitted on @action.
   // 
   // The default implementation of #ClutterDropAction returns %TRUE for
   // this signal.
   // RETURNS: %TRUE if the drop is accepted, and %FALSE otherwise
   // <actor>: the #ClutterActor attached to the @action
   // <returns>: %TRUE if the drop is accepted, and %FALSE otherwise
   extern (C) alias static c_int function (DropAction* this_, Actor* actor, float returns, float since, void* user_data=null) nothrow signal_can_drop;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"can-drop", CB/*:signal_can_drop*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_can_drop)||_ttmm!(CB, signal_can_drop)()) {
      return signal_connect_data!()(&this, cast(char*)"can-drop",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.8
   // The ::drop signal is emitted when the dragged actor is dropped
   // on @actor. This signal is only emitted if at least an handler of
   // #ClutterDropAction::can-drop returns %TRUE.
   extern (C) alias static void function (DropAction* this_, Actor* object, float p0, float p1, void* user_data=null) nothrow signal_drop;
   ulong signal_connect(string name:"drop", CB/*:signal_drop*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_drop)||_ttmm!(CB, signal_drop)()) {
      return signal_connect_data!()(&this, cast(char*)"drop",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.8
   // The ::over-in signal is emitted when the dragged actor crosses
   // into @actor.
   // <actor>: the #ClutterActor attached to the @action
   extern (C) alias static void function (DropAction* this_, Actor* actor, void* user_data=null) nothrow signal_over_in;
   ulong signal_connect(string name:"over-in", CB/*:signal_over_in*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_over_in)||_ttmm!(CB, signal_over_in)()) {
      return signal_connect_data!()(&this, cast(char*)"over-in",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.8
   // The ::over-out signal is emitted when the dragged actor crosses
   // outside @actor.
   // <actor>: the #ClutterActor attached to the @action
   extern (C) alias static void function (DropAction* this_, Actor* actor, void* user_data=null) nothrow signal_over_out;
   ulong signal_connect(string name:"over-out", CB/*:signal_over_out*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_over_out)||_ttmm!(CB, signal_over_out)()) {
      return signal_connect_data!()(&this, cast(char*)"over-out",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}


// The <structname>ClutterDropActionClass</structname> structure contains
// only private data.
struct DropActionClass /* Version 1.8 */ {
   private ActionClass parent_class;
   extern (C) int function (DropAction* action, Actor* actor, float event_x, float event_y) nothrow can_drop;
   extern (C) void function (DropAction* action, Actor* actor) nothrow over_in;
   extern (C) void function (DropAction* action, Actor* actor) nothrow over_out;
   extern (C) void function (DropAction* action, Actor* actor, float event_x, float event_y) nothrow drop;
   extern (C) void function () nothrow _clutter_drop_action1;
   extern (C) void function () nothrow _clutter_drop_action2;
   extern (C) void function () nothrow _clutter_drop_action3;
   extern (C) void function () nothrow _clutter_drop_action4;
   extern (C) void function () nothrow _clutter_drop_action5;
   extern (C) void function () nothrow _clutter_drop_action6;
   extern (C) void function () nothrow _clutter_drop_action7;
   extern (C) void function () nothrow _clutter_drop_action8;
}

struct DropActionPrivate {
}

enum int Dstroke = 464;
enum int E = 69;
enum int ENG = 957;
enum int ETH = 208;
enum int Eabovedot = 972;
enum int Eacute = 201;
enum int Ebelowdot = 16785080;
enum int Ecaron = 460;
enum int Ecircumflex = 202;
enum int Ecircumflexacute = 16785086;
enum int Ecircumflexbelowdot = 16785094;
enum int Ecircumflexgrave = 16785088;
enum int Ecircumflexhook = 16785090;
enum int Ecircumflextilde = 16785092;
enum int EcuSign = 16785568;
enum int Ediaeresis = 203;

// The #ClutterEffect structure contains only private data and should
// be accessed using the provided API
struct Effect /* : ActorMeta */ /* Version 1.4 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance actormeta;
   ActorMeta parent_instance;


   // VERSION: 1.8
   // Queues a repaint of the effect. The effect can detect when the ‘paint’
   // method is called as a result of this function because it will not
   // have the %CLUTTER_EFFECT_PAINT_ACTOR_DIRTY flag set. In that case the
   // effect is free to assume that the actor has not changed its
   // appearance since the last time it was painted so it doesn't need to
   // call clutter_actor_continue_paint() if it can draw a cached
   // image. This is mostly intended for effects that are using a
   // %CoglOffscreen to redirect the actor (such as
   // %ClutterOffscreenEffect). In that case the effect can save a bit of
   // rendering time by painting the cached texture without causing the
   // entire actor to be painted.
   // 
   // This function can be used by effects that have their own animatable
   // parameters. For example, an effect which adds a varying degree of a
   // red tint to an actor by redirecting it through a CoglOffscreen
   // might have a property to specify the level of tint. When this value
   // changes, the underlying actor doesn't need to be redrawn so the
   // effect can call clutter_effect_queue_repaint() to make sure the
   // effect is repainted.
   // 
   // Note however that modifying the position of the parent of an actor
   // may change the appearance of the actor because its transformation
   // matrix would change. In this case a redraw wouldn't be queued on
   // the actor itself so the %CLUTTER_EFFECT_PAINT_ACTOR_DIRTY would still
   // not be set. The effect can detect this case by keeping track of the
   // last modelview matrix that was used to render the actor and
   // veryifying that it remains the same in the next paint.
   // 
   // Any other effects that are layered on top of the passed in effect
   // will still be passed the %CLUTTER_EFFECT_PAINT_ACTOR_DIRTY flag. If
   // anything queues a redraw on the actor without specifying an effect
   // or with an effect that is lower in the chain of effects than this
   // one then that will override this call. In that case this effect
   // will instead be called with the %CLUTTER_EFFECT_PAINT_ACTOR_DIRTY
   // flag set.
   void queue_repaint()() nothrow {
      clutter_effect_queue_repaint(&this);
   }
}

// The #ClutterEffectClass structure contains only private data
struct EffectClass /* Version 1.4 */ {
   private ActorMetaClass parent_class;
   extern (C) int function (Effect* effect) nothrow pre_paint;
   extern (C) void function (Effect* effect) nothrow post_paint;
   extern (C) int function (Effect* effect, PaintVolume* volume) nothrow get_paint_volume;
   extern (C) void function (Effect* effect, EffectPaintFlags flags) nothrow paint;
   extern (C) void function (Effect* effect, EffectPaintFlags flags) nothrow pick;
   extern (C) void function () nothrow _clutter_effect4;
   extern (C) void function () nothrow _clutter_effect5;
   extern (C) void function () nothrow _clutter_effect6;
}

// Flags passed to the ‘paint’ or ‘pick’ method of #ClutterEffect.
enum EffectPaintFlags {
   ACTOR_DIRTY = 1
}
enum int Egrave = 200;
enum int Ehook = 16785082;
enum int Eisu_Shift = 65327;
enum int Eisu_toggle = 65328;
enum int Eject = 269025068;
enum int Emacron = 938;
enum int End = 65367;
enum int Eogonek = 458;
enum int Escape = 65307;
enum int Eth = 208;
enum int Etilde = 16785084;
enum int EuroSign = 8364;
// Generic event wrapper.
union Event /* Version 0.2 */ {
   private EventType type_;
   private AnyEvent any;
   private ButtonEvent button;
   private KeyEvent key;
   private MotionEvent motion;
   private ScrollEvent scroll;
   private StageStateEvent stage_state;
   private CrossingEvent crossing;


   // Creates a new #ClutterEvent of the specified type.
   // RETURNS: A newly allocated #ClutterEvent.
   // <type>: The type of event.
   static Event* /*new*/ new_()(EventType type) nothrow {
      return clutter_event_new(type);
   }
   static auto opCall()(EventType type) {
      return clutter_event_new(type);
   }

   // Copies @event.
   // RETURNS: A newly allocated #ClutterEvent
   Event* /*new*/ copy()() nothrow {
      return clutter_event_copy(&this);
   }
   // Frees all resources used by @event.
   void free()() nothrow {
      clutter_event_free(&this);
   }

   // VERSION: 1.6
   // Retrieves the array of axes values attached to the event.
   // RETURNS: an array of axis values
   // <n_axes>: return location for the number of axes returned
   double* get_axes(AT0)(/*out*/ AT0 /*uint*/ n_axes) nothrow {
      return clutter_event_get_axes(&this, UpCast!(uint*)(n_axes));
   }

   // VERSION: 1.0
   // Retrieves the button number of @event
   // RETURNS: the button number
   uint get_button()() nothrow {
      return clutter_event_get_button(&this);
   }

   // VERSION: 1.0
   // Retrieves the number of clicks of @event
   // RETURNS: the click count
   uint get_click_count()() nothrow {
      return clutter_event_get_click_count(&this);
   }

   // VERSION: 0.4
   // Retrieves the coordinates of @event and puts them into @x and @y.
   // <x>: return location for the X coordinate, or %NULL
   // <y>: return location for the Y coordinate, or %NULL
   void get_coords(AT0, AT1)(/*out*/ AT0 /*float*/ x, /*out*/ AT1 /*float*/ y) nothrow {
      clutter_event_get_coords(&this, UpCast!(float*)(x), UpCast!(float*)(y));
   }

   // VERSION: 1.0
   // Retrieves the #ClutterInputDevice for the event.
   // 
   // The #ClutterInputDevice structure is completely opaque and should
   // be cast to the platform-specific implementation.
   // 
   // returned device is owned by the #ClutterEvent and it should not
   // be unreferenced
   // RETURNS: the #ClutterInputDevice or %NULL. The
   InputDevice* get_device()() nothrow {
      return clutter_event_get_device(&this);
   }

   // Retrieves the events device id if set.
   // 
   // no specific device set.
   // RETURNS: A unique identifier for the device or -1 if the event has
   int get_device_id()() nothrow {
      return clutter_event_get_device_id(&this);
   }

   // VERSION: 1.0
   // Retrieves the type of the device for @event
   // 
   // any is set
   // RETURNS: the #ClutterInputDeviceType for the device, if
   InputDeviceType get_device_type()() nothrow {
      return clutter_event_get_device_type(&this);
   }

   // VERSION: 1.0
   // Retrieves the #ClutterEventFlags of @event
   // RETURNS: the event flags
   EventFlags get_flags()() nothrow {
      return clutter_event_get_flags(&this);
   }

   // VERSION: 1.0
   // Retrieves the keycode of the key that caused @event
   // RETURNS: The keycode representing the key
   ushort get_key_code()() nothrow {
      return clutter_event_get_key_code(&this);
   }

   // VERSION: 1.0
   // Retrieves the key symbol of @event
   // RETURNS: the key symbol representing the key
   uint get_key_symbol()() nothrow {
      return clutter_event_get_key_symbol(&this);
   }

   // Retrieves the unicode value for the key that caused @keyev.
   // RETURNS: The unicode value representing the key
   uint get_key_unicode()() nothrow {
      return clutter_event_get_key_unicode(&this);
   }

   // VERSION: 1.0
   // Retrieves the related actor of a crossing event.
   // RETURNS: the related #ClutterActor, or %NULL
   Actor* get_related()() nothrow {
      return clutter_event_get_related(&this);
   }

   // VERSION: 1.0
   // Retrieves the direction of the scrolling of @event
   // RETURNS: the scrolling direction
   ScrollDirection get_scroll_direction()() nothrow {
      return clutter_event_get_scroll_direction(&this);
   }

   // VERSION: 0.6
   // Retrieves the source #ClutterActor the event originated from, or
   // NULL if the event has no source.
   // RETURNS: a #ClutterActor
   Actor* get_source()() nothrow {
      return clutter_event_get_source(&this);
   }

   // VERSION: 1.6
   // Retrieves the hardware device that originated the event.
   // 
   // If you need the virtual device, use clutter_event_get_device().
   // 
   // If no hardware device originated this event, this function will
   // return the same device as clutter_event_get_device().
   // 
   // or %NULL
   // RETURNS: a pointer to a #ClutterInputDevice
   InputDevice* get_source_device()() nothrow {
      return clutter_event_get_source_device(&this);
   }

   // VERSION: 0.8
   // Retrieves the source #ClutterStage the event originated for, or
   // %NULL if the event has no stage.
   // RETURNS: a #ClutterStage
   Stage* get_stage()() nothrow {
      return clutter_event_get_stage(&this);
   }

   // VERSION: 0.4
   // Retrieves the modifier state of the event.
   // RETURNS: the modifier state parameter, or 0
   ModifierType get_state()() nothrow {
      return clutter_event_get_state(&this);
   }

   // VERSION: 0.4
   // Retrieves the time of the event.
   // RETURNS: the time of the event, or %CLUTTER_CURRENT_TIME
   uint get_time()() nothrow {
      return clutter_event_get_time(&this);
   }

   // VERSION: 0.6
   // Puts a copy of the event on the back of the event queue. The event will
   // have the %CLUTTER_EVENT_FLAG_SYNTHETIC flag set. If the source is set
   // event signals will be emitted for this source and capture/bubbling for
   // its ancestors. If the source is not set it will be generated by picking
   // or use the actor that currently has keyboard focus
   void put()() nothrow {
      clutter_event_put(&this);
   }

   // VERSION: 1.8
   // Sets the button number of @event
   // <button>: the button number
   void set_button()(uint button) nothrow {
      clutter_event_set_button(&this, button);
   }

   // VERSION: 1.8
   // Sets the coordinates of the @event.
   // <x>: the X coordinate of the event
   // <y>: the Y coordinate of the event
   void set_coords()(float x, float y) nothrow {
      clutter_event_set_coords(&this, x, y);
   }

   // VERSION: 1.6
   // Sets the device for @event.
   // <device>: a #ClutterInputDevice, or %NULL
   void set_device(AT0)(AT0 /*InputDevice*/ device=null) nothrow {
      clutter_event_set_device(&this, UpCast!(InputDevice*)(device));
   }

   // VERSION: 1.8
   // Sets the #ClutterEventFlags of @event
   // <flags>: a binary OR of #ClutterEventFlags values
   void set_flags()(EventFlags flags) nothrow {
      clutter_event_set_flags(&this, flags);
   }

   // VERSION: 1.8
   // Sets the keycode of the @event.
   // <key_code>: the keycode representing the key
   void set_key_code()(ushort key_code) nothrow {
      clutter_event_set_key_code(&this, key_code);
   }

   // VERSION: 1.8
   // Sets the key symbol of @event.
   // <key_sym>: the key symbol representing the key
   void set_key_symbol()(uint key_sym) nothrow {
      clutter_event_set_key_symbol(&this, key_sym);
   }

   // VERSION: 1.8
   // Sets the Unicode value of @event.
   // <key_unicode>: the Unicode value representing the key
   void set_key_unicode()(uint key_unicode) nothrow {
      clutter_event_set_key_unicode(&this, key_unicode);
   }

   // VERSION: 1.8
   // Sets the related actor of a crossing event
   // <actor>: a #ClutterActor or %NULL
   void set_related(AT0)(AT0 /*Actor*/ actor=null) nothrow {
      clutter_event_set_related(&this, UpCast!(Actor*)(actor));
   }

   // VERSION: 1.8
   // Sets the direction of the scrolling of @event
   // <direction>: the scrolling direction
   void set_scroll_direction()(ScrollDirection direction) nothrow {
      clutter_event_set_scroll_direction(&this, direction);
   }

   // VERSION: 1.8
   // Sets the source #ClutterActor of @event.
   // <actor>: a #ClutterActor, or %NULL
   void set_source(AT0)(AT0 /*Actor*/ actor=null) nothrow {
      clutter_event_set_source(&this, UpCast!(Actor*)(actor));
   }

   // VERSION: 1.8
   // Sets the source #ClutterInputDevice for @event.
   // 
   // The #ClutterEvent must have been created using clutter_event_new().
   // <device>: a #ClutterInputDevice
   void set_source_device(AT0)(AT0 /*InputDevice*/ device=null) nothrow {
      clutter_event_set_source_device(&this, UpCast!(InputDevice*)(device));
   }

   // VERSION: 1.8
   // Sets the source #ClutterStage of the event.
   // <stage>: a #ClutterStage, or %NULL
   void set_stage(AT0)(AT0 /*Stage*/ stage=null) nothrow {
      clutter_event_set_stage(&this, UpCast!(Stage*)(stage));
   }

   // VERSION: 1.8
   // Sets the modifier state of the event.
   // <state>: the modifier state to set
   void set_state()(ModifierType state) nothrow {
      clutter_event_set_state(&this, state);
   }

   // VERSION: 1.8
   // Sets the time of the event.
   // <time_>: the time of the event
   void set_time()(uint time_) nothrow {
      clutter_event_set_time(&this, time_);
   }

   // Retrieves the type of the event.
   // RETURNS: a #ClutterEventType
   EventType type()() nothrow {
      return clutter_event_type(&this);
   }

   // VERSION: 0.4
   // Pops an event off the event queue. Applications should not need to call 
   // this.
   // RETURNS: A #ClutterEvent or NULL if queue empty
   static Event* /*new*/ get()() nothrow {
      return clutter_event_get();
   }

   // VERSION: 0.4
   // Returns a pointer to the first event from the event queue but 
   // does not remove it.
   // RETURNS: A #ClutterEvent or NULL if queue empty.
   static Event* peek()() nothrow {
      return clutter_event_peek();
   }

   //  --- mixin/Clutter_Event.d --->

   void toString(FT)(scope void delegate(const(char)[]) sink, FT fmt) {
      import std.format;
      with (EventType)
      switch (type_) {
      // <KeyEvent> contains a dchar, which causes:
      //Error: static assert  "Cannot put a dchar into a void delegate(const(char)[])"
      //case KEY_PRESS, KEY_RELEASE: formatValue(sink, key, fmt); return;
      case MOTION:                 formatValue(sink, motion, fmt); return;
      case ENTER, LEAVE:           formatValue(sink, crossing, fmt); return;
      case BUTTON_PRESS, BUTTON_RELEASE:
                                   formatValue(sink, button, fmt); return;
      case SCROLL:                 formatValue(sink, scroll, fmt); return;
      case STAGE_STATE:            formatValue(sink, stage_state, fmt); return;
      any: case DELETE, DESTROY_NOTIFY, CLIENT_MESSAGE:
                                   formatValue(sink, any, fmt); return;
      default: sink("/*FIXME*/"); goto any;
      }
   }
   
   // <--- mixin/Clutter_Event.d ---
}

// Flags for the #ClutterEvent
enum EventFlags /* Version 0.6 */ {
   NONE = 0,
   FLAG_SYNTHETIC = 1
}
// Types of events.
enum EventType /* Version 0.4 */ {
   NOTHING = 0,
   KEY_PRESS = 1,
   KEY_RELEASE = 2,
   MOTION = 3,
   ENTER = 4,
   LEAVE = 5,
   BUTTON_PRESS = 6,
   BUTTON_RELEASE = 7,
   SCROLL = 8,
   STAGE_STATE = 9,
   DESTROY_NOTIFY = 10,
   CLIENT_MESSAGE = 11,
   DELETE = 12
}
enum int Excel = 269025116;
enum int Execute = 65378;
enum int Explorer = 269025117;
enum int F = 70;
enum int F1 = 65470;
enum int F10 = 65479;
enum int F11 = 65480;
enum int F12 = 65481;
enum int F13 = 65482;
enum int F14 = 65483;
enum int F15 = 65484;
enum int F16 = 65485;
enum int F17 = 65486;
enum int F18 = 65487;
enum int F19 = 65488;
enum int F2 = 65471;
enum int F20 = 65489;
enum int F21 = 65490;
enum int F22 = 65491;
enum int F23 = 65492;
enum int F24 = 65493;
enum int F25 = 65494;
enum int F26 = 65495;
enum int F27 = 65496;
enum int F28 = 65497;
enum int F29 = 65498;
enum int F3 = 65472;
enum int F30 = 65499;
enum int F31 = 65500;
enum int F32 = 65501;
enum int F33 = 65502;
enum int F34 = 65503;
enum int F35 = 65504;
enum int F4 = 65473;
enum int F5 = 65474;
enum int F6 = 65475;
enum int F7 = 65476;
enum int F8 = 65477;
enum int F9 = 65478;
enum int FFrancSign = 16785571;
enum FLAVOUR = "deprecated";
enum int Fabovedot = 16784926;
enum int Farsi_0 = 16778992;
enum int Farsi_1 = 16778993;
enum int Farsi_2 = 16778994;
enum int Farsi_3 = 16778995;
enum int Farsi_4 = 16778996;
enum int Farsi_5 = 16778997;
enum int Farsi_6 = 16778998;
enum int Farsi_7 = 16778999;
enum int Farsi_8 = 16779000;
enum int Farsi_9 = 16779001;
enum int Farsi_yeh = 16778956;
enum int Favorites = 269025072;

// Runtime flags indicating specific features available via Clutter window
// sysytem and graphics backend.
enum FeatureFlags /* Version 0.4 */ {
   TEXTURE_NPOT = 4,
   SYNC_TO_VBLANK = 8,
   TEXTURE_YUV = 16,
   TEXTURE_READ_PIXELS = 32,
   STAGE_STATIC = 64,
   STAGE_USER_RESIZE = 128,
   STAGE_CURSOR = 256,
   SHADERS_GLSL = 512,
   OFFSCREEN = 1024,
   STAGE_MULTIPLE = 2048,
   SWAP_EVENTS = 4096
}
enum int Finance = 269025084;
enum int Find = 65384;
enum int First_Virtual_Screen = 65232;

// The #ClutterFixedLayout structure contains only private data and
// it should be accessed using the provided API
struct FixedLayout /* : LayoutManager */ /* Version 1.2 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance layoutmanager;
   LayoutManager parent_instance;


   // VERSION: 1.2
   // Creates a new #ClutterFixedLayout
   // RETURNS: the newly created #ClutterFixedLayout
   static FixedLayout* new_()() nothrow {
      return clutter_fixed_layout_new();
   }
   static auto opCall()() {
      return clutter_fixed_layout_new();
   }
}


// The #ClutterFixedLayoutClass structure contains only private data
// and it should be accessed using the provided API
struct FixedLayoutClass /* Version 1.2 */ {
   private LayoutManagerClass parent_class;
}


// The #ClutterFlowLayout structure contains only private data
// and should be accessed using the provided API
struct FlowLayout /* : LayoutManager */ /* Version 1.2 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance layoutmanager;
   LayoutManager parent_instance;
   private FlowLayoutPrivate* priv;


   // VERSION: 1.2
   // Creates a new #ClutterFlowLayout with the given @orientation
   // RETURNS: the newly created #ClutterFlowLayout
   // <orientation>: the orientation of the flow layout
   static FlowLayout* new_()(FlowOrientation orientation) nothrow {
      return clutter_flow_layout_new(orientation);
   }
   static auto opCall()(FlowOrientation orientation) {
      return clutter_flow_layout_new(orientation);
   }

   // VERSION: 1.2
   // Retrieves the spacing between columns
   // 
   // in pixels
   // RETURNS: the spacing between columns of the #ClutterFlowLayout,
   float get_column_spacing()() nothrow {
      return clutter_flow_layout_get_column_spacing(&this);
   }

   // VERSION: 1.2
   // Retrieves the minimum and maximum column widths
   // <min_width>: return location for the minimum column width, or %NULL
   // <max_width>: return location for the maximum column width, or %NULL
   void get_column_width(AT0, AT1)(/*out*/ AT0 /*float*/ min_width, /*out*/ AT1 /*float*/ max_width) nothrow {
      clutter_flow_layout_get_column_width(&this, UpCast!(float*)(min_width), UpCast!(float*)(max_width));
   }

   // VERSION: 1.2
   // Retrieves whether the @layout is homogeneous
   // RETURNS: %TRUE if the #ClutterFlowLayout is homogeneous
   int get_homogeneous()() nothrow {
      return clutter_flow_layout_get_homogeneous(&this);
   }

   // VERSION: 1.2
   // Retrieves the orientation of the @layout
   // RETURNS: the orientation of the #ClutterFlowLayout
   FlowOrientation get_orientation()() nothrow {
      return clutter_flow_layout_get_orientation(&this);
   }

   // VERSION: 1.2
   // Retrieves the minimum and maximum row heights
   // <min_height>: return location for the minimum row height, or %NULL
   // <max_height>: return location for the maximum row height, or %NULL
   void get_row_height(AT0, AT1)(/*out*/ AT0 /*float*/ min_height, /*out*/ AT1 /*float*/ max_height) nothrow {
      clutter_flow_layout_get_row_height(&this, UpCast!(float*)(min_height), UpCast!(float*)(max_height));
   }

   // VERSION: 1.2
   // Retrieves the spacing between rows
   // 
   // in pixels
   // RETURNS: the spacing between rows of the #ClutterFlowLayout,
   float get_row_spacing()() nothrow {
      return clutter_flow_layout_get_row_spacing(&this);
   }

   // VERSION: 1.2
   // Sets the space between columns, in pixels
   // <spacing>: the space between columns
   void set_column_spacing()(float spacing) nothrow {
      clutter_flow_layout_set_column_spacing(&this, spacing);
   }

   // VERSION: 1.2
   // Sets the minimum and maximum widths that a column can have
   // <min_width>: minimum width of a column
   // <max_width>: maximum width of a column
   void set_column_width()(float min_width, float max_width) nothrow {
      clutter_flow_layout_set_column_width(&this, min_width, max_width);
   }

   // VERSION: 1.2
   // Sets whether the @layout should allocate the same space for
   // each child
   // <homogeneous>: whether the layout should be homogeneous or not
   void set_homogeneous()(int homogeneous) nothrow {
      clutter_flow_layout_set_homogeneous(&this, homogeneous);
   }

   // VERSION: 1.2
   // Sets the orientation of the flow layout
   // 
   // The orientation controls the direction used to allocate
   // the children: either horizontally or vertically. The
   // orientation also controls the direction of the overflowing
   // <orientation>: the orientation of the layout
   void set_orientation()(FlowOrientation orientation) nothrow {
      clutter_flow_layout_set_orientation(&this, orientation);
   }

   // VERSION: 1.2
   // Sets the minimum and maximum heights that a row can have
   // <min_height>: the minimum height of a row
   // <max_height>: the maximum height of a row
   void set_row_height()(float min_height, float max_height) nothrow {
      clutter_flow_layout_set_row_height(&this, min_height, max_height);
   }

   // VERSION: 1.2
   // Sets the spacing between rows, in pixels
   // <spacing>: the space between rows
   void set_row_spacing()(float spacing) nothrow {
      clutter_flow_layout_set_row_spacing(&this, spacing);
   }
}


// The #ClutterFlowLayoutClass structure contains only private data
// and should be accessed using the provided API
struct FlowLayoutClass /* Version 1.2 */ {
   private LayoutManagerClass parent_class;
}

struct FlowLayoutPrivate {
}


// The direction of the arrangement of the children inside
// a #ClutterFlowLayout
enum FlowOrientation /* Version 1.2 */ {
   HORIZONTAL = 0,
   VERTICAL = 1
}
// Fog settings used to create the depth cueing effect.
struct Fog /* Version 0.6 */ {
   float z_near, z_far;
}


// Runtime flags to change the font quality. To be used with
// clutter_set_font_flags().
enum FontFlags /* Version 1.0 */ {
   MIPMAPPING = 1,
   HINTING = 2
}
enum int Forward = 269025063;
enum int FrameBack = 269025181;
enum int FrameForward = 269025182;
enum int G = 71;
enum int Gabovedot = 725;
enum int Game = 269025118;
enum int Gbreve = 683;
enum int Gcaron = 16777702;
enum int Gcedilla = 939;
enum int Gcircumflex = 728;
// The rectangle containing an actor's bounding box, measured in pixels.
struct Geometry {
   int x, y;
   uint width, height;


   // VERSION: 1.4
   // Determines if @geometry0 and geometry1 intersect returning %TRUE if
   // they do else %FALSE.
   // 
   // %FALSE.
   // RETURNS: %TRUE of @geometry0 and geometry1 intersect else
   // <geometry1>: The second geometry to test
   int intersects(AT0)(AT0 /*Geometry*/ geometry1) nothrow {
      return clutter_geometry_intersects(&this, UpCast!(Geometry*)(geometry1));
   }

   // VERSION: 1.4
   // Find the union of two rectangles represented as #ClutterGeometry.
   // <geometry_b>: another #ClutterGeometry
   // <result>: location to store the result
   void union_(AT0, AT1)(AT0 /*Geometry*/ geometry_b, /*out*/ AT1 /*Geometry*/ result) nothrow {
      clutter_geometry_union(&this, UpCast!(Geometry*)(geometry_b), UpCast!(Geometry*)(result));
   }
}

enum int Georgian_an = 16781520;
enum int Georgian_ban = 16781521;
enum int Georgian_can = 16781546;
enum int Georgian_char = 16781549;
enum int Georgian_chin = 16781545;
enum int Georgian_cil = 16781548;
enum int Georgian_don = 16781523;
enum int Georgian_en = 16781524;
enum int Georgian_fi = 16781558;
enum int Georgian_gan = 16781522;
enum int Georgian_ghan = 16781542;
enum int Georgian_hae = 16781552;
enum int Georgian_har = 16781556;
enum int Georgian_he = 16781553;
enum int Georgian_hie = 16781554;
enum int Georgian_hoe = 16781557;
enum int Georgian_in = 16781528;
enum int Georgian_jhan = 16781551;
enum int Georgian_jil = 16781547;
enum int Georgian_kan = 16781529;
enum int Georgian_khar = 16781541;
enum int Georgian_las = 16781530;
enum int Georgian_man = 16781531;
enum int Georgian_nar = 16781532;
enum int Georgian_on = 16781533;
enum int Georgian_par = 16781534;
enum int Georgian_phar = 16781540;
enum int Georgian_qar = 16781543;
enum int Georgian_rae = 16781536;
enum int Georgian_san = 16781537;
enum int Georgian_shin = 16781544;
enum int Georgian_tan = 16781527;
enum int Georgian_tar = 16781538;
enum int Georgian_un = 16781539;
enum int Georgian_vin = 16781525;
enum int Georgian_we = 16781555;
enum int Georgian_xan = 16781550;
enum int Georgian_zen = 16781526;
enum int Georgian_zhar = 16781535;

// The <structname>ClutterGestureAction</structname> structure contains
// only private data and should be accessed using the provided API
struct GestureAction /* : Action */ /* Version 1.8 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance action;
   Action parent_instance;
   private GestureActionPrivate* priv;


   // VERSION: 1.8
   // Creates a new #ClutterGestureAction instance.
   // RETURNS: the newly created #ClutterGestureAction
   static GestureAction* new_()() nothrow {
      return clutter_gesture_action_new();
   }
   static auto opCall()() {
      return clutter_gesture_action_new();
   }

   // VERSION: 1.8
   // Retrieves the coordinates, in stage space, of the latest motion
   // event during the dragging
   // <device>: currently unused, set to 0
   // <motion_x>: return location for the latest motion event's X coordinate
   // <motion_y>: return location for the latest motion event's Y coordinate
   void get_motion_coords(AT0, AT1)(uint device, /*out*/ AT0 /*float*/ motion_x, /*out*/ AT1 /*float*/ motion_y) nothrow {
      clutter_gesture_action_get_motion_coords(&this, device, UpCast!(float*)(motion_x), UpCast!(float*)(motion_y));
   }

   // VERSION: 1.8
   // Retrieves the coordinates, in stage space, of the press event
   // that started the dragging for an specific pointer device
   // <device>: currently unused, set to 0
   // <press_x>: return location for the press event's X coordinate
   // <press_y>: return location for the press event's Y coordinate
   void get_press_coords(AT0, AT1)(uint device, /*out*/ AT0 /*float*/ press_x, /*out*/ AT1 /*float*/ press_y) nothrow {
      clutter_gesture_action_get_press_coords(&this, device, UpCast!(float*)(press_x), UpCast!(float*)(press_y));
   }

   // VERSION: 1.8
   // Retrieves the coordinates, in stage space, of the point where the pointer
   // device was last released.
   // <device>: currently unused, set to 0
   // <release_x>: return location for the X coordinate of the last release
   // <release_y>: return location for the Y coordinate of the last release
   void get_release_coords(AT0, AT1)(uint device, /*out*/ AT0 /*float*/ release_x, /*out*/ AT1 /*float*/ release_y) nothrow {
      clutter_gesture_action_get_release_coords(&this, device, UpCast!(float*)(release_x), UpCast!(float*)(release_y));
   }

   // VERSION: 1.8
   // The ::gesture_begin signal is emitted when the #ClutterActor to which
   // a #ClutterGestureAction has been applied starts receiving a gesture.
   // 
   // the gesture should be ignored.
   // RETURNS: %TRUE if the gesture should start, and %FALSE if
   // <actor>: the #ClutterActor attached to the @action
   extern (C) alias static c_int function (GestureAction* this_, Actor* actor, void* user_data=null) nothrow signal_gesture_begin;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"gesture-begin", CB/*:signal_gesture_begin*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_gesture_begin)||_ttmm!(CB, signal_gesture_begin)()) {
      return signal_connect_data!()(&this, cast(char*)"gesture-begin",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.8
   // The ::gesture-cancel signal is emitted when the ongoing gesture gets
   // cancelled from the #ClutterGestureAction::gesture-progress signal handler.
   // 
   // This signal is emitted if and only if the #ClutterGestureAction::gesture-begin
   // signal has been emitted first.
   // <actor>: the #ClutterActor attached to the @action
   extern (C) alias static void function (GestureAction* this_, Actor* actor, void* user_data=null) nothrow signal_gesture_cancel;
   ulong signal_connect(string name:"gesture-cancel", CB/*:signal_gesture_cancel*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_gesture_cancel)||_ttmm!(CB, signal_gesture_cancel)()) {
      return signal_connect_data!()(&this, cast(char*)"gesture-cancel",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.8
   // The ::gesture-end signal is emitted at the end of the gesture gesture,
   // when the pointer's button is released
   // 
   // This signal is emitted if and only if the #ClutterGestureAction::gesture-begin
   // signal has been emitted first.
   // <actor>: the #ClutterActor attached to the @action
   extern (C) alias static void function (GestureAction* this_, Actor* actor, void* user_data=null) nothrow signal_gesture_end;
   ulong signal_connect(string name:"gesture-end", CB/*:signal_gesture_end*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_gesture_end)||_ttmm!(CB, signal_gesture_end)()) {
      return signal_connect_data!()(&this, cast(char*)"gesture-end",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.8
   // The ::gesture-progress signal is emitted for each motion event after
   // the #ClutterGestureAction::gesture-begin signal has been emitted.
   // 
   // the gesture should be cancelled.
   // RETURNS: %TRUE if the gesture should continue, and %FALSE if
   // <actor>: the #ClutterActor attached to the @action
   extern (C) alias static c_int function (GestureAction* this_, Actor* actor, void* user_data=null) nothrow signal_gesture_progress;
   ulong signal_connect(string name:"gesture-progress", CB/*:signal_gesture_progress*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_gesture_progress)||_ttmm!(CB, signal_gesture_progress)()) {
      return signal_connect_data!()(&this, cast(char*)"gesture-progress",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}


// The <structname>ClutterGestureClass</structname> structure contains only
// private data.
struct GestureActionClass /* Version 1.8 */ {
   private ActionClass parent_class;
   extern (C) int function (GestureAction* action, Actor* actor) nothrow gesture_begin;
   extern (C) int function (GestureAction* action, Actor* actor) nothrow gesture_progress;
   extern (C) void function (GestureAction* action, Actor* actor) nothrow gesture_end;
   extern (C) void function (GestureAction* action, Actor* actor) nothrow gesture_cancel;
   extern (C) void function () nothrow _clutter_gesture_action1;
   extern (C) void function () nothrow _clutter_gesture_action2;
   extern (C) void function () nothrow _clutter_gesture_action3;
   extern (C) void function () nothrow _clutter_gesture_action4;
   extern (C) void function () nothrow _clutter_gesture_action5;
   extern (C) void function () nothrow _clutter_gesture_action6;
   extern (C) void function () nothrow _clutter_gesture_action7;
}

struct GestureActionPrivate {
}

enum int Go = 269025119;

// Gravity of the scaling operations. When a gravity different than
// %CLUTTER_GRAVITY_NONE is used, an actor is scaled keeping the position
// of the specified portion at the same coordinates.
enum Gravity /* Version 0.2 */ {
   NONE = 0,
   NORTH = 1,
   NORTH_EAST = 2,
   EAST = 3,
   SOUTH_EAST = 4,
   SOUTH = 5,
   SOUTH_WEST = 6,
   WEST = 7,
   NORTH_WEST = 8,
   CENTER = 9
}
enum int Greek_ALPHA = 1985;
enum int Greek_ALPHAaccent = 1953;
enum int Greek_BETA = 1986;
enum int Greek_CHI = 2007;
enum int Greek_DELTA = 1988;
enum int Greek_EPSILON = 1989;
enum int Greek_EPSILONaccent = 1954;
enum int Greek_ETA = 1991;
enum int Greek_ETAaccent = 1955;
enum int Greek_GAMMA = 1987;
enum int Greek_IOTA = 1993;
enum int Greek_IOTAaccent = 1956;
enum int Greek_IOTAdiaeresis = 1957;
enum int Greek_IOTAdieresis = 1957;
enum int Greek_KAPPA = 1994;
enum int Greek_LAMBDA = 1995;
enum int Greek_LAMDA = 1995;
enum int Greek_MU = 1996;
enum int Greek_NU = 1997;
enum int Greek_OMEGA = 2009;
enum int Greek_OMEGAaccent = 1963;
enum int Greek_OMICRON = 1999;
enum int Greek_OMICRONaccent = 1959;
enum int Greek_PHI = 2006;
enum int Greek_PI = 2000;
enum int Greek_PSI = 2008;
enum int Greek_RHO = 2001;
enum int Greek_SIGMA = 2002;
enum int Greek_TAU = 2004;
enum int Greek_THETA = 1992;
enum int Greek_UPSILON = 2005;
enum int Greek_UPSILONaccent = 1960;
enum int Greek_UPSILONdieresis = 1961;
enum int Greek_XI = 1998;
enum int Greek_ZETA = 1990;
enum int Greek_accentdieresis = 1966;
enum int Greek_alpha = 2017;
enum int Greek_alphaaccent = 1969;
enum int Greek_beta = 2018;
enum int Greek_chi = 2039;
enum int Greek_delta = 2020;
enum int Greek_epsilon = 2021;
enum int Greek_epsilonaccent = 1970;
enum int Greek_eta = 2023;
enum int Greek_etaaccent = 1971;
enum int Greek_finalsmallsigma = 2035;
enum int Greek_gamma = 2019;
enum int Greek_horizbar = 1967;
enum int Greek_iota = 2025;
enum int Greek_iotaaccent = 1972;
enum int Greek_iotaaccentdieresis = 1974;
enum int Greek_iotadieresis = 1973;
enum int Greek_kappa = 2026;
enum int Greek_lambda = 2027;
enum int Greek_lamda = 2027;
enum int Greek_mu = 2028;
enum int Greek_nu = 2029;
enum int Greek_omega = 2041;
enum int Greek_omegaaccent = 1979;
enum int Greek_omicron = 2031;
enum int Greek_omicronaccent = 1975;
enum int Greek_phi = 2038;
enum int Greek_pi = 2032;
enum int Greek_psi = 2040;
enum int Greek_rho = 2033;
enum int Greek_sigma = 2034;
enum int Greek_switch = 65406;
enum int Greek_tau = 2036;
enum int Greek_theta = 2024;
enum int Greek_upsilon = 2037;
enum int Greek_upsilonaccent = 1976;
enum int Greek_upsilonaccentdieresis = 1978;
enum int Greek_upsilondieresis = 1977;
enum int Greek_xi = 2030;
enum int Greek_zeta = 2022;
enum int Green = 269025188;

// The #ClutterGroup structure contains only private data
// and should be accessed using the provided API
struct Group /* : Actor */ /* Version 0.1 */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Animatable.__interface__;
   mixin Container.__interface__;
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance actor;
   Actor parent_instance;
   private GroupPrivate* priv;


   // DEPRECATED (v1.10) constructor: new - Use clutter_actor_new() instead.
   // Create a new  #ClutterGroup.
   // RETURNS: the newly created #ClutterGroup actor
   static Group* new_()() nothrow {
      return clutter_group_new();
   }
   static auto opCall()() {
      return clutter_group_new();
   }

   // VERSION: 0.2
   // DEPRECATED (v1.10) method: get_n_children - Use clutter_actor_get_n_children() instead.
   // Gets the number of actors held in the group.
   // RETURNS: The number of child actors held in the group.
   int get_n_children()() nothrow {
      return clutter_group_get_n_children(&this);
   }

   // VERSION: 0.2
   // DEPRECATED (v1.10) method: get_nth_child - Use clutter_actor_get_child_at_index() instead.
   // Gets a groups child held at @index_ in stack.
   // 
   // @index_ is invalid.
   // RETURNS: A Clutter actor, or %NULL if
   // <index_>: the position of the requested actor.
   Actor* get_nth_child()(int index_) nothrow {
      return clutter_group_get_nth_child(&this, index_);
   }

   // DEPRECATED (v1.10) method: remove_all - Use clutter_actor_remove_all_children() instead.
   // Removes all children actors from the #ClutterGroup.
   void remove_all()() nothrow {
      clutter_group_remove_all(&this);
   }
}

// The #ClutterGroupClass structure contains only private data
struct GroupClass /* Version 0.1 */ {
   private ActorClass parent_class;
   extern (C) void function () nothrow _clutter_reserved1;
   extern (C) void function () nothrow _clutter_reserved2;
   extern (C) void function () nothrow _clutter_reserved3;
   extern (C) void function () nothrow _clutter_reserved4;
   extern (C) void function () nothrow _clutter_reserved5;
   extern (C) void function () nothrow _clutter_reserved6;
}

struct GroupPrivate {
}

enum int H = 72;
enum int Hangul = 65329;
enum int Hangul_A = 3775;
enum int Hangul_AE = 3776;
enum int Hangul_AraeA = 3830;
enum int Hangul_AraeAE = 3831;
enum int Hangul_Banja = 65337;
enum int Hangul_Cieuc = 3770;
enum int Hangul_Codeinput = 65335;
enum int Hangul_Dikeud = 3751;
enum int Hangul_E = 3780;
enum int Hangul_EO = 3779;
enum int Hangul_EU = 3793;
enum int Hangul_End = 65331;
enum int Hangul_Hanja = 65332;
enum int Hangul_Hieuh = 3774;
enum int Hangul_I = 3795;
enum int Hangul_Ieung = 3767;
enum int Hangul_J_Cieuc = 3818;
enum int Hangul_J_Dikeud = 3802;
enum int Hangul_J_Hieuh = 3822;
enum int Hangul_J_Ieung = 3816;
enum int Hangul_J_Jieuj = 3817;
enum int Hangul_J_Khieuq = 3819;
enum int Hangul_J_Kiyeog = 3796;
enum int Hangul_J_KiyeogSios = 3798;
enum int Hangul_J_KkogjiDalrinIeung = 3833;
enum int Hangul_J_Mieum = 3811;
enum int Hangul_J_Nieun = 3799;
enum int Hangul_J_NieunHieuh = 3801;
enum int Hangul_J_NieunJieuj = 3800;
enum int Hangul_J_PanSios = 3832;
enum int Hangul_J_Phieuf = 3821;
enum int Hangul_J_Pieub = 3812;
enum int Hangul_J_PieubSios = 3813;
enum int Hangul_J_Rieul = 3803;
enum int Hangul_J_RieulHieuh = 3810;
enum int Hangul_J_RieulKiyeog = 3804;
enum int Hangul_J_RieulMieum = 3805;
enum int Hangul_J_RieulPhieuf = 3809;
enum int Hangul_J_RieulPieub = 3806;
enum int Hangul_J_RieulSios = 3807;
enum int Hangul_J_RieulTieut = 3808;
enum int Hangul_J_Sios = 3814;
enum int Hangul_J_SsangKiyeog = 3797;
enum int Hangul_J_SsangSios = 3815;
enum int Hangul_J_Tieut = 3820;
enum int Hangul_J_YeorinHieuh = 3834;
enum int Hangul_Jamo = 65333;
enum int Hangul_Jeonja = 65336;
enum int Hangul_Jieuj = 3768;
enum int Hangul_Khieuq = 3771;
enum int Hangul_Kiyeog = 3745;
enum int Hangul_KiyeogSios = 3747;
enum int Hangul_KkogjiDalrinIeung = 3827;
enum int Hangul_Mieum = 3761;
enum int Hangul_MultipleCandidate = 65341;
enum int Hangul_Nieun = 3748;
enum int Hangul_NieunHieuh = 3750;
enum int Hangul_NieunJieuj = 3749;
enum int Hangul_O = 3783;
enum int Hangul_OE = 3786;
enum int Hangul_PanSios = 3826;
enum int Hangul_Phieuf = 3773;
enum int Hangul_Pieub = 3762;
enum int Hangul_PieubSios = 3764;
enum int Hangul_PostHanja = 65339;
enum int Hangul_PreHanja = 65338;
enum int Hangul_PreviousCandidate = 65342;
enum int Hangul_Rieul = 3753;
enum int Hangul_RieulHieuh = 3760;
enum int Hangul_RieulKiyeog = 3754;
enum int Hangul_RieulMieum = 3755;
enum int Hangul_RieulPhieuf = 3759;
enum int Hangul_RieulPieub = 3756;
enum int Hangul_RieulSios = 3757;
enum int Hangul_RieulTieut = 3758;
enum int Hangul_RieulYeorinHieuh = 3823;
enum int Hangul_Romaja = 65334;
enum int Hangul_SingleCandidate = 65340;
enum int Hangul_Sios = 3765;
enum int Hangul_Special = 65343;
enum int Hangul_SsangDikeud = 3752;
enum int Hangul_SsangJieuj = 3769;
enum int Hangul_SsangKiyeog = 3746;
enum int Hangul_SsangPieub = 3763;
enum int Hangul_SsangSios = 3766;
enum int Hangul_Start = 65330;
enum int Hangul_SunkyeongeumMieum = 3824;
enum int Hangul_SunkyeongeumPhieuf = 3828;
enum int Hangul_SunkyeongeumPieub = 3825;
enum int Hangul_Tieut = 3772;
enum int Hangul_U = 3788;
enum int Hangul_WA = 3784;
enum int Hangul_WAE = 3785;
enum int Hangul_WE = 3790;
enum int Hangul_WEO = 3789;
enum int Hangul_WI = 3791;
enum int Hangul_YA = 3777;
enum int Hangul_YAE = 3778;
enum int Hangul_YE = 3782;
enum int Hangul_YEO = 3781;
enum int Hangul_YI = 3794;
enum int Hangul_YO = 3787;
enum int Hangul_YU = 3792;
enum int Hangul_YeorinHieuh = 3829;
enum int Hangul_switch = 65406;
enum int Hankaku = 65321;
enum int Hcircumflex = 678;
enum int Hebrew_switch = 65406;
enum int Help = 65386;
enum int Henkan = 65315;
enum int Henkan_Mode = 65315;
enum int Hibernate = 269025192;
enum int Hiragana = 65317;
enum int Hiragana_Katakana = 65319;
enum int History = 269025079;
enum int Home = 65360;
enum int HomePage = 269025048;
enum int HotLinks = 269025082;
enum int Hstroke = 673;
enum int Hyper_L = 65517;
enum int Hyper_R = 65518;
enum int I = 73;
enum INPUT_NULL = "null";
enum INPUT_X11 = "x11";
enum int ISO_Center_Object = 65075;
enum int ISO_Continuous_Underline = 65072;
enum int ISO_Discontinuous_Underline = 65073;
enum int ISO_Emphasize = 65074;
enum int ISO_Enter = 65076;
enum int ISO_Fast_Cursor_Down = 65071;
enum int ISO_Fast_Cursor_Left = 65068;
enum int ISO_Fast_Cursor_Right = 65069;
enum int ISO_Fast_Cursor_Up = 65070;
enum int ISO_First_Group = 65036;
enum int ISO_First_Group_Lock = 65037;
enum int ISO_Group_Latch = 65030;
enum int ISO_Group_Lock = 65031;
enum int ISO_Group_Shift = 65406;
enum int ISO_Last_Group = 65038;
enum int ISO_Last_Group_Lock = 65039;
enum int ISO_Left_Tab = 65056;
enum int ISO_Level2_Latch = 65026;
enum int ISO_Level3_Latch = 65028;
enum int ISO_Level3_Lock = 65029;
enum int ISO_Level3_Shift = 65027;
enum int ISO_Level5_Latch = 65042;
enum int ISO_Level5_Lock = 65043;
enum int ISO_Level5_Shift = 65041;
enum int ISO_Lock = 65025;
enum int ISO_Move_Line_Down = 65058;
enum int ISO_Move_Line_Up = 65057;
enum int ISO_Next_Group = 65032;
enum int ISO_Next_Group_Lock = 65033;
enum int ISO_Partial_Line_Down = 65060;
enum int ISO_Partial_Line_Up = 65059;
enum int ISO_Partial_Space_Left = 65061;
enum int ISO_Partial_Space_Right = 65062;
enum int ISO_Prev_Group = 65034;
enum int ISO_Prev_Group_Lock = 65035;
enum int ISO_Release_Both_Margins = 65067;
enum int ISO_Release_Margin_Left = 65065;
enum int ISO_Release_Margin_Right = 65066;
enum int ISO_Set_Margin_Left = 65063;
enum int ISO_Set_Margin_Right = 65064;
enum int Iabovedot = 681;
enum int Iacute = 205;
enum int Ibelowdot = 16785098;
enum int Ibreve = 16777516;
enum int Icircumflex = 206;
enum int Idiaeresis = 207;
enum int Igrave = 204;
enum int Ihook = 16785096;
enum int Imacron = 975;
// Error conditions returned by clutter_init() and clutter_init_with_args().
enum InitError /* Version 0.2 */ {
   SUCCESS = 1,
   ERROR_UNKNOWN = 0,
   ERROR_THREADS = -1,
   ERROR_BACKEND = -2,
   ERROR_INTERNAL = -3
}
// The type of axes Clutter recognizes on a #ClutterInputDevice
enum InputAxis /* Version 1.6 */ {
   IGNORE = 0,
   X = 1,
   Y = 2,
   PRESSURE = 3,
   XTILT = 4,
   YTILT = 5,
   WHEEL = 6
}

// Generic representation of an input device. The actual contents of this
// structure depend on the backend used.
struct InputDevice /* : GObject.Object */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // VERSION: 1.6
   // Retrieves a pointer to the #ClutterInputDevice that has been
   // associated to @device.
   // 
   // If the #ClutterInputDevice:device-mode property of @device is
   // set to %CLUTTER_INPUT_MODE_MASTER, this function will return
   // %NULL.
   // RETURNS: a #ClutterInputDevice, or %NULL
   InputDevice* get_associated_device()() nothrow {
      return clutter_input_device_get_associated_device(&this);
   }

   // VERSION: 1.6
   // Retrieves the type of axis on @device at the given index.
   // RETURNS: the axis type
   // <index_>: the index of the axis
   InputAxis get_axis()(uint index_) nothrow {
      return clutter_input_device_get_axis(&this, index_);
   }

   // VERSION: 1.6
   // Extracts the value of the given @axis of a #ClutterInputDevice from
   // an array of axis values.
   // 
   // An example of typical usage for this function is:
   // 
   // |[
   // ClutterInputDevice *device = clutter_event_get_device (event);
   // gdouble *axes = clutter_event_get_axes (event, NULL);
   // gdouble pressure_value = 0;
   // 
   // clutter_input_device_get_axis_value (device, axes,
   // CLUTTER_INPUT_AXIS_PRESSURE,
   // &amp;pressure_value);
   // ]|
   // RETURNS: %TRUE if the value was set, and %FALSE otherwise
   // <axes>: an array of axes values, typically coming from clutter_event_get_axes()
   // <axis>: the axis to extract
   // <value>: return location for the axis value
   int get_axis_value(AT0, AT1)(AT0 /*double*/ axes, InputAxis axis, /*out*/ AT1 /*double*/ value) nothrow {
      return clutter_input_device_get_axis_value(&this, UpCast!(double*)(axes), axis, UpCast!(double*)(value));
   }

   // VERSION: 1.2
   // Retrieves the latest coordinates of the pointer of @device
   // <x>: return location for the X coordinate
   // <y>: return location for the Y coordinate
   void get_device_coords()(/*out*/ int* x, /*out*/ int* y) nothrow {
      clutter_input_device_get_device_coords(&this, x, y);
   }

   // VERSION: 1.0
   // Retrieves the unique identifier of @device
   // RETURNS: the identifier of the device
   int get_device_id()() nothrow {
      return clutter_input_device_get_device_id(&this);
   }

   // VERSION: 1.6
   // Retrieves the #ClutterInputMode of @device.
   // RETURNS: the device mode
   InputMode get_device_mode()() nothrow {
      return clutter_input_device_get_device_mode(&this);
   }

   // VERSION: 1.2
   // Retrieves the name of the @device
   // 
   // is owned by the #ClutterInputDevice and should never be modified
   // or freed
   // RETURNS: the name of the device, or %NULL. The returned string
   char* get_device_name()() nothrow {
      return clutter_input_device_get_device_name(&this);
   }

   // VERSION: 1.0
   // Retrieves the type of @device
   // RETURNS: the type of the device
   InputDeviceType get_device_type()() nothrow {
      return clutter_input_device_get_device_type(&this);
   }

   // VERSION: 1.6
   // Retrieves whether @device is enabled.
   // RETURNS: %TRUE if the device is enabled
   int get_enabled()() nothrow {
      return clutter_input_device_get_enabled(&this);
   }

   // VERSION: 1.10
   // Retrieves a pointer to the #ClutterActor currently grabbing all
   // the events coming from @device.
   // RETURNS: a #ClutterActor, or %NULL
   Actor* get_grabbed_actor()() nothrow {
      return clutter_input_device_get_grabbed_actor(&this);
   }

   // VERSION: 1.6
   // Retrieves whether @device has a pointer that follows the
   // device motion.
   // RETURNS: %TRUE if the device has a cursor
   int get_has_cursor()() nothrow {
      return clutter_input_device_get_has_cursor(&this);
   }

   // VERSION: 1.6
   // Retrieves the key set using clutter_input_device_set_key()
   // RETURNS: %TRUE if a key was set at the given index
   // <index_>: the index of the key
   // <keyval>: return location for the keyval at @index_
   // <modifiers>: return location for the modifiers at @index_
   int get_key(AT0, AT1)(uint index_, /*out*/ AT0 /*uint*/ keyval, /*out*/ AT1 /*ModifierType*/ modifiers) nothrow {
      return clutter_input_device_get_key(&this, index_, UpCast!(uint*)(keyval), UpCast!(ModifierType*)(modifiers));
   }

   // VERSION: 1.6
   // Retrieves the number of axes available on @device.
   // RETURNS: the number of axes on the device
   uint get_n_axes()() nothrow {
      return clutter_input_device_get_n_axes(&this);
   }

   // VERSION: 1.6
   // Retrieves the number of keys registered for @device.
   // RETURNS: the number of registered keys
   uint get_n_keys()() nothrow {
      return clutter_input_device_get_n_keys(&this);
   }

   // VERSION: 1.2
   // Retrieves the #ClutterActor underneath the pointer of @device
   // RETURNS: a pointer to the #ClutterActor or %NULL
   Actor* get_pointer_actor()() nothrow {
      return clutter_input_device_get_pointer_actor(&this);
   }

   // VERSION: 1.2
   // Retrieves the #ClutterStage underneath the pointer of @device
   // RETURNS: a pointer to the #ClutterStage or %NULL
   Stage* get_pointer_stage()() nothrow {
      return clutter_input_device_get_pointer_stage(&this);
   }

   // VERSION: 1.6
   // Retrieves the slave devices attached to @device.
   // 
   // list of #ClutterInputDevice, or %NULL. The contents of the list are
   // owned by the device. Use g_list_free() when done
   // RETURNS: a
   GLib2.List* /*new container*/ get_slave_devices()() nothrow {
      return clutter_input_device_get_slave_devices(&this);
   }

   // VERSION: 1.10
   // Acquires a grab on @actor for the given @device.
   // 
   // Any event coming from @device will be delivered to @actor, bypassing
   // the usual event delivery mechanism, until the grab is released by
   // calling clutter_input_device_ungrab().
   // 
   // The grab is client-side: even if the windowing system used by the Clutter
   // backend has the concept of "device grabs", Clutter will not use them.
   // 
   // Only #ClutterInputDevice of types %CLUTTER_POINTER_DEVICE and
   // %CLUTTER_KEYBOARD_DEVICE can hold a grab.
   // <actor>: a #ClutterActor
   void grab(AT0)(AT0 /*Actor*/ actor) nothrow {
      clutter_input_device_grab(&this, UpCast!(Actor*)(actor));
   }

   // VERSION: 1.6
   // Enables or disables a #ClutterInputDevice.
   // 
   // Only devices with a #ClutterInputDevice:device-mode property set
   // to %CLUTTER_INPUT_MODE_SLAVE or %CLUTTER_INPUT_MODE_FLOATING can
   // be disabled.
   // <enabled>: %TRUE to enable the @device
   void set_enabled()(int enabled) nothrow {
      clutter_input_device_set_enabled(&this, enabled);
   }

   // VERSION: 1.6
   // Sets the keyval and modifiers at the given @index_ for @device.
   // 
   // Clutter will use the keyval and modifiers set when filling out
   // an event coming from the same input device.
   // <index_>: the index of the key
   // <keyval>: the keyval
   // <modifiers>: a bitmask of modifiers
   void set_key()(uint index_, uint keyval, ModifierType modifiers) nothrow {
      clutter_input_device_set_key(&this, index_, keyval, modifiers);
   }

   // VERSION: 1.10
   // Releases the grab on the @device, if one is in place.
   void ungrab()() nothrow {
      clutter_input_device_ungrab(&this);
   }

   // VERSION: 1.2
   // Forcibly updates the state of the @device using a #ClutterEvent
   // 
   // This function should never be used by applications: it is meant
   // for integration with embedding toolkits, like clutter-gtk
   // 
   // Embedding toolkits that disable the event collection inside Clutter
   // need to use this function to update the state of input devices depending
   // on a #ClutterEvent that they are going to submit to the event handling code
   // in Clutter though clutter_do_event(). Since the input devices hold the state
   // that is going to be used to fill in fields like the #ClutterButtonEvent
   // click count, or to emit synthesized events like %CLUTTER_ENTER and
   // %CLUTTER_LEAVE, it is necessary for embedding toolkits to also be
   // responsible of updating the input device state.
   // 
   // For instance, this might be the code to translate an embedding toolkit
   // native motion notification into a Clutter #ClutterMotionEvent and ask
   // Clutter to process it:
   // 
   // |[
   // ClutterEvent c_event;
   // 
   // translate_native_event_to_clutter (native_event, &amp;c_event);
   // 
   // clutter_do_event (&amp;c_event);
   // ]|
   // 
   // Before letting clutter_do_event() process the event, it is necessary to call
   // clutter_input_device_update_from_event():
   // 
   // |[
   // ClutterEvent c_event;
   // ClutterDeviceManager *manager;
   // ClutterInputDevice *device;
   // 
   // translate_native_event_to_clutter (native_event, &amp;c_event);
   // 
   // /&ast; get the device manager &ast;/
   // manager = clutter_device_manager_get_default ();
   // 
   // /&ast; use the default Core Pointer that Clutter
   // &ast; backends register by default
   // &ast;/
   // device = clutter_device_manager_get_core_device (manager, %CLUTTER_POINTER_DEVICE);
   // 
   // /&ast; update the state of the input device &ast;/
   // clutter_input_device_update_from_event (device, &amp;c_event, FALSE);
   // 
   // clutter_do_event (&amp;c_event);
   // ]|
   // 
   // The @update_stage boolean argument should be used when the input device
   // enters and leaves a #ClutterStage; it will use the #ClutterStage field
   // of the passed @event to update the stage associated to the input device.
   // <event>: a #ClutterEvent
   // <update_stage>: whether to update the #ClutterStage of the @device using the stage of the event
   void update_from_event(AT0)(AT0 /*Event*/ event, int update_stage) nothrow {
      clutter_input_device_update_from_event(&this, UpCast!(Event*)(event), update_stage);
   }
}

struct InputDeviceClass {
}


// The types of input devices available.
// 
// The #ClutterInputDeviceType enumeration can be extended at later
// date; not every platform supports every input device type.
enum InputDeviceType /* Version 1.0 */ {
   POINTER_DEVICE = 0,
   KEYBOARD_DEVICE = 1,
   EXTENSION_DEVICE = 2,
   JOYSTICK_DEVICE = 3,
   TABLET_DEVICE = 4,
   TOUCHPAD_DEVICE = 5,
   TOUCHSCREEN_DEVICE = 6,
   PEN_DEVICE = 7,
   ERASER_DEVICE = 8,
   CURSOR_DEVICE = 9,
   N_DEVICE_TYPES = 10
}
// The mode for input devices available.
enum InputMode /* Version 1.6 */ {
   MASTER = 0,
   SLAVE = 1,
   FLOATING = 2
}
enum int Insert = 65379;
// The mode of interpolation between key frames
enum Interpolation /* Version 1.2 */ {
   LINEAR = 0,
   CUBIC = 1
}

// The #ClutterInterval structure contains only private data and should
// be accessed using the provided functions.
struct Interval /* : GObject.InitiallyUnowned */ /* Version 1.0 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance initiallyunowned;
   GObject2.InitiallyUnowned parent_instance;
   private IntervalPrivate* priv;


   // Unintrospectable constructor: new() / clutter_interval_new()
   // VERSION: 1.0
   // Creates a new #ClutterInterval holding values of type @gtype.
   // 
   // This function avoids using a #GValue for the initial and final values
   // of the interval:
   // 
   // |[
   // interval = clutter_interval_new (G_TYPE_FLOAT, 0.0, 1.0);
   // interval = clutter_interval_new (G_TYPE_BOOLEAN, FALSE, TRUE);
   // interval = clutter_interval_new (G_TYPE_INT, 0, 360);
   // ]|
   // RETURNS: the newly created #ClutterInterval
   // <gtype>: the type of the values in the interval
   alias clutter_interval_new new_; // Variadic

   // VERSION: 1.0
   // Creates a new #ClutterInterval of type @gtype, between @initial
   // and @final.
   // 
   // This function is useful for language bindings.
   // RETURNS: the newly created #ClutterInterval
   // <gtype>: the type of the values in the interval
   // <initial>: a #GValue holding the initial value of the interval
   // <final>: a #GValue holding the final value of the interval
   static Interval* new_with_values(AT0, AT1)(Type gtype, AT0 /*GObject2.Value*/ initial, AT1 /*GObject2.Value*/ final_) nothrow {
      return clutter_interval_new_with_values(gtype, UpCast!(GObject2.Value*)(initial), UpCast!(GObject2.Value*)(final_));
   }
   static auto opCall(AT0, AT1)(Type gtype, AT0 /*GObject2.Value*/ initial, AT1 /*GObject2.Value*/ final_) {
      return clutter_interval_new_with_values(gtype, UpCast!(GObject2.Value*)(initial), UpCast!(GObject2.Value*)(final_));
   }

   // Unintrospectable function: register_progress_func() / clutter_interval_register_progress_func()
   // VERSION: 1.0
   // Sets the progress function for a given @value_type, like:
   // 
   // |[
   // clutter_interval_register_progress_func (MY_TYPE_FOO,
   // my_foo_progress);
   // ]|
   // 
   // Whenever a #ClutterInterval instance using the default
   // #ClutterInterval::compute_value implementation is set as an
   // interval between two #GValue of type @value_type, it will call
   // @func to establish the value depending on the given progress,
   // for instance:
   // 
   // |[
   // static gboolean
   // my_int_progress (const GValue *a,
   // const GValue *b,
   // gdouble       progress,
   // GValue       *retval)
   // {
   // gint ia = g_value_get_int (a);
   // gint ib = g_value_get_int (b);
   // gint res = factor * (ib - ia) + ia;
   // 
   // g_value_set_int (retval, res);
   // 
   // return TRUE;
   // }
   // 
   // clutter_interval_register_progress_func (G_TYPE_INT, my_int_progress);
   // ]|
   // 
   // To unset a previously set progress function of a #GType, pass %NULL
   // for @func.
   // <value_type>: a #GType
   // <func>: a #ClutterProgressFunc, or %NULL to unset a previously set progress function
   static void register_progress_func()(Type value_type, ProgressFunc func) nothrow {
      clutter_interval_register_progress_func(value_type, func);
   }

   // VERSION: 1.0
   // Creates a copy of @interval.
   // RETURNS: the newly created #ClutterInterval
   Interval* /*new*/ clone()() nothrow {
      return clutter_interval_clone(&this);
   }

   // VERSION: 1.4
   // Computes the value between the @interval boundaries given the
   // progress @factor
   // 
   // Unlike clutter_interval_compute_value(), this function will
   // return a const pointer to the computed value
   // 
   // You should use this function if you immediately pass the computed
   // value to another function that makes a copy of it, like
   // g_object_set_property()
   // 
   // or %NULL if the computation was not successfull
   // RETURNS: a pointer to the computed value,
   // <factor>: the progress factor, between 0 and 1
   GObject2.Value* compute()(double factor) nothrow {
      return clutter_interval_compute(&this, factor);
   }

   // VERSION: 1.0
   // Computes the value between the @interval boundaries given the
   // progress @factor and copies it into @value.
   // RETURNS: %TRUE if the operation was successful
   // <factor>: the progress factor, between 0 and 1
   // <value>: return location for an initialized #GValue
   int compute_value(AT0)(double factor, /*out*/ AT0 /*GObject2.Value*/ value) nothrow {
      return clutter_interval_compute_value(&this, factor, UpCast!(GObject2.Value*)(value));
   }

   // VERSION: 1.0
   // Retrieves the final value of @interval and copies
   // it into @value.
   // 
   // The passed #GValue must be initialized to the value held by
   // the #ClutterInterval.
   // <value>: a #GValue
   void get_final_value(AT0)(/*out*/ AT0 /*GObject2.Value*/ value) nothrow {
      clutter_interval_get_final_value(&this, UpCast!(GObject2.Value*)(value));
   }

   // VERSION: 1.0
   // Retrieves the initial value of @interval and copies
   // it into @value.
   // 
   // The passed #GValue must be initialized to the value held by
   // the #ClutterInterval.
   // <value>: a #GValue
   void get_initial_value(AT0)(/*out*/ AT0 /*GObject2.Value*/ value) nothrow {
      clutter_interval_get_initial_value(&this, UpCast!(GObject2.Value*)(value));
   }

   // Unintrospectable method: get_interval() / clutter_interval_get_interval()
   // VERSION: 1.0
   // Variable arguments wrapper for clutter_interval_get_initial_value()
   // and clutter_interval_get_final_value() that avoids using the
   // #GValue arguments:
   // 
   // |[
   // gint a = 0, b = 0;
   // clutter_interval_get_interval (interval, &a, &b);
   // ]|
   // 
   // This function is meant for the convenience of the C API; bindings
   // should reimplement this function using the #GValue-based API.
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_interval_get_interval get_interval; // Variadic
   +/

   // VERSION: 1.0
   // Retrieves the #GType of the values inside @interval.
   // RETURNS: the type of the value, or G_TYPE_INVALID
   Type get_value_type()() nothrow {
      return clutter_interval_get_value_type(&this);
   }

   // VERSION: 1.0
   // Gets the pointer to the final value of @interval
   // 
   // The value is owned by the #ClutterInterval and it should not be
   // modified or freed
   // RETURNS: the final value of the interval.
   GObject2.Value* peek_final_value()() nothrow {
      return clutter_interval_peek_final_value(&this);
   }

   // VERSION: 1.0
   // Gets the pointer to the initial value of @interval
   // 
   // The value is owned by the #ClutterInterval and it should not be
   // modified or freed
   // RETURNS: the initial value of the interval.
   GObject2.Value* peek_initial_value()() nothrow {
      return clutter_interval_peek_initial_value(&this);
   }

   // VERSION: 1.0
   // Sets the final value of @interval to @value. The value is
   // copied inside the #ClutterInterval.
   // <value>: a #GValue
   void set_final_value(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
      clutter_interval_set_final_value(&this, UpCast!(GObject2.Value*)(value));
   }

   // VERSION: 1.0
   // Sets the initial value of @interval to @value. The value is copied
   // inside the #ClutterInterval.
   // <value>: a #GValue
   void set_initial_value(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
      clutter_interval_set_initial_value(&this, UpCast!(GObject2.Value*)(value));
   }

   // Unintrospectable method: set_interval() / clutter_interval_set_interval()
   // VERSION: 1.0
   // Variable arguments wrapper for clutter_interval_set_initial_value()
   // and clutter_interval_set_final_value() that avoids using the
   // #GValue arguments:
   // 
   // |[
   // clutter_interval_set_interval (interval, 0, 50);
   // clutter_interval_set_interval (interval, 1.0, 0.0);
   // clutter_interval_set_interval (interval, FALSE, TRUE);
   // ]|
   // 
   // This function is meant for the convenience of the C API; bindings
   // should reimplement this function using the #GValue-based API.
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_interval_set_interval set_interval; // Variadic
   +/

   // VERSION: 1.0
   // Validates the initial and final values of @interval against
   // a #GParamSpec.
   // RETURNS: %TRUE if the #ClutterInterval is valid, %FALSE otherwise
   // <pspec>: a #GParamSpec
   int validate(AT0)(AT0 /*GObject2.ParamSpec*/ pspec) nothrow {
      return clutter_interval_validate(&this, UpCast!(GObject2.ParamSpec*)(pspec));
   }
}

// The #ClutterIntervalClass contains only private data.
struct IntervalClass /* Version 1.0 */ {
   private GObject2.InitiallyUnownedClass parent_class;

   // RETURNS: %TRUE if the #ClutterInterval is valid, %FALSE otherwise
   // <pspec>: a #GParamSpec
   extern (C) int function (Interval* interval, GObject2.ParamSpec* pspec) nothrow validate;

   // RETURNS: %TRUE if the operation was successful
   // <factor>: the progress factor, between 0 and 1
   // <value>: return location for an initialized #GValue
   extern (C) int function (Interval* interval, double factor, /*out*/ GObject2.Value* value) nothrow compute_value;
   extern (C) void function () nothrow _clutter_reserved1;
   extern (C) void function () nothrow _clutter_reserved2;
   extern (C) void function () nothrow _clutter_reserved3;
   extern (C) void function () nothrow _clutter_reserved4;
   extern (C) void function () nothrow _clutter_reserved5;
   extern (C) void function () nothrow _clutter_reserved6;
}

struct IntervalPrivate {
}

enum int Iogonek = 967;
enum int Itilde = 933;
enum int J = 74;
enum int Jcircumflex = 684;
enum int K = 75;
enum int KEY_0 = 48;
enum int KEY_1 = 49;
enum int KEY_2 = 50;
enum int KEY_3 = 51;
enum int KEY_3270_AltCursor = 64784;
enum int KEY_3270_Attn = 64782;
enum int KEY_3270_BackTab = 64773;
enum int KEY_3270_ChangeScreen = 64793;
enum int KEY_3270_Copy = 64789;
enum int KEY_3270_CursorBlink = 64783;
enum int KEY_3270_CursorSelect = 64796;
enum int KEY_3270_DeleteWord = 64794;
enum int KEY_3270_Duplicate = 64769;
enum int KEY_3270_Enter = 64798;
enum int KEY_3270_EraseEOF = 64774;
enum int KEY_3270_EraseInput = 64775;
enum int KEY_3270_ExSelect = 64795;
enum int KEY_3270_FieldMark = 64770;
enum int KEY_3270_Ident = 64787;
enum int KEY_3270_Jump = 64786;
enum int KEY_3270_KeyClick = 64785;
enum int KEY_3270_Left2 = 64772;
enum int KEY_3270_PA1 = 64778;
enum int KEY_3270_PA2 = 64779;
enum int KEY_3270_PA3 = 64780;
enum int KEY_3270_Play = 64790;
enum int KEY_3270_PrintScreen = 64797;
enum int KEY_3270_Quit = 64777;
enum int KEY_3270_Record = 64792;
enum int KEY_3270_Reset = 64776;
enum int KEY_3270_Right2 = 64771;
enum int KEY_3270_Rule = 64788;
enum int KEY_3270_Setup = 64791;
enum int KEY_3270_Test = 64781;
enum int KEY_4 = 52;
enum int KEY_5 = 53;
enum int KEY_6 = 54;
enum int KEY_7 = 55;
enum int KEY_8 = 56;
enum int KEY_9 = 57;
enum int KEY_A = 65;
enum int KEY_AE = 198;
enum int KEY_Aacute = 193;
enum int KEY_Abelowdot = 16785056;
enum int KEY_Abreve = 451;
enum int KEY_Abreveacute = 16785070;
enum int KEY_Abrevebelowdot = 16785078;
enum int KEY_Abrevegrave = 16785072;
enum int KEY_Abrevehook = 16785074;
enum int KEY_Abrevetilde = 16785076;
enum int KEY_AccessX_Enable = 65136;
enum int KEY_AccessX_Feedback_Enable = 65137;
enum int KEY_Acircumflex = 194;
enum int KEY_Acircumflexacute = 16785060;
enum int KEY_Acircumflexbelowdot = 16785068;
enum int KEY_Acircumflexgrave = 16785062;
enum int KEY_Acircumflexhook = 16785064;
enum int KEY_Acircumflextilde = 16785066;
enum int KEY_AddFavorite = 269025081;
enum int KEY_Adiaeresis = 196;
enum int KEY_Agrave = 192;
enum int KEY_Ahook = 16785058;
enum int KEY_Alt_L = 65513;
enum int KEY_Alt_R = 65514;
enum int KEY_Amacron = 960;
enum int KEY_Aogonek = 417;
enum int KEY_ApplicationLeft = 269025104;
enum int KEY_ApplicationRight = 269025105;
enum int KEY_Arabic_0 = 16778848;
enum int KEY_Arabic_1 = 16778849;
enum int KEY_Arabic_2 = 16778850;
enum int KEY_Arabic_3 = 16778851;
enum int KEY_Arabic_4 = 16778852;
enum int KEY_Arabic_5 = 16778853;
enum int KEY_Arabic_6 = 16778854;
enum int KEY_Arabic_7 = 16778855;
enum int KEY_Arabic_8 = 16778856;
enum int KEY_Arabic_9 = 16778857;
enum int KEY_Arabic_ain = 1497;
enum int KEY_Arabic_alef = 1479;
enum int KEY_Arabic_alefmaksura = 1513;
enum int KEY_Arabic_beh = 1480;
enum int KEY_Arabic_comma = 1452;
enum int KEY_Arabic_dad = 1494;
enum int KEY_Arabic_dal = 1487;
enum int KEY_Arabic_damma = 1519;
enum int KEY_Arabic_dammatan = 1516;
enum int KEY_Arabic_ddal = 16778888;
enum int KEY_Arabic_farsi_yeh = 16778956;
enum int KEY_Arabic_fatha = 1518;
enum int KEY_Arabic_fathatan = 1515;
enum int KEY_Arabic_feh = 1505;
enum int KEY_Arabic_fullstop = 16778964;
enum int KEY_Arabic_gaf = 16778927;
enum int KEY_Arabic_ghain = 1498;
enum int KEY_Arabic_ha = 1511;
enum int KEY_Arabic_hah = 1485;
enum int KEY_Arabic_hamza = 1473;
enum int KEY_Arabic_hamza_above = 16778836;
enum int KEY_Arabic_hamza_below = 16778837;
enum int KEY_Arabic_hamzaonalef = 1475;
enum int KEY_Arabic_hamzaonwaw = 1476;
enum int KEY_Arabic_hamzaonyeh = 1478;
enum int KEY_Arabic_hamzaunderalef = 1477;
enum int KEY_Arabic_heh = 1511;
enum int KEY_Arabic_heh_doachashmee = 16778942;
enum int KEY_Arabic_heh_goal = 16778945;
enum int KEY_Arabic_jeem = 1484;
enum int KEY_Arabic_jeh = 16778904;
enum int KEY_Arabic_kaf = 1507;
enum int KEY_Arabic_kasra = 1520;
enum int KEY_Arabic_kasratan = 1517;
enum int KEY_Arabic_keheh = 16778921;
enum int KEY_Arabic_khah = 1486;
enum int KEY_Arabic_lam = 1508;
enum int KEY_Arabic_madda_above = 16778835;
enum int KEY_Arabic_maddaonalef = 1474;
enum int KEY_Arabic_meem = 1509;
enum int KEY_Arabic_noon = 1510;
enum int KEY_Arabic_noon_ghunna = 16778938;
enum int KEY_Arabic_peh = 16778878;
enum int KEY_Arabic_percent = 16778858;
enum int KEY_Arabic_qaf = 1506;
enum int KEY_Arabic_question_mark = 1471;
enum int KEY_Arabic_ra = 1489;
enum int KEY_Arabic_rreh = 16778897;
enum int KEY_Arabic_sad = 1493;
enum int KEY_Arabic_seen = 1491;
enum int KEY_Arabic_semicolon = 1467;
enum int KEY_Arabic_shadda = 1521;
enum int KEY_Arabic_sheen = 1492;
enum int KEY_Arabic_sukun = 1522;
enum int KEY_Arabic_superscript_alef = 16778864;
enum int KEY_Arabic_switch = 65406;
enum int KEY_Arabic_tah = 1495;
enum int KEY_Arabic_tatweel = 1504;
enum int KEY_Arabic_tcheh = 16778886;
enum int KEY_Arabic_teh = 1482;
enum int KEY_Arabic_tehmarbuta = 1481;
enum int KEY_Arabic_thal = 1488;
enum int KEY_Arabic_theh = 1483;
enum int KEY_Arabic_tteh = 16778873;
enum int KEY_Arabic_veh = 16778916;
enum int KEY_Arabic_waw = 1512;
enum int KEY_Arabic_yeh = 1514;
enum int KEY_Arabic_yeh_baree = 16778962;
enum int KEY_Arabic_zah = 1496;
enum int KEY_Arabic_zain = 1490;
enum int KEY_Aring = 197;
enum int KEY_Armenian_AT = 16778552;
enum int KEY_Armenian_AYB = 16778545;
enum int KEY_Armenian_BEN = 16778546;
enum int KEY_Armenian_CHA = 16778569;
enum int KEY_Armenian_DA = 16778548;
enum int KEY_Armenian_DZA = 16778561;
enum int KEY_Armenian_E = 16778551;
enum int KEY_Armenian_FE = 16778582;
enum int KEY_Armenian_GHAT = 16778562;
enum int KEY_Armenian_GIM = 16778547;
enum int KEY_Armenian_HI = 16778565;
enum int KEY_Armenian_HO = 16778560;
enum int KEY_Armenian_INI = 16778555;
enum int KEY_Armenian_JE = 16778571;
enum int KEY_Armenian_KE = 16778580;
enum int KEY_Armenian_KEN = 16778559;
enum int KEY_Armenian_KHE = 16778557;
enum int KEY_Armenian_LYUN = 16778556;
enum int KEY_Armenian_MEN = 16778564;
enum int KEY_Armenian_NU = 16778566;
enum int KEY_Armenian_O = 16778581;
enum int KEY_Armenian_PE = 16778570;
enum int KEY_Armenian_PYUR = 16778579;
enum int KEY_Armenian_RA = 16778572;
enum int KEY_Armenian_RE = 16778576;
enum int KEY_Armenian_SE = 16778573;
enum int KEY_Armenian_SHA = 16778567;
enum int KEY_Armenian_TCHE = 16778563;
enum int KEY_Armenian_TO = 16778553;
enum int KEY_Armenian_TSA = 16778558;
enum int KEY_Armenian_TSO = 16778577;
enum int KEY_Armenian_TYUN = 16778575;
enum int KEY_Armenian_VEV = 16778574;
enum int KEY_Armenian_VO = 16778568;
enum int KEY_Armenian_VYUN = 16778578;
enum int KEY_Armenian_YECH = 16778549;
enum int KEY_Armenian_ZA = 16778550;
enum int KEY_Armenian_ZHE = 16778554;
enum int KEY_Armenian_accent = 16778587;
enum int KEY_Armenian_amanak = 16778588;
enum int KEY_Armenian_apostrophe = 16778586;
enum int KEY_Armenian_at = 16778600;
enum int KEY_Armenian_ayb = 16778593;
enum int KEY_Armenian_ben = 16778594;
enum int KEY_Armenian_but = 16778589;
enum int KEY_Armenian_cha = 16778617;
enum int KEY_Armenian_da = 16778596;
enum int KEY_Armenian_dza = 16778609;
enum int KEY_Armenian_e = 16778599;
enum int KEY_Armenian_exclam = 16778588;
enum int KEY_Armenian_fe = 16778630;
enum int KEY_Armenian_full_stop = 16778633;
enum int KEY_Armenian_ghat = 16778610;
enum int KEY_Armenian_gim = 16778595;
enum int KEY_Armenian_hi = 16778613;
enum int KEY_Armenian_ho = 16778608;
enum int KEY_Armenian_hyphen = 16778634;
enum int KEY_Armenian_ini = 16778603;
enum int KEY_Armenian_je = 16778619;
enum int KEY_Armenian_ke = 16778628;
enum int KEY_Armenian_ken = 16778607;
enum int KEY_Armenian_khe = 16778605;
enum int KEY_Armenian_ligature_ew = 16778631;
enum int KEY_Armenian_lyun = 16778604;
enum int KEY_Armenian_men = 16778612;
enum int KEY_Armenian_nu = 16778614;
enum int KEY_Armenian_o = 16778629;
enum int KEY_Armenian_paruyk = 16778590;
enum int KEY_Armenian_pe = 16778618;
enum int KEY_Armenian_pyur = 16778627;
enum int KEY_Armenian_question = 16778590;
enum int KEY_Armenian_ra = 16778620;
enum int KEY_Armenian_re = 16778624;
enum int KEY_Armenian_se = 16778621;
enum int KEY_Armenian_separation_mark = 16778589;
enum int KEY_Armenian_sha = 16778615;
enum int KEY_Armenian_shesht = 16778587;
enum int KEY_Armenian_tche = 16778611;
enum int KEY_Armenian_to = 16778601;
enum int KEY_Armenian_tsa = 16778606;
enum int KEY_Armenian_tso = 16778625;
enum int KEY_Armenian_tyun = 16778623;
enum int KEY_Armenian_verjaket = 16778633;
enum int KEY_Armenian_vev = 16778622;
enum int KEY_Armenian_vo = 16778616;
enum int KEY_Armenian_vyun = 16778626;
enum int KEY_Armenian_yech = 16778597;
enum int KEY_Armenian_yentamna = 16778634;
enum int KEY_Armenian_za = 16778598;
enum int KEY_Armenian_zhe = 16778602;
enum int KEY_Atilde = 195;
enum int KEY_AudibleBell_Enable = 65146;
enum int KEY_AudioCycleTrack = 269025179;
enum int KEY_AudioForward = 269025175;
enum int KEY_AudioLowerVolume = 269025041;
enum int KEY_AudioMedia = 269025074;
enum int KEY_AudioMute = 269025042;
enum int KEY_AudioNext = 269025047;
enum int KEY_AudioPause = 269025073;
enum int KEY_AudioPlay = 269025044;
enum int KEY_AudioPrev = 269025046;
enum int KEY_AudioRaiseVolume = 269025043;
enum int KEY_AudioRandomPlay = 269025177;
enum int KEY_AudioRecord = 269025052;
enum int KEY_AudioRepeat = 269025176;
enum int KEY_AudioRewind = 269025086;
enum int KEY_AudioStop = 269025045;
enum int KEY_Away = 269025165;
enum int KEY_B = 66;
enum int KEY_Babovedot = 16784898;
enum int KEY_Back = 269025062;
enum int KEY_BackForward = 269025087;
enum int KEY_BackSpace = 65288;
enum int KEY_Battery = 269025171;
enum int KEY_Begin = 65368;
enum int KEY_Blue = 269025190;
enum int KEY_Bluetooth = 269025172;
enum int KEY_Book = 269025106;
enum int KEY_BounceKeys_Enable = 65140;
enum int KEY_Break = 65387;
enum int KEY_BrightnessAdjust = 269025083;
enum int KEY_Byelorussian_SHORTU = 1726;
enum int KEY_Byelorussian_shortu = 1710;
enum int KEY_C = 67;
enum int KEY_CD = 269025107;
enum int KEY_CH = 65186;
enum int KEY_C_H = 65189;
enum int KEY_C_h = 65188;
enum int KEY_Cabovedot = 709;
enum int KEY_Cacute = 454;
enum int KEY_Calculator = 269025053;
enum int KEY_Calendar = 269025056;
enum int KEY_Cancel = 65385;
enum int KEY_Caps_Lock = 65509;
enum int KEY_Ccaron = 456;
enum int KEY_Ccedilla = 199;
enum int KEY_Ccircumflex = 710;
enum int KEY_Ch = 65185;
enum int KEY_Clear = 65291;
enum int KEY_ClearGrab = 269024801;
enum int KEY_Close = 269025110;
enum int KEY_Codeinput = 65335;
enum int KEY_ColonSign = 16785569;
enum int KEY_Community = 269025085;
enum int KEY_ContrastAdjust = 269025058;
enum int KEY_Control_L = 65507;
enum int KEY_Control_R = 65508;
enum int KEY_Copy = 269025111;
enum int KEY_CruzeiroSign = 16785570;
enum int KEY_Cut = 269025112;
enum int KEY_CycleAngle = 269025180;
enum int KEY_Cyrillic_A = 1761;
enum int KEY_Cyrillic_BE = 1762;
enum int KEY_Cyrillic_CHE = 1790;
enum int KEY_Cyrillic_CHE_descender = 16778422;
enum int KEY_Cyrillic_CHE_vertstroke = 16778424;
enum int KEY_Cyrillic_DE = 1764;
enum int KEY_Cyrillic_DZHE = 1727;
enum int KEY_Cyrillic_E = 1788;
enum int KEY_Cyrillic_EF = 1766;
enum int KEY_Cyrillic_EL = 1772;
enum int KEY_Cyrillic_EM = 1773;
enum int KEY_Cyrillic_EN = 1774;
enum int KEY_Cyrillic_EN_descender = 16778402;
enum int KEY_Cyrillic_ER = 1778;
enum int KEY_Cyrillic_ES = 1779;
enum int KEY_Cyrillic_GHE = 1767;
enum int KEY_Cyrillic_GHE_bar = 16778386;
enum int KEY_Cyrillic_HA = 1768;
enum int KEY_Cyrillic_HARDSIGN = 1791;
enum int KEY_Cyrillic_HA_descender = 16778418;
enum int KEY_Cyrillic_I = 1769;
enum int KEY_Cyrillic_IE = 1765;
enum int KEY_Cyrillic_IO = 1715;
enum int KEY_Cyrillic_I_macron = 16778466;
enum int KEY_Cyrillic_JE = 1720;
enum int KEY_Cyrillic_KA = 1771;
enum int KEY_Cyrillic_KA_descender = 16778394;
enum int KEY_Cyrillic_KA_vertstroke = 16778396;
enum int KEY_Cyrillic_LJE = 1721;
enum int KEY_Cyrillic_NJE = 1722;
enum int KEY_Cyrillic_O = 1775;
enum int KEY_Cyrillic_O_bar = 16778472;
enum int KEY_Cyrillic_PE = 1776;
enum int KEY_Cyrillic_SCHWA = 16778456;
enum int KEY_Cyrillic_SHA = 1787;
enum int KEY_Cyrillic_SHCHA = 1789;
enum int KEY_Cyrillic_SHHA = 16778426;
enum int KEY_Cyrillic_SHORTI = 1770;
enum int KEY_Cyrillic_SOFTSIGN = 1784;
enum int KEY_Cyrillic_TE = 1780;
enum int KEY_Cyrillic_TSE = 1763;
enum int KEY_Cyrillic_U = 1781;
enum int KEY_Cyrillic_U_macron = 16778478;
enum int KEY_Cyrillic_U_straight = 16778414;
enum int KEY_Cyrillic_U_straight_bar = 16778416;
enum int KEY_Cyrillic_VE = 1783;
enum int KEY_Cyrillic_YA = 1777;
enum int KEY_Cyrillic_YERU = 1785;
enum int KEY_Cyrillic_YU = 1760;
enum int KEY_Cyrillic_ZE = 1786;
enum int KEY_Cyrillic_ZHE = 1782;
enum int KEY_Cyrillic_ZHE_descender = 16778390;
enum int KEY_Cyrillic_a = 1729;
enum int KEY_Cyrillic_be = 1730;
enum int KEY_Cyrillic_che = 1758;
enum int KEY_Cyrillic_che_descender = 16778423;
enum int KEY_Cyrillic_che_vertstroke = 16778425;
enum int KEY_Cyrillic_de = 1732;
enum int KEY_Cyrillic_dzhe = 1711;
enum int KEY_Cyrillic_e = 1756;
enum int KEY_Cyrillic_ef = 1734;
enum int KEY_Cyrillic_el = 1740;
enum int KEY_Cyrillic_em = 1741;
enum int KEY_Cyrillic_en = 1742;
enum int KEY_Cyrillic_en_descender = 16778403;
enum int KEY_Cyrillic_er = 1746;
enum int KEY_Cyrillic_es = 1747;
enum int KEY_Cyrillic_ghe = 1735;
enum int KEY_Cyrillic_ghe_bar = 16778387;
enum int KEY_Cyrillic_ha = 1736;
enum int KEY_Cyrillic_ha_descender = 16778419;
enum int KEY_Cyrillic_hardsign = 1759;
enum int KEY_Cyrillic_i = 1737;
enum int KEY_Cyrillic_i_macron = 16778467;
enum int KEY_Cyrillic_ie = 1733;
enum int KEY_Cyrillic_io = 1699;
enum int KEY_Cyrillic_je = 1704;
enum int KEY_Cyrillic_ka = 1739;
enum int KEY_Cyrillic_ka_descender = 16778395;
enum int KEY_Cyrillic_ka_vertstroke = 16778397;
enum int KEY_Cyrillic_lje = 1705;
enum int KEY_Cyrillic_nje = 1706;
enum int KEY_Cyrillic_o = 1743;
enum int KEY_Cyrillic_o_bar = 16778473;
enum int KEY_Cyrillic_pe = 1744;
enum int KEY_Cyrillic_schwa = 16778457;
enum int KEY_Cyrillic_sha = 1755;
enum int KEY_Cyrillic_shcha = 1757;
enum int KEY_Cyrillic_shha = 16778427;
enum int KEY_Cyrillic_shorti = 1738;
enum int KEY_Cyrillic_softsign = 1752;
enum int KEY_Cyrillic_te = 1748;
enum int KEY_Cyrillic_tse = 1731;
enum int KEY_Cyrillic_u = 1749;
enum int KEY_Cyrillic_u_macron = 16778479;
enum int KEY_Cyrillic_u_straight = 16778415;
enum int KEY_Cyrillic_u_straight_bar = 16778417;
enum int KEY_Cyrillic_ve = 1751;
enum int KEY_Cyrillic_ya = 1745;
enum int KEY_Cyrillic_yeru = 1753;
enum int KEY_Cyrillic_yu = 1728;
enum int KEY_Cyrillic_ze = 1754;
enum int KEY_Cyrillic_zhe = 1750;
enum int KEY_Cyrillic_zhe_descender = 16778391;
enum int KEY_D = 68;
enum int KEY_DOS = 269025114;
enum int KEY_Dabovedot = 16784906;
enum int KEY_Dcaron = 463;
enum int KEY_Delete = 65535;
enum int KEY_Display = 269025113;
enum int KEY_Documents = 269025115;
enum int KEY_DongSign = 16785579;
enum int KEY_Down = 65364;
enum int KEY_Dstroke = 464;
enum int KEY_E = 69;
enum int KEY_ENG = 957;
enum int KEY_ETH = 208;
enum int KEY_Eabovedot = 972;
enum int KEY_Eacute = 201;
enum int KEY_Ebelowdot = 16785080;
enum int KEY_Ecaron = 460;
enum int KEY_Ecircumflex = 202;
enum int KEY_Ecircumflexacute = 16785086;
enum int KEY_Ecircumflexbelowdot = 16785094;
enum int KEY_Ecircumflexgrave = 16785088;
enum int KEY_Ecircumflexhook = 16785090;
enum int KEY_Ecircumflextilde = 16785092;
enum int KEY_EcuSign = 16785568;
enum int KEY_Ediaeresis = 203;
enum int KEY_Egrave = 200;
enum int KEY_Ehook = 16785082;
enum int KEY_Eisu_Shift = 65327;
enum int KEY_Eisu_toggle = 65328;
enum int KEY_Eject = 269025068;
enum int KEY_Emacron = 938;
enum int KEY_End = 65367;
enum int KEY_Eogonek = 458;
enum int KEY_Escape = 65307;
enum int KEY_Eth = 208;
enum int KEY_Etilde = 16785084;
enum int KEY_EuroSign = 8364;
enum int KEY_Excel = 269025116;
enum int KEY_Execute = 65378;
enum int KEY_Explorer = 269025117;
enum int KEY_F = 70;
enum int KEY_F1 = 65470;
enum int KEY_F10 = 65479;
enum int KEY_F11 = 65480;
enum int KEY_F12 = 65481;
enum int KEY_F13 = 65482;
enum int KEY_F14 = 65483;
enum int KEY_F15 = 65484;
enum int KEY_F16 = 65485;
enum int KEY_F17 = 65486;
enum int KEY_F18 = 65487;
enum int KEY_F19 = 65488;
enum int KEY_F2 = 65471;
enum int KEY_F20 = 65489;
enum int KEY_F21 = 65490;
enum int KEY_F22 = 65491;
enum int KEY_F23 = 65492;
enum int KEY_F24 = 65493;
enum int KEY_F25 = 65494;
enum int KEY_F26 = 65495;
enum int KEY_F27 = 65496;
enum int KEY_F28 = 65497;
enum int KEY_F29 = 65498;
enum int KEY_F3 = 65472;
enum int KEY_F30 = 65499;
enum int KEY_F31 = 65500;
enum int KEY_F32 = 65501;
enum int KEY_F33 = 65502;
enum int KEY_F34 = 65503;
enum int KEY_F35 = 65504;
enum int KEY_F4 = 65473;
enum int KEY_F5 = 65474;
enum int KEY_F6 = 65475;
enum int KEY_F7 = 65476;
enum int KEY_F8 = 65477;
enum int KEY_F9 = 65478;
enum int KEY_FFrancSign = 16785571;
enum int KEY_Fabovedot = 16784926;
enum int KEY_Farsi_0 = 16778992;
enum int KEY_Farsi_1 = 16778993;
enum int KEY_Farsi_2 = 16778994;
enum int KEY_Farsi_3 = 16778995;
enum int KEY_Farsi_4 = 16778996;
enum int KEY_Farsi_5 = 16778997;
enum int KEY_Farsi_6 = 16778998;
enum int KEY_Farsi_7 = 16778999;
enum int KEY_Farsi_8 = 16779000;
enum int KEY_Farsi_9 = 16779001;
enum int KEY_Farsi_yeh = 16778956;
enum int KEY_Favorites = 269025072;
enum int KEY_Finance = 269025084;
enum int KEY_Find = 65384;
enum int KEY_First_Virtual_Screen = 65232;
enum int KEY_Forward = 269025063;
enum int KEY_FrameBack = 269025181;
enum int KEY_FrameForward = 269025182;
enum int KEY_G = 71;
enum int KEY_Gabovedot = 725;
enum int KEY_Game = 269025118;
enum int KEY_Gbreve = 683;
enum int KEY_Gcaron = 16777702;
enum int KEY_Gcedilla = 939;
enum int KEY_Gcircumflex = 728;
enum int KEY_Georgian_an = 16781520;
enum int KEY_Georgian_ban = 16781521;
enum int KEY_Georgian_can = 16781546;
enum int KEY_Georgian_char = 16781549;
enum int KEY_Georgian_chin = 16781545;
enum int KEY_Georgian_cil = 16781548;
enum int KEY_Georgian_don = 16781523;
enum int KEY_Georgian_en = 16781524;
enum int KEY_Georgian_fi = 16781558;
enum int KEY_Georgian_gan = 16781522;
enum int KEY_Georgian_ghan = 16781542;
enum int KEY_Georgian_hae = 16781552;
enum int KEY_Georgian_har = 16781556;
enum int KEY_Georgian_he = 16781553;
enum int KEY_Georgian_hie = 16781554;
enum int KEY_Georgian_hoe = 16781557;
enum int KEY_Georgian_in = 16781528;
enum int KEY_Georgian_jhan = 16781551;
enum int KEY_Georgian_jil = 16781547;
enum int KEY_Georgian_kan = 16781529;
enum int KEY_Georgian_khar = 16781541;
enum int KEY_Georgian_las = 16781530;
enum int KEY_Georgian_man = 16781531;
enum int KEY_Georgian_nar = 16781532;
enum int KEY_Georgian_on = 16781533;
enum int KEY_Georgian_par = 16781534;
enum int KEY_Georgian_phar = 16781540;
enum int KEY_Georgian_qar = 16781543;
enum int KEY_Georgian_rae = 16781536;
enum int KEY_Georgian_san = 16781537;
enum int KEY_Georgian_shin = 16781544;
enum int KEY_Georgian_tan = 16781527;
enum int KEY_Georgian_tar = 16781538;
enum int KEY_Georgian_un = 16781539;
enum int KEY_Georgian_vin = 16781525;
enum int KEY_Georgian_we = 16781555;
enum int KEY_Georgian_xan = 16781550;
enum int KEY_Georgian_zen = 16781526;
enum int KEY_Georgian_zhar = 16781535;
enum int KEY_Go = 269025119;
enum int KEY_Greek_ALPHA = 1985;
enum int KEY_Greek_ALPHAaccent = 1953;
enum int KEY_Greek_BETA = 1986;
enum int KEY_Greek_CHI = 2007;
enum int KEY_Greek_DELTA = 1988;
enum int KEY_Greek_EPSILON = 1989;
enum int KEY_Greek_EPSILONaccent = 1954;
enum int KEY_Greek_ETA = 1991;
enum int KEY_Greek_ETAaccent = 1955;
enum int KEY_Greek_GAMMA = 1987;
enum int KEY_Greek_IOTA = 1993;
enum int KEY_Greek_IOTAaccent = 1956;
enum int KEY_Greek_IOTAdiaeresis = 1957;
enum int KEY_Greek_IOTAdieresis = 1957;
enum int KEY_Greek_KAPPA = 1994;
enum int KEY_Greek_LAMBDA = 1995;
enum int KEY_Greek_LAMDA = 1995;
enum int KEY_Greek_MU = 1996;
enum int KEY_Greek_NU = 1997;
enum int KEY_Greek_OMEGA = 2009;
enum int KEY_Greek_OMEGAaccent = 1963;
enum int KEY_Greek_OMICRON = 1999;
enum int KEY_Greek_OMICRONaccent = 1959;
enum int KEY_Greek_PHI = 2006;
enum int KEY_Greek_PI = 2000;
enum int KEY_Greek_PSI = 2008;
enum int KEY_Greek_RHO = 2001;
enum int KEY_Greek_SIGMA = 2002;
enum int KEY_Greek_TAU = 2004;
enum int KEY_Greek_THETA = 1992;
enum int KEY_Greek_UPSILON = 2005;
enum int KEY_Greek_UPSILONaccent = 1960;
enum int KEY_Greek_UPSILONdieresis = 1961;
enum int KEY_Greek_XI = 1998;
enum int KEY_Greek_ZETA = 1990;
enum int KEY_Greek_accentdieresis = 1966;
enum int KEY_Greek_alpha = 2017;
enum int KEY_Greek_alphaaccent = 1969;
enum int KEY_Greek_beta = 2018;
enum int KEY_Greek_chi = 2039;
enum int KEY_Greek_delta = 2020;
enum int KEY_Greek_epsilon = 2021;
enum int KEY_Greek_epsilonaccent = 1970;
enum int KEY_Greek_eta = 2023;
enum int KEY_Greek_etaaccent = 1971;
enum int KEY_Greek_finalsmallsigma = 2035;
enum int KEY_Greek_gamma = 2019;
enum int KEY_Greek_horizbar = 1967;
enum int KEY_Greek_iota = 2025;
enum int KEY_Greek_iotaaccent = 1972;
enum int KEY_Greek_iotaaccentdieresis = 1974;
enum int KEY_Greek_iotadieresis = 1973;
enum int KEY_Greek_kappa = 2026;
enum int KEY_Greek_lambda = 2027;
enum int KEY_Greek_lamda = 2027;
enum int KEY_Greek_mu = 2028;
enum int KEY_Greek_nu = 2029;
enum int KEY_Greek_omega = 2041;
enum int KEY_Greek_omegaaccent = 1979;
enum int KEY_Greek_omicron = 2031;
enum int KEY_Greek_omicronaccent = 1975;
enum int KEY_Greek_phi = 2038;
enum int KEY_Greek_pi = 2032;
enum int KEY_Greek_psi = 2040;
enum int KEY_Greek_rho = 2033;
enum int KEY_Greek_sigma = 2034;
enum int KEY_Greek_switch = 65406;
enum int KEY_Greek_tau = 2036;
enum int KEY_Greek_theta = 2024;
enum int KEY_Greek_upsilon = 2037;
enum int KEY_Greek_upsilonaccent = 1976;
enum int KEY_Greek_upsilonaccentdieresis = 1978;
enum int KEY_Greek_upsilondieresis = 1977;
enum int KEY_Greek_xi = 2030;
enum int KEY_Greek_zeta = 2022;
enum int KEY_Green = 269025188;
enum int KEY_H = 72;
enum int KEY_Hangul = 65329;
enum int KEY_Hangul_A = 3775;
enum int KEY_Hangul_AE = 3776;
enum int KEY_Hangul_AraeA = 3830;
enum int KEY_Hangul_AraeAE = 3831;
enum int KEY_Hangul_Banja = 65337;
enum int KEY_Hangul_Cieuc = 3770;
enum int KEY_Hangul_Codeinput = 65335;
enum int KEY_Hangul_Dikeud = 3751;
enum int KEY_Hangul_E = 3780;
enum int KEY_Hangul_EO = 3779;
enum int KEY_Hangul_EU = 3793;
enum int KEY_Hangul_End = 65331;
enum int KEY_Hangul_Hanja = 65332;
enum int KEY_Hangul_Hieuh = 3774;
enum int KEY_Hangul_I = 3795;
enum int KEY_Hangul_Ieung = 3767;
enum int KEY_Hangul_J_Cieuc = 3818;
enum int KEY_Hangul_J_Dikeud = 3802;
enum int KEY_Hangul_J_Hieuh = 3822;
enum int KEY_Hangul_J_Ieung = 3816;
enum int KEY_Hangul_J_Jieuj = 3817;
enum int KEY_Hangul_J_Khieuq = 3819;
enum int KEY_Hangul_J_Kiyeog = 3796;
enum int KEY_Hangul_J_KiyeogSios = 3798;
enum int KEY_Hangul_J_KkogjiDalrinIeung = 3833;
enum int KEY_Hangul_J_Mieum = 3811;
enum int KEY_Hangul_J_Nieun = 3799;
enum int KEY_Hangul_J_NieunHieuh = 3801;
enum int KEY_Hangul_J_NieunJieuj = 3800;
enum int KEY_Hangul_J_PanSios = 3832;
enum int KEY_Hangul_J_Phieuf = 3821;
enum int KEY_Hangul_J_Pieub = 3812;
enum int KEY_Hangul_J_PieubSios = 3813;
enum int KEY_Hangul_J_Rieul = 3803;
enum int KEY_Hangul_J_RieulHieuh = 3810;
enum int KEY_Hangul_J_RieulKiyeog = 3804;
enum int KEY_Hangul_J_RieulMieum = 3805;
enum int KEY_Hangul_J_RieulPhieuf = 3809;
enum int KEY_Hangul_J_RieulPieub = 3806;
enum int KEY_Hangul_J_RieulSios = 3807;
enum int KEY_Hangul_J_RieulTieut = 3808;
enum int KEY_Hangul_J_Sios = 3814;
enum int KEY_Hangul_J_SsangKiyeog = 3797;
enum int KEY_Hangul_J_SsangSios = 3815;
enum int KEY_Hangul_J_Tieut = 3820;
enum int KEY_Hangul_J_YeorinHieuh = 3834;
enum int KEY_Hangul_Jamo = 65333;
enum int KEY_Hangul_Jeonja = 65336;
enum int KEY_Hangul_Jieuj = 3768;
enum int KEY_Hangul_Khieuq = 3771;
enum int KEY_Hangul_Kiyeog = 3745;
enum int KEY_Hangul_KiyeogSios = 3747;
enum int KEY_Hangul_KkogjiDalrinIeung = 3827;
enum int KEY_Hangul_Mieum = 3761;
enum int KEY_Hangul_MultipleCandidate = 65341;
enum int KEY_Hangul_Nieun = 3748;
enum int KEY_Hangul_NieunHieuh = 3750;
enum int KEY_Hangul_NieunJieuj = 3749;
enum int KEY_Hangul_O = 3783;
enum int KEY_Hangul_OE = 3786;
enum int KEY_Hangul_PanSios = 3826;
enum int KEY_Hangul_Phieuf = 3773;
enum int KEY_Hangul_Pieub = 3762;
enum int KEY_Hangul_PieubSios = 3764;
enum int KEY_Hangul_PostHanja = 65339;
enum int KEY_Hangul_PreHanja = 65338;
enum int KEY_Hangul_PreviousCandidate = 65342;
enum int KEY_Hangul_Rieul = 3753;
enum int KEY_Hangul_RieulHieuh = 3760;
enum int KEY_Hangul_RieulKiyeog = 3754;
enum int KEY_Hangul_RieulMieum = 3755;
enum int KEY_Hangul_RieulPhieuf = 3759;
enum int KEY_Hangul_RieulPieub = 3756;
enum int KEY_Hangul_RieulSios = 3757;
enum int KEY_Hangul_RieulTieut = 3758;
enum int KEY_Hangul_RieulYeorinHieuh = 3823;
enum int KEY_Hangul_Romaja = 65334;
enum int KEY_Hangul_SingleCandidate = 65340;
enum int KEY_Hangul_Sios = 3765;
enum int KEY_Hangul_Special = 65343;
enum int KEY_Hangul_SsangDikeud = 3752;
enum int KEY_Hangul_SsangJieuj = 3769;
enum int KEY_Hangul_SsangKiyeog = 3746;
enum int KEY_Hangul_SsangPieub = 3763;
enum int KEY_Hangul_SsangSios = 3766;
enum int KEY_Hangul_Start = 65330;
enum int KEY_Hangul_SunkyeongeumMieum = 3824;
enum int KEY_Hangul_SunkyeongeumPhieuf = 3828;
enum int KEY_Hangul_SunkyeongeumPieub = 3825;
enum int KEY_Hangul_Tieut = 3772;
enum int KEY_Hangul_U = 3788;
enum int KEY_Hangul_WA = 3784;
enum int KEY_Hangul_WAE = 3785;
enum int KEY_Hangul_WE = 3790;
enum int KEY_Hangul_WEO = 3789;
enum int KEY_Hangul_WI = 3791;
enum int KEY_Hangul_YA = 3777;
enum int KEY_Hangul_YAE = 3778;
enum int KEY_Hangul_YE = 3782;
enum int KEY_Hangul_YEO = 3781;
enum int KEY_Hangul_YI = 3794;
enum int KEY_Hangul_YO = 3787;
enum int KEY_Hangul_YU = 3792;
enum int KEY_Hangul_YeorinHieuh = 3829;
enum int KEY_Hangul_switch = 65406;
enum int KEY_Hankaku = 65321;
enum int KEY_Hcircumflex = 678;
enum int KEY_Hebrew_switch = 65406;
enum int KEY_Help = 65386;
enum int KEY_Henkan = 65315;
enum int KEY_Henkan_Mode = 65315;
enum int KEY_Hibernate = 269025192;
enum int KEY_Hiragana = 65317;
enum int KEY_Hiragana_Katakana = 65319;
enum int KEY_History = 269025079;
enum int KEY_Home = 65360;
enum int KEY_HomePage = 269025048;
enum int KEY_HotLinks = 269025082;
enum int KEY_Hstroke = 673;
enum int KEY_Hyper_L = 65517;
enum int KEY_Hyper_R = 65518;
enum int KEY_I = 73;
enum int KEY_ISO_Center_Object = 65075;
enum int KEY_ISO_Continuous_Underline = 65072;
enum int KEY_ISO_Discontinuous_Underline = 65073;
enum int KEY_ISO_Emphasize = 65074;
enum int KEY_ISO_Enter = 65076;
enum int KEY_ISO_Fast_Cursor_Down = 65071;
enum int KEY_ISO_Fast_Cursor_Left = 65068;
enum int KEY_ISO_Fast_Cursor_Right = 65069;
enum int KEY_ISO_Fast_Cursor_Up = 65070;
enum int KEY_ISO_First_Group = 65036;
enum int KEY_ISO_First_Group_Lock = 65037;
enum int KEY_ISO_Group_Latch = 65030;
enum int KEY_ISO_Group_Lock = 65031;
enum int KEY_ISO_Group_Shift = 65406;
enum int KEY_ISO_Last_Group = 65038;
enum int KEY_ISO_Last_Group_Lock = 65039;
enum int KEY_ISO_Left_Tab = 65056;
enum int KEY_ISO_Level2_Latch = 65026;
enum int KEY_ISO_Level3_Latch = 65028;
enum int KEY_ISO_Level3_Lock = 65029;
enum int KEY_ISO_Level3_Shift = 65027;
enum int KEY_ISO_Level5_Latch = 65042;
enum int KEY_ISO_Level5_Lock = 65043;
enum int KEY_ISO_Level5_Shift = 65041;
enum int KEY_ISO_Lock = 65025;
enum int KEY_ISO_Move_Line_Down = 65058;
enum int KEY_ISO_Move_Line_Up = 65057;
enum int KEY_ISO_Next_Group = 65032;
enum int KEY_ISO_Next_Group_Lock = 65033;
enum int KEY_ISO_Partial_Line_Down = 65060;
enum int KEY_ISO_Partial_Line_Up = 65059;
enum int KEY_ISO_Partial_Space_Left = 65061;
enum int KEY_ISO_Partial_Space_Right = 65062;
enum int KEY_ISO_Prev_Group = 65034;
enum int KEY_ISO_Prev_Group_Lock = 65035;
enum int KEY_ISO_Release_Both_Margins = 65067;
enum int KEY_ISO_Release_Margin_Left = 65065;
enum int KEY_ISO_Release_Margin_Right = 65066;
enum int KEY_ISO_Set_Margin_Left = 65063;
enum int KEY_ISO_Set_Margin_Right = 65064;
enum int KEY_Iabovedot = 681;
enum int KEY_Iacute = 205;
enum int KEY_Ibelowdot = 16785098;
enum int KEY_Ibreve = 16777516;
enum int KEY_Icircumflex = 206;
enum int KEY_Idiaeresis = 207;
enum int KEY_Igrave = 204;
enum int KEY_Ihook = 16785096;
enum int KEY_Imacron = 975;
enum int KEY_Insert = 65379;
enum int KEY_Iogonek = 967;
enum int KEY_Itilde = 933;
enum int KEY_J = 74;
enum int KEY_Jcircumflex = 684;
enum int KEY_K = 75;
enum int KEY_KP_0 = 65456;
enum int KEY_KP_1 = 65457;
enum int KEY_KP_2 = 65458;
enum int KEY_KP_3 = 65459;
enum int KEY_KP_4 = 65460;
enum int KEY_KP_5 = 65461;
enum int KEY_KP_6 = 65462;
enum int KEY_KP_7 = 65463;
enum int KEY_KP_8 = 65464;
enum int KEY_KP_9 = 65465;
enum int KEY_KP_Add = 65451;
enum int KEY_KP_Begin = 65437;
enum int KEY_KP_Decimal = 65454;
enum int KEY_KP_Delete = 65439;
enum int KEY_KP_Divide = 65455;
enum int KEY_KP_Down = 65433;
enum int KEY_KP_End = 65436;
enum int KEY_KP_Enter = 65421;
enum int KEY_KP_Equal = 65469;
enum int KEY_KP_F1 = 65425;
enum int KEY_KP_F2 = 65426;
enum int KEY_KP_F3 = 65427;
enum int KEY_KP_F4 = 65428;
enum int KEY_KP_Home = 65429;
enum int KEY_KP_Insert = 65438;
enum int KEY_KP_Left = 65430;
enum int KEY_KP_Multiply = 65450;
enum int KEY_KP_Next = 65435;
enum int KEY_KP_Page_Down = 65435;
enum int KEY_KP_Page_Up = 65434;
enum int KEY_KP_Prior = 65434;
enum int KEY_KP_Right = 65432;
enum int KEY_KP_Separator = 65452;
enum int KEY_KP_Space = 65408;
enum int KEY_KP_Subtract = 65453;
enum int KEY_KP_Tab = 65417;
enum int KEY_KP_Up = 65431;
enum int KEY_Kana_Lock = 65325;
enum int KEY_Kana_Shift = 65326;
enum int KEY_Kanji = 65313;
enum int KEY_Kanji_Bangou = 65335;
enum int KEY_Katakana = 65318;
enum int KEY_KbdBrightnessDown = 269025030;
enum int KEY_KbdBrightnessUp = 269025029;
enum int KEY_KbdLightOnOff = 269025028;
enum int KEY_Kcedilla = 979;
enum int KEY_Korean_Won = 3839;
enum int KEY_L = 76;
enum int KEY_L1 = 65480;
enum int KEY_L10 = 65489;
enum int KEY_L2 = 65481;
enum int KEY_L3 = 65482;
enum int KEY_L4 = 65483;
enum int KEY_L5 = 65484;
enum int KEY_L6 = 65485;
enum int KEY_L7 = 65486;
enum int KEY_L8 = 65487;
enum int KEY_L9 = 65488;
enum int KEY_Lacute = 453;
enum int KEY_Last_Virtual_Screen = 65236;
enum int KEY_Launch0 = 269025088;
enum int KEY_Launch1 = 269025089;
enum int KEY_Launch2 = 269025090;
enum int KEY_Launch3 = 269025091;
enum int KEY_Launch4 = 269025092;
enum int KEY_Launch5 = 269025093;
enum int KEY_Launch6 = 269025094;
enum int KEY_Launch7 = 269025095;
enum int KEY_Launch8 = 269025096;
enum int KEY_Launch9 = 269025097;
enum int KEY_LaunchA = 269025098;
enum int KEY_LaunchB = 269025099;
enum int KEY_LaunchC = 269025100;
enum int KEY_LaunchD = 269025101;
enum int KEY_LaunchE = 269025102;
enum int KEY_LaunchF = 269025103;
enum int KEY_Lbelowdot = 16784950;
enum int KEY_Lcaron = 421;
enum int KEY_Lcedilla = 934;
enum int KEY_Left = 65361;
enum int KEY_LightBulb = 269025077;
enum int KEY_Linefeed = 65290;
enum int KEY_LiraSign = 16785572;
enum int KEY_LogGrabInfo = 269024805;
enum int KEY_LogOff = 269025121;
enum int KEY_LogWindowTree = 269024804;
enum int KEY_Lstroke = 419;
enum int KEY_M = 77;
enum int KEY_Mabovedot = 16784960;
enum int KEY_Macedonia_DSE = 1717;
enum int KEY_Macedonia_GJE = 1714;
enum int KEY_Macedonia_KJE = 1724;
enum int KEY_Macedonia_dse = 1701;
enum int KEY_Macedonia_gje = 1698;
enum int KEY_Macedonia_kje = 1708;
enum int KEY_Mae_Koho = 65342;
enum int KEY_Mail = 269025049;
enum int KEY_MailForward = 269025168;
enum int KEY_Market = 269025122;
enum int KEY_Massyo = 65324;
enum int KEY_Meeting = 269025123;
enum int KEY_Memo = 269025054;
enum int KEY_Menu = 65383;
enum int KEY_MenuKB = 269025125;
enum int KEY_MenuPB = 269025126;
enum int KEY_Messenger = 269025166;
enum int KEY_Meta_L = 65511;
enum int KEY_Meta_R = 65512;
enum int KEY_MillSign = 16785573;
enum int KEY_ModeLock = 269025025;
enum int KEY_Mode_switch = 65406;
enum int KEY_MonBrightnessDown = 269025027;
enum int KEY_MonBrightnessUp = 269025026;
enum int KEY_MouseKeys_Accel_Enable = 65143;
enum int KEY_MouseKeys_Enable = 65142;
enum int KEY_Muhenkan = 65314;
enum int KEY_Multi_key = 65312;
enum int KEY_MultipleCandidate = 65341;
enum int KEY_Music = 269025170;
enum int KEY_MyComputer = 269025075;
enum int KEY_MySites = 269025127;
enum int KEY_N = 78;
enum int KEY_Nacute = 465;
enum int KEY_NairaSign = 16785574;
enum int KEY_Ncaron = 466;
enum int KEY_Ncedilla = 977;
enum int KEY_New = 269025128;
enum int KEY_NewSheqelSign = 16785578;
enum int KEY_News = 269025129;
enum int KEY_Next = 65366;
enum int KEY_Next_VMode = 269024802;
enum int KEY_Next_Virtual_Screen = 65234;
enum int KEY_Ntilde = 209;
enum int KEY_Num_Lock = 65407;
enum int KEY_O = 79;
enum int KEY_OE = 5052;
enum int KEY_Oacute = 211;
enum int KEY_Obarred = 16777631;
enum int KEY_Obelowdot = 16785100;
enum int KEY_Ocaron = 16777681;
enum int KEY_Ocircumflex = 212;
enum int KEY_Ocircumflexacute = 16785104;
enum int KEY_Ocircumflexbelowdot = 16785112;
enum int KEY_Ocircumflexgrave = 16785106;
enum int KEY_Ocircumflexhook = 16785108;
enum int KEY_Ocircumflextilde = 16785110;
enum int KEY_Odiaeresis = 214;
enum int KEY_Odoubleacute = 469;
enum int KEY_OfficeHome = 269025130;
enum int KEY_Ograve = 210;
enum int KEY_Ohook = 16785102;
enum int KEY_Ohorn = 16777632;
enum int KEY_Ohornacute = 16785114;
enum int KEY_Ohornbelowdot = 16785122;
enum int KEY_Ohorngrave = 16785116;
enum int KEY_Ohornhook = 16785118;
enum int KEY_Ohorntilde = 16785120;
enum int KEY_Omacron = 978;
enum int KEY_Ooblique = 216;
enum int KEY_Open = 269025131;
enum int KEY_OpenURL = 269025080;
enum int KEY_Option = 269025132;
enum int KEY_Oslash = 216;
enum int KEY_Otilde = 213;
enum int KEY_Overlay1_Enable = 65144;
enum int KEY_Overlay2_Enable = 65145;
enum int KEY_P = 80;
enum int KEY_Pabovedot = 16784982;
enum int KEY_Page_Down = 65366;
enum int KEY_Page_Up = 65365;
enum int KEY_Paste = 269025133;
enum int KEY_Pause = 65299;
enum int KEY_PesetaSign = 16785575;
enum int KEY_Phone = 269025134;
enum int KEY_Pictures = 269025169;
enum int KEY_Pointer_Accelerate = 65274;
enum int KEY_Pointer_Button1 = 65257;
enum int KEY_Pointer_Button2 = 65258;
enum int KEY_Pointer_Button3 = 65259;
enum int KEY_Pointer_Button4 = 65260;
enum int KEY_Pointer_Button5 = 65261;
enum int KEY_Pointer_Button_Dflt = 65256;
enum int KEY_Pointer_DblClick1 = 65263;
enum int KEY_Pointer_DblClick2 = 65264;
enum int KEY_Pointer_DblClick3 = 65265;
enum int KEY_Pointer_DblClick4 = 65266;
enum int KEY_Pointer_DblClick5 = 65267;
enum int KEY_Pointer_DblClick_Dflt = 65262;
enum int KEY_Pointer_DfltBtnNext = 65275;
enum int KEY_Pointer_DfltBtnPrev = 65276;
enum int KEY_Pointer_Down = 65251;
enum int KEY_Pointer_DownLeft = 65254;
enum int KEY_Pointer_DownRight = 65255;
enum int KEY_Pointer_Drag1 = 65269;
enum int KEY_Pointer_Drag2 = 65270;
enum int KEY_Pointer_Drag3 = 65271;
enum int KEY_Pointer_Drag4 = 65272;
enum int KEY_Pointer_Drag5 = 65277;
enum int KEY_Pointer_Drag_Dflt = 65268;
enum int KEY_Pointer_EnableKeys = 65273;
enum int KEY_Pointer_Left = 65248;
enum int KEY_Pointer_Right = 65249;
enum int KEY_Pointer_Up = 65250;
enum int KEY_Pointer_UpLeft = 65252;
enum int KEY_Pointer_UpRight = 65253;
enum int KEY_PowerDown = 269025057;
enum int KEY_PowerOff = 269025066;
enum int KEY_Prev_VMode = 269024803;
enum int KEY_Prev_Virtual_Screen = 65233;
enum int KEY_PreviousCandidate = 65342;
enum int KEY_Print = 65377;
enum int KEY_Prior = 65365;
enum int KEY_Q = 81;
enum int KEY_R = 82;
enum int KEY_R1 = 65490;
enum int KEY_R10 = 65499;
enum int KEY_R11 = 65500;
enum int KEY_R12 = 65501;
enum int KEY_R13 = 65502;
enum int KEY_R14 = 65503;
enum int KEY_R15 = 65504;
enum int KEY_R2 = 65491;
enum int KEY_R3 = 65492;
enum int KEY_R4 = 65493;
enum int KEY_R5 = 65494;
enum int KEY_R6 = 65495;
enum int KEY_R7 = 65496;
enum int KEY_R8 = 65497;
enum int KEY_R9 = 65498;
enum int KEY_Racute = 448;
enum int KEY_Rcaron = 472;
enum int KEY_Rcedilla = 931;
enum int KEY_Red = 269025187;
enum int KEY_Redo = 65382;
enum int KEY_Refresh = 269025065;
enum int KEY_Reload = 269025139;
enum int KEY_RepeatKeys_Enable = 65138;
enum int KEY_Reply = 269025138;
enum int KEY_Return = 65293;
enum int KEY_Right = 65363;
enum int KEY_RockerDown = 269025060;
enum int KEY_RockerEnter = 269025061;
enum int KEY_RockerUp = 269025059;
enum int KEY_Romaji = 65316;
enum int KEY_RotateWindows = 269025140;
enum int KEY_RotationKB = 269025142;
enum int KEY_RotationPB = 269025141;
enum int KEY_RupeeSign = 16785576;
enum int KEY_S = 83;
enum int KEY_SCHWA = 16777615;
enum int KEY_Sabovedot = 16784992;
enum int KEY_Sacute = 422;
enum int KEY_Save = 269025143;
enum int KEY_Scaron = 425;
enum int KEY_Scedilla = 426;
enum int KEY_Scircumflex = 734;
enum int KEY_ScreenSaver = 269025069;
enum int KEY_ScrollClick = 269025146;
enum int KEY_ScrollDown = 269025145;
enum int KEY_ScrollUp = 269025144;
enum int KEY_Scroll_Lock = 65300;
enum int KEY_Search = 269025051;
enum int KEY_Select = 65376;
enum int KEY_SelectButton = 269025184;
enum int KEY_Send = 269025147;
enum int KEY_Serbian_DJE = 1713;
enum int KEY_Serbian_DZE = 1727;
enum int KEY_Serbian_JE = 1720;
enum int KEY_Serbian_LJE = 1721;
enum int KEY_Serbian_NJE = 1722;
enum int KEY_Serbian_TSHE = 1723;
enum int KEY_Serbian_dje = 1697;
enum int KEY_Serbian_dze = 1711;
enum int KEY_Serbian_je = 1704;
enum int KEY_Serbian_lje = 1705;
enum int KEY_Serbian_nje = 1706;
enum int KEY_Serbian_tshe = 1707;
enum int KEY_Shift_L = 65505;
enum int KEY_Shift_Lock = 65510;
enum int KEY_Shift_R = 65506;
enum int KEY_Shop = 269025078;
enum int KEY_SingleCandidate = 65340;
enum int KEY_Sinh_a = 16780677;
enum int KEY_Sinh_aa = 16780678;
enum int KEY_Sinh_aa2 = 16780751;
enum int KEY_Sinh_ae = 16780679;
enum int KEY_Sinh_ae2 = 16780752;
enum int KEY_Sinh_aee = 16780680;
enum int KEY_Sinh_aee2 = 16780753;
enum int KEY_Sinh_ai = 16780691;
enum int KEY_Sinh_ai2 = 16780763;
enum int KEY_Sinh_al = 16780746;
enum int KEY_Sinh_au = 16780694;
enum int KEY_Sinh_au2 = 16780766;
enum int KEY_Sinh_ba = 16780726;
enum int KEY_Sinh_bha = 16780727;
enum int KEY_Sinh_ca = 16780704;
enum int KEY_Sinh_cha = 16780705;
enum int KEY_Sinh_dda = 16780713;
enum int KEY_Sinh_ddha = 16780714;
enum int KEY_Sinh_dha = 16780719;
enum int KEY_Sinh_dhha = 16780720;
enum int KEY_Sinh_e = 16780689;
enum int KEY_Sinh_e2 = 16780761;
enum int KEY_Sinh_ee = 16780690;
enum int KEY_Sinh_ee2 = 16780762;
enum int KEY_Sinh_fa = 16780742;
enum int KEY_Sinh_ga = 16780700;
enum int KEY_Sinh_gha = 16780701;
enum int KEY_Sinh_h2 = 16780675;
enum int KEY_Sinh_ha = 16780740;
enum int KEY_Sinh_i = 16780681;
enum int KEY_Sinh_i2 = 16780754;
enum int KEY_Sinh_ii = 16780682;
enum int KEY_Sinh_ii2 = 16780755;
enum int KEY_Sinh_ja = 16780706;
enum int KEY_Sinh_jha = 16780707;
enum int KEY_Sinh_jnya = 16780709;
enum int KEY_Sinh_ka = 16780698;
enum int KEY_Sinh_kha = 16780699;
enum int KEY_Sinh_kunddaliya = 16780788;
enum int KEY_Sinh_la = 16780733;
enum int KEY_Sinh_lla = 16780741;
enum int KEY_Sinh_lu = 16780687;
enum int KEY_Sinh_lu2 = 16780767;
enum int KEY_Sinh_luu = 16780688;
enum int KEY_Sinh_luu2 = 16780787;
enum int KEY_Sinh_ma = 16780728;
enum int KEY_Sinh_mba = 16780729;
enum int KEY_Sinh_na = 16780721;
enum int KEY_Sinh_ndda = 16780716;
enum int KEY_Sinh_ndha = 16780723;
enum int KEY_Sinh_ng = 16780674;
enum int KEY_Sinh_ng2 = 16780702;
enum int KEY_Sinh_nga = 16780703;
enum int KEY_Sinh_nja = 16780710;
enum int KEY_Sinh_nna = 16780715;
enum int KEY_Sinh_nya = 16780708;
enum int KEY_Sinh_o = 16780692;
enum int KEY_Sinh_o2 = 16780764;
enum int KEY_Sinh_oo = 16780693;
enum int KEY_Sinh_oo2 = 16780765;
enum int KEY_Sinh_pa = 16780724;
enum int KEY_Sinh_pha = 16780725;
enum int KEY_Sinh_ra = 16780731;
enum int KEY_Sinh_ri = 16780685;
enum int KEY_Sinh_rii = 16780686;
enum int KEY_Sinh_ru2 = 16780760;
enum int KEY_Sinh_ruu2 = 16780786;
enum int KEY_Sinh_sa = 16780739;
enum int KEY_Sinh_sha = 16780737;
enum int KEY_Sinh_ssha = 16780738;
enum int KEY_Sinh_tha = 16780717;
enum int KEY_Sinh_thha = 16780718;
enum int KEY_Sinh_tta = 16780711;
enum int KEY_Sinh_ttha = 16780712;
enum int KEY_Sinh_u = 16780683;
enum int KEY_Sinh_u2 = 16780756;
enum int KEY_Sinh_uu = 16780684;
enum int KEY_Sinh_uu2 = 16780758;
enum int KEY_Sinh_va = 16780736;
enum int KEY_Sinh_ya = 16780730;
enum int KEY_Sleep = 269025071;
enum int KEY_SlowKeys_Enable = 65139;
enum int KEY_Spell = 269025148;
enum int KEY_SplitScreen = 269025149;
enum int KEY_Standby = 269025040;
enum int KEY_Start = 269025050;
enum int KEY_StickyKeys_Enable = 65141;
enum int KEY_Stop = 269025064;
enum int KEY_Subtitle = 269025178;
enum int KEY_Super_L = 65515;
enum int KEY_Super_R = 65516;
enum int KEY_Support = 269025150;
enum int KEY_Suspend = 269025191;
enum int KEY_Switch_VT_1 = 269024769;
enum int KEY_Switch_VT_10 = 269024778;
enum int KEY_Switch_VT_11 = 269024779;
enum int KEY_Switch_VT_12 = 269024780;
enum int KEY_Switch_VT_2 = 269024770;
enum int KEY_Switch_VT_3 = 269024771;
enum int KEY_Switch_VT_4 = 269024772;
enum int KEY_Switch_VT_5 = 269024773;
enum int KEY_Switch_VT_6 = 269024774;
enum int KEY_Switch_VT_7 = 269024775;
enum int KEY_Switch_VT_8 = 269024776;
enum int KEY_Switch_VT_9 = 269024777;
enum int KEY_Sys_Req = 65301;
enum int KEY_T = 84;
enum int KEY_THORN = 222;
enum int KEY_Tab = 65289;
enum int KEY_Tabovedot = 16785002;
enum int KEY_TaskPane = 269025151;
enum int KEY_Tcaron = 427;
enum int KEY_Tcedilla = 478;
enum int KEY_Terminal = 269025152;
enum int KEY_Terminate_Server = 65237;
enum int KEY_Thai_baht = 3551;
enum int KEY_Thai_bobaimai = 3514;
enum int KEY_Thai_chochan = 3496;
enum int KEY_Thai_chochang = 3498;
enum int KEY_Thai_choching = 3497;
enum int KEY_Thai_chochoe = 3500;
enum int KEY_Thai_dochada = 3502;
enum int KEY_Thai_dodek = 3508;
enum int KEY_Thai_fofa = 3517;
enum int KEY_Thai_fofan = 3519;
enum int KEY_Thai_hohip = 3531;
enum int KEY_Thai_honokhuk = 3534;
enum int KEY_Thai_khokhai = 3490;
enum int KEY_Thai_khokhon = 3493;
enum int KEY_Thai_khokhuat = 3491;
enum int KEY_Thai_khokhwai = 3492;
enum int KEY_Thai_khorakhang = 3494;
enum int KEY_Thai_kokai = 3489;
enum int KEY_Thai_lakkhangyao = 3557;
enum int KEY_Thai_lekchet = 3575;
enum int KEY_Thai_lekha = 3573;
enum int KEY_Thai_lekhok = 3574;
enum int KEY_Thai_lekkao = 3577;
enum int KEY_Thai_leknung = 3569;
enum int KEY_Thai_lekpaet = 3576;
enum int KEY_Thai_leksam = 3571;
enum int KEY_Thai_leksi = 3572;
enum int KEY_Thai_leksong = 3570;
enum int KEY_Thai_leksun = 3568;
enum int KEY_Thai_lochula = 3532;
enum int KEY_Thai_loling = 3525;
enum int KEY_Thai_lu = 3526;
enum int KEY_Thai_maichattawa = 3563;
enum int KEY_Thai_maiek = 3560;
enum int KEY_Thai_maihanakat = 3537;
enum int KEY_Thai_maihanakat_maitho = 3550;
enum int KEY_Thai_maitaikhu = 3559;
enum int KEY_Thai_maitho = 3561;
enum int KEY_Thai_maitri = 3562;
enum int KEY_Thai_maiyamok = 3558;
enum int KEY_Thai_moma = 3521;
enum int KEY_Thai_ngongu = 3495;
enum int KEY_Thai_nikhahit = 3565;
enum int KEY_Thai_nonen = 3507;
enum int KEY_Thai_nonu = 3513;
enum int KEY_Thai_oang = 3533;
enum int KEY_Thai_paiyannoi = 3535;
enum int KEY_Thai_phinthu = 3546;
enum int KEY_Thai_phophan = 3518;
enum int KEY_Thai_phophung = 3516;
enum int KEY_Thai_phosamphao = 3520;
enum int KEY_Thai_popla = 3515;
enum int KEY_Thai_rorua = 3523;
enum int KEY_Thai_ru = 3524;
enum int KEY_Thai_saraa = 3536;
enum int KEY_Thai_saraaa = 3538;
enum int KEY_Thai_saraae = 3553;
enum int KEY_Thai_saraaimaimalai = 3556;
enum int KEY_Thai_saraaimaimuan = 3555;
enum int KEY_Thai_saraam = 3539;
enum int KEY_Thai_sarae = 3552;
enum int KEY_Thai_sarai = 3540;
enum int KEY_Thai_saraii = 3541;
enum int KEY_Thai_sarao = 3554;
enum int KEY_Thai_sarau = 3544;
enum int KEY_Thai_saraue = 3542;
enum int KEY_Thai_sarauee = 3543;
enum int KEY_Thai_sarauu = 3545;
enum int KEY_Thai_sorusi = 3529;
enum int KEY_Thai_sosala = 3528;
enum int KEY_Thai_soso = 3499;
enum int KEY_Thai_sosua = 3530;
enum int KEY_Thai_thanthakhat = 3564;
enum int KEY_Thai_thonangmontho = 3505;
enum int KEY_Thai_thophuthao = 3506;
enum int KEY_Thai_thothahan = 3511;
enum int KEY_Thai_thothan = 3504;
enum int KEY_Thai_thothong = 3512;
enum int KEY_Thai_thothung = 3510;
enum int KEY_Thai_topatak = 3503;
enum int KEY_Thai_totao = 3509;
enum int KEY_Thai_wowaen = 3527;
enum int KEY_Thai_yoyak = 3522;
enum int KEY_Thai_yoying = 3501;
enum int KEY_Thorn = 222;
enum int KEY_Time = 269025183;
enum int KEY_ToDoList = 269025055;
enum int KEY_Tools = 269025153;
enum int KEY_TopMenu = 269025186;
enum int KEY_TouchpadOff = 269025201;
enum int KEY_TouchpadOn = 269025200;
enum int KEY_TouchpadToggle = 269025193;
enum int KEY_Touroku = 65323;
enum int KEY_Travel = 269025154;
enum int KEY_Tslash = 940;
enum int KEY_U = 85;
enum int KEY_UWB = 269025174;
enum int KEY_Uacute = 218;
enum int KEY_Ubelowdot = 16785124;
enum int KEY_Ubreve = 733;
enum int KEY_Ucircumflex = 219;
enum int KEY_Udiaeresis = 220;
enum int KEY_Udoubleacute = 475;
enum int KEY_Ugrave = 217;
enum int KEY_Uhook = 16785126;
enum int KEY_Uhorn = 16777647;
enum int KEY_Uhornacute = 16785128;
enum int KEY_Uhornbelowdot = 16785136;
enum int KEY_Uhorngrave = 16785130;
enum int KEY_Uhornhook = 16785132;
enum int KEY_Uhorntilde = 16785134;
enum int KEY_Ukrainian_GHE_WITH_UPTURN = 1725;
enum int KEY_Ukrainian_I = 1718;
enum int KEY_Ukrainian_IE = 1716;
enum int KEY_Ukrainian_YI = 1719;
enum int KEY_Ukrainian_ghe_with_upturn = 1709;
enum int KEY_Ukrainian_i = 1702;
enum int KEY_Ukrainian_ie = 1700;
enum int KEY_Ukrainian_yi = 1703;
enum int KEY_Ukranian_I = 1718;
enum int KEY_Ukranian_JE = 1716;
enum int KEY_Ukranian_YI = 1719;
enum int KEY_Ukranian_i = 1702;
enum int KEY_Ukranian_je = 1700;
enum int KEY_Ukranian_yi = 1703;
enum int KEY_Umacron = 990;
enum int KEY_Undo = 65381;
enum int KEY_Ungrab = 269024800;
enum int KEY_Uogonek = 985;
enum int KEY_Up = 65362;
enum int KEY_Uring = 473;
enum int KEY_User1KB = 269025157;
enum int KEY_User2KB = 269025158;
enum int KEY_UserPB = 269025156;
enum int KEY_Utilde = 989;
enum int KEY_V = 86;
enum int KEY_VendorHome = 269025076;
enum int KEY_Video = 269025159;
enum int KEY_View = 269025185;
enum int KEY_VoidSymbol = 16777215;
enum int KEY_W = 87;
enum int KEY_WLAN = 269025173;
enum int KEY_WWW = 269025070;
enum int KEY_Wacute = 16785026;
enum int KEY_WakeUp = 269025067;
enum int KEY_Wcircumflex = 16777588;
enum int KEY_Wdiaeresis = 16785028;
enum int KEY_WebCam = 269025167;
enum int KEY_Wgrave = 16785024;
enum int KEY_WheelButton = 269025160;
enum int KEY_WindowClear = 269025109;
enum int KEY_WonSign = 16785577;
enum int KEY_Word = 269025161;
enum int KEY_X = 88;
enum int KEY_Xabovedot = 16785034;
enum int KEY_Xfer = 269025162;
enum int KEY_Y = 89;
enum int KEY_Yacute = 221;
enum int KEY_Ybelowdot = 16785140;
enum int KEY_Ycircumflex = 16777590;
enum int KEY_Ydiaeresis = 5054;
enum int KEY_Yellow = 269025189;
enum int KEY_Ygrave = 16785138;
enum int KEY_Yhook = 16785142;
enum int KEY_Ytilde = 16785144;
enum int KEY_Z = 90;
enum int KEY_Zabovedot = 431;
enum int KEY_Zacute = 428;
enum int KEY_Zcaron = 430;
enum int KEY_Zen_Koho = 65341;
enum int KEY_Zenkaku = 65320;
enum int KEY_Zenkaku_Hankaku = 65322;
enum int KEY_ZoomIn = 269025163;
enum int KEY_ZoomOut = 269025164;
enum int KEY_Zstroke = 16777653;
enum int KEY_a = 97;
enum int KEY_aacute = 225;
enum int KEY_abelowdot = 16785057;
enum int KEY_abovedot = 511;
enum int KEY_abreve = 483;
enum int KEY_abreveacute = 16785071;
enum int KEY_abrevebelowdot = 16785079;
enum int KEY_abrevegrave = 16785073;
enum int KEY_abrevehook = 16785075;
enum int KEY_abrevetilde = 16785077;
enum int KEY_acircumflex = 226;
enum int KEY_acircumflexacute = 16785061;
enum int KEY_acircumflexbelowdot = 16785069;
enum int KEY_acircumflexgrave = 16785063;
enum int KEY_acircumflexhook = 16785065;
enum int KEY_acircumflextilde = 16785067;
enum int KEY_acute = 180;
enum int KEY_adiaeresis = 228;
enum int KEY_ae = 230;
enum int KEY_agrave = 224;
enum int KEY_ahook = 16785059;
enum int KEY_amacron = 992;
enum int KEY_ampersand = 38;
enum int KEY_aogonek = 433;
enum int KEY_apostrophe = 39;
enum int KEY_approxeq = 16785992;
enum int KEY_approximate = 2248;
enum int KEY_aring = 229;
enum int KEY_asciicircum = 94;
enum int KEY_asciitilde = 126;
enum int KEY_asterisk = 42;
enum int KEY_at = 64;
enum int KEY_atilde = 227;
enum int KEY_b = 98;
enum int KEY_babovedot = 16784899;
enum int KEY_backslash = 92;
enum int KEY_ballotcross = 2804;
enum int KEY_bar = 124;
enum int KEY_because = 16785973;
enum int KEY_blank = 2527;
enum int KEY_botintegral = 2213;
enum int KEY_botleftparens = 2220;
enum int KEY_botleftsqbracket = 2216;
enum int KEY_botleftsummation = 2226;
enum int KEY_botrightparens = 2222;
enum int KEY_botrightsqbracket = 2218;
enum int KEY_botrightsummation = 2230;
enum int KEY_bott = 2550;
enum int KEY_botvertsummationconnector = 2228;
enum int KEY_braceleft = 123;
enum int KEY_braceright = 125;
enum int KEY_bracketleft = 91;
enum int KEY_bracketright = 93;
enum int KEY_braille_blank = 16787456;
enum int KEY_braille_dot_1 = 65521;
enum int KEY_braille_dot_10 = 65530;
enum int KEY_braille_dot_2 = 65522;
enum int KEY_braille_dot_3 = 65523;
enum int KEY_braille_dot_4 = 65524;
enum int KEY_braille_dot_5 = 65525;
enum int KEY_braille_dot_6 = 65526;
enum int KEY_braille_dot_7 = 65527;
enum int KEY_braille_dot_8 = 65528;
enum int KEY_braille_dot_9 = 65529;
enum int KEY_braille_dots_1 = 16787457;
enum int KEY_braille_dots_12 = 16787459;
enum int KEY_braille_dots_123 = 16787463;
enum int KEY_braille_dots_1234 = 16787471;
enum int KEY_braille_dots_12345 = 16787487;
enum int KEY_braille_dots_123456 = 16787519;
enum int KEY_braille_dots_1234567 = 16787583;
enum int KEY_braille_dots_12345678 = 16787711;
enum int KEY_braille_dots_1234568 = 16787647;
enum int KEY_braille_dots_123457 = 16787551;
enum int KEY_braille_dots_1234578 = 16787679;
enum int KEY_braille_dots_123458 = 16787615;
enum int KEY_braille_dots_12346 = 16787503;
enum int KEY_braille_dots_123467 = 16787567;
enum int KEY_braille_dots_1234678 = 16787695;
enum int KEY_braille_dots_123468 = 16787631;
enum int KEY_braille_dots_12347 = 16787535;
enum int KEY_braille_dots_123478 = 16787663;
enum int KEY_braille_dots_12348 = 16787599;
enum int KEY_braille_dots_1235 = 16787479;
enum int KEY_braille_dots_12356 = 16787511;
enum int KEY_braille_dots_123567 = 16787575;
enum int KEY_braille_dots_1235678 = 16787703;
enum int KEY_braille_dots_123568 = 16787639;
enum int KEY_braille_dots_12357 = 16787543;
enum int KEY_braille_dots_123578 = 16787671;
enum int KEY_braille_dots_12358 = 16787607;
enum int KEY_braille_dots_1236 = 16787495;
enum int KEY_braille_dots_12367 = 16787559;
enum int KEY_braille_dots_123678 = 16787687;
enum int KEY_braille_dots_12368 = 16787623;
enum int KEY_braille_dots_1237 = 16787527;
enum int KEY_braille_dots_12378 = 16787655;
enum int KEY_braille_dots_1238 = 16787591;
enum int KEY_braille_dots_124 = 16787467;
enum int KEY_braille_dots_1245 = 16787483;
enum int KEY_braille_dots_12456 = 16787515;
enum int KEY_braille_dots_124567 = 16787579;
enum int KEY_braille_dots_1245678 = 16787707;
enum int KEY_braille_dots_124568 = 16787643;
enum int KEY_braille_dots_12457 = 16787547;
enum int KEY_braille_dots_124578 = 16787675;
enum int KEY_braille_dots_12458 = 16787611;
enum int KEY_braille_dots_1246 = 16787499;
enum int KEY_braille_dots_12467 = 16787563;
enum int KEY_braille_dots_124678 = 16787691;
enum int KEY_braille_dots_12468 = 16787627;
enum int KEY_braille_dots_1247 = 16787531;
enum int KEY_braille_dots_12478 = 16787659;
enum int KEY_braille_dots_1248 = 16787595;
enum int KEY_braille_dots_125 = 16787475;
enum int KEY_braille_dots_1256 = 16787507;
enum int KEY_braille_dots_12567 = 16787571;
enum int KEY_braille_dots_125678 = 16787699;
enum int KEY_braille_dots_12568 = 16787635;
enum int KEY_braille_dots_1257 = 16787539;
enum int KEY_braille_dots_12578 = 16787667;
enum int KEY_braille_dots_1258 = 16787603;
enum int KEY_braille_dots_126 = 16787491;
enum int KEY_braille_dots_1267 = 16787555;
enum int KEY_braille_dots_12678 = 16787683;
enum int KEY_braille_dots_1268 = 16787619;
enum int KEY_braille_dots_127 = 16787523;
enum int KEY_braille_dots_1278 = 16787651;
enum int KEY_braille_dots_128 = 16787587;
enum int KEY_braille_dots_13 = 16787461;
enum int KEY_braille_dots_134 = 16787469;
enum int KEY_braille_dots_1345 = 16787485;
enum int KEY_braille_dots_13456 = 16787517;
enum int KEY_braille_dots_134567 = 16787581;
enum int KEY_braille_dots_1345678 = 16787709;
enum int KEY_braille_dots_134568 = 16787645;
enum int KEY_braille_dots_13457 = 16787549;
enum int KEY_braille_dots_134578 = 16787677;
enum int KEY_braille_dots_13458 = 16787613;
enum int KEY_braille_dots_1346 = 16787501;
enum int KEY_braille_dots_13467 = 16787565;
enum int KEY_braille_dots_134678 = 16787693;
enum int KEY_braille_dots_13468 = 16787629;
enum int KEY_braille_dots_1347 = 16787533;
enum int KEY_braille_dots_13478 = 16787661;
enum int KEY_braille_dots_1348 = 16787597;
enum int KEY_braille_dots_135 = 16787477;
enum int KEY_braille_dots_1356 = 16787509;
enum int KEY_braille_dots_13567 = 16787573;
enum int KEY_braille_dots_135678 = 16787701;
enum int KEY_braille_dots_13568 = 16787637;
enum int KEY_braille_dots_1357 = 16787541;
enum int KEY_braille_dots_13578 = 16787669;
enum int KEY_braille_dots_1358 = 16787605;
enum int KEY_braille_dots_136 = 16787493;
enum int KEY_braille_dots_1367 = 16787557;
enum int KEY_braille_dots_13678 = 16787685;
enum int KEY_braille_dots_1368 = 16787621;
enum int KEY_braille_dots_137 = 16787525;
enum int KEY_braille_dots_1378 = 16787653;
enum int KEY_braille_dots_138 = 16787589;
enum int KEY_braille_dots_14 = 16787465;
enum int KEY_braille_dots_145 = 16787481;
enum int KEY_braille_dots_1456 = 16787513;
enum int KEY_braille_dots_14567 = 16787577;
enum int KEY_braille_dots_145678 = 16787705;
enum int KEY_braille_dots_14568 = 16787641;
enum int KEY_braille_dots_1457 = 16787545;
enum int KEY_braille_dots_14578 = 16787673;
enum int KEY_braille_dots_1458 = 16787609;
enum int KEY_braille_dots_146 = 16787497;
enum int KEY_braille_dots_1467 = 16787561;
enum int KEY_braille_dots_14678 = 16787689;
enum int KEY_braille_dots_1468 = 16787625;
enum int KEY_braille_dots_147 = 16787529;
enum int KEY_braille_dots_1478 = 16787657;
enum int KEY_braille_dots_148 = 16787593;
enum int KEY_braille_dots_15 = 16787473;
enum int KEY_braille_dots_156 = 16787505;
enum int KEY_braille_dots_1567 = 16787569;
enum int KEY_braille_dots_15678 = 16787697;
enum int KEY_braille_dots_1568 = 16787633;
enum int KEY_braille_dots_157 = 16787537;
enum int KEY_braille_dots_1578 = 16787665;
enum int KEY_braille_dots_158 = 16787601;
enum int KEY_braille_dots_16 = 16787489;
enum int KEY_braille_dots_167 = 16787553;
enum int KEY_braille_dots_1678 = 16787681;
enum int KEY_braille_dots_168 = 16787617;
enum int KEY_braille_dots_17 = 16787521;
enum int KEY_braille_dots_178 = 16787649;
enum int KEY_braille_dots_18 = 16787585;
enum int KEY_braille_dots_2 = 16787458;
enum int KEY_braille_dots_23 = 16787462;
enum int KEY_braille_dots_234 = 16787470;
enum int KEY_braille_dots_2345 = 16787486;
enum int KEY_braille_dots_23456 = 16787518;
enum int KEY_braille_dots_234567 = 16787582;
enum int KEY_braille_dots_2345678 = 16787710;
enum int KEY_braille_dots_234568 = 16787646;
enum int KEY_braille_dots_23457 = 16787550;
enum int KEY_braille_dots_234578 = 16787678;
enum int KEY_braille_dots_23458 = 16787614;
enum int KEY_braille_dots_2346 = 16787502;
enum int KEY_braille_dots_23467 = 16787566;
enum int KEY_braille_dots_234678 = 16787694;
enum int KEY_braille_dots_23468 = 16787630;
enum int KEY_braille_dots_2347 = 16787534;
enum int KEY_braille_dots_23478 = 16787662;
enum int KEY_braille_dots_2348 = 16787598;
enum int KEY_braille_dots_235 = 16787478;
enum int KEY_braille_dots_2356 = 16787510;
enum int KEY_braille_dots_23567 = 16787574;
enum int KEY_braille_dots_235678 = 16787702;
enum int KEY_braille_dots_23568 = 16787638;
enum int KEY_braille_dots_2357 = 16787542;
enum int KEY_braille_dots_23578 = 16787670;
enum int KEY_braille_dots_2358 = 16787606;
enum int KEY_braille_dots_236 = 16787494;
enum int KEY_braille_dots_2367 = 16787558;
enum int KEY_braille_dots_23678 = 16787686;
enum int KEY_braille_dots_2368 = 16787622;
enum int KEY_braille_dots_237 = 16787526;
enum int KEY_braille_dots_2378 = 16787654;
enum int KEY_braille_dots_238 = 16787590;
enum int KEY_braille_dots_24 = 16787466;
enum int KEY_braille_dots_245 = 16787482;
enum int KEY_braille_dots_2456 = 16787514;
enum int KEY_braille_dots_24567 = 16787578;
enum int KEY_braille_dots_245678 = 16787706;
enum int KEY_braille_dots_24568 = 16787642;
enum int KEY_braille_dots_2457 = 16787546;
enum int KEY_braille_dots_24578 = 16787674;
enum int KEY_braille_dots_2458 = 16787610;
enum int KEY_braille_dots_246 = 16787498;
enum int KEY_braille_dots_2467 = 16787562;
enum int KEY_braille_dots_24678 = 16787690;
enum int KEY_braille_dots_2468 = 16787626;
enum int KEY_braille_dots_247 = 16787530;
enum int KEY_braille_dots_2478 = 16787658;
enum int KEY_braille_dots_248 = 16787594;
enum int KEY_braille_dots_25 = 16787474;
enum int KEY_braille_dots_256 = 16787506;
enum int KEY_braille_dots_2567 = 16787570;
enum int KEY_braille_dots_25678 = 16787698;
enum int KEY_braille_dots_2568 = 16787634;
enum int KEY_braille_dots_257 = 16787538;
enum int KEY_braille_dots_2578 = 16787666;
enum int KEY_braille_dots_258 = 16787602;
enum int KEY_braille_dots_26 = 16787490;
enum int KEY_braille_dots_267 = 16787554;
enum int KEY_braille_dots_2678 = 16787682;
enum int KEY_braille_dots_268 = 16787618;
enum int KEY_braille_dots_27 = 16787522;
enum int KEY_braille_dots_278 = 16787650;
enum int KEY_braille_dots_28 = 16787586;
enum int KEY_braille_dots_3 = 16787460;
enum int KEY_braille_dots_34 = 16787468;
enum int KEY_braille_dots_345 = 16787484;
enum int KEY_braille_dots_3456 = 16787516;
enum int KEY_braille_dots_34567 = 16787580;
enum int KEY_braille_dots_345678 = 16787708;
enum int KEY_braille_dots_34568 = 16787644;
enum int KEY_braille_dots_3457 = 16787548;
enum int KEY_braille_dots_34578 = 16787676;
enum int KEY_braille_dots_3458 = 16787612;
enum int KEY_braille_dots_346 = 16787500;
enum int KEY_braille_dots_3467 = 16787564;
enum int KEY_braille_dots_34678 = 16787692;
enum int KEY_braille_dots_3468 = 16787628;
enum int KEY_braille_dots_347 = 16787532;
enum int KEY_braille_dots_3478 = 16787660;
enum int KEY_braille_dots_348 = 16787596;
enum int KEY_braille_dots_35 = 16787476;
enum int KEY_braille_dots_356 = 16787508;
enum int KEY_braille_dots_3567 = 16787572;
enum int KEY_braille_dots_35678 = 16787700;
enum int KEY_braille_dots_3568 = 16787636;
enum int KEY_braille_dots_357 = 16787540;
enum int KEY_braille_dots_3578 = 16787668;
enum int KEY_braille_dots_358 = 16787604;
enum int KEY_braille_dots_36 = 16787492;
enum int KEY_braille_dots_367 = 16787556;
enum int KEY_braille_dots_3678 = 16787684;
enum int KEY_braille_dots_368 = 16787620;
enum int KEY_braille_dots_37 = 16787524;
enum int KEY_braille_dots_378 = 16787652;
enum int KEY_braille_dots_38 = 16787588;
enum int KEY_braille_dots_4 = 16787464;
enum int KEY_braille_dots_45 = 16787480;
enum int KEY_braille_dots_456 = 16787512;
enum int KEY_braille_dots_4567 = 16787576;
enum int KEY_braille_dots_45678 = 16787704;
enum int KEY_braille_dots_4568 = 16787640;
enum int KEY_braille_dots_457 = 16787544;
enum int KEY_braille_dots_4578 = 16787672;
enum int KEY_braille_dots_458 = 16787608;
enum int KEY_braille_dots_46 = 16787496;
enum int KEY_braille_dots_467 = 16787560;
enum int KEY_braille_dots_4678 = 16787688;
enum int KEY_braille_dots_468 = 16787624;
enum int KEY_braille_dots_47 = 16787528;
enum int KEY_braille_dots_478 = 16787656;
enum int KEY_braille_dots_48 = 16787592;
enum int KEY_braille_dots_5 = 16787472;
enum int KEY_braille_dots_56 = 16787504;
enum int KEY_braille_dots_567 = 16787568;
enum int KEY_braille_dots_5678 = 16787696;
enum int KEY_braille_dots_568 = 16787632;
enum int KEY_braille_dots_57 = 16787536;
enum int KEY_braille_dots_578 = 16787664;
enum int KEY_braille_dots_58 = 16787600;
enum int KEY_braille_dots_6 = 16787488;
enum int KEY_braille_dots_67 = 16787552;
enum int KEY_braille_dots_678 = 16787680;
enum int KEY_braille_dots_68 = 16787616;
enum int KEY_braille_dots_7 = 16787520;
enum int KEY_braille_dots_78 = 16787648;
enum int KEY_braille_dots_8 = 16787584;
enum int KEY_breve = 418;
enum int KEY_brokenbar = 166;
enum int KEY_c = 99;
enum int KEY_c_h = 65187;
enum int KEY_cabovedot = 741;
enum int KEY_cacute = 486;
enum int KEY_careof = 2744;
enum int KEY_caret = 2812;
enum int KEY_caron = 439;
enum int KEY_ccaron = 488;
enum int KEY_ccedilla = 231;
enum int KEY_ccircumflex = 742;
enum int KEY_cedilla = 184;
enum int KEY_cent = 162;
enum int KEY_ch = 65184;
enum int KEY_checkerboard = 2529;
enum int KEY_checkmark = 2803;
enum int KEY_circle = 3023;
enum int KEY_club = 2796;
enum int KEY_colon = 58;
enum int KEY_comma = 44;
enum int KEY_containsas = 16785931;
enum int KEY_copyright = 169;
enum int KEY_cr = 2532;
enum int KEY_crossinglines = 2542;
enum int KEY_cuberoot = 16785947;
enum int KEY_currency = 164;
enum int KEY_cursor = 2815;
enum int KEY_d = 100;
enum int KEY_dabovedot = 16784907;
enum int KEY_dagger = 2801;
enum int KEY_dcaron = 495;
enum int KEY_dead_A = 65153;
enum int KEY_dead_E = 65155;
enum int KEY_dead_I = 65157;
enum int KEY_dead_O = 65159;
enum int KEY_dead_U = 65161;
enum int KEY_dead_a = 65152;
enum int KEY_dead_abovecomma = 65124;
enum int KEY_dead_abovedot = 65110;
enum int KEY_dead_abovereversedcomma = 65125;
enum int KEY_dead_abovering = 65112;
enum int KEY_dead_acute = 65105;
enum int KEY_dead_belowbreve = 65131;
enum int KEY_dead_belowcircumflex = 65129;
enum int KEY_dead_belowcomma = 65134;
enum int KEY_dead_belowdiaeresis = 65132;
enum int KEY_dead_belowdot = 65120;
enum int KEY_dead_belowmacron = 65128;
enum int KEY_dead_belowring = 65127;
enum int KEY_dead_belowtilde = 65130;
enum int KEY_dead_breve = 65109;
enum int KEY_dead_capital_schwa = 65163;
enum int KEY_dead_caron = 65114;
enum int KEY_dead_cedilla = 65115;
enum int KEY_dead_circumflex = 65106;
enum int KEY_dead_currency = 65135;
enum int KEY_dead_dasia = 65125;
enum int KEY_dead_diaeresis = 65111;
enum int KEY_dead_doubleacute = 65113;
enum int KEY_dead_doublegrave = 65126;
enum int KEY_dead_e = 65154;
enum int KEY_dead_grave = 65104;
enum int KEY_dead_hook = 65121;
enum int KEY_dead_horn = 65122;
enum int KEY_dead_i = 65156;
enum int KEY_dead_invertedbreve = 65133;
enum int KEY_dead_iota = 65117;
enum int KEY_dead_macron = 65108;
enum int KEY_dead_o = 65158;
enum int KEY_dead_ogonek = 65116;
enum int KEY_dead_perispomeni = 65107;
enum int KEY_dead_psili = 65124;
enum int KEY_dead_semivoiced_sound = 65119;
enum int KEY_dead_small_schwa = 65162;
enum int KEY_dead_stroke = 65123;
enum int KEY_dead_tilde = 65107;
enum int KEY_dead_u = 65160;
enum int KEY_dead_voiced_sound = 65118;
enum int KEY_decimalpoint = 2749;
enum int KEY_degree = 176;
enum int KEY_diaeresis = 168;
enum int KEY_diamond = 2797;
enum int KEY_digitspace = 2725;
enum int KEY_dintegral = 16785964;
enum int KEY_division = 247;
enum int KEY_dollar = 36;
enum int KEY_doubbaselinedot = 2735;
enum int KEY_doubleacute = 445;
enum int KEY_doubledagger = 2802;
enum int KEY_doublelowquotemark = 2814;
enum int KEY_downarrow = 2302;
enum int KEY_downcaret = 2984;
enum int KEY_downshoe = 3030;
enum int KEY_downstile = 3012;
enum int KEY_downtack = 3010;
enum int KEY_dstroke = 496;
enum int KEY_e = 101;
enum int KEY_eabovedot = 1004;
enum int KEY_eacute = 233;
enum int KEY_ebelowdot = 16785081;
enum int KEY_ecaron = 492;
enum int KEY_ecircumflex = 234;
enum int KEY_ecircumflexacute = 16785087;
enum int KEY_ecircumflexbelowdot = 16785095;
enum int KEY_ecircumflexgrave = 16785089;
enum int KEY_ecircumflexhook = 16785091;
enum int KEY_ecircumflextilde = 16785093;
enum int KEY_ediaeresis = 235;
enum int KEY_egrave = 232;
enum int KEY_ehook = 16785083;
enum int KEY_eightsubscript = 16785544;
enum int KEY_eightsuperior = 16785528;
enum int KEY_elementof = 16785928;
enum int KEY_ellipsis = 2734;
enum int KEY_em3space = 2723;
enum int KEY_em4space = 2724;
enum int KEY_emacron = 954;
enum int KEY_emdash = 2729;
enum int KEY_emfilledcircle = 2782;
enum int KEY_emfilledrect = 2783;
enum int KEY_emopencircle = 2766;
enum int KEY_emopenrectangle = 2767;
enum int KEY_emptyset = 16785925;
enum int KEY_emspace = 2721;
enum int KEY_endash = 2730;
enum int KEY_enfilledcircbullet = 2790;
enum int KEY_enfilledsqbullet = 2791;
enum int KEY_eng = 959;
enum int KEY_enopencircbullet = 2784;
enum int KEY_enopensquarebullet = 2785;
enum int KEY_enspace = 2722;
enum int KEY_eogonek = 490;
enum int KEY_equal = 61;
enum int KEY_eth = 240;
enum int KEY_etilde = 16785085;
enum int KEY_exclam = 33;
enum int KEY_exclamdown = 161;
enum int KEY_f = 102;
enum int KEY_fabovedot = 16784927;
enum int KEY_femalesymbol = 2808;
enum int KEY_ff = 2531;
enum int KEY_figdash = 2747;
enum int KEY_filledlefttribullet = 2780;
enum int KEY_filledrectbullet = 2779;
enum int KEY_filledrighttribullet = 2781;
enum int KEY_filledtribulletdown = 2793;
enum int KEY_filledtribulletup = 2792;
enum int KEY_fiveeighths = 2757;
enum int KEY_fivesixths = 2743;
enum int KEY_fivesubscript = 16785541;
enum int KEY_fivesuperior = 16785525;
enum int KEY_fourfifths = 2741;
enum int KEY_foursubscript = 16785540;
enum int KEY_foursuperior = 16785524;
enum int KEY_fourthroot = 16785948;
enum int KEY_function = 2294;
enum int KEY_g = 103;
enum int KEY_gabovedot = 757;
enum int KEY_gbreve = 699;
enum int KEY_gcaron = 16777703;
enum int KEY_gcedilla = 955;
enum int KEY_gcircumflex = 760;
enum int KEY_grave = 96;
enum int KEY_greater = 62;
enum int KEY_greaterthanequal = 2238;
enum int KEY_guillemotleft = 171;
enum int KEY_guillemotright = 187;
enum int KEY_h = 104;
enum int KEY_hairspace = 2728;
enum int KEY_hcircumflex = 694;
enum int KEY_heart = 2798;
enum int KEY_hebrew_aleph = 3296;
enum int KEY_hebrew_ayin = 3314;
enum int KEY_hebrew_bet = 3297;
enum int KEY_hebrew_beth = 3297;
enum int KEY_hebrew_chet = 3303;
enum int KEY_hebrew_dalet = 3299;
enum int KEY_hebrew_daleth = 3299;
enum int KEY_hebrew_doublelowline = 3295;
enum int KEY_hebrew_finalkaph = 3306;
enum int KEY_hebrew_finalmem = 3309;
enum int KEY_hebrew_finalnun = 3311;
enum int KEY_hebrew_finalpe = 3315;
enum int KEY_hebrew_finalzade = 3317;
enum int KEY_hebrew_finalzadi = 3317;
enum int KEY_hebrew_gimel = 3298;
enum int KEY_hebrew_gimmel = 3298;
enum int KEY_hebrew_he = 3300;
enum int KEY_hebrew_het = 3303;
enum int KEY_hebrew_kaph = 3307;
enum int KEY_hebrew_kuf = 3319;
enum int KEY_hebrew_lamed = 3308;
enum int KEY_hebrew_mem = 3310;
enum int KEY_hebrew_nun = 3312;
enum int KEY_hebrew_pe = 3316;
enum int KEY_hebrew_qoph = 3319;
enum int KEY_hebrew_resh = 3320;
enum int KEY_hebrew_samech = 3313;
enum int KEY_hebrew_samekh = 3313;
enum int KEY_hebrew_shin = 3321;
enum int KEY_hebrew_taf = 3322;
enum int KEY_hebrew_taw = 3322;
enum int KEY_hebrew_tet = 3304;
enum int KEY_hebrew_teth = 3304;
enum int KEY_hebrew_waw = 3301;
enum int KEY_hebrew_yod = 3305;
enum int KEY_hebrew_zade = 3318;
enum int KEY_hebrew_zadi = 3318;
enum int KEY_hebrew_zain = 3302;
enum int KEY_hebrew_zayin = 3302;
enum int KEY_hexagram = 2778;
enum int KEY_horizconnector = 2211;
enum int KEY_horizlinescan1 = 2543;
enum int KEY_horizlinescan3 = 2544;
enum int KEY_horizlinescan5 = 2545;
enum int KEY_horizlinescan7 = 2546;
enum int KEY_horizlinescan9 = 2547;
enum int KEY_hstroke = 689;
enum int KEY_ht = 2530;
enum int KEY_hyphen = 173;
enum int KEY_i = 105;
enum int KEY_iTouch = 269025120;
enum int KEY_iacute = 237;
enum int KEY_ibelowdot = 16785099;
enum int KEY_ibreve = 16777517;
enum int KEY_icircumflex = 238;
enum int KEY_identical = 2255;
enum int KEY_idiaeresis = 239;
enum int KEY_idotless = 697;
enum int KEY_ifonlyif = 2253;
enum int KEY_igrave = 236;
enum int KEY_ihook = 16785097;
enum int KEY_imacron = 1007;
enum int KEY_implies = 2254;
enum int KEY_includedin = 2266;
enum int KEY_includes = 2267;
enum int KEY_infinity = 2242;
enum int KEY_integral = 2239;
enum int KEY_intersection = 2268;
enum int KEY_iogonek = 999;
enum int KEY_itilde = 949;
enum int KEY_j = 106;
enum int KEY_jcircumflex = 700;
enum int KEY_jot = 3018;
enum int KEY_k = 107;
enum int KEY_kana_A = 1201;
enum int KEY_kana_CHI = 1217;
enum int KEY_kana_E = 1204;
enum int KEY_kana_FU = 1228;
enum int KEY_kana_HA = 1226;
enum int KEY_kana_HE = 1229;
enum int KEY_kana_HI = 1227;
enum int KEY_kana_HO = 1230;
enum int KEY_kana_HU = 1228;
enum int KEY_kana_I = 1202;
enum int KEY_kana_KA = 1206;
enum int KEY_kana_KE = 1209;
enum int KEY_kana_KI = 1207;
enum int KEY_kana_KO = 1210;
enum int KEY_kana_KU = 1208;
enum int KEY_kana_MA = 1231;
enum int KEY_kana_ME = 1234;
enum int KEY_kana_MI = 1232;
enum int KEY_kana_MO = 1235;
enum int KEY_kana_MU = 1233;
enum int KEY_kana_N = 1245;
enum int KEY_kana_NA = 1221;
enum int KEY_kana_NE = 1224;
enum int KEY_kana_NI = 1222;
enum int KEY_kana_NO = 1225;
enum int KEY_kana_NU = 1223;
enum int KEY_kana_O = 1205;
enum int KEY_kana_RA = 1239;
enum int KEY_kana_RE = 1242;
enum int KEY_kana_RI = 1240;
enum int KEY_kana_RO = 1243;
enum int KEY_kana_RU = 1241;
enum int KEY_kana_SA = 1211;
enum int KEY_kana_SE = 1214;
enum int KEY_kana_SHI = 1212;
enum int KEY_kana_SO = 1215;
enum int KEY_kana_SU = 1213;
enum int KEY_kana_TA = 1216;
enum int KEY_kana_TE = 1219;
enum int KEY_kana_TI = 1217;
enum int KEY_kana_TO = 1220;
enum int KEY_kana_TSU = 1218;
enum int KEY_kana_TU = 1218;
enum int KEY_kana_U = 1203;
enum int KEY_kana_WA = 1244;
enum int KEY_kana_WO = 1190;
enum int KEY_kana_YA = 1236;
enum int KEY_kana_YO = 1238;
enum int KEY_kana_YU = 1237;
enum int KEY_kana_a = 1191;
enum int KEY_kana_closingbracket = 1187;
enum int KEY_kana_comma = 1188;
enum int KEY_kana_conjunctive = 1189;
enum int KEY_kana_e = 1194;
enum int KEY_kana_fullstop = 1185;
enum int KEY_kana_i = 1192;
enum int KEY_kana_middledot = 1189;
enum int KEY_kana_o = 1195;
enum int KEY_kana_openingbracket = 1186;
enum int KEY_kana_switch = 65406;
enum int KEY_kana_tsu = 1199;
enum int KEY_kana_tu = 1199;
enum int KEY_kana_u = 1193;
enum int KEY_kana_ya = 1196;
enum int KEY_kana_yo = 1198;
enum int KEY_kana_yu = 1197;
enum int KEY_kappa = 930;
enum int KEY_kcedilla = 1011;
enum int KEY_kra = 930;
enum int KEY_l = 108;
enum int KEY_lacute = 485;
enum int KEY_latincross = 2777;
enum int KEY_lbelowdot = 16784951;
enum int KEY_lcaron = 437;
enum int KEY_lcedilla = 950;
enum int KEY_leftanglebracket = 2748;
enum int KEY_leftarrow = 2299;
enum int KEY_leftcaret = 2979;
enum int KEY_leftdoublequotemark = 2770;
enum int KEY_leftmiddlecurlybrace = 2223;
enum int KEY_leftopentriangle = 2764;
enum int KEY_leftpointer = 2794;
enum int KEY_leftradical = 2209;
enum int KEY_leftshoe = 3034;
enum int KEY_leftsinglequotemark = 2768;
enum int KEY_leftt = 2548;
enum int KEY_lefttack = 3036;
enum int KEY_less = 60;
enum int KEY_lessthanequal = 2236;
enum int KEY_lf = 2533;
enum int KEY_logicaland = 2270;
enum int KEY_logicalor = 2271;
enum int KEY_lowleftcorner = 2541;
enum int KEY_lowrightcorner = 2538;
enum int KEY_lstroke = 435;
enum int KEY_m = 109;
enum int KEY_mabovedot = 16784961;
enum int KEY_macron = 175;
enum int KEY_malesymbol = 2807;
enum int KEY_maltesecross = 2800;
enum int KEY_marker = 2751;
enum int KEY_masculine = 186;
enum int KEY_minus = 45;
enum int KEY_minutes = 2774;
enum int KEY_mu = 181;
enum int KEY_multiply = 215;
enum int KEY_musicalflat = 2806;
enum int KEY_musicalsharp = 2805;
enum int KEY_n = 110;
enum int KEY_nabla = 2245;
enum int KEY_nacute = 497;
enum int KEY_ncaron = 498;
enum int KEY_ncedilla = 1009;
enum int KEY_ninesubscript = 16785545;
enum int KEY_ninesuperior = 16785529;
enum int KEY_nl = 2536;
enum int KEY_nobreakspace = 160;
enum int KEY_notapproxeq = 16785991;
enum int KEY_notelementof = 16785929;
enum int KEY_notequal = 2237;
enum int KEY_notidentical = 16786018;
enum int KEY_notsign = 172;
enum int KEY_ntilde = 241;
enum int KEY_numbersign = 35;
enum int KEY_numerosign = 1712;
enum int KEY_o = 111;
enum int KEY_oacute = 243;
enum int KEY_obarred = 16777845;
enum int KEY_obelowdot = 16785101;
enum int KEY_ocaron = 16777682;
enum int KEY_ocircumflex = 244;
enum int KEY_ocircumflexacute = 16785105;
enum int KEY_ocircumflexbelowdot = 16785113;
enum int KEY_ocircumflexgrave = 16785107;
enum int KEY_ocircumflexhook = 16785109;
enum int KEY_ocircumflextilde = 16785111;
enum int KEY_odiaeresis = 246;
enum int KEY_odoubleacute = 501;
enum int KEY_oe = 5053;
enum int KEY_ogonek = 434;
enum int KEY_ograve = 242;
enum int KEY_ohook = 16785103;
enum int KEY_ohorn = 16777633;
enum int KEY_ohornacute = 16785115;
enum int KEY_ohornbelowdot = 16785123;
enum int KEY_ohorngrave = 16785117;
enum int KEY_ohornhook = 16785119;
enum int KEY_ohorntilde = 16785121;
enum int KEY_omacron = 1010;
enum int KEY_oneeighth = 2755;
enum int KEY_onefifth = 2738;
enum int KEY_onehalf = 189;
enum int KEY_onequarter = 188;
enum int KEY_onesixth = 2742;
enum int KEY_onesubscript = 16785537;
enum int KEY_onesuperior = 185;
enum int KEY_onethird = 2736;
enum int KEY_ooblique = 248;
enum int KEY_openrectbullet = 2786;
enum int KEY_openstar = 2789;
enum int KEY_opentribulletdown = 2788;
enum int KEY_opentribulletup = 2787;
enum int KEY_ordfeminine = 170;
enum int KEY_oslash = 248;
enum int KEY_otilde = 245;
enum int KEY_overbar = 3008;
enum int KEY_overline = 1150;
enum int KEY_p = 112;
enum int KEY_pabovedot = 16784983;
enum int KEY_paragraph = 182;
enum int KEY_parenleft = 40;
enum int KEY_parenright = 41;
enum int KEY_partdifferential = 16785922;
enum int KEY_partialderivative = 2287;
enum int KEY_percent = 37;
enum int KEY_period = 46;
enum int KEY_periodcentered = 183;
enum int KEY_phonographcopyright = 2811;
enum int KEY_plus = 43;
enum int KEY_plusminus = 177;
enum int KEY_prescription = 2772;
enum int KEY_prolongedsound = 1200;
enum int KEY_punctspace = 2726;
enum int KEY_q = 113;
enum int KEY_quad = 3020;
enum int KEY_question = 63;
enum int KEY_questiondown = 191;
enum int KEY_quotedbl = 34;
enum int KEY_quoteleft = 96;
enum int KEY_quoteright = 39;
enum int KEY_r = 114;
enum int KEY_racute = 480;
enum int KEY_radical = 2262;
enum int KEY_rcaron = 504;
enum int KEY_rcedilla = 947;
enum int KEY_registered = 174;
enum int KEY_rightanglebracket = 2750;
enum int KEY_rightarrow = 2301;
enum int KEY_rightcaret = 2982;
enum int KEY_rightdoublequotemark = 2771;
enum int KEY_rightmiddlecurlybrace = 2224;
enum int KEY_rightmiddlesummation = 2231;
enum int KEY_rightopentriangle = 2765;
enum int KEY_rightpointer = 2795;
enum int KEY_rightshoe = 3032;
enum int KEY_rightsinglequotemark = 2769;
enum int KEY_rightt = 2549;
enum int KEY_righttack = 3068;
enum int KEY_s = 115;
enum int KEY_sabovedot = 16784993;
enum int KEY_sacute = 438;
enum int KEY_scaron = 441;
enum int KEY_scedilla = 442;
enum int KEY_schwa = 16777817;
enum int KEY_scircumflex = 766;
enum int KEY_script_switch = 65406;
enum int KEY_seconds = 2775;
enum int KEY_section = 167;
enum int KEY_semicolon = 59;
enum int KEY_semivoicedsound = 1247;
enum int KEY_seveneighths = 2758;
enum int KEY_sevensubscript = 16785543;
enum int KEY_sevensuperior = 16785527;
enum int KEY_signaturemark = 2762;
enum int KEY_signifblank = 2732;
enum int KEY_similarequal = 2249;
enum int KEY_singlelowquotemark = 2813;
enum int KEY_sixsubscript = 16785542;
enum int KEY_sixsuperior = 16785526;
enum int KEY_slash = 47;
enum int KEY_soliddiamond = 2528;
enum int KEY_space = 32;
enum int KEY_squareroot = 16785946;
enum int KEY_ssharp = 223;
enum int KEY_sterling = 163;
enum int KEY_stricteq = 16786019;
enum int KEY_t = 116;
enum int KEY_tabovedot = 16785003;
enum int KEY_tcaron = 443;
enum int KEY_tcedilla = 510;
enum int KEY_telephone = 2809;
enum int KEY_telephonerecorder = 2810;
enum int KEY_therefore = 2240;
enum int KEY_thinspace = 2727;
enum int KEY_thorn = 254;
enum int KEY_threeeighths = 2756;
enum int KEY_threefifths = 2740;
enum int KEY_threequarters = 190;
enum int KEY_threesubscript = 16785539;
enum int KEY_threesuperior = 179;
enum int KEY_tintegral = 16785965;
enum int KEY_topintegral = 2212;
enum int KEY_topleftparens = 2219;
enum int KEY_topleftradical = 2210;
enum int KEY_topleftsqbracket = 2215;
enum int KEY_topleftsummation = 2225;
enum int KEY_toprightparens = 2221;
enum int KEY_toprightsqbracket = 2217;
enum int KEY_toprightsummation = 2229;
enum int KEY_topt = 2551;
enum int KEY_topvertsummationconnector = 2227;
enum int KEY_trademark = 2761;
enum int KEY_trademarkincircle = 2763;
enum int KEY_tslash = 956;
enum int KEY_twofifths = 2739;
enum int KEY_twosubscript = 16785538;
enum int KEY_twosuperior = 178;
enum int KEY_twothirds = 2737;
enum int KEY_u = 117;
enum int KEY_uacute = 250;
enum int KEY_ubelowdot = 16785125;
enum int KEY_ubreve = 765;
enum int KEY_ucircumflex = 251;
enum int KEY_udiaeresis = 252;
enum int KEY_udoubleacute = 507;
enum int KEY_ugrave = 249;
enum int KEY_uhook = 16785127;
enum int KEY_uhorn = 16777648;
enum int KEY_uhornacute = 16785129;
enum int KEY_uhornbelowdot = 16785137;
enum int KEY_uhorngrave = 16785131;
enum int KEY_uhornhook = 16785133;
enum int KEY_uhorntilde = 16785135;
enum int KEY_umacron = 1022;
enum int KEY_underbar = 3014;
enum int KEY_underscore = 95;
enum int KEY_union = 2269;
enum int KEY_uogonek = 1017;
enum int KEY_uparrow = 2300;
enum int KEY_upcaret = 2985;
enum int KEY_upleftcorner = 2540;
enum int KEY_uprightcorner = 2539;
enum int KEY_upshoe = 3011;
enum int KEY_upstile = 3027;
enum int KEY_uptack = 3022;
enum int KEY_uring = 505;
enum int KEY_utilde = 1021;
enum int KEY_v = 118;
enum int KEY_variation = 2241;
enum int KEY_vertbar = 2552;
enum int KEY_vertconnector = 2214;
enum int KEY_voicedsound = 1246;
enum int KEY_vt = 2537;
enum int KEY_w = 119;
enum int KEY_wacute = 16785027;
enum int KEY_wcircumflex = 16777589;
enum int KEY_wdiaeresis = 16785029;
enum int KEY_wgrave = 16785025;
enum int KEY_x = 120;
enum int KEY_xabovedot = 16785035;
enum int KEY_y = 121;
enum int KEY_yacute = 253;
enum int KEY_ybelowdot = 16785141;
enum int KEY_ycircumflex = 16777591;
enum int KEY_ydiaeresis = 255;
enum int KEY_yen = 165;
enum int KEY_ygrave = 16785139;
enum int KEY_yhook = 16785143;
enum int KEY_ytilde = 16785145;
enum int KEY_z = 122;
enum int KEY_zabovedot = 447;
enum int KEY_zacute = 444;
enum int KEY_zcaron = 446;
enum int KEY_zerosubscript = 16785536;
enum int KEY_zerosuperior = 16785520;
enum int KEY_zstroke = 16777654;
enum int KP_0 = 65456;
enum int KP_1 = 65457;
enum int KP_2 = 65458;
enum int KP_3 = 65459;
enum int KP_4 = 65460;
enum int KP_5 = 65461;
enum int KP_6 = 65462;
enum int KP_7 = 65463;
enum int KP_8 = 65464;
enum int KP_9 = 65465;
enum int KP_Add = 65451;
enum int KP_Begin = 65437;
enum int KP_Decimal = 65454;
enum int KP_Delete = 65439;
enum int KP_Divide = 65455;
enum int KP_Down = 65433;
enum int KP_End = 65436;
enum int KP_Enter = 65421;
enum int KP_Equal = 65469;
enum int KP_F1 = 65425;
enum int KP_F2 = 65426;
enum int KP_F3 = 65427;
enum int KP_F4 = 65428;
enum int KP_Home = 65429;
enum int KP_Insert = 65438;
enum int KP_Left = 65430;
enum int KP_Multiply = 65450;
enum int KP_Next = 65435;
enum int KP_Page_Down = 65435;
enum int KP_Page_Up = 65434;
enum int KP_Prior = 65434;
enum int KP_Right = 65432;
enum int KP_Separator = 65452;
enum int KP_Space = 65408;
enum int KP_Subtract = 65453;
enum int KP_Tab = 65417;
enum int KP_Up = 65431;
enum int Kana_Lock = 65325;
enum int Kana_Shift = 65326;
enum int Kanji = 65313;
enum int Kanji_Bangou = 65335;
enum int Katakana = 65318;
enum int KbdBrightnessDown = 269025030;
enum int KbdBrightnessUp = 269025029;
enum int KbdLightOnOff = 269025028;
enum int Kcedilla = 979;
// Key event
struct KeyEvent /* Version 0.2 */ {
   EventType type;
   uint time;
   EventFlags flags;
   Stage* stage;
   Actor* source;
   ModifierType modifier_state;
   uint keyval;
   ushort hardware_keycode;
   dchar unicode_value;
   InputDevice* device;
}

// Point in a path behaviour.
struct Knot /* Version 0.2 */ {
   int x, y;


   // VERSION: 0.2
   // Makes an allocated copy of a knot.
   // RETURNS: the copied knot.
   Knot* /*new*/ copy()() nothrow {
      return clutter_knot_copy(&this);
   }

   // VERSION: 0.2
   // Compares to knot and checks if the point to the same location.
   // RETURNS: %TRUE if the knots point to the same location.
   // <knot_b>: Second knot
   int equal(AT0)(AT0 /*Knot*/ knot_b) nothrow {
      return clutter_knot_equal(&this, UpCast!(Knot*)(knot_b));
   }

   // VERSION: 0.2
   // Frees the memory of an allocated knot.
   void free()() nothrow {
      clutter_knot_free(&this);
   }
}

enum int Korean_Won = 3839;
enum int L = 76;
enum int L1 = 65480;
enum int L10 = 65489;
enum int L2 = 65481;
enum int L3 = 65482;
enum int L4 = 65483;
enum int L5 = 65484;
enum int L6 = 65485;
enum int L7 = 65486;
enum int L8 = 65487;
enum int L9 = 65488;
enum int Lacute = 453;
enum int Last_Virtual_Screen = 65236;
enum int Launch0 = 269025088;
enum int Launch1 = 269025089;
enum int Launch2 = 269025090;
enum int Launch3 = 269025091;
enum int Launch4 = 269025092;
enum int Launch5 = 269025093;
enum int Launch6 = 269025094;
enum int Launch7 = 269025095;
enum int Launch8 = 269025096;
enum int Launch9 = 269025097;
enum int LaunchA = 269025098;
enum int LaunchB = 269025099;
enum int LaunchC = 269025100;
enum int LaunchD = 269025101;
enum int LaunchE = 269025102;
enum int LaunchF = 269025103;

// The #ClutterLayoutManager structure contains only private data
// and should be accessed using the provided API
struct LayoutManager /* : GObject.InitiallyUnowned */ /* Version 1.2 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance initiallyunowned;
   GObject2.InitiallyUnowned parent_instance;
   private void* dummy;


   // VERSION: 1.2
   // Allocates the children of @container given an area
   // 
   // See also clutter_actor_allocate()
   // <container>: the #ClutterContainer using @manager
   // <allocation>: the #ClutterActorBox containing the allocated area of @container
   // <flags>: the allocation flags
   void allocate(AT0, AT1)(AT0 /*Container*/ container, AT1 /*ActorBox*/ allocation, AllocationFlags flags) nothrow {
      clutter_layout_manager_allocate(&this, UpCast!(Container*)(container), UpCast!(ActorBox*)(allocation), flags);
   }

   // VERSION: 1.2
   // Begins an animation of @duration milliseconds, using the provided
   // easing @mode
   // 
   // The easing mode can be specified either as a #ClutterAnimationMode
   // or as a logical id returned by clutter_alpha_register_func()
   // 
   // The result of this function depends on the @manager implementation
   // 
   // layout manager; the returned instance is owned by the layout
   // manager and should not be unreferenced
   // RETURNS: The #ClutterAlpha created by the
   // <duration>: the duration of the animation, in milliseconds
   // <mode>: the easing mode of the animation
   Alpha* begin_animation()(uint duration, c_ulong mode) nothrow {
      return clutter_layout_manager_begin_animation(&this, duration, mode);
   }

   // Unintrospectable method: child_get() / clutter_layout_manager_child_get()
   // VERSION: 1.2
   // Retrieves the values for a list of properties out of the
   // #ClutterLayoutMeta created by @manager and attached to the
   // child of a @container
   // <container>: a #ClutterContainer using @manager
   // <actor>: a #ClutterActor child of @container
   // <first_property>: the name of the first property
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_layout_manager_child_get child_get; // Variadic
   +/

   // VERSION: 1.2
   // Gets a property on the #ClutterLayoutMeta created by @manager and
   // attached to a child of @container
   // 
   // The #GValue must already be initialized to the type of the property
   // and has to be unset with g_value_unset() after extracting the real
   // value out of it
   // <container>: a #ClutterContainer using @manager
   // <actor>: a #ClutterActor child of @container
   // <property_name>: the name of the property to get
   // <value>: a #GValue with the value of the property to get
   void child_get_property(AT0, AT1, AT2, AT3)(AT0 /*Container*/ container, AT1 /*Actor*/ actor, AT2 /*char*/ property_name, AT3 /*GObject2.Value*/ value) nothrow {
      clutter_layout_manager_child_get_property(&this, UpCast!(Container*)(container), UpCast!(Actor*)(actor), toCString!(char*)(property_name), UpCast!(GObject2.Value*)(value));
   }

   // Unintrospectable method: child_set() / clutter_layout_manager_child_set()
   // VERSION: 1.2
   // Sets a list of properties and their values on the #ClutterLayoutMeta
   // associated by @manager to a child of @container
   // 
   // Languages bindings should use clutter_layout_manager_child_set_property()
   // instead
   // <container>: a #ClutterContainer using @manager
   // <actor>: a #ClutterActor child of @container
   // <first_property>: the first property name
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_layout_manager_child_set child_set; // Variadic
   +/

   // VERSION: 1.2
   // Sets a property on the #ClutterLayoutMeta created by @manager and
   // attached to a child of @container
   // <container>: a #ClutterContainer using @manager
   // <actor>: a #ClutterActor child of @container
   // <property_name>: the name of the property to set
   // <value>: a #GValue with the value of the property to set
   void child_set_property(AT0, AT1, AT2, AT3)(AT0 /*Container*/ container, AT1 /*Actor*/ actor, AT2 /*char*/ property_name, AT3 /*GObject2.Value*/ value) nothrow {
      clutter_layout_manager_child_set_property(&this, UpCast!(Container*)(container), UpCast!(Actor*)(actor), toCString!(char*)(property_name), UpCast!(GObject2.Value*)(value));
   }

   // VERSION: 1.2
   // Ends an animation started by clutter_layout_manager_begin_animation()
   // 
   // The result of this call depends on the @manager implementation
   void end_animation()() nothrow {
      clutter_layout_manager_end_animation(&this);
   }

   // VERSION: 1.2
   // Retrieves the #GParamSpec for the layout property @name inside
   // the #ClutterLayoutMeta sub-class used by @manager
   // 
   // or %NULL if no property with that name exists. The returned
   // #GParamSpec is owned by the layout manager and should not be
   // modified or freed
   // RETURNS: a #GParamSpec describing the property,
   // <name>: the name of the property
   GObject2.ParamSpec* find_child_property(AT0)(AT0 /*char*/ name) nothrow {
      return clutter_layout_manager_find_child_property(&this, toCString!(char*)(name));
   }

   // VERSION: 1.2
   // Retrieves the progress of the animation, if one has been started by
   // clutter_layout_manager_begin_animation()
   // 
   // The returned value has the same semantics of the #ClutterAlpha:alpha
   // value
   // RETURNS: the progress of the animation
   double get_animation_progress()() nothrow {
      return clutter_layout_manager_get_animation_progress(&this);
   }

   // VERSION: 1.0
   // Retrieves the #ClutterLayoutMeta that the layout @manager associated
   // to the @actor child of @container, eventually by creating one if the
   // #ClutterLayoutManager supports layout properties
   // 
   // #ClutterLayoutManager does not have layout properties. The returned
   // layout meta instance is owned by the #ClutterLayoutManager and it
   // should not be unreferenced
   // RETURNS: a #ClutterLayoutMeta, or %NULL if the
   // <container>: a #ClutterContainer using @manager
   // <actor>: a #ClutterActor child of @container
   LayoutMeta* get_child_meta(AT0, AT1)(AT0 /*Container*/ container, AT1 /*Actor*/ actor) nothrow {
      return clutter_layout_manager_get_child_meta(&this, UpCast!(Container*)(container), UpCast!(Actor*)(actor));
   }

   // VERSION: 1.2
   // Computes the minimum and natural heights of the @container according
   // to @manager.
   // 
   // See also clutter_actor_get_preferred_height()
   // <container>: the #ClutterContainer using @manager
   // <for_width>: the width for which the height should be computed, or -1
   // <min_height_p>: return location for the minimum height of the layout, or %NULL
   // <nat_height_p>: return location for the natural height of the layout, or %NULL
   void get_preferred_height(AT0, AT1, AT2)(AT0 /*Container*/ container, float for_width, /*out*/ AT1 /*float*/ min_height_p=null, /*out*/ AT2 /*float*/ nat_height_p=null) nothrow {
      clutter_layout_manager_get_preferred_height(&this, UpCast!(Container*)(container), for_width, UpCast!(float*)(min_height_p), UpCast!(float*)(nat_height_p));
   }

   // VERSION: 1.2
   // Computes the minimum and natural widths of the @container according
   // to @manager.
   // 
   // See also clutter_actor_get_preferred_width()
   // <container>: the #ClutterContainer using @manager
   // <for_height>: the height for which the width should be computed, or -1
   // <min_width_p>: return location for the minimum width of the layout, or %NULL
   // <nat_width_p>: return location for the natural width of the layout, or %NULL
   void get_preferred_width(AT0, AT1, AT2)(AT0 /*Container*/ container, float for_height, /*out*/ AT1 /*float*/ min_width_p=null, /*out*/ AT2 /*float*/ nat_width_p=null) nothrow {
      clutter_layout_manager_get_preferred_width(&this, UpCast!(Container*)(container), for_height, UpCast!(float*)(min_width_p), UpCast!(float*)(nat_width_p));
   }

   // VERSION: 1.2
   // Emits the #ClutterLayoutManager::layout-changed signal on @manager
   // 
   // This function should only be called by implementations of the
   // #ClutterLayoutManager class
   void layout_changed()() nothrow {
      clutter_layout_manager_layout_changed(&this);
   }

   // VERSION: 1.2
   // Retrieves all the #GParamSpec<!-- -->s for the layout properties
   // stored inside the #ClutterLayoutMeta sub-class used by @manager
   // 
   // %NULL-terminated array of #GParamSpec<!-- -->s. Use g_free() to free the
   // resources allocated for the array
   // RETURNS: the newly-allocated,
   // <n_pspecs>: return location for the number of returned #GParamSpec<!-- -->s
   GObject2.ParamSpec** /*new*/ list_child_properties(AT0)(/*out*/ AT0 /*uint*/ n_pspecs) nothrow {
      return clutter_layout_manager_list_child_properties(&this, UpCast!(uint*)(n_pspecs));
   }

   // VERSION: 1.2
   // If the #ClutterLayoutManager sub-class allows it, allow
   // adding a weak reference of the @container using @manager
   // from within the layout manager
   // 
   // The layout manager should not increase the reference
   // count of the @container
   // <container>: a #ClutterContainer using @manager
   void set_container(AT0)(AT0 /*Container*/ container=null) nothrow {
      clutter_layout_manager_set_container(&this, UpCast!(Container*)(container));
   }

   // VERSION: 1.2
   // The ::layout-changed signal is emitted each time a layout manager
   // has been changed. Every #ClutterActor using the @manager instance
   // as a layout manager should connect a handler to the ::layout-changed
   // signal and queue a relayout on themselves:
   // 
   // |[
   // static void layout_changed (ClutterLayoutManager *manager,
   // ClutterActor         *self)
   // {
   // clutter_actor_queue_relayout (self);
   // }
   // ...
   // self->manager = g_object_ref_sink (manager);
   // g_signal_connect (self->manager, "layout-changed",
   // G_CALLBACK (layout_changed),
   // self);
   // ]|
   // 
   // Sub-classes of #ClutterLayoutManager that implement a layout that
   // can be controlled or changed using parameters should emit the
   // ::layout-changed signal whenever one of the parameters changes,
   // by using clutter_layout_manager_layout_changed().
   extern (C) alias static void function (LayoutManager* this_, void* user_data=null) nothrow signal_layout_changed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"layout-changed", CB/*:signal_layout_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_layout_changed)||_ttmm!(CB, signal_layout_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"layout-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}


// The #ClutterLayoutManagerClass structure contains only private
// data and should be accessed using the provided API
struct LayoutManagerClass /* Version 1.2 */ {
   private GObject2.InitiallyUnownedClass parent_class;

   // <container>: the #ClutterContainer using @manager
   // <for_height>: the height for which the width should be computed, or -1
   // <min_width_p>: return location for the minimum width of the layout, or %NULL
   // <nat_width_p>: return location for the natural width of the layout, or %NULL
   extern (C) void function (LayoutManager* manager, Container* container, float for_height, /*out*/ float* min_width_p=null, /*out*/ float* nat_width_p=null) nothrow get_preferred_width;

   // <container>: the #ClutterContainer using @manager
   // <for_width>: the width for which the height should be computed, or -1
   // <min_height_p>: return location for the minimum height of the layout, or %NULL
   // <nat_height_p>: return location for the natural height of the layout, or %NULL
   extern (C) void function (LayoutManager* manager, Container* container, float for_width, /*out*/ float* min_height_p=null, /*out*/ float* nat_height_p=null) nothrow get_preferred_height;

   // <container>: the #ClutterContainer using @manager
   // <allocation>: the #ClutterActorBox containing the allocated area of @container
   // <flags>: the allocation flags
   extern (C) void function (LayoutManager* manager, Container* container, ActorBox* allocation, AllocationFlags flags) nothrow allocate;
   // <container>: a #ClutterContainer using @manager
   extern (C) void function (LayoutManager* manager, Container* container=null) nothrow set_container;
   extern (C) Type function (LayoutManager* manager) nothrow get_child_meta_type;
   // Unintrospectable functionp: create_child_meta() / ()
   extern (C) LayoutMeta* function (LayoutManager* manager, Container* container, Actor* actor) nothrow create_child_meta;

   // RETURNS: The #ClutterAlpha created by the
   // <duration>: the duration of the animation, in milliseconds
   // <mode>: the easing mode of the animation
   extern (C) Alpha* function (LayoutManager* manager, uint duration, c_ulong mode) nothrow begin_animation;
   // RETURNS: the progress of the animation
   extern (C) double function (LayoutManager* manager) nothrow get_animation_progress;
   extern (C) void function (LayoutManager* manager) nothrow end_animation;
   extern (C) void function (LayoutManager* manager) nothrow layout_changed;
   extern (C) void function () nothrow _clutter_padding_1;
   extern (C) void function () nothrow _clutter_padding_2;
   extern (C) void function () nothrow _clutter_padding_3;
   extern (C) void function () nothrow _clutter_padding_4;
   extern (C) void function () nothrow _clutter_padding_5;
   extern (C) void function () nothrow _clutter_padding_6;
   extern (C) void function () nothrow _clutter_padding_7;
   extern (C) void function () nothrow _clutter_padding_8;
}


// Sub-class of #ClutterChildMeta specific for layout managers
// 
// A #ClutterLayoutManager sub-class should create a #ClutterLayoutMeta
// instance by overriding the #ClutterLayoutManager::create_child_meta()
// virtual function
struct LayoutMeta /* : ChildMeta */ /* Version 1.2 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance childmeta;
   ChildMeta parent_instance;
   LayoutManager* manager;
   private int dummy0;
   private void* dummy1;


   // VERSION: 1.2
   // Retrieves the actor wrapped by @data
   // RETURNS: a #ClutterLayoutManager
   LayoutManager* get_manager()() nothrow {
      return clutter_layout_meta_get_manager(&this);
   }
}


// The #ClutterLayoutMetaClass contains only private data and
// should never be accessed directly
struct LayoutMetaClass /* Version 1.2 */ {
   private ChildMetaClass parent_class;
   extern (C) void function () nothrow _clutter_padding1;
   extern (C) void function () nothrow _clutter_padding2;
   extern (C) void function () nothrow _clutter_padding3;
   extern (C) void function () nothrow _clutter_padding4;
}

enum int Lbelowdot = 16784950;
enum int Lcaron = 421;
enum int Lcedilla = 934;
enum int Left = 65361;
enum int LightBulb = 269025077;
enum int Linefeed = 65290;
enum int LiraSign = 16785572;
// The #ClutterListModel struct contains only private data.
struct ListModel /* : Model */ /* Version 0.6 */ {
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance model;
   Model parent_instance;
   private ListModelPrivate* priv;


   // Unintrospectable constructor: new() / clutter_list_model_new()
   // VERSION: 0.6
   // Creates a new default model with @n_columns columns with the types 
   // and names passed in.
   // 
   // For example:
   // 
   // <informalexample><programlisting>
   // model = clutter_list_model_new (3,
   // G_TYPE_INT,      "Score",
   // G_TYPE_STRING,   "Team",
   // GDK_TYPE_PIXBUF, "Logo");
   // </programlisting></informalexample>
   // 
   // will create a new #ClutterModel with three columns of type int,
   // string and #GdkPixbuf respectively.
   // 
   // Note that the name of the column can be set to %NULL, in which case
   // the canonical name of the type held by the column will be used as
   // the title.
   // RETURNS: a new #ClutterListModel
   // <n_columns>: number of columns in the model
   alias clutter_list_model_new new_; // Variadic

   // VERSION: 0.6
   // Non-vararg version of clutter_list_model_new(). This function is
   // useful for language bindings.
   // RETURNS: a new default #ClutterModel
   // <n_columns>: number of columns in the model
   // <types>: an array of #GType types for the columns, from first to last
   // <names>: an array of names for the columns, from first to last
   static ListModel* /*new*/ newv(AT0, AT1)(uint n_columns, AT0 /*Type*/ types, AT1 /*char*/ names) nothrow {
      return clutter_list_model_newv(n_columns, UpCast!(Type*)(types), toCString!(char*)(names));
   }
   static auto opCall(AT0, AT1)(uint n_columns, AT0 /*Type*/ types, AT1 /*char*/ names) {
      return clutter_list_model_newv(n_columns, UpCast!(Type*)(types), toCString!(char*)(names));
   }
}

// The #ClutterListModelClass struct contains only private data.
struct ListModelClass /* Version 0.6 */ {
   private ModelClass parent_class;
}

struct ListModelPrivate {
}

enum int LogGrabInfo = 269024805;
enum int LogOff = 269025121;
enum int LogWindowTree = 269024804;
// The states for the #ClutterClikAction::long-press signal.
enum LongPressState /* Version 1.8 */ {
   QUERY = 0,
   ACTIVATE = 1,
   CANCEL = 2
}
enum int Lstroke = 419;
enum int M = 77;
enum int MAJOR_VERSION = 1;
enum int MICRO_VERSION = 11;
enum int MINOR_VERSION = 9;
enum int Mabovedot = 16784960;
enum int Macedonia_DSE = 1717;
enum int Macedonia_GJE = 1714;
enum int Macedonia_KJE = 1724;
enum int Macedonia_dse = 1701;
enum int Macedonia_gje = 1698;
enum int Macedonia_kje = 1708;
enum int Mae_Koho = 65342;
enum int Mail = 269025049;
enum int MailForward = 269025168;
// A representation of the components of a margin.
struct Margin /* Version 1.10 */ {
   float left, right, top, bottom;


   // VERSION: 1.10
   // Creates a new #ClutterMargin.
   // 
   // clutter_margin_free() to free the resources associated with it when
   // done.
   // RETURNS: a newly allocated #ClutterMargin. Use
   static Margin* /*new*/ new_()() nothrow {
      return clutter_margin_new();
   }
   static auto opCall()() {
      return clutter_margin_new();
   }

   // VERSION: 1.10
   // Creates a new #ClutterMargin and copies the contents of @margin_ into
   // the newly created structure.
   // RETURNS: a copy of the #ClutterMargin.
   Margin* /*new*/ copy()() nothrow {
      return clutter_margin_copy(&this);
   }

   // VERSION: 1.10
   // Frees the resources allocated by clutter_margin_new() and
   // clutter_margin_copy().
   void free()() nothrow {
      clutter_margin_free(&this);
   }
}

enum int Market = 269025122;
enum int Massyo = 65324;

// #ClutterMedia is an opaque structure whose members cannot be directly
// accessed
struct Media /* Interface */ /* Version 0.2 */ {
   mixin template __interface__() {
      // VERSION: 1.0
      // Retrieves the playback volume of @media.
      // RETURNS: The playback volume between 0.0 and 1.0
      double get_audio_volume()() nothrow {
         return clutter_media_get_audio_volume(cast(Media*)&this);
      }

      // VERSION: 1.0
      // Retrieves the amount of the stream that is buffered.
      // RETURNS: the fill level, between 0.0 and 1.0
      double get_buffer_fill()() nothrow {
         return clutter_media_get_buffer_fill(cast(Media*)&this);
      }

      // VERSION: 0.2
      // Retrieves whether @media is seekable or not.
      // RETURNS: %TRUE if @media can seek, %FALSE otherwise.
      int get_can_seek()() nothrow {
         return clutter_media_get_can_seek(cast(Media*)&this);
      }

      // VERSION: 0.2
      // Retrieves the duration of the media stream that @media represents.
      // RETURNS: the duration of the media stream, in seconds
      double get_duration()() nothrow {
         return clutter_media_get_duration(cast(Media*)&this);
      }

      // VERSION: 0.2
      // Retrieves the playing status of @media.
      // RETURNS: %TRUE if playing, %FALSE if stopped.
      int get_playing()() nothrow {
         return clutter_media_get_playing(cast(Media*)&this);
      }

      // VERSION: 1.0
      // Retrieves the playback progress of @media.
      // RETURNS: the playback progress, between 0.0 and 1.0
      double get_progress()() nothrow {
         return clutter_media_get_progress(cast(Media*)&this);
      }

      // VERSION: 1.2
      // Retrieves the font name currently used.
      // 
      // to free the returned string
      // RETURNS: a string containing the font name. Use g_free()
      char* /*new*/ get_subtitle_font_name()() nothrow {
         return clutter_media_get_subtitle_font_name(cast(Media*)&this);
      }

      // VERSION: 1.2
      // Retrieves the URI of the subtitle file in use.
      // 
      // to free the returned string
      // RETURNS: the URI of the subtitle file. Use g_free()
      char* /*new*/ get_subtitle_uri()() nothrow {
         return clutter_media_get_subtitle_uri(cast(Media*)&this);
      }

      // VERSION: 0.2
      // Retrieves the URI from @media.
      // 
      // to free the returned string
      // RETURNS: the URI of the media stream. Use g_free()
      char* /*new*/ get_uri()() nothrow {
         return clutter_media_get_uri(cast(Media*)&this);
      }

      // VERSION: 1.0
      // Sets the playback volume of @media to @volume.
      // <volume>: the volume as a double between 0.0 and 1.0
      void set_audio_volume()(double volume) nothrow {
         clutter_media_set_audio_volume(cast(Media*)&this, volume);
      }

      // VERSION: 0.2
      // Sets the source of @media using a file path.
      // <filename>: A filename
      void set_filename(AT0)(AT0 /*char*/ filename) nothrow {
         clutter_media_set_filename(cast(Media*)&this, toCString!(char*)(filename));
      }

      // VERSION: 0.2
      // Starts or stops playing of @media. 
      // The implementation might be asynchronous, so the way to know whether
      // the actual playing state of the @media is to use the #GObject::notify
      // signal on the #ClutterMedia:playing property and then retrieve the
      // current state with clutter_media_get_playing(). ClutterGstVideoTexture
      // in clutter-gst is an example of such an asynchronous implementation.
      // <playing>: %TRUE to start playing
      void set_playing()(int playing) nothrow {
         clutter_media_set_playing(cast(Media*)&this, playing);
      }

      // VERSION: 1.0
      // Sets the playback progress of @media. The @progress is
      // a normalized value between 0.0 (begin) and 1.0 (end).
      // <progress>: the progress of the playback, between 0.0 and 1.0
      void set_progress()(double progress) nothrow {
         clutter_media_set_progress(cast(Media*)&this, progress);
      }

      // VERSION: 1.2
      // Sets the font used by the subtitle renderer. The @font_name string must be
      // either %NULL, which means that the default font name of the underlying
      // implementation will be used; or must follow the grammar recognized by
      // pango_font_description_from_string() like:
      // 
      // |[
      // clutter_media_set_subtitle_font_name (media, "Sans 24pt");
      // ]|
      // <font_name>: a font name, or %NULL to set the default font name
      void set_subtitle_font_name(AT0)(AT0 /*char*/ font_name) nothrow {
         clutter_media_set_subtitle_font_name(cast(Media*)&this, toCString!(char*)(font_name));
      }

      // VERSION: 1.2
      // Sets the location of a subtitle file to display while playing @media.
      // <uri>: the URI of a subtitle file
      void set_subtitle_uri(AT0)(AT0 /*char*/ uri) nothrow {
         clutter_media_set_subtitle_uri(cast(Media*)&this, toCString!(char*)(uri));
      }

      // VERSION: 0.2
      // Sets the URI of @media to @uri.
      // <uri>: the URI of the media stream
      void set_uri(AT0)(AT0 /*char*/ uri) nothrow {
         clutter_media_set_uri(cast(Media*)&this, toCString!(char*)(uri));
      }

      // VERSION: 0.2
      // The ::eos signal is emitted each time the media stream ends.
      extern (C) alias static void function (Media* this_, void* user_data=null) nothrow signal_eos;

      ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
         return super_.signal_connect!name(cb, data, cf);
      }

      ulong signal_connect(string name:"eos", CB/*:signal_eos*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_eos)||_ttmm!(CB, signal_eos)()) {
         return signal_connect_data!()(&this, cast(char*)"eos",
         cast(GObject2.Callback)cb, data, null, cf);
      }

      // VERSION: 0.2
      // The ::error signal is emitted each time an error occurred.
      // <error>: the #GError
      extern (C) alias static void function (Media* this_, GLib2.Error* error, void* user_data=null) nothrow signal_error;
      ulong signal_connect(string name:"error", CB/*:signal_error*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_error)||_ttmm!(CB, signal_error)()) {
         return signal_connect_data!()(&this, cast(char*)"error",
         cast(GObject2.Callback)cb, data, null, cf);
      }
   }
   mixin __interface__;
}

// Interface vtable for #ClutterMedia implementations
struct MediaIface /* Version 0.2 */ {
   private GObject2.TypeInterface base_iface;
   extern (C) void function (Media* media) nothrow eos;
   extern (C) void function (Media* media, GLib2.Error* error) nothrow error;
}

enum int Meeting = 269025123;
enum int Memo = 269025054;
enum int Menu = 65383;
enum int MenuKB = 269025125;
enum int MenuPB = 269025126;
enum int Messenger = 269025166;
enum int Meta_L = 65511;
enum int Meta_R = 65512;
enum int MillSign = 16785573;
enum int ModeLock = 269025025;
enum int Mode_switch = 65406;

// Base class for list models. The #ClutterModel structure contains
// only private data and should be manipulated using the provided
// API.
struct Model /* : GObject.Object */ /* Version 0.6 */ {
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private ModelPrivate* priv;


   // Unintrospectable method: append() / clutter_model_append()
   // VERSION: 0.6
   // Creates and appends a new row to the #ClutterModel, setting the
   // row values upon creation. For example, to append a new row where
   // column 0 is type %G_TYPE_INT and column 1 is of type %G_TYPE_STRING:
   // 
   // <informalexample><programlisting>
   // ClutterModel *model;
   // model = clutter_model_default_new (2,
   // G_TYPE_INT,    "Score",
   // G_TYPE_STRING, "Team");
   // clutter_model_append (model, 0, 42, 1, "Team #1", -1);
   // </programlisting></informalexample>
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_model_append append; // Variadic
   +/

   // VERSION: 0.6
   // Creates and appends a new row to the #ClutterModel, setting the row
   // values for the given @columns upon creation.
   // <n_columns>: the number of columns and values
   // <columns>: a vector with the columns to set
   // <values>: a vector with the values
   void appendv(AT0, AT1)(uint n_columns, AT0 /*uint*/ columns, AT1 /*GObject2.Value*/ values) nothrow {
      clutter_model_appendv(&this, n_columns, UpCast!(uint*)(columns), UpCast!(GObject2.Value*)(values));
   }

   // VERSION: 0.6
   // Checks whether the row pointer by @iter should be filtered or not using
   // the filtering function set on @model.
   // 
   // This function should be used only by subclasses of #ClutterModel.
   // 
   // %FALSE otherwise
   // RETURNS: %TRUE if the row should be displayed,
   // <iter>: the row to filter
   int filter_iter(AT0)(AT0 /*ModelIter*/ iter) nothrow {
      return clutter_model_filter_iter(&this, UpCast!(ModelIter*)(iter));
   }

   // VERSION: 0.6
   // Checks whether @row should be filtered or not using the
   // filtering function set on @model.
   // 
   // This function should be used only by subclasses of #ClutterModel.
   // 
   // %FALSE otherwise
   // RETURNS: %TRUE if the row should be displayed,
   // <row>: the row to filter
   int filter_row()(uint row) nothrow {
      return clutter_model_filter_row(&this, row);
   }

   // VERSION: 0.6
   // Calls @func for each row in the model.
   // <func>: a #ClutterModelForeachFunc
   // <user_data>: user data to pass to @func
   void foreach_(AT0)(ModelForeachFunc func, AT0 /*void*/ user_data) nothrow {
      clutter_model_foreach(&this, func, UpCast!(void*)(user_data));
   }

   // VERSION: 0.6
   // Retrieves the name of the @column
   // 
   // string, and it should not be modified or freed
   // RETURNS: the name of the column. The model holds the returned
   // <column>: the column number
   char* get_column_name()(uint column) nothrow {
      return clutter_model_get_column_name(&this, column);
   }

   // VERSION: 0.6
   // Retrieves the type of the @column.
   // RETURNS: the type of the column.
   // <column>: the column number
   Type get_column_type()(uint column) nothrow {
      return clutter_model_get_column_type(&this, column);
   }

   // VERSION: 1.0
   // Returns whether the @model has a filter in place, set
   // using clutter_model_set_filter()
   // RETURNS: %TRUE if a filter is set
   int get_filter_set()() nothrow {
      return clutter_model_get_filter_set(&this);
   }

   // VERSION: 0.6
   // Retrieves a #ClutterModelIter representing the first non-filtered
   // row in @model.
   // 
   // Call g_object_unref() when done using it
   // RETURNS: A new #ClutterModelIter.
   ModelIter* /*new*/ get_first_iter()() nothrow {
      return clutter_model_get_first_iter(&this);
   }

   // VERSION: 0.6
   // Retrieves a #ClutterModelIter representing the row at the given index.
   // 
   // If a filter function has been set using clutter_model_set_filter()
   // then the @model implementation will return the first non filtered
   // row.
   // 
   // out of bounds. When done using the iterator object, call g_object_unref()
   // to deallocate its resources
   // RETURNS: A new #ClutterModelIter, or %NULL if @row was
   // <row>: position of the row to retrieve
   ModelIter* /*new*/ get_iter_at_row()(uint row) nothrow {
      return clutter_model_get_iter_at_row(&this, row);
   }

   // VERSION: 0.6
   // Retrieves a #ClutterModelIter representing the last non-filtered
   // row in @model.
   // 
   // Call g_object_unref() when done using it
   // RETURNS: A new #ClutterModelIter.
   ModelIter* /*new*/ get_last_iter()() nothrow {
      return clutter_model_get_last_iter(&this);
   }

   // VERSION: 0.6
   // Retrieves the number of columns inside @model.
   // RETURNS: the number of columns
   uint get_n_columns()() nothrow {
      return clutter_model_get_n_columns(&this);
   }

   // VERSION: 0.6
   // Retrieves the number of rows inside @model, eventually taking
   // into account any filtering function set using clutter_model_set_filter().
   // 
   // the length of the filtered @model is returned.
   // RETURNS: The length of the @model. If there is a filter set, then
   uint get_n_rows()() nothrow {
      return clutter_model_get_n_rows(&this);
   }

   // VERSION: 0.6
   // Retrieves the number of column used for sorting the @model.
   // RETURNS: a column number, or -1 if the model is not sorted
   int get_sorting_column()() nothrow {
      return clutter_model_get_sorting_column(&this);
   }

   // Unintrospectable method: insert() / clutter_model_insert()
   // VERSION: 0.6
   // Inserts a new row to the #ClutterModel at @row, setting the row
   // values upon creation. For example, to insert a new row at index 100,
   // where column 0 is type %G_TYPE_INT and column 1 is of type
   // %G_TYPE_STRING:
   // 
   // <informalexample><programlisting>
   // ClutterModel *model;
   // model = clutter_model_default_new (2,
   // G_TYPE_INT,    "Score",
   // G_TYPE_STRING, "Team");
   // clutter_model_insert (model, 3, 0, 42, 1, "Team #1", -1);
   // </programlisting></informalexample>
   // <row>: the position to insert the new row
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_model_insert insert; // Variadic
   +/

   // VERSION: 0.6
   // Sets the data in the cell specified by @iter and @column. The type of 
   // @value must be convertable to the type of the column. If the row does
   // not exist then it is created.
   // <row>: position of the row to modify
   // <column>: column to modify
   // <value>: new value for the cell
   void insert_value(AT0)(uint row, uint column, AT0 /*GObject2.Value*/ value) nothrow {
      clutter_model_insert_value(&this, row, column, UpCast!(GObject2.Value*)(value));
   }

   // VERSION: 0.6
   // Inserts data at @row into the #ClutterModel, setting the row
   // values for the given @columns upon creation.
   // <row>: row index
   // <n_columns>: the number of columns and values to set
   // <columns>: a vector containing the columns to set
   // <values>: a vector containing the values for the cells
   void insertv(AT0, AT1)(uint row, uint n_columns, AT0 /*uint*/ columns, AT1 /*GObject2.Value*/ values) nothrow {
      clutter_model_insertv(&this, row, n_columns, UpCast!(uint*)(columns), UpCast!(GObject2.Value*)(values));
   }

   // Unintrospectable method: prepend() / clutter_model_prepend()
   // VERSION: 0.6
   // Creates and prepends a new row to the #ClutterModel, setting the row
   // values upon creation. For example, to prepend a new row where column 0
   // is type %G_TYPE_INT and column 1 is of type %G_TYPE_STRING:
   // 
   // <informalexample><programlisting>
   // ClutterModel *model;
   // model = clutter_model_default_new (2,
   // G_TYPE_INT,    "Score",
   // G_TYPE_STRING, "Team");
   // clutter_model_prepend (model, 0, 42, 1, "Team #1", -1);
   // </programlisting></informalexample>
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_model_prepend prepend; // Variadic
   +/

   // VERSION: 0.6
   // Creates and prepends a new row to the #ClutterModel, setting the row
   // values for the given @columns upon creation.
   // <n_columns>: the number of columns and values to set
   // <columns>: a vector containing the columns to set
   // <values>: a vector containing the values for the cells
   void prependv(AT0, AT1)(uint n_columns, AT0 /*uint*/ columns, AT1 /*GObject2.Value*/ values) nothrow {
      clutter_model_prependv(&this, n_columns, UpCast!(uint*)(columns), UpCast!(GObject2.Value*)(values));
   }

   // VERSION: 0.6
   // Removes the row at the given position from the model.
   // <row>: position of row to remove
   void remove()(uint row) nothrow {
      clutter_model_remove(&this, row);
   }

   // VERSION: 0.6
   // Force a resort on the @model. This function should only be
   // used by subclasses of #ClutterModel.
   void resort()() nothrow {
      clutter_model_resort(&this);
   }

   // VERSION: 0.6
   // Filters the @model using the given filtering function.
   // <func>: a #ClutterModelFilterFunc, or #NULL
   // <user_data>: user data to pass to @func, or #NULL
   // <notify>: destroy notifier of @user_data, or #NULL
   void set_filter(AT0)(ModelFilterFunc func, AT0 /*void*/ user_data, GLib2.DestroyNotify notify) nothrow {
      clutter_model_set_filter(&this, func, UpCast!(void*)(user_data), notify);
   }

   // VERSION: 0.6
   // Assigns a name to the columns of a #ClutterModel.
   // 
   // This function is meant primarily for #GObjects that inherit from
   // #ClutterModel, and should only be used when contructing a #ClutterModel.
   // It will not work after the initial creation of the #ClutterModel.
   // <n_columns>: the number of column names
   // <names>: an array of strings
   void set_names(AT0)(uint n_columns, AT0 /*char*/ names) nothrow {
      clutter_model_set_names(&this, n_columns, toCString!(char*)(names));
   }

   // VERSION: 0.6
   // Sorts @model using the given sorting function.
   // <column>: the column to sort on
   // <func>: a #ClutterModelSortFunc, or #NULL
   // <user_data>: user data to pass to @func, or #NULL
   // <notify>: destroy notifier of @user_data, or #NULL
   void set_sort(AT0)(int column, ModelSortFunc func, AT0 /*void*/ user_data, GLib2.DestroyNotify notify) nothrow {
      clutter_model_set_sort(&this, column, func, UpCast!(void*)(user_data), notify);
   }

   // VERSION: 0.6
   // Sets the model to sort by @column. If @column is a negative value
   // the sorting column will be unset.
   // <column>: the column of the @model to sort, or -1
   void set_sorting_column()(int column) nothrow {
      clutter_model_set_sorting_column(&this, column);
   }

   // VERSION: 0.6
   // Sets the types of the columns inside a #ClutterModel.
   // 
   // This function is meant primarily for #GObjects that inherit from
   // #ClutterModel, and should only be used when contructing a #ClutterModel.
   // It will not work after the initial creation of the #ClutterModel.
   // <n_columns>: number of columns for the model
   // <types>: an array of #GType types
   void set_types(AT0)(uint n_columns, AT0 /*Type*/ types) nothrow {
      clutter_model_set_types(&this, n_columns, UpCast!(Type*)(types));
   }

   // VERSION: 0.6
   // The ::filter-changed signal is emitted when a new filter has been applied
   extern (C) alias static void function (Model* this_, void* user_data=null) nothrow signal_filter_changed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"filter-changed", CB/*:signal_filter_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_filter_changed)||_ttmm!(CB, signal_filter_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"filter-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::row-added signal is emitted when a new row has been added.
   // The data on the row has already been set when the ::row-added signal
   // has been emitted.
   // <iter>: a #ClutterModelIter pointing to the new row
   extern (C) alias static void function (Model* this_, ModelIter* iter, void* user_data=null) nothrow signal_row_added;
   ulong signal_connect(string name:"row-added", CB/*:signal_row_added*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_row_added)||_ttmm!(CB, signal_row_added)()) {
      return signal_connect_data!()(&this, cast(char*)"row-added",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::row-removed signal is emitted when a row has been changed.
   // The data on the row has already been updated when the ::row-changed
   // signal has been emitted.
   // <iter>: a #ClutterModelIter pointing to the changed row
   extern (C) alias static void function (Model* this_, ModelIter* iter, void* user_data=null) nothrow signal_row_changed;
   ulong signal_connect(string name:"row-changed", CB/*:signal_row_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_row_changed)||_ttmm!(CB, signal_row_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"row-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::row-removed signal is emitted when a row has been removed.
   // The data on the row pointed by the passed iterator is still valid
   // when the ::row-removed signal has been emitted.
   // <iter>: a #ClutterModelIter pointing to the removed row
   extern (C) alias static void function (Model* this_, ModelIter* iter, void* user_data=null) nothrow signal_row_removed;
   ulong signal_connect(string name:"row-removed", CB/*:signal_row_removed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_row_removed)||_ttmm!(CB, signal_row_removed)()) {
      return signal_connect_data!()(&this, cast(char*)"row-removed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::sort-changed signal is emitted after the model has been sorted
   extern (C) alias static void function (Model* this_, void* user_data=null) nothrow signal_sort_changed;
   ulong signal_connect(string name:"sort-changed", CB/*:signal_sort_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_sort_changed)||_ttmm!(CB, signal_sort_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"sort-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// Class for #ClutterModel instances.
struct ModelClass /* Version 0.6 */ {
   private GObject2.ObjectClass parent_class;
   // RETURNS: The length of the @model. If there is a filter set, then
   extern (C) uint function (Model* model) nothrow get_n_rows;
   // RETURNS: the number of columns
   extern (C) uint function (Model* model) nothrow get_n_columns;

   // RETURNS: the name of the column. The model holds the returned
   // <column>: the column number
   extern (C) char* function (Model* model, uint column) nothrow get_column_name;

   // RETURNS: the type of the column.
   // <column>: the column number
   extern (C) Type function (Model* model, uint column) nothrow get_column_type;
   // Unintrospectable functionp: insert_row() / ()
   extern (C) ModelIter* function (Model* model, int index_) nothrow insert_row;
   extern (C) void function (Model* model, uint row) nothrow remove_row;

   // RETURNS: A new #ClutterModelIter, or %NULL if @row was
   // <row>: position of the row to retrieve
   extern (C) ModelIter* /*new*/ function (Model* model, uint row) nothrow get_iter_at_row;
   // Unintrospectable functionp: resort() / ()
   extern (C) void function (Model* model, ModelSortFunc func, void* data) nothrow resort;
   extern (C) void function (Model* model, ModelIter* iter) nothrow row_added;
   extern (C) void function (Model* model, ModelIter* iter) nothrow row_removed;
   extern (C) void function (Model* model, ModelIter* iter) nothrow row_changed;
   extern (C) void function (Model* model) nothrow sort_changed;
   extern (C) void function (Model* model) nothrow filter_changed;
   extern (C) void function () nothrow _clutter_model_1;
   extern (C) void function () nothrow _clutter_model_2;
   extern (C) void function () nothrow _clutter_model_3;
   extern (C) void function () nothrow _clutter_model_4;
   extern (C) void function () nothrow _clutter_model_5;
   extern (C) void function () nothrow _clutter_model_6;
   extern (C) void function () nothrow _clutter_model_7;
   extern (C) void function () nothrow _clutter_model_8;
}


// VERSION: 0.6
// Filters the content of a row in the model.
// RETURNS: If the row should be displayed, return %TRUE
// <model>: a #ClutterModel
// <iter>: the iterator for the row
// <user_data>: data passed to clutter_model_set_filter()
extern (C) alias int function (Model* model, ModelIter* iter, void* user_data) nothrow ModelFilterFunc;


// VERSION: 0.6
// Iterates on the content of a row in the model
// RETURNS: %TRUE if the iteration should continue, %FALSE otherwise
// <model>: a #ClutterModel
// <iter>: the iterator for the row
// <user_data>: data passed to clutter_model_foreach()
extern (C) alias int function (Model* model, ModelIter* iter, void* user_data) nothrow ModelForeachFunc;


// Base class for list models iters. The #ClutterModelIter structure
// contains only private data and should be manipulated using the
// provided API.
struct ModelIter /* : GObject.Object */ /* Version 0.6 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private ModelIterPrivate* priv;


   // VERSION: 0.8
   // Copies the passed iterator.
   // RETURNS: a copy of the iterator, or %NULL
   ModelIter* /*new*/ copy()() nothrow {
      return clutter_model_iter_copy(&this);
   }

   // Unintrospectable method: get() / clutter_model_iter_get()
   // VERSION: 0.6
   // Gets the value of one or more cells in the row referenced by @iter. The
   // variable argument list should contain integer column numbers, each column
   // column number followed by a place to store the value being retrieved. The
   // list is terminated by a -1.
   // 
   // For example, to get a value from column 0 with type %G_TYPE_STRING use:
   // <informalexample><programlisting>
   // clutter_model_iter_get (iter, 0, &place_string_here, -1);
   // </programlisting></informalexample>
   // 
   // where place_string_here is a gchar* to be filled with the string. If
   // appropriate, the returned values have to be freed or unreferenced.
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_model_iter_get get; // Variadic
   +/

   // VERSION: 0.6
   // Retrieves a pointer to the #ClutterModel that this iter is part of.
   // RETURNS: a pointer to a #ClutterModel.
   Model* get_model()() nothrow {
      return clutter_model_iter_get_model(&this);
   }

   // VERSION: 0.6
   // Retrieves the position of the row that the @iter points to.
   // RETURNS: the position of the @iter in the model
   uint get_row()() nothrow {
      return clutter_model_iter_get_row(&this);
   }

   // Unintrospectable method: get_valist() / clutter_model_iter_get_valist()
   // VERSION: 0.6
   // See clutter_model_iter_get(). This version takes a va_list for language
   // bindings.
   // <args>: a list of column/return location pairs, terminated by -1
   void get_valist()(va_list args) nothrow {
      clutter_model_iter_get_valist(&this, args);
   }

   // VERSION: 0.6
   // Sets an initializes @value to that at @column. When done with @value, 
   // g_value_unset() needs to be called to free any allocated memory.
   // <column>: column number to retrieve the value from
   // <value>: an empty #GValue to set
   void get_value(AT0)(uint column, /*out*/ AT0 /*GObject2.Value*/ value) nothrow {
      clutter_model_iter_get_value(&this, column, UpCast!(GObject2.Value*)(value));
   }

   // VERSION: 0.6
   // Gets whether the current iterator is at the beginning of the model
   // to which it belongs.
   // RETURNS: #TRUE if @iter is the first iter in the filtered model
   int is_first()() nothrow {
      return clutter_model_iter_is_first(&this);
   }

   // VERSION: 0.6
   // Gets whether the iterator is at the end of the model to which it
   // belongs.
   // RETURNS: #TRUE if @iter is the last iter in the filtered model.
   int is_last()() nothrow {
      return clutter_model_iter_is_last(&this);
   }

   // VERSION: 0.6
   // Updates the @iter to point at the next position in the model.
   // The model implementation should take into account the presence of
   // a filter function.
   // 
   // row in the model.
   // RETURNS: The passed iterator, updated to point at the next
   ModelIter* next()() nothrow {
      return clutter_model_iter_next(&this);
   }

   // VERSION: 0.6
   // Sets the @iter to point at the previous position in the model.
   // The model implementation should take into account the presence of
   // a filter function.
   // 
   // row in the model.
   // RETURNS: The passed iterator, updated to point at the previous
   ModelIter* prev()() nothrow {
      return clutter_model_iter_prev(&this);
   }

   // Unintrospectable method: set() / clutter_model_iter_set()
   // VERSION: 0.6
   // Sets the value of one or more cells in the row referenced by @iter. The
   // variable argument list should contain integer column numbers, each column
   // column number followed by the value to be set. The  list is terminated by a
   // -1.
   // 
   // For example, to set column 0 with type %G_TYPE_STRING, use:
   // <informalexample><programlisting>
   // clutter_model_iter_set (iter, 0, "foo", -1);
   // </programlisting></informalexample>
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_model_iter_set set; // Variadic
   +/

   // Unintrospectable method: set_valist() / clutter_model_iter_set_valist()
   // VERSION: 0.6
   // See clutter_model_iter_set(); this version takes a va_list for language
   // bindings.
   // <args>: va_list of column/value pairs, terminiated by -1
   void set_valist()(va_list args) nothrow {
      clutter_model_iter_set_valist(&this, args);
   }

   // VERSION: 0.6
   // Sets the data in the cell specified by @iter and @column. The type of
   // @value must be convertable to the type of the column.
   // <column>: column number to retrieve the value from
   // <value>: new value for the cell
   void set_value(AT0)(uint column, AT0 /*GObject2.Value*/ value) nothrow {
      clutter_model_iter_set_value(&this, column, UpCast!(GObject2.Value*)(value));
   }
}

// Class for #ClutterModelIter instances.
struct ModelIterClass /* Version 0.6 */ {
   private GObject2.ObjectClass parent_class;

   // <column>: column number to retrieve the value from
   // <value>: an empty #GValue to set
   extern (C) void function (ModelIter* iter, uint column, /*out*/ GObject2.Value* value) nothrow get_value;

   // <column>: column number to retrieve the value from
   // <value>: new value for the cell
   extern (C) void function (ModelIter* iter, uint column, GObject2.Value* value) nothrow set_value;
   // RETURNS: #TRUE if @iter is the first iter in the filtered model
   extern (C) int function (ModelIter* iter) nothrow is_first;
   // RETURNS: #TRUE if @iter is the last iter in the filtered model.
   extern (C) int function (ModelIter* iter) nothrow is_last;
   // RETURNS: The passed iterator, updated to point at the next
   extern (C) ModelIter* function (ModelIter* iter) nothrow next;
   // RETURNS: The passed iterator, updated to point at the previous
   extern (C) ModelIter* function (ModelIter* iter) nothrow prev;
   // RETURNS: a pointer to a #ClutterModel.
   extern (C) Model* function (ModelIter* iter) nothrow get_model;
   // RETURNS: the position of the @iter in the model
   extern (C) uint function (ModelIter* iter) nothrow get_row;
   // RETURNS: a copy of the iterator, or %NULL
   extern (C) ModelIter* /*new*/ function (ModelIter* iter) nothrow copy;
   extern (C) void function () nothrow _clutter_model_iter_1;
   extern (C) void function () nothrow _clutter_model_iter_2;
   extern (C) void function () nothrow _clutter_model_iter_3;
   extern (C) void function () nothrow _clutter_model_iter_4;
   extern (C) void function () nothrow _clutter_model_iter_5;
   extern (C) void function () nothrow _clutter_model_iter_6;
   extern (C) void function () nothrow _clutter_model_iter_7;
   extern (C) void function () nothrow _clutter_model_iter_8;
}

struct ModelIterPrivate {
}

struct ModelPrivate {
}


// VERSION: 0.6
// Compares the content of two rows in the model.
// 
// @a is before @b, or 0 if the rows are the same
// RETURNS: a positive integer if @a is after @b, a negative integer if
// <model>: a #ClutterModel
// <a>: a #GValue representing the contents of the row
// <b>: a #GValue representing the contents of the second row
// <user_data>: data passed to clutter_model_set_sort()
extern (C) alias int function (Model* model, GObject2.Value* a, GObject2.Value* b, void* user_data) nothrow ModelSortFunc;


// Masks applied to a #ClutterEvent by modifiers.
// 
// Note that Clutter may add internal values to events which include
// reserved values such as %CLUTTER_MODIFIER_RESERVED_13_MASK.  Your code
// should preserve and ignore them.  You can use %CLUTTER_MODIFIER_MASK to
// remove all reserved values.
enum ModifierType /* Version 0.4 */ {
   SHIFT_MASK = 1,
   LOCK_MASK = 2,
   CONTROL_MASK = 4,
   MOD1_MASK = 8,
   MOD2_MASK = 16,
   MOD3_MASK = 32,
   MOD4_MASK = 64,
   MOD5_MASK = 128,
   BUTTON1_MASK = 256,
   BUTTON2_MASK = 512,
   BUTTON3_MASK = 1024,
   BUTTON4_MASK = 2048,
   BUTTON5_MASK = 4096,
   MODIFIER_RESERVED_13_MASK = 8192,
   MODIFIER_RESERVED_14_MASK = 16384,
   MODIFIER_RESERVED_15_MASK = 32768,
   MODIFIER_RESERVED_16_MASK = 65536,
   MODIFIER_RESERVED_17_MASK = 131072,
   MODIFIER_RESERVED_18_MASK = 262144,
   MODIFIER_RESERVED_19_MASK = 524288,
   MODIFIER_RESERVED_20_MASK = 1048576,
   MODIFIER_RESERVED_21_MASK = 2097152,
   MODIFIER_RESERVED_22_MASK = 4194304,
   MODIFIER_RESERVED_23_MASK = 8388608,
   MODIFIER_RESERVED_24_MASK = 16777216,
   MODIFIER_RESERVED_25_MASK = 33554432,
   SUPER_MASK = 67108864,
   HYPER_MASK = 134217728,
   META_MASK = 268435456,
   MODIFIER_RESERVED_29_MASK = 536870912,
   RELEASE_MASK = 1073741824,
   MODIFIER_MASK = 1543512063
}
enum int MonBrightnessDown = 269025027;
enum int MonBrightnessUp = 269025026;
// Event for the pointer motion
struct MotionEvent /* Version 0.2 */ {
   EventType type;
   uint time;
   EventFlags flags;
   Stage* stage;
   Actor* source;
   float x, y;
   ModifierType modifier_state;
   double* axes;
   InputDevice* device;
}

enum int MouseKeys_Accel_Enable = 65143;
enum int MouseKeys_Enable = 65142;
enum int Muhenkan = 65314;
enum int Multi_key = 65312;
enum int MultipleCandidate = 65341;
enum int Music = 269025170;
enum int MyComputer = 269025075;
enum int MySites = 269025127;
enum int N = 78;
enum int Nacute = 465;
enum int NairaSign = 16785574;
enum int Ncaron = 466;
enum int Ncedilla = 977;
enum int New = 269025128;
enum int NewSheqelSign = 16785578;
enum int News = 269025129;
enum int Next = 65366;
enum int Next_VMode = 269024802;
enum int Next_Virtual_Screen = 65234;
enum int Ntilde = 209;
enum int Num_Lock = 65407;
enum int O = 79;
enum int OE = 5052;
enum int Oacute = 211;
enum int Obarred = 16777631;
enum int Obelowdot = 16785100;
enum int Ocaron = 16777681;
enum int Ocircumflex = 212;
enum int Ocircumflexacute = 16785104;
enum int Ocircumflexbelowdot = 16785112;
enum int Ocircumflexgrave = 16785106;
enum int Ocircumflexhook = 16785108;
enum int Ocircumflextilde = 16785110;
enum int Odiaeresis = 214;
enum int Odoubleacute = 469;
enum int OfficeHome = 269025130;

// The #ClutterOffscreenEffect structure contains only private data
// and should be accessed using the provided API
struct OffscreenEffect /* : Effect */ /* Version 1.4 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance effect;
   Effect parent_instance;
   private OffscreenEffectPrivate* priv;


   // VERSION: 1.4
   // Calls the create_texture() virtual function of the @effect
   // 
   // %COGL_INVALID_HANDLE. The returned handle has its reference
   // count increased.
   // RETURNS: a handle to a Cogl texture, or
   // <width>: the minimum width of the target texture
   // <height>: the minimum height of the target texture
   Cogl.Handle /*new*/ create_texture()(float width, float height) nothrow {
      return clutter_offscreen_effect_create_texture(&this, width, height);
   }

   // VERSION: 1.4
   // Retrieves the material used as a render target for the offscreen
   // buffer created by @effect
   // 
   // You should only use the returned #CoglMaterial when painting. The
   // returned material might change between different frames.
   // 
   // returned material is owned by Clutter and it should not be
   // modified or freed
   // RETURNS: a #CoglMaterial or %NULL. The
   Cogl.Material* get_target()() nothrow {
      return clutter_offscreen_effect_get_target(&this);
   }

   // VERSION: 1.8
   // Retrieves the size of the offscreen buffer used by @effect to
   // paint the actor to which it has been applied.
   // 
   // This function should only be called by #ClutterOffscreenEffect
   // implementations, from within the <function>paint_target()</function>
   // virtual function.
   // 
   // and %FALSE otherwise
   // RETURNS: %TRUE if the offscreen buffer has a valid size,
   // <width>: return location for the target width, or %NULL
   // <height>: return location for the target height, or %NULL
   int get_target_size(AT0, AT1)(/*out*/ AT0 /*float*/ width, /*out*/ AT1 /*float*/ height) nothrow {
      return clutter_offscreen_effect_get_target_size(&this, UpCast!(float*)(width), UpCast!(float*)(height));
   }

   // VERSION: 1.4
   // Calls the paint_target() virtual function of the @effect
   void paint_target()() nothrow {
      clutter_offscreen_effect_paint_target(&this);
   }
}

// The #ClutterOffscreenEffectClass structure contains only private data
struct OffscreenEffectClass /* Version 1.4 */ {
   private EffectClass parent_class;

   // RETURNS: a handle to a Cogl texture, or
   // <width>: the minimum width of the target texture
   // <height>: the minimum height of the target texture
   extern (C) Cogl.Handle /*new*/ function (OffscreenEffect* effect, float width, float height) nothrow create_texture;
   extern (C) void function (OffscreenEffect* effect) nothrow paint_target;
   extern (C) void function () nothrow _clutter_offscreen1;
   extern (C) void function () nothrow _clutter_offscreen2;
   extern (C) void function () nothrow _clutter_offscreen3;
   extern (C) void function () nothrow _clutter_offscreen4;
   extern (C) void function () nothrow _clutter_offscreen5;
   extern (C) void function () nothrow _clutter_offscreen6;
   extern (C) void function () nothrow _clutter_offscreen7;
}

struct OffscreenEffectPrivate {
}

// Possible flags to pass to clutter_actor_set_offscreen_redirect().
enum OffscreenRedirect /* Version 1.8 */ {
   AUTOMATIC_FOR_OPACITY = 1,
   ALWAYS = 2
}
enum int Ograve = 210;
enum int Ohook = 16785102;
enum int Ohorn = 16777632;
enum int Ohornacute = 16785114;
enum int Ohornbelowdot = 16785122;
enum int Ohorngrave = 16785116;
enum int Ohornhook = 16785118;
enum int Ohorntilde = 16785120;
enum int Omacron = 978;
enum int Ooblique = 216;
enum int Open = 269025131;
enum int OpenURL = 269025080;
enum int Option = 269025132;
enum int Oslash = 216;
enum int Otilde = 213;
enum int Overlay1_Enable = 65144;
enum int Overlay2_Enable = 65145;
enum int P = 80;
enum int PATH_RELATIVE = 32;
enum int PRIORITY_REDRAW = 50;
enum int Pabovedot = 16784982;

// <structname>ClutterPageTurnEffect</structname> is an opaque structure
// whose members can only be accessed using the provided API
struct PageTurnEffect /* : DeformEffect */ /* Version 1.4 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent deformeffect;
   DeformEffect method_parent;


   // VERSION: 1.4
   // Creates a new #ClutterPageTurnEffect instance with the given parameters
   // RETURNS: the newly created #ClutterPageTurnEffect
   // <period>: the period of the page curl, between 0.0 and 1.0
   // <angle>: the angle of the page curl, between 0.0 and 360.0
   // <radius>: the radius of the page curl, in pixels
   static PageTurnEffect* new_()(double period, double angle, float radius) nothrow {
      return clutter_page_turn_effect_new(period, angle, radius);
   }
   static auto opCall()(double period, double angle, float radius) {
      return clutter_page_turn_effect_new(period, angle, radius);
   }

   // VERSION: 1.4
   // Retrieves the value set using clutter_page_turn_effect_get_angle()
   // RETURNS: the angle of the page curling
   double get_angle()() nothrow {
      return clutter_page_turn_effect_get_angle(&this);
   }

   // VERSION: 1.4
   // Retrieves the value set using clutter_page_turn_effect_get_period()
   // RETURNS: the period of the page curling
   double get_period()() nothrow {
      return clutter_page_turn_effect_get_period(&this);
   }

   // VERSION: 1.4
   // Retrieves the value set using clutter_page_turn_effect_set_radius()
   // RETURNS: the radius of the page curling
   float get_radius()() nothrow {
      return clutter_page_turn_effect_get_radius(&this);
   }

   // VERSION: 1.4
   // Sets the angle of the page curling, in degrees
   // <angle>: the angle of the page curl, in degrees
   void set_angle()(double angle) nothrow {
      clutter_page_turn_effect_set_angle(&this, angle);
   }

   // VERSION: 1.4
   // Sets the period of the page curling, between 0.0 (no curling)
   // and 1.0 (fully curled)
   // <period>: the period of the page curl, between 0.0 and 1.0
   void set_period()(double period) nothrow {
      clutter_page_turn_effect_set_period(&this, period);
   }

   // VERSION: 1.4
   // Sets the radius of the page curling
   // <radius>: the radius of the page curling, in pixels
   void set_radius()(float radius) nothrow {
      clutter_page_turn_effect_set_radius(&this, radius);
   }
}

struct PageTurnEffectClass {
}

enum int Page_Down = 65366;
enum int Page_Up = 65365;

// <structname>ClutterPaintVolume</structname> is an opaque structure
// whose members cannot be directly accessed.
// 
// A <structname>ClutterPaintVolume</structname> represents an
// a bounding volume whos internal representation isn't defined but
// can be set and queried in terms of an axis aligned bounding box.
// 
// Other internal representation and methods for describing the
// bounding volume may be added in the future.
struct PaintVolume /* Version 1.4 */ {

   // VERSION: 1.6
   // Copies @pv into a new #ClutterPaintVolume
   // RETURNS: a newly allocated copy of a #ClutterPaintVolume
   PaintVolume* /*new*/ copy()() nothrow {
      return clutter_paint_volume_copy(&this);
   }

   // VERSION: 1.6
   // Frees the resources allocated by @pv
   void free()() nothrow {
      clutter_paint_volume_free(&this);
   }

   // VERSION: 1.6
   // Retrieves the depth of the volume's, axis aligned, bounding box.
   // 
   // In other words; this takes into account what actor's coordinate
   // space @pv belongs too and conceptually fits an axis aligned box
   // around the volume. It returns the size of that bounding box as
   // measured along the z-axis.
   // 
   // <note><para>If, for example, clutter_actor_get_transformed_paint_volume()
   // is used to transform a 2D child actor that is 100px wide, 100px
   // high and 0px deep into container coordinates then the depth might
   // not simply be 0px if the child actor has a 3D rotation applied to
   // it.</para>
   // <para>Remember; after clutter_actor_get_transformed_paint_volume() is
   // used then the transformed volume will be defined relative to the
   // container actor and in container coordinates a 2D child actor
   // can have a 3D bounding volume.</para></note>
   // 
   // <note>There are no accuracy guarantees for the reported depth,
   // except that it must always be >= to the true depth. This is
   // because actors may report simple, loose fitting paint-volumes
   // for efficiency.</note>
   // RETURNS: the depth, in units of @pv's local coordinate system.
   float get_depth()() nothrow {
      return clutter_paint_volume_get_depth(&this);
   }

   // VERSION: 1.6
   // Retrieves the height of the volume's, axis aligned, bounding box.
   // 
   // In other words; this takes into account what actor's coordinate
   // space @pv belongs too and conceptually fits an axis aligned box
   // around the volume. It returns the size of that bounding box as
   // measured along the y-axis.
   // 
   // <note><para>If, for example, clutter_actor_get_transformed_paint_volume()
   // is used to transform a 2D child actor that is 100px wide, 100px
   // high and 0px deep into container coordinates then the height might
   // not simply be 100px if the child actor has a 3D rotation applied to
   // it.</para>
   // <para>Remember; after clutter_actor_get_transformed_paint_volume() is
   // used then a transformed child volume will be defined relative to the
   // ancestor container actor and so a 2D child actor
   // can have a 3D bounding volume.</para></note>
   // 
   // <note>There are no accuracy guarantees for the reported height,
   // except that it must always be >= to the true height. This is
   // because actors may report simple, loose fitting paint-volumes
   // for efficiency</note>
   // RETURNS: the height, in units of @pv's local coordinate system.
   float get_height()() nothrow {
      return clutter_paint_volume_get_height(&this);
   }

   // VERSION: 1.6
   // Retrieves the origin of the #ClutterPaintVolume.
   // <vertex>: the return location for a #ClutterVertex
   void get_origin(AT0)(/*out*/ AT0 /*Vertex*/ vertex) nothrow {
      clutter_paint_volume_get_origin(&this, UpCast!(Vertex*)(vertex));
   }

   // VERSION: 1.6
   // Retrieves the width of the volume's, axis aligned, bounding box.
   // 
   // In other words; this takes into account what actor's coordinate
   // space @pv belongs too and conceptually fits an axis aligned box
   // around the volume. It returns the size of that bounding box as
   // measured along the x-axis.
   // 
   // <note><para>If, for example, clutter_actor_get_transformed_paint_volume()
   // is used to transform a 2D child actor that is 100px wide, 100px
   // high and 0px deep into container coordinates then the width might
   // not simply be 100px if the child actor has a 3D rotation applied to
   // it.</para>
   // <para>Remember; after clutter_actor_get_transformed_paint_volume() is
   // used then a transformed child volume will be defined relative to the
   // ancestor container actor and so a 2D child actor
   // can have a 3D bounding volume.</para></note>
   // 
   // <note>There are no accuracy guarantees for the reported width,
   // except that it must always be >= to the true width. This is
   // because actors may report simple, loose fitting paint-volumes
   // for efficiency</note>
   // RETURNS: the width, in units of @pv's local coordinate system.
   float get_width()() nothrow {
      return clutter_paint_volume_get_width(&this);
   }

   // VERSION: 1.6
   // Sets the depth of the paint volume. The depth is measured along
   // the z axis in the actor coordinates that @pv is associated with.
   // <depth>: the depth of the paint volume, in pixels
   void set_depth()(float depth) nothrow {
      clutter_paint_volume_set_depth(&this, depth);
   }

   // VERSION: 1.6
   // Sets the #ClutterPaintVolume from the allocation of @actor.
   // 
   // This function should be used when overriding the
   // <function>get_paint_volume()</function> by #ClutterActor sub-classes that do
   // not paint outside their allocation.
   // 
   // A typical example is:
   // 
   // |[
   // static gboolean
   // my_actor_get_paint_volume (ClutterActor       *self,
   // ClutterPaintVolume *volume)
   // {
   // return clutter_paint_volume_set_from_allocation (volume, self);
   // }
   // ]|
   // 
   // otherwise
   // RETURNS: %TRUE if the paint volume was successfully set, and %FALSE
   // <actor>: a #ClutterActor
   int set_from_allocation(AT0)(AT0 /*Actor*/ actor) nothrow {
      return clutter_paint_volume_set_from_allocation(&this, UpCast!(Actor*)(actor));
   }

   // VERSION: 1.6
   // Sets the height of the paint volume. The height is measured along
   // the y axis in the actor coordinates that @pv is associated with.
   // <height>: the height of the paint volume, in pixels
   void set_height()(float height) nothrow {
      clutter_paint_volume_set_height(&this, height);
   }

   // VERSION: 1.6
   // Sets the origin of the paint volume.
   // 
   // The origin is defined as the X, Y and Z coordinates of the top-left
   // corner of an actor's paint volume, in actor coordinates.
   // 
   // The default is origin is assumed at: (0, 0, 0)
   // <origin>: a #ClutterVertex
   void set_origin(AT0)(AT0 /*Vertex*/ origin) nothrow {
      clutter_paint_volume_set_origin(&this, UpCast!(Vertex*)(origin));
   }

   // VERSION: 1.6
   // Sets the width of the paint volume. The width is measured along
   // the x axis in the actor coordinates that @pv is associated with.
   // <width>: the width of the paint volume, in pixels
   void set_width()(float width) nothrow {
      clutter_paint_volume_set_width(&this, width);
   }

   // VERSION: 1.6
   // Updates the geometry of @pv to encompass @pv and @another_pv.
   // 
   // <note>There are no guarantees about how precisely the two volumes
   // will be encompassed.</note>
   // <another_pv>: A second #ClutterPaintVolume to union with @pv
   void union_(AT0)(AT0 /*PaintVolume*/ another_pv) nothrow {
      clutter_paint_volume_union(&this, UpCast!(PaintVolume*)(another_pv));
   }
}


// A #GParamSpec subclass for defining properties holding
// a #ClutterColor.
struct ParamSpecColor /* : GObject.ParamSpec */ /* Version 1.0 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   GObject2.ParamSpec parent_instance;
   Color* default_value;
}

// #GParamSpec subclass for fixed point based properties
struct ParamSpecFixed /* : GObject.ParamSpec */ /* Version 0.8 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   GObject2.ParamSpec parent_instance;
   Cogl.Fixed minimum, maximum, default_value;
}

struct ParamSpecUnit /* : GObject.ParamSpec */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent paramspec;
   GObject2.ParamSpec method_parent;
}

// #GParamSpec subclass for unit based properties.
struct ParamSpecUnits /* Version 1.0 */ {
   private GObject2.ParamSpec parent_instance;
   UnitType default_type;
   float default_value, minimum, maximum;
}

enum int Paste = 269025133;

// The #ClutterPath struct contains only private data and should
// be accessed with the functions below.
struct Path /* : GObject.InitiallyUnowned */ /* Version 1.0 */ {
   alias parent this;
   alias parent super_;
   alias parent initiallyunowned;
   GObject2.InitiallyUnowned parent;
   private PathPrivate* priv;


   // VERSION: 1.0
   // Creates a new #ClutterPath instance with no nodes.
   // 
   // The object has a floating reference so if you add it to a
   // #ClutterBehaviourPath then you do not need to unref it.
   // RETURNS: the newly created #ClutterPath
   static Path* new_()() nothrow {
      return clutter_path_new();
   }
   static auto opCall()() {
      return clutter_path_new();
   }

   // VERSION: 1.0
   // Creates a new #ClutterPath instance with the nodes described in
   // @desc. See clutter_path_add_string() for details of the format of
   // the string.
   // 
   // The object has a floating reference so if you add it to a
   // #ClutterBehaviourPath then you do not need to unref it.
   // RETURNS: the newly created #ClutterPath
   // <desc>: a string describing the path
   static Path* new_with_description(AT0)(AT0 /*char*/ desc) nothrow {
      return clutter_path_new_with_description(toCString!(char*)(desc));
   }
   static auto opCall(AT0)(AT0 /*char*/ desc) {
      return clutter_path_new_with_description(toCString!(char*)(desc));
   }

   // VERSION: 1.0
   // Add the nodes of the Cairo path to the end of @path.
   // <cpath>: a Cairo path
   void add_cairo_path(AT0)(AT0 /*cairo.Path*/ cpath) nothrow {
      clutter_path_add_cairo_path(&this, UpCast!(cairo.Path*)(cpath));
   }

   // VERSION: 1.0
   // Adds a %CLUTTER_PATH_CLOSE type node to the path. This creates a
   // straight line from the last node to the last %CLUTTER_PATH_MOVE_TO
   // type node.
   void add_close()() nothrow {
      clutter_path_add_close(&this);
   }

   // VERSION: 1.0
   // Adds a %CLUTTER_PATH_CURVE_TO type node to the path. This causes
   // the actor to follow a bezier from the last node to (@x_3, @y_3) using
   // (@x_1, @y_1) and (@x_2,@y_2) as control points.
   // <x_1>: the x coordinate of the first control point
   // <y_1>: the y coordinate of the first control point
   // <x_2>: the x coordinate of the second control point
   // <y_2>: the y coordinate of the second control point
   // <x_3>: the x coordinate of the third control point
   // <y_3>: the y coordinate of the third control point
   void add_curve_to()(int x_1, int y_1, int x_2, int y_2, int x_3, int y_3) nothrow {
      clutter_path_add_curve_to(&this, x_1, y_1, x_2, y_2, x_3, y_3);
   }

   // VERSION: 1.0
   // Adds a %CLUTTER_PATH_LINE_TO type node to the path. This causes the
   // actor to move to the new coordinates in a straight line.
   // <x>: the x coordinate
   // <y>: the y coordinate
   void add_line_to()(int x, int y) nothrow {
      clutter_path_add_line_to(&this, x, y);
   }

   // VERSION: 1.0
   // Adds a %CLUTTER_PATH_MOVE_TO type node to the path. This is usually
   // used as the first node in a path. It can also be used in the middle
   // of the path to cause the actor to jump to the new coordinate.
   // <x>: the x coordinate
   // <y>: the y coordinate
   void add_move_to()(int x, int y) nothrow {
      clutter_path_add_move_to(&this, x, y);
   }

   // VERSION: 1.0
   // Adds @node to the end of the path.
   // <node>: a #ClutterPathNode
   void add_node(AT0)(AT0 /*PathNode*/ node) nothrow {
      clutter_path_add_node(&this, UpCast!(PathNode*)(node));
   }

   // VERSION: 1.0
   // Same as clutter_path_add_curve_to() except the coordinates are
   // relative to the previous node.
   // <x_1>: the x coordinate of the first control point
   // <y_1>: the y coordinate of the first control point
   // <x_2>: the x coordinate of the second control point
   // <y_2>: the y coordinate of the second control point
   // <x_3>: the x coordinate of the third control point
   // <y_3>: the y coordinate of the third control point
   void add_rel_curve_to()(int x_1, int y_1, int x_2, int y_2, int x_3, int y_3) nothrow {
      clutter_path_add_rel_curve_to(&this, x_1, y_1, x_2, y_2, x_3, y_3);
   }

   // VERSION: 1.0
   // Same as clutter_path_add_line_to() except the coordinates are
   // relative to the previous node.
   // <x>: the x coordinate
   // <y>: the y coordinate
   void add_rel_line_to()(int x, int y) nothrow {
      clutter_path_add_rel_line_to(&this, x, y);
   }

   // VERSION: 1.0
   // Same as clutter_path_add_move_to() except the coordinates are
   // relative to the previous node.
   // <x>: the x coordinate
   // <y>: the y coordinate
   void add_rel_move_to()(int x, int y) nothrow {
      clutter_path_add_rel_move_to(&this, x, y);
   }

   // VERSION: 1.0
   // Adds new nodes to the end of the path as described in @str. The
   // format is a subset of the SVG path format. Each node is represented
   // by a letter and is followed by zero, one or three pairs of
   // coordinates. The coordinates can be separated by spaces or a
   // comma. The types are:
   // 
   // <variablelist>
   // <varlistentry><term>M</term>
   // <listitem><para>
   // Adds a %CLUTTER_PATH_MOVE_TO node. Takes one pair of coordinates.
   // </para></listitem></varlistentry>
   // <varlistentry><term>L</term>
   // <listitem><para>
   // Adds a %CLUTTER_PATH_LINE_TO node. Takes one pair of coordinates.
   // </para></listitem></varlistentry>
   // <varlistentry><term>C</term>
   // <listitem><para>
   // Adds a %CLUTTER_PATH_CURVE_TO node. Takes three pairs of coordinates.
   // </para></listitem></varlistentry>
   // <varlistentry><term>z</term>
   // <listitem><para>
   // Adds a %CLUTTER_PATH_CLOSE node. No coordinates are needed.
   // </para></listitem></varlistentry>
   // </variablelist>
   // 
   // The M, L and C commands can also be specified in lower case which
   // means the coordinates are relative to the previous node.
   // 
   // For example, to move an actor in a 100 by 100 pixel square centered
   // on the point 300,300 you could use the following path:
   // 
   // <informalexample>
   // <programlisting>
   // M 250,350 l 0 -100 L 350,250 l 0 100 z
   // </programlisting>
   // </informalexample>
   // 
   // If the path description isn't valid %FALSE will be returned and no
   // nodes will be added.
   // 
   // otherwise.
   // RETURNS: %TRUE is the path description was valid or %FALSE
   // <str>: a string describing the new nodes
   int add_string(AT0)(AT0 /*char*/ str) nothrow {
      return clutter_path_add_string(&this, toCString!(char*)(str));
   }

   // VERSION: 1.0
   // Removes all nodes from the path.
   void clear()() nothrow {
      clutter_path_clear(&this);
   }

   // VERSION: 1.0
   // Calls a function for each node of the path.
   // <callback>: the function to call with each node
   // <user_data>: user data to pass to the function
   void foreach_(AT0)(PathCallback callback, AT0 /*void*/ user_data) nothrow {
      clutter_path_foreach(&this, callback, UpCast!(void*)(user_data));
   }

   // VERSION: 1.0
   // Returns a newly allocated string describing the path in the same
   // format as used by clutter_path_add_string().
   // RETURNS: a string description of the path. Free with g_free().
   char* /*new*/ get_description()() nothrow {
      return clutter_path_get_description(&this);
   }

   // VERSION: 1.0
   // Retrieves an approximation of the total length of the path.
   // RETURNS: the length of the path.
   uint get_length()() nothrow {
      return clutter_path_get_length(&this);
   }

   // VERSION: 1.0
   // Retrieves the number of nodes in the path.
   // RETURNS: the number of nodes.
   uint get_n_nodes()() nothrow {
      return clutter_path_get_n_nodes(&this);
   }

   // VERSION: 1.0
   // Retrieves the node of the path indexed by @index.
   // <index_>: the node number to retrieve
   // <node>: a location to store a copy of the node
   void get_node(AT0)(uint index_, /*out*/ AT0 /*PathNode*/ node) nothrow {
      clutter_path_get_node(&this, index_, UpCast!(PathNode*)(node));
   }

   // VERSION: 1.0
   // Returns a #GSList of #ClutterPathNode<!-- -->s. The list should be
   // freed with g_slist_free(). The nodes are owned by the path and
   // should not be freed. Altering the path may cause the nodes in the
   // list to become invalid so you should copy them if you want to keep
   // the list.
   // 
   // list of nodes in the path.
   // RETURNS: a
   GLib2.SList* /*new container*/ get_nodes()() nothrow {
      return clutter_path_get_nodes(&this);
   }

   // VERSION: 1.0
   // The value in @progress represents a position along the path where
   // 0.0 is the beginning and 1.0 is the end of the path. An
   // interpolated position is then stored in @position.
   // RETURNS: index of the node used to calculate the position.
   // <progress>: a position along the path as a fraction of its length
   // <position>: location to store the position
   uint get_position(AT0)(double progress, /*out*/ AT0 /*Knot*/ position) nothrow {
      return clutter_path_get_position(&this, progress, UpCast!(Knot*)(position));
   }

   // VERSION: 1.0
   // Inserts @node into the path before the node at the given offset. If
   // @index_ is negative it will append the node to the end of the path.
   // <index_>: offset of where to insert the node
   // <node>: the node to insert
   void insert_node(AT0)(int index_, AT0 /*PathNode*/ node) nothrow {
      clutter_path_insert_node(&this, index_, UpCast!(PathNode*)(node));
   }

   // VERSION: 1.0
   // Removes the node at the given offset from the path.
   // <index_>: index of the node to remove
   void remove_node()(uint index_) nothrow {
      clutter_path_remove_node(&this, index_);
   }

   // VERSION: 1.0
   // Replaces the node at offset @index_ with @node.
   // <index_>: index to the existing node
   // <node>: the replacement node
   void replace_node(AT0)(uint index_, AT0 /*PathNode*/ node) nothrow {
      clutter_path_replace_node(&this, index_, UpCast!(PathNode*)(node));
   }

   // VERSION: 1.0
   // Replaces all of the nodes in the path with nodes described by
   // @str. See clutter_path_add_string() for details of the format.
   // 
   // If the string is invalid then %FALSE is returned and the path is
   // unaltered.
   // RETURNS: %TRUE is the path was valid, %FALSE otherwise.
   // <str>: a string describing the path
   int set_description(AT0)(AT0 /*char*/ str) nothrow {
      return clutter_path_set_description(&this, toCString!(char*)(str));
   }

   // VERSION: 1.0
   // Add the nodes of the ClutterPath to the path in the Cairo context.
   // <cr>: a Cairo context
   void to_cairo_path(AT0)(AT0 /*cairo.Context*/ cr) nothrow {
      clutter_path_to_cairo_path(&this, UpCast!(cairo.Context*)(cr));
   }
}


// VERSION: 1.0
// This function is passed to clutter_path_foreach() and will be
// called for each node contained in the path.
// <node>: the node
// <data>: optional data passed to the function
extern (C) alias void function (PathNode* node, void* data) nothrow PathCallback;

// The #ClutterPathClass struct contains only private data.
struct PathClass /* Version 1.0 */ {
   private GObject2.InitiallyUnownedClass parent_class;
}


// <structname>ClutterPathConstraint</structname> is an opaque structure
// whose members cannot be directly accessed
struct PathConstraint /* : Constraint */ /* Version 1.6 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent constraint;
   Constraint method_parent;


   // VERSION: 1.6
   // Creates a new #ClutterPathConstraint with the given @path and @offset
   // RETURNS: the newly created #ClutterPathConstraint
   // <path>: a #ClutterPath, or %NULL
   // <offset>: the offset along the #ClutterPath
   static PathConstraint* /*new*/ new_(AT0)(AT0 /*Path*/ path, float offset) nothrow {
      return clutter_path_constraint_new(UpCast!(Path*)(path), offset);
   }
   static auto opCall(AT0)(AT0 /*Path*/ path, float offset) {
      return clutter_path_constraint_new(UpCast!(Path*)(path), offset);
   }

   // VERSION: 1.6
   // Retrieves the offset along the #ClutterPath used by @constraint.
   // RETURNS: the offset
   float get_offset()() nothrow {
      return clutter_path_constraint_get_offset(&this);
   }

   // VERSION: 1.6
   // Retrieves a pointer to the #ClutterPath used by @constraint.
   // 
   // #ClutterPathConstraint, or %NULL. The returned #ClutterPath is owned
   // by the constraint and it should not be unreferenced
   // RETURNS: the #ClutterPath used by the
   Path* get_path()() nothrow {
      return clutter_path_constraint_get_path(&this);
   }

   // VERSION: 1.6
   // Sets the offset along the #ClutterPath used by @constraint.
   // <offset>: the offset along the path
   void set_offset()(float offset) nothrow {
      clutter_path_constraint_set_offset(&this, offset);
   }

   // VERSION: 1.6
   // Sets the @path to be followed by the #ClutterPathConstraint.
   // 
   // The @constraint will take ownership of the #ClutterPath passed to this
   // function.
   // <path>: a #ClutterPath
   void set_path(AT0)(AT0 /*Path*/ path=null) nothrow {
      clutter_path_constraint_set_path(&this, UpCast!(Path*)(path));
   }

   // VERSION: 1.6
   // The ::node-reached signal is emitted each time a
   // #ClutterPathConstraint:offset value results in the actor
   // passing a #ClutterPathNode
   // <actor>: the #ClutterActor using the @constraint
   // <index>: the index of the node that has been reached
   extern (C) alias static void function (PathConstraint* this_, Actor* actor, c_uint index, void* user_data=null) nothrow signal_node_reached;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"node-reached", CB/*:signal_node_reached*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_node_reached)||_ttmm!(CB, signal_node_reached)()) {
      return signal_connect_data!()(&this, cast(char*)"node-reached",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct PathConstraintClass {
}


// Represents a single node of a #ClutterPath.
// 
// Some of the coordinates in @points may be unused for some node
// types. %CLUTTER_PATH_MOVE_TO and %CLUTTER_PATH_LINE_TO use only one
// pair of coordinates, %CLUTTER_PATH_CURVE_TO uses all three and
// %CLUTTER_PATH_CLOSE uses none.
struct PathNode /* Version 1.0 */ {
   PathNodeType type;
   Knot[3] points;


   // VERSION: 1.0
   // Makes an allocated copy of a node.
   // RETURNS: the copied node.
   PathNode* /*new*/ copy()() nothrow {
      return clutter_path_node_copy(&this);
   }

   // VERSION: 1.0
   // Compares two nodes and checks if they are the same type with the
   // same coordinates.
   // RETURNS: %TRUE if the nodes are the same.
   // <node_b>: Second node
   int equal(AT0)(AT0 /*PathNode*/ node_b) nothrow {
      return clutter_path_node_equal(&this, UpCast!(PathNode*)(node_b));
   }

   // VERSION: 1.0
   // Frees the memory of an allocated node.
   void free()() nothrow {
      clutter_path_node_free(&this);
   }
}

// Types of nodes in a #ClutterPath.
enum PathNodeType /* Version 1.0 */ {
   MOVE_TO = 0,
   LINE_TO = 1,
   CURVE_TO = 2,
   CLOSE = 3,
   REL_MOVE_TO = 32,
   REL_LINE_TO = 33,
   REL_CURVE_TO = 34
}
struct PathPrivate {
}

enum int Pause = 65299;

// Stage perspective definition. #ClutterPerspective is only used by
// the fixed point version of clutter_stage_set_perspective().
struct Perspective /* Version 0.4 */ {
   float fovy, aspect, z_near, z_far;
}

enum int PesetaSign = 16785575;
enum int Phone = 269025134;
// Controls the paint cycle of the scene graph when in pick mode
enum PickMode /* Version 1.0 */ {
   NONE = 0,
   REACTIVE = 1,
   ALL = 2
}
enum int Pictures = 269025169;
enum int Pointer_Accelerate = 65274;
enum int Pointer_Button1 = 65257;
enum int Pointer_Button2 = 65258;
enum int Pointer_Button3 = 65259;
enum int Pointer_Button4 = 65260;
enum int Pointer_Button5 = 65261;
enum int Pointer_Button_Dflt = 65256;
enum int Pointer_DblClick1 = 65263;
enum int Pointer_DblClick2 = 65264;
enum int Pointer_DblClick3 = 65265;
enum int Pointer_DblClick4 = 65266;
enum int Pointer_DblClick5 = 65267;
enum int Pointer_DblClick_Dflt = 65262;
enum int Pointer_DfltBtnNext = 65275;
enum int Pointer_DfltBtnPrev = 65276;
enum int Pointer_Down = 65251;
enum int Pointer_DownLeft = 65254;
enum int Pointer_DownRight = 65255;
enum int Pointer_Drag1 = 65269;
enum int Pointer_Drag2 = 65270;
enum int Pointer_Drag3 = 65271;
enum int Pointer_Drag4 = 65272;
enum int Pointer_Drag5 = 65277;
enum int Pointer_Drag_Dflt = 65268;
enum int Pointer_EnableKeys = 65273;
enum int Pointer_Left = 65248;
enum int Pointer_Right = 65249;
enum int Pointer_Up = 65250;
enum int Pointer_UpLeft = 65252;
enum int Pointer_UpRight = 65253;
enum int PowerDown = 269025057;
enum int PowerOff = 269025066;
enum int Prev_VMode = 269024803;
enum int Prev_Virtual_Screen = 65233;
enum int PreviousCandidate = 65342;
enum int Print = 65377;
enum int Prior = 65365;

// VERSION: 1.0
// Prototype of the progress function used to compute the value
// between the two ends @a and @b of an interval depending on
// the value of @progress.
// 
// The #GValue in @retval is already initialized with the same
// type as @a and @b.
// 
// This function will be called by #ClutterInterval if the
// type of the values of the interval was registered using
// clutter_interval_register_progress_func().
// 
// the value and stored it inside @retval
// RETURNS: %TRUE if the function successfully computed
// <a>: the initial value of an interval
// <b>: the final value of an interval
// <progress>: the progress factor, between 0 and 1
// <retval>: the value used to store the progress
extern (C) alias int function (GObject2.Value* a, GObject2.Value* b, double progress, GObject2.Value* retval) nothrow ProgressFunc;

enum int Q = 81;
enum int R = 82;
enum int R1 = 65490;
enum int R10 = 65499;
enum int R11 = 65500;
enum int R12 = 65501;
enum int R13 = 65502;
enum int R14 = 65503;
enum int R15 = 65504;
enum int R2 = 65491;
enum int R3 = 65492;
enum int R4 = 65493;
enum int R5 = 65494;
enum int R6 = 65495;
enum int R7 = 65496;
enum int R8 = 65497;
enum int R9 = 65498;
enum int Racute = 448;
enum int Rcaron = 472;
enum int Rcedilla = 931;

// The #ClutterRectangle structure contains only private data
// and should be accessed using the provided API
struct Rectangle /* : Actor */ /* Version 0.1 */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Animatable.__interface__;
   mixin Container.__interface__;
   mixin Scriptable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent actor;
   Actor parent;
   private RectanglePrivate* priv;


   // Creates a new #ClutterActor with a rectangular shape.
   // RETURNS: a new #ClutterActor
   static Rectangle* new_()() nothrow {
      return clutter_rectangle_new();
   }
   static auto opCall()() {
      return clutter_rectangle_new();
   }

   // Creates a new #ClutterActor with a rectangular shape
   // and of the given @color.
   // RETURNS: a new #ClutterActor
   // <color>: a #ClutterColor
   static Rectangle* new_with_color(AT0)(AT0 /*Color*/ color) nothrow {
      return clutter_rectangle_new_with_color(UpCast!(Color*)(color));
   }
   static auto opCall(AT0)(AT0 /*Color*/ color) {
      return clutter_rectangle_new_with_color(UpCast!(Color*)(color));
   }

   // VERSION: 0.2
   // Gets the color of the border used by @rectangle and places
   // it into @color.
   // <color>: return location for a #ClutterColor
   void get_border_color(AT0)(/*out*/ AT0 /*Color*/ color) nothrow {
      clutter_rectangle_get_border_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 0.2
   // Gets the width (in pixels) of the border used by @rectangle
   // RETURNS: the border's width
   uint get_border_width()() nothrow {
      return clutter_rectangle_get_border_width(&this);
   }

   // Retrieves the color of @rectangle.
   // <color>: return location for a #ClutterColor
   void get_color(AT0)(/*out*/ AT0 /*Color*/ color) nothrow {
      clutter_rectangle_get_color(&this, UpCast!(Color*)(color));
   }

   // Sets the color of the border used by @rectangle using @color
   // <color>: the color of the border
   void set_border_color(AT0)(AT0 /*Color*/ color) nothrow {
      clutter_rectangle_set_border_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 0.2
   // Sets the width (in pixel) of the border used by @rectangle.
   // A @width of 0 will unset the border.
   // <width>: the width of the border
   void set_border_width()(uint width) nothrow {
      clutter_rectangle_set_border_width(&this, width);
   }

   // Sets the color of @rectangle.
   // <color>: a #ClutterColor
   void set_color(AT0)(AT0 /*Color*/ color) nothrow {
      clutter_rectangle_set_color(&this, UpCast!(Color*)(color));
   }
}

// The #ClutterRectangleClass structure contains only private data
struct RectangleClass /* Version 0.1 */ {
   private ActorClass parent_class;
   extern (C) void function () nothrow _clutter_rectangle1;
   extern (C) void function () nothrow _clutter_rectangle2;
   extern (C) void function () nothrow _clutter_rectangle3;
   extern (C) void function () nothrow _clutter_rectangle4;
}

struct RectanglePrivate {
}

enum int Red = 269025187;
enum int Redo = 65382;
enum int Refresh = 269025065;
enum int Reload = 269025139;
enum int RepeatKeys_Enable = 65138;
enum int Reply = 269025138;
// Specifies the type of requests for a #ClutterActor.
enum RequestMode /* Version 0.8 */ {
   HEIGHT_FOR_WIDTH = 0,
   WIDTH_FOR_HEIGHT = 1
}
enum int Return = 65293;
enum int Right = 65363;
enum int RockerDown = 269025060;
enum int RockerEnter = 269025061;
enum int RockerUp = 269025059;
enum int Romaji = 65316;
// Axis of a rotation.
enum RotateAxis /* Version 0.4 */ {
   X_AXIS = 0,
   Y_AXIS = 1,
   Z_AXIS = 2
}
// Direction of a rotation.
enum RotateDirection /* Version 0.4 */ {
   CW = 0,
   CCW = 1
}
enum int RotateWindows = 269025140;
enum int RotationKB = 269025142;
enum int RotationPB = 269025141;
enum int RupeeSign = 16785576;
enum int S = 83;
enum int SCHWA = 16777615;
enum int Sabovedot = 16784992;
enum int Sacute = 422;
enum int Save = 269025143;
enum int Scaron = 425;
enum int Scedilla = 426;
enum int Scircumflex = 734;

// The #ClutterScore structure contains only private data
// and should be accessed using the provided API
struct Score /* : GObject.Object */ /* Version 0.6 */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   private ScorePrivate* priv;


   // VERSION: 0.6
   // Creates a new #ClutterScore. A #ClutterScore is an object that can
   // hold multiple #ClutterTimeline<!-- -->s in a sequential order.
   // 
   // when done.
   // RETURNS: the newly created #ClutterScore. Use g_object_unref()
   static Score* /*new*/ new_()() nothrow {
      return clutter_score_new();
   }
   static auto opCall()() {
      return clutter_score_new();
   }

   // VERSION: 0.6
   // Appends a timeline to another one existing in the score; the newly
   // appended timeline will be started when @parent is complete.
   // 
   // If @parent is %NULL, the new #ClutterTimeline will be started when
   // clutter_score_start() is called.
   // 
   // #ClutterScore will take a reference on @timeline.
   // 
   // 0 on failure. The returned id can be used with clutter_score_remove()
   // or clutter_score_get_timeline().
   // RETURNS: the id of the #ClutterTimeline inside the score, or
   // <parent>: a #ClutterTimeline in the score, or %NULL
   // <timeline>: a #ClutterTimeline
   c_ulong append(AT0, AT1)(AT0 /*Timeline*/ parent, AT1 /*Timeline*/ timeline) nothrow {
      return clutter_score_append(&this, UpCast!(Timeline*)(parent), UpCast!(Timeline*)(timeline));
   }

   // VERSION: 0.8
   // Appends @timeline at the given @marker_name on the @parent
   // #ClutterTimeline.
   // 
   // If you want to append @timeline at the end of @parent, use
   // clutter_score_append().
   // 
   // The #ClutterScore will take a reference on @timeline.
   // 
   // 0 on failure. The returned id can be used with clutter_score_remove()
   // or clutter_score_get_timeline().
   // RETURNS: the id of the #ClutterTimeline inside the score, or
   // <parent>: the parent #ClutterTimeline
   // <marker_name>: the name of the marker to use
   // <timeline>: the #ClutterTimeline to append
   c_ulong append_at_marker(AT0, AT1, AT2)(AT0 /*Timeline*/ parent, AT1 /*char*/ marker_name, AT2 /*Timeline*/ timeline) nothrow {
      return clutter_score_append_at_marker(&this, UpCast!(Timeline*)(parent), toCString!(char*)(marker_name), UpCast!(Timeline*)(timeline));
   }

   // VERSION: 0.6
   // Gets whether @score is looping
   // RETURNS: %TRUE if the score is looping
   int get_loop()() nothrow {
      return clutter_score_get_loop(&this);
   }

   // VERSION: 0.6
   // Retrieves the #ClutterTimeline for @id_ inside @score.
   // 
   // function does not increase the reference count on the returned
   // #ClutterTimeline
   // RETURNS: the requested timeline, or %NULL. This
   // <id_>: the id of the timeline
   Timeline* get_timeline()(c_ulong id_) nothrow {
      return clutter_score_get_timeline(&this, id_);
   }

   // VERSION: 0.6
   // Query state of a #ClutterScore instance.
   // RETURNS: %TRUE if score is currently playing
   int is_playing()() nothrow {
      return clutter_score_is_playing(&this);
   }

   // VERSION: 0.6
   // Retrieves a list of all the #ClutterTimelines managed by @score.
   // 
   // #GSList containing all the timelines in the score. This function does
   // not increase the reference count of the returned timelines. Use
   // g_slist_free() on the returned list to deallocate its resources.
   // RETURNS: a
   GLib2.SList* /*new container*/ list_timelines()() nothrow {
      return clutter_score_list_timelines(&this);
   }

   // VERSION: 0.6
   // Pauses a playing score @score.
   void pause()() nothrow {
      clutter_score_pause(&this);
   }

   // VERSION: 0.6
   // Removes the #ClutterTimeline with the given id inside @score. If
   // the timeline has other timelines attached to it, those are removed
   // as well.
   // <id_>: the id of the timeline to remove
   void remove()(c_ulong id_) nothrow {
      clutter_score_remove(&this, id_);
   }

   // VERSION: 0.6
   // Removes all the timelines inside @score.
   void remove_all()() nothrow {
      clutter_score_remove_all(&this);
   }

   // VERSION: 0.6
   // Rewinds a #ClutterScore to its initial state.
   void rewind()() nothrow {
      clutter_score_rewind(&this);
   }

   // VERSION: 0.6
   // Sets whether @score should loop. A looping #ClutterScore will start
   // from its initial state after the ::complete signal has been fired.
   // <loop>: %TRUE for enable looping
   void set_loop()(int loop) nothrow {
      clutter_score_set_loop(&this, loop);
   }

   // VERSION: 0.6
   // Starts the score.
   void start()() nothrow {
      clutter_score_start(&this);
   }

   // VERSION: 0.6
   // Stops and rewinds a playing #ClutterScore instance.
   void stop()() nothrow {
      clutter_score_stop(&this);
   }

   // VERSION: 0.6
   // The ::completed signal is emitted each time a #ClutterScore terminates.
   extern (C) alias static void function (Score* this_, void* user_data=null) nothrow signal_completed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"completed", CB/*:signal_completed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_completed)||_ttmm!(CB, signal_completed)()) {
      return signal_connect_data!()(&this, cast(char*)"completed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::paused signal is emitted each time a #ClutterScore
   // is paused.
   extern (C) alias static void function (Score* this_, void* user_data=null) nothrow signal_paused;
   ulong signal_connect(string name:"paused", CB/*:signal_paused*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_paused)||_ttmm!(CB, signal_paused)()) {
      return signal_connect_data!()(&this, cast(char*)"paused",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::started signal is emitted each time a #ClutterScore starts playing.
   extern (C) alias static void function (Score* this_, void* user_data=null) nothrow signal_started;
   ulong signal_connect(string name:"started", CB/*:signal_started*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_started)||_ttmm!(CB, signal_started)()) {
      return signal_connect_data!()(&this, cast(char*)"started",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::timeline-completed signal is emitted each time a timeline
   // inside a #ClutterScore terminates.
   // <timeline>: the completed timeline
   extern (C) alias static void function (Score* this_, Timeline* timeline, void* user_data=null) nothrow signal_timeline_completed;
   ulong signal_connect(string name:"timeline-completed", CB/*:signal_timeline_completed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_timeline_completed)||_ttmm!(CB, signal_timeline_completed)()) {
      return signal_connect_data!()(&this, cast(char*)"timeline-completed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::timeline-started signal is emitted each time a new timeline
   // inside a #ClutterScore starts playing.
   // <timeline>: the current timeline
   extern (C) alias static void function (Score* this_, Timeline* timeline, void* user_data=null) nothrow signal_timeline_started;
   ulong signal_connect(string name:"timeline-started", CB/*:signal_timeline_started*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_timeline_started)||_ttmm!(CB, signal_timeline_started)()) {
      return signal_connect_data!()(&this, cast(char*)"timeline-started",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// The #ClutterScoreClass structure contains only private data
struct ScoreClass /* Version 0.6 */ {
   private GObject2.ObjectClass parent_class;
   extern (C) void function (Score* score, Timeline* timeline) nothrow timeline_started;
   extern (C) void function (Score* score, Timeline* timeline) nothrow timeline_completed;
   extern (C) void function (Score* score) nothrow started;
   extern (C) void function (Score* score) nothrow completed;
   extern (C) void function (Score* score) nothrow paused;
   extern (C) void function () nothrow _clutter_score_1;
   extern (C) void function () nothrow _clutter_score_2;
   extern (C) void function () nothrow _clutter_score_3;
   extern (C) void function () nothrow _clutter_score_4;
   extern (C) void function () nothrow _clutter_score_5;
}

struct ScorePrivate {
}

enum int ScreenSaver = 269025069;

// The #ClutterScript structure contains only private data
// and should be accessed using the provided API
struct Script /* : GObject.Object */ /* Version 0.6 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private ScriptPrivate* priv;


   // VERSION: 0.6
   // Creates a new #ClutterScript instance. #ClutterScript can be used
   // to load objects definitions for scenegraph elements, like actors,
   // or behavioural elements, like behaviours and timelines. The
   // definitions must be encoded using the JavaScript Object Notation (JSON)
   // language.
   // 
   // g_object_unref() when done.
   // RETURNS: the newly created #ClutterScript instance. Use
   static Script* /*new*/ new_()() nothrow {
      return clutter_script_new();
   }
   static auto opCall()() {
      return clutter_script_new();
   }

   // VERSION: 0.8
   // Adds @paths to the list of search paths held by @script.
   // 
   // The search paths are used by clutter_script_lookup_filename(), which
   // can be used to define search paths for the textures source file name
   // or other custom, file-based properties.
   // <paths>: an array of strings containing different search paths
   // <n_paths>: the length of the passed array
   void add_search_paths(AT0)(AT0 /*char*/ paths, size_t n_paths) nothrow {
      clutter_script_add_search_paths(&this, toCString!(char*)(paths), n_paths);
   }

   // VERSION: 1.8
   // Associates a #ClutterState to the #ClutterScript instance using the given
   // name.
   // 
   // The #ClutterScript instance will use @state to resolve target states when
   // connecting signal handlers.
   // 
   // The #ClutterScript instance will take a reference on the #ClutterState
   // passed to this function.
   // <name>: a name for the @state, or %NULL to set the default #ClutterState
   // <state>: a #ClutterState
   void add_states(AT0, AT1)(AT0 /*char*/ name, AT1 /*State*/ state) nothrow {
      clutter_script_add_states(&this, toCString!(char*)(name), UpCast!(State*)(state));
   }

   // VERSION: 0.6
   // Connects all the signals defined into a UI definition file to their
   // handlers.
   // 
   // This method invokes clutter_script_connect_signals_full() internally
   // and uses  #GModule's introspective features (by opening the current
   // module's scope) to look at the application's symbol table.
   // 
   // Note that this function will not work if #GModule is not supported by
   // the platform Clutter is running on.
   // <user_data>: data to be passed to the signal handlers, or %NULL
   void connect_signals(AT0)(AT0 /*void*/ user_data) nothrow {
      clutter_script_connect_signals(&this, UpCast!(void*)(user_data));
   }

   // VERSION: 0.6
   // Connects all the signals defined into a UI definition file to their
   // handlers.
   // 
   // This function allows to control how the signal handlers are
   // going to be connected to their respective signals. It is meant
   // primarily for language bindings to allow resolving the function
   // names using the native API, but it can also be used on platforms
   // that do not support GModule.
   // 
   // Applications should use clutter_script_connect_signals().
   // <func>: signal connection function
   // <user_data>: data to be passed to the signal handlers, or %NULL
   void connect_signals_full(AT0)(ScriptConnectFunc func, AT0 /*void*/ user_data) nothrow {
      clutter_script_connect_signals_full(&this, func, UpCast!(void*)(user_data));
   }

   // VERSION: 0.6
   // Ensure that every object defined inside @script is correctly
   // constructed. You should rarely need to use this function.
   void ensure_objects()() nothrow {
      clutter_script_ensure_objects(&this);
   }

   // VERSION: 0.6
   // Retrieves the object bound to @name. This function does not increment
   // the reference count of the returned object.
   // 
   // with the given name was available
   // RETURNS: the named object, or %NULL if no object
   // <name>: the name of the object to retrieve
   GObject2.Object* get_object(AT0)(AT0 /*char*/ name) nothrow {
      return clutter_script_get_object(&this, toCString!(char*)(name));
   }

   // Unintrospectable method: get_objects() / clutter_script_get_objects()
   // VERSION: 0.6
   // Retrieves a list of objects for the given names. After @script, object
   // names/return location pairs should be listed, with a %NULL pointer
   // ending the list, like:
   // 
   // <informalexample><programlisting>
   // GObject *my_label, *a_button, *main_timeline;
   // 
   // clutter_script_get_objects (script,
   // "my-label", &amp;my_label,
   // "a-button", &amp;a_button,
   // "main-timeline", &amp;main_timeline,
   // NULL);
   // </programlisting></informalexample>
   // 
   // Note: This function does not increment the reference count of the
   // returned objects.
   // RETURNS: the number of objects returned.
   // <first_name>: the name of the first object to retrieve
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_script_get_objects get_objects; // Variadic
   +/

   // VERSION: 1.8
   // Retrieves the #ClutterState for the given @state_name.
   // 
   // If @name is %NULL, this function will return the default
   // #ClutterState instance.
   // 
   // given name. The #ClutterState is owned by the #ClutterScript instance
   // and it should not be unreferenced
   // RETURNS: a pointer to the #ClutterState for the
   // <name>: the name of the #ClutterState, or %NULL
   State* get_states(AT0)(AT0 /*char*/ name=null) nothrow {
      return clutter_script_get_states(&this, toCString!(char*)(name));
   }

   // VERSION: 0.6
   // Looks up a type by name, using the virtual function that 
   // #ClutterScript has for that purpose. This function should
   // rarely be used.
   // 
   // %G_TYPE_INVALID if not corresponding type was found.
   // RETURNS: the type for the requested type name, or
   // <type_name>: name of the type to look up
   Type get_type_from_name(AT0)(AT0 /*char*/ type_name) nothrow {
      return clutter_script_get_type_from_name(&this, toCString!(char*)(type_name));
   }

   // VERSION: 0.8.2
   // Retrieves all the objects created by @script.
   // 
   // Note: this function does not increment the reference count of the
   // objects it returns.
   // 
   // of #GObject<!-- -->s, or %NULL. The objects are owned by the
   // #ClutterScript instance. Use g_list_free() on the returned list when
   // done.
   // RETURNS: a list
   GLib2.List* /*new container*/ list_objects()() nothrow {
      return clutter_script_list_objects(&this);
   }

   // VERSION: 0.6
   // Loads the definitions from @data into @script and merges with
   // the currently loaded ones, if any.
   // 
   // accordingly. On success, the merge id for the UI definitions is
   // returned. You can use the merge id with clutter_script_unmerge_objects().
   // RETURNS: on error, zero is returned and @error is set
   // <data>: a buffer containing the definitions
   // <length>: the length of the buffer, or -1 if @data is a NUL-terminated buffer
   uint load_from_data(AT0, AT1)(AT0 /*char*/ data, ssize_t length, AT1 /*GLib2.Error**/ error=null) nothrow {
      return clutter_script_load_from_data(&this, toCString!(char*)(data), length, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 0.6
   // Loads the definitions from @filename into @script and merges with
   // the currently loaded ones, if any.
   // 
   // accordingly. On success, the merge id for the UI definitions is
   // returned. You can use the merge id with clutter_script_unmerge_objects().
   // RETURNS: on error, zero is returned and @error is set
   // <filename>: the full path to the definition file
   uint load_from_file(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) nothrow {
      return clutter_script_load_from_file(&this, toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.10
   // Loads the definitions from a resource file into @script and merges with
   // the currently loaded ones, if any.
   // 
   // accordingly. On success, the merge id for the UI definitions is
   // returned. You can use the merge id with clutter_script_unmerge_objects().
   // RETURNS: on error, zero is returned and @error is set
   // <resource_path>: the resource path of the file to parse
   uint load_from_resource(AT0, AT1)(AT0 /*char*/ resource_path, AT1 /*GLib2.Error**/ error=null) nothrow {
      return clutter_script_load_from_resource(&this, toCString!(char*)(resource_path), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 0.8
   // Looks up @filename inside the search paths of @script. If @filename
   // is found, its full path will be returned .
   // 
   // found.
   // RETURNS: the full path of @filename or %NULL if no path was
   // <filename>: the name of the file to lookup
   char* /*new*/ lookup_filename(AT0)(AT0 /*char*/ filename) nothrow {
      return clutter_script_lookup_filename(&this, toCString!(char*)(filename));
   }

   // VERSION: 0.6
   // Unmerges the objects identified by @merge_id.
   // <merge_id>: merge id returned when loading a UI definition
   void unmerge_objects()(uint merge_id) nothrow {
      clutter_script_unmerge_objects(&this, merge_id);
   }
}

// The #ClutterScriptClass structure contains only private data
struct ScriptClass /* Version 0.6 */ {
   private GObject2.ObjectClass parent_class;

   // RETURNS: the type for the requested type name, or
   // <type_name>: name of the type to look up
   extern (C) Type function (Script* script, char* type_name) nothrow get_type_from_name;
   extern (C) void function () nothrow _clutter_reserved1;
   extern (C) void function () nothrow _clutter_reserved2;
   extern (C) void function () nothrow _clutter_reserved3;
   extern (C) void function () nothrow _clutter_reserved4;
   extern (C) void function () nothrow _clutter_reserved5;
   extern (C) void function () nothrow _clutter_reserved6;
   extern (C) void function () nothrow _clutter_reserved7;
   extern (C) void function () nothrow _clutter_reserved8;
}


// VERSION: 0.6
// This is the signature of a function used to connect signals.  It is used
// by the clutter_script_connect_signals_full() function.  It is mainly
// intended for interpreted language bindings, but could be useful where the
// programmer wants more control over the signal connection process.
// <script>: a #ClutterScript
// <object>: the object to connect
// <signal_name>: the name of the signal
// <handler_name>: the name of the signal handler
// <connect_object>: the object to connect the signal to, or %NULL
// <flags>: signal connection flags
// <user_data>: user data to pass to the signal handler
extern (C) alias void function (Script* script, GObject2.Object* object, char* signal_name, char* handler_name, GObject2.Object* connect_object, GObject2.ConnectFlags flags, void* user_data) nothrow ScriptConnectFunc;

// #ClutterScript error enumeration.
enum ScriptError /* Version 0.6 */ {
   TYPE_FUNCTION = 0,
   PROPERTY = 1,
   VALUE = 2
}
struct ScriptPrivate {
}


// #ClutterScriptable is an opaque structure whose members cannot be directly
// accessed
struct Scriptable /* Interface */ /* Version 0.6 */ {
   mixin template __interface__() {
      // VERSION: 0.6
      // Retrieves the id of @scriptable set using clutter_scriptable_set_id().
      // 
      // the scriptable object and should never be modified of freed
      // RETURNS: the id of the object. The returned string is owned by
      char* get_id()() nothrow {
         return clutter_scriptable_get_id(cast(Scriptable*)&this);
      }

      // VERSION: 0.6
      // Parses the passed JSON node. The implementation must set the type
      // of the passed #GValue pointer using g_value_init().
      // RETURNS: %TRUE if the node was successfully parsed, %FALSE otherwise.
      // <script>: the #ClutterScript creating the scriptable instance
      // <value>: the generic value to be set
      // <name>: the name of the node
      // <node>: the JSON node to be parsed
      int parse_custom_node(AT0, AT1, AT2, AT3)(AT0 /*Script*/ script, AT1 /*GObject2.Value*/ value, AT2 /*char*/ name, AT3 /*Json.Node*/ node) nothrow {
         return clutter_scriptable_parse_custom_node(cast(Scriptable*)&this, UpCast!(Script*)(script), UpCast!(GObject2.Value*)(value), toCString!(char*)(name), UpCast!(Json.Node*)(node));
      }

      // VERSION: 0.6
      // Overrides the common properties setting. The underlying virtual
      // function should be used when implementing custom properties.
      // <script>: the #ClutterScript creating the scriptable instance
      // <name>: the name of the property
      // <value>: the value of the property
      void set_custom_property(AT0, AT1, AT2)(AT0 /*Script*/ script, AT1 /*char*/ name, AT2 /*GObject2.Value*/ value) nothrow {
         clutter_scriptable_set_custom_property(cast(Scriptable*)&this, UpCast!(Script*)(script), toCString!(char*)(name), UpCast!(GObject2.Value*)(value));
      }

      // VERSION: 0.6
      // Sets @id_ as the unique Clutter script it for this instance of
      // #ClutterScriptableIface.
      // 
      // This name can be used by user interface designer applications to
      // define a unique name for an object constructable using the UI
      // definition language parsed by #ClutterScript.
      // <id_>: the #ClutterScript id of the object
      void set_id(AT0)(AT0 /*char*/ id_) nothrow {
         clutter_scriptable_set_id(cast(Scriptable*)&this, toCString!(char*)(id_));
      }
   }
   mixin __interface__;
}


// Interface for implementing "scriptable" objects. An object implementing
// this interface can override the parsing and properties setting sequence
// when loading a UI definition data with #ClutterScript
struct ScriptableIface /* Version 0.6 */ {
   private GObject2.TypeInterface g_iface;
   // <id_>: the #ClutterScript id of the object
   extern (C) void function (Scriptable* scriptable, char* id_) nothrow set_id;
   // RETURNS: the id of the object. The returned string is owned by
   extern (C) char* function (Scriptable* scriptable) nothrow get_id;

   // RETURNS: %TRUE if the node was successfully parsed, %FALSE otherwise.
   // <script>: the #ClutterScript creating the scriptable instance
   // <value>: the generic value to be set
   // <name>: the name of the node
   // <node>: the JSON node to be parsed
   extern (C) int function (Scriptable* scriptable, Script* script, GObject2.Value* value, char* name, Json.Node* node) nothrow parse_custom_node;

   // <script>: the #ClutterScript creating the scriptable instance
   // <name>: the name of the property
   // <value>: the value of the property
   extern (C) void function (Scriptable* scriptable, Script* script, char* name, GObject2.Value* value) nothrow set_custom_property;
}

enum int ScrollClick = 269025146;
// Direction of a pointer scroll event.
enum ScrollDirection /* Version 0.4 */ {
   UP = 0,
   DOWN = 1,
   LEFT = 2,
   RIGHT = 3
}
enum int ScrollDown = 269025145;
// Scroll wheel (or similar device) event
struct ScrollEvent /* Version 0.2 */ {
   EventType type;
   uint time;
   EventFlags flags;
   Stage* stage;
   Actor* source;
   float x, y;
   ScrollDirection direction;
   ModifierType modifier_state;
   double* axes;
   InputDevice* device;
}

enum int ScrollUp = 269025144;
enum int Scroll_Lock = 65300;
enum int Search = 269025051;
enum int Select = 65376;
enum int SelectButton = 269025184;
enum int Send = 269025147;
enum int Serbian_DJE = 1713;
enum int Serbian_DZE = 1727;
enum int Serbian_JE = 1720;
enum int Serbian_LJE = 1721;
enum int Serbian_NJE = 1722;
enum int Serbian_TSHE = 1723;
enum int Serbian_dje = 1697;
enum int Serbian_dze = 1711;
enum int Serbian_je = 1704;
enum int Serbian_lje = 1705;
enum int Serbian_nje = 1706;
enum int Serbian_tshe = 1707;

// <structname>ClutterSettings</structname> is an opaque structure whose
// members cannot be directly accessed.
struct Settings /* : GObject.Object */ /* Version 1.4 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // VERSION: 1.4
   // Retrieves the singleton instance of #ClutterSettings
   // 
   // returned object is owned by Clutter and it should not be unreferenced
   // directly
   // RETURNS: the instance of #ClutterSettings. The
   static Settings* get_default()() nothrow {
      return clutter_settings_get_default();
   }
}

struct SettingsClass {
}


// The #ClutterShader structure contains only private data
// and should be accessed using the provided API
struct Shader /* : GObject.Object */ /* Version 0.6 */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   private ShaderPrivate* priv;


   // VERSION: 0.6
   // DEPRECATED (v1.8) constructor: new - Use #ClutterShaderEffect instead.
   // Create a new #ClutterShader instance.
   // RETURNS: a new #ClutterShader.
   static Shader* /*new*/ new_()() nothrow {
      return clutter_shader_new();
   }
   static auto opCall()() {
      return clutter_shader_new();
   }
   static GLib2.Quark error_quark()() nothrow {
      return clutter_shader_error_quark();
   }

   // VERSION: 0.8
   // DEPRECATED (v1.8) method: compile - Use #ClutterShaderEffect instead.
   // Compiles and links GLSL sources set for vertex and fragment shaders for
   // a #ClutterShader. If the compilation fails and a #GError return location is
   // provided the error will contain the errors from the compiler, if any.
   // RETURNS: returns TRUE if the shader was succesfully compiled.
   int compile(AT0)(AT0 /*GLib2.Error**/ error=null) nothrow {
      return clutter_shader_compile(&this, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.0
   // DEPRECATED (v1.8) method: get_cogl_fragment_shader - Use #ClutterShaderEffect instead.
   // Retrieves the underlying #CoglHandle for the fragment shader.
   // 
   // shader, or %NULL. The handle is owned by the #ClutterShader
   // and it should not be unreferenced
   // RETURNS: A #CoglHandle for the fragment
   Cogl.Handle get_cogl_fragment_shader()() nothrow {
      return clutter_shader_get_cogl_fragment_shader(&this);
   }

   // VERSION: 1.0
   // DEPRECATED (v1.8) method: get_cogl_program - Use #ClutterShaderEffect instead.
   // Retrieves the underlying #CoglHandle for the shader program.
   // 
   // or %NULL. The handle is owned by the #ClutterShader and it should
   // not be unreferenced
   // RETURNS: A #CoglHandle for the shader program,
   Cogl.Handle get_cogl_program()() nothrow {
      return clutter_shader_get_cogl_program(&this);
   }

   // VERSION: 1.0
   // DEPRECATED (v1.8) method: get_cogl_vertex_shader - Use #ClutterShaderEffect instead.
   // Retrieves the underlying #CoglHandle for the vertex shader.
   // 
   // shader, or %NULL. The handle is owned by the #ClutterShader
   // and it should not be unreferenced
   // RETURNS: A #CoglHandle for the vertex
   Cogl.Handle get_cogl_vertex_shader()() nothrow {
      return clutter_shader_get_cogl_vertex_shader(&this);
   }

   // VERSION: 0.6
   // DEPRECATED (v1.8) method: get_fragment_source - Use #ClutterShaderEffect instead.
   // Query the current GLSL fragment source set on @shader.
   // 
   // ClutterShader object or %NULL. The returned string is owned by the
   // shader object and should never be modified or freed
   // RETURNS: the source of the fragment shader for this
   char* get_fragment_source()() nothrow {
      return clutter_shader_get_fragment_source(&this);
   }

   // VERSION: 0.6
   // DEPRECATED (v1.8) method: get_is_enabled - Use #ClutterShaderEffect instead.
   // Checks whether @shader is enabled.
   // RETURNS: %TRUE if the shader is enabled.
   int get_is_enabled()() nothrow {
      return clutter_shader_get_is_enabled(&this);
   }

   // VERSION: 0.6
   // DEPRECATED (v1.8) method: get_vertex_source - Use #ClutterShaderEffect instead.
   // Query the current GLSL vertex source set on @shader.
   // 
   // ClutterShader object or %NULL. The returned string is owned by the
   // shader object and should never be modified or freed
   // RETURNS: the source of the vertex shader for this
   char* get_vertex_source()() nothrow {
      return clutter_shader_get_vertex_source(&this);
   }

   // VERSION: 0.8
   // DEPRECATED (v1.8) method: is_compiled - Use #ClutterShaderEffect instead.
   // Checks whether @shader is is currently compiled, linked and bound
   // to the GL context.
   // RETURNS: %TRUE if the shader is compiled, linked and ready for use.
   int is_compiled()() nothrow {
      return clutter_shader_is_compiled(&this);
   }

   // VERSION: 0.6
   // DEPRECATED (v1.8) method: release - Use #ClutterShaderEffect instead.
   // Frees up any GL context resources held by the shader.
   void release()() nothrow {
      clutter_shader_release(&this);
   }

   // VERSION: 0.6
   // DEPRECATED (v1.8) method: set_fragment_source - Use #ClutterShaderEffect instead.
   // Sets the GLSL source code to be used by a #ClutterShader for the fragment
   // program.
   // <data>: GLSL source code.
   // <length>: length of source buffer (currently ignored)
   void set_fragment_source(AT0)(AT0 /*char*/ data, ssize_t length) nothrow {
      clutter_shader_set_fragment_source(&this, toCString!(char*)(data), length);
   }

   // VERSION: 0.6
   // DEPRECATED (v1.8) method: set_is_enabled - Use #ClutterShaderEffect instead.
   // Enables a shader. This function will attempt to compile and link
   // the shader, if it isn't already.
   // 
   // When @enabled is %FALSE the default state of the GL pipeline will be
   // used instead.
   // <enabled>: The new state of the shader.
   void set_is_enabled()(int enabled) nothrow {
      clutter_shader_set_is_enabled(&this, enabled);
   }

   // VERSION: 1.0
   // DEPRECATED (v1.8) method: set_uniform - Use #ClutterShaderEffect instead.
   // Sets a user configurable variable in the GLSL shader programs attached to
   // a #ClutterShader.
   // <name>: name of uniform in GLSL shader program to set.
   // <value>: a #ClutterShaderFloat, #ClutterShaderInt or #ClutterShaderMatrix #GValue.
   void set_uniform(AT0, AT1)(AT0 /*char*/ name, AT1 /*GObject2.Value*/ value) nothrow {
      clutter_shader_set_uniform(&this, toCString!(char*)(name), UpCast!(GObject2.Value*)(value));
   }

   // VERSION: 0.6
   // DEPRECATED (v1.8) method: set_vertex_source - Use #ClutterShaderEffect instead.
   // Sets the GLSL source code to be used by a #ClutterShader for the vertex
   // program.
   // <data>: GLSL source code.
   // <length>: length of source buffer (currently ignored)
   void set_vertex_source(AT0)(AT0 /*char*/ data, ssize_t length) nothrow {
      clutter_shader_set_vertex_source(&this, toCString!(char*)(data), length);
   }
}

// The #ClutterShaderClass structure contains only private data
struct ShaderClass /* Version 0.6 */ {
   private GObject2.ObjectClass parent_class;
}


// The <structname>ClutterShaderEffect</structname> structure contains
// only private data and should be accessed using the provided API
struct ShaderEffect /* : OffscreenEffect */ /* Version 1.4 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance offscreeneffect;
   OffscreenEffect parent_instance;
   private ShaderEffectPrivate* priv;


   // VERSION: 1.8
   // Creates a new #ClutterShaderEffect, to be applied to an actor using
   // clutter_actor_add_effect().
   // 
   // The effect will be empty until clutter_shader_effect_set_shader_source()
   // is called.
   // 
   // Use g_object_unref() when done.
   // RETURNS: the newly created #ClutterShaderEffect.
   // <shader_type>: the type of the shader, either %CLUTTER_FRAGMENT_SHADER, or %CLUTTER_VERTEX_SHADER
   static ShaderEffect* /*new*/ new_()(ShaderType shader_type) nothrow {
      return clutter_shader_effect_new(shader_type);
   }
   static auto opCall()(ShaderType shader_type) {
      return clutter_shader_effect_new(shader_type);
   }

   // VERSION: 1.4
   // Retrieves a pointer to the program's handle
   // 
   // or %COGL_INVALID_HANDLE
   // RETURNS: a pointer to the program's handle,
   Cogl.Handle get_program()() nothrow {
      return clutter_shader_effect_get_program(&this);
   }

   // VERSION: 1.4
   // Retrieves a pointer to the shader's handle
   // 
   // or %COGL_INVALID_HANDLE
   // RETURNS: a pointer to the shader's handle,
   Cogl.Handle get_shader()() nothrow {
      return clutter_shader_effect_get_shader(&this);
   }

   // VERSION: 1.4
   // Sets the source of the GLSL shader used by @effect
   // 
   // This function should only be called by implementations of
   // the #ClutterShaderEffect class, and not by application code.
   // 
   // This function can only be called once; subsequent calls will
   // yield no result.
   // RETURNS: %TRUE if the source was set
   // <source>: the source of a GLSL shader
   int set_shader_source(AT0)(AT0 /*char*/ source) nothrow {
      return clutter_shader_effect_set_shader_source(&this, toCString!(char*)(source));
   }

   // Unintrospectable method: set_uniform() / clutter_shader_effect_set_uniform()
   // VERSION: 1.4
   // Sets a list of values as the payload for the uniform @name inside
   // the shader effect
   // 
   // The @gtype must be one of: %G_TYPE_INT, for 1 or more integer values;
   // %G_TYPE_FLOAT, for 1 or more floating point values;
   // %CLUTTER_TYPE_SHADER_INT, for a pointer to an array of integer values;
   // %CLUTTER_TYPE_SHADER_FLOAT, for a pointer to an array of floating point
   // values; and %CLUTTER_TYPE_SHADER_MATRIX, for a pointer to an array of
   // floating point values mapping a matrix
   // 
   // The number of values interepreted is defined by the @n_value
   // argument, and by the @gtype argument. For instance, a uniform named
   // "sampler0" and containing a single integer value is set using:
   // 
   // |[
   // clutter_shader_effect_set_uniform (effect, "sampler0",
   // G_TYPE_INT, 1,
   // 0);
   // ]|
   // 
   // While a uniform named "components" and containing a 3-elements vector
   // of floating point values (a "vec3") can be set using:
   // 
   // |[
   // gfloat component_r, component_g, component_b;
   // 
   // clutter_shader_effect_set_uniform (effect, "components",
   // G_TYPE_FLOAT, 3,
   // component_r,
   // component_g,
   // component_b);
   // ]|
   // 
   // or can be set using:
   // 
   // |[
   // gfloat component_vec[3];
   // 
   // clutter_shader_effect_set_uniform (effect, "components",
   // CLUTTER_TYPE_SHADER_FLOAT, 3,
   // component_vec);
   // ]|
   // 
   // Finally, a uniform named "map" and containing a matrix can be set using:
   // 
   // |[
   // clutter_shader_effect_set_uniform (effect, "map",
   // CLUTTER_TYPE_SHADER_MATRIX, 1,
   // cogl_matrix_get_array (&matrix));
   // ]|
   // <name>: the name of the uniform to set
   // <gtype>: the type of the uniform to set
   // <n_values>: the number of values
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_shader_effect_set_uniform set_uniform; // Variadic
   +/

   // VERSION: 1.4
   // Sets @value as the payload for the uniform @name inside the shader
   // effect
   // 
   // The #GType of the @value must be one of: %G_TYPE_INT, for a single
   // integer value; %G_TYPE_FLOAT, for a single floating point value;
   // %CLUTTER_TYPE_SHADER_INT, for an array of integer values;
   // %CLUTTER_TYPE_SHADER_FLOAT, for an array of floating point values;
   // and %CLUTTER_TYPE_SHADER_MATRIX, for a matrix of floating point
   // values. It also accepts %G_TYPE_DOUBLE for compatibility with other
   // languages than C.
   // <name>: the name of the uniform to set
   // <value>: a #GValue with the value of the uniform to set
   void set_uniform_value(AT0, AT1)(AT0 /*char*/ name, AT1 /*GObject2.Value*/ value) nothrow {
      clutter_shader_effect_set_uniform_value(&this, toCString!(char*)(name), UpCast!(GObject2.Value*)(value));
   }
}


// The <structname>ClutterShaderEffectClass</structname> structure contains
// only private data
struct ShaderEffectClass /* Version 1.4 */ {
   private OffscreenEffectClass parent_class;
   extern (C) char* /*new*/ function (ShaderEffect* effect) nothrow get_static_shader_source;
   extern (C) void function () nothrow _clutter_shader1;
   extern (C) void function () nothrow _clutter_shader2;
   extern (C) void function () nothrow _clutter_shader3;
   extern (C) void function () nothrow _clutter_shader4;
   extern (C) void function () nothrow _clutter_shader5;
}

struct ShaderEffectPrivate {
}

// #ClutterShader error enumeration
enum ShaderError /* Version 0.6 */ {
   NO_ASM = 0,
   NO_GLSL = 1,
   COMPILE = 2
}
struct ShaderFloat {
}

struct ShaderInt {
}

struct ShaderMatrix {
}

struct ShaderPrivate {
}

// The type of GLSL shader program
enum ShaderType /* Version 1.4 */ {
   VERTEX_SHADER = 0,
   FRAGMENT_SHADER = 1
}
enum int Shift_L = 65505;
enum int Shift_Lock = 65510;
enum int Shift_R = 65506;
enum int Shop = 269025078;
enum int SingleCandidate = 65340;
enum int Sinh_a = 16780677;
enum int Sinh_aa = 16780678;
enum int Sinh_aa2 = 16780751;
enum int Sinh_ae = 16780679;
enum int Sinh_ae2 = 16780752;
enum int Sinh_aee = 16780680;
enum int Sinh_aee2 = 16780753;
enum int Sinh_ai = 16780691;
enum int Sinh_ai2 = 16780763;
enum int Sinh_al = 16780746;
enum int Sinh_au = 16780694;
enum int Sinh_au2 = 16780766;
enum int Sinh_ba = 16780726;
enum int Sinh_bha = 16780727;
enum int Sinh_ca = 16780704;
enum int Sinh_cha = 16780705;
enum int Sinh_dda = 16780713;
enum int Sinh_ddha = 16780714;
enum int Sinh_dha = 16780719;
enum int Sinh_dhha = 16780720;
enum int Sinh_e = 16780689;
enum int Sinh_e2 = 16780761;
enum int Sinh_ee = 16780690;
enum int Sinh_ee2 = 16780762;
enum int Sinh_fa = 16780742;
enum int Sinh_ga = 16780700;
enum int Sinh_gha = 16780701;
enum int Sinh_h2 = 16780675;
enum int Sinh_ha = 16780740;
enum int Sinh_i = 16780681;
enum int Sinh_i2 = 16780754;
enum int Sinh_ii = 16780682;
enum int Sinh_ii2 = 16780755;
enum int Sinh_ja = 16780706;
enum int Sinh_jha = 16780707;
enum int Sinh_jnya = 16780709;
enum int Sinh_ka = 16780698;
enum int Sinh_kha = 16780699;
enum int Sinh_kunddaliya = 16780788;
enum int Sinh_la = 16780733;
enum int Sinh_lla = 16780741;
enum int Sinh_lu = 16780687;
enum int Sinh_lu2 = 16780767;
enum int Sinh_luu = 16780688;
enum int Sinh_luu2 = 16780787;
enum int Sinh_ma = 16780728;
enum int Sinh_mba = 16780729;
enum int Sinh_na = 16780721;
enum int Sinh_ndda = 16780716;
enum int Sinh_ndha = 16780723;
enum int Sinh_ng = 16780674;
enum int Sinh_ng2 = 16780702;
enum int Sinh_nga = 16780703;
enum int Sinh_nja = 16780710;
enum int Sinh_nna = 16780715;
enum int Sinh_nya = 16780708;
enum int Sinh_o = 16780692;
enum int Sinh_o2 = 16780764;
enum int Sinh_oo = 16780693;
enum int Sinh_oo2 = 16780765;
enum int Sinh_pa = 16780724;
enum int Sinh_pha = 16780725;
enum int Sinh_ra = 16780731;
enum int Sinh_ri = 16780685;
enum int Sinh_rii = 16780686;
enum int Sinh_ru2 = 16780760;
enum int Sinh_ruu2 = 16780786;
enum int Sinh_sa = 16780739;
enum int Sinh_sha = 16780737;
enum int Sinh_ssha = 16780738;
enum int Sinh_tha = 16780717;
enum int Sinh_thha = 16780718;
enum int Sinh_tta = 16780711;
enum int Sinh_ttha = 16780712;
enum int Sinh_u = 16780683;
enum int Sinh_u2 = 16780756;
enum int Sinh_uu = 16780684;
enum int Sinh_uu2 = 16780758;
enum int Sinh_va = 16780736;
enum int Sinh_ya = 16780730;
enum int Sleep = 269025071;
enum int SlowKeys_Enable = 65139;

// <structname>ClutterSnapConstraint</structname> is an opaque structure
// whose members cannot be directly accesses
struct SnapConstraint /* : Constraint */ /* Version 1.6 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent constraint;
   Constraint method_parent;


   // VERSION: 1.6
   // Creates a new #ClutterSnapConstraint that will snap a #ClutterActor
   // to the @edge of @source, with the given @offset.
   // RETURNS: the newly created #ClutterSnapConstraint
   // <source>: the #ClutterActor to use as the source of the constraint, or %NULL
   // <from_edge>: the edge of the actor to use in the constraint
   // <to_edge>: the edge of @source to use in the constraint
   // <offset>: the offset to apply to the constraint, in pixels
   static SnapConstraint* new_(AT0)(AT0 /*Actor*/ source, SnapEdge from_edge, SnapEdge to_edge, float offset) nothrow {
      return clutter_snap_constraint_new(UpCast!(Actor*)(source), from_edge, to_edge, offset);
   }
   static auto opCall(AT0)(AT0 /*Actor*/ source, SnapEdge from_edge, SnapEdge to_edge, float offset) {
      return clutter_snap_constraint_new(UpCast!(Actor*)(source), from_edge, to_edge, offset);
   }

   // VERSION: 1.6
   // Retrieves the edges used by the @constraint
   // <from_edge>: return location for the actor's edge, or %NULL
   // <to_edge>: return location for the source's edge, or %NULL
   
   // method "get_edges" removed: Link failures - missing in library.

   // VERSION: 1.6
   // Retrieves the offset set using clutter_snap_constraint_set_offset()
   // RETURNS: the offset, in pixels
   float get_offset()() nothrow {
      return clutter_snap_constraint_get_offset(&this);
   }

   // VERSION: 1.6
   // Retrieves the #ClutterActor set using clutter_snap_constraint_set_source()
   // RETURNS: a pointer to the source actor
   Actor* get_source()() nothrow {
      return clutter_snap_constraint_get_source(&this);
   }

   // VERSION: 1.6
   // Sets the edges to be used by the @constraint
   // 
   // The @from_edge is the edge on the #ClutterActor to which @constraint
   // has been added. The @to_edge is the edge of the #ClutterActor inside
   // the #ClutterSnapConstraint:source property.
   // <from_edge>: the edge on the actor
   // <to_edge>: the edge on the source
   void set_edges()(SnapEdge from_edge, SnapEdge to_edge) nothrow {
      clutter_snap_constraint_set_edges(&this, from_edge, to_edge);
   }

   // VERSION: 1.6
   // Sets the offset to be applied to the constraint
   // <offset>: the offset to apply, in pixels
   void set_offset()(float offset) nothrow {
      clutter_snap_constraint_set_offset(&this, offset);
   }

   // VERSION: 1.6
   // Sets the source #ClutterActor for the constraint
   // <source>: a #ClutterActor, or %NULL to unset the source
   void set_source(AT0)(AT0 /*Actor*/ source=null) nothrow {
      clutter_snap_constraint_set_source(&this, UpCast!(Actor*)(source));
   }
}

struct SnapConstraintClass {
}

// The edge to snap
enum SnapEdge /* Version 1.6 */ {
   TOP = 0,
   RIGHT = 1,
   BOTTOM = 2,
   LEFT = 3
}
enum int Spell = 269025148;
enum int SplitScreen = 269025149;

// The #ClutterStage structure contains only private data
// and should be accessed using the provided API
struct Stage /* : Group */ /* Version 0.1 */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Animatable.__interface__;
   mixin Container.__interface__;
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance group;
   Group parent_instance;
   private StagePrivate* priv;


   // VERSION: 0.8
   // Creates a new, non-default stage. A non-default stage is a new
   // top-level actor which can be used as another container. It works
   // exactly like the default stage, but while clutter_stage_get_default()
   // will always return the same instance, you will have to keep a pointer
   // to any #ClutterStage returned by clutter_stage_new().
   // 
   // The ability to support multiple stages depends on the current
   // backend. Use clutter_feature_available() and
   // %CLUTTER_FEATURE_STAGE_MULTIPLE to check at runtime whether a
   // backend supports multiple stages.
   // 
   // not support multiple stages. Use clutter_actor_destroy() to
   // programmatically close the returned stage.
   // RETURNS: a new stage, or %NULL if the default backend does
   static Stage* new_()() nothrow {
      return clutter_stage_new();
   }
   static auto opCall()() {
      return clutter_stage_new();
   }

   // DEPRECATED (v1.10) function: get_default - Use clutter_stage_new() instead.
   // Retrieves a #ClutterStage singleton.
   // 
   // This function is not as useful as it sounds, and will most likely
   // by deprecated in the future. Application code should only create
   // a #ClutterStage instance using clutter_stage_new(), and manage the
   // lifetime of the stage manually.
   // 
   // The default stage singleton has a platform-specific behaviour: on
   // platforms without the %CLUTTER_FEATURE_STAGE_MULTIPLE feature flag
   // set, the first #ClutterStage instance will also be set to be the
   // default stage instance, and this function will always return a
   // pointer to it.
   // 
   // On platforms with the %CLUTTER_FEATURE_STAGE_MULTIPLE feature flag
   // set, the default stage will be created by the first call to this
   // function, and every following call will return the same pointer to
   // it.
   // 
   // #ClutterStage. You should never destroy or unref the returned
   // actor.
   // RETURNS: the main
   static Clutter.Stage* get_default()() nothrow {
      return clutter_stage_get_default();
   }

   // VERSION: 0.8
   // This function essentially makes sure the right GL context is
   // current for the passed stage. It is not intended to
   // be used by applications.
   void ensure_current()() nothrow {
      clutter_stage_ensure_current(&this);
   }

   // VERSION: 1.0
   // Ensures that @stage is redrawn
   // 
   // This function should not be called by applications: it is
   // used when embedding a #ClutterStage into a toolkit with
   // another windowing system, like GTK+.
   void ensure_redraw()() nothrow {
      clutter_stage_ensure_redraw(&this);
   }

   // VERSION: 1.0
   // Ensures that the GL viewport is updated with the current
   // stage window size.
   // 
   // This function will queue a redraw of @stage.
   // 
   // This function should not be called by applications; it is used
   // when embedding a #ClutterStage into a toolkit with another
   // windowing system, like GTK+.
   void ensure_viewport()() nothrow {
      clutter_stage_ensure_viewport(&this);
   }

   // VERSION: 0.4
   // This function is used to emit an event on the main stage.
   // 
   // You should rarely need to use this function, except for
   // synthetised events.
   // RETURNS: the return value from the signal emission
   // <event>: a #ClutterEvent
   int event(AT0)(AT0 /*Event*/ event) nothrow {
      return clutter_stage_event(&this, UpCast!(Event*)(event));
   }

   // VERSION: 1.6
   // Retrieves the value set with clutter_stage_set_accept_focus().
   // 
   // otherwise
   // RETURNS: %TRUE if the #ClutterStage should accept focus, and %FALSE
   int get_accept_focus()() nothrow {
      return clutter_stage_get_accept_focus(&this);
   }

   // Checks the scene at the coordinates @x and @y and returns a pointer
   // to the #ClutterActor at those coordinates.
   // 
   // By using @pick_mode it is possible to control which actors will be
   // painted and thus available.
   // 
   // if any
   // RETURNS: the actor at the specified coordinates,
   // <pick_mode>: how the scene graph should be painted
   // <x>: X coordinate to check
   // <y>: Y coordinate to check
   Actor* get_actor_at_pos()(PickMode pick_mode, int x, int y) nothrow {
      return clutter_stage_get_actor_at_pos(&this, pick_mode, x, y);
   }

   // Retrieves the stage color.
   // <color>: return location for a #ClutterColor
   void get_color(AT0)(/*out*/ AT0 /*Color*/ color) nothrow {
      clutter_stage_get_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 0.6
   // DEPRECATED (v1.10) method: get_fog - This function will always return the default
   // Retrieves the current depth cueing settings from the stage.
   // 
   // 
   // values of #ClutterFog
   // <fog>: return location for a #ClutterFog structure
   void get_fog(AT0)(/*out*/ AT0 /*Fog*/ fog) nothrow {
      clutter_stage_get_fog(&this, UpCast!(Fog*)(fog));
   }

   // VERSION: 1.0
   // Retrieves whether the stage is full screen or not
   // RETURNS: %TRUE if the stage is full screen
   int get_fullscreen()() nothrow {
      return clutter_stage_get_fullscreen(&this);
   }

   // VERSION: 0.6
   // Retrieves the actor that is currently under key focus.
   // RETURNS: the actor with key focus, or the stage
   Actor* get_key_focus()() nothrow {
      return clutter_stage_get_key_focus(&this);
   }

   // VERSION: 1.2
   // Retrieves the minimum size for a stage window as set using
   // clutter_stage_set_minimum_size().
   // 
   // The returned size may not correspond to the actual minimum size and
   // it is specific to the #ClutterStage implementation inside the
   // Clutter backend
   // <width>: return location for the minimum width, in pixels, or %NULL
   // <height>: return location for the minimum height, in pixels, or %NULL
   void get_minimum_size(AT0, AT1)(/*out*/ AT0 /*uint*/ width, /*out*/ AT1 /*uint*/ height) nothrow {
      clutter_stage_get_minimum_size(&this, UpCast!(uint*)(width), UpCast!(uint*)(height));
   }

   // VERSION: 1.8
   // Retrieves the value set using clutter_stage_set_motion_events_enabled().
   // 
   // and %FALSE otherwise
   // RETURNS: %TRUE if the per-actor motion event delivery is enabled
   int get_motion_events_enabled()() nothrow {
      return clutter_stage_get_motion_events_enabled(&this);
   }

   // VERSION: 1.4
   // Retrieves the hint set with clutter_stage_set_no_clear_hint()
   // 
   // cycle, and %FALSE otherwise
   // RETURNS: %TRUE if the stage should not clear itself on every paint
   int get_no_clear_hint()() nothrow {
      return clutter_stage_get_no_clear_hint(&this);
   }

   // Retrieves the stage perspective.
   // <perspective>: return location for a #ClutterPerspective
   void get_perspective(AT0)(/*out*/ AT0 /*Perspective*/ perspective=null) nothrow {
      clutter_stage_get_perspective(&this, UpCast!(Perspective*)(perspective));
   }

   // VERSION: 1.8
   // Gets the bounds of the current redraw for @stage in stage pixel
   // coordinates. E.g., if only a single actor has queued a redraw then
   // Clutter may redraw the stage with a clip so that it doesn't have to
   // paint every pixel in the stage. This function would then return the
   // bounds of that clip. An application can use this information to
   // avoid some extra work if it knows that some regions of the stage
   // aren't going to be painted. This should only be called while the
   // stage is being painted. If there is no current redraw clip then
   // this function will set @clip to the full extents of the stage.
   // <clip>: Return location for the clip bounds
   void get_redraw_clip_bounds(AT0)(/*out*/ AT0 /*cairo.RectangleInt*/ clip) nothrow {
      clutter_stage_get_redraw_clip_bounds(&this, UpCast!(cairo.RectangleInt*)(clip));
   }

   // VERSION: 1.0
   // Retrieves the value set with clutter_stage_set_throttle_motion_events()
   // 
   // and %FALSE otherwise
   // RETURNS: %TRUE if the motion events are being throttled,
   int get_throttle_motion_events()() nothrow {
      return clutter_stage_get_throttle_motion_events(&this);
   }

   // VERSION: 0.4
   // Gets the stage title.
   // 
   // returned string is owned by the actor and should not
   // be modified or freed.
   // RETURNS: pointer to the title string for the stage. The
   char* get_title()() nothrow {
      return clutter_stage_get_title(&this);
   }

   // VERSION: 1.2
   // Retrieves the value set using clutter_stage_set_use_alpha()
   // 
   // alpha channel of the stage color
   // RETURNS: %TRUE if the stage should honour the opacity and the
   int get_use_alpha()() nothrow {
      return clutter_stage_get_use_alpha(&this);
   }

   // VERSION: 0.6
   // DEPRECATED (v1.10) method: get_use_fog - This function will always return %FALSE
   // Gets whether the depth cueing effect is enabled on @stage.
   // RETURNS: %TRUE if the depth cueing effect is enabled
   int get_use_fog()() nothrow {
      return clutter_stage_get_use_fog(&this);
   }

   // VERSION: 0.4
   // Retrieves the value set with clutter_stage_set_user_resizable().
   // RETURNS: %TRUE if the stage is resizable by the user.
   int get_user_resizable()() nothrow {
      return clutter_stage_get_user_resizable(&this);
   }

   // VERSION: 0.4
   // Makes the cursor invisible on the stage window
   void hide_cursor()() nothrow {
      clutter_stage_hide_cursor(&this);
   }

   // VERSION: 0.8
   // DEPRECATED (v1.10) method: is_default - Track the stage pointer inside your application
   // Checks if @stage is the default stage, or an instance created using
   // clutter_stage_new() but internally using the same implementation.
   // 
   // 
   // 
   // code, or use clutter_actor_get_stage() to retrieve the stage for
   // a given actor.
   // RETURNS: %TRUE if the passed stage is the default one
   int is_default()() nothrow {
      return clutter_stage_is_default(&this);
   }

   // VERSION: 0.8
   // DEPRECATED (v1.10) method: queue_redraw - Use clutter_actor_queue_redraw() instead.
   // Queues a redraw for the passed stage.
   // 
   // <note>Applications should call clutter_actor_queue_redraw() and not
   // this function.</note>
   void queue_redraw()() nothrow {
      clutter_stage_queue_redraw(&this);
   }

   // Makes a screenshot of the stage in RGBA 8bit data, returns a
   // linear buffer with @width * 4 as rowstride.
   // 
   // The alpha data contained in the returned buffer is driver-dependent,
   // and not guaranteed to hold any sensible value.
   // 
   // or %NULL if the read failed. Use g_free() on the returned data
   // to release the resources it has allocated.
   // RETURNS: a pointer to newly allocated memory with the buffer
   // <x>: x coordinate of the first pixel that is read from stage
   // <y>: y coordinate of the first pixel that is read from stage
   // <width>: Width dimention of pixels to be read, or -1 for the entire stage width
   // <height>: Height dimention of pixels to be read, or -1 for the entire stage height
   ubyte* read_pixels()(int x, int y, int width, int height) nothrow {
      return clutter_stage_read_pixels(&this, x, y, width, height);
   }

   // VERSION: 1.6
   // Sets whether the @stage should accept the key focus when shown.
   // 
   // This function should be called before showing @stage using
   // clutter_actor_show().
   // <accept_focus>: %TRUE to accept focus on show
   void set_accept_focus()(int accept_focus) nothrow {
      clutter_stage_set_accept_focus(&this, accept_focus);
   }

   // Sets the stage color.
   // <color>: A #ClutterColor
   void set_color(AT0)(AT0 /*Color*/ color) nothrow {
      clutter_stage_set_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 0.6
   // DEPRECATED (v1.10) method: set_fog - Fog settings are ignored.
   // Sets the fog (also known as "depth cueing") settings for the @stage.
   // 
   // A #ClutterStage will only use a linear fog progression, which
   // depends solely on the distance from the viewer. The cogl_set_fog()
   // function in COGL exposes more of the underlying implementation,
   // and allows changing the for progression function. It can be directly
   // used by disabling the #ClutterStage:use-fog property and connecting
   // a signal handler to the #ClutterActor::paint signal on the @stage,
   // like:
   // 
   // |[
   // clutter_stage_set_use_fog (stage, FALSE);
   // g_signal_connect (stage, "paint", G_CALLBACK (on_stage_paint), NULL);
   // ]|
   // 
   // The paint signal handler will call cogl_set_fog() with the
   // desired settings:
   // 
   // |[
   // static void
   // on_stage_paint (ClutterActor *actor)
   // {
   // ClutterColor stage_color = { 0, };
   // CoglColor fog_color = { 0, };
   // 
   // /&ast; set the fog color to the stage background color &ast;/
   // clutter_stage_get_color (CLUTTER_STAGE (actor), &amp;stage_color);
   // cogl_color_init_from_4ub (&amp;fog_color,
   // stage_color.red,
   // stage_color.green,
   // stage_color.blue,
   // stage_color.alpha);
   // 
   // /&ast; enable fog &ast;/
   // cogl_set_fog (&amp;fog_color,
   // COGL_FOG_MODE_EXPONENTIAL, /&ast; mode &ast;/
   // 0.5,                       /&ast; density &ast;/
   // 5.0, 30.0);                /&ast; z_near and z_far &ast;/
   // }
   // ]|
   // 
   // <note>The fogging functions only work correctly when the visible actors use
   // unmultiplied alpha colors. By default Cogl will premultiply textures and
   // cogl_set_source_color() will premultiply colors, so unless you explicitly
   // load your textures requesting an unmultiplied internal format and use
   // cogl_material_set_color() you can only use fogging with fully opaque actors.
   // Support for premultiplied colors will improve in the future when we can
   // depend on fragment shaders.</note>
   // <fog>: a #ClutterFog structure
   void set_fog(AT0)(AT0 /*Fog*/ fog) nothrow {
      clutter_stage_set_fog(&this, UpCast!(Fog*)(fog));
   }

   // VERSION: 1.0
   // Asks to place the stage window in the fullscreen or unfullscreen
   // states.
   // 
   // afterward, because other entities (e.g. the user or window manager)
   // could unfullscreen it again, and not all window managers honor
   // requests to fullscreen windows.
   // 
   // If you want to receive notification of the fullscreen state you
   // should either use the #ClutterStage::fullscreen and
   // #ClutterStage::unfullscreen signals, or use the notify signal
   // for the #ClutterStage:fullscreen-set property
   // <fullscreen>: %TRUE to to set the stage fullscreen
   void set_fullscreen()(int fullscreen) nothrow {
      clutter_stage_set_fullscreen(&this, fullscreen);
   }

   // VERSION: 0.6
   // Sets the key focus on @actor. An actor with key focus will receive
   // all the key events. If @actor is %NULL, the stage will receive
   // focus.
   // <actor>: the actor to set key focus to, or %NULL
   void set_key_focus(AT0)(AT0 /*Actor*/ actor=null) nothrow {
      clutter_stage_set_key_focus(&this, UpCast!(Actor*)(actor));
   }

   // VERSION: 1.2
   // Sets the minimum size for a stage window, if the default backend
   // uses #ClutterStage inside a window
   // 
   // This is a convenience function, and it is equivalent to setting the
   // #ClutterActor:min-width and #ClutterActor:min-height on @stage
   // 
   // If the current size of @stage is smaller than the minimum size, the
   // @stage will be resized to the new @width and @height
   // 
   // This function has no effect if @stage is fullscreen
   // <width>: width, in pixels
   // <height>: height, in pixels
   void set_minimum_size()(uint width, uint height) nothrow {
      clutter_stage_set_minimum_size(&this, width, height);
   }

   // VERSION: 1.8
   // Sets whether per-actor motion events (and relative crossing
   // events) should be disabled or not.
   // 
   // The default is %TRUE.
   // 
   // If @enable is %FALSE the following events will not be delivered
   // to the actors children of @stage.
   // 
   // <itemizedlist>
   // <listitem><para>#ClutterActor::motion-event</para></listitem>
   // <listitem><para>#ClutterActor::enter-event</para></listitem>
   // <listitem><para>#ClutterActor::leave-event</para></listitem>
   // </itemizedlist>
   // 
   // The events will still be delivered to the #ClutterStage.
   // 
   // The main side effect of this function is that disabling the motion
   // events will disable picking to detect the #ClutterActor underneath
   // the pointer for each motion event. This is useful, for instance,
   // when dragging a #ClutterActor across the @stage: the actor underneath
   // the pointer is not going to change, so it's meaningless to perform
   // a pick.
   // <enabled>: %TRUE to enable the motion events delivery, and %FALSE otherwise
   void set_motion_events_enabled()(int enabled) nothrow {
      clutter_stage_set_motion_events_enabled(&this, enabled);
   }

   // VERSION: 1.4
   // Sets whether the @stage should clear itself at the beginning
   // of each paint cycle or not.
   // 
   // Clearing the #ClutterStage can be a costly operation, especially
   // if the stage is always covered - for instance, in a full-screen
   // video player or in a game with a background texture.
   // 
   // <note><para>This setting is a hint; Clutter might discard this
   // hint depending on its internal state.</para></note>
   // 
   // <warning><para>If parts of the stage are visible and you disable
   // clearing you might end up with visual artifacts while painting the
   // contents of the stage.</para></warning>
   // <no_clear>: %TRUE if the @stage should not clear itself on every repaint cycle
   void set_no_clear_hint()(int no_clear) nothrow {
      clutter_stage_set_no_clear_hint(&this, no_clear);
   }

   // Sets the stage perspective. Using this function is not recommended
   // because it will disable Clutter's attempts to generate an
   // appropriate perspective based on the size of the stage.
   // <perspective>: A #ClutterPerspective
   void set_perspective(AT0)(AT0 /*Perspective*/ perspective) nothrow {
      clutter_stage_set_perspective(&this, UpCast!(Perspective*)(perspective));
   }

   // VERSION: 1.0
   // Sets whether motion events received between redraws should
   // be throttled or not. If motion events are throttled, those
   // events received by the windowing system between redraws will
   // be compressed so that only the last event will be propagated
   // to the @stage and its actors.
   // 
   // This function should only be used if you want to have all
   // the motion events delivered to your application code.
   // <throttle>: %TRUE to throttle motion events
   void set_throttle_motion_events()(int throttle) nothrow {
      clutter_stage_set_throttle_motion_events(&this, throttle);
   }

   // VERSION: 0.4
   // Sets the stage title.
   // <title>: A utf8 string for the stage windows title.
   void set_title(AT0)(AT0 /*char*/ title) nothrow {
      clutter_stage_set_title(&this, toCString!(char*)(title));
   }

   // VERSION: 1.2
   // Sets whether the @stage should honour the #ClutterActor:opacity and
   // the alpha channel of the #ClutterStage:color
   // <use_alpha>: whether the stage should honour the opacity or the alpha channel of the stage color
   void set_use_alpha()(int use_alpha) nothrow {
      clutter_stage_set_use_alpha(&this, use_alpha);
   }

   // VERSION: 0.6
   // DEPRECATED (v1.10) method: set_use_fog - Calling this function produces no visible effect
   // Sets whether the depth cueing effect on the stage should be enabled
   // or not.
   // 
   // Depth cueing is a 3D effect that makes actors farther away from the
   // viewing point less opaque, by fading them with the stage color.
   // The parameters of the GL fog used can be changed using the
   // clutter_stage_set_fog() function.
   // <fog>: %TRUE for enabling the depth cueing effect
   void set_use_fog()(int fog) nothrow {
      clutter_stage_set_use_fog(&this, fog);
   }

   // VERSION: 0.4
   // Sets if the stage is resizable by user interaction (e.g. via
   // window manager controls)
   // <resizable>: whether the stage should be user resizable.
   void set_user_resizable()(int resizable) nothrow {
      clutter_stage_set_user_resizable(&this, resizable);
   }
   // Shows the cursor on the stage window
   void show_cursor()() nothrow {
      clutter_stage_show_cursor(&this);
   }

   // VERSION: 0.6
   // The ::activate signal is emitted when the stage receives key focus
   // from the underlying window system.
   extern (C) alias static void function (Stage* this_, void* user_data=null) nothrow signal_activate;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"activate", CB/*:signal_activate*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_activate)||_ttmm!(CB, signal_activate)()) {
      return signal_connect_data!()(&this, cast(char*)"activate",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::activate signal is emitted when the stage loses key focus
   // from the underlying window system.
   extern (C) alias static void function (Stage* this_, void* user_data=null) nothrow signal_deactivate;
   ulong signal_connect(string name:"deactivate", CB/*:signal_deactivate*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_deactivate)||_ttmm!(CB, signal_deactivate)()) {
      return signal_connect_data!()(&this, cast(char*)"deactivate",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.2
   // The ::delete-event signal is emitted when the user closes a
   // #ClutterStage window using the window controls.
   // 
   // Clutter by default will call clutter_main_quit() if @stage is
   // the default stage, and clutter_actor_destroy() for any other
   // stage.
   // 
   // It is possible to override the default behaviour by connecting
   // a new handler and returning %TRUE there.
   // 
   // <note>This signal is emitted only on Clutter backends that
   // embed #ClutterStage in native windows. It is not emitted for
   // backends that use a static frame buffer.</note>
   // <event>: a #ClutterEvent of type %CLUTTER_DELETE
   extern (C) alias static c_int function (Stage* this_, Event* event, void* user_data=null) nothrow signal_delete_event;
   ulong signal_connect(string name:"delete-event", CB/*:signal_delete_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_delete_event)||_ttmm!(CB, signal_delete_event)()) {
      return signal_connect_data!()(&this, cast(char*)"delete-event",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::fullscreen signal is emitted when the stage is made fullscreen.
   extern (C) alias static void function (Stage* this_, void* user_data=null) nothrow signal_fullscreen;
   ulong signal_connect(string name:"fullscreen", CB/*:signal_fullscreen*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_fullscreen)||_ttmm!(CB, signal_fullscreen)()) {
      return signal_connect_data!()(&this, cast(char*)"fullscreen",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.6
   // The ::unfullscreen signal is emitted when the stage leaves a fullscreen
   // state.
   extern (C) alias static void function (Stage* this_, void* user_data=null) nothrow signal_unfullscreen;
   ulong signal_connect(string name:"unfullscreen", CB/*:signal_unfullscreen*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_unfullscreen)||_ttmm!(CB, signal_unfullscreen)()) {
      return signal_connect_data!()(&this, cast(char*)"unfullscreen",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// The #ClutterStageClass structure contains only private data
struct StageClass /* Version 0.1 */ {
   private GroupClass parent_class;
   extern (C) void function (Stage* stage) nothrow fullscreen;
   extern (C) void function (Stage* stage) nothrow unfullscreen;
   extern (C) void function (Stage* stage) nothrow activate;
   extern (C) void function (Stage* stage) nothrow deactivate;
   extern (C) int function (Stage* stage, Event* event) nothrow delete_event;
   private void*[31] _padding_dummy;
}

// The #ClutterStageManager structure is private.
struct StageManager /* : GObject.Object */ /* Version 1.0 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // VERSION: 0.8
   // Returns the default #ClutterStageManager.
   // 
   // object is owned by Clutter and you should not reference or unreference it.
   // RETURNS: the default stage manager instance. The returned
   static StageManager* get_default()() nothrow {
      return clutter_stage_manager_get_default();
   }

   // VERSION: 0.8
   // Returns the default #ClutterStage.
   // 
   // is owned by Clutter and you should never reference or unreference it
   // RETURNS: the default stage. The returned object
   Stage* get_default_stage()() nothrow {
      return clutter_stage_manager_get_default_stage(&this);
   }

   // VERSION: 0.8
   // Lists all currently used stages.
   // 
   // allocated list of #ClutterStage objects. Use g_slist_free() to
   // deallocate it when done.
   // RETURNS: a newly
   GLib2.SList* /*new container*/ list_stages()() nothrow {
      return clutter_stage_manager_list_stages(&this);
   }

   // VERSION: 1.0
   // Lists all currently used stages.
   // 
   // to the internal list of #ClutterStage objects. The returned list
   // is owned by the #ClutterStageManager and should never be modified
   // or freed
   // RETURNS: a pointer
   GLib2.SList* peek_stages()() nothrow {
      return clutter_stage_manager_peek_stages(&this);
   }

   // VERSION: 0.8
   // DEPRECATED (v1.2) method: set_default_stage - Calling this function has no effect
   // Sets @stage as the default stage.
   // <stage>: a #ClutterStage
   void set_default_stage(AT0)(AT0 /*Stage*/ stage) nothrow {
      clutter_stage_manager_set_default_stage(&this, UpCast!(Stage*)(stage));
   }

   // VERSION: 0.8
   // The ::stage-added signal is emitted each time a new #ClutterStage
   // has been added to the stage manager.
   // <stage>: the added stage
   extern (C) alias static void function (StageManager* this_, Stage* stage, void* user_data=null) nothrow signal_stage_added;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"stage-added", CB/*:signal_stage_added*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_stage_added)||_ttmm!(CB, signal_stage_added)()) {
      return signal_connect_data!()(&this, cast(char*)"stage-added",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.8
   // The ::stage-removed signal is emitted each time a #ClutterStage
   // has been removed from the stage manager.
   // <stage>: the removed stage
   extern (C) alias static void function (StageManager* this_, Stage* stage, void* user_data=null) nothrow signal_stage_removed;
   ulong signal_connect(string name:"stage-removed", CB/*:signal_stage_removed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_stage_removed)||_ttmm!(CB, signal_stage_removed)()) {
      return signal_connect_data!()(&this, cast(char*)"stage-removed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}


// The #ClutterStageManagerClass structure contains only private data
// and should be accessed using the provided API
struct StageManagerClass /* Version 1.0 */ {
   private GObject2.ObjectClass parent_class;
   extern (C) void function (StageManager* stage_manager, Stage* stage) nothrow stage_added;
   extern (C) void function (StageManager* stage_manager, Stage* stage) nothrow stage_removed;
}

struct StagePrivate {
}

// Stage state masks, used by the #ClutterEvent of type %CLUTTER_STAGE_STATE.
enum StageState /* Version 0.4 */ {
   FULLSCREEN = 2,
   OFFSCREEN = 4,
   ACTIVATED = 8
}
// Event signalling a change in the #ClutterStage state.
struct StageStateEvent /* Version 0.2 */ {
   EventType type;
   uint time;
   EventFlags flags;
   Stage* stage;
   Actor* source;
   StageState changed_mask, new_state;
}


// <structname>ClutterStageWindow</structname> is an opaque structure
// whose members should not be accessed directly
struct StageWindow /* Interface */ /* Version 0.8 */ {
   mixin template __interface__() {   }
   mixin __interface__;
}

// The interface implemented by backends for stage windows
struct StageWindowIface /* Version 0.8 */ {
   private GObject2.TypeInterface parent_iface;
   // Unintrospectable functionp: get_wrapper() / ()
   extern (C) Actor* function (StageWindow* stage_window) nothrow get_wrapper;
   // Unintrospectable functionp: set_title() / ()
   extern (C) void function (StageWindow* stage_window, char* title) nothrow set_title;
   // Unintrospectable functionp: set_fullscreen() / ()
   extern (C) void function (StageWindow* stage_window, int is_fullscreen) nothrow set_fullscreen;
   // Unintrospectable functionp: set_cursor_visible() / ()
   extern (C) void function (StageWindow* stage_window, int cursor_visible) nothrow set_cursor_visible;
   // Unintrospectable functionp: set_user_resizable() / ()
   extern (C) void function (StageWindow* stage_window, int is_resizable) nothrow set_user_resizable;
   // Unintrospectable functionp: realize() / ()
   extern (C) int function (StageWindow* stage_window) nothrow realize;
   // Unintrospectable functionp: unrealize() / ()
   extern (C) void function (StageWindow* stage_window) nothrow unrealize;
   // Unintrospectable functionp: show() / ()
   extern (C) void function (StageWindow* stage_window, int do_raise) nothrow show;
   // Unintrospectable functionp: hide() / ()
   extern (C) void function (StageWindow* stage_window) nothrow hide;
   // Unintrospectable functionp: resize() / ()
   extern (C) void function (StageWindow* stage_window, int width, int height) nothrow resize;
   // Unintrospectable functionp: get_geometry() / ()
   extern (C) void function (StageWindow* stage_window, cairo.RectangleInt* geometry) nothrow get_geometry;
   // Unintrospectable functionp: get_pending_swaps() / ()
   extern (C) int function (StageWindow* stage_window) nothrow get_pending_swaps;
   // Unintrospectable functionp: add_redraw_clip() / ()
   extern (C) void function (StageWindow* stage_window, cairo.RectangleInt* stage_rectangle) nothrow add_redraw_clip;
   // Unintrospectable functionp: has_redraw_clips() / ()
   extern (C) int function (StageWindow* stage_window) nothrow has_redraw_clips;
   // Unintrospectable functionp: ignoring_redraw_clips() / ()
   extern (C) int function (StageWindow* stage_window) nothrow ignoring_redraw_clips;
   // Unintrospectable functionp: get_redraw_clip_bounds() / ()
   extern (C) int function (StageWindow* stage_window, cairo.RectangleInt* clip) nothrow get_redraw_clip_bounds;
   // Unintrospectable functionp: set_accept_focus() / ()
   extern (C) void function (StageWindow* stage_window, int accept_focus) nothrow set_accept_focus;
   // Unintrospectable functionp: redraw() / ()
   extern (C) void function (StageWindow* stage_window) nothrow redraw;
   // Unintrospectable functionp: get_active_framebuffer() / ()
   extern (C) Cogl.Framebuffer* function (StageWindow* stage_window) nothrow get_active_framebuffer;
   // Unintrospectable functionp: can_clip_redraws() / ()
   extern (C) int function (StageWindow* stage_window) nothrow can_clip_redraws;
}

enum int Standby = 269025040;
enum int Start = 269025050;

// The <structname>ClutterState</structname> structure contains only
// private data and should be accessed using the provided API
struct State /* : GObject.Object */ /* Version 1.4 */ {
   mixin Scriptable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   private StatePrivate* priv;


   // Creates a new #ClutterState
   // RETURNS: the newly create #ClutterState instance
   static State* /*new*/ new_()() nothrow {
      return clutter_state_new();
   }
   static auto opCall()() {
      return clutter_state_new();
   }

   // VERSION: 1.4
   // Retrieves the #ClutterAnimator that is being used for transitioning
   // between the two states, if any has been set
   // RETURNS: a #ClutterAnimator instance, or %NULL
   // <source_state_name>: the name of a source state
   // <target_state_name>: the name of a target state
   Animator* get_animator(AT0, AT1)(AT0 /*char*/ source_state_name, AT1 /*char*/ target_state_name) nothrow {
      return clutter_state_get_animator(&this, toCString!(char*)(source_state_name), toCString!(char*)(target_state_name));
   }

   // VERSION: 1.4
   // Queries the duration used for transitions between a source and
   // target state pair
   // 
   // The semantics for the query are the same as the semantics used for
   // setting the duration with clutter_state_set_duration()
   // RETURNS: the duration, in milliseconds
   // <source_state_name>: the name of the source state to get the duration of, or %NULL
   // <target_state_name>: the name of the source state to get the duration of, or %NULL
   uint get_duration(AT0, AT1)(AT0 /*char*/ source_state_name=null, AT1 /*char*/ target_state_name=null) nothrow {
      return clutter_state_get_duration(&this, toCString!(char*)(source_state_name), toCString!(char*)(target_state_name));
   }

   // VERSION: 1.4
   // Returns a list of pointers to opaque structures with accessor functions
   // that describe the keys added to an animator.
   // 
   // newly allocated #GList of #ClutterStateKey<!-- -->s. The contents of
   // the returned list are owned by the #ClutterState and should not be
   // modified or freed. Use g_list_free() to free the resources allocated
   // by the returned list when done using it
   // RETURNS: a
   // <source_state_name>: the source transition name to query, or %NULL for all source states
   // <target_state_name>: the target transition name to query, or %NULL for all target states
   // <object>: the specific object instance to list keys for, or %NULL for all managed objects
   // <property_name>: the property name to search for, or %NULL for all properties.
   GLib2.List* /*new container*/ get_keys(AT0, AT1, AT2, AT3)(AT0 /*char*/ source_state_name=null, AT1 /*char*/ target_state_name=null, AT2 /*GObject2.Object*/ object=null, AT3 /*char*/ property_name=null) nothrow {
      return clutter_state_get_keys(&this, toCString!(char*)(source_state_name), toCString!(char*)(target_state_name), UpCast!(GObject2.Object*)(object), toCString!(char*)(property_name));
   }

   // VERSION: 1.4
   // Queries the currently set target state.
   // 
   // During a transition this function will return the target of the transition.
   // 
   // This function is useful when called from handlers of the
   // #ClutterState::completed signal.
   // 
   // is owned by the #ClutterState and should not be modified or freed
   // RETURNS: a string containing the target state. The returned string
   char* get_state()() nothrow {
      return clutter_state_get_state(&this);
   }

   // VERSION: 1.4
   // Gets a list of all the state names managed by this #ClutterState.
   // 
   // #GList of state names. The contents of the returned #GList are owned
   // by the #ClutterState and should not be modified or freed. Use
   // g_list_free() to free the resources allocated by the returned list when
   // done using it
   // RETURNS: a newly allocated
   GLib2.List* /*new container*/ get_states()() nothrow {
      return clutter_state_get_states(&this);
   }

   // VERSION: 1.4
   // Gets the timeline driving the #ClutterState
   // 
   // the state change animations. The returned timeline is owned
   // by the #ClutterState and it should not be unreferenced directly
   // RETURNS: the #ClutterTimeline that drives
   Timeline* get_timeline()() nothrow {
      return clutter_state_get_timeline(&this);
   }

   // VERSION: 1.4
   // Removes all keys matching the search criteria passed in arguments.
   // <source_state_name>: the source state name to query, or %NULL for all source states
   // <target_state_name>: the target state name to query, or %NULL for all target states
   // <object>: the specific object instance to list keys for, or %NULL for all managed objects
   // <property_name>: the property name to search for, or %NULL for all properties.
   void remove_key(AT0, AT1, AT2, AT3)(AT0 /*char*/ source_state_name=null, AT1 /*char*/ target_state_name=null, AT2 /*GObject2.Object*/ object=null, AT3 /*char*/ property_name=null) nothrow {
      clutter_state_remove_key(&this, toCString!(char*)(source_state_name), toCString!(char*)(target_state_name), UpCast!(GObject2.Object*)(object), toCString!(char*)(property_name));
   }

   // Unintrospectable method: set() / clutter_state_set()
   // VERSION: 1.4
   // Adds multiple keys to a named state of a #ClutterState instance, specifying
   // the easing mode and value a given property of an object should have at a
   // given progress of the animation.
   // 
   // The mode specified is the easing mode used when going to from the previous
   // key to the specified key.
   // 
   // For instance, the code below:
   // 
   // |[
   // clutter_state_set (state, NULL, "hover",
   // button, "opacity", CLUTTER_LINEAR, 255,
   // button, "scale-x", CLUTTER_EASE_OUT_CUBIC, 1.2,
   // button, "scale-y", CLUTTER_EASE_OUT_CUBIC, 1.2,
   // NULL);
   // ]|
   // 
   // will create a transition from any state (a @source_state_name or NULL is
   // treated as a wildcard) and a state named "hover"; the
   // <emphasis>button</emphasis> object will have the #ClutterActor:opacity
   // property animated to a value of 255 using %CLUTTER_LINEAR as the animation
   // mode, and the #ClutterActor:scale-x and #ClutterActor:scale-y properties
   // animated to a value of 1.2 using %CLUTTER_EASE_OUT_CUBIC as the animation
   // mode. To change the state (and start the transition) you can use the
   // clutter_state_set_state() function:
   // 
   // |[
   // clutter_state_set_state (state, "hover");
   // ]|
   // 
   // If a given object, state_name, property tuple already exist in the
   // #ClutterState instance, then the mode and value will be replaced with
   // the new specified values.
   // 
   // If a property name is prefixed with "delayed::" two additional
   // arguments per key are expected: a value relative to the full state time
   // to pause before transitioning and a similar value to pause after
   // transitioning, e.g.:
   // 
   // |[
   // clutter_state_set (state, "hover", "toggled",
   // button, "delayed::scale-x", CLUTTER_LINEAR, 1.0, 0.2, 0.2,
   // button, "delayed::scale-y", CLUTTER_LINEAR, 1.0, 0.2, 0.2,
   // NULL);
   // ]|
   // 
   // will pause for 20% of the duration of the transition before animating,
   // and 20% of the duration after animating.
   // <source_state_name>: the name of the source state keys are being added for
   // <target_state_name>: the name of the target state keys are being added for
   // <first_object>: a #GObject
   // <first_property_name>: a property of @first_object to specify a key for
   // <first_mode>: the id of the alpha function to use
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias clutter_state_set set; // Variadic
   +/

   // VERSION: 1.4
   // Specifies a #ClutterAnimator to be used when transitioning between
   // the two named states.
   // 
   // The @animator allows specifying a transition between the state that is
   // more elaborate than the basic transitions allowed by the tweening of
   // properties defined in the #ClutterState keys.
   // 
   // If @animator is %NULL it will unset an existing animator.
   // 
   // #ClutterState will take a reference on the passed @animator, if any
   // <source_state_name>: the name of a source state
   // <target_state_name>: the name of a target state
   // <animator>: a #ClutterAnimator instance, or %NULL to unset an existing #ClutterAnimator
   void set_animator(AT0, AT1, AT2)(AT0 /*char*/ source_state_name, AT1 /*char*/ target_state_name, AT2 /*Animator*/ animator=null) nothrow {
      clutter_state_set_animator(&this, toCString!(char*)(source_state_name), toCString!(char*)(target_state_name), UpCast!(Animator*)(animator));
   }

   // VERSION: 1.4
   // Sets the duration of a transition.
   // 
   // If both state names are %NULL the default duration for @state is set.
   // 
   // If only @target_state_name is specified, the passed @duration becomes
   // the default duration for transitions to the target state.
   // 
   // If both states names are specified, the passed @duration only applies
   // to the specified transition.
   // <source_state_name>: the name of the source state, or %NULL
   // <target_state_name>: the name of the target state, or %NULL
   // <duration>: the duration of the transition, in milliseconds
   void set_duration(AT0, AT1)(AT0 /*char*/ source_state_name, AT1 /*char*/ target_state_name, uint duration) nothrow {
      clutter_state_set_duration(&this, toCString!(char*)(source_state_name), toCString!(char*)(target_state_name), duration);
   }

   // VERSION: 1.4
   // Sets one specific end key for a state name, @object, @property_name
   // combination.
   // 
   // chaining of multiple calls
   // RETURNS: the #ClutterState instance, allowing
   // <source_state_name>: the source transition to specify transition for, or %NULL to specify the default fallback when a more specific source state doesn't exist.
   // <target_state_name>: the name of the transition to set a key value for.
   // <object>: the #GObject to set a key for
   // <property_name>: the property to set a key for
   // <mode>: the id of the alpha function to use
   // <value>: the value for property_name of object in state_name
   // <pre_delay>: relative time of the transition to be idle in the beginning of the transition
   // <post_delay>: relative time of the transition to be idle in the end of the transition
   State* set_key(AT0, AT1, AT2, AT3, AT4)(AT0 /*char*/ source_state_name, AT1 /*char*/ target_state_name, AT2 /*GObject2.Object*/ object, AT3 /*char*/ property_name, uint mode, AT4 /*GObject2.Value*/ value, double pre_delay, double post_delay) nothrow {
      return clutter_state_set_key(&this, toCString!(char*)(source_state_name), toCString!(char*)(target_state_name), UpCast!(GObject2.Object*)(object), toCString!(char*)(property_name), mode, UpCast!(GObject2.Value*)(value), pre_delay, post_delay);
   }

   // VERSION: 1.4
   // Change the current state of #ClutterState to @target_state_name.
   // 
   // The state will animate during its transition, see
   // #clutter_state_warp_to_state for animation-free state switching.
   // 
   // Setting a %NULL state will stop the current animation and unset
   // the current state, but keys will be left intact.
   // 
   // state transition. The returned timeline is owned by the #ClutterState
   // and it should not be unreferenced
   // RETURNS: the #ClutterTimeline that drives the
   // <target_state_name>: the state to transition to
   Timeline* set_state(AT0)(AT0 /*char*/ target_state_name) nothrow {
      return clutter_state_set_state(&this, toCString!(char*)(target_state_name));
   }

   // VERSION: 1.4
   // Change to the specified target state immediately with no animation.
   // 
   // See clutter_state_set_state().
   // 
   // state transition. The returned timeline is owned by the #ClutterState
   // and it should not be unreferenced
   // RETURNS: the #ClutterTimeline that drives the
   // <target_state_name>: the state to transition to
   Timeline* warp_to_state(AT0)(AT0 /*char*/ target_state_name) nothrow {
      return clutter_state_warp_to_state(&this, toCString!(char*)(target_state_name));
   }

   // VERSION: 1.4
   // The ::completed signal is emitted when a #ClutterState reaches
   // the target state specified by clutter_state_set_state() or
   // clutter_state_warp_to_state().
   extern (C) alias static void function (State* this_, void* user_data=null) nothrow signal_completed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"completed", CB/*:signal_completed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_completed)||_ttmm!(CB, signal_completed)()) {
      return signal_connect_data!()(&this, cast(char*)"completed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}


// The <structname>ClutterStateClass</structname> structure contains
// only private data
struct StateClass /* Version 1.4 */ {
   private GObject2.ObjectClass parent_class;
   extern (C) void function (State* state) nothrow completed;
   private void*[8] _padding_dummy;
}


// <structname>ClutterStateKey</structname> is an opaque structure whose
// members cannot be accessed directly
struct StateKey /* Version 1.4 */ {

   // VERSION: 1.4
   // Retrieves the easing mode used for @state_key.
   // RETURNS: the mode of a #ClutterStateKey
   c_ulong get_mode()() nothrow {
      return clutter_state_key_get_mode(&this);
   }

   // VERSION: 1.4
   // Retrieves the object instance this #ClutterStateKey applies to.
   // RETURNS: the object this state key applies to.
   GObject2.Object* get_object()() nothrow {
      return clutter_state_key_get_object(&this);
   }

   // VERSION: 1.4
   // Retrieves the duration of the pause after transitioning is complete
   // as a fraction of the total transition time.
   // RETURNS: the post delay, used after doing the transition.
   double get_post_delay()() nothrow {
      return clutter_state_key_get_post_delay(&this);
   }

   // VERSION: 1.4
   // Retrieves the pause before transitioning starts as a fraction of
   // the total transition time.
   // RETURNS: the pre delay used before starting the transition.
   double get_pre_delay()() nothrow {
      return clutter_state_key_get_pre_delay(&this);
   }

   // VERSION: 1.4
   // Retrieves the name of the property this #ClutterStateKey applies to
   // 
   // by the #ClutterStateKey and should never be modified or freed
   // RETURNS: the name of the property. The returned string is owned
   char* get_property_name()() nothrow {
      return clutter_state_key_get_property_name(&this);
   }

   // VERSION: 1.4
   // Retrieves the #GType of the property a key applies to
   // 
   // You can use this type to initialize the #GValue to pass to
   // clutter_state_key_get_value()
   // RETURNS: the #GType of the property
   Type get_property_type()() nothrow {
      return clutter_state_key_get_property_type(&this);
   }

   // VERSION: 1.4
   // Retrieves the name of the source state of the @state_key
   // 
   // if this is the generic state key for the given property when
   // transitioning to the target state. The returned string is owned
   // by the #ClutterStateKey and should never be modified or freed
   // RETURNS: the name of the source state for this key, or %NULL
   char* get_source_state_name()() nothrow {
      return clutter_state_key_get_source_state_name(&this);
   }

   // VERSION: 1.4
   // Get the name of the source state this #ClutterStateKey contains,
   // or NULL if this is the generic state key for the given property
   // when transitioning to the target state.
   // 
   // the key is generic
   // RETURNS: the name of the source state for this key, or NULL if
   char* get_target_state_name()() nothrow {
      return clutter_state_key_get_target_state_name(&this);
   }

   // VERSION: 1.4
   // Retrieves a copy of the value for a #ClutterStateKey.
   // 
   // The #GValue needs to be already initialized for the value type
   // of the property or to a type that allow transformation from the value
   // type of the key.
   // 
   // Use g_value_unset() when done.
   // 
   // and %FALSE otherwise
   // RETURNS: %TRUE if the value was successfully retrieved,
   // <value>: a #GValue initialized with the correct type for the @state_key
   int get_value(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
      return clutter_state_key_get_value(&this, UpCast!(GObject2.Value*)(value));
   }
}

struct StatePrivate {
}

// Named colors, for accessing global colors defined by Clutter
enum StaticColor /* Version 1.6 */ {
   WHITE = 0,
   BLACK = 1,
   RED = 2,
   DARK_RED = 3,
   GREEN = 4,
   DARK_GREEN = 5,
   BLUE = 6,
   DARK_BLUE = 7,
   CYAN = 8,
   DARK_CYAN = 9,
   MAGENTA = 10,
   DARK_MAGENTA = 11,
   YELLOW = 12,
   DARK_YELLOW = 13,
   GRAY = 14,
   DARK_GRAY = 15,
   LIGHT_GRAY = 16,
   BUTTER = 17,
   BUTTER_LIGHT = 18,
   BUTTER_DARK = 19,
   ORANGE = 20,
   ORANGE_LIGHT = 21,
   ORANGE_DARK = 22,
   CHOCOLATE = 23,
   CHOCOLATE_LIGHT = 24,
   CHOCOLATE_DARK = 25,
   CHAMELEON = 26,
   CHAMELEON_LIGHT = 27,
   CHAMELEON_DARK = 28,
   SKY_BLUE = 29,
   SKY_BLUE_LIGHT = 30,
   SKY_BLUE_DARK = 31,
   PLUM = 32,
   PLUM_LIGHT = 33,
   PLUM_DARK = 34,
   SCARLET_RED = 35,
   SCARLET_RED_LIGHT = 36,
   SCARLET_RED_DARK = 37,
   ALUMINIUM_1 = 38,
   ALUMINIUM_2 = 39,
   ALUMINIUM_3 = 40,
   ALUMINIUM_4 = 41,
   ALUMINIUM_5 = 42,
   ALUMINIUM_6 = 43,
   TRANSPARENT = 44
}
enum int StickyKeys_Enable = 65141;
enum int Stop = 269025064;
enum int Subtitle = 269025178;
enum int Super_L = 65515;
enum int Super_R = 65516;
enum int Support = 269025150;
enum int Suspend = 269025191;

// The <structname>ClutterSwipeAction</structname> structure contains
// only private data and should be accessed using the provided API
struct SwipeAction /* : GestureAction */ /* Version 1.8 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance gestureaction;
   GestureAction parent_instance;
   private SwipeActionPrivate* priv;


   // VERSION: 1.8
   // Creates a new #ClutterSwipeAction instance
   // RETURNS: the newly created #ClutterSwipeAction
   static SwipeAction* new_()() nothrow {
      return clutter_swipe_action_new();
   }
   static auto opCall()() {
      return clutter_swipe_action_new();
   }

   // VERSION: 1.8
   // The ::swept signal is emitted when a swipe gesture is recognized on the
   // attached actor.
   // <actor>: the #ClutterActor attached to the @action
   // <direction>: the main direction of the swipe gesture
   extern (C) alias static void function (SwipeAction* this_, Actor* actor, SwipeDirection* direction, void* user_data=null) nothrow signal_swept;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"swept", CB/*:signal_swept*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_swept)||_ttmm!(CB, signal_swept)()) {
      return signal_connect_data!()(&this, cast(char*)"swept",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}


// The <structname>ClutterSwipeActionClass</structname> structure contains
// only private data.
struct SwipeActionClass /* Version 1.8 */ {
   private GestureActionClass parent_class;
   extern (C) void function (SwipeAction* action, Actor* actor, SwipeDirection direction) nothrow swept;
   extern (C) void function () nothrow _clutter_swipe_action1;
   extern (C) void function () nothrow _clutter_swipe_action2;
   extern (C) void function () nothrow _clutter_swipe_action3;
   extern (C) void function () nothrow _clutter_swipe_action4;
   extern (C) void function () nothrow _clutter_swipe_action5;
   extern (C) void function () nothrow _clutter_swipe_action6;
   extern (C) void function () nothrow _clutter_swipe_action7;
}

struct SwipeActionPrivate {
}

// The main direction of the swipe gesture
enum SwipeDirection /* Version 1.8 */ {
   UP = 1,
   DOWN = 2,
   LEFT = 4,
   RIGHT = 8
}
enum int Switch_VT_1 = 269024769;
enum int Switch_VT_10 = 269024778;
enum int Switch_VT_11 = 269024779;
enum int Switch_VT_12 = 269024780;
enum int Switch_VT_2 = 269024770;
enum int Switch_VT_3 = 269024771;
enum int Switch_VT_4 = 269024772;
enum int Switch_VT_5 = 269024773;
enum int Switch_VT_6 = 269024774;
enum int Switch_VT_7 = 269024775;
enum int Switch_VT_8 = 269024776;
enum int Switch_VT_9 = 269024777;
enum int Sys_Req = 65301;
enum int T = 84;
enum int THORN = 222;
enum int Tab = 65289;
// The alignment policies available on each axis of the #ClutterTableLayout
enum TableAlignment /* Version 1.4 */ {
   START = 0,
   CENTER = 1,
   END = 2
}

// The #ClutterTableLayout structure contains only private data
// and should be accessed using the provided API
struct TableLayout /* : LayoutManager */ /* Version 1.4 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance layoutmanager;
   LayoutManager parent_instance;
   private TableLayoutPrivate* priv;


   // VERSION: 1.4
   // Creates a new #ClutterTableLayout layout manager
   // RETURNS: the newly created #ClutterTableLayout
   static TableLayout* new_()() nothrow {
      return clutter_table_layout_new();
   }
   static auto opCall()() {
      return clutter_table_layout_new();
   }

   // VERSION: 1.4
   // Retrieves the horizontal and vertical alignment policies for @actor
   // as set using clutter_table_layout_pack() or
   // clutter_table_layout_set_alignment().
   // <actor>: a #ClutterActor child of @layout
   // <x_align>: return location for the horizontal alignment policy
   // <y_align>: return location for the vertical alignment policy
   void get_alignment(AT0, AT1, AT2)(AT0 /*Actor*/ actor, /*out*/ AT1 /*TableAlignment*/ x_align, /*out*/ AT2 /*TableAlignment*/ y_align) nothrow {
      clutter_table_layout_get_alignment(&this, UpCast!(Actor*)(actor), UpCast!(TableAlignment*)(x_align), UpCast!(TableAlignment*)(y_align));
   }

   // VERSION: 1.4
   // Retrieve the current number of columns in @layout
   // RETURNS: the number of columns
   int get_column_count()() nothrow {
      return clutter_table_layout_get_column_count(&this);
   }

   // VERSION: 1.4
   // Retrieves the spacing set using clutter_table_layout_set_column_spacing()
   // RETURNS: the spacing between columns of the #ClutterTableLayout
   uint get_column_spacing()() nothrow {
      return clutter_table_layout_get_column_spacing(&this);
   }

   // VERSION: 1.4
   // Retrieves the duration set using clutter_table_layout_set_easing_duration()
   // RETURNS: the duration of the animations, in milliseconds
   uint get_easing_duration()() nothrow {
      return clutter_table_layout_get_easing_duration(&this);
   }

   // VERSION: 1.4
   // Retrieves the easing mode set using clutter_table_layout_set_easing_mode()
   // RETURNS: an easing mode
   c_ulong get_easing_mode()() nothrow {
      return clutter_table_layout_get_easing_mode(&this);
   }

   // VERSION: 1.4
   // Retrieves the horizontal and vertical expand policies for @actor
   // as set using clutter_table_layout_pack() or clutter_table_layout_set_expand()
   // <actor>: a #ClutterActor child of @layout
   // <x_expand>: return location for the horizontal expand policy
   // <y_expand>: return location for the vertical expand policy
   void get_expand(AT0)(AT0 /*Actor*/ actor, /*out*/ int* x_expand, /*out*/ int* y_expand) nothrow {
      clutter_table_layout_get_expand(&this, UpCast!(Actor*)(actor), x_expand, y_expand);
   }

   // VERSION: 1.4
   // Retrieves the horizontal and vertical fill policies for @actor
   // as set using clutter_table_layout_pack() or clutter_table_layout_set_fill()
   // <actor>: a #ClutterActor child of @layout
   // <x_fill>: return location for the horizontal fill policy
   // <y_fill>: return location for the vertical fill policy
   void get_fill(AT0)(AT0 /*Actor*/ actor, /*out*/ int* x_fill, /*out*/ int* y_fill) nothrow {
      clutter_table_layout_get_fill(&this, UpCast!(Actor*)(actor), x_fill, y_fill);
   }

   // VERSION: 1.4
   // Retrieve the current number rows in the @layout
   // RETURNS: the number of rows
   int get_row_count()() nothrow {
      return clutter_table_layout_get_row_count(&this);
   }

   // VERSION: 1.4
   // Retrieves the spacing set using clutter_table_layout_set_row_spacing()
   // RETURNS: the spacing between rows of the #ClutterTableLayout
   uint get_row_spacing()() nothrow {
      return clutter_table_layout_get_row_spacing(&this);
   }

   // VERSION: 1.4
   // Retrieves the row and column span for @actor as set using
   // clutter_table_layout_pack() or clutter_table_layout_set_span()
   // <actor>: a #ClutterActor child of @layout
   // <column_span>: return location for the col span
   // <row_span>: return location for the row span
   void get_span(AT0)(AT0 /*Actor*/ actor, /*out*/ int* column_span, /*out*/ int* row_span) nothrow {
      clutter_table_layout_get_span(&this, UpCast!(Actor*)(actor), column_span, row_span);
   }

   // VERSION: 1.4
   // Retrieves whether @layout should animate changes in the layout properties
   // 
   // Since clutter_table_layout_set_use_animations()
   // RETURNS: %TRUE if the animations should be used, %FALSE otherwise
   int get_use_animations()() nothrow {
      return clutter_table_layout_get_use_animations(&this);
   }

   // VERSION: 1.4
   // Packs @actor inside the #ClutterContainer associated to @layout
   // at the given row and column.
   // <actor>: a #ClutterActor
   // <column>: the column the @actor should be put, or -1 to append
   // <row>: the row the @actor should be put, or -1 to append
   void pack(AT0)(AT0 /*Actor*/ actor, int column, int row) nothrow {
      clutter_table_layout_pack(&this, UpCast!(Actor*)(actor), column, row);
   }

   // VERSION: 1.4
   // Sets the horizontal and vertical alignment policies for @actor
   // inside @layout
   // <actor>: a #ClutterActor child of @layout
   // <x_align>: Horizontal alignment policy for @actor
   // <y_align>: Vertical alignment policy for @actor
   void set_alignment(AT0)(AT0 /*Actor*/ actor, TableAlignment x_align, TableAlignment y_align) nothrow {
      clutter_table_layout_set_alignment(&this, UpCast!(Actor*)(actor), x_align, y_align);
   }

   // VERSION: 1.4
   // Sets the spacing between columns of @layout
   // <spacing>: the spacing between columns of the layout, in pixels
   void set_column_spacing()(uint spacing) nothrow {
      clutter_table_layout_set_column_spacing(&this, spacing);
   }

   // VERSION: 1.4
   // Sets the duration of the animations used by @layout when animating changes
   // in the layout properties
   // 
   // Use clutter_table_layout_set_use_animations() to enable and disable the
   // animations
   // <msecs>: the duration of the animations, in milliseconds
   void set_easing_duration()(uint msecs) nothrow {
      clutter_table_layout_set_easing_duration(&this, msecs);
   }

   // VERSION: 1.4
   // Sets the easing mode to be used by @layout when animating changes in layout
   // properties
   // 
   // Use clutter_table_layout_set_use_animations() to enable and disable the
   // animations
   // <mode>: an easing mode, either from #ClutterAnimationMode or a logical id from clutter_alpha_register_func()
   void set_easing_mode()(c_ulong mode) nothrow {
      clutter_table_layout_set_easing_mode(&this, mode);
   }

   // VERSION: 1.4
   // Sets the horizontal and vertical expand policies for @actor
   // inside @layout
   // <actor>: a #ClutterActor child of @layout
   // <x_expand>: whether @actor should allocate extra space horizontally
   // <y_expand>: whether @actor should allocate extra space vertically
   void set_expand(AT0)(AT0 /*Actor*/ actor, int x_expand, int y_expand) nothrow {
      clutter_table_layout_set_expand(&this, UpCast!(Actor*)(actor), x_expand, y_expand);
   }

   // VERSION: 1.4
   // Sets the horizontal and vertical fill policies for @actor
   // inside @layout
   // <actor>: a #ClutterActor child of @layout
   // <x_fill>: whether @actor should fill horizontally the allocated space
   // <y_fill>: whether @actor should fill vertically the allocated space
   void set_fill(AT0)(AT0 /*Actor*/ actor, int x_fill, int y_fill) nothrow {
      clutter_table_layout_set_fill(&this, UpCast!(Actor*)(actor), x_fill, y_fill);
   }

   // VERSION: 1.4
   // Sets the spacing between rows of @layout
   // <spacing>: the spacing between rows of the layout, in pixels
   void set_row_spacing()(uint spacing) nothrow {
      clutter_table_layout_set_row_spacing(&this, spacing);
   }

   // VERSION: 1.4
   // Sets the row and column span for @actor
   // inside @layout
   // <actor>: a #ClutterActor child of @layout
   // <column_span>: Column span for @actor
   // <row_span>: Row span for @actor
   void set_span(AT0)(AT0 /*Actor*/ actor, int column_span, int row_span) nothrow {
      clutter_table_layout_set_span(&this, UpCast!(Actor*)(actor), column_span, row_span);
   }

   // VERSION: 1.4
   // Sets whether @layout should animate changes in the layout properties
   // 
   // The duration of the animations is controlled by
   // clutter_table_layout_set_easing_duration(); the easing mode to be used
   // by the animations is controlled by clutter_table_layout_set_easing_mode()
   // <animate>: %TRUE if the @layout should use animations
   void set_use_animations()(int animate) nothrow {
      clutter_table_layout_set_use_animations(&this, animate);
   }
}


// The #ClutterTableLayoutClass structure contains only private
// data and should be accessed using the provided API
struct TableLayoutClass /* Version 1.4 */ {
   private LayoutManagerClass parent_class;
}

struct TableLayoutPrivate {
}

enum int Tabovedot = 16785002;
enum int TaskPane = 269025151;
enum int Tcaron = 427;
enum int Tcedilla = 478;
enum int Terminal = 269025152;
enum int Terminate_Server = 65237;
// The #ClutterText struct contains only private data.
struct Text /* : Actor */ /* Version 1.0 */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Animatable.__interface__;
   mixin Container.__interface__;
   mixin Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance actor;
   Actor parent_instance;
   private TextPrivate* priv;


   // VERSION: 1.0
   // Creates a new #ClutterText actor. This actor can be used to
   // display and edit text.
   // RETURNS: the newly created #ClutterText actor
   static Text* new_()() nothrow {
      return clutter_text_new();
   }
   static auto opCall()() {
      return clutter_text_new();
   }

   // VERSION: 1.0
   // Creates a new #ClutterText actor, using @font_name as the font
   // description; @text will be used to set the contents of the actor;
   // and @color will be used as the color to render @text.
   // 
   // This function is equivalent to calling clutter_text_new(),
   // clutter_text_set_font_name(), clutter_text_set_text() and
   // clutter_text_set_color().
   // RETURNS: the newly created #ClutterText actor
   // <font_name>: a string with a font description
   // <text>: the contents of the actor
   // <color>: the color to be used to render @text
   static Text* new_full(AT0, AT1, AT2)(AT0 /*char*/ font_name, AT1 /*char*/ text, AT2 /*Color*/ color) nothrow {
      return clutter_text_new_full(toCString!(char*)(font_name), toCString!(char*)(text), UpCast!(Color*)(color));
   }
   static auto opCall(AT0, AT1, AT2)(AT0 /*char*/ font_name, AT1 /*char*/ text, AT2 /*Color*/ color) {
      return clutter_text_new_full(toCString!(char*)(font_name), toCString!(char*)(text), UpCast!(Color*)(color));
   }

   // VERSION: 1.8
   // Creates a new entry with the specified text buffer.
   // RETURNS: a new #ClutterText
   // <buffer>: The buffer to use for the new #ClutterText.
   static Text* new_with_buffer(AT0)(AT0 /*TextBuffer*/ buffer) nothrow {
      return clutter_text_new_with_buffer(UpCast!(TextBuffer*)(buffer));
   }
   static auto opCall(AT0)(AT0 /*TextBuffer*/ buffer) {
      return clutter_text_new_with_buffer(UpCast!(TextBuffer*)(buffer));
   }

   // VERSION: 1.0
   // Creates a new #ClutterText actor, using @font_name as the font
   // description; @text will be used to set the contents of the actor.
   // 
   // This function is equivalent to calling clutter_text_new(),
   // clutter_text_set_font_name(), and clutter_text_set_text().
   // RETURNS: the newly created #ClutterText actor
   // <font_name>: a string with a font description
   // <text>: the contents of the actor
   static Text* new_with_text(AT0, AT1)(AT0 /*char*/ font_name, AT1 /*char*/ text) nothrow {
      return clutter_text_new_with_text(toCString!(char*)(font_name), toCString!(char*)(text));
   }
   static auto opCall(AT0, AT1)(AT0 /*char*/ font_name, AT1 /*char*/ text) {
      return clutter_text_new_with_text(toCString!(char*)(font_name), toCString!(char*)(text));
   }

   // VERSION: 1.0
   // Emits the #ClutterText::activate signal, if @self has been set
   // as activatable using clutter_text_set_activatable().
   // 
   // This function can be used to emit the ::activate signal inside
   // a #ClutterActor::captured-event or #ClutterActor::key-press-event
   // signal handlers before the default signal handler for the
   // #ClutterText is invoked.
   // 
   // and %FALSE otherwise
   // RETURNS: %TRUE if the ::activate signal has been emitted,
   int activate()() nothrow {
      return clutter_text_activate(&this);
   }

   // VERSION: 1.10
   // Retrieves the position of the character at the given coordinates.
   // RETURNS: the position of the character
   // <x>: the X coordinate, relative to the actor
   // <y>: the Y coordinate, relative to the actor
   int coords_to_position()(float x, float y) nothrow {
      return clutter_text_coords_to_position(&this, x, y);
   }

   // VERSION: 1.0
   // Deletes @n_chars inside a #ClutterText actor, starting from the
   // current cursor position.
   // 
   // Somewhat awkwardly, the cursor position is decremented by the same
   // number of characters you've deleted.
   // <n_chars>: the number of characters to delete
   void delete_chars()(uint n_chars) nothrow {
      clutter_text_delete_chars(&this, n_chars);
   }

   // VERSION: 1.0
   // Deletes the currently selected text
   // 
   // This function is only useful in subclasses of #ClutterText
   // 
   // is empty, and %FALSE otherwise
   // RETURNS: %TRUE if text was deleted or if the text actor
   int delete_selection()() nothrow {
      return clutter_text_delete_selection(&this);
   }

   // VERSION: 1.0
   // Deletes the text inside a #ClutterText actor between @start_pos
   // and @end_pos.
   // 
   // The starting and ending positions are expressed in characters,
   // not in bytes.
   // <start_pos>: starting position
   // <end_pos>: ending position
   void delete_text()(ssize_t start_pos, ssize_t end_pos) nothrow {
      clutter_text_delete_text(&this, start_pos, end_pos);
   }

   // VERSION: 1.0
   // Retrieves whether a #ClutterText is activatable or not.
   // RETURNS: %TRUE if the actor is activatable
   int get_activatable()() nothrow {
      return clutter_text_get_activatable(&this);
   }

   // VERSION: 1.0
   // Gets the attribute list that was set on the #ClutterText actor
   // clutter_text_set_attributes(), if any.
   // 
   // returned value is owned by the #ClutterText and should not be unreferenced.
   // RETURNS: the attribute list, or %NULL if none was set. The
   Pango.AttrList* get_attributes()() nothrow {
      return clutter_text_get_attributes(&this);
   }

   // VERSION: 1.8
   // Get the #ClutterTextBuffer object which holds the text for
   // this widget.
   // RETURNS: A #GtkEntryBuffer object.
   TextBuffer* get_buffer()() nothrow {
      return clutter_text_get_buffer(&this);
   }

   // VERSION: 1.0
   // Retrieves the contents of the #ClutterText actor between
   // @start_pos and @end_pos, but not including @end_pos.
   // 
   // The positions are specified in characters, not in bytes.
   // 
   // the text actor between the specified positions. Use g_free()
   // to free the resources when done
   // RETURNS: a newly allocated string with the contents of
   // <start_pos>: start of text, in characters
   // <end_pos>: end of text, in characters
   char* /*new*/ get_chars()(ssize_t start_pos, ssize_t end_pos) nothrow {
      return clutter_text_get_chars(&this, start_pos, end_pos);
   }

   // VERSION: 1.0
   // Retrieves the text color as set by clutter_text_set_color().
   // <color>: return location for a #ClutterColor
   void get_color(AT0)(/*out*/ AT0 /*Color*/ color) nothrow {
      clutter_text_get_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.0
   // Retrieves the color of the cursor of a #ClutterText actor.
   // <color>: return location for a #ClutterColor
   void get_cursor_color(AT0)(/*out*/ AT0 /*Color*/ color) nothrow {
      clutter_text_get_cursor_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.0
   // Retrieves the cursor position.
   // RETURNS: the cursor position, in characters
   int get_cursor_position()() nothrow {
      return clutter_text_get_cursor_position(&this);
   }

   // VERSION: 1.0
   // Retrieves the size of the cursor of a #ClutterText actor.
   // RETURNS: the size of the cursor, in pixels
   uint get_cursor_size()() nothrow {
      return clutter_text_get_cursor_size(&this);
   }

   // VERSION: 1.0
   // Retrieves whether the cursor of a #ClutterText actor is visible.
   // RETURNS: %TRUE if the cursor is visible
   int get_cursor_visible()() nothrow {
      return clutter_text_get_cursor_visible(&this);
   }

   // VERSION: 1.0
   // Retrieves whether a #ClutterText is editable or not.
   // RETURNS: %TRUE if the actor is editable
   int get_editable()() nothrow {
      return clutter_text_get_editable(&this);
   }

   // VERSION: 1.0
   // Returns the ellipsizing position of a #ClutterText actor, as
   // set by clutter_text_set_ellipsize().
   // RETURNS: #PangoEllipsizeMode
   Pango.EllipsizeMode get_ellipsize()() nothrow {
      return clutter_text_get_ellipsize(&this);
   }

   // VERSION: 1.2
   // Retrieves the #PangoFontDescription used by @self
   // 
   // by the #ClutterText actor and it should not be modified or freed
   // RETURNS: a #PangoFontDescription. The returned value is owned
   Pango.FontDescription* /*new*/ get_font_description()() nothrow {
      return clutter_text_get_font_description(&this);
   }

   // VERSION: 1.0
   // Retrieves the font name as set by clutter_text_set_font_name().
   // 
   // string is owned by the #ClutterText actor and should not be
   // modified or freed
   // RETURNS: a string containing the font name. The returned
   char* get_font_name()() nothrow {
      return clutter_text_get_font_name(&this);
   }

   // VERSION: 0.6
   // Retrieves whether the #ClutterText actor should justify its contents
   // on both margins.
   // RETURNS: %TRUE if the text should be justified
   int get_justify()() nothrow {
      return clutter_text_get_justify(&this);
   }

   // VERSION: 1.0
   // Retrieves the current #PangoLayout used by a #ClutterText actor.
   // 
   // the #ClutterText actor and should not be modified or freed
   // RETURNS: a #PangoLayout. The returned object is owned by
   Pango.Layout* get_layout()() nothrow {
      return clutter_text_get_layout(&this);
   }

   // VERSION: 1.8
   // Obtains the coordinates where the #ClutterText will draw the #PangoLayout
   // representing the text.
   // <x>: location to store X offset of layout, or %NULL
   // <y>: location to store Y offset of layout, or %NULL
   void get_layout_offsets()(/*out*/ int* x, /*out*/ int* y) nothrow {
      clutter_text_get_layout_offsets(&this, x, y);
   }

   // VERSION: 1.0
   // Retrieves the alignment of a #ClutterText, as set by
   // clutter_text_set_line_alignment().
   // RETURNS: a #PangoAlignment
   Pango.Alignment get_line_alignment()() nothrow {
      return clutter_text_get_line_alignment(&this);
   }

   // VERSION: 1.0
   // Retrieves the value set using clutter_text_set_line_wrap().
   // 
   // its contents
   // RETURNS: %TRUE if the #ClutterText actor should wrap
   int get_line_wrap()() nothrow {
      return clutter_text_get_line_wrap(&this);
   }

   // VERSION: 1.0
   // Retrieves the line wrap mode used by the #ClutterText actor.
   // 
   // See clutter_text_set_line_wrap_mode ().
   // RETURNS: the wrap mode used by the #ClutterText
   Pango.WrapMode get_line_wrap_mode()() nothrow {
      return clutter_text_get_line_wrap_mode(&this);
   }

   // VERSION: 1.0
   // Gets the maximum length of text that can be set into a text actor.
   // 
   // See clutter_text_set_max_length().
   // RETURNS: the maximum number of characters.
   int get_max_length()() nothrow {
      return clutter_text_get_max_length(&this);
   }

   // VERSION: 1.0
   // Retrieves the character to use in place of the actual text
   // as set by clutter_text_set_password_char().
   // 
   // character is not set
   // RETURNS: a Unicode character or 0 if the password
   dchar get_password_char()() nothrow {
      return clutter_text_get_password_char(&this);
   }

   // VERSION: 1.0
   // Retrieves whether a #ClutterText is selectable or not.
   // RETURNS: %TRUE if the actor is selectable
   int get_selectable()() nothrow {
      return clutter_text_get_selectable(&this);
   }

   // VERSION: 1.8
   // Retrieves the color of selected text of a #ClutterText actor.
   // <color>: return location for a #ClutterColor
   void get_selected_text_color(AT0)(/*out*/ AT0 /*Color*/ color) nothrow {
      clutter_text_get_selected_text_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.0
   // Retrieves the currently selected text.
   // 
   // selected text, or %NULL. Use g_free() to free the returned
   // string.
   // RETURNS: a newly allocated string containing the currently
   char* /*new*/ get_selection()() nothrow {
      return clutter_text_get_selection(&this);
   }

   // VERSION: 1.0
   // Retrieves the other end of the selection of a #ClutterText actor,
   // in characters from the current cursor position.
   // RETURNS: the position of the other end of the selection
   int get_selection_bound()() nothrow {
      return clutter_text_get_selection_bound(&this);
   }

   // VERSION: 1.0
   // Retrieves the color of the selection of a #ClutterText actor.
   // <color>: return location for a #ClutterColor
   void get_selection_color(AT0)(/*out*/ AT0 /*Color*/ color) nothrow {
      clutter_text_get_selection_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.0
   // Retrieves whether the #ClutterText actor is in single line mode.
   // RETURNS: %TRUE if the #ClutterText actor is in single line mode
   int get_single_line_mode()() nothrow {
      return clutter_text_get_single_line_mode(&this);
   }

   // VERSION: 1.0
   // Retrieves a pointer to the current contents of a #ClutterText
   // actor.
   // 
   // If you need a copy of the contents for manipulating, either
   // use g_strdup() on the returned string, or use:
   // 
   // |[
   // copy = clutter_text_get_chars (text, 0, -1);
   // ]|
   // 
   // Which will return a newly allocated string.
   // 
   // If the #ClutterText actor is empty, this function will return
   // an empty string, and not %NULL.
   // 
   // string is owned by the #ClutterText actor and should never be modified
   // or freed
   // RETURNS: the contents of the actor. The returned
   char* get_text()() nothrow {
      return clutter_text_get_text(&this);
   }

   // VERSION: 1.0
   // Retrieves whether the contents of the #ClutterText actor should be
   // parsed for the Pango text markup.
   // RETURNS: %TRUE if the contents will be parsed for markup
   int get_use_markup()() nothrow {
      return clutter_text_get_use_markup(&this);
   }

   // VERSION: 1.0
   // Inserts @text into a #ClutterActor at the given position.
   // 
   // If @position is a negative number, the text will be appended
   // at the end of the current contents of the #ClutterText.
   // 
   // The position is expressed in characters, not in bytes.
   // <text>: the text to be inserted
   // <position>: the position of the insertion, or -1
   void insert_text(AT0)(AT0 /*char*/ text, ssize_t position) nothrow {
      clutter_text_insert_text(&this, toCString!(char*)(text), position);
   }

   // VERSION: 1.0
   // Inserts @wc at the current cursor position of a
   // #ClutterText actor.
   // <wc>: a Unicode character
   void insert_unichar()(dchar wc) nothrow {
      clutter_text_insert_unichar(&this, wc);
   }

   // VERSION: 1.0
   // Retrieves the coordinates of the given @position.
   // RETURNS: %TRUE if the conversion was successful
   // <position>: position in characters
   // <x>: return location for the X coordinate, or %NULL
   // <y>: return location for the Y coordinate, or %NULL
   // <line_height>: return location for the line height, or %NULL
   int position_to_coords(AT0, AT1, AT2)(int position, /*out*/ AT0 /*float*/ x, /*out*/ AT1 /*float*/ y, /*out*/ AT2 /*float*/ line_height) nothrow {
      return clutter_text_position_to_coords(&this, position, UpCast!(float*)(x), UpCast!(float*)(y), UpCast!(float*)(line_height));
   }

   // VERSION: 1.0
   // Sets whether a #ClutterText actor should be activatable.
   // 
   // An activatable #ClutterText actor will emit the #ClutterText::activate
   // signal whenever the 'Enter' (or 'Return') key is pressed; if it is not
   // activatable, a new line will be appended to the current content.
   // 
   // An activatable #ClutterText must also be set as editable using
   // clutter_text_set_editable().
   // <activatable>: whether the #ClutterText actor should be activatable
   void set_activatable()(int activatable) nothrow {
      clutter_text_set_activatable(&this, activatable);
   }

   // VERSION: 1.0
   // Sets the attributes list that are going to be applied to the
   // #ClutterText contents.
   // 
   // The #ClutterText actor will take a reference on the #PangoAttrList
   // passed to this function.
   // <attrs>: a #PangoAttrList or %NULL to unset the attributes
   void set_attributes(AT0)(AT0 /*Pango.AttrList*/ attrs) nothrow {
      clutter_text_set_attributes(&this, UpCast!(Pango.AttrList*)(attrs));
   }

   // VERSION: 1.8
   // Set the #ClutterTextBuffer object which holds the text for
   // this widget.
   // <buffer>: a #ClutterTextBuffer
   void set_buffer(AT0)(AT0 /*TextBuffer*/ buffer) nothrow {
      clutter_text_set_buffer(&this, UpCast!(TextBuffer*)(buffer));
   }

   // VERSION: 1.0
   // Sets the color of the contents of a #ClutterText actor.
   // 
   // The overall opacity of the #ClutterText actor will be the
   // result of the alpha value of @color and the composited
   // opacity of the actor itself on the scenegraph, as returned
   // by clutter_actor_get_paint_opacity().
   // <color>: a #ClutterColor
   void set_color(AT0)(AT0 /*Color*/ color) nothrow {
      clutter_text_set_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.0
   // Sets the color of the cursor of a #ClutterText actor.
   // 
   // If @color is %NULL, the cursor color will be the same as the
   // text color.
   // <color>: the color of the cursor, or %NULL to unset it
   void set_cursor_color(AT0)(AT0 /*Color*/ color) nothrow {
      clutter_text_set_cursor_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.0
   // Sets the cursor of a #ClutterText actor at @position.
   // 
   // The position is expressed in characters, not in bytes.
   // <position>: the new cursor position, in characters
   void set_cursor_position()(int position) nothrow {
      clutter_text_set_cursor_position(&this, position);
   }

   // VERSION: 1.0
   // Sets the size of the cursor of a #ClutterText. The cursor
   // will only be visible if the #ClutterText:cursor-visible property
   // is set to %TRUE.
   // <size>: the size of the cursor, in pixels, or -1 to use the default value
   void set_cursor_size()(int size) nothrow {
      clutter_text_set_cursor_size(&this, size);
   }

   // VERSION: 1.0
   // Sets whether the cursor of a #ClutterText actor should be
   // visible or not.
   // 
   // The color of the cursor will be the same as the text color
   // unless clutter_text_set_cursor_color() has been called.
   // 
   // The size of the cursor can be set using clutter_text_set_cursor_size().
   // 
   // The position of the cursor can be changed programmatically using
   // clutter_text_set_cursor_position().
   // <cursor_visible>: whether the cursor should be visible
   void set_cursor_visible()(int cursor_visible) nothrow {
      clutter_text_set_cursor_visible(&this, cursor_visible);
   }

   // VERSION: 1.0
   // Sets whether the #ClutterText actor should be editable.
   // 
   // An editable #ClutterText with key focus set using
   // clutter_actor_grab_key_focus() or clutter_stage_set_key_focus()
   // will receive key events and will update its contents accordingly.
   // <editable>: whether the #ClutterText should be editable
   void set_editable()(int editable) nothrow {
      clutter_text_set_editable(&this, editable);
   }

   // VERSION: 1.0
   // Sets the mode used to ellipsize (add an ellipsis: "...") to the
   // text if there is not enough space to render the entire contents
   // of a #ClutterText actor
   // <mode>: a #PangoEllipsizeMode
   void set_ellipsize()(Pango.EllipsizeMode mode) nothrow {
      clutter_text_set_ellipsize(&this, mode);
   }

   // VERSION: 1.2
   // Sets @font_desc as the font description for a #ClutterText
   // 
   // The #PangoFontDescription is copied by the #ClutterText actor
   // so you can safely call pango_font_description_free() on it after
   // calling this function.
   // <font_desc>: a #PangoFontDescription
   void set_font_description(AT0)(AT0 /*Pango.FontDescription*/ font_desc) nothrow {
      clutter_text_set_font_description(&this, UpCast!(Pango.FontDescription*)(font_desc));
   }

   // VERSION: 1.0
   // Sets the font used by a #ClutterText. The @font_name string
   // must either be %NULL, which means that the font name from the
   // default #ClutterBackend will be used; or be something that can
   // be parsed by the pango_font_description_from_string() function,
   // like:
   // 
   // |[
   // clutter_text_set_font_name (text, "Sans 10pt");
   // clutter_text_set_font_name (text, "Serif 16px");
   // clutter_text_set_font_name (text, "Helvetica 10");
   // ]|
   // <font_name>: a font name, or %NULL to set the default font name
   void set_font_name(AT0)(AT0 /*char*/ font_name=null) nothrow {
      clutter_text_set_font_name(&this, toCString!(char*)(font_name));
   }

   // VERSION: 1.0
   // Sets whether the text of the #ClutterText actor should be justified
   // on both margins. This setting is ignored if Clutter is compiled
   // against Pango &lt; 1.18.
   // <justify>: whether the text should be justified
   void set_justify()(int justify) nothrow {
      clutter_text_set_justify(&this, justify);
   }

   // VERSION: 1.0
   // Sets the way that the lines of a wrapped label are aligned with
   // respect to each other. This does not affect the overall alignment
   // of the label within its allocated or specified width.
   // 
   // To align a #ClutterText actor you should add it to a container
   // that supports alignment, or use the anchor point.
   // <alignment>: A #PangoAlignment
   void set_line_alignment()(Pango.Alignment alignment) nothrow {
      clutter_text_set_line_alignment(&this, alignment);
   }

   // VERSION: 1.0
   // Sets whether the contents of a #ClutterText actor should wrap,
   // if they don't fit the size assigned to the actor.
   // <line_wrap>: whether the contents should wrap
   void set_line_wrap()(int line_wrap) nothrow {
      clutter_text_set_line_wrap(&this, line_wrap);
   }

   // VERSION: 1.0
   // If line wrapping is enabled (see clutter_text_set_line_wrap()) this
   // function controls how the line wrapping is performed. The default is
   // %PANGO_WRAP_WORD which means wrap on word boundaries.
   // <wrap_mode>: the line wrapping mode
   void set_line_wrap_mode()(Pango.WrapMode wrap_mode) nothrow {
      clutter_text_set_line_wrap_mode(&this, wrap_mode);
   }

   // VERSION: 1.0
   // Sets @markup as the contents of a #ClutterText.
   // 
   // This is a convenience function for setting a string containing
   // Pango markup, and it is logically equivalent to:
   // 
   // |[
   // /&ast; the order is important &ast;/
   // clutter_text_set_text (CLUTTER_TEXT (actor), markup);
   // clutter_text_set_use_markup (CLUTTER_TEXT (actor), TRUE);
   // ]|
   // <markup>: a string containing Pango markup. Passing %NULL is the same as passing "" (the empty string)
   void set_markup(AT0)(AT0 /*char*/ markup) nothrow {
      clutter_text_set_markup(&this, toCString!(char*)(markup));
   }

   // VERSION: 1.0
   // Sets the maximum allowed length of the contents of the actor. If the
   // current contents are longer than the given length, then they will be
   // truncated to fit.
   // <max>: the maximum number of characters allowed in the text actor; 0 to disable or -1 to set the length of the current string
   void set_max_length()(int max) nothrow {
      clutter_text_set_max_length(&this, max);
   }

   // VERSION: 1.0
   // Sets the character to use in place of the actual text in a
   // password text actor.
   // 
   // If @wc is 0 the text will be displayed as it is entered in the
   // #ClutterText actor.
   // <wc>: a Unicode character, or 0 to unset the password character
   void set_password_char()(dchar wc) nothrow {
      clutter_text_set_password_char(&this, wc);
   }

   // VERSION: 1.2
   // Sets, or unsets, the pre-edit string. This function is useful
   // for input methods to display a string (with eventual specific
   // Pango attributes) before it is entered inside the #ClutterText
   // buffer.
   // 
   // The preedit string and attributes are ignored if the #ClutterText
   // actor is not editable.
   // 
   // This function should not be used by applications
   // <preedit_str>: the pre-edit string, or %NULL to unset it
   // <preedit_attrs>: the pre-edit string attributes
   // <cursor_pos>: the cursor position for the pre-edit string
   void set_preedit_string(AT0, AT1)(AT0 /*char*/ preedit_str, AT1 /*Pango.AttrList*/ preedit_attrs, uint cursor_pos) nothrow {
      clutter_text_set_preedit_string(&this, toCString!(char*)(preedit_str), UpCast!(Pango.AttrList*)(preedit_attrs), cursor_pos);
   }

   // VERSION: 1.0
   // Sets whether a #ClutterText actor should be selectable.
   // 
   // A selectable #ClutterText will allow selecting its contents using
   // the pointer or the keyboard.
   // <selectable>: whether the #ClutterText actor should be selectable
   void set_selectable()(int selectable) nothrow {
      clutter_text_set_selectable(&this, selectable);
   }

   // VERSION: 1.8
   // Sets the selected text color of a #ClutterText actor.
   // 
   // If @color is %NULL, the selected text color will be the same as the
   // selection color, which then falls back to cursor, and then text color.
   // <color>: the selected text color, or %NULL to unset it
   void set_selected_text_color(AT0)(AT0 /*Color*/ color) nothrow {
      clutter_text_set_selected_text_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.0
   // Selects the region of text between @start_pos and @end_pos.
   // 
   // This function changes the position of the cursor to match
   // @start_pos and the selection bound to match @end_pos.
   // <start_pos>: start of the selection, in characters
   // <end_pos>: end of the selection, in characters
   void set_selection()(ssize_t start_pos, ssize_t end_pos) nothrow {
      clutter_text_set_selection(&this, start_pos, end_pos);
   }

   // VERSION: 1.0
   // Sets the other end of the selection, starting from the current
   // cursor position.
   // 
   // If @selection_bound is -1, the selection unset.
   // <selection_bound>: the position of the end of the selection, in characters
   void set_selection_bound()(int selection_bound) nothrow {
      clutter_text_set_selection_bound(&this, selection_bound);
   }

   // VERSION: 1.0
   // Sets the color of the selection of a #ClutterText actor.
   // 
   // If @color is %NULL, the selection color will be the same as the
   // cursor color, or if no cursor color is set either then it will be
   // the same as the text color.
   // <color>: the color of the selection, or %NULL to unset it
   void set_selection_color(AT0)(AT0 /*Color*/ color) nothrow {
      clutter_text_set_selection_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.0
   // Sets whether a #ClutterText actor should be in single line mode
   // or not. Only editable #ClutterText<!-- -->s can be in single line
   // mode.
   // 
   // A text actor in single line mode will not wrap text and will clip
   // the visible area to the predefined size. The contents of the
   // text actor will scroll to display the end of the text if its length
   // is bigger than the allocated width.
   // 
   // When setting the single line mode the #ClutterText:activatable
   // property is also set as a side effect. Instead of entering a new
   // line character, the text actor will emit the #ClutterText::activate
   // signal.
   // <single_line>: whether to enable single line mode
   void set_single_line_mode()(int single_line) nothrow {
      clutter_text_set_single_line_mode(&this, single_line);
   }

   // VERSION: 1.0
   // Sets the contents of a #ClutterText actor.
   // 
   // If the #ClutterText:use-markup property was set to %TRUE it
   // will be reset to %FALSE as a side effect. If you want to
   // maintain the #ClutterText:use-markup you should use the
   // clutter_text_set_markup() function instead
   // <text>: the text to set. Passing %NULL is the same as passing "" (the empty string)
   void set_text(AT0)(AT0 /*char*/ text) nothrow {
      clutter_text_set_text(&this, toCString!(char*)(text));
   }

   // VERSION: 1.0
   // Sets whether the contents of the #ClutterText actor contains markup
   // in <link linkend="PangoMarkupFormat">Pango's text markup language</link>.
   // 
   // Setting #ClutterText:use-markup on an editable #ClutterText will
   // not have any effect except hiding the markup.
   // 
   // See also #ClutterText:use-markup.
   // <setting>: %TRUE if the text should be parsed for markup.
   void set_use_markup()(int setting) nothrow {
      clutter_text_set_use_markup(&this, setting);
   }

   // VERSION: 1.0
   // The ::activate signal is emitted each time the actor is 'activated'
   // by the user, normally by pressing the 'Enter' key. The signal is
   // emitted only if #ClutterText:activatable is set to %TRUE.
   extern (C) alias static void function (Text* this_, void* user_data=null) nothrow signal_activate;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"activate", CB/*:signal_activate*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_activate)||_ttmm!(CB, signal_activate)()) {
      return signal_connect_data!()(&this, cast(char*)"activate",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.0
   // The ::cursor-event signal is emitted whenever the cursor position
   // changes inside a #ClutterText actor. Inside @geometry it is stored
   // the current position and size of the cursor, relative to the actor
   // itself.
   // <geometry>: the coordinates of the cursor
   extern (C) alias static void function (Text* this_, Geometry* geometry, void* user_data=null) nothrow signal_cursor_event;
   ulong signal_connect(string name:"cursor-event", CB/*:signal_cursor_event*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_cursor_event)||_ttmm!(CB, signal_cursor_event)()) {
      return signal_connect_data!()(&this, cast(char*)"cursor-event",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.2
   // This signal is emitted when text is deleted from the actor by
   // the user. It is emitted before @self text changes.
   // <start_pos>: the starting position
   // <end_pos>: the end position
   extern (C) alias static void function (Text* this_, int start_pos, int end_pos, void* user_data=null) nothrow signal_delete_text;
   ulong signal_connect(string name:"delete-text", CB/*:signal_delete_text*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_delete_text)||_ttmm!(CB, signal_delete_text)()) {
      return signal_connect_data!()(&this, cast(char*)"delete-text",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.2
   // This signal is emitted when text is inserted into the actor by
   // the user. It is emitted before @self text changes.
   // <new_text>: the new text to insert
   // <new_text_length>: the length of the new text, in bytes, or -1 if new_text is nul-terminated
   // <position>: the position, in characters, at which to insert the new text. this is an in-out parameter.  After the signal emission is finished, it should point after the newly inserted text.
   extern (C) alias static void function (Text* this_, char* new_text, int new_text_length, void* position, void* user_data=null) nothrow signal_insert_text;
   ulong signal_connect(string name:"insert-text", CB/*:signal_insert_text*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_insert_text)||_ttmm!(CB, signal_insert_text)()) {
      return signal_connect_data!()(&this, cast(char*)"insert-text",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.0
   // The ::text-changed signal is emitted after @actor's text changes
   extern (C) alias static void function (Text* this_, void* user_data=null) nothrow signal_text_changed;
   ulong signal_connect(string name:"text-changed", CB/*:signal_text_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_text_changed)||_ttmm!(CB, signal_text_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"text-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct TextBuffer /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private TextBufferPrivate* priv;


   // VERSION: 1.10
   // Create a new ClutterTextBuffer object.
   // RETURNS: A new ClutterTextBuffer object.
   static TextBuffer* /*new*/ new_()() nothrow {
      return clutter_text_buffer_new();
   }
   static auto opCall()() {
      return clutter_text_buffer_new();
   }

   // VERSION: 1.10
   // Create a new ClutterTextBuffer object with some text.
   // RETURNS: A new ClutterTextBuffer object.
   // <text>: initial buffer text
   // <text_len>: initial buffer text length, or -1 for null-terminated.
   static TextBuffer* /*new*/ new_with_text(AT0)(AT0 /*char*/ text, ssize_t text_len) nothrow {
      return clutter_text_buffer_new_with_text(toCString!(char*)(text), text_len);
   }
   static auto opCall(AT0)(AT0 /*char*/ text, ssize_t text_len) {
      return clutter_text_buffer_new_with_text(toCString!(char*)(text), text_len);
   }

   // VERSION: 1.10
   // Deletes a sequence of characters from the buffer. @n_chars characters are
   // deleted starting at @position. If @n_chars is negative, then all characters
   // until the end of the text are deleted.
   // 
   // If @position or @n_chars are out of bounds, then they are coerced to sane
   // values.
   // 
   // Note that the positions are specified in characters, not bytes.
   // RETURNS: The number of characters deleted.
   // <position>: position at which to delete text
   // <n_chars>: number of characters to delete
   uint delete_text()(uint position, int n_chars) nothrow {
      return clutter_text_buffer_delete_text(&this, position, n_chars);
   }

   // VERSION: 1.10
   // Emits the #ClutterTextBuffer::deleted-text signal on @buffer.
   // 
   // Used when subclassing #ClutterTextBuffer
   // <position>: position at which text was deleted
   // <n_chars>: number of characters deleted
   void emit_deleted_text()(uint position, uint n_chars) nothrow {
      clutter_text_buffer_emit_deleted_text(&this, position, n_chars);
   }

   // VERSION: 1.10
   // Emits the #ClutterTextBuffer::inserted-text signal on @buffer.
   // 
   // Used when subclassing #ClutterTextBuffer
   // <position>: position at which text was inserted
   // <chars>: text that was inserted
   // <n_chars>: number of characters inserted
   void emit_inserted_text(AT0)(uint position, AT0 /*char*/ chars, uint n_chars) nothrow {
      clutter_text_buffer_emit_inserted_text(&this, position, toCString!(char*)(chars), n_chars);
   }

   // VERSION: 1.10
   // Retrieves the length in bytes of the buffer.
   // See clutter_text_buffer_get_length().
   // RETURNS: The byte length of the buffer.
   size_t get_bytes()() nothrow {
      return clutter_text_buffer_get_bytes(&this);
   }

   // VERSION: 1.10
   // Retrieves the length in characters of the buffer.
   // RETURNS: The number of characters in the buffer.
   uint get_length()() nothrow {
      return clutter_text_buffer_get_length(&this);
   }

   // VERSION: 1.10
   // Retrieves the maximum allowed length of the text in
   // @buffer. See clutter_text_buffer_set_max_length().
   // 
   // in #ClutterTextBuffer, or 0 if there is no maximum.
   // RETURNS: the maximum allowed number of characters
   int get_max_length()() nothrow {
      return clutter_text_buffer_get_max_length(&this);
   }

   // VERSION: 1.10
   // Retrieves the contents of the buffer.
   // 
   // The memory pointer returned by this call will not change
   // unless this object emits a signal, or is finalized.
   // 
   // string. This string points to internally allocated
   // storage in the buffer and must not be freed, modified or
   // stored.
   // RETURNS: a pointer to the contents of the widget as a
   char* get_text()() nothrow {
      return clutter_text_buffer_get_text(&this);
   }

   // VERSION: 1.10
   // Inserts @n_chars characters of @chars into the contents of the
   // buffer, at position @position.
   // 
   // If @n_chars is negative, then characters from chars will be inserted
   // until a null-terminator is found. If @position or @n_chars are out of
   // bounds, or the maximum buffer text length is exceeded, then they are
   // coerced to sane values.
   // 
   // Note that the position and length are in characters, not in bytes.
   // RETURNS: The number of characters actually inserted.
   // <position>: the position at which to insert text.
   // <chars>: the text to insert into the buffer.
   // <n_chars>: the length of the text in characters, or -1
   uint insert_text(AT0)(uint position, AT0 /*char*/ chars, int n_chars) nothrow {
      return clutter_text_buffer_insert_text(&this, position, toCString!(char*)(chars), n_chars);
   }

   // VERSION: 1.10
   // Sets the maximum allowed length of the contents of the buffer. If
   // the current contents are longer than the given length, then they
   // will be truncated to fit.
   // <max_length>: the maximum length of the entry buffer, or 0 for no maximum. (other than the maximum length of entries.) The value passed in will be clamped to the range [ 0, %CLUTTER_TEXT_BUFFER_MAX_SIZE ].
   void set_max_length()(int max_length) nothrow {
      clutter_text_buffer_set_max_length(&this, max_length);
   }

   // VERSION: 1.10
   // Sets the text in the buffer.
   // 
   // This is roughly equivalent to calling clutter_text_buffer_delete_text()
   // and clutter_text_buffer_insert_text().
   // 
   // Note that @n_chars is in characters, not in bytes.
   // <chars>: the new text
   // <n_chars>: the number of characters in @text, or -1
   void set_text(AT0)(AT0 /*char*/ chars, int n_chars) nothrow {
      clutter_text_buffer_set_text(&this, toCString!(char*)(chars), n_chars);
   }

   // VERSION: 1.10
   // This signal is emitted after text is deleted from the buffer.
   // <position>: the position the text was deleted at.
   // <n_chars>: The number of characters that were deleted.
   extern (C) alias static void function (TextBuffer* this_, c_uint position, c_uint n_chars, void* user_data=null) nothrow signal_deleted_text;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"deleted-text", CB/*:signal_deleted_text*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_deleted_text)||_ttmm!(CB, signal_deleted_text)()) {
      return signal_connect_data!()(&this, cast(char*)"deleted-text",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.10
   // This signal is emitted after text is inserted into the buffer.
   // <position>: the position the text was inserted at.
   // <chars>: The text that was inserted.
   // <n_chars>: The number of characters that were inserted.
   extern (C) alias static void function (TextBuffer* this_, c_uint position, char* chars, c_uint n_chars, void* user_data=null) nothrow signal_inserted_text;
   ulong signal_connect(string name:"inserted-text", CB/*:signal_inserted_text*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_inserted_text)||_ttmm!(CB, signal_inserted_text)()) {
      return signal_connect_data!()(&this, cast(char*)"inserted-text",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct TextBufferClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function (TextBuffer* buffer, uint position, char* chars, uint n_chars) nothrow inserted_text;
   extern (C) void function (TextBuffer* buffer, uint position, uint n_chars) nothrow deleted_text;
   extern (C) char* function (TextBuffer* buffer, size_t* n_bytes) nothrow get_text;
   // RETURNS: The number of characters in the buffer.
   extern (C) uint function (TextBuffer* buffer) nothrow get_length;

   // RETURNS: The number of characters actually inserted.
   // <position>: the position at which to insert text.
   // <chars>: the text to insert into the buffer.
   // <n_chars>: the length of the text in characters, or -1
   extern (C) uint function (TextBuffer* buffer, uint position, char* chars, uint n_chars) nothrow insert_text;

   // RETURNS: The number of characters deleted.
   // <position>: position at which to delete text
   // <n_chars>: number of characters to delete
   extern (C) uint function (TextBuffer* buffer, uint position, uint n_chars) nothrow delete_text;
   extern (C) void function () nothrow _clutter_reserved1;
   extern (C) void function () nothrow _clutter_reserved2;
   extern (C) void function () nothrow _clutter_reserved3;
   extern (C) void function () nothrow _clutter_reserved4;
   extern (C) void function () nothrow _clutter_reserved5;
   extern (C) void function () nothrow _clutter_reserved6;
   extern (C) void function () nothrow _clutter_reserved7;
   extern (C) void function () nothrow _clutter_reserved8;
}

struct TextBufferPrivate {
}

// The #ClutterTextClass struct contains only private data.
struct TextClass /* Version 1.0 */ {
   private ActorClass parent_class;
   extern (C) void function (Text* self) nothrow text_changed;
   extern (C) void function (Text* self) nothrow activate;
   extern (C) void function (Text* self, Geometry* geometry) nothrow cursor_event;
   extern (C) void function () nothrow _clutter_reserved1;
   extern (C) void function () nothrow _clutter_reserved2;
   extern (C) void function () nothrow _clutter_reserved3;
   extern (C) void function () nothrow _clutter_reserved4;
   extern (C) void function () nothrow _clutter_reserved5;
   extern (C) void function () nothrow _clutter_reserved6;
   extern (C) void function () nothrow _clutter_reserved7;
   extern (C) void function () nothrow _clutter_reserved8;
}

// The text direction to be used by #ClutterActor<!-- -->s
enum TextDirection /* Version 1.2 */ {
   DEFAULT = 0,
   LTR = 1,
   RTL = 2
}
struct TextPrivate {
}


// The #ClutterTexture structure contains only private data
// and should be accessed using the provided API
struct Texture /* : Actor */ /* Version 0.1 */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Animatable.__interface__;
   mixin Container.__interface__;
   mixin Scriptable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent actor;
   Actor parent;
   private TexturePrivate* priv;


   // Creates a new empty #ClutterTexture object.
   // RETURNS: A newly created #ClutterTexture object.
   static Texture* new_()() nothrow {
      return clutter_texture_new();
   }
   static auto opCall()() {
      return clutter_texture_new();
   }

   // VERSION: 0.6
   // DEPRECATED (v1.8) constructor: new_from_actor - Use the #ClutterOffscreenEffect and #ClutterShaderEffect
   // Creates a new #ClutterTexture object with its source a prexisting
   // actor (and associated children). The textures content will contain
   // 'live' redirected output of the actors scene.
   // 
   // Note this function is intented as a utility call for uniformly applying
   // shaders to groups and other potential visual effects. It requires that
   // the %CLUTTER_FEATURE_OFFSCREEN feature is supported by the current backend
   // and the target system.
   // 
   // Some tips on usage:
   // 
   // <itemizedlist>
   // <listitem>
   // <para>The source actor must be made visible (i.e by calling
   // #clutter_actor_show).</para>
   // </listitem>
   // <listitem>
   // <para>The source actor must have a parent in order for it to be
   // allocated a size from the layouting mechanism. If the source
   // actor does not have a parent when this function is called then
   // the ClutterTexture will adopt it and allocate it at its
   // preferred size. Using this you can clone an actor that is
   // otherwise not displayed. Because of this feature if you do
   // intend to display the source actor then you must make sure that
   // the actor is parented before calling
   // clutter_texture_new_from_actor() or that you unparent it before
   // adding it to a container.</para>
   // </listitem>
   // <listitem>
   // <para>When getting the image for the clone texture, Clutter
   // will attempt to render the source actor exactly as it would
   // appear if it was rendered on screen. The source actor's parent
   // transformations are taken into account. Therefore if your
   // source actor is rotated along the X or Y axes so that it has
   // some depth, the texture will appear differently depending on
   // the on-screen location of the source actor. While painting the
   // source actor, Clutter will set up a temporary asymmetric
   // perspective matrix as the projection matrix so that the source
   // actor will be projected as if a small section of the screen was
   // being viewed. Before version 0.8.2, an orthogonal identity
   // projection was used which meant that the source actor would be
   // clipped if any part of it was not on the zero Z-plane.</para>
   // </listitem>
   // <listitem>
   // <para>Avoid reparenting the source with the created texture.</para>
   // </listitem>
   // <listitem>
   // <para>A group can be padded with a transparent rectangle as to
   // provide a border to contents for shader output (blurring text
   // for example).</para>
   // </listitem>
   // <listitem>
   // <para>The texture will automatically resize to contain a further
   // transformed source. However, this involves overhead and can be
   // avoided by placing the source actor in a bounding group
   // sized large enough to contain any child tranformations.</para>
   // </listitem>
   // <listitem>
   // <para>Uploading pixel data to the texture (e.g by using
   // clutter_texture_set_from_file()) will destroy the offscreen texture
   // data and end redirection.</para>
   // </listitem>
   // <listitem>
   // <para>cogl_texture_get_data() with the handle returned by
   // clutter_texture_get_cogl_texture() can be used to read the
   // offscreen texture pixels into a pixbuf.</para>
   // </listitem>
   // </itemizedlist>
   // 
   // 
   // directly on the intended #ClutterActor to replace the functionality of
   // this function.
   // RETURNS: A newly created #ClutterTexture object, or %NULL on failure.
   // <actor>: A source #ClutterActor
   static Texture* new_from_actor(AT0)(AT0 /*Actor*/ actor) nothrow {
      return clutter_texture_new_from_actor(UpCast!(Actor*)(actor));
   }
   static auto opCall(AT0)(AT0 /*Actor*/ actor) {
      return clutter_texture_new_from_actor(UpCast!(Actor*)(actor));
   }

   // VERSION: 0.8
   // Creates a new ClutterTexture actor to display the image contained a
   // file. If the image failed to load then NULL is returned and @error
   // is set.
   // 
   // error.
   // RETURNS: A newly created #ClutterTexture object or NULL on
   // <filename>: The name of an image file to load.
   static Texture* new_from_file(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) nothrow {
      return clutter_texture_new_from_file(toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }
   static auto opCall(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) {
      return clutter_texture_new_from_file(toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }

   // Gets the size in pixels of the untransformed underlying image
   // <width>: return location for the width, or %NULL
   // <height>: return location for the height, or %NULL
   void get_base_size()(/*out*/ int* width, /*out*/ int* height) nothrow {
      clutter_texture_get_base_size(&this, width, height);
   }

   // VERSION: 1.0
   // Returns a handle to the underlying COGL material used for drawing
   // the actor.
   // 
   // material is owned by the #ClutterTexture and it should not be
   // unreferenced
   // RETURNS: a handle for a #CoglMaterial. The
   Cogl.Handle get_cogl_material()() nothrow {
      return clutter_texture_get_cogl_material(&this);
   }

   // VERSION: 0.8
   // Retrieves the handle to the underlying COGL texture used for drawing
   // the actor. No extra reference is taken so if you need to keep the
   // handle then you should call cogl_handle_ref() on it.
   // 
   // The texture handle returned is the first layer of the material
   // handle used by the #ClutterTexture. If you need to access the other
   // layers you should use clutter_texture_get_cogl_material() instead
   // and use the #CoglMaterial API.
   // 
   // handle is owned by the #ClutterTexture and it should not be unreferenced
   // RETURNS: a #CoglHandle for the texture. The returned
   Cogl.Handle get_cogl_texture()() nothrow {
      return clutter_texture_get_cogl_texture(&this);
   }

   // VERSION: 0.8
   // Gets the filter quality used when scaling a texture.
   // RETURNS: The filter quality value.
   TextureQuality get_filter_quality()() nothrow {
      return clutter_texture_get_filter_quality(&this);
   }

   // VERSION: 1.0
   // Retrieves the value set using clutter_texture_set_keep_aspect_ratio()
   // 
   // aspect ratio of the underlying image
   // RETURNS: %TRUE if the #ClutterTexture should maintain the
   int get_keep_aspect_ratio()() nothrow {
      return clutter_texture_get_keep_aspect_ratio(&this);
   }

   // VERSION: 1.0
   // Retrieves the value set using clutter_texture_set_load_async()
   // 
   // disk asynchronously
   // RETURNS: %TRUE if the #ClutterTexture should load the data from
   int get_load_async()() nothrow {
      return clutter_texture_get_load_async(&this);
   }

   // VERSION: 1.0
   // Retrieves the value set by clutter_texture_set_load_data_async()
   // 
   // data from a file asynchronously
   // RETURNS: %TRUE if the #ClutterTexture should load the image
   int get_load_data_async()() nothrow {
      return clutter_texture_get_load_data_async(&this);
   }

   // VERSION: 0.8
   // Gets the maximum waste that will be used when creating a texture or
   // -1 if slicing is disabled.
   // 
   // unlimited.
   // RETURNS: The maximum waste or -1 if the texture waste is
   int get_max_tile_waste()() nothrow {
      return clutter_texture_get_max_tile_waste(&this);
   }

   // VERSION: 1.4
   // Retrieves the value set by clutter_texture_set_load_data_async()
   // 
   // using the alpha channel when picking.
   // RETURNS: %TRUE if the #ClutterTexture should define its shape
   int get_pick_with_alpha()() nothrow {
      return clutter_texture_get_pick_with_alpha(&this);
   }

   // VERSION: 1.0
   // Retrieves the pixel format used by @texture. This is
   // equivalent to:
   // 
   // |[
   // handle = clutter_texture_get_pixel_format (texture);
   // 
   // if (handle != COGL_INVALID_HANDLE)
   // format = cogl_texture_get_format (handle);
   // ]|
   // RETURNS: a #CoglPixelFormat value
   Cogl.PixelFormat get_pixel_format()() nothrow {
      return clutter_texture_get_pixel_format(&this);
   }

   // VERSION: 1.0
   // Retrieves the horizontal and vertical repeat values set
   // using clutter_texture_set_repeat()
   // <repeat_x>: return location for the horizontal repeat
   // <repeat_y>: return location for the vertical repeat
   void get_repeat()(/*out*/ int* repeat_x, /*out*/ int* repeat_y) nothrow {
      clutter_texture_get_repeat(&this, repeat_x, repeat_y);
   }

   // VERSION: 1.0
   // Retrieves the value set with clutter_texture_set_sync_size()
   // 
   // preferred size of the underlying image data
   // RETURNS: %TRUE if the #ClutterTexture should have the same
   int get_sync_size()() nothrow {
      return clutter_texture_get_sync_size(&this);
   }

   // VERSION: 0.6
   // Updates a sub-region of the pixel data in a #ClutterTexture.
   // RETURNS: %TRUE on success, %FALSE on failure.
   // <data>: Image data in RGB type colorspace.
   // <has_alpha>: Set to TRUE if image data has an alpha channel.
   // <x>: X coordinate of upper left corner of region to update.
   // <y>: Y coordinate of upper left corner of region to update.
   // <width>: Width in pixels of region to update.
   // <height>: Height in pixels of region to update.
   // <rowstride>: Distance in bytes between row starts on source buffer.
   // <bpp>: bytes per pixel (Currently only 3 and 4 supported, depending on @has_alpha)
   // <flags>: #ClutterTextureFlags
   int set_area_from_rgb_data(AT0, AT1)(AT0 /*ubyte*/ data, int has_alpha, int x, int y, int width, int height, int rowstride, int bpp, TextureFlags flags, AT1 /*GLib2.Error**/ error=null) nothrow {
      return clutter_texture_set_area_from_rgb_data(&this, UpCast!(ubyte*)(data), has_alpha, x, y, width, height, rowstride, bpp, flags, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 0.8
   // Replaces the underlying Cogl material drawn by this actor with
   // @cogl_material. A reference to the material is taken so if the
   // handle is no longer needed it should be deref'd with
   // cogl_handle_unref. Texture data is attached to the material so
   // calling this function also replaces the Cogl
   // texture. #ClutterTexture requires that the material have a texture
   // layer so you should set one on the material before calling this
   // function.
   // <cogl_material>: A CoglHandle for a material
   void set_cogl_material()(Cogl.Handle cogl_material) nothrow {
      clutter_texture_set_cogl_material(&this, cogl_material);
   }

   // VERSION: 0.8
   // Replaces the underlying COGL texture drawn by this actor with
   // @cogl_tex. A reference to the texture is taken so if the handle is
   // no longer needed it should be deref'd with cogl_handle_unref.
   // <cogl_tex>: A CoglHandle for a texture
   void set_cogl_texture()(Cogl.Handle cogl_tex) nothrow {
      clutter_texture_set_cogl_texture(&this, cogl_tex);
   }

   // VERSION: 0.8
   // Sets the filter quality when scaling a texture. The quality is an
   // enumeration currently the following values are supported:
   // %CLUTTER_TEXTURE_QUALITY_LOW which is fast but only uses nearest neighbour
   // interpolation. %CLUTTER_TEXTURE_QUALITY_MEDIUM which is computationally a
   // bit more expensive (bilinear interpolation), and
   // %CLUTTER_TEXTURE_QUALITY_HIGH which uses extra texture memory resources to
   // improve scaled down rendering as well (by using mipmaps). The default value
   // is %CLUTTER_TEXTURE_QUALITY_MEDIUM.
   // <filter_quality>: new filter quality value
   void set_filter_quality()(TextureQuality filter_quality) nothrow {
      clutter_texture_set_filter_quality(&this, filter_quality);
   }

   // VERSION: 0.8
   // Sets the #ClutterTexture image data from an image file. In case of
   // failure, %FALSE is returned and @error is set.
   // 
   // If #ClutterTexture:load-async is set to %TRUE, this function
   // will return as soon as possible, and the actual image loading
   // from disk will be performed asynchronously. #ClutterTexture::size-change
   // will be emitten when the size of the texture is available and
   // #ClutterTexture::load-finished will be emitted when the image has been
   // loaded or if an error occurred.
   // RETURNS: %TRUE if the image was successfully loaded and set
   // <filename>: The filename of the image in GLib file name encoding
   int set_from_file(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) nothrow {
      return clutter_texture_set_from_file(&this, toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 0.4.
   // Sets #ClutterTexture image data.
   // RETURNS: %TRUE on success, %FALSE on failure.
   // <data>: image data in RGBA type colorspace.
   // <has_alpha>: set to %TRUE if image data has an alpha channel.
   // <width>: width in pixels of image data.
   // <height>: height in pixels of image data
   // <rowstride>: distance in bytes between row starts.
   // <bpp>: bytes per pixel (currently only 3 and 4 supported, depending on the value of @has_alpha)
   // <flags>: #ClutterTextureFlags
   int set_from_rgb_data(AT0, AT1)(AT0 /*ubyte*/ data, int has_alpha, int width, int height, int rowstride, int bpp, TextureFlags flags, AT1 /*GLib2.Error**/ error=null) nothrow {
      return clutter_texture_set_from_rgb_data(&this, UpCast!(ubyte*)(data), has_alpha, width, height, rowstride, bpp, flags, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 0.4
   // DEPRECATED (v1.10) method: set_from_yuv_data - Use clutter_texture_get_cogl_material() and
   // Sets a #ClutterTexture from YUV image data. If an error occurred,
   // %FALSE is returned and @error is set.
   // 
   // The YUV support depends on the driver; the format supported by the
   // few drivers exposing this capability are not really useful.
   // 
   // The proper way to convert image data in any YUV colorspace to any
   // RGB colorspace is to use a fragment shader associated with the
   // #ClutterTexture material.
   // 
   // 
   // 
   // the Cogl API to install a fragment shader for decoding YUV
   // formats on the GPU
   // RETURNS: %TRUE if the texture was successfully updated
   // <data>: Image data in YUV type colorspace.
   // <width>: Width in pixels of image data.
   // <height>: Height in pixels of image data
   // <flags>: #ClutterTextureFlags
   int set_from_yuv_data(AT0, AT1)(AT0 /*ubyte*/ data, int width, int height, TextureFlags flags, AT1 /*GLib2.Error**/ error=null) nothrow {
      return clutter_texture_set_from_yuv_data(&this, UpCast!(ubyte*)(data), width, height, flags, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.0
   // Sets whether @texture should have a preferred size maintaining
   // the aspect ratio of the underlying image
   // <keep_aspect>: %TRUE to maintain aspect ratio
   void set_keep_aspect_ratio()(int keep_aspect) nothrow {
      clutter_texture_set_keep_aspect_ratio(&this, keep_aspect);
   }

   // VERSION: 1.0
   // Sets whether @texture should use a worker thread to load the data
   // from disk asynchronously. Setting @load_async to %TRUE will make
   // clutter_texture_set_from_file() return immediately.
   // 
   // See the #ClutterTexture:load-async property documentation, and
   // clutter_texture_set_load_data_async().
   // <load_async>: %TRUE if the texture should asynchronously load data from a filename
   void set_load_async()(int load_async) nothrow {
      clutter_texture_set_load_async(&this, load_async);
   }

   // VERSION: 1.0
   // Sets whether @texture should use a worker thread to load the data
   // from disk asynchronously. Setting @load_async to %TRUE will make
   // clutter_texture_set_from_file() block until the #ClutterTexture has
   // determined the width and height of the image data.
   // 
   // See the #ClutterTexture:load-async property documentation, and
   // clutter_texture_set_load_async().
   // <load_async>: %TRUE if the texture should asynchronously load data from a filename
   void set_load_data_async()(int load_async) nothrow {
      clutter_texture_set_load_data_async(&this, load_async);
   }

   // VERSION: 1.4
   // Sets whether @texture should have it's shape defined by the alpha
   // channel when picking.
   // 
   // Be aware that this is a bit more costly than the default picking
   // due to the texture lookup, extra test against the alpha value and
   // the fact that it will also interrupt the batching of geometry done
   // internally.
   // 
   // Also there is currently no control over the threshold used to
   // determine what value of alpha is considered pickable, and so only
   // fully opaque parts of the texture will react to picking.
   // <pick_with_alpha>: %TRUE if the alpha channel should affect the picking shape
   void set_pick_with_alpha()(int pick_with_alpha) nothrow {
      clutter_texture_set_pick_with_alpha(&this, pick_with_alpha);
   }

   // VERSION: 1.0
   // Sets whether the @texture should repeat horizontally or
   // vertically when the actor size is bigger than the image size
   // <repeat_x>: %TRUE if the texture should repeat horizontally
   // <repeat_y>: %TRUE if the texture should repeat vertically
   void set_repeat()(int repeat_x, int repeat_y) nothrow {
      clutter_texture_set_repeat(&this, repeat_x, repeat_y);
   }

   // VERSION: 1.0
   // Sets whether @texture should have the same preferred size as the
   // underlying image data.
   // <sync_size>: %TRUE if the texture should have the same size of the underlying image data
   void set_sync_size()(int sync_size) nothrow {
      clutter_texture_set_sync_size(&this, sync_size);
   }

   // VERSION: 1.0
   // The ::load-finished signal is emitted when a texture load has
   // completed. If there was an error during loading, @error will
   // be set, otherwise it will be %NULL
   // <error>: A set error, or %NULL
   extern (C) alias static void function (Texture* this_, GLib2.Error* error, void* user_data=null) nothrow signal_load_finished;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"load-finished", CB/*:signal_load_finished*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_load_finished)||_ttmm!(CB, signal_load_finished)()) {
      return signal_connect_data!()(&this, cast(char*)"load-finished",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // The ::pixbuf-change signal is emitted each time the pixbuf
   // used by @texture changes.
   extern (C) alias static void function (Texture* this_, void* user_data=null) nothrow signal_pixbuf_change;
   ulong signal_connect(string name:"pixbuf-change", CB/*:signal_pixbuf_change*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_pixbuf_change)||_ttmm!(CB, signal_pixbuf_change)()) {
      return signal_connect_data!()(&this, cast(char*)"pixbuf-change",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // The ::size-change signal is emitted each time the size of the
   // pixbuf used by @texture changes.  The new size is given as
   // argument to the callback.
   // <width>: the width of the new texture
   // <height>: the height of the new texture
   extern (C) alias static void function (Texture* this_, int width, int height, void* user_data=null) nothrow signal_size_change;
   ulong signal_connect(string name:"size-change", CB/*:signal_size_change*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_size_change)||_ttmm!(CB, signal_size_change)()) {
      return signal_connect_data!()(&this, cast(char*)"size-change",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// The #ClutterTextureClass structure contains only private data
struct TextureClass /* Version 0.1 */ {
   private ActorClass parent_class;
   extern (C) void function (Texture* texture, int width, int height) nothrow size_change;
   extern (C) void function (Texture* texture) nothrow pixbuf_change;
   extern (C) void function (Texture* texture, GLib2.Error* error) nothrow load_finished;
   extern (C) void function () nothrow _clutter_texture1;
   extern (C) void function () nothrow _clutter_texture2;
   extern (C) void function () nothrow _clutter_texture3;
   extern (C) void function () nothrow _clutter_texture4;
   extern (C) void function () nothrow _clutter_texture5;
}

// Error enumeration for #ClutterTexture
enum TextureError /* Version 0.4 */ {
   OUT_OF_MEMORY = 0,
   NO_YUV = 1,
   BAD_FORMAT = 2
}

// Flags for clutter_texture_set_from_rgb_data() and
// clutter_texture_set_from_yuv_data().
enum TextureFlags /* Version 0.4 */ {
   NONE = 0,
   RGB_FLAG_BGR = 2,
   RGB_FLAG_PREMULT = 4,
   YUV_FLAG_YUV2 = 8
}
struct TexturePrivate {
}

// Enumaration controlling the texture quality.
enum TextureQuality /* Version 0.8 */ {
   LOW = 0,
   MEDIUM = 1,
   HIGH = 2
}
enum int Thai_baht = 3551;
enum int Thai_bobaimai = 3514;
enum int Thai_chochan = 3496;
enum int Thai_chochang = 3498;
enum int Thai_choching = 3497;
enum int Thai_chochoe = 3500;
enum int Thai_dochada = 3502;
enum int Thai_dodek = 3508;
enum int Thai_fofa = 3517;
enum int Thai_fofan = 3519;
enum int Thai_hohip = 3531;
enum int Thai_honokhuk = 3534;
enum int Thai_khokhai = 3490;
enum int Thai_khokhon = 3493;
enum int Thai_khokhuat = 3491;
enum int Thai_khokhwai = 3492;
enum int Thai_khorakhang = 3494;
enum int Thai_kokai = 3489;
enum int Thai_lakkhangyao = 3557;
enum int Thai_lekchet = 3575;
enum int Thai_lekha = 3573;
enum int Thai_lekhok = 3574;
enum int Thai_lekkao = 3577;
enum int Thai_leknung = 3569;
enum int Thai_lekpaet = 3576;
enum int Thai_leksam = 3571;
enum int Thai_leksi = 3572;
enum int Thai_leksong = 3570;
enum int Thai_leksun = 3568;
enum int Thai_lochula = 3532;
enum int Thai_loling = 3525;
enum int Thai_lu = 3526;
enum int Thai_maichattawa = 3563;
enum int Thai_maiek = 3560;
enum int Thai_maihanakat = 3537;
enum int Thai_maihanakat_maitho = 3550;
enum int Thai_maitaikhu = 3559;
enum int Thai_maitho = 3561;
enum int Thai_maitri = 3562;
enum int Thai_maiyamok = 3558;
enum int Thai_moma = 3521;
enum int Thai_ngongu = 3495;
enum int Thai_nikhahit = 3565;
enum int Thai_nonen = 3507;
enum int Thai_nonu = 3513;
enum int Thai_oang = 3533;
enum int Thai_paiyannoi = 3535;
enum int Thai_phinthu = 3546;
enum int Thai_phophan = 3518;
enum int Thai_phophung = 3516;
enum int Thai_phosamphao = 3520;
enum int Thai_popla = 3515;
enum int Thai_rorua = 3523;
enum int Thai_ru = 3524;
enum int Thai_saraa = 3536;
enum int Thai_saraaa = 3538;
enum int Thai_saraae = 3553;
enum int Thai_saraaimaimalai = 3556;
enum int Thai_saraaimaimuan = 3555;
enum int Thai_saraam = 3539;
enum int Thai_sarae = 3552;
enum int Thai_sarai = 3540;
enum int Thai_saraii = 3541;
enum int Thai_sarao = 3554;
enum int Thai_sarau = 3544;
enum int Thai_saraue = 3542;
enum int Thai_sarauee = 3543;
enum int Thai_sarauu = 3545;
enum int Thai_sorusi = 3529;
enum int Thai_sosala = 3528;
enum int Thai_soso = 3499;
enum int Thai_sosua = 3530;
enum int Thai_thanthakhat = 3564;
enum int Thai_thonangmontho = 3505;
enum int Thai_thophuthao = 3506;
enum int Thai_thothahan = 3511;
enum int Thai_thothan = 3504;
enum int Thai_thothong = 3512;
enum int Thai_thothung = 3510;
enum int Thai_topatak = 3503;
enum int Thai_totao = 3509;
enum int Thai_wowaen = 3527;
enum int Thai_yoyak = 3522;
enum int Thai_yoying = 3501;
enum int Thorn = 222;
enum int Time = 269025183;

// The #ClutterTimeline structure contains only private data
// and should be accessed using the provided API
struct Timeline /* : GObject.Object */ /* Version 0.2 */ {
   mixin Scriptable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   private TimelinePrivate* priv;


   // VERSION: 0.6
   // Creates a new #ClutterTimeline with a duration of @msecs.
   // 
   // g_object_unref() when done using it
   // RETURNS: the newly created #ClutterTimeline instance. Use
   // <msecs>: Duration of the timeline in milliseconds
   static Timeline* /*new*/ new_()(uint msecs) nothrow {
      return clutter_timeline_new(msecs);
   }
   static auto opCall()(uint msecs) {
      return clutter_timeline_new(msecs);
   }

   // VERSION: 0.8
   // Adds a named marker that will be hit when the timeline has been
   // running for @msecs milliseconds. Markers are unique string
   // identifiers for a given time. Once @timeline reaches
   // @msecs, it will emit a ::marker-reached signal for each marker
   // attached to that time.
   // 
   // A marker can be removed with clutter_timeline_remove_marker(). The
   // timeline can be advanced to a marker using
   // clutter_timeline_advance_to_marker().
   // <marker_name>: the unique name for this marker
   // <msecs>: position of the marker in milliseconds
   void add_marker_at_time(AT0)(AT0 /*char*/ marker_name, uint msecs) nothrow {
      clutter_timeline_add_marker_at_time(&this, toCString!(char*)(marker_name), msecs);
   }

   // Advance timeline to the requested point. The point is given as a
   // time in milliseconds since the timeline started.
   // 
   // <note><para>The @timeline will not emit the #ClutterTimeline::new-frame
   // signal for the given time. The first ::new-frame signal after the call to
   // clutter_timeline_advance() will be emit the skipped markers.
   // </para></note>
   // <msecs>: Time to advance to
   void advance()(uint msecs) nothrow {
      clutter_timeline_advance(&this, msecs);
   }

   // VERSION: 0.8
   // Advances @timeline to the time of the given @marker_name.
   // 
   // <note><para>Like clutter_timeline_advance(), this function will not
   // emit the #ClutterTimeline::new-frame for the time where @marker_name
   // is set, nor it will emit #ClutterTimeline::marker-reached for
   // @marker_name.</para></note>
   // <marker_name>: the name of the marker
   void advance_to_marker(AT0)(AT0 /*char*/ marker_name) nothrow {
      clutter_timeline_advance_to_marker(&this, toCString!(char*)(marker_name));
   }

   // VERSION: 0.4
   // Create a new #ClutterTimeline instance which has property values
   // matching that of supplied timeline. The cloned timeline will not
   // be started and will not be positioned to the current position of
   // 
   // from @timeline
   // RETURNS: a new #ClutterTimeline, cloned
   Timeline* /*new*/ clone()() nothrow {
      return clutter_timeline_clone(&this);
   }

   // VERSION: 1.6
   // Retrieves the value set by clutter_timeline_set_auto_reverse().
   // 
   // %FALSE otherwise
   // RETURNS: %TRUE if the timeline should automatically reverse, and
   int get_auto_reverse()() nothrow {
      return clutter_timeline_get_auto_reverse(&this);
   }

   // VERSION: 0.4
   // Retrieves the delay set using clutter_timeline_set_delay().
   // RETURNS: the delay in milliseconds.
   uint get_delay()() nothrow {
      return clutter_timeline_get_delay(&this);
   }

   // VERSION: 0.6
   // Retrieves the amount of time elapsed since the last
   // ClutterTimeline::new-frame signal.
   // 
   // This function is only useful inside handlers for the ::new-frame
   // signal, and its behaviour is undefined if the timeline is not
   // playing.
   // 
   // last frame
   // RETURNS: the amount of time in milliseconds elapsed since the
   uint get_delta()() nothrow {
      return clutter_timeline_get_delta(&this);
   }

   // VERSION: 0.6
   // Retrieves the direction of the timeline set with
   // clutter_timeline_set_direction().
   // RETURNS: the direction of the timeline
   TimelineDirection get_direction()() nothrow {
      return clutter_timeline_get_direction(&this);
   }

   // VERSION: 0.6
   // Retrieves the duration of a #ClutterTimeline in milliseconds.
   // See clutter_timeline_set_duration().
   // RETURNS: the duration of the timeline, in milliseconds.
   uint get_duration()() nothrow {
      return clutter_timeline_get_duration(&this);
   }

   // Request the current time position of the timeline.
   // RETURNS: current elapsed time in milliseconds.
   uint get_elapsed_time()() nothrow {
      return clutter_timeline_get_elapsed_time(&this);
   }

   // Gets whether @timeline is looping
   // RETURNS: %TRUE if the timeline is looping
   int get_loop()() nothrow {
      return clutter_timeline_get_loop(&this);
   }

   // VERSION: 0.6
   // The position of the timeline in a [0, 1] interval.
   // RETURNS: the position of the timeline.
   double get_progress()() nothrow {
      return clutter_timeline_get_progress(&this);
   }

   // VERSION: 0.8
   // Checks whether @timeline has a marker set with the given name.
   // RETURNS: %TRUE if the marker was found
   // <marker_name>: the name of the marker
   int has_marker(AT0)(AT0 /*char*/ marker_name) nothrow {
      return clutter_timeline_has_marker(&this, toCString!(char*)(marker_name));
   }

   // Queries state of a #ClutterTimeline.
   // RETURNS: %TRUE if timeline is currently playing
   int is_playing()() nothrow {
      return clutter_timeline_is_playing(&this);
   }

   // VERSION: 0.8
   // Retrieves the list of markers at time @msecs. If @msecs is a
   // negative integer, all the markers attached to @timeline will be
   // returned.
   // 
   // a newly allocated, %NULL terminated string array containing the names
   // of the markers. Use g_strfreev() when done.
   // <msecs>: the time to check, or -1
   // <n_markers>: the number of markers returned
   char** /*new*/ list_markers(AT0)(int msecs, /*out*/ AT0 /*size_t*/ n_markers) nothrow {
      return clutter_timeline_list_markers(&this, msecs, UpCast!(size_t*)(n_markers));
   }
   // Pauses the #ClutterTimeline on current frame
   void pause()() nothrow {
      clutter_timeline_pause(&this);
   }

   // VERSION: 0.8
   // Removes @marker_name, if found, from @timeline.
   // <marker_name>: the name of the marker to remove
   void remove_marker(AT0)(AT0 /*char*/ marker_name) nothrow {
      clutter_timeline_remove_marker(&this, toCString!(char*)(marker_name));
   }

   // Rewinds #ClutterTimeline to the first frame if its direction is
   // %CLUTTER_TIMELINE_FORWARD and the last frame if it is
   // %CLUTTER_TIMELINE_BACKWARD.
   void rewind()() nothrow {
      clutter_timeline_rewind(&this);
   }

   // VERSION: 1.6
   // Sets whether @timeline should reverse the direction after the
   // emission of the #ClutterTimeline::completed signal.
   // 
   // Setting the #ClutterTimeline:auto-reverse property to %TRUE is the
   // equivalent of connecting a callback to the #ClutterTimeline::completed
   // signal and changing the direction of the timeline from that callback;
   // for instance, this code:
   // 
   // |[
   // static void
   // reverse_timeline (ClutterTimeline *timeline)
   // {
   // ClutterTimelineDirection dir = clutter_timeline_get_direction (timeline);
   // 
   // if (dir == CLUTTER_TIMELINE_FORWARD)
   // dir = CLUTTER_TIMELINE_BACKWARD;
   // else
   // dir = CLUTTER_TIMELINE_FORWARD;
   // 
   // clutter_timeline_set_direction (timeline, dir);
   // }
   // ...
   // timeline = clutter_timeline_new (1000);
   // clutter_timeline_set_loop (timeline);
   // g_signal_connect (timeline, "completed",
   // G_CALLBACK (reverse_timeline),
   // NULL);
   // ]|
   // 
   // can be effectively replaced by:
   // 
   // |[
   // timeline = clutter_timeline_new (1000);
   // clutter_timeline_set_loop (timeline);
   // clutter_timeline_set_auto_reverse (timeline);
   // ]|
   // <reverse>: %TRUE if the @timeline should reverse the direction
   void set_auto_reverse()(int reverse) nothrow {
      clutter_timeline_set_auto_reverse(&this, reverse);
   }

   // VERSION: 0.4
   // Sets the delay, in milliseconds, before @timeline should start.
   // <msecs>: delay in milliseconds
   void set_delay()(uint msecs) nothrow {
      clutter_timeline_set_delay(&this, msecs);
   }

   // VERSION: 0.6
   // Sets the direction of @timeline, either %CLUTTER_TIMELINE_FORWARD or
   // %CLUTTER_TIMELINE_BACKWARD.
   // <direction>: the direction of the timeline
   void set_direction()(TimelineDirection direction) nothrow {
      clutter_timeline_set_direction(&this, direction);
   }

   // VERSION: 0.6
   // Sets the duration of the timeline, in milliseconds. The speed
   // of the timeline depends on the ClutterTimeline:fps setting.
   // <msecs>: duration of the timeline in milliseconds
   void set_duration()(uint msecs) nothrow {
      clutter_timeline_set_duration(&this, msecs);
   }

   // Sets whether @timeline should loop.
   // <loop>: %TRUE for enable looping
   void set_loop()(int loop) nothrow {
      clutter_timeline_set_loop(&this, loop);
   }

   // Advance timeline by the requested time in milliseconds
   // <msecs>: Amount of time to skip
   void skip()(uint msecs) nothrow {
      clutter_timeline_skip(&this, msecs);
   }
   // Starts the #ClutterTimeline playing.
   void start()() nothrow {
      clutter_timeline_start(&this);
   }
   // Stops the #ClutterTimeline and moves to frame 0
   void stop()() nothrow {
      clutter_timeline_stop(&this);
   }

   // The ::completed signal is emitted when the timeline reaches the
   // number of frames specified by the ClutterTimeline:num-frames property.
   extern (C) alias static void function (Timeline* this_, void* user_data=null) nothrow signal_completed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"completed", CB/*:signal_completed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_completed)||_ttmm!(CB, signal_completed)()) {
      return signal_connect_data!()(&this, cast(char*)"completed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.8
   // The ::marker-reached signal is emitted each time a timeline
   // reaches a marker set with
   // clutter_timeline_add_marker_at_time(). This signal is detailed
   // with the name of the marker as well, so it is possible to connect
   // a callback to the ::marker-reached signal for a specific marker
   // with:
   // 
   // <informalexample><programlisting>
   // clutter_timeline_add_marker_at_time (timeline, "foo", 500);
   // clutter_timeline_add_marker_at_time (timeline, "bar", 750);
   // 
   // g_signal_connect (timeline, "marker-reached",
   // G_CALLBACK (each_marker_reached), NULL);
   // g_signal_connect (timeline, "marker-reached::foo",
   // G_CALLBACK (foo_marker_reached), NULL);
   // g_signal_connect (timeline, "marker-reached::bar",
   // G_CALLBACK (bar_marker_reached), NULL);
   // </programlisting></informalexample>
   // 
   // In the example, the first callback will be invoked for both
   // the "foo" and "bar" marker, while the second and third callbacks
   // will be invoked for the "foo" or "bar" markers, respectively.
   // <marker_name>: the name of the marker reached
   // <msecs>: the elapsed time
   extern (C) alias static void function (Timeline* this_, char* marker_name, int msecs, void* user_data=null) nothrow signal_marker_reached;
   ulong signal_connect(string name:"marker-reached", CB/*:signal_marker_reached*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_marker_reached)||_ttmm!(CB, signal_marker_reached)()) {
      return signal_connect_data!()(&this, cast(char*)"marker-reached",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // The ::new-frame signal is emitted for each timeline running
   // timeline before a new frame is drawn to give animations a chance
   // to update the scene.
   // <msecs>: the elapsed time between 0 and duration
   extern (C) alias static void function (Timeline* this_, int msecs, void* user_data=null) nothrow signal_new_frame;
   ulong signal_connect(string name:"new-frame", CB/*:signal_new_frame*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_new_frame)||_ttmm!(CB, signal_new_frame)()) {
      return signal_connect_data!()(&this, cast(char*)"new-frame",
      cast(GObject2.Callback)cb, data, null, cf);
   }
   // The ::paused signal is emitted when clutter_timeline_pause() is invoked.
   extern (C) alias static void function (Timeline* this_, void* user_data=null) nothrow signal_paused;
   ulong signal_connect(string name:"paused", CB/*:signal_paused*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_paused)||_ttmm!(CB, signal_paused)()) {
      return signal_connect_data!()(&this, cast(char*)"paused",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // The ::started signal is emitted when the timeline starts its run.
   // This might be as soon as clutter_timeline_start() is invoked or
   // after the delay set in the ClutterTimeline:delay property has
   // expired.
   extern (C) alias static void function (Timeline* this_, void* user_data=null) nothrow signal_started;
   ulong signal_connect(string name:"started", CB/*:signal_started*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_started)||_ttmm!(CB, signal_started)()) {
      return signal_connect_data!()(&this, cast(char*)"started",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// The #ClutterTimelineClass structure contains only private data
struct TimelineClass /* Version 0.2 */ {
   private GObject2.ObjectClass parent_class;
   extern (C) void function (Timeline* timeline) nothrow started;
   extern (C) void function (Timeline* timeline) nothrow completed;
   extern (C) void function (Timeline* timeline) nothrow paused;
   extern (C) void function (Timeline* timeline, int msecs) nothrow new_frame;
   extern (C) void function (Timeline* timeline, char* marker_name, int msecs) nothrow marker_reached;
   extern (C) void function () nothrow _clutter_timeline_1;
   extern (C) void function () nothrow _clutter_timeline_2;
   extern (C) void function () nothrow _clutter_timeline_3;
   extern (C) void function () nothrow _clutter_timeline_4;
   extern (C) void function () nothrow _clutter_timeline_5;
}

// The direction of a #ClutterTimeline
enum TimelineDirection /* Version 0.6 */ {
   FORWARD = 0,
   BACKWARD = 1
}
struct TimelinePrivate {
}


// <structname>ClutterTimeoutPool</structname> is an opaque structure
// whose members cannot be directly accessed.
struct TimeoutPool /* Version 0.6 */ {

   // VERSION: 0.4
   // DEPRECATED method: add - 1.6
   // Sets a function to be called at regular intervals, and puts it inside
   // the @pool. The function is repeatedly called until it returns %FALSE,
   // at which point the timeout is automatically destroyed and the function
   // won't be called again. If @notify is not %NULL, the @notify function
   // will be called. The first call to @func will be at the end of @interval.
   // 
   // Since Clutter 0.8 this will try to compensate for delays. For
   // example, if @func takes half the interval time to execute then the
   // function will be called again half the interval time after it
   // finished. Before version 0.8 it would not fire until a full
   // interval after the function completes so the delay between calls
   // would be @interval * 1.5. This function does not however try to
   // invoke the function multiple times to catch up missing frames if
   // @func takes more than @interval ms to execute.
   // 
   // Use clutter_timeout_pool_remove() to stop the timeout.
   // RETURNS: the ID (greater than 0) of the timeout inside the pool.
   // <fps>: the time between calls to the function, in frames per second
   // <func>: function to call
   // <data>: data to pass to the function, or %NULL
   // <notify>: function to call when the timeout is removed, or %NULL
   uint add(AT0)(uint fps, GLib2.SourceFunc func, AT0 /*void*/ data, GLib2.DestroyNotify notify) nothrow {
      return clutter_timeout_pool_add(&this, fps, func, UpCast!(void*)(data), notify);
   }

   // VERSION: 0.4
   // DEPRECATED method: remove - 1.6
   // Removes a timeout function with @id_ from the timeout pool. The id
   // is the same returned when adding a function to the timeout pool with
   // clutter_timeout_pool_add().
   // <id_>: the id of the timeout to remove
   void remove()(uint id_) nothrow {
      clutter_timeout_pool_remove(&this, id_);
   }

   // Unintrospectable function: new() / clutter_timeout_pool_new()
   // VERSION: 0.4
   // DEPRECATED function: new - 1.6
   // Creates a new timeout pool source. A timeout pool should be used when
   // multiple timeout functions, running at the same priority, are needed and
   // the g_timeout_add() API might lead to starvation of the time slice of
   // the main loop. A timeout pool allocates a single time slice of the main
   // loop and runs every timeout function inside it. The timeout pool is
   // always sorted, so that the extraction of the next timeout function is
   // a constant time operation.
   // 
   // is owned by the GLib default context and will be automatically
   // destroyed when the context is destroyed. It is possible to force
   // the destruction of the timeout pool using g_source_destroy()
   // RETURNS: the newly created #ClutterTimeoutPool. The created pool
   // <priority>: the priority of the timeout pool. Typically this will be #G_PRIORITY_DEFAULT
   static TimeoutPool* new_()(int priority) nothrow {
      return clutter_timeout_pool_new(priority);
   }
}

enum int ToDoList = 269025055;
enum int Tools = 269025153;
enum int TopMenu = 269025186;
enum int TouchpadOff = 269025201;
enum int TouchpadOn = 269025200;
enum int TouchpadToggle = 269025193;
enum int Touroku = 65323;
enum int Travel = 269025154;
enum int Tslash = 940;
enum int U = 85;
enum int UWB = 269025174;
enum int Uacute = 218;
enum int Ubelowdot = 16785124;
enum int Ubreve = 733;
enum int Ucircumflex = 219;
enum int Udiaeresis = 220;
enum int Udoubleacute = 475;
enum int Ugrave = 217;
enum int Uhook = 16785126;
enum int Uhorn = 16777647;
enum int Uhornacute = 16785128;
enum int Uhornbelowdot = 16785136;
enum int Uhorngrave = 16785130;
enum int Uhornhook = 16785132;
enum int Uhorntilde = 16785134;
enum int Ukrainian_GHE_WITH_UPTURN = 1725;
enum int Ukrainian_I = 1718;
enum int Ukrainian_IE = 1716;
enum int Ukrainian_YI = 1719;
enum int Ukrainian_ghe_with_upturn = 1709;
enum int Ukrainian_i = 1702;
enum int Ukrainian_ie = 1700;
enum int Ukrainian_yi = 1703;
enum int Ukranian_I = 1718;
enum int Ukranian_JE = 1716;
enum int Ukranian_YI = 1719;
enum int Ukranian_i = 1702;
enum int Ukranian_je = 1700;
enum int Ukranian_yi = 1703;
enum int Umacron = 990;
enum int Undo = 65381;
enum int Ungrab = 269024800;

// The type of unit in which a value is expressed
// 
// This enumeration might be expanded at later date
enum UnitType /* Version 1.0 */ {
   PIXEL = 0,
   EM = 1,
   MM = 2,
   POINT = 3,
   CM = 4
}

// An opaque structure, to be used to store sizing and positioning
// values along with their unit.
struct Units /* Version 1.0 */ {
   private UnitType unit_type;
   private float value, pixels;
   private uint pixels_set;
   private int serial, __padding_1;
   private long __padding_2;


   // VERSION: 1.0
   // Copies @units
   // 
   // #ClutterUnits structure. Use clutter_units_free() to free
   // the allocated resources
   // RETURNS: the newly created copy of a
   Units* /*new*/ copy()() nothrow {
      return clutter_units_copy(&this);
   }

   // VERSION: 1.0
   // Frees the resources allocated by @units
   // 
   // You should only call this function on a #ClutterUnits
   // created using clutter_units_copy()
   void free()() nothrow {
      clutter_units_free(&this);
   }

   // VERSION: 1.2
   // Stores a value in centimeters inside @units
   // <cm>: centimeters
   void from_cm()(float cm) nothrow {
      clutter_units_from_cm(&this, cm);
   }

   // VERSION: 1.0
   // Stores a value in em inside @units, using the default font
   // name as returned by clutter_backend_get_font_name()
   // <em>: em
   void from_em()(float em) nothrow {
      clutter_units_from_em(&this, em);
   }

   // VERSION: 1.0
   // Stores a value in em inside @units using @font_name
   // <font_name>: the font name and size
   // <em>: em
   void from_em_for_font(AT0)(AT0 /*char*/ font_name, float em) nothrow {
      clutter_units_from_em_for_font(&this, toCString!(char*)(font_name), em);
   }

   // VERSION: 1.0
   // Stores a value in millimiters inside @units
   // <mm>: millimeters
   void from_mm()(float mm) nothrow {
      clutter_units_from_mm(&this, mm);
   }

   // VERSION: 1.0
   // Stores a value in pixels inside @units
   // <px>: pixels
   void from_pixels()(int px) nothrow {
      clutter_units_from_pixels(&this, px);
   }

   // VERSION: 1.0
   // Stores a value in typographic points inside @units
   // <pt>: typographic points
   void from_pt()(float pt) nothrow {
      clutter_units_from_pt(&this, pt);
   }

   // VERSION: 1.0
   // Parses a value and updates @units with it
   // 
   // A #ClutterUnits expressed in string should match:
   // 
   // |[
   // units: wsp* unit-value wsp* unit-name? wsp*
   // unit-value: number
   // unit-name: 'px' | 'pt' | 'mm' | 'em' | 'cm'
   // number: digit+
   // | digit* sep digit+
   // sep: '.' | ','
   // digit: '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
   // wsp: (#0x20 | #0x9 | #0xA | #0xB | #0xC | #0xD)+
   // ]|
   // 
   // For instance, these are valid strings:
   // 
   // |[
   // 10 px
   // 5.1 em
   // 24 pt
   // 12.6 mm
   // .3 cm
   // ]|
   // 
   // While these are not:
   // 
   // |[
   // 42 cats
   // omg!1!ponies
   // ]|
   // 
   // <note><para>If no unit is specified, pixels are assumed.</para></note>
   // 
   // and %FALSE otherwise
   // RETURNS: %TRUE if the string was successfully parsed,
   // <str>: the string to convert
   int from_string(AT0)(AT0 /*char*/ str) nothrow {
      return clutter_units_from_string(&this, toCString!(char*)(str));
   }

   // VERSION: 1.0
   // Retrieves the unit type of the value stored inside @units
   // RETURNS: a unit type
   UnitType get_unit_type()() nothrow {
      return clutter_units_get_unit_type(&this);
   }

   // VERSION: 1.0
   // Retrieves the value stored inside @units
   // RETURNS: the value stored inside a #ClutterUnits
   float get_unit_value()() nothrow {
      return clutter_units_get_unit_value(&this);
   }

   // VERSION: 1.0
   // Converts a value in #ClutterUnits to pixels
   // RETURNS: the value in pixels
   float to_pixels()() nothrow {
      return clutter_units_to_pixels(&this);
   }

   // VERSION: 1.0
   // Converts @units into a string
   // 
   // See clutter_units_from_string() for the units syntax and for
   // examples of output
   // 
   // <note>Fractional values are truncated to the second decimal
   // position for em, mm and cm, and to the first decimal position for
   // typographic points. Pixels are integers.</note>
   // 
   // #ClutterUnits value. Use g_free() to free the string
   // RETURNS: a newly allocated string containing the encoded
   char* /*new*/ to_string()() nothrow {
      return clutter_units_to_string(&this);
   }
}

enum int Uogonek = 985;
enum int Up = 65362;
enum int Uring = 473;
enum int User1KB = 269025157;
enum int User2KB = 269025158;
enum int UserPB = 269025156;
enum int Utilde = 989;
enum int V = 86;
enum double VERSION = 1.900000;
enum int VERSION_HEX = 0;
enum VERSION_S = "1.9.11";
enum int VendorHome = 269025076;
// Vertex of an actor in 3D space, expressed in pixels
struct Vertex /* Version 0.4 */ {
   float x, y, z;


   // VERSION: 1.0
   // Creates a new #ClutterVertex for the point in 3D space
   // identified by the 3 coordinates @x, @y, @z
   // 
   // clutter_vertex_free() to free the resources
   // RETURNS: the newly allocate #ClutterVertex. Use
   // <x>: X coordinate
   // <y>: Y coordinate
   // <z>: Z coordinate
   static Vertex* /*new*/ new_()(float x, float y, float z) nothrow {
      return clutter_vertex_new(x, y, z);
   }
   static auto opCall()(float x, float y, float z) {
      return clutter_vertex_new(x, y, z);
   }

   // VERSION: 1.0
   // Copies @vertex
   // 
   // clutter_vertex_free() to free the allocated resources
   // RETURNS: a newly allocated copy of #ClutterVertex. Use
   Vertex* /*new*/ copy()() nothrow {
      return clutter_vertex_copy(&this);
   }

   // VERSION: 1.0
   // Compares @vertex_a and @vertex_b for equality
   // RETURNS: %TRUE if the passed #ClutterVertex are equal
   // <vertex_b>: a #ClutterVertex
   int equal(AT0)(AT0 /*Vertex*/ vertex_b) nothrow {
      return clutter_vertex_equal(&this, UpCast!(Vertex*)(vertex_b));
   }

   // VERSION: 1.0
   // Frees a #ClutterVertex allocated using clutter_vertex_copy()
   void free()() nothrow {
      clutter_vertex_free(&this);
   }
}

enum int Video = 269025159;
enum int View = 269025185;
enum int VoidSymbol = 16777215;
enum int W = 87;
enum WINDOWING_GLX = "glx";
enum WINDOWING_X11 = "x11";
enum int WLAN = 269025173;
enum int WWW = 269025070;
enum int Wacute = 16785026;
enum int WakeUp = 269025067;
enum int Wcircumflex = 16777588;
enum int Wdiaeresis = 16785028;
enum int WebCam = 269025167;
enum int Wgrave = 16785024;
enum int WheelButton = 269025160;
enum int WindowClear = 269025109;
enum int WonSign = 16785577;
enum int Word = 269025161;
enum int X = 88;
enum X11FilterReturn {
   CONTINUE = 0,
   TRANSLATE = 1,
   REMOVE = 2
}
enum X11XInputEventTypes {
   KEY_PRESS_EVENT = 0,
   KEY_RELEASE_EVENT = 1,
   BUTTON_PRESS_EVENT = 2,
   BUTTON_RELEASE_EVENT = 3,
   MOTION_NOTIFY_EVENT = 4,
   LAST_EVENT = 5
}
enum int Xabovedot = 16785034;
enum int Xfer = 269025162;
enum int Y = 89;
enum int Yacute = 221;
enum int Ybelowdot = 16785140;
enum int Ycircumflex = 16777590;
enum int Ydiaeresis = 5054;
enum int Yellow = 269025189;
enum int Ygrave = 16785138;
enum int Yhook = 16785142;
enum int Ytilde = 16785144;
enum int Z = 90;
enum int Zabovedot = 431;
enum int Zacute = 428;
enum int Zcaron = 430;
enum int Zen_Koho = 65341;
enum int Zenkaku = 65320;
enum int Zenkaku_Hankaku = 65322;
enum int ZoomIn = 269025163;
enum int ZoomOut = 269025164;
enum int Zstroke = 16777653;
enum int a = 97;
enum int aacute = 225;
enum int abelowdot = 16785057;
enum int abovedot = 511;
enum int abreve = 483;
enum int abreveacute = 16785071;
enum int abrevebelowdot = 16785079;
enum int abrevegrave = 16785073;
enum int abrevehook = 16785075;
enum int abrevetilde = 16785077;
enum int acircumflex = 226;
enum int acircumflexacute = 16785061;
enum int acircumflexbelowdot = 16785069;
enum int acircumflexgrave = 16785063;
enum int acircumflexhook = 16785065;
enum int acircumflextilde = 16785067;
enum int acute = 180;
enum int adiaeresis = 228;
enum int ae = 230;
enum int agrave = 224;
enum int ahook = 16785059;
enum int amacron = 992;
enum int ampersand = 38;
enum int aogonek = 433;
enum int apostrophe = 39;
enum int approxeq = 16785992;
enum int approximate = 2248;
enum int aring = 229;
enum int asciicircum = 94;
enum int asciitilde = 126;
enum int asterisk = 42;
enum int at = 64;
enum int atilde = 227;
enum int b = 98;
enum int babovedot = 16784899;
enum int backslash = 92;
enum int ballotcross = 2804;
enum int bar = 124;
static void base_init()() nothrow {
   clutter_base_init();
}

enum int because = 16785973;
enum int blank = 2527;
enum int botintegral = 2213;
enum int botleftparens = 2220;
enum int botleftsqbracket = 2216;
enum int botleftsummation = 2226;
enum int botrightparens = 2222;
enum int botrightsqbracket = 2218;
enum int botrightsummation = 2230;
enum int bott = 2550;
enum int botvertsummationconnector = 2228;
enum int braceleft = 123;
enum int braceright = 125;
enum int bracketleft = 91;
enum int bracketright = 93;
enum int braille_blank = 16787456;
enum int braille_dot_1 = 65521;
enum int braille_dot_10 = 65530;
enum int braille_dot_2 = 65522;
enum int braille_dot_3 = 65523;
enum int braille_dot_4 = 65524;
enum int braille_dot_5 = 65525;
enum int braille_dot_6 = 65526;
enum int braille_dot_7 = 65527;
enum int braille_dot_8 = 65528;
enum int braille_dot_9 = 65529;
enum int braille_dots_1 = 16787457;
enum int braille_dots_12 = 16787459;
enum int braille_dots_123 = 16787463;
enum int braille_dots_1234 = 16787471;
enum int braille_dots_12345 = 16787487;
enum int braille_dots_123456 = 16787519;
enum int braille_dots_1234567 = 16787583;
enum int braille_dots_12345678 = 16787711;
enum int braille_dots_1234568 = 16787647;
enum int braille_dots_123457 = 16787551;
enum int braille_dots_1234578 = 16787679;
enum int braille_dots_123458 = 16787615;
enum int braille_dots_12346 = 16787503;
enum int braille_dots_123467 = 16787567;
enum int braille_dots_1234678 = 16787695;
enum int braille_dots_123468 = 16787631;
enum int braille_dots_12347 = 16787535;
enum int braille_dots_123478 = 16787663;
enum int braille_dots_12348 = 16787599;
enum int braille_dots_1235 = 16787479;
enum int braille_dots_12356 = 16787511;
enum int braille_dots_123567 = 16787575;
enum int braille_dots_1235678 = 16787703;
enum int braille_dots_123568 = 16787639;
enum int braille_dots_12357 = 16787543;
enum int braille_dots_123578 = 16787671;
enum int braille_dots_12358 = 16787607;
enum int braille_dots_1236 = 16787495;
enum int braille_dots_12367 = 16787559;
enum int braille_dots_123678 = 16787687;
enum int braille_dots_12368 = 16787623;
enum int braille_dots_1237 = 16787527;
enum int braille_dots_12378 = 16787655;
enum int braille_dots_1238 = 16787591;
enum int braille_dots_124 = 16787467;
enum int braille_dots_1245 = 16787483;
enum int braille_dots_12456 = 16787515;
enum int braille_dots_124567 = 16787579;
enum int braille_dots_1245678 = 16787707;
enum int braille_dots_124568 = 16787643;
enum int braille_dots_12457 = 16787547;
enum int braille_dots_124578 = 16787675;
enum int braille_dots_12458 = 16787611;
enum int braille_dots_1246 = 16787499;
enum int braille_dots_12467 = 16787563;
enum int braille_dots_124678 = 16787691;
enum int braille_dots_12468 = 16787627;
enum int braille_dots_1247 = 16787531;
enum int braille_dots_12478 = 16787659;
enum int braille_dots_1248 = 16787595;
enum int braille_dots_125 = 16787475;
enum int braille_dots_1256 = 16787507;
enum int braille_dots_12567 = 16787571;
enum int braille_dots_125678 = 16787699;
enum int braille_dots_12568 = 16787635;
enum int braille_dots_1257 = 16787539;
enum int braille_dots_12578 = 16787667;
enum int braille_dots_1258 = 16787603;
enum int braille_dots_126 = 16787491;
enum int braille_dots_1267 = 16787555;
enum int braille_dots_12678 = 16787683;
enum int braille_dots_1268 = 16787619;
enum int braille_dots_127 = 16787523;
enum int braille_dots_1278 = 16787651;
enum int braille_dots_128 = 16787587;
enum int braille_dots_13 = 16787461;
enum int braille_dots_134 = 16787469;
enum int braille_dots_1345 = 16787485;
enum int braille_dots_13456 = 16787517;
enum int braille_dots_134567 = 16787581;
enum int braille_dots_1345678 = 16787709;
enum int braille_dots_134568 = 16787645;
enum int braille_dots_13457 = 16787549;
enum int braille_dots_134578 = 16787677;
enum int braille_dots_13458 = 16787613;
enum int braille_dots_1346 = 16787501;
enum int braille_dots_13467 = 16787565;
enum int braille_dots_134678 = 16787693;
enum int braille_dots_13468 = 16787629;
enum int braille_dots_1347 = 16787533;
enum int braille_dots_13478 = 16787661;
enum int braille_dots_1348 = 16787597;
enum int braille_dots_135 = 16787477;
enum int braille_dots_1356 = 16787509;
enum int braille_dots_13567 = 16787573;
enum int braille_dots_135678 = 16787701;
enum int braille_dots_13568 = 16787637;
enum int braille_dots_1357 = 16787541;
enum int braille_dots_13578 = 16787669;
enum int braille_dots_1358 = 16787605;
enum int braille_dots_136 = 16787493;
enum int braille_dots_1367 = 16787557;
enum int braille_dots_13678 = 16787685;
enum int braille_dots_1368 = 16787621;
enum int braille_dots_137 = 16787525;
enum int braille_dots_1378 = 16787653;
enum int braille_dots_138 = 16787589;
enum int braille_dots_14 = 16787465;
enum int braille_dots_145 = 16787481;
enum int braille_dots_1456 = 16787513;
enum int braille_dots_14567 = 16787577;
enum int braille_dots_145678 = 16787705;
enum int braille_dots_14568 = 16787641;
enum int braille_dots_1457 = 16787545;
enum int braille_dots_14578 = 16787673;
enum int braille_dots_1458 = 16787609;
enum int braille_dots_146 = 16787497;
enum int braille_dots_1467 = 16787561;
enum int braille_dots_14678 = 16787689;
enum int braille_dots_1468 = 16787625;
enum int braille_dots_147 = 16787529;
enum int braille_dots_1478 = 16787657;
enum int braille_dots_148 = 16787593;
enum int braille_dots_15 = 16787473;
enum int braille_dots_156 = 16787505;
enum int braille_dots_1567 = 16787569;
enum int braille_dots_15678 = 16787697;
enum int braille_dots_1568 = 16787633;
enum int braille_dots_157 = 16787537;
enum int braille_dots_1578 = 16787665;
enum int braille_dots_158 = 16787601;
enum int braille_dots_16 = 16787489;
enum int braille_dots_167 = 16787553;
enum int braille_dots_1678 = 16787681;
enum int braille_dots_168 = 16787617;
enum int braille_dots_17 = 16787521;
enum int braille_dots_178 = 16787649;
enum int braille_dots_18 = 16787585;
enum int braille_dots_2 = 16787458;
enum int braille_dots_23 = 16787462;
enum int braille_dots_234 = 16787470;
enum int braille_dots_2345 = 16787486;
enum int braille_dots_23456 = 16787518;
enum int braille_dots_234567 = 16787582;
enum int braille_dots_2345678 = 16787710;
enum int braille_dots_234568 = 16787646;
enum int braille_dots_23457 = 16787550;
enum int braille_dots_234578 = 16787678;
enum int braille_dots_23458 = 16787614;
enum int braille_dots_2346 = 16787502;
enum int braille_dots_23467 = 16787566;
enum int braille_dots_234678 = 16787694;
enum int braille_dots_23468 = 16787630;
enum int braille_dots_2347 = 16787534;
enum int braille_dots_23478 = 16787662;
enum int braille_dots_2348 = 16787598;
enum int braille_dots_235 = 16787478;
enum int braille_dots_2356 = 16787510;
enum int braille_dots_23567 = 16787574;
enum int braille_dots_235678 = 16787702;
enum int braille_dots_23568 = 16787638;
enum int braille_dots_2357 = 16787542;
enum int braille_dots_23578 = 16787670;
enum int braille_dots_2358 = 16787606;
enum int braille_dots_236 = 16787494;
enum int braille_dots_2367 = 16787558;
enum int braille_dots_23678 = 16787686;
enum int braille_dots_2368 = 16787622;
enum int braille_dots_237 = 16787526;
enum int braille_dots_2378 = 16787654;
enum int braille_dots_238 = 16787590;
enum int braille_dots_24 = 16787466;
enum int braille_dots_245 = 16787482;
enum int braille_dots_2456 = 16787514;
enum int braille_dots_24567 = 16787578;
enum int braille_dots_245678 = 16787706;
enum int braille_dots_24568 = 16787642;
enum int braille_dots_2457 = 16787546;
enum int braille_dots_24578 = 16787674;
enum int braille_dots_2458 = 16787610;
enum int braille_dots_246 = 16787498;
enum int braille_dots_2467 = 16787562;
enum int braille_dots_24678 = 16787690;
enum int braille_dots_2468 = 16787626;
enum int braille_dots_247 = 16787530;
enum int braille_dots_2478 = 16787658;
enum int braille_dots_248 = 16787594;
enum int braille_dots_25 = 16787474;
enum int braille_dots_256 = 16787506;
enum int braille_dots_2567 = 16787570;
enum int braille_dots_25678 = 16787698;
enum int braille_dots_2568 = 16787634;
enum int braille_dots_257 = 16787538;
enum int braille_dots_2578 = 16787666;
enum int braille_dots_258 = 16787602;
enum int braille_dots_26 = 16787490;
enum int braille_dots_267 = 16787554;
enum int braille_dots_2678 = 16787682;
enum int braille_dots_268 = 16787618;
enum int braille_dots_27 = 16787522;
enum int braille_dots_278 = 16787650;
enum int braille_dots_28 = 16787586;
enum int braille_dots_3 = 16787460;
enum int braille_dots_34 = 16787468;
enum int braille_dots_345 = 16787484;
enum int braille_dots_3456 = 16787516;
enum int braille_dots_34567 = 16787580;
enum int braille_dots_345678 = 16787708;
enum int braille_dots_34568 = 16787644;
enum int braille_dots_3457 = 16787548;
enum int braille_dots_34578 = 16787676;
enum int braille_dots_3458 = 16787612;
enum int braille_dots_346 = 16787500;
enum int braille_dots_3467 = 16787564;
enum int braille_dots_34678 = 16787692;
enum int braille_dots_3468 = 16787628;
enum int braille_dots_347 = 16787532;
enum int braille_dots_3478 = 16787660;
enum int braille_dots_348 = 16787596;
enum int braille_dots_35 = 16787476;
enum int braille_dots_356 = 16787508;
enum int braille_dots_3567 = 16787572;
enum int braille_dots_35678 = 16787700;
enum int braille_dots_3568 = 16787636;
enum int braille_dots_357 = 16787540;
enum int braille_dots_3578 = 16787668;
enum int braille_dots_358 = 16787604;
enum int braille_dots_36 = 16787492;
enum int braille_dots_367 = 16787556;
enum int braille_dots_3678 = 16787684;
enum int braille_dots_368 = 16787620;
enum int braille_dots_37 = 16787524;
enum int braille_dots_378 = 16787652;
enum int braille_dots_38 = 16787588;
enum int braille_dots_4 = 16787464;
enum int braille_dots_45 = 16787480;
enum int braille_dots_456 = 16787512;
enum int braille_dots_4567 = 16787576;
enum int braille_dots_45678 = 16787704;
enum int braille_dots_4568 = 16787640;
enum int braille_dots_457 = 16787544;
enum int braille_dots_4578 = 16787672;
enum int braille_dots_458 = 16787608;
enum int braille_dots_46 = 16787496;
enum int braille_dots_467 = 16787560;
enum int braille_dots_4678 = 16787688;
enum int braille_dots_468 = 16787624;
enum int braille_dots_47 = 16787528;
enum int braille_dots_478 = 16787656;
enum int braille_dots_48 = 16787592;
enum int braille_dots_5 = 16787472;
enum int braille_dots_56 = 16787504;
enum int braille_dots_567 = 16787568;
enum int braille_dots_5678 = 16787696;
enum int braille_dots_568 = 16787632;
enum int braille_dots_57 = 16787536;
enum int braille_dots_578 = 16787664;
enum int braille_dots_58 = 16787600;
enum int braille_dots_6 = 16787488;
enum int braille_dots_67 = 16787552;
enum int braille_dots_678 = 16787680;
enum int braille_dots_68 = 16787616;
enum int braille_dots_7 = 16787520;
enum int braille_dots_78 = 16787648;
enum int braille_dots_8 = 16787584;
enum int breve = 418;
enum int brokenbar = 166;
enum int c = 99;
enum int c_h = 65187;
enum int cabovedot = 741;
enum int cacute = 486;

// VERSION: 1.0
// Utility function for setting the source color of @cr using
// a #ClutterColor. This function is the equivalent of:
// 
// |[
// cairo_set_source_rgba (cr,
// color->red / 255.0,
// color->green / 255.0,
// color->blue / 255.0,
// color->alpha / 255.0);
// ]|
// <cr>: a Cairo context
// <color>: a #ClutterColor
static void cairo_set_source_color(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Color*/ color) nothrow {
   clutter_cairo_set_source_color(UpCast!(cairo.Context*)(cr), UpCast!(Color*)(color));
}

enum int careof = 2744;
enum int caret = 2812;
enum int caron = 439;
enum int ccaron = 488;
enum int ccedilla = 231;
enum int ccircumflex = 742;
enum int cedilla = 184;
enum int cent_ = 162;
enum int ch = 65184;

// VERSION: 1.2
// Run-time version check, to check the version the Clutter library
// that an application is currently linked against
// 
// This is the run-time equivalent of the compile-time %CLUTTER_CHECK_VERSION
// pre-processor macro
// 
// greater than (@major, @minor, @micro), and %FALSE otherwise
// RETURNS: %TRUE if the version of the Clutter library is
// <major>: major version, like 1 in 1.2.3
// <minor>: minor version, like 2 in 1.2.3
// <micro>: micro version, like 3 in 1.2.3
static int check_version()(uint major, uint minor, uint micro) nothrow {
   return clutter_check_version(major, minor, micro);
}


// VERSION: 1.10
// Checks the run-time name of the Clutter windowing system backend, using
// the symbolic macros like %CLUTTER_WINDOWING_WIN32 or
// %CLUTTER_WINDOWING_X11.
// 
// This function should be used in conjuction with the compile-time macros
// inside applications and libraries that are using the platform-specific
// windowing system API, to ensure that they are running on the correct
// windowing system; for instance:
// 
// |[
// &num;ifdef CLUTTER_WINDOWING_X11
// if (clutter_check_windowing_backend (CLUTTER_WINDOWING_X11))
// {
// /&ast; it is safe to use the clutter_x11_* API &ast;/
// }
// else
// &num;endif
// &num;ifdef CLUTTER_WINDOWING_WIN32
// if (clutter_check_windowing_backend (CLUTTER_WINDOWING_WIN32))
// {
// /&ast; it is safe to use the clutter_win32_* API &ast;/
// }
// else
// &num;endif
// g_error ("Unknown Clutter backend.");
// ]|
// 
// the one checked, and %FALSE otherwise
// RETURNS: %TRUE if the current Clutter windowing system backend is
// <backend_type>: the name of the backend to check
static int check_windowing_backend(AT0)(AT0 /*char*/ backend_type) nothrow {
   return clutter_check_windowing_backend(toCString!(char*)(backend_type));
}

enum int checkerboard = 2529;
enum int checkmark = 2803;
enum int circle = 3023;

// VERSION: 0.8
// DEPRECATED (v1.10) function: clear_glyph_cache - Use clutter_get_font_map() and
// Clears the internal cache of glyphs used by the Pango
// renderer. This will free up some memory and GL texture
// resources. The cache will be automatically refilled as more text is
// drawn.
// 
// 
// cogl_pango_font_map_clear_glyph_cache() instead.
static void clear_glyph_cache()() nothrow {
   clutter_clear_glyph_cache();
}

enum int club = 2796;
enum int colon = 58;

// VERSION: 0.2
// MOVED TO: Color.equal
// Compares two #ClutterColor<!-- -->s and checks if they are the same.
// 
// This function can be passed to g_hash_table_new() as the @key_equal_func
// parameter, when using #ClutterColor<!-- -->s as keys in a #GHashTable.
// RETURNS: %TRUE if the two colors are the same.
// <v1>: a #ClutterColor
// <v2>: a #ClutterColor
static int color_equal(AT0, AT1)(AT0 /*const(void)*/ v1, AT1 /*const(void)*/ v2) nothrow {
   return clutter_color_equal(UpCast!(const(void)*)(v1), UpCast!(const(void)*)(v2));
}


// VERSION: 1.6
// MOVED TO: Color.get_static
// Retrieves a static color for the given @color name
// 
// Static colors are created by Clutter and are guaranteed to always be
// available and valid
// 
// is owned by Clutter and it should never be modified or freed
// RETURNS: a pointer to a static color; the returned pointer
// <color>: the named global color
static Color* color_get_static()(StaticColor color) nothrow {
   return clutter_color_get_static(color);
}


// VERSION: 1.0
// MOVED TO: Color.hash
// Converts a #ClutterColor to a hash value.
// 
// This function can be passed to g_hash_table_new() as the @hash_func
// parameter, when using #ClutterColor<!-- -->s as keys in a #GHashTable.
// RETURNS: a hash value corresponding to the color
// <v>: a #ClutterColor
static uint color_hash(AT0)(AT0 /*const(void)*/ v) nothrow {
   return clutter_color_hash(UpCast!(const(void)*)(v));
}

enum int comma = 44;

// VERSION: 0.8
// MOVED TO: Container.class_find_child_property
// Looks up the #GParamSpec for a child property of @klass.
// 
// if no such property exist.
// RETURNS: The #GParamSpec for the property or %NULL
// <klass>: a #GObjectClass implementing the #ClutterContainer interface.
// <property_name>: a property name.
static GObject2.ParamSpec* container_class_find_child_property(AT0, AT1)(AT0 /*GObject2.ObjectClass*/ klass, AT1 /*char*/ property_name) nothrow {
   return clutter_container_class_find_child_property(UpCast!(GObject2.ObjectClass*)(klass), toCString!(char*)(property_name));
}


// VERSION: 0.8
// MOVED TO: Container.class_list_child_properties
// Returns an array of #GParamSpec for all child properties.
// 
// of #GParamSpec<!-- -->s which should be freed after use.
// RETURNS: an array
// <klass>: a #GObjectClass implementing the #ClutterContainer interface.
// <n_properties>: return location for length of returned array.
static GObject2.ParamSpec** /*new*/ container_class_list_child_properties(AT0, AT1)(AT0 /*GObject2.ObjectClass*/ klass, /*out*/ AT1 /*uint*/ n_properties) nothrow {
   return clutter_container_class_list_child_properties(UpCast!(GObject2.ObjectClass*)(klass), UpCast!(uint*)(n_properties));
}

enum int containsas = 16785931;
enum int copyright = 169;
enum int cr = 2532;
enum int crossinglines = 2542;
enum int cuberoot = 16785947;
enum int currency = 164;
enum int cursor = 2815;
enum int d = 100;
enum int dabovedot = 16784907;
enum int dagger = 2801;
enum int dcaron = 495;
enum int dead_A = 65153;
enum int dead_E = 65155;
enum int dead_I = 65157;
enum int dead_O = 65159;
enum int dead_U = 65161;
enum int dead_a = 65152;
enum int dead_abovecomma = 65124;
enum int dead_abovedot = 65110;
enum int dead_abovereversedcomma = 65125;
enum int dead_abovering = 65112;
enum int dead_acute = 65105;
enum int dead_belowbreve = 65131;
enum int dead_belowcircumflex = 65129;
enum int dead_belowcomma = 65134;
enum int dead_belowdiaeresis = 65132;
enum int dead_belowdot = 65120;
enum int dead_belowmacron = 65128;
enum int dead_belowring = 65127;
enum int dead_belowtilde = 65130;
enum int dead_breve = 65109;
enum int dead_capital_schwa = 65163;
enum int dead_caron = 65114;
enum int dead_cedilla = 65115;
enum int dead_circumflex = 65106;
enum int dead_currency = 65135;
enum int dead_dasia = 65125;
enum int dead_diaeresis = 65111;
enum int dead_doubleacute = 65113;
enum int dead_doublegrave = 65126;
enum int dead_e = 65154;
enum int dead_grave = 65104;
enum int dead_hook = 65121;
enum int dead_horn = 65122;
enum int dead_i = 65156;
enum int dead_invertedbreve = 65133;
enum int dead_iota = 65117;
enum int dead_macron = 65108;
enum int dead_o = 65158;
enum int dead_ogonek = 65116;
enum int dead_perispomeni = 65107;
enum int dead_psili = 65124;
enum int dead_semivoiced_sound = 65119;
enum int dead_small_schwa = 65162;
enum int dead_stroke = 65123;
enum int dead_tilde = 65107;
enum int dead_u = 65160;
enum int dead_voiced_sound = 65118;
enum int decimalpoint = 2749;
enum int degree = 176;
enum int diaeresis = 168;
enum int diamond = 2797;
enum int digitspace = 2725;
enum int dintegral = 16785964;
enum int division = 247;

// VERSION: 0.4
// Processes an event.
// 
// The @event must be a valid #ClutterEvent and have a #ClutterStage
// associated to it.
// 
// This function is only useful when embedding Clutter inside another
// toolkit, and it should never be called by applications.
// <event>: a #ClutterEvent.
static void do_event(AT0)(AT0 /*Event*/ event) nothrow {
   clutter_do_event(UpCast!(Event*)(event));
}

enum int dollar = 36;
enum int doubbaselinedot = 2735;
enum int doubleacute = 445;
enum int doubledagger = 2802;
enum int doublelowquotemark = 2814;
enum int downarrow = 2302;
enum int downcaret = 2984;
enum int downshoe = 3030;
enum int downstile = 3012;
enum int downtack = 3010;
enum int dstroke = 496;
enum int e = 101;
enum int eabovedot = 1004;
enum int eacute = 233;
enum int ebelowdot = 16785081;
enum int ecaron = 492;
enum int ecircumflex = 234;
enum int ecircumflexacute = 16785087;
enum int ecircumflexbelowdot = 16785095;
enum int ecircumflexgrave = 16785089;
enum int ecircumflexhook = 16785091;
enum int ecircumflextilde = 16785093;
enum int ediaeresis = 235;
enum int egrave = 232;
enum int ehook = 16785083;
enum int eightsubscript = 16785544;
enum int eightsuperior = 16785528;
enum int elementof = 16785928;
enum int ellipsis = 2734;
enum int em3space = 2723;
enum int em4space = 2724;
enum int emacron = 954;
enum int emdash = 2729;
enum int emfilledcircle = 2782;
enum int emfilledrect = 2783;
enum int emopencircle = 2766;
enum int emopenrectangle = 2767;
enum int emptyset = 16785925;
enum int emspace = 2721;
enum int endash = 2730;
enum int enfilledcircbullet = 2790;
enum int enfilledsqbullet = 2791;
enum int eng = 959;
enum int enopencircbullet = 2784;
enum int enopensquarebullet = 2785;
enum int enspace = 2722;
enum int eogonek = 490;
enum int equal = 61;
enum int eth = 240;
enum int etilde = 16785085;

// VERSION: 0.4
// MOVED TO: Event.get
// Pops an event off the event queue. Applications should not need to call 
// this.
// RETURNS: A #ClutterEvent or NULL if queue empty
static Event* /*new*/ event_get()() nothrow {
   return clutter_event_get();
}


// VERSION: 0.4
// MOVED TO: Event.peek
// Returns a pointer to the first event from the event queue but 
// does not remove it.
// RETURNS: A #ClutterEvent or NULL if queue empty.
static Event* event_peek()() nothrow {
   return clutter_event_peek();
}


// VERSION: 0.4
// Checks if events are pending in the event queue.
// RETURNS: TRUE if there are pending events, FALSE otherwise.
static int events_pending()() nothrow {
   return clutter_events_pending();
}

enum int exclam = 33;
enum int exclamdown = 161;
enum int f = 102;
enum int fabovedot = 16784927;

// VERSION: 0.1.1
// Checks whether @feature is available.  @feature can be a logical
// OR of #ClutterFeatureFlags.
// RETURNS: %TRUE if a feature is available
// <feature>: a #ClutterFeatureFlags
static int feature_available()(FeatureFlags feature) nothrow {
   return clutter_feature_available(feature);
}


// VERSION: 0.1.1
// Returns all the supported features.
// RETURNS: a logical OR of all the supported features.
static FeatureFlags feature_get_all()() nothrow {
   return clutter_feature_get_all();
}

enum int femalesymbol = 2808;
enum int ff = 2531;
enum int figdash = 2747;
enum int filledlefttribullet = 2780;
enum int filledrectbullet = 2779;
enum int filledrighttribullet = 2781;
enum int filledtribulletdown = 2793;
enum int filledtribulletup = 2792;
enum int fiveeighths = 2757;
enum int fivesixths = 2743;
enum int fivesubscript = 16785541;
enum int fivesuperior = 16785525;
enum int fourfifths = 2741;
enum int foursubscript = 16785540;
enum int foursuperior = 16785524;
enum int fourthroot = 16785948;

// Unintrospectable function: frame_source_add() / clutter_frame_source_add()
// VERSION: 0.8
// DEPRECATED function: frame_source_add - 1.6
// Simple wrapper around clutter_frame_source_add_full().
// RETURNS: the ID (greater than 0) of the event source.
// <fps>: the number of times per second to call the function
// <func>: function to call
// <data>: data to pass to the function
static uint frame_source_add(AT0)(uint fps, GLib2.SourceFunc func, AT0 /*void*/ data) nothrow {
   return clutter_frame_source_add(fps, func, UpCast!(void*)(data));
}


// VERSION: 0.8
// DEPRECATED function: frame_source_add_full - 1.6
// Sets a function to be called at regular intervals with the given
// priority.  The function is called repeatedly until it returns
// %FALSE, at which point the timeout is automatically destroyed and
// the function will not be called again.  The @notify function is
// called when the timeout is destroyed.  The first call to the
// function will be at the end of the first @interval.
// 
// This function is similar to g_timeout_add_full() except that it
// will try to compensate for delays. For example, if @func takes half
// the interval time to execute then the function will be called again
// half the interval time after it finished. In contrast
// g_timeout_add_full() would not fire until a full interval after the
// function completes so the delay between calls would be 1.0 / @fps *
// 1.5. This function does not however try to invoke the function
// multiple times to catch up missing frames if @func takes more than
// @interval ms to execute.
// RETURNS: the ID (greater than 0) of the event source.
// <priority>: the priority of the frame source. Typically this will be in the range between %G_PRIORITY_DEFAULT and %G_PRIORITY_HIGH.
// <fps>: the number of times per second to call the function
// <func>: function to call
// <data>: data to pass to the function
// <notify>: function to call when the timeout source is removed
static uint frame_source_add_full(AT0)(int priority, uint fps, GLib2.SourceFunc func, AT0 /*void*/ data, GLib2.DestroyNotify notify) nothrow {
   return clutter_frame_source_add_full(priority, fps, func, UpCast!(void*)(data), notify);
}

enum int function_ = 2294;
enum int g = 103;
enum int gabovedot = 757;
enum int gbreve = 699;
enum int gcaron = 16777703;
enum int gcedilla = 955;
enum int gcircumflex = 760;

// VERSION: 1.4
// Returns whether Clutter has accessibility support enabled.  As
// least, a value of TRUE means that there are a proper AtkUtil
// implementation available
// RETURNS: %TRUE if Clutter has accessibility support enabled
static int get_accessibility_enabled()() nothrow {
   return clutter_get_accessibility_enabled();
}


// VERSION: 0.6
// DEPRECATED (v1.8) function: get_actor_by_gid - The id is not used any longer.
// Retrieves the #ClutterActor with @id_.
// 
// The returned actor does not have its reference count increased.
// RETURNS: the actor with the passed id or %NULL.
// <id_>: a #ClutterActor unique id.
static Actor* get_actor_by_gid()(uint id_) nothrow {
   return clutter_get_actor_by_gid(id_);
}


// VERSION: 1.2
// If an event is currently being processed, return that event.
// This function is intended to be used to access event state
// that might not be exposed by higher-level widgets.  For
// example, to get the key modifier state from a Button 'clicked'
// event.
// RETURNS: The current ClutterEvent, or %NULL if none
static Event* get_current_event()() nothrow {
   return clutter_get_current_event();
}


// VERSION: 1.0
// Retrieves the timestamp of the last event, if there is an
// event or if the event has a timestamp.
// RETURNS: the event timestamp, or %CLUTTER_CURRENT_TIME
static uint get_current_event_time()() nothrow {
   return clutter_get_current_event_time();
}


// DEPRECATED (v1.10) function: get_debug_enabled - This function does not do anything.
// Check if Clutter has debugging enabled.
// RETURNS: %FALSE
static int get_debug_enabled()() nothrow {
   return clutter_get_debug_enabled();
}


// VERSION: 0.4
// Retrieves the default #ClutterBackend used by Clutter. The
// #ClutterBackend holds backend-specific configuration options.
// 
// not ref or unref the returned object. Applications should rarely
// need to use this.
// RETURNS: the default backend. You should
static Backend* get_default_backend()() nothrow {
   return clutter_get_default_backend();
}


// VERSION: 0.6
// Retrieves the default frame rate. See clutter_set_default_frame_rate().
// RETURNS: the default frame rate
static uint get_default_frame_rate()() nothrow {
   return clutter_get_default_frame_rate();
}


// VERSION: 1.2
// Retrieves the default direction for the text. The text direction is
// determined by the locale and/or by the <varname>CLUTTER_TEXT_DIRECTION</varname>
// environment variable.
// 
// The default text direction can be overridden on a per-actor basis by using
// clutter_actor_set_text_direction().
// RETURNS: the default text direction
static TextDirection get_default_text_direction()() nothrow {
   return clutter_get_default_text_direction();
}


// VERSION: 1.0
// DEPRECATED (v1.10) function: get_font_flags - Use clutter_backend_get_font_options() and the
// Gets the current font flags for rendering text. See
// clutter_set_font_flags().
// 
// 
// 
// #cairo_font_options_t API.
// RETURNS: The font flags
static FontFlags get_font_flags()() nothrow {
   return clutter_get_font_flags();
}


// VERSION: 1.0
// Retrieves the #PangoFontMap instance used by Clutter.
// You can use the global font map object with the COGL
// Pango API.
// 
// value is owned by Clutter and it should never be unreferenced.
// RETURNS: the #PangoFontMap instance. The returned
static Pango.FontMap* get_font_map()() nothrow {
   return clutter_get_font_map();
}


// VERSION: 0.8
// DEPRECATED (v1.10) function: get_input_device_for_id - Use clutter_device_manager_get_device() instead.
// Retrieves the #ClutterInputDevice from its @id_. This is a convenience
// wrapper for clutter_device_manager_get_device() and it is functionally
// equivalent to:
// 
// |[
// ClutterDeviceManager *manager;
// ClutterInputDevice *device;
// 
// manager = clutter_device_manager_get_default ();
// device = clutter_device_manager_get_device (manager, id);
// ]|
// RETURNS: a #ClutterInputDevice, or %NULL
// <id_>: the unique id for a device
static InputDevice* get_input_device_for_id()(int id_) nothrow {
   return clutter_get_input_device_for_id(id_);
}


// VERSION: 0.6
// Queries the current keyboard grab of clutter.
// RETURNS: the actor currently holding the keyboard grab, or NULL if there is no grab.
static Actor* get_keyboard_grab()() nothrow {
   return clutter_get_keyboard_grab();
}


// VERSION: 0.6
// DEPRECATED (v1.8) function: get_motion_events_enabled - Use clutter_stage_get_motion_events_enabled() instead.
// Gets whether the per-actor motion events are enabled.
// RETURNS: %TRUE if the motion events are enabled
static int get_motion_events_enabled()() nothrow {
   return clutter_get_motion_events_enabled();
}


// Unintrospectable function: get_option_group() / clutter_get_option_group()
// VERSION: 0.2
// Returns a #GOptionGroup for the command line arguments recognized
// by Clutter. You should add this group to your #GOptionContext with
// g_option_context_add_group(), if you are using g_option_context_parse()
// to parse your commandline arguments.
// 
// Calling g_option_context_parse() with Clutter's #GOptionGroup will result
// in Clutter's initialization. That is, the following code:
// 
// |[
// g_option_context_set_main_group (context, clutter_get_option_group ());
// res = g_option_context_parse (context, &amp;argc, &amp;argc, NULL);
// ]|
// 
// is functionally equivalent to:
// 
// |[
// clutter_init (&amp;argc, &amp;argv);
// ]|
// 
// After g_option_context_parse() on a #GOptionContext containing the
// Clutter #GOptionGroup has returned %TRUE, Clutter is guaranteed to be
// initialized.
// 
// recognized by Clutter
// RETURNS: a #GOptionGroup for the commandline arguments
static GLib2.OptionGroup* /*new*/ get_option_group()() nothrow {
   return clutter_get_option_group();
}


// Unintrospectable function: get_option_group_without_init() / clutter_get_option_group_without_init()
// VERSION: 0.8.2
// Returns a #GOptionGroup for the command line arguments recognized
// by Clutter. You should add this group to your #GOptionContext with
// g_option_context_add_group(), if you are using g_option_context_parse()
// to parse your commandline arguments.
// 
// Unlike clutter_get_option_group(), calling g_option_context_parse() with
// the #GOptionGroup returned by this function requires a subsequent explicit
// call to clutter_init(); use this function when needing to set foreign
// display connection with clutter_x11_set_display(), or with
// <function>gtk_clutter_init()</function>.
// 
// recognized by Clutter
// RETURNS: a #GOptionGroup for the commandline arguments
static GLib2.OptionGroup* /*new*/ get_option_group_without_init()() nothrow {
   return clutter_get_option_group_without_init();
}


// VERSION: 0.6
// Queries the current pointer grab of clutter.
// RETURNS: the actor currently holding the pointer grab, or NULL if there is no grab.
static Actor* get_pointer_grab()() nothrow {
   return clutter_get_pointer_grab();
}


// VERSION: 0.6
// Retrieves the Clutter script id, if any.
// 
// a UI definition file. The returned string is owned by the object and
// should never be modified or freed.
// RETURNS: the script id, or %NULL if @object was not defined inside
// <gobject>: a #GObject
static char* get_script_id(AT0)(AT0 /*GObject2.Object*/ gobject) nothrow {
   return clutter_get_script_id(UpCast!(GObject2.Object*)(gobject));
}


// VERSION: 0.4
// DEPRECATED (v1.10) function: get_show_fps - This function does not do anything. Use the environment
// Returns whether Clutter should print out the frames per second on the
// console. You can enable this setting either using the
// <literal>CLUTTER_SHOW_FPS</literal> environment variable or passing
// the <literal>--clutter-show-fps</literal> command line argument. *
// 
// 
// 
// variable or the configuration file to determine whether Clutter should
// print out the FPS counter on the console.
// RETURNS: %TRUE if Clutter should show the FPS.
static int get_show_fps()() nothrow {
   return clutter_get_show_fps();
}


// DEPRECATED (v1.10) function: get_timestamp - Use #GTimer or g_get_monotonic_time() for a proper
// Returns the approximate number of microseconds passed since Clutter was
// intialised.
// 
// This function shdould not be used by application code.
// 
// The output of this function depends on whether Clutter was configured to
// enable its debugging code paths, so it's less useful than intended.
// 
// Since Clutter 1.10, this function is an alias to g_get_monotonic_time()
// if Clutter was configured to enable the debugging code paths.
// 
// zero if Clutter was not configured with debugging code paths.
// 
// timing source
// RETURNS: Number of microseconds since clutter_init() was called, or
static c_ulong get_timestamp()() nothrow {
   return clutter_get_timestamp();
}


// VERSION: 0.6
// Grabs keyboard events, after the grab is done keyboard
// events (#ClutterActor::key-press-event and #ClutterActor::key-release-event)
// are delivered to this actor directly. The source set in the event will be
// the actor that would have received the event if the keyboard grab was not
// in effect.
// 
// Like pointer grabs, keyboard grabs should only be used as a last
// resource.
// 
// See also clutter_stage_set_key_focus() and clutter_actor_grab_key_focus()
// to perform a "soft" key grab and assign key focus to a specific actor.
// <actor>: a #ClutterActor
static void grab_keyboard(AT0)(AT0 /*Actor*/ actor) nothrow {
   clutter_grab_keyboard(UpCast!(Actor*)(actor));
}


// VERSION: 0.6
// Grabs pointer events, after the grab is done all pointer related events
// (press, motion, release, enter, leave and scroll) are delivered to this
// actor directly without passing through both capture and bubble phases of
// the event delivery chain. The source set in the event will be the actor
// that would have received the event if the pointer grab was not in effect.
// 
// <note><para>Grabs completely override the entire event delivery chain
// done by Clutter. Pointer grabs should only be used as a last resource;
// using the #ClutterActor::captured-event signal should always be the
// preferred way to intercept event delivery to reactive actors.</para></note>
// 
// This function should rarely be used.
// 
// If a grab is required, you are strongly encouraged to use a specific
// input device by calling clutter_input_device_grab().
// <actor>: a #ClutterActor
static void grab_pointer(AT0)(AT0 /*Actor*/ actor) nothrow {
   clutter_grab_pointer(UpCast!(Actor*)(actor));
}


// VERSION: 0.8
// DEPRECATED (v1.10) function: grab_pointer_for_device - Use clutter_input_device_grab() instead.
// Grabs all the pointer events coming from the device @id for @actor.
// 
// If @id is -1 then this function is equivalent to clutter_grab_pointer().
// <actor>: a #ClutterActor
// <id_>: a device id, or -1
static void grab_pointer_for_device(AT0)(AT0 /*Actor*/ actor, int id_) nothrow {
   clutter_grab_pointer_for_device(UpCast!(Actor*)(actor), id_);
}

enum int grave = 96;
enum int greater = 62;
enum int greaterthanequal = 2238;
enum int guillemotleft = 171;
enum int guillemotright = 187;
enum int h = 104;
enum int hairspace = 2728;
enum int hcircumflex = 694;
enum int heart = 2798;
enum int hebrew_aleph = 3296;
enum int hebrew_ayin = 3314;
enum int hebrew_bet = 3297;
enum int hebrew_beth = 3297;
enum int hebrew_chet = 3303;
enum int hebrew_dalet = 3299;
enum int hebrew_daleth = 3299;
enum int hebrew_doublelowline = 3295;
enum int hebrew_finalkaph = 3306;
enum int hebrew_finalmem = 3309;
enum int hebrew_finalnun = 3311;
enum int hebrew_finalpe = 3315;
enum int hebrew_finalzade = 3317;
enum int hebrew_finalzadi = 3317;
enum int hebrew_gimel = 3298;
enum int hebrew_gimmel = 3298;
enum int hebrew_he = 3300;
enum int hebrew_het = 3303;
enum int hebrew_kaph = 3307;
enum int hebrew_kuf = 3319;
enum int hebrew_lamed = 3308;
enum int hebrew_mem = 3310;
enum int hebrew_nun = 3312;
enum int hebrew_pe = 3316;
enum int hebrew_qoph = 3319;
enum int hebrew_resh = 3320;
enum int hebrew_samech = 3313;
enum int hebrew_samekh = 3313;
enum int hebrew_shin = 3321;
enum int hebrew_taf = 3322;
enum int hebrew_taw = 3322;
enum int hebrew_tet = 3304;
enum int hebrew_teth = 3304;
enum int hebrew_waw = 3301;
enum int hebrew_yod = 3305;
enum int hebrew_zade = 3318;
enum int hebrew_zadi = 3318;
enum int hebrew_zain = 3302;
enum int hebrew_zayin = 3302;
enum int hexagram = 2778;
enum int horizconnector = 2211;
enum int horizlinescan1 = 2543;
enum int horizlinescan3 = 2544;
enum int horizlinescan5 = 2545;
enum int horizlinescan7 = 2546;
enum int horizlinescan9 = 2547;
enum int hstroke = 689;
enum int ht = 2530;
enum int hyphen = 173;
enum int i = 105;
enum int iTouch = 269025120;
enum int iacute = 237;
enum int ibelowdot = 16785099;
enum int ibreve = 16777517;
enum int icircumflex = 238;
enum int identical = 2255;
enum int idiaeresis = 239;
enum int idotless = 697;
enum int ifonlyif = 2253;
enum int igrave = 236;
enum int ihook = 16785097;
enum int imacron = 1007;
enum int implies = 2254;
enum int includedin = 2266;
enum int includes = 2267;
enum int infinity = 2242;

// Initialises everything needed to operate with Clutter and parses some
// standard command line options; @argc and @argv are adjusted accordingly
// so your own code will never see those standard arguments.
// 
// It is safe to call this function multiple times.
// 
// <note>This function will not abort in case of errors during
// initialization; clutter_init() will print out the error message on
// stderr, and will return an error code. It is up to the application
// code to handle this case. If you need to display the error message
// yourself, you can use clutter_init_with_args(), which takes a #GError
// pointer.</note>
// 
// If this function fails, and returns an error code, any subsequent
// Clutter API will have undefined behaviour - including segmentation
// faults and assertion failures. Make sure to handle the returned
// #ClutterInitError enumeration value.
// RETURNS: a #ClutterInitError value
// <argc>: The number of arguments in @argv
// <argv>: A pointer to an array of arguments.
static InitError init(AT0)(/*inout*/ int* argc, /*inout*/ AT0 /*char***/ argv=null) nothrow {
   return clutter_init(argc, toCString!(char***)(argv));
}

// MOVED TO: InitError.quark
static GLib2.Quark init_error_quark()() nothrow {
   return clutter_init_error_quark();
}


// VERSION: 0.2
// This function does the same work as clutter_init(). Additionally,
// it allows you to add your own command line options, and it
// automatically generates nicely formatted <option>--help</option>
// output. Note that your program will be terminated after writing
// out the help output. Also note that, in case of error, the
// error message will be placed inside @error instead of being
// printed on the display.
// 
// Just like clutter_init(), if this function returns an error code then
// any subsequent call to any other Clutter API will result in undefined
// behaviour - including segmentation faults.
// 
// initialised, or other values or #ClutterInitError in case of
// error.
// RETURNS: %CLUTTER_INIT_SUCCESS if Clutter has been successfully
// <argc>: a pointer to the number of command line arguments
// <argv>: a pointer to the array of command line arguments
// <parameter_string>: a string which is displayed in the first line of <option>--help</option> output, after <literal><replaceable>programname</replaceable> [OPTION...]</literal>
// <entries>: a %NULL terminated array of #GOptionEntry<!-- -->s describing the options of your program
// <translation_domain>: a translation domain to use for translating the <option>--help</option> output for the options in @entries with gettext(), or %NULL
static InitError init_with_args(AT0, AT1, AT2, AT3, AT4)(/*inout*/ int* argc, /*inout*/ AT0 /*char***/ argv, AT1 /*char*/ parameter_string, AT2 /*GLib2.OptionEntry*/ entries, AT3 /*char*/ translation_domain, AT4 /*GLib2.Error**/ error=null) nothrow {
   return clutter_init_with_args(argc, toCString!(char***)(argv), toCString!(char*)(parameter_string), UpCast!(GLib2.OptionEntry*)(entries), toCString!(char*)(translation_domain), UpCast!(GLib2.Error**)(error));
}

enum int integral = 2239;
enum int intersection = 2268;
enum int iogonek = 999;
enum int itilde = 949;
enum int j = 106;
enum int jcircumflex = 700;
enum int jot = 3018;
enum int k = 107;
enum int kana_A = 1201;
enum int kana_CHI = 1217;
enum int kana_E = 1204;
enum int kana_FU = 1228;
enum int kana_HA = 1226;
enum int kana_HE = 1229;
enum int kana_HI = 1227;
enum int kana_HO = 1230;
enum int kana_HU = 1228;
enum int kana_I = 1202;
enum int kana_KA = 1206;
enum int kana_KE = 1209;
enum int kana_KI = 1207;
enum int kana_KO = 1210;
enum int kana_KU = 1208;
enum int kana_MA = 1231;
enum int kana_ME = 1234;
enum int kana_MI = 1232;
enum int kana_MO = 1235;
enum int kana_MU = 1233;
enum int kana_N = 1245;
enum int kana_NA = 1221;
enum int kana_NE = 1224;
enum int kana_NI = 1222;
enum int kana_NO = 1225;
enum int kana_NU = 1223;
enum int kana_O = 1205;
enum int kana_RA = 1239;
enum int kana_RE = 1242;
enum int kana_RI = 1240;
enum int kana_RO = 1243;
enum int kana_RU = 1241;
enum int kana_SA = 1211;
enum int kana_SE = 1214;
enum int kana_SHI = 1212;
enum int kana_SO = 1215;
enum int kana_SU = 1213;
enum int kana_TA = 1216;
enum int kana_TE = 1219;
enum int kana_TI = 1217;
enum int kana_TO = 1220;
enum int kana_TSU = 1218;
enum int kana_TU = 1218;
enum int kana_U = 1203;
enum int kana_WA = 1244;
enum int kana_WO = 1190;
enum int kana_YA = 1236;
enum int kana_YO = 1238;
enum int kana_YU = 1237;
enum int kana_a = 1191;
enum int kana_closingbracket = 1187;
enum int kana_comma = 1188;
enum int kana_conjunctive = 1189;
enum int kana_e = 1194;
enum int kana_fullstop = 1185;
enum int kana_i = 1192;
enum int kana_middledot = 1189;
enum int kana_o = 1195;
enum int kana_openingbracket = 1186;
enum int kana_switch = 65406;
enum int kana_tsu = 1199;
enum int kana_tu = 1199;
enum int kana_u = 1193;
enum int kana_ya = 1196;
enum int kana_yo = 1198;
enum int kana_yu = 1197;
enum int kappa = 930;
enum int kcedilla = 1011;

// Converts @keyval from a Clutter key symbol to the corresponding
// ISO10646 (Unicode) character.
// 
// character.
// RETURNS: a Unicode character, or 0 if there  is no corresponding
// <keyval>: a key symbol
static uint keysym_to_unicode()(uint keyval) nothrow {
   return clutter_keysym_to_unicode(keyval);
}

enum int kra = 930;
enum int l = 108;
enum int lacute = 485;
enum int latincross = 2777;
enum int lbelowdot = 16784951;
enum int lcaron = 437;
enum int lcedilla = 950;
enum int leftanglebracket = 2748;
enum int leftarrow = 2299;
enum int leftcaret = 2979;
enum int leftdoublequotemark = 2770;
enum int leftmiddlecurlybrace = 2223;
enum int leftopentriangle = 2764;
enum int leftpointer = 2794;
enum int leftradical = 2209;
enum int leftshoe = 3034;
enum int leftsinglequotemark = 2768;
enum int leftt = 2548;
enum int lefttack = 3036;
enum int less = 60;
enum int lessthanequal = 2236;
enum int lf = 2533;
enum int logicaland = 2270;
enum int logicalor = 2271;
enum int lowleftcorner = 2541;
enum int lowrightcorner = 2538;
enum int lstroke = 435;
enum int m = 109;
enum int mabovedot = 16784961;
enum int macron = 175;
// Starts the Clutter mainloop.
static void main_()() nothrow {
   clutter_main();
}


// Retrieves the depth of the Clutter mainloop.
// RETURNS: The level of the mainloop.
static int main_level()() nothrow {
   return clutter_main_level();
}

// Terminates the Clutter mainloop.
static void main_quit()() nothrow {
   clutter_main_quit();
}

enum int malesymbol = 2807;
enum int maltesecross = 2800;
enum int marker = 2751;
enum int masculine = 186;
enum int minus = 45;
enum int minutes = 2774;
enum int mu = 181;
enum int multiply = 215;
enum int musicalflat = 2806;
enum int musicalsharp = 2805;
enum int n = 110;
enum int nabla = 2245;
enum int nacute = 497;
enum int ncaron = 498;
enum int ncedilla = 1009;
enum int ninesubscript = 16785545;
enum int ninesuperior = 16785529;
enum int nl = 2536;
enum int nobreakspace = 160;
enum int notapproxeq = 16785991;
enum int notelementof = 16785929;
enum int notequal = 2237;
enum int notidentical = 16786018;
enum int notsign = 172;
enum int ntilde = 241;
enum int numbersign = 35;
enum int numerosign = 1712;
enum int o = 111;
enum int oacute = 243;
enum int obarred = 16777845;
enum int obelowdot = 16785101;
enum int ocaron = 16777682;
enum int ocircumflex = 244;
enum int ocircumflexacute = 16785105;
enum int ocircumflexbelowdot = 16785113;
enum int ocircumflexgrave = 16785107;
enum int ocircumflexhook = 16785109;
enum int ocircumflextilde = 16785111;
enum int odiaeresis = 246;
enum int odoubleacute = 501;
enum int oe = 5053;
enum int ogonek = 434;
enum int ograve = 242;
enum int ohook = 16785103;
enum int ohorn = 16777633;
enum int ohornacute = 16785115;
enum int ohornbelowdot = 16785123;
enum int ohorngrave = 16785117;
enum int ohornhook = 16785119;
enum int ohorntilde = 16785121;
enum int omacron = 1010;
enum int oneeighth = 2755;
enum int onefifth = 2738;
enum int onehalf = 189;
enum int onequarter = 188;
enum int onesixth = 2742;
enum int onesubscript = 16785537;
enum int onesuperior = 185;
enum int onethird = 2736;
enum int ooblique = 248;
enum int openrectbullet = 2786;
enum int openstar = 2789;
enum int opentribulletdown = 2788;
enum int opentribulletup = 2787;
enum int ordfeminine = 170;
enum int oslash = 248;
enum int otilde = 245;
enum int overbar = 3008;
enum int overline = 1150;
enum int p = 112;
enum int pabovedot = 16784983;
enum int paragraph = 182;

// Unintrospectable function: param_spec_color() / clutter_param_spec_color()
// VERSION: 0.8.4
// Creates a #GParamSpec for properties using #ClutterColor.
// RETURNS: the newly created #GParamSpec
// <name>: name of the property
// <nick>: short name
// <blurb>: description (can be translatable)
// <default_value>: default value
// <flags>: flags for the param spec
static GObject2.ParamSpec* param_spec_color(AT0, AT1, AT2, AT3)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, AT3 /*Color*/ default_value, GObject2.ParamFlags flags) nothrow {
   return clutter_param_spec_color(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), UpCast!(Color*)(default_value), flags);
}


// Unintrospectable function: param_spec_fixed() / clutter_param_spec_fixed()
// VERSION: 0.8
// DEPRECATED (v1.10) function: param_spec_fixed - Use #GParamSpecInt instead.
// Creates a #GParamSpec for properties using #CoglFixed values
// RETURNS: the newly created #GParamSpec
// <name>: name of the property
// <nick>: short name
// <blurb>: description (can be translatable)
// <minimum>: lower boundary
// <maximum>: higher boundary
// <default_value>: default value
// <flags>: flags for the param spec
static GObject2.ParamSpec* /*new*/ param_spec_fixed(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, Cogl.Fixed minimum, Cogl.Fixed maximum, Cogl.Fixed default_value, GObject2.ParamFlags flags) nothrow {
   return clutter_param_spec_fixed(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), minimum, maximum, default_value, flags);
}


// Unintrospectable function: param_spec_units() / clutter_param_spec_units()
// VERSION: 1.0
// Creates a #GParamSpec for properties using #ClutterUnits.
// RETURNS: the newly created #GParamSpec
// <name>: name of the property
// <nick>: short name
// <blurb>: description (can be translatable)
// <default_type>: the default type for the #ClutterUnits
// <minimum>: lower boundary
// <maximum>: higher boundary
// <default_value>: default value
// <flags>: flags for the param spec
static GObject2.ParamSpec* param_spec_units(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, UnitType default_type, float minimum, float maximum, float default_value, GObject2.ParamFlags flags) nothrow {
   return clutter_param_spec_units(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), default_type, minimum, maximum, default_value, flags);
}

enum int parenleft = 40;
enum int parenright = 41;
enum int partdifferential = 16785922;
enum int partialderivative = 2287;
enum int percent = 37;
enum int period = 46;
enum int periodcentered = 183;
enum int phonographcopyright = 2811;
enum int plus = 43;
enum int plusminus = 177;
enum int prescription = 2772;
enum int prolongedsound = 1200;
enum int punctspace = 2726;
enum int q = 113;
enum int quad = 3020;
enum int question = 63;
enum int questiondown = 191;
enum int quotedbl = 34;
enum int quoteleft = 96;
enum int quoteright = 39;
enum int r = 114;
enum int racute = 480;
enum int radical = 2262;
enum int rcaron = 504;
enum int rcedilla = 947;

// DEPRECATED (v1.10) function: redraw - Use clutter_stage_ensure_redraw() instead.
// Forces a redraw of the entire stage. Applications should never use this
// function, but queue a redraw using clutter_actor_queue_redraw().
// 
// This function should only be used by libraries integrating Clutter from
// within another toolkit.
static void redraw(AT0)(AT0 /*Stage*/ stage) nothrow {
   clutter_redraw(UpCast!(Stage*)(stage));
}

enum int registered = 174;
enum int rightanglebracket = 2750;
enum int rightarrow = 2301;
enum int rightcaret = 2982;
enum int rightdoublequotemark = 2771;
enum int rightmiddlecurlybrace = 2224;
enum int rightmiddlesummation = 2231;
enum int rightopentriangle = 2765;
enum int rightpointer = 2795;
enum int rightshoe = 3032;
enum int rightsinglequotemark = 2769;
enum int rightt = 2549;
enum int righttack = 3068;
enum int s = 115;
enum int sabovedot = 16784993;
enum int sacute = 438;
enum int scaron = 441;
enum int scedilla = 442;
enum int schwa = 16777817;
enum int scircumflex = 766;
// MOVED TO: ScriptError.quark
static GLib2.Quark script_error_quark()() nothrow {
   return clutter_script_error_quark();
}

enum int script_switch = 65406;
enum int seconds = 2775;
enum int section = 167;
enum int semicolon = 59;
enum int semivoicedsound = 1247;

// VERSION: 0.6
// DEPRECATED (v1.10) function: set_default_frame_rate - This function does not do anything any more.
// Sets the default frame rate. This frame rate will be used to limit
// the number of frames drawn if Clutter is not able to synchronize
// with the vertical refresh rate of the display. When synchronization
// is possible, this value is ignored.
// <frames_per_sec>: the new default frame rate
static void set_default_frame_rate()(uint frames_per_sec) nothrow {
   clutter_set_default_frame_rate(frames_per_sec);
}


// VERSION: 1.0
// DEPRECATED (v1.10) function: set_font_flags - Use clutter_backend_set_font_options() and the
// Sets the font quality options for subsequent text rendering
// operations.
// 
// Using mipmapped textures will improve the quality for scaled down
// text but will use more texture memory.
// 
// Enabling hinting improves text quality for static text but may
// introduce some artifacts if the text is animated.
// 
// 
// #cairo_font_option_t API.
// <flags>: The new flags
static void set_font_flags()(FontFlags flags) nothrow {
   clutter_set_font_flags(flags);
}


// VERSION: 0.6
// DEPRECATED (v1.8) function: set_motion_events_enabled - Use clutter_stage_set_motion_events_enabled() instead.
// Sets whether per-actor motion events should be enabled or not on
// all #ClutterStage<!-- -->s managed by Clutter.
// 
// If @enable is %FALSE the following events will not work:
// <itemizedlist>
// <listitem><para>ClutterActor::motion-event, unless on the
// #ClutterStage</para></listitem>
// <listitem><para>ClutterActor::enter-event</para></listitem>
// <listitem><para>ClutterActor::leave-event</para></listitem>
// </itemizedlist>
// <enable>: %TRUE to enable per-actor motion events
static void set_motion_events_enabled()(int enable) nothrow {
   clutter_set_motion_events_enabled(enable);
}

enum int seveneighths = 2758;
enum int sevensubscript = 16785543;
enum int sevensuperior = 16785527;
enum int signaturemark = 2762;
enum int signifblank = 2732;
enum int similarequal = 2249;
enum int singlelowquotemark = 2813;
enum int sixsubscript = 16785542;
enum int sixsuperior = 16785526;
enum int slash = 47;
enum int soliddiamond = 2528;
enum int space = 32;
enum int squareroot = 16785946;
enum int ssharp = 223;
enum int sterling = 163;
enum int stricteq = 16786019;
enum int t = 116;
enum int tabovedot = 16785003;
enum int tcaron = 443;
enum int tcedilla = 510;
enum int telephone = 2809;
enum int telephonerecorder = 2810;
// MOVED TO: TextureError.quark
static GLib2.Quark texture_error_quark()() nothrow {
   return clutter_texture_error_quark();
}

enum int therefore = 2240;
enum int thinspace = 2727;
enum int thorn = 254;

// Unintrospectable function: threads_add_frame_source() / clutter_threads_add_frame_source()
// VERSION: 0.8
// DEPRECATED function: threads_add_frame_source - 1.6
// Simple wrapper around clutter_threads_add_frame_source_full().
// RETURNS: the ID (greater than 0) of the event source.
// <fps>: the number of times per second to call the function
// <func>: function to call
// <data>: data to pass to the function
static uint threads_add_frame_source(AT0)(uint fps, GLib2.SourceFunc func, AT0 /*void*/ data) nothrow {
   return clutter_threads_add_frame_source(fps, func, UpCast!(void*)(data));
}


// VERSION: 0.8
// DEPRECATED function: threads_add_frame_source_full - 1.6
// Sets a function to be called at regular intervals holding the Clutter
// threads lock, with the given priority. The function is called repeatedly
// until it returns %FALSE, at which point the timeout is automatically
// removed and the function will not be called again. The @notify function
// is called when the timeout is removed.
// 
// This function is similar to clutter_threads_add_timeout_full()
// except that it will try to compensate for delays. For example, if
// @func takes half the interval time to execute then the function
// will be called again half the interval time after it finished. In
// contrast clutter_threads_add_timeout_full() would not fire until a
// full interval after the function completes so the delay between
// calls would be @interval * 1.5. This function does not however try
// to invoke the function multiple times to catch up missing frames if
// @func takes more than @interval ms to execute.
// 
// See also clutter_threads_add_idle_full().
// RETURNS: the ID (greater than 0) of the event source.
// <priority>: the priority of the frame source. Typically this will be in the range between %G_PRIORITY_DEFAULT and %G_PRIORITY_HIGH.
// <fps>: the number of times per second to call the function
// <func>: function to call
// <data>: data to pass to the function
// <notify>: function to call when the timeout source is removed
static uint threads_add_frame_source_full(AT0)(int priority, uint fps, GLib2.SourceFunc func, AT0 /*void*/ data, GLib2.DestroyNotify notify) nothrow {
   return clutter_threads_add_frame_source_full(priority, fps, func, UpCast!(void*)(data), notify);
}


// Unintrospectable function: threads_add_idle() / clutter_threads_add_idle()
// VERSION: 0.4
// Simple wrapper around clutter_threads_add_idle_full() using the
// default priority.
// RETURNS: the ID (greater than 0) of the event source.
// <func>: function to call
// <data>: data to pass to the function
static uint threads_add_idle(AT0)(GLib2.SourceFunc func, AT0 /*void*/ data) nothrow {
   return clutter_threads_add_idle(func, UpCast!(void*)(data));
}


// VERSION: 0.4
// Adds a function to be called whenever there are no higher priority
// events pending. If the function returns %FALSE it is automatically
// removed from the list of event sources and will not be called again.
// 
// This function can be considered a thread-safe variant of g_idle_add_full():
// it will call @function while holding the Clutter lock. It is logically
// equivalent to the following implementation:
// 
// |[
// static gboolean
// idle_safe_callback (gpointer data)
// {
// SafeClosure *closure = data;
// gboolean res = FALSE;
// 
// /&ast; mark the critical section &ast;/
// 
// clutter_threads_enter();
// 
// /&ast; the callback does not need to acquire the Clutter
// &ast; lock itself, as it is held by the this proxy handler
// &ast;/
// res = closure->callback (closure->data);
// 
// clutter_threads_leave();
// 
// return res;
// }
// static gulong
// add_safe_idle (GSourceFunc callback,
// gpointer    data)
// {
// SafeClosure *closure = g_new0 (SafeClosure, 1);
// 
// closure-&gt;callback = callback;
// closure-&gt;data = data;
// 
// return g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
// idle_safe_callback,
// closure,
// g_free)
// }
// ]|
// 
// This function should be used by threaded applications to make sure
// that @func is emitted under the Clutter threads lock and invoked
// from the same thread that started the Clutter main loop. For instance,
// it can be used to update the UI using the results from a worker
// thread:
// 
// |[
// static gboolean
// update_ui (gpointer data)
// {
// SomeClosure *closure = data;
// 
// /&ast; it is safe to call Clutter API from this function because
// &ast; it is invoked from the same thread that started the main
// &ast; loop and under the Clutter thread lock
// &ast;/
// clutter_label_set_text (CLUTTER_LABEL (closure-&gt;label),
// closure-&gt;text);
// 
// g_object_unref (closure-&gt;label);
// g_free (closure);
// 
// return FALSE;
// }
// 
// /&ast; within another thread &ast;/
// closure = g_new0 (SomeClosure, 1);
// /&ast; always take a reference on GObject instances &ast;/
// closure-&gt;label = g_object_ref (my_application-&gt;label);
// closure-&gt;text = g_strdup (processed_text_to_update_the_label);
// 
// clutter_threads_add_idle_full (G_PRIORITY_HIGH_IDLE,
// update_ui,
// closure,
// NULL);
// ]|
// RETURNS: the ID (greater than 0) of the event source.
// <priority>: the priority of the timeout source. Typically this will be in the range between #G_PRIORITY_DEFAULT_IDLE and #G_PRIORITY_HIGH_IDLE
// <func>: function to call
// <data>: data to pass to the function
// <notify>: functio to call when the idle source is removed
static uint threads_add_idle_full(AT0)(int priority, GLib2.SourceFunc func, AT0 /*void*/ data, GLib2.DestroyNotify notify) nothrow {
   return clutter_threads_add_idle_full(priority, func, UpCast!(void*)(data), notify);
}


// VERSION: 1.0
// Adds a function to be called whenever Clutter is repainting a Stage.
// If the function returns %FALSE it is automatically removed from the
// list of repaint functions and will not be called again.
// 
// This function is guaranteed to be called from within the same thread
// that called clutter_main(), and while the Clutter lock is being held.
// 
// A repaint function is useful to ensure that an update of the scenegraph
// is performed before the scenegraph is repainted; for instance, uploading
// a frame from a video into a #ClutterTexture.
// 
// When the repaint function is removed (either because it returned %FALSE
// or because clutter_threads_remove_repaint_func() has been called) the
// @notify function will be called, if any is set.
// 
// can use the returned integer to remove the repaint function by
// calling clutter_threads_remove_repaint_func().
// RETURNS: the ID (greater than 0) of the repaint function. You
// <func>: the function to be called within the paint cycle
// <data>: data to be passed to the function, or %NULL
// <notify>: function to be called when removing the repaint function, or %NULL
static uint threads_add_repaint_func(AT0)(GLib2.SourceFunc func, AT0 /*void*/ data, GLib2.DestroyNotify notify) nothrow {
   return clutter_threads_add_repaint_func(func, UpCast!(void*)(data), notify);
}


// Unintrospectable function: threads_add_timeout() / clutter_threads_add_timeout()
// VERSION: 0.4
// Simple wrapper around clutter_threads_add_timeout_full().
// RETURNS: the ID (greater than 0) of the event source.
// <interval>: the time between calls to the function, in milliseconds
// <func>: function to call
// <data>: data to pass to the function
static uint threads_add_timeout(AT0)(uint interval, GLib2.SourceFunc func, AT0 /*void*/ data) nothrow {
   return clutter_threads_add_timeout(interval, func, UpCast!(void*)(data));
}


// VERSION: 0.4
// Sets a function to be called at regular intervals holding the Clutter
// threads lock, with the given priority. The function is called repeatedly
// until it returns %FALSE, at which point the timeout is automatically
// removed and the function will not be called again. The @notify function
// is called when the timeout is removed.
// 
// The first call to the function will be at the end of the first @interval.
// 
// It is important to note that, due to how the Clutter main loop is
// implemented, the timing will not be accurate and it will not try to
// "keep up" with the interval.
// 
// See also clutter_threads_add_idle_full().
// RETURNS: the ID (greater than 0) of the event source.
// <priority>: the priority of the timeout source. Typically this will be in the range between #G_PRIORITY_DEFAULT and #G_PRIORITY_HIGH.
// <interval>: the time between calls to the function, in milliseconds
// <func>: function to call
// <data>: data to pass to the function
// <notify>: function to call when the timeout source is removed
static uint threads_add_timeout_full(AT0)(int priority, uint interval, GLib2.SourceFunc func, AT0 /*void*/ data, GLib2.DestroyNotify notify) nothrow {
   return clutter_threads_add_timeout_full(priority, interval, func, UpCast!(void*)(data), notify);
}


// VERSION: 0.4
// Locks the Clutter thread lock.
static void threads_enter()() nothrow {
   clutter_threads_enter();
}


// VERSION: 0.4
// DEPRECATED (v1.10) function: threads_init - This function does not do anything. Threading support
// Initialises the Clutter threading mechanism, so that Clutter API can be
// called by multiple threads, using clutter_threads_enter() and
// clutter_threads_leave() to mark the critical sections.
// 
// You must call g_thread_init() before this function.
// 
// This function must be called before clutter_init().
// 
// It is safe to call this function multiple times.
// 
// 
// is initialized when Clutter is initialized.
static void threads_init()() nothrow {
   clutter_threads_init();
}


// VERSION: 0.4
// Unlocks the Clutter thread lock.
static void threads_leave()() nothrow {
   clutter_threads_leave();
}


// VERSION: 1.0
// Removes the repaint function with @handle_id as its id
// <handle_id>: an unsigned integer greater than zero
static void threads_remove_repaint_func()(uint handle_id) nothrow {
   clutter_threads_remove_repaint_func(handle_id);
}


// Unintrospectable function: threads_set_lock_functions() / clutter_threads_set_lock_functions()
// VERSION: 0.4
// Allows the application to replace the standard method that
// Clutter uses to protect its data structures. Normally, Clutter
// creates a single #GMutex that is locked by clutter_threads_enter(),
// and released by clutter_threads_leave(); using this function an
// application provides, instead, a function @enter_fn that is
// called by clutter_threads_enter() and a function @leave_fn that is
// called by clutter_threads_leave().
// 
// The functions must provide at least same locking functionality
// as the default implementation, but can also do extra application
// specific processing.
// 
// As an example, consider an application that has its own recursive
// lock that when held, holds the Clutter lock as well. When Clutter
// unlocks the Clutter lock when entering a recursive main loop, the
// application must temporarily release its lock as well.
// 
// Most threaded Clutter apps won't need to use this method.
// 
// This method must be called before clutter_init(), and cannot
// be called multiple times.
// <enter_fn>: function called when aquiring the Clutter main lock
// <leave_fn>: function called when releasing the Clutter main lock
static void threads_set_lock_functions()(GObject2.Callback enter_fn, GObject2.Callback leave_fn) nothrow {
   clutter_threads_set_lock_functions(enter_fn, leave_fn);
}

enum int threeeighths = 2756;
enum int threefifths = 2740;
enum int threequarters = 190;
enum int threesubscript = 16785539;
enum int threesuperior = 179;

// Unintrospectable function: timeout_pool_new() / clutter_timeout_pool_new()
// VERSION: 0.4
// DEPRECATED function: timeout_pool_new - 1.6
// MOVED TO: TimeoutPool.new
// Creates a new timeout pool source. A timeout pool should be used when
// multiple timeout functions, running at the same priority, are needed and
// the g_timeout_add() API might lead to starvation of the time slice of
// the main loop. A timeout pool allocates a single time slice of the main
// loop and runs every timeout function inside it. The timeout pool is
// always sorted, so that the extraction of the next timeout function is
// a constant time operation.
// 
// is owned by the GLib default context and will be automatically
// destroyed when the context is destroyed. It is possible to force
// the destruction of the timeout pool using g_source_destroy()
// RETURNS: the newly created #ClutterTimeoutPool. The created pool
// <priority>: the priority of the timeout pool. Typically this will be #G_PRIORITY_DEFAULT
static TimeoutPool* timeout_pool_new()(int priority) nothrow {
   return clutter_timeout_pool_new(priority);
}

enum int tintegral = 16785965;
enum int topintegral = 2212;
enum int topleftparens = 2219;
enum int topleftradical = 2210;
enum int topleftsqbracket = 2215;
enum int topleftsummation = 2225;
enum int toprightparens = 2221;
enum int toprightsqbracket = 2217;
enum int toprightsummation = 2229;
enum int topt = 2551;
enum int topvertsummationconnector = 2227;
enum int trademark = 2761;
enum int trademarkincircle = 2763;
enum int tslash = 956;
enum int twofifths = 2739;
enum int twosubscript = 16785538;
enum int twosuperior = 178;
enum int twothirds = 2737;
enum int u = 117;
enum int uacute = 250;
enum int ubelowdot = 16785125;
enum int ubreve = 765;
enum int ucircumflex = 251;
enum int udiaeresis = 252;
enum int udoubleacute = 507;
enum int ugrave = 249;
enum int uhook = 16785127;
enum int uhorn = 16777648;
enum int uhornacute = 16785129;
enum int uhornbelowdot = 16785137;
enum int uhorngrave = 16785131;
enum int uhornhook = 16785133;
enum int uhorntilde = 16785135;
enum int umacron = 1022;
enum int underbar = 3014;
enum int underscore = 95;

// VERSION: 0.6
// Removes an existing grab of the keyboard.
static void ungrab_keyboard()() nothrow {
   clutter_ungrab_keyboard();
}


// VERSION: 0.6
// Removes an existing grab of the pointer.
static void ungrab_pointer()() nothrow {
   clutter_ungrab_pointer();
}


// VERSION: 0.8
// DEPRECATED (v1.10) function: ungrab_pointer_for_device - Use clutter_input_device_ungrab() instead.
// Removes an existing grab of the pointer events for device @id_.
// <id_>: a device id
static void ungrab_pointer_for_device()(int id_) nothrow {
   clutter_ungrab_pointer_for_device(id_);
}


// VERSION: 1.10
// Convert from a ISO10646 character to a key symbol.
// 
// or, if there is no corresponding symbol, wc | 0x01000000
// RETURNS: the corresponding Clutter key symbol, if one exists.
// <wc>: a ISO10646 encoded character
static uint unicode_to_keysym()(uint wc) nothrow {
   return clutter_unicode_to_keysym(wc);
}

enum int union_ = 2269;
enum int uogonek = 1017;
enum int uparrow = 2300;
enum int upcaret = 2985;
enum int upleftcorner = 2540;
enum int uprightcorner = 2539;
enum int upshoe = 3011;
enum int upstile = 3027;
enum int uptack = 3022;
enum int uring = 505;

// DEPRECATED function: util_next_p2 - 1.2
// Calculates the nearest power of two, greater than or equal to @a.
// RETURNS: The nearest power of two, greater or equal to @a.
// <a>: Value to get the next power
static int util_next_p2()(int a) nothrow {
   return clutter_util_next_p2(a);
}

enum int utilde = 1021;
enum int v = 118;

// VERSION: 0.8.4
// Gets the #ClutterColor contained in @value.
// RETURNS: the color inside the passed #GValue
// <value>: a #GValue initialized to #CLUTTER_TYPE_COLOR
static Color* value_get_color(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
   return clutter_value_get_color(UpCast!(GObject2.Value*)(value));
}


// Unintrospectable function: value_get_fixed() / clutter_value_get_fixed()
// VERSION: 0.8
// DEPRECATED (v1.10) function: value_get_fixed - Use g_value_get_int() instead.
// Gets the fixed point value stored inside @value.
// RETURNS: the value inside the passed #GValue
// <value>: a #GValue initialized to %COGL_TYPE_FIXED
static Cogl.Fixed value_get_fixed(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
   return clutter_value_get_fixed(UpCast!(GObject2.Value*)(value));
}


// VERSION: 0.8
// Retrieves the list of floating point values stored inside
// the passed #GValue. @value must have been initialized with
// %CLUTTER_TYPE_SHADER_FLOAT.
// 
// The returned value is owned by the #GValue and should never
// be modified or freed.
// RETURNS: the pointer to a list of floating point values.
// <value>: a #GValue
// <length>: return location for the number of returned floating point values, or %NULL
static float* value_get_shader_float(AT0, AT1)(AT0 /*GObject2.Value*/ value, AT1 /*size_t*/ length) nothrow {
   return clutter_value_get_shader_float(UpCast!(GObject2.Value*)(value), UpCast!(size_t*)(length));
}


// VERSION: 0.8
// Retrieves the list of integer values stored inside the passed
// #GValue. @value must have been initialized with
// %CLUTTER_TYPE_SHADER_INT.
// 
// The returned value is owned by the #GValue and should never
// be modified or freed.
// RETURNS: the pointer to a list of integer values.
// <value>: a #GValue
// <length>: return location for the number of returned integer values, or %NULL
static int* value_get_shader_int(AT0, AT1)(AT0 /*GObject2.Value*/ value, AT1 /*size_t*/ length) nothrow {
   return clutter_value_get_shader_int(UpCast!(GObject2.Value*)(value), UpCast!(size_t*)(length));
}


// VERSION: 0.8
// Retrieves a matrix of floating point values stored inside
// the passed #GValue. @value must have been initialized with
// %CLUTTER_TYPE_SHADER_MATRIX.
// 
// of floating point values. The returned value is owned by the #GValue and
// should never be modified or freed.
// RETURNS: the pointer to a matrix
// <value>: a #GValue
// <length>: return location for the number of returned floating point values, or %NULL
static float* value_get_shader_matrix(AT0, AT1)(AT0 /*GObject2.Value*/ value, /*out*/ AT1 /*size_t*/ length) nothrow {
   return clutter_value_get_shader_matrix(UpCast!(GObject2.Value*)(value), UpCast!(size_t*)(length));
}


// VERSION: 0.8
// Gets the #ClutterUnits contained in @value.
// RETURNS: the units inside the passed #GValue
// <value>: a #GValue initialized to %CLUTTER_TYPE_UNITS
static Units* value_get_units(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
   return clutter_value_get_units(UpCast!(GObject2.Value*)(value));
}


// VERSION: 0.8.4
// Sets @value to @color.
// <value>: a #GValue initialized to #CLUTTER_TYPE_COLOR
// <color>: the color to set
static void value_set_color(AT0, AT1)(AT0 /*GObject2.Value*/ value, AT1 /*Color*/ color) nothrow {
   clutter_value_set_color(UpCast!(GObject2.Value*)(value), UpCast!(Color*)(color));
}


// Unintrospectable function: value_set_fixed() / clutter_value_set_fixed()
// VERSION: 0.8
// DEPRECATED (v1.10) function: value_set_fixed - Use g_value_set_int() instead.
// Sets @value to @fixed_.
// <value>: a #GValue initialized to %COGL_TYPE_FIXED
// <fixed_>: the fixed point value to set
static void value_set_fixed(AT0)(AT0 /*GObject2.Value*/ value, Cogl.Fixed fixed_) nothrow {
   clutter_value_set_fixed(UpCast!(GObject2.Value*)(value), fixed_);
}


// VERSION: 0.8
// Sets @floats as the contents of @value. The passed #GValue
// must have been initialized using %CLUTTER_TYPE_SHADER_FLOAT.
// <value>: a #GValue
// <size>: number of floating point values in @floats
// <floats>: an array of floating point values
static void value_set_shader_float(AT0, AT1)(AT0 /*GObject2.Value*/ value, int size, AT1 /*float*/ floats) nothrow {
   clutter_value_set_shader_float(UpCast!(GObject2.Value*)(value), size, UpCast!(float*)(floats));
}


// VERSION: 0.8
// Sets @ints as the contents of @value. The passed #GValue
// must have been initialized using %CLUTTER_TYPE_SHADER_INT.
// <value>: a #GValue
// <size>: number of integer values in @ints
// <ints>: an array of integer values
static void value_set_shader_int(AT0)(AT0 /*GObject2.Value*/ value, int size, int* ints) nothrow {
   clutter_value_set_shader_int(UpCast!(GObject2.Value*)(value), size, ints);
}


// VERSION: 0.8
// Sets @matrix as the contents of @value. The passed #GValue
// must have been initialized using %CLUTTER_TYPE_SHADER_MATRIX.
// <value>: a #GValue
// <size>: number of floating point values in @floats
// <matrix>: a matrix of floating point values
static void value_set_shader_matrix(AT0, AT1)(AT0 /*GObject2.Value*/ value, int size, AT1 /*float*/ matrix) nothrow {
   clutter_value_set_shader_matrix(UpCast!(GObject2.Value*)(value), size, UpCast!(float*)(matrix));
}


// VERSION: 0.8
// Sets @value to @units
// <value>: a #GValue initialized to %CLUTTER_TYPE_UNITS
// <units>: the units to set
static void value_set_units(AT0, AT1)(AT0 /*GObject2.Value*/ value, AT1 /*Units*/ units) nothrow {
   clutter_value_set_units(UpCast!(GObject2.Value*)(value), UpCast!(Units*)(units));
}

enum int variation = 2241;
enum int vertbar = 2552;
enum int vertconnector = 2214;
enum int voicedsound = 1246;
enum int vt = 2537;
enum int w = 119;
enum int wacute = 16785027;
enum int wcircumflex = 16777589;
enum int wdiaeresis = 16785029;
enum int wgrave = 16785025;
enum int x = 120;
enum int xabovedot = 16785035;
enum int y = 121;
enum int yacute = 253;
enum int ybelowdot = 16785141;
enum int ycircumflex = 16777591;
enum int ydiaeresis = 255;
enum int yen = 165;
enum int ygrave = 16785139;
enum int yhook = 16785143;
enum int ytilde = 16785145;
enum int z = 122;
enum int zabovedot = 447;
enum int zacute = 444;
enum int zcaron = 446;
enum int zerosubscript = 16785536;
enum int zerosuperior = 16785520;
enum int zstroke = 16777654;

//  --- mixin/Clutter__MODULE.d --->

// An overload of init() that works with D strings.
// Unlike the original, it does not need to modify the inputs.
// Returns a new string array (that can be assigned to the argv
// array in the caller).

string[] init()(string argv[]) {
   // init() wants 'argc' as 'int*'...
   c_int argc = cast(c_int)argv.length;
   auto c_argv = argvToC(argv);
   init(&argc, &c_argv);
   return argvFromC(argc, c_argv);
}


// <--- mixin/Clutter__MODULE.d ---

// C prototypes:

extern (C) {
Actor* /*new*/ clutter_actor_new() nothrow;
void clutter_actor_add_action(Actor* this_, Action* action) nothrow;
void clutter_actor_add_action_with_name(Actor* this_, char* name, Action* action) nothrow;
void clutter_actor_add_child(Actor* this_, Actor* child) nothrow;
void clutter_actor_add_constraint(Actor* this_, Constraint* constraint) nothrow;
void clutter_actor_add_constraint_with_name(Actor* this_, char* name, Constraint* constraint) nothrow;
void clutter_actor_add_effect(Actor* this_, Effect* effect) nothrow;
void clutter_actor_add_effect_with_name(Actor* this_, char* name, Effect* effect) nothrow;
void clutter_actor_allocate(Actor* this_, ActorBox* box, AllocationFlags flags) nothrow;
void clutter_actor_allocate_align_fill(Actor* this_, ActorBox* box, double x_align, double y_align, int x_fill, int y_fill, AllocationFlags flags) nothrow;
void clutter_actor_allocate_available_size(Actor* this_, float x, float y, float available_width, float available_height, AllocationFlags flags) nothrow;
void clutter_actor_allocate_preferred_size(Actor* this_, AllocationFlags flags) nothrow;
Animation* clutter_actor_animate(Actor* this_, c_ulong mode, uint duration, char* first_property_name, ...) nothrow;
Animation* clutter_actor_animate_with_alpha(Actor* this_, Alpha* alpha, char* first_property_name, ...) nothrow;
Animation* clutter_actor_animate_with_alphav(Actor* this_, Alpha* alpha, int n_properties, char* properties, GObject2.Value* values) nothrow;
Animation* clutter_actor_animate_with_timeline(Actor* this_, c_ulong mode, Timeline* timeline, char* first_property_name, ...) nothrow;
Animation* clutter_actor_animate_with_timelinev(Actor* this_, c_ulong mode, Timeline* timeline, int n_properties, char* properties, GObject2.Value* values) nothrow;
Animation* clutter_actor_animatev(Actor* this_, c_ulong mode, uint duration, int n_properties, char* properties, GObject2.Value* values) nothrow;
void clutter_actor_apply_relative_transform_to_point(Actor* this_, Actor* ancestor, Vertex* point, /*out*/ Vertex* vertex) nothrow;
void clutter_actor_apply_transform_to_point(Actor* this_, Vertex* point, /*out*/ Vertex* vertex) nothrow;
void clutter_actor_clear_actions(Actor* this_) nothrow;
void clutter_actor_clear_constraints(Actor* this_) nothrow;
void clutter_actor_clear_effects(Actor* this_) nothrow;
int clutter_actor_contains(Actor* this_, Actor* descendant) nothrow;
void clutter_actor_continue_paint(Actor* this_) nothrow;
Pango.Context* /*new*/ clutter_actor_create_pango_context(Actor* this_) nothrow;
Pango.Layout* /*new*/ clutter_actor_create_pango_layout(Actor* this_, char* text) nothrow;
void clutter_actor_destroy(Actor* this_) nothrow;
void clutter_actor_detach_animation(Actor* this_) nothrow;
int clutter_actor_event(Actor* this_, Event* event, int capture) nothrow;
void clutter_actor_get_abs_allocation_vertices(Actor* this_, /*out*/ Vertex verts) nothrow;
Atk.Object* clutter_actor_get_accessible(Actor* this_) nothrow;
Action* clutter_actor_get_action(Actor* this_, char* name) nothrow;
GLib2.List* /*new container*/ clutter_actor_get_actions(Actor* this_) nothrow;
void clutter_actor_get_allocation_box(Actor* this_, /*out*/ ActorBox* box) nothrow;
void clutter_actor_get_allocation_geometry(Actor* this_, /*out*/ Geometry* geom) nothrow;
void clutter_actor_get_allocation_vertices(Actor* this_, Actor* ancestor, /*out*/ Vertex verts) nothrow;
void clutter_actor_get_anchor_point(Actor* this_, /*out*/ float* anchor_x, /*out*/ float* anchor_y) nothrow;
Gravity clutter_actor_get_anchor_point_gravity(Actor* this_) nothrow;
Animation* clutter_actor_get_animation(Actor* this_) nothrow;
void clutter_actor_get_background_color(Actor* this_, /*out*/ Color* color) nothrow;
Actor* clutter_actor_get_child_at_index(Actor* this_, int index_) nothrow;
GLib2.List* /*new container*/ clutter_actor_get_children(Actor* this_) nothrow;
void clutter_actor_get_clip(Actor* this_, /*out*/ float* xoff=null, /*out*/ float* yoff=null, /*out*/ float* width=null, /*out*/ float* height=null) nothrow;
int clutter_actor_get_clip_to_allocation(Actor* this_) nothrow;
Constraint* clutter_actor_get_constraint(Actor* this_, char* name) nothrow;
GLib2.List* /*new container*/ clutter_actor_get_constraints(Actor* this_) nothrow;
float clutter_actor_get_depth(Actor* this_) nothrow;
Effect* clutter_actor_get_effect(Actor* this_, char* name) nothrow;
GLib2.List* /*new container*/ clutter_actor_get_effects(Actor* this_) nothrow;
Actor* clutter_actor_get_first_child(Actor* this_) nothrow;
int clutter_actor_get_fixed_position_set(Actor* this_) nothrow;
ActorFlags clutter_actor_get_flags(Actor* this_) nothrow;
void clutter_actor_get_geometry(Actor* this_, /*out*/ Geometry* geometry) nothrow;
uint clutter_actor_get_gid(Actor* this_) nothrow;
float clutter_actor_get_height(Actor* this_) nothrow;
Actor* clutter_actor_get_last_child(Actor* this_) nothrow;
LayoutManager* clutter_actor_get_layout_manager(Actor* this_) nothrow;
void clutter_actor_get_margin(Actor* this_, /*out*/ Margin* margin) nothrow;
float clutter_actor_get_margin_bottom(Actor* this_) nothrow;
float clutter_actor_get_margin_left(Actor* this_) nothrow;
float clutter_actor_get_margin_right(Actor* this_) nothrow;
float clutter_actor_get_margin_top(Actor* this_) nothrow;
int clutter_actor_get_n_children(Actor* this_) nothrow;
char* clutter_actor_get_name(Actor* this_) nothrow;
Actor* clutter_actor_get_next_sibling(Actor* this_) nothrow;
OffscreenRedirect clutter_actor_get_offscreen_redirect(Actor* this_) nothrow;
ubyte clutter_actor_get_opacity(Actor* this_) nothrow;
int clutter_actor_get_paint_box(Actor* this_, /*out*/ ActorBox* box) nothrow;
ubyte clutter_actor_get_paint_opacity(Actor* this_) nothrow;
int clutter_actor_get_paint_visibility(Actor* this_) nothrow;
PaintVolume* clutter_actor_get_paint_volume(Actor* this_) nothrow;
Pango.Context* clutter_actor_get_pango_context(Actor* this_) nothrow;
Actor* clutter_actor_get_parent(Actor* this_) nothrow;
void clutter_actor_get_position(Actor* this_, /*out*/ float* x=null, /*out*/ float* y=null) nothrow;
void clutter_actor_get_preferred_height(Actor* this_, float for_width, /*out*/ float* min_height_p=null, /*out*/ float* natural_height_p=null) nothrow;
void clutter_actor_get_preferred_size(Actor* this_, /*out*/ float* min_width_p=null, /*out*/ float* min_height_p=null, /*out*/ float* natural_width_p=null, /*out*/ float* natural_height_p=null) nothrow;
void clutter_actor_get_preferred_width(Actor* this_, float for_height, /*out*/ float* min_width_p=null, /*out*/ float* natural_width_p=null) nothrow;
Actor* clutter_actor_get_previous_sibling(Actor* this_) nothrow;
int clutter_actor_get_reactive(Actor* this_) nothrow;
RequestMode clutter_actor_get_request_mode(Actor* this_) nothrow;
double clutter_actor_get_rotation(Actor* this_, RotateAxis axis, /*out*/ float* x, /*out*/ float* y, /*out*/ float* z) nothrow;
void clutter_actor_get_scale(Actor* this_, /*out*/ double* scale_x=null, /*out*/ double* scale_y=null) nothrow;
void clutter_actor_get_scale_center(Actor* this_, /*out*/ float* center_x=null, /*out*/ float* center_y=null) nothrow;
Gravity clutter_actor_get_scale_gravity(Actor* this_) nothrow;
Shader* clutter_actor_get_shader(Actor* this_) nothrow;
void clutter_actor_get_size(Actor* this_, /*out*/ float* width=null, /*out*/ float* height=null) nothrow;
Clutter.Stage* clutter_actor_get_stage(Actor* this_) nothrow;
TextDirection clutter_actor_get_text_direction(Actor* this_) nothrow;
void clutter_actor_get_transformation_matrix(Actor* this_, /*out*/ Cogl.Matrix* matrix) nothrow;
PaintVolume* clutter_actor_get_transformed_paint_volume(Actor* this_, Actor* relative_to_ancestor) nothrow;
void clutter_actor_get_transformed_position(Actor* this_, /*out*/ float* x=null, /*out*/ float* y=null) nothrow;
void clutter_actor_get_transformed_size(Actor* this_, /*out*/ float* width=null, /*out*/ float* height=null) nothrow;
float clutter_actor_get_width(Actor* this_) nothrow;
float clutter_actor_get_x(Actor* this_) nothrow;
ActorAlign clutter_actor_get_x_align(Actor* this_) nothrow;
float clutter_actor_get_y(Actor* this_) nothrow;
ActorAlign clutter_actor_get_y_align(Actor* this_) nothrow;
Gravity clutter_actor_get_z_rotation_gravity(Actor* this_) nothrow;
void clutter_actor_grab_key_focus(Actor* this_) nothrow;
int clutter_actor_has_actions(Actor* this_) nothrow;
int clutter_actor_has_allocation(Actor* this_) nothrow;
int clutter_actor_has_clip(Actor* this_) nothrow;
int clutter_actor_has_constraints(Actor* this_) nothrow;
int clutter_actor_has_effects(Actor* this_) nothrow;
int clutter_actor_has_key_focus(Actor* this_) nothrow;
int clutter_actor_has_overlaps(Actor* this_) nothrow;
int clutter_actor_has_pointer(Actor* this_) nothrow;
void clutter_actor_hide(Actor* this_) nothrow;
void clutter_actor_hide_all(Actor* this_) nothrow;
void clutter_actor_insert_child_above(Actor* this_, Actor* child, Actor* sibling=null) nothrow;
void clutter_actor_insert_child_at_index(Actor* this_, Actor* child, int index_) nothrow;
void clutter_actor_insert_child_below(Actor* this_, Actor* child, Actor* sibling=null) nothrow;
int clutter_actor_is_in_clone_paint(Actor* this_) nothrow;
int clutter_actor_is_rotated(Actor* this_) nothrow;
int clutter_actor_is_scaled(Actor* this_) nothrow;
void clutter_actor_lower(Actor* this_, Actor* above=null) nothrow;
void clutter_actor_lower_bottom(Actor* this_) nothrow;
void clutter_actor_map(Actor* this_) nothrow;
void clutter_actor_move_anchor_point(Actor* this_, float anchor_x, float anchor_y) nothrow;
void clutter_actor_move_anchor_point_from_gravity(Actor* this_, Gravity gravity) nothrow;
void clutter_actor_move_by(Actor* this_, float dx, float dy) nothrow;
void clutter_actor_paint(Actor* this_) nothrow;
void clutter_actor_pop_internal(Actor* this_) nothrow;
void clutter_actor_push_internal(Actor* this_) nothrow;
void clutter_actor_queue_redraw(Actor* this_) nothrow;
void clutter_actor_queue_redraw_with_clip(Actor* this_, cairo.RectangleInt* clip=null) nothrow;
void clutter_actor_queue_relayout(Actor* this_) nothrow;
void clutter_actor_raise(Actor* this_, Actor* below=null) nothrow;
void clutter_actor_raise_top(Actor* this_) nothrow;
void clutter_actor_realize(Actor* this_) nothrow;
void clutter_actor_remove_action(Actor* this_, Action* action) nothrow;
void clutter_actor_remove_action_by_name(Actor* this_, char* name) nothrow;
void clutter_actor_remove_all_children(Actor* this_) nothrow;
void clutter_actor_remove_child(Actor* this_, Actor* child) nothrow;
void clutter_actor_remove_clip(Actor* this_) nothrow;
void clutter_actor_remove_constraint(Actor* this_, Constraint* constraint) nothrow;
void clutter_actor_remove_constraint_by_name(Actor* this_, char* name) nothrow;
void clutter_actor_remove_effect(Actor* this_, Effect* effect) nothrow;
void clutter_actor_remove_effect_by_name(Actor* this_, char* name) nothrow;
void clutter_actor_reparent(Actor* this_, Actor* new_parent) nothrow;
void clutter_actor_replace_child(Actor* this_, Actor* old_child, Actor* new_child) nothrow;
void clutter_actor_set_allocation(Actor* this_, ActorBox* box, AllocationFlags flags) nothrow;
void clutter_actor_set_anchor_point(Actor* this_, float anchor_x, float anchor_y) nothrow;
void clutter_actor_set_anchor_point_from_gravity(Actor* this_, Gravity gravity) nothrow;
void clutter_actor_set_background_color(Actor* this_, Color* color=null) nothrow;
void clutter_actor_set_child_above_sibling(Actor* this_, Actor* child, Actor* sibling=null) nothrow;
void clutter_actor_set_child_at_index(Actor* this_, Actor* child, int index_) nothrow;
void clutter_actor_set_child_below_sibling(Actor* this_, Actor* child, Actor* sibling=null) nothrow;
void clutter_actor_set_clip(Actor* this_, float xoff, float yoff, float width, float height) nothrow;
void clutter_actor_set_clip_to_allocation(Actor* this_, int clip_set) nothrow;
void clutter_actor_set_depth(Actor* this_, float depth) nothrow;
void clutter_actor_set_fixed_position_set(Actor* this_, int is_set) nothrow;
void clutter_actor_set_flags(Actor* this_, ActorFlags flags) nothrow;
void clutter_actor_set_geometry(Actor* this_, Geometry* geometry) nothrow;
void clutter_actor_set_height(Actor* this_, float height) nothrow;
void clutter_actor_set_layout_manager(Actor* this_, LayoutManager* manager=null) nothrow;
void clutter_actor_set_margin(Actor* this_, Margin* margin) nothrow;
void clutter_actor_set_margin_bottom(Actor* this_, float margin) nothrow;
void clutter_actor_set_margin_left(Actor* this_, float margin) nothrow;
void clutter_actor_set_margin_right(Actor* this_, float margin) nothrow;
void clutter_actor_set_margin_top(Actor* this_, float margin) nothrow;
void clutter_actor_set_name(Actor* this_, char* name) nothrow;
void clutter_actor_set_offscreen_redirect(Actor* this_, OffscreenRedirect redirect) nothrow;
void clutter_actor_set_opacity(Actor* this_, ubyte opacity) nothrow;
void clutter_actor_set_parent(Actor* this_, Actor* parent) nothrow;
void clutter_actor_set_position(Actor* this_, float x, float y) nothrow;
void clutter_actor_set_reactive(Actor* this_, int reactive) nothrow;
void clutter_actor_set_request_mode(Actor* this_, RequestMode mode) nothrow;
void clutter_actor_set_rotation(Actor* this_, RotateAxis axis, double angle, float x, float y, float z) nothrow;
void clutter_actor_set_scale(Actor* this_, double scale_x, double scale_y) nothrow;
void clutter_actor_set_scale_full(Actor* this_, double scale_x, double scale_y, float center_x, float center_y) nothrow;
void clutter_actor_set_scale_with_gravity(Actor* this_, double scale_x, double scale_y, Gravity gravity) nothrow;
int clutter_actor_set_shader(Actor* this_, Shader* shader=null) nothrow;
void clutter_actor_set_shader_param(Actor* this_, char* param, GObject2.Value* value) nothrow;
void clutter_actor_set_shader_param_float(Actor* this_, char* param, float value) nothrow;
void clutter_actor_set_shader_param_int(Actor* this_, char* param, int value) nothrow;
void clutter_actor_set_size(Actor* this_, float width, float height) nothrow;
void clutter_actor_set_text_direction(Actor* this_, TextDirection text_dir) nothrow;
void clutter_actor_set_width(Actor* this_, float width) nothrow;
void clutter_actor_set_x(Actor* this_, float x) nothrow;
void clutter_actor_set_x_align(Actor* this_, ActorAlign x_align) nothrow;
void clutter_actor_set_y(Actor* this_, float y) nothrow;
void clutter_actor_set_y_align(Actor* this_, ActorAlign y_align) nothrow;
void clutter_actor_set_z_rotation_from_gravity(Actor* this_, double angle, Gravity gravity) nothrow;
int clutter_actor_should_pick_paint(Actor* this_) nothrow;
void clutter_actor_show(Actor* this_) nothrow;
void clutter_actor_show_all(Actor* this_) nothrow;
int clutter_actor_transform_stage_point(Actor* this_, float x, float y, /*out*/ float* x_out, /*out*/ float* y_out) nothrow;
void clutter_actor_unmap(Actor* this_) nothrow;
void clutter_actor_unparent(Actor* this_) nothrow;
void clutter_actor_unrealize(Actor* this_) nothrow;
void clutter_actor_unset_flags(Actor* this_, ActorFlags flags) nothrow;
ActorBox* /*new*/ clutter_actor_box_new(float x_1, float y_1, float x_2, float y_2) nothrow;
void clutter_actor_box_clamp_to_pixel(ActorBox* this_) nothrow;
int clutter_actor_box_contains(ActorBox* this_, float x, float y) nothrow;
ActorBox* /*new*/ clutter_actor_box_copy(ActorBox* this_) nothrow;
int clutter_actor_box_equal(ActorBox* this_, ActorBox* box_b) nothrow;
void clutter_actor_box_free(ActorBox* this_) nothrow;
void clutter_actor_box_from_vertices(ActorBox* this_, Vertex verts) nothrow;
float clutter_actor_box_get_area(ActorBox* this_) nothrow;
float clutter_actor_box_get_height(ActorBox* this_) nothrow;
void clutter_actor_box_get_origin(ActorBox* this_, /*out*/ float* x=null, /*out*/ float* y=null) nothrow;
void clutter_actor_box_get_size(ActorBox* this_, /*out*/ float* width=null, /*out*/ float* height=null) nothrow;
float clutter_actor_box_get_width(ActorBox* this_) nothrow;
float clutter_actor_box_get_x(ActorBox* this_) nothrow;
float clutter_actor_box_get_y(ActorBox* this_) nothrow;
void clutter_actor_box_interpolate(ActorBox* this_, ActorBox* final_, double progress, /*out*/ ActorBox* result) nothrow;
void clutter_actor_box_set_origin(ActorBox* this_, float x, float y) nothrow;
void clutter_actor_box_set_size(ActorBox* this_, float width, float height) nothrow;
void clutter_actor_box_union(ActorBox* this_, ActorBox* b, /*out*/ ActorBox* result) nothrow;
void clutter_actor_iter_init(ActorIter* this_, Actor* root) nothrow;
int clutter_actor_iter_next(ActorIter* this_, /*out*/ Actor** child) nothrow;
int clutter_actor_iter_prev(ActorIter* this_, Actor** child) nothrow;
void clutter_actor_iter_remove(ActorIter* this_) nothrow;
Actor* clutter_actor_meta_get_actor(ActorMeta* this_) nothrow;
int clutter_actor_meta_get_enabled(ActorMeta* this_) nothrow;
char* clutter_actor_meta_get_name(ActorMeta* this_) nothrow;
void clutter_actor_meta_set_enabled(ActorMeta* this_, int is_enabled) nothrow;
void clutter_actor_meta_set_name(ActorMeta* this_, char* name) nothrow;
AlignConstraint* clutter_align_constraint_new(Actor* source, AlignAxis axis, float factor) nothrow;
AlignAxis clutter_align_constraint_get_align_axis(AlignConstraint* this_) nothrow;
float clutter_align_constraint_get_factor(AlignConstraint* this_) nothrow;
Actor* clutter_align_constraint_get_source(AlignConstraint* this_) nothrow;
void clutter_align_constraint_set_align_axis(AlignConstraint* this_, AlignAxis axis) nothrow;
void clutter_align_constraint_set_factor(AlignConstraint* this_, float factor) nothrow;
void clutter_align_constraint_set_source(AlignConstraint* this_, Actor* source=null) nothrow;
Alpha* clutter_alpha_new() nothrow;
Alpha* clutter_alpha_new_full(Timeline* timeline, c_ulong mode) nothrow;
Alpha* clutter_alpha_new_with_func(Timeline* timeline, AlphaFunc func, void* data, GLib2.DestroyNotify destroy) nothrow;
c_ulong clutter_alpha_register_closure(GObject2.Closure* closure) nothrow;
c_ulong clutter_alpha_register_func(AlphaFunc func, void* data) nothrow;
double clutter_alpha_get_alpha(Alpha* this_) nothrow;
c_ulong clutter_alpha_get_mode(Alpha* this_) nothrow;
Timeline* clutter_alpha_get_timeline(Alpha* this_) nothrow;
void clutter_alpha_set_closure(Alpha* this_, GObject2.Closure* closure) nothrow;
void clutter_alpha_set_func(Alpha* this_, AlphaFunc func, void* data, GLib2.DestroyNotify destroy) nothrow;
void clutter_alpha_set_mode(Alpha* this_, c_ulong mode) nothrow;
void clutter_alpha_set_timeline(Alpha* this_, Timeline* timeline) nothrow;
int clutter_animatable_animate_property(Animatable* this_, Animation* animation, char* property_name, GObject2.Value* initial_value, GObject2.Value* final_value, double progress, GObject2.Value* value) nothrow;
GObject2.ParamSpec* clutter_animatable_find_property(Animatable* this_, char* property_name) nothrow;
void clutter_animatable_get_initial_state(Animatable* this_, char* property_name, GObject2.Value* value) nothrow;
int clutter_animatable_interpolate_value(Animatable* this_, char* property_name, Interval* interval, double progress, /*out*/ GObject2.Value* value) nothrow;
void clutter_animatable_set_final_state(Animatable* this_, char* property_name, GObject2.Value* value) nothrow;
Animation* /*new*/ clutter_animation_new() nothrow;
Animation* clutter_animation_bind(Animation* this_, char* property_name, GObject2.Value* final_) nothrow;
Animation* clutter_animation_bind_interval(Animation* this_, char* property_name, Interval* interval) nothrow;
void clutter_animation_completed(Animation* this_) nothrow;
Alpha* clutter_animation_get_alpha(Animation* this_) nothrow;
uint clutter_animation_get_duration(Animation* this_) nothrow;
Interval* clutter_animation_get_interval(Animation* this_, char* property_name) nothrow;
int clutter_animation_get_loop(Animation* this_) nothrow;
c_ulong clutter_animation_get_mode(Animation* this_) nothrow;
GObject2.Object* clutter_animation_get_object(Animation* this_) nothrow;
Timeline* clutter_animation_get_timeline(Animation* this_) nothrow;
int clutter_animation_has_property(Animation* this_, char* property_name) nothrow;
void clutter_animation_set_alpha(Animation* this_, Alpha* alpha) nothrow;
void clutter_animation_set_duration(Animation* this_, uint msecs) nothrow;
void clutter_animation_set_loop(Animation* this_, int loop) nothrow;
void clutter_animation_set_mode(Animation* this_, c_ulong mode) nothrow;
void clutter_animation_set_object(Animation* this_, GObject2.Object* object) nothrow;
void clutter_animation_set_timeline(Animation* this_, Timeline* timeline) nothrow;
void clutter_animation_unbind_property(Animation* this_, char* property_name) nothrow;
Animation* clutter_animation_update(Animation* this_, char* property_name, GObject2.Value* final_) nothrow;
void clutter_animation_update_interval(Animation* this_, char* property_name, Interval* interval) nothrow;
Animator* /*new*/ clutter_animator_new() nothrow;
int clutter_animator_compute_value(Animator* this_, GObject2.Object* object, char* property_name, double progress, GObject2.Value* value) nothrow;
uint clutter_animator_get_duration(Animator* this_) nothrow;
GLib2.List* /*new container*/ clutter_animator_get_keys(Animator* this_, GObject2.Object* object, char* property_name, double progress) nothrow;
Timeline* clutter_animator_get_timeline(Animator* this_) nothrow;
int clutter_animator_property_get_ease_in(Animator* this_, GObject2.Object* object, char* property_name) nothrow;
Interpolation clutter_animator_property_get_interpolation(Animator* this_, GObject2.Object* object, char* property_name) nothrow;
void clutter_animator_property_set_ease_in(Animator* this_, GObject2.Object* object, char* property_name, int ease_in) nothrow;
void clutter_animator_property_set_interpolation(Animator* this_, GObject2.Object* object, char* property_name, Interpolation interpolation) nothrow;
void clutter_animator_remove_key(Animator* this_, GObject2.Object* object, char* property_name, double progress) nothrow;
void clutter_animator_set(Animator* this_, void* first_object, char* first_property_name, uint first_mode, double first_progress, ...) nothrow;
void clutter_animator_set_duration(Animator* this_, uint duration) nothrow;
Animator* clutter_animator_set_key(Animator* this_, GObject2.Object* object, char* property_name, uint mode, double progress, GObject2.Value* value) nothrow;
void clutter_animator_set_timeline(Animator* this_, Timeline* timeline) nothrow;
Timeline* clutter_animator_start(Animator* this_) nothrow;
c_ulong clutter_animator_key_get_mode(AnimatorKey* this_) nothrow;
GObject2.Object* clutter_animator_key_get_object(AnimatorKey* this_) nothrow;
double clutter_animator_key_get_progress(AnimatorKey* this_) nothrow;
char* clutter_animator_key_get_property_name(AnimatorKey* this_) nothrow;
Type clutter_animator_key_get_property_type(AnimatorKey* this_) nothrow;
int clutter_animator_key_get_value(AnimatorKey* this_, GObject2.Value* value) nothrow;
uint clutter_backend_get_double_click_distance(Backend* this_) nothrow;
uint clutter_backend_get_double_click_time(Backend* this_) nothrow;
char* clutter_backend_get_font_name(Backend* this_) nothrow;
cairo.FontOptions* clutter_backend_get_font_options(Backend* this_) nothrow;
double clutter_backend_get_resolution(Backend* this_) nothrow;
void clutter_backend_set_double_click_distance(Backend* this_, uint distance) nothrow;
void clutter_backend_set_double_click_time(Backend* this_, uint msec) nothrow;
void clutter_backend_set_font_name(Backend* this_, char* font_name) nothrow;
void clutter_backend_set_font_options(Backend* this_, cairo.FontOptions* options) nothrow;
void clutter_backend_set_resolution(Backend* this_, double dpi) nothrow;
void clutter_behaviour_actors_foreach(Behaviour* this_, BehaviourForeachFunc func, void* data) nothrow;
void clutter_behaviour_apply(Behaviour* this_, Actor* actor) nothrow;
GLib2.SList* /*new container*/ clutter_behaviour_get_actors(Behaviour* this_) nothrow;
Alpha* clutter_behaviour_get_alpha(Behaviour* this_) nothrow;
int clutter_behaviour_get_n_actors(Behaviour* this_) nothrow;
Actor* clutter_behaviour_get_nth_actor(Behaviour* this_, int index_) nothrow;
int clutter_behaviour_is_applied(Behaviour* this_, Actor* actor) nothrow;
void clutter_behaviour_remove(Behaviour* this_, Actor* actor) nothrow;
void clutter_behaviour_remove_all(Behaviour* this_) nothrow;
void clutter_behaviour_set_alpha(Behaviour* this_, Alpha* alpha) nothrow;
BehaviourDepth* /*new*/ clutter_behaviour_depth_new(Alpha* alpha, int depth_start, int depth_end) nothrow;
void clutter_behaviour_depth_get_bounds(BehaviourDepth* this_, /*out*/ int* depth_start, /*out*/ int* depth_end) nothrow;
void clutter_behaviour_depth_set_bounds(BehaviourDepth* this_, int depth_start, int depth_end) nothrow;
BehaviourEllipse* /*new*/ clutter_behaviour_ellipse_new(Alpha* alpha, int x, int y, int width, int height, RotateDirection direction, double start, double end) nothrow;
double clutter_behaviour_ellipse_get_angle_end(BehaviourEllipse* this_) nothrow;
double clutter_behaviour_ellipse_get_angle_start(BehaviourEllipse* this_) nothrow;
double clutter_behaviour_ellipse_get_angle_tilt(BehaviourEllipse* this_, RotateAxis axis) nothrow;
void clutter_behaviour_ellipse_get_center(BehaviourEllipse* this_, /*out*/ int* x, /*out*/ int* y) nothrow;
RotateDirection clutter_behaviour_ellipse_get_direction(BehaviourEllipse* this_) nothrow;
int clutter_behaviour_ellipse_get_height(BehaviourEllipse* this_) nothrow;
void clutter_behaviour_ellipse_get_tilt(BehaviourEllipse* this_, /*out*/ double* angle_tilt_x, /*out*/ double* angle_tilt_y, /*out*/ double* angle_tilt_z) nothrow;
int clutter_behaviour_ellipse_get_width(BehaviourEllipse* this_) nothrow;
void clutter_behaviour_ellipse_set_angle_end(BehaviourEllipse* this_, double angle_end) nothrow;
void clutter_behaviour_ellipse_set_angle_start(BehaviourEllipse* this_, double angle_start) nothrow;
void clutter_behaviour_ellipse_set_angle_tilt(BehaviourEllipse* this_, RotateAxis axis, double angle_tilt) nothrow;
void clutter_behaviour_ellipse_set_center(BehaviourEllipse* this_, int x, int y) nothrow;
void clutter_behaviour_ellipse_set_direction(BehaviourEllipse* this_, RotateDirection direction) nothrow;
void clutter_behaviour_ellipse_set_height(BehaviourEllipse* this_, int height) nothrow;
void clutter_behaviour_ellipse_set_tilt(BehaviourEllipse* this_, double angle_tilt_x, double angle_tilt_y, double angle_tilt_z) nothrow;
void clutter_behaviour_ellipse_set_width(BehaviourEllipse* this_, int width) nothrow;
BehaviourOpacity* /*new*/ clutter_behaviour_opacity_new(Alpha* alpha, ubyte opacity_start, ubyte opacity_end) nothrow;
void clutter_behaviour_opacity_get_bounds(BehaviourOpacity* this_, /*out*/ ubyte* opacity_start, /*out*/ ubyte* opacity_end) nothrow;
void clutter_behaviour_opacity_set_bounds(BehaviourOpacity* this_, ubyte opacity_start, ubyte opacity_end) nothrow;
BehaviourPath* /*new*/ clutter_behaviour_path_new(Alpha* alpha, Path* path) nothrow;
BehaviourPath* /*new*/ clutter_behaviour_path_new_with_description(Alpha* alpha, char* desc) nothrow;
BehaviourPath* /*new*/ clutter_behaviour_path_new_with_knots(Alpha* alpha, Knot* knots, uint n_knots) nothrow;
Path* clutter_behaviour_path_get_path(BehaviourPath* this_) nothrow;
void clutter_behaviour_path_set_path(BehaviourPath* this_, Path* path) nothrow;
BehaviourRotate* /*new*/ clutter_behaviour_rotate_new(Alpha* alpha, RotateAxis axis, RotateDirection direction, double angle_start, double angle_end) nothrow;
RotateAxis clutter_behaviour_rotate_get_axis(BehaviourRotate* this_) nothrow;
void clutter_behaviour_rotate_get_bounds(BehaviourRotate* this_, /*out*/ double* angle_start, /*out*/ double* angle_end) nothrow;
void clutter_behaviour_rotate_get_center(BehaviourRotate* this_, /*out*/ int* x, /*out*/ int* y, /*out*/ int* z) nothrow;
RotateDirection clutter_behaviour_rotate_get_direction(BehaviourRotate* this_) nothrow;
void clutter_behaviour_rotate_set_axis(BehaviourRotate* this_, RotateAxis axis) nothrow;
void clutter_behaviour_rotate_set_bounds(BehaviourRotate* this_, double angle_start, double angle_end) nothrow;
void clutter_behaviour_rotate_set_center(BehaviourRotate* this_, int x, int y, int z) nothrow;
void clutter_behaviour_rotate_set_direction(BehaviourRotate* this_, RotateDirection direction) nothrow;
BehaviourScale* /*new*/ clutter_behaviour_scale_new(Alpha* alpha, double x_scale_start, double y_scale_start, double x_scale_end, double y_scale_end) nothrow;
void clutter_behaviour_scale_get_bounds(BehaviourScale* this_, /*out*/ double* x_scale_start, /*out*/ double* y_scale_start, /*out*/ double* x_scale_end, /*out*/ double* y_scale_end) nothrow;
void clutter_behaviour_scale_set_bounds(BehaviourScale* this_, double x_scale_start, double y_scale_start, double x_scale_end, double y_scale_end) nothrow;
BinLayout* clutter_bin_layout_new(BinAlignment x_align, BinAlignment y_align) nothrow;
void clutter_bin_layout_add(BinLayout* this_, Actor* child, BinAlignment x_align, BinAlignment y_align) nothrow;
void clutter_bin_layout_get_alignment(BinLayout* this_, Actor* child=null, /*out*/ BinAlignment* x_align=null, /*out*/ BinAlignment* y_align=null) nothrow;
void clutter_bin_layout_set_alignment(BinLayout* this_, Actor* child, BinAlignment x_align, BinAlignment y_align) nothrow;
BindConstraint* clutter_bind_constraint_new(Actor* source, BindCoordinate coordinate, float offset) nothrow;
BindCoordinate clutter_bind_constraint_get_coordinate(BindConstraint* this_) nothrow;
float clutter_bind_constraint_get_offset(BindConstraint* this_) nothrow;
Actor* clutter_bind_constraint_get_source(BindConstraint* this_) nothrow;
void clutter_bind_constraint_set_coordinate(BindConstraint* this_, BindCoordinate coordinate) nothrow;
void clutter_bind_constraint_set_offset(BindConstraint* this_, float offset) nothrow;
void clutter_bind_constraint_set_source(BindConstraint* this_, Actor* source=null) nothrow;
BindingPool* /*new*/ clutter_binding_pool_new(char* name) nothrow;
BindingPool* clutter_binding_pool_find(char* name) nothrow;
BindingPool* clutter_binding_pool_get_for_class(void* klass) nothrow;
int clutter_binding_pool_activate(BindingPool* this_, uint key_val, ModifierType modifiers, GObject2.Object* gobject) nothrow;
void clutter_binding_pool_block_action(BindingPool* this_, char* action_name) nothrow;
char* clutter_binding_pool_find_action(BindingPool* this_, uint key_val, ModifierType modifiers) nothrow;
void clutter_binding_pool_install_action(BindingPool* this_, char* action_name, uint key_val, ModifierType modifiers, GObject2.Callback callback, void* data, GLib2.DestroyNotify notify) nothrow;
void clutter_binding_pool_install_closure(BindingPool* this_, char* action_name, uint key_val, ModifierType modifiers, GObject2.Closure* closure) nothrow;
void clutter_binding_pool_override_action(BindingPool* this_, uint key_val, ModifierType modifiers, GObject2.Callback callback, void* data, GLib2.DestroyNotify notify) nothrow;
void clutter_binding_pool_override_closure(BindingPool* this_, uint key_val, ModifierType modifiers, GObject2.Closure* closure) nothrow;
void clutter_binding_pool_remove_action(BindingPool* this_, uint key_val, ModifierType modifiers) nothrow;
void clutter_binding_pool_unblock_action(BindingPool* this_, char* action_name) nothrow;
BlurEffect* clutter_blur_effect_new() nothrow;
Box* clutter_box_new(LayoutManager* manager) nothrow;
void clutter_box_get_color(Box* this_, /*out*/ Color* color) nothrow;
LayoutManager* clutter_box_get_layout_manager(Box* this_) nothrow;
void clutter_box_pack(Box* this_, Actor* actor, char* first_property, ...) nothrow;
void clutter_box_pack_after(Box* this_, Actor* actor, Actor* sibling, char* first_property, ...) nothrow;
void clutter_box_pack_at(Box* this_, Actor* actor, int position, char* first_property, ...) nothrow;
void clutter_box_pack_before(Box* this_, Actor* actor, Actor* sibling, char* first_property, ...) nothrow;
void clutter_box_packv(Box* this_, Actor* actor, uint n_properties, char* properties, GObject2.Value* values) nothrow;
void clutter_box_set_color(Box* this_, Color* color=null) nothrow;
void clutter_box_set_layout_manager(Box* this_, LayoutManager* manager) nothrow;
BoxLayout* clutter_box_layout_new() nothrow;
void clutter_box_layout_get_alignment(BoxLayout* this_, Actor* actor, /*out*/ BoxAlignment* x_align, /*out*/ BoxAlignment* y_align) nothrow;
uint clutter_box_layout_get_easing_duration(BoxLayout* this_) nothrow;
c_ulong clutter_box_layout_get_easing_mode(BoxLayout* this_) nothrow;
int clutter_box_layout_get_expand(BoxLayout* this_, Actor* actor) nothrow;
void clutter_box_layout_get_fill(BoxLayout* this_, Actor* actor, /*out*/ int* x_fill, /*out*/ int* y_fill) nothrow;
int clutter_box_layout_get_homogeneous(BoxLayout* this_) nothrow;
int clutter_box_layout_get_pack_start(BoxLayout* this_) nothrow;
uint clutter_box_layout_get_spacing(BoxLayout* this_) nothrow;
int clutter_box_layout_get_use_animations(BoxLayout* this_) nothrow;
int clutter_box_layout_get_vertical(BoxLayout* this_) nothrow;
void clutter_box_layout_pack(BoxLayout* this_, Actor* actor, int expand, int x_fill, int y_fill, BoxAlignment x_align, BoxAlignment y_align) nothrow;
void clutter_box_layout_set_alignment(BoxLayout* this_, Actor* actor, BoxAlignment x_align, BoxAlignment y_align) nothrow;
void clutter_box_layout_set_easing_duration(BoxLayout* this_, uint msecs) nothrow;
void clutter_box_layout_set_easing_mode(BoxLayout* this_, c_ulong mode) nothrow;
void clutter_box_layout_set_expand(BoxLayout* this_, Actor* actor, int expand) nothrow;
void clutter_box_layout_set_fill(BoxLayout* this_, Actor* actor, int x_fill, int y_fill) nothrow;
void clutter_box_layout_set_homogeneous(BoxLayout* this_, int homogeneous) nothrow;
void clutter_box_layout_set_pack_start(BoxLayout* this_, int pack_start) nothrow;
void clutter_box_layout_set_spacing(BoxLayout* this_, uint spacing) nothrow;
void clutter_box_layout_set_use_animations(BoxLayout* this_, int animate) nothrow;
void clutter_box_layout_set_vertical(BoxLayout* this_, int vertical) nothrow;
CairoTexture* clutter_cairo_texture_new(uint width, uint height) nothrow;
void clutter_cairo_texture_clear(CairoTexture* this_) nothrow;
cairo.Context* /*new*/ clutter_cairo_texture_create(CairoTexture* this_) nothrow;
cairo.Context* /*new*/ clutter_cairo_texture_create_region(CairoTexture* this_, int x_offset, int y_offset, int width, int height) nothrow;
int clutter_cairo_texture_get_auto_resize(CairoTexture* this_) nothrow;
void clutter_cairo_texture_get_surface_size(CairoTexture* this_, /*out*/ uint* width, /*out*/ uint* height) nothrow;
void clutter_cairo_texture_invalidate(CairoTexture* this_) nothrow;
void clutter_cairo_texture_invalidate_rectangle(CairoTexture* this_, cairo.RectangleInt* rect=null) nothrow;
void clutter_cairo_texture_set_auto_resize(CairoTexture* this_, int value) nothrow;
void clutter_cairo_texture_set_surface_size(CairoTexture* this_, uint width, uint height) nothrow;
Actor* clutter_child_meta_get_actor(ChildMeta* this_) nothrow;
Container* clutter_child_meta_get_container(ChildMeta* this_) nothrow;
ClickAction* clutter_click_action_new() nothrow;
uint clutter_click_action_get_button(ClickAction* this_) nothrow;
void clutter_click_action_get_coords(ClickAction* this_, /*out*/ float* press_x, /*out*/ float* press_y) nothrow;
ModifierType clutter_click_action_get_state(ClickAction* this_) nothrow;
void clutter_click_action_release(ClickAction* this_) nothrow;
Clone* clutter_clone_new(Actor* source) nothrow;
Actor* clutter_clone_get_source(Clone* this_) nothrow;
void clutter_clone_set_source(Clone* this_, Actor* source=null) nothrow;
Color* /*new*/ clutter_color_new(ubyte red, ubyte green, ubyte blue, ubyte alpha) nothrow;
void clutter_color_add(Color* this_, Color* b, /*out*/ Color* result) nothrow;
Color* /*new*/ clutter_color_copy(Color* this_) nothrow;
void clutter_color_darken(Color* this_, /*out*/ Color* result) nothrow;
void clutter_color_free(Color* this_) nothrow;
void clutter_color_from_hls(Color* this_, float hue, float luminance, float saturation) nothrow;
void clutter_color_from_pixel(Color* this_, uint pixel) nothrow;
int clutter_color_from_string(Color* this_, char* str) nothrow;
void clutter_color_interpolate(Color* this_, Color* final_, double progress, /*out*/ Color* result) nothrow;
void clutter_color_lighten(Color* this_, /*out*/ Color* result) nothrow;
void clutter_color_shade(Color* this_, double factor, /*out*/ Color* result) nothrow;
void clutter_color_subtract(Color* this_, Color* b, /*out*/ Color* result) nothrow;
void clutter_color_to_hls(Color* this_, /*out*/ float* hue, /*out*/ float* luminance, /*out*/ float* saturation) nothrow;
uint clutter_color_to_pixel(Color* this_) nothrow;
char* /*new*/ clutter_color_to_string(Color* this_) nothrow;
int clutter_color_equal(const(void)* v1, const(void)* v2) nothrow;
Color* clutter_color_get_static(StaticColor color) nothrow;
uint clutter_color_hash(const(void)* v) nothrow;
ColorizeEffect* clutter_colorize_effect_new(Color* tint) nothrow;
void clutter_colorize_effect_get_tint(ColorizeEffect* this_, /*out*/ Color* tint) nothrow;
void clutter_colorize_effect_set_tint(ColorizeEffect* this_, Color* tint) nothrow;
GObject2.ParamSpec* clutter_container_class_find_child_property(GObject2.ObjectClass* klass, char* property_name) nothrow;
GObject2.ParamSpec** /*new*/ clutter_container_class_list_child_properties(GObject2.ObjectClass* klass, /*out*/ uint* n_properties) nothrow;
void clutter_container_add(Container* this_, Actor* first_actor, ...) nothrow;
void clutter_container_add_actor(Container* this_, Actor* actor) nothrow;
void clutter_container_add_valist(Container* this_, Actor* first_actor, va_list var_args) nothrow;
void clutter_container_child_get(Container* this_, Actor* actor, char* first_prop, ...) nothrow;
void clutter_container_child_get_property(Container* this_, Actor* child, char* property, GObject2.Value* value) nothrow;
void clutter_container_child_notify(Container* this_, Actor* child, GObject2.ParamSpec* pspec) nothrow;
void clutter_container_child_set(Container* this_, Actor* actor, char* first_prop, ...) nothrow;
void clutter_container_child_set_property(Container* this_, Actor* child, char* property, GObject2.Value* value) nothrow;
void clutter_container_create_child_meta(Container* this_, Actor* actor) nothrow;
void clutter_container_destroy_child_meta(Container* this_, Actor* actor) nothrow;
Actor* clutter_container_find_child_by_name(Container* this_, char* child_name) nothrow;
void clutter_container_foreach(Container* this_, Callback callback, void* user_data) nothrow;
void clutter_container_foreach_with_internals(Container* this_, Callback callback, void* user_data) nothrow;
ChildMeta* clutter_container_get_child_meta(Container* this_, Actor* actor) nothrow;
GLib2.List* /*new container*/ clutter_container_get_children(Container* this_) nothrow;
void clutter_container_lower_child(Container* this_, Actor* actor, Actor* sibling=null) nothrow;
void clutter_container_raise_child(Container* this_, Actor* actor, Actor* sibling=null) nothrow;
void clutter_container_remove(Container* this_, Actor* first_actor, ...) nothrow;
void clutter_container_remove_actor(Container* this_, Actor* actor) nothrow;
void clutter_container_remove_valist(Container* this_, Actor* first_actor, va_list var_args) nothrow;
void clutter_container_sort_depth_order(Container* this_) nothrow;
Cogl.Handle clutter_deform_effect_get_back_material(DeformEffect* this_) nothrow;
void clutter_deform_effect_get_n_tiles(DeformEffect* this_, /*out*/ uint* x_tiles, /*out*/ uint* y_tiles) nothrow;
void clutter_deform_effect_invalidate(DeformEffect* this_) nothrow;
void clutter_deform_effect_set_back_material(DeformEffect* this_, Cogl.Handle material=null) nothrow;
void clutter_deform_effect_set_n_tiles(DeformEffect* this_, uint x_tiles, uint y_tiles) nothrow;
DesaturateEffect* clutter_desaturate_effect_new(double factor) nothrow;
double clutter_desaturate_effect_get_factor(DesaturateEffect* this_) nothrow;
void clutter_desaturate_effect_set_factor(DesaturateEffect* this_, double factor) nothrow;
DeviceManager* clutter_device_manager_get_default() nothrow;
InputDevice* clutter_device_manager_get_core_device(DeviceManager* this_, InputDeviceType device_type) nothrow;
InputDevice* clutter_device_manager_get_device(DeviceManager* this_, int device_id) nothrow;
GLib2.SList* /*new container*/ clutter_device_manager_list_devices(DeviceManager* this_) nothrow;
GLib2.SList* clutter_device_manager_peek_devices(DeviceManager* this_) nothrow;
DragAction* clutter_drag_action_new() nothrow;
DragAxis clutter_drag_action_get_drag_axis(DragAction* this_) nothrow;
Actor* clutter_drag_action_get_drag_handle(DragAction* this_) nothrow;
void clutter_drag_action_get_drag_threshold(DragAction* this_, /*out*/ uint* x_threshold, /*out*/ uint* y_threshold) nothrow;
void clutter_drag_action_get_motion_coords(DragAction* this_, /*out*/ float* motion_x, /*out*/ float* motion_y) nothrow;
void clutter_drag_action_get_press_coords(DragAction* this_, /*out*/ float* press_x, /*out*/ float* press_y) nothrow;
void clutter_drag_action_set_drag_axis(DragAction* this_, DragAxis axis) nothrow;
void clutter_drag_action_set_drag_handle(DragAction* this_, Actor* handle=null) nothrow;
void clutter_drag_action_set_drag_threshold(DragAction* this_, int x_threshold, int y_threshold) nothrow;
DropAction* clutter_drop_action_new() nothrow;
void clutter_effect_queue_repaint(Effect* this_) nothrow;
Event* /*new*/ clutter_event_new(EventType type) nothrow;
Event* /*new*/ clutter_event_copy(Event* this_) nothrow;
void clutter_event_free(Event* this_) nothrow;
double* clutter_event_get_axes(Event* this_, /*out*/ uint* n_axes) nothrow;
uint clutter_event_get_button(Event* this_) nothrow;
uint clutter_event_get_click_count(Event* this_) nothrow;
void clutter_event_get_coords(Event* this_, /*out*/ float* x, /*out*/ float* y) nothrow;
InputDevice* clutter_event_get_device(Event* this_) nothrow;
int clutter_event_get_device_id(Event* this_) nothrow;
InputDeviceType clutter_event_get_device_type(Event* this_) nothrow;
EventFlags clutter_event_get_flags(Event* this_) nothrow;
ushort clutter_event_get_key_code(Event* this_) nothrow;
uint clutter_event_get_key_symbol(Event* this_) nothrow;
uint clutter_event_get_key_unicode(Event* this_) nothrow;
Actor* clutter_event_get_related(Event* this_) nothrow;
ScrollDirection clutter_event_get_scroll_direction(Event* this_) nothrow;
Actor* clutter_event_get_source(Event* this_) nothrow;
InputDevice* clutter_event_get_source_device(Event* this_) nothrow;
Stage* clutter_event_get_stage(Event* this_) nothrow;
ModifierType clutter_event_get_state(Event* this_) nothrow;
uint clutter_event_get_time(Event* this_) nothrow;
void clutter_event_put(Event* this_) nothrow;
void clutter_event_set_button(Event* this_, uint button) nothrow;
void clutter_event_set_coords(Event* this_, float x, float y) nothrow;
void clutter_event_set_device(Event* this_, InputDevice* device=null) nothrow;
void clutter_event_set_flags(Event* this_, EventFlags flags) nothrow;
void clutter_event_set_key_code(Event* this_, ushort key_code) nothrow;
void clutter_event_set_key_symbol(Event* this_, uint key_sym) nothrow;
void clutter_event_set_key_unicode(Event* this_, uint key_unicode) nothrow;
void clutter_event_set_related(Event* this_, Actor* actor=null) nothrow;
void clutter_event_set_scroll_direction(Event* this_, ScrollDirection direction) nothrow;
void clutter_event_set_source(Event* this_, Actor* actor=null) nothrow;
void clutter_event_set_source_device(Event* this_, InputDevice* device=null) nothrow;
void clutter_event_set_stage(Event* this_, Stage* stage=null) nothrow;
void clutter_event_set_state(Event* this_, ModifierType state) nothrow;
void clutter_event_set_time(Event* this_, uint time_) nothrow;
EventType clutter_event_type(Event* this_) nothrow;
Event* /*new*/ clutter_event_get() nothrow;
Event* clutter_event_peek() nothrow;
FixedLayout* clutter_fixed_layout_new() nothrow;
FlowLayout* clutter_flow_layout_new(FlowOrientation orientation) nothrow;
float clutter_flow_layout_get_column_spacing(FlowLayout* this_) nothrow;
void clutter_flow_layout_get_column_width(FlowLayout* this_, /*out*/ float* min_width, /*out*/ float* max_width) nothrow;
int clutter_flow_layout_get_homogeneous(FlowLayout* this_) nothrow;
FlowOrientation clutter_flow_layout_get_orientation(FlowLayout* this_) nothrow;
void clutter_flow_layout_get_row_height(FlowLayout* this_, /*out*/ float* min_height, /*out*/ float* max_height) nothrow;
float clutter_flow_layout_get_row_spacing(FlowLayout* this_) nothrow;
void clutter_flow_layout_set_column_spacing(FlowLayout* this_, float spacing) nothrow;
void clutter_flow_layout_set_column_width(FlowLayout* this_, float min_width, float max_width) nothrow;
void clutter_flow_layout_set_homogeneous(FlowLayout* this_, int homogeneous) nothrow;
void clutter_flow_layout_set_orientation(FlowLayout* this_, FlowOrientation orientation) nothrow;
void clutter_flow_layout_set_row_height(FlowLayout* this_, float min_height, float max_height) nothrow;
void clutter_flow_layout_set_row_spacing(FlowLayout* this_, float spacing) nothrow;
int clutter_geometry_intersects(Geometry* this_, Geometry* geometry1) nothrow;
void clutter_geometry_union(Geometry* this_, Geometry* geometry_b, /*out*/ Geometry* result) nothrow;
GestureAction* clutter_gesture_action_new() nothrow;
void clutter_gesture_action_get_motion_coords(GestureAction* this_, uint device, /*out*/ float* motion_x, /*out*/ float* motion_y) nothrow;
void clutter_gesture_action_get_press_coords(GestureAction* this_, uint device, /*out*/ float* press_x, /*out*/ float* press_y) nothrow;
void clutter_gesture_action_get_release_coords(GestureAction* this_, uint device, /*out*/ float* release_x, /*out*/ float* release_y) nothrow;
Group* clutter_group_new() nothrow;
int clutter_group_get_n_children(Group* this_) nothrow;
Actor* clutter_group_get_nth_child(Group* this_, int index_) nothrow;
void clutter_group_remove_all(Group* this_) nothrow;
InputDevice* clutter_input_device_get_associated_device(InputDevice* this_) nothrow;
InputAxis clutter_input_device_get_axis(InputDevice* this_, uint index_) nothrow;
int clutter_input_device_get_axis_value(InputDevice* this_, double* axes, InputAxis axis, /*out*/ double* value) nothrow;
void clutter_input_device_get_device_coords(InputDevice* this_, /*out*/ int* x, /*out*/ int* y) nothrow;
int clutter_input_device_get_device_id(InputDevice* this_) nothrow;
InputMode clutter_input_device_get_device_mode(InputDevice* this_) nothrow;
char* clutter_input_device_get_device_name(InputDevice* this_) nothrow;
InputDeviceType clutter_input_device_get_device_type(InputDevice* this_) nothrow;
int clutter_input_device_get_enabled(InputDevice* this_) nothrow;
Actor* clutter_input_device_get_grabbed_actor(InputDevice* this_) nothrow;
int clutter_input_device_get_has_cursor(InputDevice* this_) nothrow;
int clutter_input_device_get_key(InputDevice* this_, uint index_, /*out*/ uint* keyval, /*out*/ ModifierType* modifiers) nothrow;
uint clutter_input_device_get_n_axes(InputDevice* this_) nothrow;
uint clutter_input_device_get_n_keys(InputDevice* this_) nothrow;
Actor* clutter_input_device_get_pointer_actor(InputDevice* this_) nothrow;
Stage* clutter_input_device_get_pointer_stage(InputDevice* this_) nothrow;
GLib2.List* /*new container*/ clutter_input_device_get_slave_devices(InputDevice* this_) nothrow;
void clutter_input_device_grab(InputDevice* this_, Actor* actor) nothrow;
void clutter_input_device_set_enabled(InputDevice* this_, int enabled) nothrow;
void clutter_input_device_set_key(InputDevice* this_, uint index_, uint keyval, ModifierType modifiers) nothrow;
void clutter_input_device_ungrab(InputDevice* this_) nothrow;
void clutter_input_device_update_from_event(InputDevice* this_, Event* event, int update_stage) nothrow;
Interval* clutter_interval_new(Type gtype, ...) nothrow;
Interval* clutter_interval_new_with_values(Type gtype, GObject2.Value* initial, GObject2.Value* final_) nothrow;
void clutter_interval_register_progress_func(Type value_type, ProgressFunc func) nothrow;
Interval* /*new*/ clutter_interval_clone(Interval* this_) nothrow;
GObject2.Value* clutter_interval_compute(Interval* this_, double factor) nothrow;
int clutter_interval_compute_value(Interval* this_, double factor, /*out*/ GObject2.Value* value) nothrow;
void clutter_interval_get_final_value(Interval* this_, /*out*/ GObject2.Value* value) nothrow;
void clutter_interval_get_initial_value(Interval* this_, /*out*/ GObject2.Value* value) nothrow;
void clutter_interval_get_interval(Interval* this_, ...) nothrow;
Type clutter_interval_get_value_type(Interval* this_) nothrow;
GObject2.Value* clutter_interval_peek_final_value(Interval* this_) nothrow;
GObject2.Value* clutter_interval_peek_initial_value(Interval* this_) nothrow;
void clutter_interval_set_final_value(Interval* this_, GObject2.Value* value) nothrow;
void clutter_interval_set_initial_value(Interval* this_, GObject2.Value* value) nothrow;
void clutter_interval_set_interval(Interval* this_, ...) nothrow;
int clutter_interval_validate(Interval* this_, GObject2.ParamSpec* pspec) nothrow;
Knot* /*new*/ clutter_knot_copy(Knot* this_) nothrow;
int clutter_knot_equal(Knot* this_, Knot* knot_b) nothrow;
void clutter_knot_free(Knot* this_) nothrow;
void clutter_layout_manager_allocate(LayoutManager* this_, Container* container, ActorBox* allocation, AllocationFlags flags) nothrow;
Alpha* clutter_layout_manager_begin_animation(LayoutManager* this_, uint duration, c_ulong mode) nothrow;
void clutter_layout_manager_child_get(LayoutManager* this_, Container* container, Actor* actor, char* first_property, ...) nothrow;
void clutter_layout_manager_child_get_property(LayoutManager* this_, Container* container, Actor* actor, char* property_name, GObject2.Value* value) nothrow;
void clutter_layout_manager_child_set(LayoutManager* this_, Container* container, Actor* actor, char* first_property, ...) nothrow;
void clutter_layout_manager_child_set_property(LayoutManager* this_, Container* container, Actor* actor, char* property_name, GObject2.Value* value) nothrow;
void clutter_layout_manager_end_animation(LayoutManager* this_) nothrow;
GObject2.ParamSpec* clutter_layout_manager_find_child_property(LayoutManager* this_, char* name) nothrow;
double clutter_layout_manager_get_animation_progress(LayoutManager* this_) nothrow;
LayoutMeta* clutter_layout_manager_get_child_meta(LayoutManager* this_, Container* container, Actor* actor) nothrow;
void clutter_layout_manager_get_preferred_height(LayoutManager* this_, Container* container, float for_width, /*out*/ float* min_height_p=null, /*out*/ float* nat_height_p=null) nothrow;
void clutter_layout_manager_get_preferred_width(LayoutManager* this_, Container* container, float for_height, /*out*/ float* min_width_p=null, /*out*/ float* nat_width_p=null) nothrow;
void clutter_layout_manager_layout_changed(LayoutManager* this_) nothrow;
GObject2.ParamSpec** /*new*/ clutter_layout_manager_list_child_properties(LayoutManager* this_, /*out*/ uint* n_pspecs) nothrow;
void clutter_layout_manager_set_container(LayoutManager* this_, Container* container=null) nothrow;
LayoutManager* clutter_layout_meta_get_manager(LayoutMeta* this_) nothrow;
ListModel* /*new*/ clutter_list_model_new(uint n_columns, ...) nothrow;
ListModel* /*new*/ clutter_list_model_newv(uint n_columns, Type* types, char* names) nothrow;
Margin* /*new*/ clutter_margin_new() nothrow;
Margin* /*new*/ clutter_margin_copy(Margin* this_) nothrow;
void clutter_margin_free(Margin* this_) nothrow;
double clutter_media_get_audio_volume(Media* this_) nothrow;
double clutter_media_get_buffer_fill(Media* this_) nothrow;
int clutter_media_get_can_seek(Media* this_) nothrow;
double clutter_media_get_duration(Media* this_) nothrow;
int clutter_media_get_playing(Media* this_) nothrow;
double clutter_media_get_progress(Media* this_) nothrow;
char* /*new*/ clutter_media_get_subtitle_font_name(Media* this_) nothrow;
char* /*new*/ clutter_media_get_subtitle_uri(Media* this_) nothrow;
char* /*new*/ clutter_media_get_uri(Media* this_) nothrow;
void clutter_media_set_audio_volume(Media* this_, double volume) nothrow;
void clutter_media_set_filename(Media* this_, char* filename) nothrow;
void clutter_media_set_playing(Media* this_, int playing) nothrow;
void clutter_media_set_progress(Media* this_, double progress) nothrow;
void clutter_media_set_subtitle_font_name(Media* this_, char* font_name) nothrow;
void clutter_media_set_subtitle_uri(Media* this_, char* uri) nothrow;
void clutter_media_set_uri(Media* this_, char* uri) nothrow;
void clutter_model_append(Model* this_, ...) nothrow;
void clutter_model_appendv(Model* this_, uint n_columns, uint* columns, GObject2.Value* values) nothrow;
int clutter_model_filter_iter(Model* this_, ModelIter* iter) nothrow;
int clutter_model_filter_row(Model* this_, uint row) nothrow;
void clutter_model_foreach(Model* this_, ModelForeachFunc func, void* user_data) nothrow;
char* clutter_model_get_column_name(Model* this_, uint column) nothrow;
Type clutter_model_get_column_type(Model* this_, uint column) nothrow;
int clutter_model_get_filter_set(Model* this_) nothrow;
ModelIter* /*new*/ clutter_model_get_first_iter(Model* this_) nothrow;
ModelIter* /*new*/ clutter_model_get_iter_at_row(Model* this_, uint row) nothrow;
ModelIter* /*new*/ clutter_model_get_last_iter(Model* this_) nothrow;
uint clutter_model_get_n_columns(Model* this_) nothrow;
uint clutter_model_get_n_rows(Model* this_) nothrow;
int clutter_model_get_sorting_column(Model* this_) nothrow;
void clutter_model_insert(Model* this_, uint row, ...) nothrow;
void clutter_model_insert_value(Model* this_, uint row, uint column, GObject2.Value* value) nothrow;
void clutter_model_insertv(Model* this_, uint row, uint n_columns, uint* columns, GObject2.Value* values) nothrow;
void clutter_model_prepend(Model* this_, ...) nothrow;
void clutter_model_prependv(Model* this_, uint n_columns, uint* columns, GObject2.Value* values) nothrow;
void clutter_model_remove(Model* this_, uint row) nothrow;
void clutter_model_resort(Model* this_) nothrow;
void clutter_model_set_filter(Model* this_, ModelFilterFunc func, void* user_data, GLib2.DestroyNotify notify) nothrow;
void clutter_model_set_names(Model* this_, uint n_columns, char* names) nothrow;
void clutter_model_set_sort(Model* this_, int column, ModelSortFunc func, void* user_data, GLib2.DestroyNotify notify) nothrow;
void clutter_model_set_sorting_column(Model* this_, int column) nothrow;
void clutter_model_set_types(Model* this_, uint n_columns, Type* types) nothrow;
ModelIter* /*new*/ clutter_model_iter_copy(ModelIter* this_) nothrow;
void clutter_model_iter_get(ModelIter* this_, ...) nothrow;
Model* clutter_model_iter_get_model(ModelIter* this_) nothrow;
uint clutter_model_iter_get_row(ModelIter* this_) nothrow;
void clutter_model_iter_get_valist(ModelIter* this_, va_list args) nothrow;
void clutter_model_iter_get_value(ModelIter* this_, uint column, /*out*/ GObject2.Value* value) nothrow;
int clutter_model_iter_is_first(ModelIter* this_) nothrow;
int clutter_model_iter_is_last(ModelIter* this_) nothrow;
ModelIter* clutter_model_iter_next(ModelIter* this_) nothrow;
ModelIter* clutter_model_iter_prev(ModelIter* this_) nothrow;
void clutter_model_iter_set(ModelIter* this_, ...) nothrow;
void clutter_model_iter_set_valist(ModelIter* this_, va_list args) nothrow;
void clutter_model_iter_set_value(ModelIter* this_, uint column, GObject2.Value* value) nothrow;
Cogl.Handle /*new*/ clutter_offscreen_effect_create_texture(OffscreenEffect* this_, float width, float height) nothrow;
Cogl.Material* clutter_offscreen_effect_get_target(OffscreenEffect* this_) nothrow;
int clutter_offscreen_effect_get_target_size(OffscreenEffect* this_, /*out*/ float* width, /*out*/ float* height) nothrow;
void clutter_offscreen_effect_paint_target(OffscreenEffect* this_) nothrow;
PageTurnEffect* clutter_page_turn_effect_new(double period, double angle, float radius) nothrow;
double clutter_page_turn_effect_get_angle(PageTurnEffect* this_) nothrow;
double clutter_page_turn_effect_get_period(PageTurnEffect* this_) nothrow;
float clutter_page_turn_effect_get_radius(PageTurnEffect* this_) nothrow;
void clutter_page_turn_effect_set_angle(PageTurnEffect* this_, double angle) nothrow;
void clutter_page_turn_effect_set_period(PageTurnEffect* this_, double period) nothrow;
void clutter_page_turn_effect_set_radius(PageTurnEffect* this_, float radius) nothrow;
PaintVolume* /*new*/ clutter_paint_volume_copy(PaintVolume* this_) nothrow;
void clutter_paint_volume_free(PaintVolume* this_) nothrow;
float clutter_paint_volume_get_depth(PaintVolume* this_) nothrow;
float clutter_paint_volume_get_height(PaintVolume* this_) nothrow;
void clutter_paint_volume_get_origin(PaintVolume* this_, /*out*/ Vertex* vertex) nothrow;
float clutter_paint_volume_get_width(PaintVolume* this_) nothrow;
void clutter_paint_volume_set_depth(PaintVolume* this_, float depth) nothrow;
int clutter_paint_volume_set_from_allocation(PaintVolume* this_, Actor* actor) nothrow;
void clutter_paint_volume_set_height(PaintVolume* this_, float height) nothrow;
void clutter_paint_volume_set_origin(PaintVolume* this_, Vertex* origin) nothrow;
void clutter_paint_volume_set_width(PaintVolume* this_, float width) nothrow;
void clutter_paint_volume_union(PaintVolume* this_, PaintVolume* another_pv) nothrow;
Path* clutter_path_new() nothrow;
Path* clutter_path_new_with_description(char* desc) nothrow;
void clutter_path_add_cairo_path(Path* this_, cairo.Path* cpath) nothrow;
void clutter_path_add_close(Path* this_) nothrow;
void clutter_path_add_curve_to(Path* this_, int x_1, int y_1, int x_2, int y_2, int x_3, int y_3) nothrow;
void clutter_path_add_line_to(Path* this_, int x, int y) nothrow;
void clutter_path_add_move_to(Path* this_, int x, int y) nothrow;
void clutter_path_add_node(Path* this_, PathNode* node) nothrow;
void clutter_path_add_rel_curve_to(Path* this_, int x_1, int y_1, int x_2, int y_2, int x_3, int y_3) nothrow;
void clutter_path_add_rel_line_to(Path* this_, int x, int y) nothrow;
void clutter_path_add_rel_move_to(Path* this_, int x, int y) nothrow;
int clutter_path_add_string(Path* this_, char* str) nothrow;
void clutter_path_clear(Path* this_) nothrow;
void clutter_path_foreach(Path* this_, PathCallback callback, void* user_data) nothrow;
char* /*new*/ clutter_path_get_description(Path* this_) nothrow;
uint clutter_path_get_length(Path* this_) nothrow;
uint clutter_path_get_n_nodes(Path* this_) nothrow;
void clutter_path_get_node(Path* this_, uint index_, /*out*/ PathNode* node) nothrow;
GLib2.SList* /*new container*/ clutter_path_get_nodes(Path* this_) nothrow;
uint clutter_path_get_position(Path* this_, double progress, /*out*/ Knot* position) nothrow;
void clutter_path_insert_node(Path* this_, int index_, PathNode* node) nothrow;
void clutter_path_remove_node(Path* this_, uint index_) nothrow;
void clutter_path_replace_node(Path* this_, uint index_, PathNode* node) nothrow;
int clutter_path_set_description(Path* this_, char* str) nothrow;
void clutter_path_to_cairo_path(Path* this_, cairo.Context* cr) nothrow;
PathConstraint* /*new*/ clutter_path_constraint_new(Path* path, float offset) nothrow;
float clutter_path_constraint_get_offset(PathConstraint* this_) nothrow;
Path* clutter_path_constraint_get_path(PathConstraint* this_) nothrow;
void clutter_path_constraint_set_offset(PathConstraint* this_, float offset) nothrow;
void clutter_path_constraint_set_path(PathConstraint* this_, Path* path=null) nothrow;
PathNode* /*new*/ clutter_path_node_copy(PathNode* this_) nothrow;
int clutter_path_node_equal(PathNode* this_, PathNode* node_b) nothrow;
void clutter_path_node_free(PathNode* this_) nothrow;
Rectangle* clutter_rectangle_new() nothrow;
Rectangle* clutter_rectangle_new_with_color(Color* color) nothrow;
void clutter_rectangle_get_border_color(Rectangle* this_, /*out*/ Color* color) nothrow;
uint clutter_rectangle_get_border_width(Rectangle* this_) nothrow;
void clutter_rectangle_get_color(Rectangle* this_, /*out*/ Color* color) nothrow;
void clutter_rectangle_set_border_color(Rectangle* this_, Color* color) nothrow;
void clutter_rectangle_set_border_width(Rectangle* this_, uint width) nothrow;
void clutter_rectangle_set_color(Rectangle* this_, Color* color) nothrow;
Score* /*new*/ clutter_score_new() nothrow;
c_ulong clutter_score_append(Score* this_, Timeline* parent, Timeline* timeline) nothrow;
c_ulong clutter_score_append_at_marker(Score* this_, Timeline* parent, char* marker_name, Timeline* timeline) nothrow;
int clutter_score_get_loop(Score* this_) nothrow;
Timeline* clutter_score_get_timeline(Score* this_, c_ulong id_) nothrow;
int clutter_score_is_playing(Score* this_) nothrow;
GLib2.SList* /*new container*/ clutter_score_list_timelines(Score* this_) nothrow;
void clutter_score_pause(Score* this_) nothrow;
void clutter_score_remove(Score* this_, c_ulong id_) nothrow;
void clutter_score_remove_all(Score* this_) nothrow;
void clutter_score_rewind(Score* this_) nothrow;
void clutter_score_set_loop(Score* this_, int loop) nothrow;
void clutter_score_start(Score* this_) nothrow;
void clutter_score_stop(Score* this_) nothrow;
Script* /*new*/ clutter_script_new() nothrow;
void clutter_script_add_search_paths(Script* this_, char* paths, size_t n_paths) nothrow;
void clutter_script_add_states(Script* this_, char* name, State* state) nothrow;
void clutter_script_connect_signals(Script* this_, void* user_data) nothrow;
void clutter_script_connect_signals_full(Script* this_, ScriptConnectFunc func, void* user_data) nothrow;
void clutter_script_ensure_objects(Script* this_) nothrow;
GObject2.Object* clutter_script_get_object(Script* this_, char* name) nothrow;
int clutter_script_get_objects(Script* this_, char* first_name, ...) nothrow;
State* clutter_script_get_states(Script* this_, char* name=null) nothrow;
Type clutter_script_get_type_from_name(Script* this_, char* type_name) nothrow;
GLib2.List* /*new container*/ clutter_script_list_objects(Script* this_) nothrow;
uint clutter_script_load_from_data(Script* this_, char* data, ssize_t length, GLib2.Error** error) nothrow;
uint clutter_script_load_from_file(Script* this_, char* filename, GLib2.Error** error) nothrow;
uint clutter_script_load_from_resource(Script* this_, char* resource_path, GLib2.Error** error) nothrow;
char* /*new*/ clutter_script_lookup_filename(Script* this_, char* filename) nothrow;
void clutter_script_unmerge_objects(Script* this_, uint merge_id) nothrow;
char* clutter_scriptable_get_id(Scriptable* this_) nothrow;
int clutter_scriptable_parse_custom_node(Scriptable* this_, Script* script, GObject2.Value* value, char* name, Json.Node* node) nothrow;
void clutter_scriptable_set_custom_property(Scriptable* this_, Script* script, char* name, GObject2.Value* value) nothrow;
void clutter_scriptable_set_id(Scriptable* this_, char* id_) nothrow;
Settings* clutter_settings_get_default() nothrow;
Shader* /*new*/ clutter_shader_new() nothrow;
GLib2.Quark clutter_shader_error_quark() nothrow;
int clutter_shader_compile(Shader* this_, GLib2.Error** error) nothrow;
Cogl.Handle clutter_shader_get_cogl_fragment_shader(Shader* this_) nothrow;
Cogl.Handle clutter_shader_get_cogl_program(Shader* this_) nothrow;
Cogl.Handle clutter_shader_get_cogl_vertex_shader(Shader* this_) nothrow;
char* clutter_shader_get_fragment_source(Shader* this_) nothrow;
int clutter_shader_get_is_enabled(Shader* this_) nothrow;
char* clutter_shader_get_vertex_source(Shader* this_) nothrow;
int clutter_shader_is_compiled(Shader* this_) nothrow;
void clutter_shader_release(Shader* this_) nothrow;
void clutter_shader_set_fragment_source(Shader* this_, char* data, ssize_t length) nothrow;
void clutter_shader_set_is_enabled(Shader* this_, int enabled) nothrow;
void clutter_shader_set_uniform(Shader* this_, char* name, GObject2.Value* value) nothrow;
void clutter_shader_set_vertex_source(Shader* this_, char* data, ssize_t length) nothrow;
ShaderEffect* /*new*/ clutter_shader_effect_new(ShaderType shader_type) nothrow;
Cogl.Handle clutter_shader_effect_get_program(ShaderEffect* this_) nothrow;
Cogl.Handle clutter_shader_effect_get_shader(ShaderEffect* this_) nothrow;
int clutter_shader_effect_set_shader_source(ShaderEffect* this_, char* source) nothrow;
void clutter_shader_effect_set_uniform(ShaderEffect* this_, char* name, Type gtype, size_t n_values, ...) nothrow;
void clutter_shader_effect_set_uniform_value(ShaderEffect* this_, char* name, GObject2.Value* value) nothrow;
SnapConstraint* clutter_snap_constraint_new(Actor* source, SnapEdge from_edge, SnapEdge to_edge, float offset) nothrow;
void clutter_snap_constraint_get_edges(SnapConstraint* this_, /*out*/ SnapEdge* from_edge, /*out*/ SnapEdge* to_edge) nothrow;
float clutter_snap_constraint_get_offset(SnapConstraint* this_) nothrow;
Actor* clutter_snap_constraint_get_source(SnapConstraint* this_) nothrow;
void clutter_snap_constraint_set_edges(SnapConstraint* this_, SnapEdge from_edge, SnapEdge to_edge) nothrow;
void clutter_snap_constraint_set_offset(SnapConstraint* this_, float offset) nothrow;
void clutter_snap_constraint_set_source(SnapConstraint* this_, Actor* source=null) nothrow;
Stage* clutter_stage_new() nothrow;
Clutter.Stage* clutter_stage_get_default() nothrow;
void clutter_stage_ensure_current(Stage* this_) nothrow;
void clutter_stage_ensure_redraw(Stage* this_) nothrow;
void clutter_stage_ensure_viewport(Stage* this_) nothrow;
int clutter_stage_event(Stage* this_, Event* event) nothrow;
int clutter_stage_get_accept_focus(Stage* this_) nothrow;
Actor* clutter_stage_get_actor_at_pos(Stage* this_, PickMode pick_mode, int x, int y) nothrow;
void clutter_stage_get_color(Stage* this_, /*out*/ Color* color) nothrow;
void clutter_stage_get_fog(Stage* this_, /*out*/ Fog* fog) nothrow;
int clutter_stage_get_fullscreen(Stage* this_) nothrow;
Actor* clutter_stage_get_key_focus(Stage* this_) nothrow;
void clutter_stage_get_minimum_size(Stage* this_, /*out*/ uint* width, /*out*/ uint* height) nothrow;
int clutter_stage_get_motion_events_enabled(Stage* this_) nothrow;
int clutter_stage_get_no_clear_hint(Stage* this_) nothrow;
void clutter_stage_get_perspective(Stage* this_, /*out*/ Perspective* perspective=null) nothrow;
void clutter_stage_get_redraw_clip_bounds(Stage* this_, /*out*/ cairo.RectangleInt* clip) nothrow;
int clutter_stage_get_throttle_motion_events(Stage* this_) nothrow;
char* clutter_stage_get_title(Stage* this_) nothrow;
int clutter_stage_get_use_alpha(Stage* this_) nothrow;
int clutter_stage_get_use_fog(Stage* this_) nothrow;
int clutter_stage_get_user_resizable(Stage* this_) nothrow;
void clutter_stage_hide_cursor(Stage* this_) nothrow;
int clutter_stage_is_default(Stage* this_) nothrow;
void clutter_stage_queue_redraw(Stage* this_) nothrow;
ubyte* clutter_stage_read_pixels(Stage* this_, int x, int y, int width, int height) nothrow;
void clutter_stage_set_accept_focus(Stage* this_, int accept_focus) nothrow;
void clutter_stage_set_color(Stage* this_, Color* color) nothrow;
void clutter_stage_set_fog(Stage* this_, Fog* fog) nothrow;
void clutter_stage_set_fullscreen(Stage* this_, int fullscreen) nothrow;
void clutter_stage_set_key_focus(Stage* this_, Actor* actor=null) nothrow;
void clutter_stage_set_minimum_size(Stage* this_, uint width, uint height) nothrow;
void clutter_stage_set_motion_events_enabled(Stage* this_, int enabled) nothrow;
void clutter_stage_set_no_clear_hint(Stage* this_, int no_clear) nothrow;
void clutter_stage_set_perspective(Stage* this_, Perspective* perspective) nothrow;
void clutter_stage_set_throttle_motion_events(Stage* this_, int throttle) nothrow;
void clutter_stage_set_title(Stage* this_, char* title) nothrow;
void clutter_stage_set_use_alpha(Stage* this_, int use_alpha) nothrow;
void clutter_stage_set_use_fog(Stage* this_, int fog) nothrow;
void clutter_stage_set_user_resizable(Stage* this_, int resizable) nothrow;
void clutter_stage_show_cursor(Stage* this_) nothrow;
StageManager* clutter_stage_manager_get_default() nothrow;
Stage* clutter_stage_manager_get_default_stage(StageManager* this_) nothrow;
GLib2.SList* /*new container*/ clutter_stage_manager_list_stages(StageManager* this_) nothrow;
GLib2.SList* clutter_stage_manager_peek_stages(StageManager* this_) nothrow;
void clutter_stage_manager_set_default_stage(StageManager* this_, Stage* stage) nothrow;
State* /*new*/ clutter_state_new() nothrow;
Animator* clutter_state_get_animator(State* this_, char* source_state_name, char* target_state_name) nothrow;
uint clutter_state_get_duration(State* this_, char* source_state_name=null, char* target_state_name=null) nothrow;
GLib2.List* /*new container*/ clutter_state_get_keys(State* this_, char* source_state_name=null, char* target_state_name=null, GObject2.Object* object=null, char* property_name=null) nothrow;
char* clutter_state_get_state(State* this_) nothrow;
GLib2.List* /*new container*/ clutter_state_get_states(State* this_) nothrow;
Timeline* clutter_state_get_timeline(State* this_) nothrow;
void clutter_state_remove_key(State* this_, char* source_state_name=null, char* target_state_name=null, GObject2.Object* object=null, char* property_name=null) nothrow;
void clutter_state_set(State* this_, char* source_state_name, char* target_state_name, void* first_object, char* first_property_name, c_ulong first_mode, ...) nothrow;
void clutter_state_set_animator(State* this_, char* source_state_name, char* target_state_name, Animator* animator=null) nothrow;
void clutter_state_set_duration(State* this_, char* source_state_name, char* target_state_name, uint duration) nothrow;
State* clutter_state_set_key(State* this_, char* source_state_name, char* target_state_name, GObject2.Object* object, char* property_name, uint mode, GObject2.Value* value, double pre_delay, double post_delay) nothrow;
Timeline* clutter_state_set_state(State* this_, char* target_state_name) nothrow;
Timeline* clutter_state_warp_to_state(State* this_, char* target_state_name) nothrow;
c_ulong clutter_state_key_get_mode(StateKey* this_) nothrow;
GObject2.Object* clutter_state_key_get_object(StateKey* this_) nothrow;
double clutter_state_key_get_post_delay(StateKey* this_) nothrow;
double clutter_state_key_get_pre_delay(StateKey* this_) nothrow;
char* clutter_state_key_get_property_name(StateKey* this_) nothrow;
Type clutter_state_key_get_property_type(StateKey* this_) nothrow;
char* clutter_state_key_get_source_state_name(StateKey* this_) nothrow;
char* clutter_state_key_get_target_state_name(StateKey* this_) nothrow;
int clutter_state_key_get_value(StateKey* this_, GObject2.Value* value) nothrow;
SwipeAction* clutter_swipe_action_new() nothrow;
TableLayout* clutter_table_layout_new() nothrow;
void clutter_table_layout_get_alignment(TableLayout* this_, Actor* actor, /*out*/ TableAlignment* x_align, /*out*/ TableAlignment* y_align) nothrow;
int clutter_table_layout_get_column_count(TableLayout* this_) nothrow;
uint clutter_table_layout_get_column_spacing(TableLayout* this_) nothrow;
uint clutter_table_layout_get_easing_duration(TableLayout* this_) nothrow;
c_ulong clutter_table_layout_get_easing_mode(TableLayout* this_) nothrow;
void clutter_table_layout_get_expand(TableLayout* this_, Actor* actor, /*out*/ int* x_expand, /*out*/ int* y_expand) nothrow;
void clutter_table_layout_get_fill(TableLayout* this_, Actor* actor, /*out*/ int* x_fill, /*out*/ int* y_fill) nothrow;
int clutter_table_layout_get_row_count(TableLayout* this_) nothrow;
uint clutter_table_layout_get_row_spacing(TableLayout* this_) nothrow;
void clutter_table_layout_get_span(TableLayout* this_, Actor* actor, /*out*/ int* column_span, /*out*/ int* row_span) nothrow;
int clutter_table_layout_get_use_animations(TableLayout* this_) nothrow;
void clutter_table_layout_pack(TableLayout* this_, Actor* actor, int column, int row) nothrow;
void clutter_table_layout_set_alignment(TableLayout* this_, Actor* actor, TableAlignment x_align, TableAlignment y_align) nothrow;
void clutter_table_layout_set_column_spacing(TableLayout* this_, uint spacing) nothrow;
void clutter_table_layout_set_easing_duration(TableLayout* this_, uint msecs) nothrow;
void clutter_table_layout_set_easing_mode(TableLayout* this_, c_ulong mode) nothrow;
void clutter_table_layout_set_expand(TableLayout* this_, Actor* actor, int x_expand, int y_expand) nothrow;
void clutter_table_layout_set_fill(TableLayout* this_, Actor* actor, int x_fill, int y_fill) nothrow;
void clutter_table_layout_set_row_spacing(TableLayout* this_, uint spacing) nothrow;
void clutter_table_layout_set_span(TableLayout* this_, Actor* actor, int column_span, int row_span) nothrow;
void clutter_table_layout_set_use_animations(TableLayout* this_, int animate) nothrow;
Text* clutter_text_new() nothrow;
Text* clutter_text_new_full(char* font_name, char* text, Color* color) nothrow;
Text* clutter_text_new_with_buffer(TextBuffer* buffer) nothrow;
Text* clutter_text_new_with_text(char* font_name, char* text) nothrow;
int clutter_text_activate(Text* this_) nothrow;
int clutter_text_coords_to_position(Text* this_, float x, float y) nothrow;
void clutter_text_delete_chars(Text* this_, uint n_chars) nothrow;
int clutter_text_delete_selection(Text* this_) nothrow;
void clutter_text_delete_text(Text* this_, ssize_t start_pos, ssize_t end_pos) nothrow;
int clutter_text_get_activatable(Text* this_) nothrow;
Pango.AttrList* clutter_text_get_attributes(Text* this_) nothrow;
TextBuffer* clutter_text_get_buffer(Text* this_) nothrow;
char* /*new*/ clutter_text_get_chars(Text* this_, ssize_t start_pos, ssize_t end_pos) nothrow;
void clutter_text_get_color(Text* this_, /*out*/ Color* color) nothrow;
void clutter_text_get_cursor_color(Text* this_, /*out*/ Color* color) nothrow;
int clutter_text_get_cursor_position(Text* this_) nothrow;
uint clutter_text_get_cursor_size(Text* this_) nothrow;
int clutter_text_get_cursor_visible(Text* this_) nothrow;
int clutter_text_get_editable(Text* this_) nothrow;
Pango.EllipsizeMode clutter_text_get_ellipsize(Text* this_) nothrow;
Pango.FontDescription* /*new*/ clutter_text_get_font_description(Text* this_) nothrow;
char* clutter_text_get_font_name(Text* this_) nothrow;
int clutter_text_get_justify(Text* this_) nothrow;
Pango.Layout* clutter_text_get_layout(Text* this_) nothrow;
void clutter_text_get_layout_offsets(Text* this_, /*out*/ int* x, /*out*/ int* y) nothrow;
Pango.Alignment clutter_text_get_line_alignment(Text* this_) nothrow;
int clutter_text_get_line_wrap(Text* this_) nothrow;
Pango.WrapMode clutter_text_get_line_wrap_mode(Text* this_) nothrow;
int clutter_text_get_max_length(Text* this_) nothrow;
dchar clutter_text_get_password_char(Text* this_) nothrow;
int clutter_text_get_selectable(Text* this_) nothrow;
void clutter_text_get_selected_text_color(Text* this_, /*out*/ Color* color) nothrow;
char* /*new*/ clutter_text_get_selection(Text* this_) nothrow;
int clutter_text_get_selection_bound(Text* this_) nothrow;
void clutter_text_get_selection_color(Text* this_, /*out*/ Color* color) nothrow;
int clutter_text_get_single_line_mode(Text* this_) nothrow;
char* clutter_text_get_text(Text* this_) nothrow;
int clutter_text_get_use_markup(Text* this_) nothrow;
void clutter_text_insert_text(Text* this_, char* text, ssize_t position) nothrow;
void clutter_text_insert_unichar(Text* this_, dchar wc) nothrow;
int clutter_text_position_to_coords(Text* this_, int position, /*out*/ float* x, /*out*/ float* y, /*out*/ float* line_height) nothrow;
void clutter_text_set_activatable(Text* this_, int activatable) nothrow;
void clutter_text_set_attributes(Text* this_, Pango.AttrList* attrs) nothrow;
void clutter_text_set_buffer(Text* this_, TextBuffer* buffer) nothrow;
void clutter_text_set_color(Text* this_, Color* color) nothrow;
void clutter_text_set_cursor_color(Text* this_, Color* color) nothrow;
void clutter_text_set_cursor_position(Text* this_, int position) nothrow;
void clutter_text_set_cursor_size(Text* this_, int size) nothrow;
void clutter_text_set_cursor_visible(Text* this_, int cursor_visible) nothrow;
void clutter_text_set_editable(Text* this_, int editable) nothrow;
void clutter_text_set_ellipsize(Text* this_, Pango.EllipsizeMode mode) nothrow;
void clutter_text_set_font_description(Text* this_, Pango.FontDescription* font_desc) nothrow;
void clutter_text_set_font_name(Text* this_, char* font_name=null) nothrow;
void clutter_text_set_justify(Text* this_, int justify) nothrow;
void clutter_text_set_line_alignment(Text* this_, Pango.Alignment alignment) nothrow;
void clutter_text_set_line_wrap(Text* this_, int line_wrap) nothrow;
void clutter_text_set_line_wrap_mode(Text* this_, Pango.WrapMode wrap_mode) nothrow;
void clutter_text_set_markup(Text* this_, char* markup) nothrow;
void clutter_text_set_max_length(Text* this_, int max) nothrow;
void clutter_text_set_password_char(Text* this_, dchar wc) nothrow;
void clutter_text_set_preedit_string(Text* this_, char* preedit_str, Pango.AttrList* preedit_attrs, uint cursor_pos) nothrow;
void clutter_text_set_selectable(Text* this_, int selectable) nothrow;
void clutter_text_set_selected_text_color(Text* this_, Color* color) nothrow;
void clutter_text_set_selection(Text* this_, ssize_t start_pos, ssize_t end_pos) nothrow;
void clutter_text_set_selection_bound(Text* this_, int selection_bound) nothrow;
void clutter_text_set_selection_color(Text* this_, Color* color) nothrow;
void clutter_text_set_single_line_mode(Text* this_, int single_line) nothrow;
void clutter_text_set_text(Text* this_, char* text) nothrow;
void clutter_text_set_use_markup(Text* this_, int setting) nothrow;
TextBuffer* /*new*/ clutter_text_buffer_new() nothrow;
TextBuffer* /*new*/ clutter_text_buffer_new_with_text(char* text, ssize_t text_len) nothrow;
uint clutter_text_buffer_delete_text(TextBuffer* this_, uint position, int n_chars) nothrow;
void clutter_text_buffer_emit_deleted_text(TextBuffer* this_, uint position, uint n_chars) nothrow;
void clutter_text_buffer_emit_inserted_text(TextBuffer* this_, uint position, char* chars, uint n_chars) nothrow;
size_t clutter_text_buffer_get_bytes(TextBuffer* this_) nothrow;
uint clutter_text_buffer_get_length(TextBuffer* this_) nothrow;
int clutter_text_buffer_get_max_length(TextBuffer* this_) nothrow;
char* clutter_text_buffer_get_text(TextBuffer* this_) nothrow;
uint clutter_text_buffer_insert_text(TextBuffer* this_, uint position, char* chars, int n_chars) nothrow;
void clutter_text_buffer_set_max_length(TextBuffer* this_, int max_length) nothrow;
void clutter_text_buffer_set_text(TextBuffer* this_, char* chars, int n_chars) nothrow;
Texture* clutter_texture_new() nothrow;
Texture* clutter_texture_new_from_actor(Actor* actor) nothrow;
Texture* clutter_texture_new_from_file(char* filename, GLib2.Error** error) nothrow;
void clutter_texture_get_base_size(Texture* this_, /*out*/ int* width, /*out*/ int* height) nothrow;
Cogl.Handle clutter_texture_get_cogl_material(Texture* this_) nothrow;
Cogl.Handle clutter_texture_get_cogl_texture(Texture* this_) nothrow;
TextureQuality clutter_texture_get_filter_quality(Texture* this_) nothrow;
int clutter_texture_get_keep_aspect_ratio(Texture* this_) nothrow;
int clutter_texture_get_load_async(Texture* this_) nothrow;
int clutter_texture_get_load_data_async(Texture* this_) nothrow;
int clutter_texture_get_max_tile_waste(Texture* this_) nothrow;
int clutter_texture_get_pick_with_alpha(Texture* this_) nothrow;
Cogl.PixelFormat clutter_texture_get_pixel_format(Texture* this_) nothrow;
void clutter_texture_get_repeat(Texture* this_, /*out*/ int* repeat_x, /*out*/ int* repeat_y) nothrow;
int clutter_texture_get_sync_size(Texture* this_) nothrow;
int clutter_texture_set_area_from_rgb_data(Texture* this_, ubyte* data, int has_alpha, int x, int y, int width, int height, int rowstride, int bpp, TextureFlags flags, GLib2.Error** error) nothrow;
void clutter_texture_set_cogl_material(Texture* this_, Cogl.Handle cogl_material) nothrow;
void clutter_texture_set_cogl_texture(Texture* this_, Cogl.Handle cogl_tex) nothrow;
void clutter_texture_set_filter_quality(Texture* this_, TextureQuality filter_quality) nothrow;
int clutter_texture_set_from_file(Texture* this_, char* filename, GLib2.Error** error) nothrow;
int clutter_texture_set_from_rgb_data(Texture* this_, ubyte* data, int has_alpha, int width, int height, int rowstride, int bpp, TextureFlags flags, GLib2.Error** error) nothrow;
int clutter_texture_set_from_yuv_data(Texture* this_, ubyte* data, int width, int height, TextureFlags flags, GLib2.Error** error) nothrow;
void clutter_texture_set_keep_aspect_ratio(Texture* this_, int keep_aspect) nothrow;
void clutter_texture_set_load_async(Texture* this_, int load_async) nothrow;
void clutter_texture_set_load_data_async(Texture* this_, int load_async) nothrow;
void clutter_texture_set_pick_with_alpha(Texture* this_, int pick_with_alpha) nothrow;
void clutter_texture_set_repeat(Texture* this_, int repeat_x, int repeat_y) nothrow;
void clutter_texture_set_sync_size(Texture* this_, int sync_size) nothrow;
Timeline* /*new*/ clutter_timeline_new(uint msecs) nothrow;
void clutter_timeline_add_marker_at_time(Timeline* this_, char* marker_name, uint msecs) nothrow;
void clutter_timeline_advance(Timeline* this_, uint msecs) nothrow;
void clutter_timeline_advance_to_marker(Timeline* this_, char* marker_name) nothrow;
Timeline* /*new*/ clutter_timeline_clone(Timeline* this_) nothrow;
int clutter_timeline_get_auto_reverse(Timeline* this_) nothrow;
uint clutter_timeline_get_delay(Timeline* this_) nothrow;
uint clutter_timeline_get_delta(Timeline* this_) nothrow;
TimelineDirection clutter_timeline_get_direction(Timeline* this_) nothrow;
uint clutter_timeline_get_duration(Timeline* this_) nothrow;
uint clutter_timeline_get_elapsed_time(Timeline* this_) nothrow;
int clutter_timeline_get_loop(Timeline* this_) nothrow;
double clutter_timeline_get_progress(Timeline* this_) nothrow;
int clutter_timeline_has_marker(Timeline* this_, char* marker_name) nothrow;
int clutter_timeline_is_playing(Timeline* this_) nothrow;
char** /*new*/ clutter_timeline_list_markers(Timeline* this_, int msecs, /*out*/ size_t* n_markers) nothrow;
void clutter_timeline_pause(Timeline* this_) nothrow;
void clutter_timeline_remove_marker(Timeline* this_, char* marker_name) nothrow;
void clutter_timeline_rewind(Timeline* this_) nothrow;
void clutter_timeline_set_auto_reverse(Timeline* this_, int reverse) nothrow;
void clutter_timeline_set_delay(Timeline* this_, uint msecs) nothrow;
void clutter_timeline_set_direction(Timeline* this_, TimelineDirection direction) nothrow;
void clutter_timeline_set_duration(Timeline* this_, uint msecs) nothrow;
void clutter_timeline_set_loop(Timeline* this_, int loop) nothrow;
void clutter_timeline_skip(Timeline* this_, uint msecs) nothrow;
void clutter_timeline_start(Timeline* this_) nothrow;
void clutter_timeline_stop(Timeline* this_) nothrow;
uint clutter_timeout_pool_add(TimeoutPool* this_, uint fps, GLib2.SourceFunc func, void* data, GLib2.DestroyNotify notify) nothrow;
void clutter_timeout_pool_remove(TimeoutPool* this_, uint id_) nothrow;
TimeoutPool* clutter_timeout_pool_new(int priority) nothrow;
Units* /*new*/ clutter_units_copy(Units* this_) nothrow;
void clutter_units_free(Units* this_) nothrow;
void clutter_units_from_cm(Units* this_, float cm) nothrow;
void clutter_units_from_em(Units* this_, float em) nothrow;
void clutter_units_from_em_for_font(Units* this_, char* font_name, float em) nothrow;
void clutter_units_from_mm(Units* this_, float mm) nothrow;
void clutter_units_from_pixels(Units* this_, int px) nothrow;
void clutter_units_from_pt(Units* this_, float pt) nothrow;
int clutter_units_from_string(Units* this_, char* str) nothrow;
UnitType clutter_units_get_unit_type(Units* this_) nothrow;
float clutter_units_get_unit_value(Units* this_) nothrow;
float clutter_units_to_pixels(Units* this_) nothrow;
char* /*new*/ clutter_units_to_string(Units* this_) nothrow;
Vertex* /*new*/ clutter_vertex_new(float x, float y, float z) nothrow;
Vertex* /*new*/ clutter_vertex_copy(Vertex* this_) nothrow;
int clutter_vertex_equal(Vertex* this_, Vertex* vertex_b) nothrow;
void clutter_vertex_free(Vertex* this_) nothrow;
void clutter_base_init() nothrow;
void clutter_cairo_set_source_color(cairo.Context* cr, Color* color) nothrow;
int clutter_check_version(uint major, uint minor, uint micro) nothrow;
int clutter_check_windowing_backend(char* backend_type) nothrow;
void clutter_clear_glyph_cache() nothrow;
void clutter_do_event(Event* event) nothrow;
int clutter_events_pending() nothrow;
int clutter_feature_available(FeatureFlags feature) nothrow;
FeatureFlags clutter_feature_get_all() nothrow;
uint clutter_frame_source_add(uint fps, GLib2.SourceFunc func, void* data) nothrow;
uint clutter_frame_source_add_full(int priority, uint fps, GLib2.SourceFunc func, void* data, GLib2.DestroyNotify notify) nothrow;
int clutter_get_accessibility_enabled() nothrow;
Actor* clutter_get_actor_by_gid(uint id_) nothrow;
Event* clutter_get_current_event() nothrow;
uint clutter_get_current_event_time() nothrow;
int clutter_get_debug_enabled() nothrow;
Backend* clutter_get_default_backend() nothrow;
uint clutter_get_default_frame_rate() nothrow;
TextDirection clutter_get_default_text_direction() nothrow;
FontFlags clutter_get_font_flags() nothrow;
Pango.FontMap* clutter_get_font_map() nothrow;
InputDevice* clutter_get_input_device_for_id(int id_) nothrow;
Actor* clutter_get_keyboard_grab() nothrow;
int clutter_get_motion_events_enabled() nothrow;
GLib2.OptionGroup* /*new*/ clutter_get_option_group() nothrow;
GLib2.OptionGroup* /*new*/ clutter_get_option_group_without_init() nothrow;
Actor* clutter_get_pointer_grab() nothrow;
char* clutter_get_script_id(GObject2.Object* gobject) nothrow;
int clutter_get_show_fps() nothrow;
c_ulong clutter_get_timestamp() nothrow;
void clutter_grab_keyboard(Actor* actor) nothrow;
void clutter_grab_pointer(Actor* actor) nothrow;
void clutter_grab_pointer_for_device(Actor* actor, int id_) nothrow;
InitError clutter_init(/*inout*/ int* argc, /*inout*/ char*** argv=null) nothrow;
GLib2.Quark clutter_init_error_quark() nothrow;
InitError clutter_init_with_args(/*inout*/ int* argc, /*inout*/ char*** argv, char* parameter_string, GLib2.OptionEntry* entries, char* translation_domain, GLib2.Error** error) nothrow;
uint clutter_keysym_to_unicode(uint keyval) nothrow;
void clutter_main() nothrow;
int clutter_main_level() nothrow;
void clutter_main_quit() nothrow;
GObject2.ParamSpec* clutter_param_spec_color(char* name, char* nick, char* blurb, Color* default_value, GObject2.ParamFlags flags) nothrow;
GObject2.ParamSpec* /*new*/ clutter_param_spec_fixed(char* name, char* nick, char* blurb, Cogl.Fixed minimum, Cogl.Fixed maximum, Cogl.Fixed default_value, GObject2.ParamFlags flags) nothrow;
GObject2.ParamSpec* clutter_param_spec_units(char* name, char* nick, char* blurb, UnitType default_type, float minimum, float maximum, float default_value, GObject2.ParamFlags flags) nothrow;
void clutter_redraw(Stage* stage) nothrow;
GLib2.Quark clutter_script_error_quark() nothrow;
void clutter_set_default_frame_rate(uint frames_per_sec) nothrow;
void clutter_set_font_flags(FontFlags flags) nothrow;
void clutter_set_motion_events_enabled(int enable) nothrow;
GLib2.Quark clutter_texture_error_quark() nothrow;
uint clutter_threads_add_frame_source(uint fps, GLib2.SourceFunc func, void* data) nothrow;
uint clutter_threads_add_frame_source_full(int priority, uint fps, GLib2.SourceFunc func, void* data, GLib2.DestroyNotify notify) nothrow;
uint clutter_threads_add_idle(GLib2.SourceFunc func, void* data) nothrow;
uint clutter_threads_add_idle_full(int priority, GLib2.SourceFunc func, void* data, GLib2.DestroyNotify notify) nothrow;
uint clutter_threads_add_repaint_func(GLib2.SourceFunc func, void* data, GLib2.DestroyNotify notify) nothrow;
uint clutter_threads_add_timeout(uint interval, GLib2.SourceFunc func, void* data) nothrow;
uint clutter_threads_add_timeout_full(int priority, uint interval, GLib2.SourceFunc func, void* data, GLib2.DestroyNotify notify) nothrow;
void clutter_threads_enter() nothrow;
void clutter_threads_init() nothrow;
void clutter_threads_leave() nothrow;
void clutter_threads_remove_repaint_func(uint handle_id) nothrow;
void clutter_threads_set_lock_functions(GObject2.Callback enter_fn, GObject2.Callback leave_fn) nothrow;
void clutter_ungrab_keyboard() nothrow;
void clutter_ungrab_pointer() nothrow;
void clutter_ungrab_pointer_for_device(int id_) nothrow;
uint clutter_unicode_to_keysym(uint wc) nothrow;
int clutter_util_next_p2(int a) nothrow;
Color* clutter_value_get_color(GObject2.Value* value) nothrow;
Cogl.Fixed clutter_value_get_fixed(GObject2.Value* value) nothrow;
float* clutter_value_get_shader_float(GObject2.Value* value, size_t* length) nothrow;
int* clutter_value_get_shader_int(GObject2.Value* value, size_t* length) nothrow;
float* clutter_value_get_shader_matrix(GObject2.Value* value, /*out*/ size_t* length) nothrow;
Units* clutter_value_get_units(GObject2.Value* value) nothrow;
void clutter_value_set_color(GObject2.Value* value, Color* color) nothrow;
void clutter_value_set_fixed(GObject2.Value* value, Cogl.Fixed fixed_) nothrow;
void clutter_value_set_shader_float(GObject2.Value* value, int size, float* floats) nothrow;
void clutter_value_set_shader_int(GObject2.Value* value, int size, int* ints) nothrow;
void clutter_value_set_shader_matrix(GObject2.Value* value, int size, float* matrix) nothrow;
void clutter_value_set_units(GObject2.Value* value, Units* units) nothrow;
}

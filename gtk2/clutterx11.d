// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/ClutterX11-1.0.gir"

module ClutterX11;
public import gtk2.atk;
alias gtk2.atk Atk;
public import gtk2.clutter;
alias gtk2.clutter Clutter;
public import gtk2.cogl;
alias gtk2.cogl Cogl;
public import gtk2.coglpango;
alias gtk2.coglpango CoglPango;
public import gtk2.gl;
alias gtk2.gl GL;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;
public import gtk2.gio2;
alias gtk2.gio2 Gio2;
public import gtk2.json;
alias gtk2.json Json;
public import gtk2.pango;
alias gtk2.pango Pango;
public import gtk2.pangocairo;
alias gtk2.pangocairo PangoCairo;
public import gtk2.pangoft2;
alias gtk2.pangoft2 PangoFT2;
public import gtk2.cairo;
alias gtk2.cairo cairo;
public import gtk2.fontconfig2;
alias gtk2.fontconfig2 fontconfig2;
public import gtk2.freetype2;
alias gtk2.freetype2 freetype2;
public import gtk2.xlib2;
alias gtk2.xlib2 xlib2;

// package: "clutter-x11-1.0";
// C header: "clutter/x11/clutter-x11.h";

// c:symbol-prefixes: ["clutter_x11"]
// c:identifier-prefixes: ["ClutterX11"]

// module ClutterX11;


// VERSION: 0.6
// Filter function for X11 native events.
// RETURNS: the result of the filtering
// <xev>: Native X11 event structure
// <cev>: Clutter event structure
// <data>: user data passed to the filter function
extern (C) alias Clutter.X11FilterReturn function (xlib.XEvent* xev, Clutter.Event* cev, void* data) nothrow FilterFunc;

// Return values for the #ClutterX11FilterFunc function.
enum FilterReturn /* Version 0.6 */ {
   CONTINUE = 0,
   TRANSLATE = 1,
   REMOVE = 2
}
// The #ClutterX11TexturePixmap structure contains only private data
struct TexturePixmap /* : Clutter.Texture */ /* Version 0.8 */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent texture;
   Clutter.Texture parent;
   private TexturePixmapPrivate* priv;


   // VERSION: 0.8
   // Creates a new #ClutterX11TexturePixmap which can be used to display the
   // contents of an X11 Pixmap inside a Clutter scene graph
   // RETURNS: A new #ClutterX11TexturePixmap
   static TexturePixmap* new_()() nothrow {
      return clutter_x11_texture_pixmap_new();
   }
   static auto opCall()() {
      return clutter_x11_texture_pixmap_new();
   }

   // VERSION: 0.8
   // Creates a new #ClutterX11TexturePixmap for @pixmap
   // RETURNS: A new #ClutterX11TexturePixmap bound to the given X Pixmap
   // <pixmap>: the X Pixmap to which this texture should be bound
   static TexturePixmap* new_with_pixmap()(xlib.Pixmap pixmap) nothrow {
      return clutter_x11_texture_pixmap_new_with_pixmap(pixmap);
   }
   static auto opCall()(xlib.Pixmap pixmap) {
      return clutter_x11_texture_pixmap_new_with_pixmap(pixmap);
   }

   // VERSION: 0.8
   // Creates a new #ClutterX11TexturePixmap for @window
   // RETURNS: A new #ClutterX11TexturePixmap bound to the given X window.
   // <window>: the X window to which this texture should be bound
   static TexturePixmap* new_with_window()(xlib.Window window) nothrow {
      return clutter_x11_texture_pixmap_new_with_window(window);
   }
   static auto opCall()(xlib.Window window) {
      return clutter_x11_texture_pixmap_new_with_window(window);
   }

   // VERSION: 0.8
   // Enables or disables the automatic updates ot @texture in case the backing
   // pixmap or window is damaged
   // <setting>: %TRUE to enable automatic updates
   void set_automatic()(int setting) nothrow {
      clutter_x11_texture_pixmap_set_automatic(&this, setting);
   }

   // VERSION: 0.8
   // Sets the X Pixmap to which the texture should be bound.
   // <pixmap>: the X Pixmap to which the texture should be bound
   void set_pixmap()(xlib.Pixmap pixmap) nothrow {
      clutter_x11_texture_pixmap_set_pixmap(&this, pixmap);
   }

   // VERSION: 0.8
   // Sets up a suitable pixmap for the window, using the composite and damage
   // extensions if possible, and then calls
   // clutter_x11_texture_pixmap_set_pixmap().
   // 
   // If you want to display a window in a #ClutterTexture, you probably want
   // this function, or its older sister, clutter_glx_texture_pixmap_set_window().
   // 
   // This function has no effect unless the XComposite extension is available.
   // <window>: the X window to which the texture should be bound
   // <automatic>: %TRUE for automatic window updates, %FALSE for manual.
   void set_window()(xlib.Window window, int automatic) nothrow {
      clutter_x11_texture_pixmap_set_window(&this, window, automatic);
   }

   // VERSION: 0.8
   // Resets the texture's pixmap from its window, perhaps in response to the
   // pixmap's invalidation as the window changed size.
   void sync_window()() nothrow {
      clutter_x11_texture_pixmap_sync_window(&this);
   }

   // VERSION: 0.8
   // Performs the actual binding of texture to the current content of
   // the pixmap. Can be called to update the texture if the pixmap
   // content has changed.
   // <x>: the X coordinate of the area to update
   // <y>: the Y coordinate of the area to update
   // <width>: the width of the area to update
   // <height>: the height of the area to update
   void update_area()(int x, int y, int width, int height) nothrow {
      clutter_x11_texture_pixmap_update_area(&this, x, y, width, height);
   }

   // VERSION: 1.2
   // ::queue-damage-redraw is emitted to notify that some sub-region
   // of the texture has been changed (either by an automatic damage
   // update or by an explicit call to
   // clutter_x11_texture_pixmap_update_area). This usually means a
   // redraw needs to be queued for the actor.
   // 
   // The default handler will queue a clipped redraw in response to
   // the damage, using the assumption that the pixmap is being painted
   // to a rectangle covering the transformed allocation of the actor.
   // If you sub-class and change the paint method so this isn't true
   // then you must also provide your own damage signal handler to
   // queue a redraw that blocks this default behaviour.
   // <x>: The top left x position of the damage region
   // <y>: The top left y position of the damage region
   // <width>: The width of the damage region
   // <height>: The height of the damage region
   extern (C) alias static void function (TexturePixmap* this_, int x, int y, int width, int height, void* user_data=null) nothrow signal_queue_damage_redraw;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"queue-damage-redraw", CB/*:signal_queue_damage_redraw*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_queue_damage_redraw)||_ttmm!(CB, signal_queue_damage_redraw)()) {
      return signal_connect_data!()(&this, cast(char*)"queue-damage-redraw",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 0.8
   // The ::update-area signal is emitted to ask the texture to update its
   // content from its source pixmap.
   extern (C) alias static void function (TexturePixmap* this_, int object, int p0, int p1, int p2, void* user_data=null) nothrow signal_update_area;
   ulong signal_connect(string name:"update-area", CB/*:signal_update_area*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_update_area)||_ttmm!(CB, signal_update_area)()) {
      return signal_connect_data!()(&this, cast(char*)"update-area",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// The #ClutterX11TexturePixmapClass structure contains only private data
struct TexturePixmapClass /* Version 0.8 */ {
   private Clutter.TextureClass parent_class;

   // <x>: the X coordinate of the area to update
   // <y>: the Y coordinate of the area to update
   // <width>: the width of the area to update
   // <height>: the height of the area to update
   extern (C) void function (TexturePixmap* texture, int x, int y, int width, int height) nothrow update_area;
}

struct TexturePixmapPrivate {
}

struct XInputDevice {
}

enum XInputEventTypes {
   KEY_PRESS_EVENT = 0,
   KEY_RELEASE_EVENT = 1,
   BUTTON_PRESS_EVENT = 2,
   BUTTON_RELEASE_EVENT = 3,
   MOTION_NOTIFY_EVENT = 4,
   LAST_EVENT = 5
}

// Unintrospectable function: add_filter() / clutter_x11_add_filter()
// VERSION: 0.6
// Adds an event filter function.
// <func>: a filter function
// <data>: user data to be passed to the filter function, or %NULL
static void add_filter(AT0)(FilterFunc func, AT0 /*void*/ data) nothrow {
   clutter_x11_add_filter(func, UpCast!(void*)(data));
}


// VERSION: 0.8
// Disables the internal polling of X11 events in the main loop.
// 
// Libraries or applications calling this function will be responsible of
// polling all X11 events.
// 
// You also must call clutter_x11_handle_event() to let Clutter process
// events and maintain its internal state.
// 
// <warning>This function can only be called before calling
// clutter_init().</warning>
// 
// <note>Even with event handling disabled, Clutter will still select
// all the events required to maintain its internal state on the stage
// Window; compositors using Clutter and input regions to pass events
// through to application windows should not rely on an empty input
// region, and should instead clear it themselves explicitly using the
// XFixes extension.</note>
// 
// This function should not be normally used by applications.
static void disable_event_retrieval()() nothrow {
   clutter_x11_disable_event_retrieval();
}


// VERSION: 0.8
// Enables the use of the XInput extension if present on connected
// XServer and support built into Clutter. XInput allows for multiple
// pointing devices to be used.
// 
// This function must be called before clutter_init().
// 
// Since XInput might not be supported by the X server, you might
// want to use clutter_x11_has_xinput() to see if support was enabled.
static void enable_xinput()() nothrow {
   clutter_x11_enable_xinput();
}


// VERSION: 1.4
// Retrieves the group for the modifiers set in @event
// RETURNS: the group id
// <event>: a #ClutterEvent of type %CLUTTER_KEY_PRESS or %CLUTTER_KEY_RELEASE
static int event_get_key_group(AT0)(AT0 /*Clutter.Event*/ event) nothrow {
   return clutter_x11_event_get_key_group(UpCast!(Clutter.Event*)(event));
}


// Unintrospectable function: get_current_event_time() / clutter_x11_get_current_event_time()
// VERSION: 1.0
// Retrieves the timestamp of the last X11 event processed by
// Clutter. This might be different from the timestamp returned
// by clutter_get_current_event_time(), as Clutter may synthesize
// or throttle events.
// RETURNS: a timestamp, in milliseconds
static xlib.Time get_current_event_time()() nothrow {
   return clutter_x11_get_current_event_time();
}


// VERSION: 0.6
// Retrieves the pointer to the default display.
// RETURNS: the default display
static xlib.Display* get_default_display()() nothrow {
   return clutter_x11_get_default_display();
}


// VERSION: 0.6
// Gets the number of the default X Screen object.
// RETURNS: the number of the default screen
static int get_default_screen()() nothrow {
   return clutter_x11_get_default_screen();
}


// VERSION: 0.8
// DEPRECATED (v1.2) function: get_input_devices - Use clutter_device_manager_peek_devices() instead
// Retrieves a pointer to the list of input devices
// 
// 
// 
// pointer to the internal list of input devices; the returned list is
// owned by Clutter and should not be modified or freed
// RETURNS: a
static GLib2.SList* get_input_devices()() nothrow {
   return clutter_x11_get_input_devices();
}


// Unintrospectable function: get_root_window() / clutter_x11_get_root_window()
// VERSION: 0.6
// Retrieves the root window.
// RETURNS: the id of the root window
static xlib.Window get_root_window()() nothrow {
   return clutter_x11_get_root_window();
}


// VERSION: 0.8
// Gets the stage for a particular X window.  
// 
// does not exist for the window
// RETURNS: A #ClutterStage, or% NULL if a stage
// <win>: an X Window ID
static Clutter.Stage* get_stage_from_window()(xlib.Window win) nothrow {
   return clutter_x11_get_stage_from_window(win);
}


// Unintrospectable function: get_stage_visual() / clutter_x11_get_stage_visual()
// VERSION: 0.4
// DEPRECATED (v1.2) function: get_stage_visual - Use clutter_x11_get_visual_info() instead
// Returns an XVisualInfo suitable for creating a foreign window for the given
// stage. NOTE: It doesn't do as the name may suggest, which is return the
// XVisualInfo that was used to create an existing window for the given stage.
// 
// XXX: It might be best to deprecate this function and replace with something
// along the lines of clutter_backend_x11_get_foreign_visual () or perhaps
// clutter_stage_x11_get_foreign_visual ()
// 
// foreign stage. Use XFree() to free the returned value instead
// RETURNS: An XVisualInfo suitable for creating a
// <stage>: a #ClutterStage
static xlib.XVisualInfo* /*new*/ get_stage_visual(AT0)(AT0 /*Clutter.Stage*/ stage) nothrow {
   return clutter_x11_get_stage_visual(UpCast!(Clutter.Stage*)(stage));
}


// Unintrospectable function: get_stage_window() / clutter_x11_get_stage_window()
// VERSION: 0.4
// Gets the stages X Window.
// RETURNS: An XID for the stage window.
// <stage>: a #ClutterStage
static xlib.Window get_stage_window(AT0)(AT0 /*Clutter.Stage*/ stage) nothrow {
   return clutter_x11_get_stage_window(UpCast!(Clutter.Stage*)(stage));
}


// VERSION: 1.2
// Retrieves whether the Clutter X11 backend is using ARGB visuals by default
// RETURNS: %TRUE if ARGB visuals are queried by default
static int get_use_argb_visual()() nothrow {
   return clutter_x11_get_use_argb_visual();
}


// Unintrospectable function: get_visual_info() / clutter_x11_get_visual_info()
// VERSION: 1.2
// Retrieves the <structname>XVisualInfo</structname> used by the Clutter X11
// backend.
// 
// <varname>None</varname>. The returned value should be freed using XFree()
// when done
// RETURNS: a <structname>XVisualInfo</structname>, or
static xlib.XVisualInfo* /*new*/ get_visual_info()() nothrow {
   return clutter_x11_get_visual_info();
}


// VERSION: 0.8
// This function processes a single X event; it can be used to hook
// into external X11 event processing (for example, a GDK filter
// function).
// 
// If clutter_x11_disable_event_retrieval() has been called, you must
// let this function process events to update Clutter's internal state.
// 
// indicates that Clutter has internally handled the event and the
// caller should do no further processing. %CLUTTER_X11_FILTER_CONTINUE
// indicates that Clutter is either not interested in the event,
// or has used the event to update internal state without taking
// any exclusive action. %CLUTTER_X11_FILTER_TRANSLATE will not
// occur.
// RETURNS: #ClutterX11FilterReturn. %CLUTTER_X11_FILTER_REMOVE
// <xevent>: pointer to XEvent structure
static Clutter.X11FilterReturn handle_event(AT0)(AT0 /*xlib.XEvent*/ xevent) nothrow {
   return clutter_x11_handle_event(UpCast!(xlib.XEvent*)(xevent));
}


// Retrieves whether Clutter is running on an X11 server with the
// XComposite extension
// RETURNS: %TRUE if the XComposite extension is available
static int has_composite_extension()() nothrow {
   return clutter_x11_has_composite_extension();
}


// VERSION: 0.8
// Queries the X11 backend to check if event collection has been disabled.
// RETURNS: TRUE if event retrival has been disabled. FALSE otherwise.
static int has_event_retrieval()() nothrow {
   return clutter_x11_has_event_retrieval();
}


// VERSION: 0.8
// Gets whether Clutter has XInput support.
// 
// and XInput support is available at run time.
// RETURNS: %TRUE if Clutter was compiled with XInput support
static int has_xinput()() nothrow {
   return clutter_x11_has_xinput();
}


// Unintrospectable function: remove_filter() / clutter_x11_remove_filter()
// VERSION: 0.6
// Removes the given filter function.
// <func>: a filter function
// <data>: user data to be passed to the filter function, or %NULL
static void remove_filter(AT0)(FilterFunc func, AT0 /*void*/ data) nothrow {
   clutter_x11_remove_filter(func, UpCast!(void*)(data));
}


// VERSION: 0.8
// Sets the display connection Clutter should use; must be called
// before clutter_init(), clutter_init_with_args() or other functions
// pertaining Clutter's initialization process.
// 
// If you are parsing the command line arguments by retrieving Clutter's
// #GOptionGroup with clutter_get_option_group() and calling
// g_option_context_parse() yourself, you should also call
// clutter_x11_set_display() before g_option_context_parse().
// <xdpy>: pointer to a X display connection.
static void set_display(AT0)(AT0 /*xlib.Display*/ xdpy) nothrow {
   clutter_x11_set_display(UpCast!(xlib.Display*)(xdpy));
}


// VERSION: 0.4
// Target the #ClutterStage to use an existing external X Window
// RETURNS: %TRUE if foreign window is valid
// <stage>: a #ClutterStage
// <xwindow>: an existing X Window id
static int set_stage_foreign(AT0)(AT0 /*Clutter.Stage*/ stage, xlib.Window xwindow) nothrow {
   return clutter_x11_set_stage_foreign(UpCast!(Clutter.Stage*)(stage), xwindow);
}


// VERSION: 1.2
// Sets whether the Clutter X11 backend should request ARGB visuals by default
// or not.
// 
// By default, Clutter requests RGB visuals.
// 
// <note>If no ARGB visuals are found, the X11 backend will fall back to
// requesting a RGB visual instead.</note>
// 
// ARGB visuals are required for the #ClutterStage:use-alpha property to work.
// 
// <note>This function can only be called once, and before clutter_init() is
// called.</note>
// <use_argb>: %TRUE if ARGB visuals should be requested by default
static void set_use_argb_visual()(int use_argb) nothrow {
   clutter_x11_set_use_argb_visual(use_argb);
}


// VERSION: 0.6
// Traps every X error until clutter_x11_untrap_x_errors() is called.
static void trap_x_errors()() nothrow {
   clutter_x11_trap_x_errors();
}


// VERSION: 0.4
// Removes the X error trap and returns the current status.
// RETURNS: the trapped error code, or 0 for success
static int untrap_x_errors()() nothrow {
   return clutter_x11_untrap_x_errors();
}


// C prototypes:

extern (C) {
TexturePixmap* clutter_x11_texture_pixmap_new() nothrow;
TexturePixmap* clutter_x11_texture_pixmap_new_with_pixmap(xlib.Pixmap pixmap) nothrow;
TexturePixmap* clutter_x11_texture_pixmap_new_with_window(xlib.Window window) nothrow;
void clutter_x11_texture_pixmap_set_automatic(TexturePixmap* this_, int setting) nothrow;
void clutter_x11_texture_pixmap_set_pixmap(TexturePixmap* this_, xlib.Pixmap pixmap) nothrow;
void clutter_x11_texture_pixmap_set_window(TexturePixmap* this_, xlib.Window window, int automatic) nothrow;
void clutter_x11_texture_pixmap_sync_window(TexturePixmap* this_) nothrow;
void clutter_x11_texture_pixmap_update_area(TexturePixmap* this_, int x, int y, int width, int height) nothrow;
void clutter_x11_add_filter(FilterFunc func, void* data) nothrow;
void clutter_x11_disable_event_retrieval() nothrow;
void clutter_x11_enable_xinput() nothrow;
int clutter_x11_event_get_key_group(Clutter.Event* event) nothrow;
xlib.Time clutter_x11_get_current_event_time() nothrow;
xlib.Display* clutter_x11_get_default_display() nothrow;
int clutter_x11_get_default_screen() nothrow;
GLib2.SList* clutter_x11_get_input_devices() nothrow;
xlib.Window clutter_x11_get_root_window() nothrow;
Clutter.Stage* clutter_x11_get_stage_from_window(xlib.Window win) nothrow;
xlib.XVisualInfo* /*new*/ clutter_x11_get_stage_visual(Clutter.Stage* stage) nothrow;
xlib.Window clutter_x11_get_stage_window(Clutter.Stage* stage) nothrow;
int clutter_x11_get_use_argb_visual() nothrow;
xlib.XVisualInfo* /*new*/ clutter_x11_get_visual_info() nothrow;
Clutter.X11FilterReturn clutter_x11_handle_event(xlib.XEvent* xevent) nothrow;
int clutter_x11_has_composite_extension() nothrow;
int clutter_x11_has_event_retrieval() nothrow;
int clutter_x11_has_xinput() nothrow;
void clutter_x11_remove_filter(FilterFunc func, void* data) nothrow;
void clutter_x11_set_display(xlib.Display* xdpy) nothrow;
int clutter_x11_set_stage_foreign(Clutter.Stage* stage, xlib.Window xwindow) nothrow;
void clutter_x11_set_use_argb_visual(int use_argb) nothrow;
void clutter_x11_trap_x_errors() nothrow;
int clutter_x11_untrap_x_errors() nothrow;
}

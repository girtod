// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/Cogl-1.0.gir"

module Cogl;
public import gtk2.gl;
alias gtk2.gl GL;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;

// package: "cogl-1.0";
// C header: "cogl/cogl.h";

// c:symbol-prefixes: ["cogl"]
// c:identifier-prefixes: ["Cogl"]

// module Cogl;

//  --- mixin/Cogl__MODULE_HEAD.d --->

// Cogl-1.0.gir as of 1.9.4 is missing some things;
// let's add just enough to make it usable:

// Really X11, but this will do for now.

private alias void XVisualInfo;
private alias void XEvent;

// <--- mixin/Cogl__MODULE_HEAD.d ---


// Integer representation of an angle such that 1024 corresponds to
// full circle (i.e., 2 * pi).
alias int Angle;

// Type used for storing references to cogl objects, the CoglHandle is
// a fully opaque type without any public data members.
alias void* Handle;
enum int AFIRST_BIT = 64;
enum int A_BIT = 16;
struct Attribute {

   // Unintrospectable method: get_buffer() / cogl_attribute_get_buffer()
   // VERSION: 1.10
   // cogl_attribute_set_buffer() or cogl_attribute_new().
   // RETURNS: the #CoglAttributeBuffer that was set with
   AttributeBuffer* get_buffer()() nothrow {
      return cogl_attribute_get_buffer(&this);
   }

   // VERSION: 1.10
   // cogl_attribute_set_normalized().
   // RETURNS: the value of the normalized property set with
   int get_normalized()() nothrow {
      return cogl_attribute_get_normalized(&this);
   }

   // VERSION: 1.10
   // Sets a new #CoglAttributeBuffer for the attribute.
   // <attribute_buffer>: A #CoglAttributeBuffer
   void set_buffer(AT0)(AT0 /*AttributeBuffer*/ attribute_buffer) nothrow {
      cogl_attribute_set_buffer(&this, UpCast!(AttributeBuffer*)(attribute_buffer));
   }

   // VERSION: 1.10
   // Sets whether fixed point attribute types are mapped to the range
   // 0→1. For example when this property is TRUE and a
   // %COGL_ATTRIBUTE_TYPE_UNSIGNED_BYTE type is used then the value 255
   // will be mapped to 1.0.
   // 
   // The default value of this property depends on the name of the
   // attribute. For the builtin properties cogl_color_in and
   // cogl_normal_in it will default to TRUE and for all other names it
   // will default to FALSE.
   // <normalized>: The new value for the normalized property.
   void set_normalized()(int normalized) nothrow {
      cogl_attribute_set_normalized(&this, normalized);
   }

   // Unintrospectable function: new() / cogl_attribute_new()
   // VERSION: 1.4
   // Describes the layout for a list of vertex attribute values (For
   // example, a list of texture coordinates or colors).
   // 
   // The @name is used to access the attribute inside a GLSL vertex
   // shader and there are some special names you should use if they are
   // applicable:
   // <itemizedlist>
   // <listitem>"cogl_position_in" (used for vertex positions)</listitem>
   // <listitem>"cogl_color_in" (used for vertex colors)</listitem>
   // <listitem>"cogl_tex_coord0_in", "cogl_tex_coord1", ...
   // (used for vertex texture coordinates)</listitem>
   // <listitem>"cogl_normal_in" (used for vertex normals)</listitem>
   // </itemizedlist>
   // 
   // The attribute values corresponding to different vertices can either
   // be tightly packed or interleaved with other attribute values. For
   // example it's common to define a structure for a single vertex like:
   // |[
   // typedef struct
   // {
   // float x, y, z; /<!-- -->* position attribute *<!-- -->/
   // float s, t; /<!-- -->* texture coordinate attribute *<!-- -->/
   // } MyVertex;
   // ]|
   // 
   // And then create an array of vertex data something like:
   // |[
   // MyVertex vertices[100] = { .... }
   // ]|
   // 
   // In this case, to describe either the position or texture coordinate
   // attribute you have to move <pre>sizeof (MyVertex)</pre> bytes to
   // move from one vertex to the next.  This is called the attribute
   // @stride. If you weren't interleving attributes and you instead had
   // a packed array of float x, y pairs then the attribute stride would
   // be <pre>(2 * sizeof (float))</pre>. So the @stride is the number of
   // bytes to move to find the attribute value of the next vertex.
   // 
   // Normally a list of attributes starts at the beginning of an array.
   // So for the <pre>MyVertex</pre> example above the @offset is the
   // offset inside the <pre>MyVertex</pre> structure to the first
   // component of the attribute. For the texture coordinate attribute
   // the offset would be <pre>offsetof (MyVertex, s)</pre> or instead of
   // using the offsetof macro you could use <pre>sizeof (float) * 3</pre>.
   // If you've divided your @array into blocks of non-interleved
   // attributes then you will need to calculate the @offset as the
   // number of bytes in blocks preceding the attribute you're
   // describing.
   // 
   // An attribute often has more than one component. For example a color
   // is often comprised of 4 red, green, blue and alpha @components, and a
   // position may be comprised of 2 x and y @components. You should aim
   // to keep the number of components to a minimum as more components
   // means more data needs to be mapped into the GPU which can be a
   // bottlneck when dealing with a large number of vertices.
   // 
   // Finally you need to specify the component data type. Here you
   // should aim to use the smallest type that meets your precision
   // requirements. Again the larger the type then more data needs to be
   // mapped into the GPU which can be a bottlneck when dealing with
   // a large number of vertices.
   // 
   // layout for a list of attribute values stored in @array.
   // RETURNS: A newly allocated #CoglAttribute describing the
   // <attribute_buffer>: The #CoglAttributeBuffer containing the actual attribute data
   // <name>: The name of the attribute (used to reference it from GLSL)
   // <stride>: The number of bytes to jump to get to the next attribute value for the next vertex. (Usually <pre>sizeof (MyVertex)</pre>)
   // <offset>: The byte offset from the start of @attribute_buffer for the first attribute value. (Usually <pre>offsetof (MyVertex, component0)</pre>
   // <components>: The number of components (e.g. 4 for an rgba color or 3 for and (x,y,z) position)
   // <type>: FIXME
   static Attribute* new_(AT0, AT1)(AT0 /*AttributeBuffer*/ attribute_buffer, AT1 /*char*/ name, size_t stride, size_t offset, int components, AttributeType type) nothrow {
      return cogl_attribute_new(UpCast!(AttributeBuffer*)(attribute_buffer), toCString!(char*)(name), stride, offset, components, type);
   }
}

struct AttributeBuffer {

   // Unintrospectable function: new() / cogl_attribute_buffer_new()
   // VERSION: 1.4
   // Declares a new #CoglAttributeBuffer of @size bytes to contain arrays of vertex
   // attribute data. Once declared, data can be set using cogl_buffer_set_data()
   // or by mapping it into the application's address space using cogl_buffer_map().
   // 
   // If @data isn't %NULL then @size bytes will be read from @data and
   // immediately copied into the new buffer.
   // <bytes>: The number of bytes to allocate for vertex attribute data.
   // <data>: An optional pointer to vertex data to upload immediately.
   static AttributeBuffer* new_(AT0)(size_t bytes, AT0 /*void*/ data) nothrow {
      return cogl_attribute_buffer_new(bytes, UpCast!(void*)(data));
   }
}

// Data types for the components of a vertex attribute.
enum AttributeType /* Version 1.0 */ {
   BYTE = 5120,
   UNSIGNED_BYTE = 5121,
   SHORT = 5122,
   UNSIGNED_SHORT = 5123,
   FLOAT = 5126
}
enum int BGR_BIT = 32;
struct Bitmap {

   // VERSION: 1.0
   // Parses an image file enough to extract the width and height
   // of the bitmap.
   // RETURNS: %TRUE if the image was successfully parsed
   // <filename>: the file to check
   // <width>: return location for the bitmap width, or %NULL
   // <height>: return location for the bitmap height, or %NULL
   static int get_size_from_file(AT0)(AT0 /*char*/ filename, /*out*/ int* width, /*out*/ int* height) nothrow {
      return cogl_bitmap_get_size_from_file(toCString!(char*)(filename), width, height);
   }

   // Unintrospectable function: new_from_buffer() / cogl_bitmap_new_from_buffer()
   // VERSION: 1.8
   // Wraps some image data that has been uploaded into a #CoglBuffer as
   // a #CoglBitmap. The data is not copied in this process.
   // RETURNS: a #CoglBitmap encapsulating the given @buffer.
   // <buffer>: A #CoglBuffer containing image data
   // <format>: The #CoglPixelFormat defining the format of the image data in the given @buffer.
   // <width>: The width of the image data in the given @buffer.
   // <height>: The height of the image data in the given @buffer.
   // <rowstride>: The rowstride in bytes of the image data in the given @buffer.
   // <offset>: The offset into the given @buffer to the first pixel that should be considered part of the #CoglBitmap.
   static Bitmap* new_from_buffer(AT0)(AT0 /*Buffer*/ buffer, PixelFormat format, int width, int height, int rowstride, int offset) nothrow {
      return cogl_bitmap_new_from_buffer(UpCast!(Buffer*)(buffer), format, width, height, rowstride, offset);
   }

   // Unintrospectable function: new_from_file() / cogl_bitmap_new_from_file()
   // VERSION: 1.0
   // Loads an image file from disk. This function can be safely called from
   // within a thread.
   // 
   // %NULL if loading the image failed.
   // RETURNS: a #CoglBitmap to the new loaded image data, or
   // <filename>: the file to load.
   static Bitmap* new_from_file(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_bitmap_new_from_file(toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }
}


// Error codes that can be thrown when performing bitmap
// operations. Note that gdk_pixbuf_new_from_file() can also throw
// errors directly from the underlying image loading library. For
// example, if GdkPixbuf is used then errors #GdkPixbufError<!-- -->s
// will be used directly.
enum BitmapError /* Version 1.4 */ {
   FAILED = 0,
   UNKNOWN_TYPE = 1,
   CORRUPT_IMAGE = 2
}
// Error enumeration for the blend strings parser
enum BlendStringError /* Version 1.0 */ {
   PARSE_ERROR = 0,
   ARGUMENT_PARSE_ERROR = 1,
   INVALID_ERROR = 2,
   GPU_UNSUPPORTED_ERROR = 3
}
struct Buffer {

   // VERSION: 1.2
   // Retrieves the size of buffer
   // RETURNS: the size of the buffer in bytes
   uint get_size()() nothrow {
      return cogl_buffer_get_size(&this);
   }

   // VERSION: 1.2
   // Retrieves the update hints set using cogl_buffer_set_update_hint()
   // RETURNS: the #CoglBufferUpdateHint currently used by the buffer
   BufferUpdateHint get_update_hint()() nothrow {
      return cogl_buffer_get_update_hint(&this);
   }

   // Unintrospectable method: map() / cogl_buffer_map()
   // VERSION: 1.2
   // Maps the buffer into the application address space for direct access.
   // 
   // It is strongly recommended that you pass
   // %COGL_BUFFER_MAP_HINT_DISCARD as a hint if you are going to replace
   // all the buffer's data. This way if the buffer is currently being
   // used by the GPU then the driver won't have to stall the CPU and
   // wait for the hardware to finish because it can instead allocate a
   // new buffer to map.
   // 
   // The behaviour is undefined if you access the buffer in a way
   // conflicting with the @access mask you pass. It is also an error to
   // release your last reference while the buffer is mapped.
   // RETURNS: A pointer to the mapped memory or %NULL is the call fails
   // <access>: how the mapped buffer will be used by the application
   // <hints>: A mask of #CoglBufferMapHint<!-- -->s that tell Cogl how the data will be modified once mapped.
   void* map()(BufferAccess access, BufferMapHint hints) nothrow {
      return cogl_buffer_map(&this, access, hints);
   }

   // VERSION: 1.2
   // Updates part of the buffer with new data from @data. Where to put this new
   // data is controlled by @offset and @offset + @data should be less than the
   // buffer size.
   // RETURNS: %TRUE is the operation succeeded, %FALSE otherwise
   // <offset>: destination offset (in bytes) in the buffer
   // <data>: a pointer to the data to be copied into the buffer
   // <size>: number of bytes to copy
   int set_data(AT0)(size_t offset, AT0 /*void*/ data, size_t size) nothrow {
      return cogl_buffer_set_data(&this, offset, UpCast!(void*)(data), size);
   }

   // VERSION: 1.2
   // Sets the update hint on a buffer. See #CoglBufferUpdateHint for a description
   // of the available hints.
   // <hint>: the new hint
   void set_update_hint()(BufferUpdateHint hint) nothrow {
      cogl_buffer_set_update_hint(&this, hint);
   }

   // VERSION: 1.2
   // Unmaps a buffer previously mapped by cogl_buffer_map().
   void unmap()() nothrow {
      cogl_buffer_unmap(&this);
   }
}

// The access hints for cogl_buffer_set_update_hint()
enum BufferAccess /* Version 1.2 */ {
   READ = 1,
   WRITE = 2,
   READ_WRITE = 3
}
// Types of auxiliary buffers
enum BufferBit /* Version 1.0 */ {
   COLOR = 1,
   DEPTH = 2,
   STENCIL = 4
}

// Hints to Cogl about how you are planning to modify the data once it
// is mapped.
enum BufferMapHint /* Version 1.4 */ {
   DISCARD = 1
}
// Target flags for FBOs.
enum BufferTarget /* Version 0.8 */ {
   WINDOW_BUFFER = 2,
   OFFSCREEN_BUFFER = 4
}

// The update hint on a buffer allows the user to give some detail on how often
// the buffer data is going to be updated.
enum BufferUpdateHint /* Version 1.2 */ {
   STATIC = 0,
   DYNAMIC = 1,
   STREAM = 2
}

// A structure for holding a color definition. The contents of
// the CoglColor structure are private and should never by accessed
// directly.
struct Color /* Version 1.0 */ {
   private ubyte red, green, blue, alpha;
   private uint padding0, padding1, padding2;


   // Unintrospectable method: copy() / cogl_color_copy()
   // VERSION: 1.0
   // Creates a copy of @color
   // 
   // to free the allocate resources
   // RETURNS: a newly-allocated #CoglColor. Use cogl_color_free()
   Color* copy()() nothrow {
      return cogl_color_copy(&this);
   }

   // VERSION: 1.0
   // Frees the resources allocated by cogl_color_new() and cogl_color_copy()
   void free()() nothrow {
      cogl_color_free(&this);
   }

   // VERSION: 1.0
   // Retrieves the alpha channel of @color as a fixed point
   // value between 0 and %1.0.
   // RETURNS: the alpha channel of the passed color
   float get_alpha()() nothrow {
      return cogl_color_get_alpha(&this);
   }

   // VERSION: 1.0
   // Retrieves the alpha channel of @color as a byte value
   // between 0 and 255
   // RETURNS: the alpha channel of the passed color
   ubyte get_alpha_byte()() nothrow {
      return cogl_color_get_alpha_byte(&this);
   }

   // VERSION: 1.0
   // Retrieves the alpha channel of @color as a floating point
   // value between 0.0 and 1.0
   // RETURNS: the alpha channel of the passed color
   float get_alpha_float()() nothrow {
      return cogl_color_get_alpha_float(&this);
   }

   // VERSION: 1.0
   // Retrieves the blue channel of @color as a fixed point
   // value between 0 and %1.0.
   // RETURNS: the blue channel of the passed color
   float get_blue()() nothrow {
      return cogl_color_get_blue(&this);
   }

   // VERSION: 1.0
   // Retrieves the blue channel of @color as a byte value
   // between 0 and 255
   // RETURNS: the blue channel of the passed color
   ubyte get_blue_byte()() nothrow {
      return cogl_color_get_blue_byte(&this);
   }

   // VERSION: 1.0
   // Retrieves the blue channel of @color as a floating point
   // value between 0.0 and 1.0
   // RETURNS: the blue channel of the passed color
   float get_blue_float()() nothrow {
      return cogl_color_get_blue_float(&this);
   }

   // VERSION: 1.0
   // Retrieves the green channel of @color as a fixed point
   // value between 0 and %1.0.
   // RETURNS: the green channel of the passed color
   float get_green()() nothrow {
      return cogl_color_get_green(&this);
   }

   // VERSION: 1.0
   // Retrieves the green channel of @color as a byte value
   // between 0 and 255
   // RETURNS: the green channel of the passed color
   ubyte get_green_byte()() nothrow {
      return cogl_color_get_green_byte(&this);
   }

   // VERSION: 1.0
   // Retrieves the green channel of @color as a floating point
   // value between 0.0 and 1.0
   // RETURNS: the green channel of the passed color
   float get_green_float()() nothrow {
      return cogl_color_get_green_float(&this);
   }

   // VERSION: 1.0
   // Retrieves the red channel of @color as a fixed point
   // value between 0 and %1.0.
   // RETURNS: the red channel of the passed color
   float get_red()() nothrow {
      return cogl_color_get_red(&this);
   }

   // VERSION: 1.0
   // Retrieves the red channel of @color as a byte value
   // between 0 and 255
   // RETURNS: the red channel of the passed color
   ubyte get_red_byte()() nothrow {
      return cogl_color_get_red_byte(&this);
   }

   // VERSION: 1.0
   // Retrieves the red channel of @color as a floating point
   // value between 0.0 and 1.0
   // RETURNS: the red channel of the passed color
   float get_red_float()() nothrow {
      return cogl_color_get_red_float(&this);
   }

   // VERSION: 1.4
   // Sets the values of the passed channels into a #CoglColor
   // <red>: value of the red channel, between 0 and %1.0
   // <green>: value of the green channel, between 0 and %1.0
   // <blue>: value of the blue channel, between 0 and %1.0
   // <alpha>: value of the alpha channel, between 0 and %1.0
   void init_from_4f()(float red, float green, float blue, float alpha) nothrow {
      cogl_color_init_from_4f(&this, red, green, blue, alpha);
   }

   // VERSION: 1.4
   // Sets the values of the passed channels into a #CoglColor
   // <color_array>: a pointer to an array of 4 float color components
   void init_from_4fv(AT0)(AT0 /*float*/ color_array) nothrow {
      cogl_color_init_from_4fv(&this, UpCast!(float*)(color_array));
   }

   // VERSION: 1.4
   // Sets the values of the passed channels into a #CoglColor.
   // <red>: value of the red channel, between 0 and 255
   // <green>: value of the green channel, between 0 and 255
   // <blue>: value of the blue channel, between 0 and 255
   // <alpha>: value of the alpha channel, between 0 and 255
   void init_from_4ub()(ubyte red, ubyte green, ubyte blue, ubyte alpha) nothrow {
      cogl_color_init_from_4ub(&this, red, green, blue, alpha);
   }

   // VERSION: 1.0
   // Converts a non-premultiplied color to a pre-multiplied color. For
   // example, semi-transparent red is (1.0, 0, 0, 0.5) when non-premultiplied
   // and (0.5, 0, 0, 0.5) when premultiplied.
   void premultiply()() nothrow {
      cogl_color_premultiply(&this);
   }

   // VERSION: 1.4
   // Sets the alpha channel of @color to @alpha.
   // <alpha>: a float value between 0.0f and 1.0f
   void set_alpha()(float alpha) nothrow {
      cogl_color_set_alpha(&this, alpha);
   }

   // VERSION: 1.4
   // Sets the alpha channel of @color to @alpha.
   // <alpha>: a byte value between 0 and 255
   void set_alpha_byte()(ubyte alpha) nothrow {
      cogl_color_set_alpha_byte(&this, alpha);
   }

   // VERSION: 1.4
   // Sets the alpha channel of @color to @alpha.
   // <alpha>: a float value between 0.0f and 1.0f
   void set_alpha_float()(float alpha) nothrow {
      cogl_color_set_alpha_float(&this, alpha);
   }

   // VERSION: 1.4
   // Sets the blue channel of @color to @blue.
   // <blue>: a float value between 0.0f and 1.0f
   void set_blue()(float blue) nothrow {
      cogl_color_set_blue(&this, blue);
   }

   // VERSION: 1.4
   // Sets the blue channel of @color to @blue.
   // <blue>: a byte value between 0 and 255
   void set_blue_byte()(ubyte blue) nothrow {
      cogl_color_set_blue_byte(&this, blue);
   }

   // VERSION: 1.4
   // Sets the blue channel of @color to @blue.
   // <blue>: a float value between 0.0f and 1.0f
   void set_blue_float()(float blue) nothrow {
      cogl_color_set_blue_float(&this, blue);
   }

   // VERSION: 1.0
   // DEPRECATED (v1.4) method: set_from_4f - Use cogl_color_init_from_4f instead.
   // Sets the values of the passed channels into a #CoglColor
   // <red>: value of the red channel, between 0 and %1.0
   // <green>: value of the green channel, between 0 and %1.0
   // <blue>: value of the blue channel, between 0 and %1.0
   // <alpha>: value of the alpha channel, between 0 and %1.0
   void set_from_4f()(float red, float green, float blue, float alpha) nothrow {
      cogl_color_set_from_4f(&this, red, green, blue, alpha);
   }

   // VERSION: 1.0
   // DEPRECATED (v1.4) method: set_from_4ub - Use cogl_color_init_from_4ub instead.
   // Sets the values of the passed channels into a #CoglColor.
   // <red>: value of the red channel, between 0 and 255
   // <green>: value of the green channel, between 0 and 255
   // <blue>: value of the blue channel, between 0 and 255
   // <alpha>: value of the alpha channel, between 0 and 255
   void set_from_4ub()(ubyte red, ubyte green, ubyte blue, ubyte alpha) nothrow {
      cogl_color_set_from_4ub(&this, red, green, blue, alpha);
   }

   // VERSION: 1.4
   // Sets the green channel of @color to @green.
   // <green>: a float value between 0.0f and 1.0f
   void set_green()(float green) nothrow {
      cogl_color_set_green(&this, green);
   }

   // VERSION: 1.4
   // Sets the green channel of @color to @green.
   // <green>: a byte value between 0 and 255
   void set_green_byte()(ubyte green) nothrow {
      cogl_color_set_green_byte(&this, green);
   }

   // VERSION: 1.4
   // Sets the green channel of @color to @green.
   // <green>: a float value between 0.0f and 1.0f
   void set_green_float()(float green) nothrow {
      cogl_color_set_green_float(&this, green);
   }

   // VERSION: 1.4
   // Sets the red channel of @color to @red.
   // <red>: a float value between 0.0f and 1.0f
   void set_red()(float red) nothrow {
      cogl_color_set_red(&this, red);
   }

   // VERSION: 1.4
   // Sets the red channel of @color to @red.
   // <red>: a byte value between 0 and 255
   void set_red_byte()(ubyte red) nothrow {
      cogl_color_set_red_byte(&this, red);
   }

   // VERSION: 1.4
   // Sets the red channel of @color to @red.
   // <red>: a float value between 0.0f and 1.0f
   void set_red_float()(float red) nothrow {
      cogl_color_set_red_float(&this, red);
   }

   // VERSION: 1.4
   // Converts a pre-multiplied color to a non-premultiplied color. For
   // example, semi-transparent red is (0.5, 0, 0, 0.5) when premultiplied
   // and (1.0, 0, 0, 0.5) when non-premultiplied.
   void unpremultiply()() nothrow {
      cogl_color_unpremultiply(&this);
   }

   // VERSION: 1.0
   // Compares two #CoglColor<!-- -->s and checks if they are the same.
   // 
   // This function can be passed to g_hash_table_new() as the @key_equal_func
   // parameter, when using #CoglColor<!-- -->s as keys in a #GHashTable.
   // RETURNS: %TRUE if the two colors are the same.
   // <v1>: a #CoglColor
   // <v2>: a #CoglColor
   static int equal(AT0, AT1)(AT0 /*const(void)*/ v1, AT1 /*const(void)*/ v2) nothrow {
      return cogl_color_equal(UpCast!(const(void)*)(v1), UpCast!(const(void)*)(v2));
   }

   // Unintrospectable function: new() / cogl_color_new()
   // VERSION: 1.0
   // Creates a new (empty) color
   // 
   // to free the allocated resources
   // RETURNS: a newly-allocated #CoglColor. Use cogl_color_free()
   static Color* new_()() nothrow {
      return cogl_color_new();
   }
}


// Defines a bit mask of color channels. This can be used with
// cogl_pipeline_set_color_mask() for example to define which color
// channels should be written to the current framebuffer when
// drawing something.
enum ColorMask {
   NONE = 0,
   RED = 1,
   GREEN = 2,
   BLUE = 4,
   ALPHA = 8,
   ALL = 15
}
struct Context {

   // VERSION: 1.8
   // Retrieves the #CoglDisplay that is internally associated with the
   // given @context. This will return the same #CoglDisplay that was
   // passed to cogl_context_new() or if %NULL was passed to
   // cogl_context_new() then this function returns a pointer to the
   // display that was automatically setup internally.
   // 
   // given @context.
   // RETURNS: The #CoglDisplay associated with the
   Cogl.Display* get_display()() nothrow {
      return cogl_context_get_display(&this);
   }

   // Unintrospectable function: new() / cogl_context_new()
   // VERSION: 1.8
   // Creates a new #CoglContext which acts as an application sandbox
   // for any state objects that are allocated.
   // RETURNS: A newly allocated #CoglContext
   // <display>: A #CoglDisplay pointer
   static Cogl.Context* /*new*/ new_(AT0, AT1)(AT0 /*Cogl.Display*/ display, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_context_new(UpCast!(Cogl.Display*)(display), UpCast!(GLib2.Error**)(error));
   }
}


// VERSION: 1.8
// A callback function to use for cogl_debug_object_foreach_type().
// <info>: A pointer to a struct containing information about the type.
extern (C) alias void function (DebugObjectTypeInfo* info, void* user_data) nothrow DebugObjectForeachTypeCallback;


// This struct is used to pass information to the callback when
// cogl_debug_object_foreach_type() is called.
struct DebugObjectTypeInfo /* Version 1.8 */ {
   char* name;
   uint instance_count;
}

struct DepthState {
   uint magic;
   int test_enabled;
   DepthTestFunction test_function;
   int write_enabled;
   float range_near, range_far;
   uint padding0, padding1, padding2, padding3, padding4, padding5, padding6, padding7, padding8, padding9;


   // VERSION: 2.0
   // Gets the current range to which normalized depth values are mapped
   // before writing to the depth buffer. This corresponds to the range
   // set with cogl_pipeline_set_depth_range().
   // <near_val>: A pointer to store the near component of the depth range
   // <far_val>: A pointer to store the far component of the depth range
   void get_range(AT0, AT1)(AT0 /*float*/ near_val, AT1 /*float*/ far_val) nothrow {
      cogl_depth_state_get_range(&this, UpCast!(float*)(near_val), UpCast!(float*)(far_val));
   }

   // VERSION: 2.0
   // Gets the current depth test enabled state as previously set by
   // cogl_depth_state_set_test_enabled().
   // RETURNS: The pipeline's current depth test enabled state.
   int get_test_enabled()() nothrow {
      return cogl_depth_state_get_test_enabled(&this);
   }

   // VERSION: 2.0
   // Gets the current depth test enable state as previously set via
   // cogl_pipeline_set_depth_test_enabled().
   // RETURNS: The current depth test enable state.
   DepthTestFunction get_test_function()() nothrow {
      return cogl_depth_state_get_test_function(&this);
   }

   // VERSION: 2.0
   // Gets the depth writing enable state as set by the corresponding
   // cogl_pipeline_set_depth_writing_enabled.
   // RETURNS: The current depth writing enable state
   int get_write_enabled()() nothrow {
      return cogl_depth_state_get_write_enabled(&this);
   }

   // VERSION: 2.0
   // Initializes the members of @state to their default values.
   // 
   // You should never pass an un initialized #CoglDepthState structure
   // to cogl_pipeline_set_depth_state().
   void init()() nothrow {
      cogl_depth_state_init(&this);
   }

   // VERSION: 2.0
   // Sets the range to map depth values in normalized device coordinates
   // to before writing out to a depth buffer.
   // 
   // After your geometry has be transformed, clipped and had perspective
   // division applied placing it in normalized device
   // coordinates all depth values between the near and far z clipping
   // planes are in the range -1 to 1. Before writing any depth value to
   // the depth buffer though the value is mapped into the range [0, 1].
   // 
   // With this function you can change the range which depth values are
   // mapped too although the range must still lye within the range [0,
   // 1].
   // 
   // If your driver does not support this feature (for example you are
   // using GLES 1 drivers) then if you don't use the default range
   // values you will get an error reported when calling
   // cogl_pipeline_set_depth_state (). You can check ahead of time for
   // the %COGL_FEATURE_ID_DEPTH_RANGE feature with
   // cogl_has_feature() to know if this function will succeed.
   // 
   // By default normalized device coordinate depth values are mapped to
   // the full range of depth buffer values, [0, 1].
   // 
   // NB: this won't directly affect the state of the GPU. You have
   // to then set the state on a #CoglPipeline using
   // cogl_pipeline_set_depth_state().
   // <near_val>: The near component of the desired depth range which will be clamped to the range [0, 1]
   // <far_val>: The far component of the desired depth range which will be clamped to the range [0, 1]
   void set_range()(float near_val, float far_val) nothrow {
      cogl_depth_state_set_range(&this, near_val, far_val);
   }

   // VERSION: 2.0
   // Enables or disables depth testing according to the value of
   // @enable.
   // 
   // If depth testing is enable then the #CoglDepthTestFunction set
   // using cogl_pipeline_set_depth_test_function() us used to evaluate
   // the depth value of incoming fragments against the corresponding
   // value stored in the current depth buffer, and if the test passes
   // then the fragments depth value is used to update the depth buffer.
   // (unless you have disabled depth writing via
   // cogl_pipeline_set_depth_writing_enabled ())
   // 
   // By default depth testing is disabled.
   // 
   // NB: this won't directly affect the state of the GPU. You have
   // to then set the state on a #CoglPipeline using
   // cogl_pipeline_set_depth_state()
   // <enable>: The enable state you want
   void set_test_enabled()(int enable) nothrow {
      cogl_depth_state_set_test_enabled(&this, enable);
   }

   // VERSION: 2.0
   // Sets the #CoglDepthTestFunction used to compare the depth value of
   // an incoming fragment against the corresponding value in the current
   // depth buffer.
   // 
   // By default the depth test function is %COGL_DEPTH_TEST_FUNCTION_LESS
   // 
   // NB: this won't directly affect the state of the GPU. You have
   // to then set the state on a #CoglPipeline using
   // cogl_pipeline_set_depth_state()
   // <function>: The #CoglDepthTestFunction to set
   void set_test_function()(DepthTestFunction function_) nothrow {
      cogl_depth_state_set_test_function(&this, function_);
   }

   // VERSION: 2.0
   // Enables or disables depth buffer writing according to the value of
   // @enable. Normally when depth testing is enabled and the comparison
   // between a fragment's depth value and the corresponding depth buffer
   // value passes then the fragment's depth is written to the depth
   // buffer unless writing is disabled here.
   // 
   // By default depth writing is enabled
   // 
   // NB: this won't directly affect the state of the GPU. You have
   // to then set the state on a #CoglPipeline using
   // cogl_pipeline_set_depth_state()
   // <enable>: The enable state you want
   void set_write_enabled()(int enable) nothrow {
      cogl_depth_state_set_write_enabled(&this, enable);
   }
}


// When using depth testing one of these functions is used to compare
// the depth of an incoming fragment against the depth value currently
// stored in the depth buffer. The function is changed using
// cogl_material_set_depth_test_function().
// 
// The test is only done when depth testing is explicitly enabled. (See
// cogl_material_set_depth_test_enabled())
enum DepthTestFunction {
   NEVER = 512,
   LESS = 513,
   EQUAL = 514,
   LEQUAL = 515,
   GREATER = 516,
   NOTEQUAL = 517,
   GEQUAL = 518,
   ALWAYS = 519
}
struct Display {
   // Unintrospectable method: get_renderer_EXP() / cogl_display_get_renderer_EXP()
   Cogl.Renderer* get_renderer_EXP()() nothrow {
      return cogl_display_get_renderer_EXP(&this);
   }
   int setup_EXP(AT0)(AT0 /*GLib2.Error**/ error=null) nothrow {
      return cogl_display_setup_EXP(&this, UpCast!(GLib2.Error**)(error));
   }
   // Unintrospectable function: new_EXP() / cogl_display_new_EXP()
   static Cogl.Display* new_EXP(AT0, AT1)(AT0 /*Cogl.Renderer*/ renderer, AT1 /*OnscreenTemplate*/ onscreen_template) nothrow {
      return cogl_display_new_EXP(UpCast!(Cogl.Renderer*)(renderer), UpCast!(OnscreenTemplate*)(onscreen_template));
   }
}


// Error enumeration for Cogl
// 
// The @COGL_ERROR_UNSUPPORTED error can be thrown for a variety of
// reasons. For example:
// 
// <itemizedlist>
// <listitem><para>You've tried to use a feature that is not
// advertised by cogl_get_features(). This could happen if you create
// a 2d texture with a non-power-of-two size when
// %COGL_FEATURE_TEXTURE_NPOT is not advertised.</para></listitem>
// <listitem><para>The GPU can not handle the configuration you have
// requested. An example might be if you try to use too many texture
// layers in a single #CoglPipeline</para></listitem>
// <listitem><para>The driver does not support some
// configuration.</para></listiem>
// </itemizedlist>
// 
// Currently this is only used by Cogl API marked as experimental so
// this enum should also be considered experimental.
enum Error /* Version 1.4 */ {
   UNSUPPORTED = 0,
   NO_MEMORY = 1
}

// Represents an ordered rotation first of @heading degrees around an
// object's y axis, then @pitch degrees around an object's x axis and
// finally @roll degrees around an object's z axis.
// 
// <note>It's important to understand the that axis are associated
// with the object being rotated, so the axis also rotate in sequence
// with the rotations being applied.</note>
// 
// The members of a #CoglEuler can be initialized, for example, with
// cogl_euler_init() and cogl_euler_init_from_quaternion ().
// 
// You may also want to look at cogl_quaternion_init_from_euler() if
// you want to do interpolation between 3d rotations.
struct Euler /* Version 2.0 */ {
   float heading, pitch, roll;
   private float padding0, padding1, padding2, padding3, padding4;


   // Unintrospectable method: copy() / cogl_euler_copy()
   // VERSION: 2.0
   // Allocates a new #CoglEuler and initilizes it with the component
   // angles of @src. The newly allocated euler should be freed using
   // cogl_euler_free().
   // RETURNS: A newly allocated #CoglEuler
   Euler* copy()() nothrow {
      return cogl_euler_copy(&this);
   }

   // VERSION: 2.0
   // Frees a #CoglEuler that was previously allocated using
   // cogl_euler_copy().
   void free()() nothrow {
      cogl_euler_free(&this);
   }

   // VERSION: 2.0
   // Initializes @euler to represent a rotation of @x_angle degrees
   // around the x axis, then @y_angle degrees around the y_axis and
   // @z_angle degrees around the z axis.
   // <heading>: Angle to rotate around an object's y axis
   // <pitch>: Angle to rotate around an object's x axis
   // <roll>: Angle to rotate around an object's z axis
   void init()(float heading, float pitch, float roll) nothrow {
      cogl_euler_init(&this, heading, pitch, roll);
   }

   // Extracts a euler rotation from the given @matrix and
   // initializses @euler with the component x, y and z rotation angles.
   // <matrix>: A #CoglMatrix containing a rotation, but no scaling, mirroring or skewing.
   void init_from_matrix(AT0)(AT0 /*Matrix*/ matrix) nothrow {
      cogl_euler_init_from_matrix(&this, UpCast!(Matrix*)(matrix));
   }

   // Initializes a @euler rotation with the equivalent rotation
   // represented by the given @quaternion.
   // <quaternion>: A #CoglEuler with the rotation to initialize with
   void init_from_quaternion(AT0)(AT0 /*Quaternion*/ quaternion) nothrow {
      cogl_euler_init_from_quaternion(&this, UpCast!(Quaternion*)(quaternion));
   }

   // VERSION: 2.0
   // Compares the two given euler angles @v1 and @v1 and it they are
   // equal returns %TRUE else %FALSE.
   // 
   // <note>This function only checks that all three components rotations
   // are numerically equal, it does not consider that some rotations
   // can be represented with different component rotations</note>
   // RETURNS: %TRUE if @v1 and @v2 are equal else %FALSE.
   // <v1>: The second euler angle to compare
   static int equal(AT0, AT1)(AT0 /*const(void)*/ v1, AT1 /*const(void)*/ v2) nothrow {
      return cogl_euler_equal(UpCast!(const(void)*)(v1), UpCast!(const(void)*)(v2));
   }
}

enum int FIXED_0_5 = 32768;
enum int FIXED_1 = 1;
enum int FIXED_2_PI = 411775;
enum int FIXED_BITS = 32;
enum int FIXED_EPSILON = 1;
enum int FIXED_MAX = 2147483647;
enum int FIXED_MIN = cast(int)2147483648;
enum int FIXED_PI = 205887;
enum int FIXED_PI_2 = 102944;
enum int FIXED_PI_4 = 51472;
enum int FIXED_Q = -16;

// VERSION: 1.10
// A callback used with cogl_foreach_feature() for enumerating all
// context level features supported by Cogl.
// <feature>: A single feature currently supported by Cogl
// <user_data>: A private pointer passed to cogl_foreach_feature().
extern (C) alias void function (FeatureID feature, void* user_data) nothrow FeatureCallback;

// Flags for the supported features.
enum FeatureFlags /* Version 0.8 */ {
   TEXTURE_RECTANGLE = 2,
   TEXTURE_NPOT = 4,
   TEXTURE_YUV = 8,
   TEXTURE_READ_PIXELS = 16,
   SHADERS_GLSL = 32,
   OFFSCREEN = 64,
   OFFSCREEN_MULTISAMPLE = 128,
   OFFSCREEN_BLIT = 256,
   FOUR_CLIP_PLANES = 512,
   STENCIL_BUFFER = 1024,
   VBOS = 2048,
   PBOS = 4096,
   UNSIGNED_INT_INDICES = 8192,
   DEPTH_RANGE = 16384,
   TEXTURE_NPOT_BASIC = 32768,
   TEXTURE_NPOT_MIPMAP = 65536,
   TEXTURE_NPOT_REPEAT = 131072,
   POINT_SPRITE = 262144,
   TEXTURE_3D = 524288,
   SHADERS_ARBFP = 1048576,
   MAP_BUFFER_FOR_READ = 2097152,
   MAP_BUFFER_FOR_WRITE = 4194304,
   ONSCREEN_MULTIPLE = 8388608
}

// All the capabilities that can vary between different GPUs supported
// by Cogl. Applications that depend on any of these features should explicitly
// check for them using cogl_has_feature() or cogl_has_features().
enum FeatureID /* Version 1.10 */ {
   COGL_FEATURE_ID_TEXTURE_NPOT_BASIC = 1,
   COGL_FEATURE_ID_TEXTURE_NPOT_MIPMAP = 2,
   COGL_FEATURE_ID_TEXTURE_NPOT_REPEAT = 3,
   COGL_FEATURE_ID_TEXTURE_NPOT = 4,
   COGL_FEATURE_ID_TEXTURE_RECTANGLE = 5,
   COGL_FEATURE_ID_TEXTURE_3D = 6,
   COGL_FEATURE_ID_GLSL = 7,
   COGL_FEATURE_ID_ARBFP = 8,
   COGL_FEATURE_ID_OFFSCREEN = 9,
   COGL_FEATURE_ID_OFFSCREEN_MULTISAMPLE = 10,
   COGL_FEATURE_ID_ONSCREEN_MULTIPLE = 11,
   COGL_FEATURE_ID_UNSIGNED_INT_INDICES = 12,
   COGL_FEATURE_ID_DEPTH_RANGE = 13,
   COGL_FEATURE_ID_POINT_SPRITE = 14,
   COGL_FEATURE_ID_MAP_BUFFER_FOR_READ = 15,
   COGL_FEATURE_ID_MAP_BUFFER_FOR_WRITE = 16,
   COGL_FEATURE_ID_MIRRORED_REPEAT = 17,
   COGL_FEATURE_ID_SWAP_BUFFERS_EVENT = 18,
   _COGL_N_FEATURE_IDS = 19
}
enum FilterReturn {
   CONTINUE = 0,
   REMOVE = 1
}
// Fixed point number using a (16.16) notation.
struct Fixed {

   // Unintrospectable function: log2() / cogl_fixed_log2()
   // VERSION: 1.0
   // Calculates base 2 logarithm.
   // 
   // This function is some 2.5 times faster on x86, and over 12 times faster on
   // fpu-less arm, than using libc log().
   // RETURNS: base 2 logarithm.
   // <x>: value to calculate base 2 logarithm from
   static Fixed log2()(uint x) nothrow {
      return cogl_fixed_log2(x);
   }

   // VERSION: 1.0
   // Calculates @x to the @y power.
   // RETURNS: the power of @x to the @y
   // <x>: base
   // <y>: #CoglFixed exponent
   static uint pow()(uint x, Fixed y) nothrow {
      return cogl_fixed_pow(x, y);
   }

   // Unintrospectable method: atan() / cogl_fixed_atan()
   // VERSION: 1.0
   // Computes the arc tangent of @a.
   // RETURNS: the arc tangent of the passed value, in fixed point notation
   Fixed atan()() nothrow {
      return cogl_fixed_atan(&this);
   }

   // Unintrospectable method: atan2() / cogl_fixed_atan2()
   // VERSION: 1.0
   // Computes the arc tangent of @a / @b but uses the sign of both
   // arguments to return the angle in right quadrant.
   // 
   // notation
   // RETURNS: the arc tangent of the passed fraction, in fixed point
   // <b>: the denominator as a #CoglFixed number
   Fixed atan2()(Fixed b) nothrow {
      return cogl_fixed_atan2(&this, b);
   }

   // Unintrospectable method: cos() / cogl_fixed_cos()
   // VERSION: 1.0
   // Computes the cosine of @angle.
   // RETURNS: the cosine of the passed angle, in fixed point notation
   Fixed cos()() nothrow {
      return cogl_fixed_cos(&this);
   }
   // Unintrospectable method: div() / cogl_fixed_div()
   Fixed div()(Fixed b) nothrow {
      return cogl_fixed_div(&this, b);
   }
   // Unintrospectable method: mul() / cogl_fixed_mul()
   Fixed mul()(Fixed b) nothrow {
      return cogl_fixed_mul(&this, b);
   }
   // Unintrospectable method: mul_div() / cogl_fixed_mul_div()
   Fixed mul_div()(Fixed b, Fixed c) nothrow {
      return cogl_fixed_mul_div(&this, b, c);
   }

   // VERSION: 1.0
   // Calculates 2 to the @x power.
   // 
   // This function is around 11 times faster on x86, and around 22 times faster
   // on fpu-less arm than libc pow(2, x).
   // RETURNS: the power of 2 to the passed value
   uint pow2()() nothrow {
      return cogl_fixed_pow2(&this);
   }

   // Unintrospectable method: sin() / cogl_fixed_sin()
   // VERSION: 1.0
   // Computes the sine of @angle.
   // RETURNS: the sine of the passed angle, in fixed point notation
   Fixed sin()() nothrow {
      return cogl_fixed_sin(&this);
   }

   // Unintrospectable method: sqrt() / cogl_fixed_sqrt()
   // VERSION: 1.0
   // Computes the square root of @x.
   // 
   // notation
   // RETURNS: the square root of the passed value, in floating point
   Fixed sqrt()() nothrow {
      return cogl_fixed_sqrt(&this);
   }

   // Unintrospectable method: tan() / cogl_fixed_tan()
   // VERSION: 1.0
   // Computes the tangent of @angle.
   // RETURNS: the tangent of the passed angle, in fixed point notation
   Fixed tan()() nothrow {
      return cogl_fixed_tan(&this);
   }
}


// The fog mode determines the equation used to calculate the fogging blend
// factor while fogging is enabled. The simplest %COGL_FOG_MODE_LINEAR mode
// determines f as:
// 
// |[
// f = end - eye_distance / end - start
// ]|
// 
// Where eye_distance is the distance of the current fragment in eye
// coordinates from the origin.
enum FogMode /* Version 1.0 */ {
   LINEAR = 0,
   EXPONENTIAL = 1,
   EXPONENTIAL_SQUARED = 2
}
struct Framebuffer {

   // Unintrospectable method: add_swap_buffers_callback() / cogl_framebuffer_add_swap_buffers_callback()
   // VERSION: 1.10
   // Installs a @callback function that should be called whenever a swap buffers
   // request (made using cogl_framebuffer_swap_buffers()) for the given
   // @framebuffer completes.
   // 
   // <note>Applications should check for the %COGL_FEATURE_ID_SWAP_BUFFERS_EVENT
   // feature before using this API. It's currently undefined when and if
   // registered callbacks will be called if this feature is not supported.</note>
   // 
   // We recommend using this mechanism when available to manually throttle your
   // applications (in conjunction with  cogl_onscreen_set_swap_throttled()) so
   // your application will be able to avoid long blocks in the driver caused by
   // throttling when you request to swap buffers too quickly.
   // 
   // the callback later.
   // RETURNS: a unique identifier that can be used to remove to remove
   // <callback>: A callback function to call when a swap has completed
   // <user_data>: A private pointer to be passed to @callback
   uint add_swap_buffers_callback(AT0)(SwapBuffersNotify callback, AT0 /*void*/ user_data) nothrow {
      return cogl_framebuffer_add_swap_buffers_callback(&this, callback, UpCast!(void*)(user_data));
   }

   // VERSION: 1.8
   // Explicitly allocates a configured #CoglFramebuffer allowing developers to
   // check and handle any errors that might arise from an unsupported
   // configuration so that fallback configurations may be tried.
   // 
   // <note>Many applications don't support any fallback options at least when
   // they are initially developed and in that case the don't need to use this API
   // since Cogl will automatically allocate a framebuffer when it first gets
   // used.  The disadvantage of relying on automatic allocation is that the
   // program will abort with an error message if there is an error during
   // automatic allocation.<note>
   // RETURNS: %TRUE if there were no error allocating the framebuffer, else %FALSE.
   int allocate(AT0)(AT0 /*GLib2.Error**/ error=null) nothrow {
      return cogl_framebuffer_allocate(&this, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.8
   // Clears all the auxiliary buffers identified in the @buffers mask, and if
   // that includes the color buffer then the specified @color is used.
   // <buffers>: A mask of #CoglBufferBit<!-- -->'s identifying which auxiliary buffers to clear
   // <color>: The color to clear the color buffer too if specified in @buffers.
   void clear(AT0)(c_ulong buffers, AT0 /*Color*/ color) nothrow {
      cogl_framebuffer_clear(&this, buffers, UpCast!(Color*)(color));
   }

   // VERSION: 1.8
   // Clears all the auxiliary buffers identified in the @buffers mask, and if
   // that includes the color buffer then the specified @color is used.
   // <buffers>: A mask of #CoglBufferBit<!-- -->'s identifying which auxiliary buffers to clear
   // <red>: The red component of color to clear the color buffer too if specified in @buffers.
   // <green>: The green component of color to clear the color buffer too if specified in @buffers.
   // <blue>: The blue component of color to clear the color buffer too if specified in @buffers.
   // <alpha>: The alpha component of color to clear the color buffer too if specified in @buffers.
   void clear4f()(c_ulong buffers, float red, float green, float blue, float alpha) nothrow {
      cogl_framebuffer_clear4f(&this, buffers, red, green, blue, alpha);
   }

   // VERSION: 1.8
   // Declares that the specified @buffers no longer need to be referenced
   // by any further rendering commands. This can be an important
   // optimization to avoid subsequent frames of rendering depending on
   // the results of a previous frame.
   // 
   // For example; some tile-based rendering GPUs are able to avoid allocating and
   // accessing system memory for the depth and stencil buffer so long as these
   // buffers are not required as input for subsequent frames and that can save a
   // significant amount of memory bandwidth used to save and restore their
   // contents to system memory between frames.
   // 
   // It is currently considered an error to try and explicitly discard the color
   // buffer by passing %COGL_BUFFER_BIT_COLOR. This is because the color buffer is
   // already implicitly discard when you finish rendering to a #CoglOnscreen
   // framebuffer, and it's not meaningful to try and discard the color buffer of
   // a #CoglOffscreen framebuffer since they are single-buffered.
   // <buffers>: A #CoglBufferBit mask of which ancillary buffers you want to discard.
   void discard_buffers()(c_ulong buffers) nothrow {
      cogl_framebuffer_discard_buffers(&this, buffers);
   }

   // VERSION: 1.10
   // This blocks the CPU until all pending rendering associated with the
   // specified framebuffer has completed. It's very rare that developers should
   // ever need this level of synchronization with the GPU and should never be
   // used unless you clearly understand why you need to explicitly force
   // synchronization.
   // 
   // One example might be for benchmarking purposes to be sure timing
   // measurements reflect the time that the GPU is busy for not just the time it
   // takes to queue rendering commands.
   void finish()() nothrow {
      cogl_framebuffer_finish(&this);
   }

   // VERSION: 1.10
   // Replaces the current projection matrix with a perspective matrix
   // for a given viewing frustum defined by 4 side clip planes that
   // all cross through the origin and 2 near and far clip planes.
   // <left>: X position of the left clipping plane where it intersects the near clipping plane
   // <right>: X position of the right clipping plane where it intersects the near clipping plane
   // <bottom>: Y position of the bottom clipping plane where it intersects the near clipping plane
   // <top>: Y position of the top clipping plane where it intersects the near clipping plane
   // <z_near>: The distance to the near clipping plane (Must be positive)
   // <z_far>: The distance to the far clipping plane (Must be positive)
   void frustum()(float left, float right, float bottom, float top, float z_near, float z_far) nothrow {
      cogl_framebuffer_frustum(&this, left, right, bottom, top, z_near, z_far);
   }

   // VERSION: 1.8
   // Retrieves the number of alpha bits of @framebuffer
   // RETURNS: the number of bits
   int get_alpha_bits()() nothrow {
      return cogl_framebuffer_get_alpha_bits(&this);
   }

   // VERSION: 1.8
   // Retrieves the number of blue bits of @framebuffer
   // RETURNS: the number of bits
   int get_blue_bits()() nothrow {
      return cogl_framebuffer_get_blue_bits(&this);
   }

   // VERSION: 1.8
   // Queries the common #CoglPixelFormat of all color buffers attached
   // to this framebuffer. For an offscreen framebuffer created with
   // cogl_offscreen_new_to_texture() this will correspond to the format
   // of the texture.
   PixelFormat get_color_format()() nothrow {
      return cogl_framebuffer_get_color_format(&this);
   }

   // VERSION: 1.8
   // Gets the current #CoglColorMask of which channels would be written to the
   // current framebuffer. Each bit set in the mask means that the
   // corresponding color would be written.
   // RETURNS: A #CoglColorMask
   ColorMask get_color_mask()() nothrow {
      return cogl_framebuffer_get_color_mask(&this);
   }
   // Unintrospectable method: get_context() / cogl_framebuffer_get_context()
   Cogl.Context* get_context()() nothrow {
      return cogl_framebuffer_get_context(&this);
   }

   // VERSION: 1.8
   // Returns whether dithering has been requested for the given @framebuffer.
   // See cogl_framebuffer_set_dither_enabled() for more details about dithering.
   // 
   // <note>This may return %TRUE even when the underlying @framebuffer
   // display pipeline does not support dithering. This value only represents
   // the user's request for dithering.</note>
   // RETURNS: %TRUE if dithering has been requested or %FALSE if not.
   int get_dither_enabled()() nothrow {
      return cogl_framebuffer_get_dither_enabled(&this);
   }

   // VERSION: 1.8
   // Retrieves the number of green bits of @framebuffer
   // RETURNS: the number of bits
   int get_green_bits()() nothrow {
      return cogl_framebuffer_get_green_bits(&this);
   }

   // VERSION: 1.8
   // Queries the current height of the given @framebuffer.
   // RETURNS: The height of @framebuffer.
   int get_height()() nothrow {
      return cogl_framebuffer_get_height(&this);
   }

   // VERSION: 1.10
   // Stores the current model-view matrix in @matrix.
   // <matrix>: return location for the model-view matrix
   void get_modelview_matrix(AT0)(/*out*/ AT0 /*Matrix*/ matrix) nothrow {
      cogl_framebuffer_get_modelview_matrix(&this, UpCast!(Matrix*)(matrix));
   }

   // VERSION: 1.10
   // Stores the current projection matrix in @matrix.
   // <matrix>: return location for the projection matrix
   void get_projection_matrix(AT0)(/*out*/ AT0 /*Matrix*/ matrix) nothrow {
      cogl_framebuffer_get_projection_matrix(&this, UpCast!(Matrix*)(matrix));
   }

   // VERSION: 1.8
   // Retrieves the number of red bits of @framebuffer
   // RETURNS: the number of bits
   int get_red_bits()() nothrow {
      return cogl_framebuffer_get_red_bits(&this);
   }

   // VERSION: 1.10
   // Gets the number of points that are sampled per-pixel when
   // rasterizing geometry. Usually by default this will return 0 which
   // means that single-sample not multisample rendering has been chosen.
   // When using a GPU supporting multisample rendering it's possible to
   // increase the number of samples per pixel using
   // cogl_framebuffer_set_samples_per_pixel().
   // 
   // Calling cogl_framebuffer_get_samples_per_pixel() before the
   // framebuffer has been allocated will simply return the value set
   // using cogl_framebuffer_set_samples_per_pixel(). After the
   // framebuffer has been allocated the value will reflect the actual
   // number of samples that will be made by the GPU.
   // 
   // rasterizing geometry or 0 if single-sample rendering
   // has been chosen.
   // RETURNS: The number of point samples made per pixel when
   int get_samples_per_pixel()() nothrow {
      return cogl_framebuffer_get_samples_per_pixel(&this);
   }

   // VERSION: 1.8
   // Queries the x, y, width and height components of the current viewport as set
   // using cogl_framebuffer_set_viewport() or the default values which are 0, 0,
   // framebuffer_width and framebuffer_height.  The values are written into the
   // given @viewport array.
   // <viewport>: A pointer to an array of 4 floats to receive the (x, y, width, height) components of the current viewport.
   void get_viewport4fv(AT0)(AT0 /*float*/ viewport) nothrow {
      cogl_framebuffer_get_viewport4fv(&this, UpCast!(float*)(viewport));
   }

   // VERSION: 1.8
   // Queries the height of the viewport as set using cogl_framebuffer_set_viewport()
   // or the default value which is the height of the framebuffer.
   // RETURNS: The height of the viewport.
   float get_viewport_height()() nothrow {
      return cogl_framebuffer_get_viewport_height(&this);
   }

   // VERSION: 1.8
   // Queries the width of the viewport as set using cogl_framebuffer_set_viewport()
   // or the default value which is the width of the framebuffer.
   // RETURNS: The width of the viewport.
   float get_viewport_width()() nothrow {
      return cogl_framebuffer_get_viewport_width(&this);
   }

   // VERSION: 1.8
   // Queries the x coordinate of the viewport origin as set using cogl_framebuffer_set_viewport()
   // or the default value which is %0.
   // RETURNS: The x coordinate of the viewport origin.
   float get_viewport_x()() nothrow {
      return cogl_framebuffer_get_viewport_x(&this);
   }

   // VERSION: 1.8
   // Queries the y coordinate of the viewport origin as set using cogl_framebuffer_set_viewport()
   // or the default value which is %0.
   // RETURNS: The y coordinate of the viewport origin.
   float get_viewport_y()() nothrow {
      return cogl_framebuffer_get_viewport_y(&this);
   }

   // VERSION: 1.8
   // Queries the current width of the given @framebuffer.
   // RETURNS: The width of @framebuffer.
   int get_width()() nothrow {
      return cogl_framebuffer_get_width(&this);
   }

   // VERSION: 1.10
   // Resets the current model-view matrix to the identity matrix.
   void identity_matrix()() nothrow {
      cogl_framebuffer_identity_matrix(&this);
   }

   // VERSION: 1.0
   // Replaces the current projection matrix with an orthographic projection
   // matrix.
   // <x_1>: The x coordinate for the first vertical clipping plane
   // <y_1>: The y coordinate for the first horizontal clipping plane
   // <x_2>: The x coordinate for the second vertical clipping plane
   // <y_2>: The y coordinate for the second horizontal clipping plane
   // <near>: The <emphasis>distance</emphasis> to the near clipping plane (will be <emphasis>negative</emphasis> if the plane is behind the viewer)
   // <far>: The <emphasis>distance</emphasis> to the far clipping plane (will be <emphasis>negative</emphasis> if the plane is behind the viewer)
   void orthographic()(float x_1, float y_1, float x_2, float y_2, float near, float far) nothrow {
      cogl_framebuffer_orthographic(&this, x_1, y_1, x_2, y_2, near, far);
   }

   // VERSION: 1.10
   // Replaces the current projection matrix with a perspective matrix
   // based on the provided values.
   // 
   // <note>You should be careful not to have to great a @z_far / @z_near
   // ratio since that will reduce the effectiveness of depth testing
   // since there wont be enough precision to identify the depth of
   // objects near to each other.</note>
   // <aspect>: The (width over height) aspect ratio for display
   // <z_near>: The distance to the near clipping plane (Must be positive, and must not be 0)
   // <z_far>: The distance to the far clipping plane (Must be positive)
   void perspective()(float fov_y, float aspect, float z_near, float z_far) nothrow {
      cogl_framebuffer_perspective(&this, fov_y, aspect, z_near, z_far);
   }

   // VERSION: 1.10
   // Reverts the clipping region to the state before the last call to
   // cogl_framebuffer_push_clip().
   void pop_clip()() nothrow {
      cogl_framebuffer_pop_clip(&this);
   }

   // VERSION: 1.10
   // Restores the model-view matrix on the top of the matrix stack.
   void pop_matrix()() nothrow {
      cogl_framebuffer_pop_matrix(&this);
   }

   // VERSION: 1.10
   // Copies the current model-view matrix onto the matrix stack. The matrix
   // can later be restored with cogl_framebuffer_pop_matrix().
   void push_matrix()() nothrow {
      cogl_framebuffer_push_matrix(&this);
   }

   // VERSION: 1.0
   // Sets a new clipping area using the silhouette of the specified,
   // filled @path.  The clipping area is intersected with the previous
   // clipping area. To restore the previous clipping area, call
   // cogl_framebuffer_pop_clip().
   // <path>: The path to clip with.
   void push_path_clip(AT0)(AT0 /*Path*/ path) nothrow {
      cogl_framebuffer_push_path_clip(&this, UpCast!(Path*)(path));
   }

   // VERSION: 1.10
   // Sets a new clipping area using a 2D shaped described with a
   // #CoglPrimitive. The shape must not contain self overlapping
   // geometry and must lie on a single 2D plane. A bounding box of the
   // 2D shape in local coordinates (the same coordinates used to
   // describe the shape) must be given. It is acceptable for the bounds
   // to be larger than the true bounds but behaviour is undefined if the
   // bounds are smaller than the true bounds.
   // 
   // The primitive is transformed by the current model-view matrix and
   // the silhouette is intersected with the previous clipping area.  To
   // restore the previous clipping area, call
   // cogl_framebuffer_pop_clip().
   // <primitive>: A #CoglPrimitive describing a flat 2D shape
   // <bounds_x1>: y coordinate for the bottom-right corner of the primitives bounds.
   // <bounds_y1>: y coordinate for the top-left corner of the primitives bounds
   // <bounds_x2>: x coordinate for the top-left corner of the primitives bounds
   // <bounds_y2>: x coordinate for the bottom-right corner of the primitives bounds.
   void push_primitive_clip(AT0)(AT0 /*Primitive*/ primitive, float bounds_x1, float bounds_y1, float bounds_x2, float bounds_y2) nothrow {
      cogl_framebuffer_push_primitive_clip(&this, UpCast!(Primitive*)(primitive), bounds_x1, bounds_y1, bounds_x2, bounds_y2);
   }

   // VERSION: 1.10
   // Specifies a modelview transformed rectangular clipping area for all
   // subsequent drawing operations. Any drawing commands that extend
   // outside the rectangle will be clipped so that only the portion
   // inside the rectangle will be displayed. The rectangle dimensions
   // are transformed by the current model-view matrix.
   // 
   // The rectangle is intersected with the current clip region. To undo
   // the effect of this function, call cogl_framebuffer_pop_clip().
   // <x_1>: x coordinate for top left corner of the clip rectangle
   // <y_1>: y coordinate for top left corner of the clip rectangle
   // <x_2>: x coordinate for bottom right corner of the clip rectangle
   // <y_2>: y coordinate for bottom right corner of the clip rectangle
   void push_rectangle_clip()(float x_1, float y_1, float x_2, float y_2) nothrow {
      cogl_framebuffer_push_rectangle_clip(&this, x_1, y_1, x_2, y_2);
   }

   // VERSION: 1.10
   // Specifies a rectangular clipping area for all subsequent drawing
   // operations. Any drawing commands that extend outside the rectangle
   // will be clipped so that only the portion inside the rectangle will
   // be displayed. The rectangle dimensions are not transformed by the
   // current model-view matrix.
   // 
   // The rectangle is intersected with the current clip region. To undo
   // the effect of this function, call cogl_framebuffer_pop_clip().
   // <x>: left edge of the clip rectangle in window coordinates
   // <y>: top edge of the clip rectangle in window coordinates
   // <width>: width of the clip rectangle
   // <height>: height of the clip rectangle
   void push_scissor_clip()(int x, int y, int width, int height) nothrow {
      cogl_framebuffer_push_scissor_clip(&this, x, y, width, height);
   }

   // VERSION: 1.10
   // Removes a callback that was previously registered
   // using cogl_framebuffer_add_swap_buffers_callback().
   // <id>: An identifier returned from cogl_framebuffer_add_swap_buffers_callback()
   void remove_swap_buffers_callback()(uint id) nothrow {
      cogl_framebuffer_remove_swap_buffers_callback(&this, id);
   }

   // VERSION: 1.8
   // When point sample rendering (also known as multisample rendering)
   // has been enabled via cogl_framebuffer_set_samples_per_pixel()
   // then you can optionally call this function (or
   // cogl_framebuffer_resolve_samples_region()) to explicitly resolve
   // the point samples into values for the final color buffer.
   // 
   // Some GPUs will implicitly resolve the point samples during
   // rendering and so this function is effectively a nop, but with other
   // architectures it is desirable to defer the resolve step until the
   // end of the frame.
   // 
   // Since Cogl will automatically ensure samples are resolved if the
   // target color buffer is used as a source this API only needs to be
   // used if explicit control is desired - perhaps because you want to
   // ensure that the resolve is completed in advance to avoid later
   // having to wait for the resolve to complete.
   // 
   // If you are performing incremental updates to a framebuffer you
   // should consider using cogl_framebuffer_resolve_samples_region()
   // instead to avoid resolving redundant pixels.
   void resolve_samples()() nothrow {
      cogl_framebuffer_resolve_samples(&this);
   }

   // VERSION: 1.8
   // When point sample rendering (also known as multisample rendering)
   // has been enabled via cogl_framebuffer_set_samples_per_pixel()
   // then you can optionally call this function (or
   // cogl_framebuffer_resolve_samples()) to explicitly resolve the point
   // samples into values for the final color buffer.
   // 
   // Some GPUs will implicitly resolve the point samples during
   // rendering and so this function is effectively a nop, but with other
   // architectures it is desirable to defer the resolve step until the
   // end of the frame.
   // 
   // Use of this API is recommended if incremental, small updates to
   // a framebuffer are being made because by default Cogl will
   // implicitly resolve all the point samples of the framebuffer which
   // can result in redundant work if only a small number of samples have
   // changed.
   // 
   // Because some GPUs implicitly resolve point samples this function
   // only guarantees that at-least the region specified will be resolved
   // and if you have rendered to a larger region then it's possible that
   // other samples may be implicitly resolved.
   // <x>: top-left x coordinate of region to resolve
   // <y>: top-left y coordinate of region to resolve
   // <width>: width of region to resolve
   // <height>: height of region to resolve
   void resolve_samples_region()(int x, int y, int width, int height) nothrow {
      cogl_framebuffer_resolve_samples_region(&this, x, y, width, height);
   }

   // VERSION: 1.10
   // Multiplies the current model-view matrix by one that rotates the
   // model around the vertex specified by @x, @y and @z. The rotation
   // follows the right-hand thumb rule so for example rotating by 10
   // degrees about the vertex (0, 0, 1) causes a small counter-clockwise
   // rotation.
   // <angle>: Angle in degrees to rotate.
   // <x>: X-component of vertex to rotate around.
   // <y>: Y-component of vertex to rotate around.
   // <z>: Z-component of vertex to rotate around.
   void rotate()(float angle, float x, float y, float z) nothrow {
      cogl_framebuffer_rotate(&this, angle, x, y, z);
   }

   // VERSION: 1.10
   // Multiplies the current model-view matrix by one that scales the x,
   // y and z axes by the given values.
   // <x>: Amount to scale along the x-axis
   // <y>: Amount to scale along the y-axis
   // <z>: Amount to scale along the z-axis
   void scale()(float x, float y, float z) nothrow {
      cogl_framebuffer_scale(&this, x, y, z);
   }

   // VERSION: 1.8
   // Defines a bit mask of which color channels should be written to the
   // given @framebuffer. If a bit is set in @color_mask that means that
   // color will be written.
   // <color_mask>: A #CoglColorMask of which color channels to write to the current framebuffer.
   void set_color_mask()(ColorMask color_mask) nothrow {
      cogl_framebuffer_set_color_mask(&this, color_mask);
   }

   // VERSION: 1.8
   // Enables or disabled dithering if supported by the hardware.
   // 
   // Dithering is a hardware dependent technique to increase the visible
   // color resolution beyond what the underlying hardware supports by playing
   // tricks with the colors placed into the framebuffer to give the illusion
   // of other colors. (For example this can be compared to half-toning used
   // by some news papers to show varying levels of grey even though their may
   // only be black and white are available).
   // 
   // If the current display pipeline for @framebuffer does not support dithering
   // then this has no affect.
   // 
   // Dithering is enabled by default.
   // <dither_enabled>: %TRUE to enable dithering or %FALSE to disable
   void set_dither_enabled()(int dither_enabled) nothrow {
      cogl_framebuffer_set_dither_enabled(&this, dither_enabled);
   }

   // VERSION: 1.10
   // Sets @matrix as the new model-view matrix.
   // <matrix>: the new model-view matrix
   void set_modelview_matrix(AT0)(AT0 /*Matrix*/ matrix) nothrow {
      cogl_framebuffer_set_modelview_matrix(&this, UpCast!(Matrix*)(matrix));
   }
   void set_projection_matrix(AT0)(AT0 /*Matrix*/ matrix) nothrow {
      cogl_framebuffer_set_projection_matrix(&this, UpCast!(Matrix*)(matrix));
   }

   // VERSION: 1.8
   // Requires that when rendering to @framebuffer then @n point samples
   // should be made per pixel which will all contribute to the final
   // resolved color for that pixel. The idea is that the hardware aims
   // to get quality similar to what you would get if you rendered
   // everything twice as big (for 4 samples per pixel) and then scaled
   // that image back down with filtering. It can effectively remove the
   // jagged edges of polygons and should be more efficient than if you
   // were to manually render at a higher resolution and downscale
   // because the hardware is often able to take some shortcuts. For
   // example the GPU may only calculate a single texture sample for all
   // points of a single pixel, and for tile based architectures all the
   // extra sample data (such as depth and stencil samples) may be
   // handled on-chip and so avoid increased demand on system memory
   // bandwidth.
   // 
   // By default this value is usually set to 0 and that is referred to
   // as "single-sample" rendering. A value of 1 or greater is referred
   // to as "multisample" rendering.
   // 
   // <note>There are some semantic differences between single-sample
   // rendering and multisampling with just 1 point sample such as it
   // being redundant to use the cogl_framebuffer_resolve_samples() and
   // cogl_framebuffer_resolve_samples_region() apis with single-sample
   // rendering.</note>
   // 
   // <note>It's recommended that
   // cogl_framebuffer_resolve_samples_region() be explicitly used at the
   // end of rendering to a point sample buffer to minimize the number of
   // samples that get resolved. By default Cogl will implicitly resolve
   // all framebuffer samples but if only a small region of a
   // framebuffer has changed this can lead to redundant work being
   // done.</note>
   void set_samples_per_pixel()(int samples_per_pixel) nothrow {
      cogl_framebuffer_set_samples_per_pixel(&this, samples_per_pixel);
   }

   // VERSION: 1.8
   // Defines a scale and offset for everything rendered relative to the
   // top-left of the destination framebuffer.
   // 
   // By default the viewport has an origin of (0,0) and width and height
   // that match the framebuffer's size. Assuming a default projection and
   // modelview matrix then you could translate the contents of a window
   // down and right by leaving the viewport size unchanged by moving the
   // offset to (10,10). The viewport coordinates are measured in pixels.
   // If you left the x and y origin as (0,0) you could scale the windows
   // contents down by specify and width and height that's half the real
   // size of the framebuffer.
   // 
   // <note>Although the function takes floating point arguments, existing
   // drivers only allow the use of integer values. In the future floating
   // point values will be exposed via a checkable feature.</note>
   // <x>: The top-left x coordinate of the viewport origin (only integers supported currently)
   // <y>: The top-left y coordinate of the viewport origin (only integers supported currently)
   // <width>: The width of the viewport (only integers supported currently)
   // <height>: The height of the viewport (only integers supported currently)
   void set_viewport()(float x, float y, float width, float height) nothrow {
      cogl_framebuffer_set_viewport(&this, x, y, width, height);
   }

   // VERSION: 1.8
   // Swaps the current back buffer being rendered too, to the front for display.
   // 
   // This function also implicitly discards the contents of the color, depth and
   // stencil buffers as if cogl_framebuffer_discard_buffers() were used. The
   // significance of the discard is that you should not expect to be able to
   // start a new frame that incrementally builds on the contents of the previous
   // frame.
   void swap_buffers()() nothrow {
      cogl_framebuffer_swap_buffers(&this);
   }

   // VERSION: 1.8
   // Swaps a region of the back buffer being rendered too, to the front for
   // display.  @rectangles represents the region as array of @n_rectangles each
   // defined by 4 sequential (x, y, width, height) integers.
   // 
   // This function also implicitly discards the contents of the color, depth and
   // stencil buffers as if cogl_framebuffer_discard_buffers() were used. The
   // significance of the discard is that you should not expect to be able to
   // start a new frame that incrementally builds on the contents of the previous
   // frame.
   // <rectangles>: An array of integer 4-tuples representing rectangles as (x, y, width, height) tuples.
   // <n_rectangles>: The number of 4-tuples to be read from @rectangles
   void swap_region()(int* rectangles, int n_rectangles) nothrow {
      cogl_framebuffer_swap_region(&this, rectangles, n_rectangles);
   }

   // VERSION: 1.10
   // Multiplies the current model-view matrix by the given matrix.
   // <matrix>: the matrix to multiply with the current model-view
   void transform(AT0)(AT0 /*Matrix*/ matrix) nothrow {
      cogl_framebuffer_transform(&this, UpCast!(Matrix*)(matrix));
   }

   // VERSION: 1.10
   // Multiplies the current model-view matrix by one that translates the
   // model along all three axes according to the given values.
   // <x>: Distance to translate along the x-axis
   // <y>: Distance to translate along the y-axis
   // <z>: Distance to translate along the z-axis
   void translate()(float x, float y, float z) nothrow {
      cogl_framebuffer_translate(&this, x, y, z);
   }
}

enum FramebufferError {
   ALLOCATE = 0
}

// The type used by cogl for function pointers, note that this type
// is used as a generic catch-all cast for function pointers and the
// actual arguments and return type may be different.
extern (C) alias void function () nothrow FuncPtr;

struct IndexBuffer {

   // Unintrospectable function: new() / cogl_index_buffer_new()
   // VERSION: 1.4
   // Declares a new #CoglIndexBuffer of @size bytes to contain vertex
   // indices. Once declared, data can be set using
   // cogl_buffer_set_data() or by mapping it into the application's
   // address space using cogl_buffer_map().
   // <bytes>: The number of bytes to allocate for vertex attribute data.
   static IndexBuffer* new_()(size_t bytes) nothrow {
      return cogl_index_buffer_new(bytes);
   }
}

struct Indices {
   // Unintrospectable method: get_buffer() / cogl_indices_get_buffer()
   IndexBuffer* get_buffer()() nothrow {
      return cogl_indices_get_buffer(&this);
   }
   size_t get_offset()() nothrow {
      return cogl_indices_get_offset(&this);
   }
   void set_offset()(size_t offset) nothrow {
      cogl_indices_set_offset(&this, offset);
   }
   // Unintrospectable function: new() / cogl_indices_new()
   static Indices* new_(AT0)(IndicesType type, AT0 /*void*/ indices_data, int n_indices) nothrow {
      return cogl_indices_new(type, UpCast!(void*)(indices_data), n_indices);
   }
   // Unintrospectable function: new_for_buffer() / cogl_indices_new_for_buffer()
   static Indices* new_for_buffer(AT0)(IndicesType type, AT0 /*IndexBuffer*/ buffer, size_t offset) nothrow {
      return cogl_indices_new_for_buffer(type, UpCast!(IndexBuffer*)(buffer), offset);
   }
}


// You should aim to use the smallest data type that gives you enough
// range, since it reduces the size of your index array and can help
// reduce the demand on memory bandwidth.
// 
// Note that %COGL_INDICES_TYPE_UNSIGNED_INT is only supported if the
// %COGL_FEATURE_UNSIGNED_INT_INDICES feature is available. This
// should always be available on OpenGL but on OpenGL ES it will only
// be available if the GL_OES_element_index_uint extension is
// advertized.
enum IndicesType {
   BYTE = 0,
   SHORT = 1,
   INT = 2
}
struct Material {

   // Unintrospectable method: copy() / cogl_material_copy()
   // VERSION: 1.2
   // Creates a new material with the configuration copied from the
   // source material.
   // 
   // We would strongly advise developers to always aim to use
   // cogl_material_copy() instead of cogl_material_new() whenever there will
   // be any similarity between two materials. Copying a material helps Cogl
   // keep track of a materials ancestry which we may use to help minimize GPU
   // state changes.
   // RETURNS: a pointer to the newly allocated #CoglMaterial
   Material* copy()() nothrow {
      return cogl_material_copy(&this);
   }

   // Unintrospectable method: foreach_layer() / cogl_material_foreach_layer()
   // VERSION: 1.4
   // Iterates all the layer indices of the given @material.
   // <callback>: A #CoglMaterialLayerCallback to be called for each layer index
   // <user_data>: Private data that will be passed to the callback
   void foreach_layer(AT0)(MaterialLayerCallback callback, AT0 /*void*/ user_data) nothrow {
      cogl_material_foreach_layer(&this, callback, UpCast!(void*)(user_data));
   }

   // VERSION: 1.0
   // Retrieves the current ambient color for @material
   // <ambient>: The location to store the ambient color
   void get_ambient(AT0)(AT0 /*Color*/ ambient) nothrow {
      cogl_material_get_ambient(&this, UpCast!(Color*)(ambient));
   }

   // VERSION: 1.0
   // Retrieves the current material color.
   // <color>: The location to store the color
   void get_color(AT0)(/*out*/ AT0 /*Color*/ color) nothrow {
      cogl_material_get_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 2.0
   // Retrieves the current depth state configuration for the given
   // @pipeline as previously set using cogl_pipeline_set_depth_state().
   void get_depth_state(AT0)(AT0 /*DepthState*/ state_out) nothrow {
      cogl_material_get_depth_state(&this, UpCast!(DepthState*)(state_out));
   }

   // VERSION: 1.0
   // Retrieves the current diffuse color for @material
   // <diffuse>: The location to store the diffuse color
   void get_diffuse(AT0)(AT0 /*Color*/ diffuse) nothrow {
      cogl_material_get_diffuse(&this, UpCast!(Color*)(diffuse));
   }

   // VERSION: 1.0
   // Retrieves the materials current emission color.
   // <emission>: The location to store the emission color
   void get_emission(AT0)(AT0 /*Color*/ emission) nothrow {
      cogl_material_get_emission(&this, UpCast!(Color*)(emission));
   }

   // VERSION: 1.4
   // Gets whether point sprite coordinate generation is enabled for this
   // texture layer.
   // 
   // point sprite coordinates.
   // RETURNS: whether the texture coordinates will be replaced with
   // <layer_index>: the layer number to check.
   int get_layer_point_sprite_coords_enabled()(int layer_index) nothrow {
      return cogl_material_get_layer_point_sprite_coords_enabled(&this, layer_index);
   }

   // VERSION: 1.6
   // Returns the wrap mode for the 'p' coordinate of texture lookups on this
   // layer.
   // 
   // this layer.
   // RETURNS: the wrap mode for the 'p' coordinate of texture lookups on
   // <layer_index>: the layer number to change.
   MaterialWrapMode get_layer_wrap_mode_p()(int layer_index) nothrow {
      return cogl_material_get_layer_wrap_mode_p(&this, layer_index);
   }

   // VERSION: 1.6
   // Returns the wrap mode for the 's' coordinate of texture lookups on this
   // layer.
   // 
   // this layer.
   // RETURNS: the wrap mode for the 's' coordinate of texture lookups on
   // <layer_index>: the layer number to change.
   MaterialWrapMode get_layer_wrap_mode_s()(int layer_index) nothrow {
      return cogl_material_get_layer_wrap_mode_s(&this, layer_index);
   }

   // VERSION: 1.6
   // Returns the wrap mode for the 't' coordinate of texture lookups on this
   // layer.
   // 
   // this layer.
   // RETURNS: the wrap mode for the 't' coordinate of texture lookups on
   // <layer_index>: the layer number to change.
   MaterialWrapMode get_layer_wrap_mode_t()(int layer_index) nothrow {
      return cogl_material_get_layer_wrap_mode_t(&this, layer_index);
   }

   // This function lets you access a material's internal list of layers
   // for iteration.
   // 
   // <note>You should avoid using this API if possible since it was only
   // made public by mistake and will be deprecated when we have
   // suitable alternative.</note>
   // 
   // <note>It's important to understand that the list returned may not
   // remain valid if you modify the material or any of the layers in any
   // way and so you would have to re-get the list in that
   // situation.</note>
   // 
   // list of #CoglMaterialLayer<!-- -->'s that can be passed to the
   // cogl_material_layer_* functions. The list is owned by Cogl and it
   // should not be modified or freed
   // RETURNS: A
   GLib2.List* get_layers()() nothrow {
      return cogl_material_get_layers(&this);
   }

   // VERSION: 1.0
   // Retrieves the number of layers defined for the given @material
   // RETURNS: the number of layers
   int get_n_layers()() nothrow {
      return cogl_material_get_n_layers(&this);
   }

   // VERSION: 1.4
   // Get the size of points drawn when %COGL_VERTICES_MODE_POINTS is
   // used with the vertex buffer API.
   // RETURNS: the point size of the material.
   float get_point_size()() nothrow {
      return cogl_material_get_point_size(&this);
   }

   // VERSION: 1.0
   // Retrieves the materials current emission color.
   // RETURNS: The materials current shininess value
   float get_shininess()() nothrow {
      return cogl_material_get_shininess(&this);
   }

   // VERSION: 1.0
   // Retrieves the materials current specular color.
   // <specular>: The location to store the specular color
   void get_specular(AT0)(AT0 /*Color*/ specular) nothrow {
      cogl_material_get_specular(&this, UpCast!(Color*)(specular));
   }

   // VERSION: 1.4
   // Queries what user program has been associated with the given
   // @material using cogl_material_set_user_program().
   // 
   // or %COGL_INVALID_HANDLE.
   // RETURNS: The current user program
   Handle get_user_program()() nothrow {
      return cogl_material_get_user_program(&this);
   }

   // This function removes a layer from your material
   // <layer_index>: Specifies the layer you want to remove
   void remove_layer()(int layer_index) nothrow {
      cogl_material_remove_layer(&this, layer_index);
   }

   // VERSION: 1.0
   // Before a primitive is blended with the framebuffer, it goes through an
   // alpha test stage which lets you discard fragments based on the current
   // alpha value. This function lets you change the function used to evaluate
   // the alpha channel, and thus determine which fragments are discarded
   // and which continue on to the blending stage.
   // 
   // The default is %COGL_MATERIAL_ALPHA_FUNC_ALWAYS
   // <alpha_func>: A @CoglMaterialAlphaFunc constant
   // <alpha_reference>: A reference point that the chosen alpha function uses to compare incoming fragments to.
   void set_alpha_test_function()(MaterialAlphaFunc alpha_func, float alpha_reference) nothrow {
      cogl_material_set_alpha_test_function(&this, alpha_func, alpha_reference);
   }

   // VERSION: 1.0
   // Sets the material's ambient color, in the standard OpenGL lighting
   // model. The ambient color affects the overall color of the object.
   // 
   // Since the diffuse color will be intense when the light hits the surface
   // directly, the ambient will be most apparent where the light hits at a
   // slant.
   // 
   // The default value is (0.2, 0.2, 0.2, 1.0)
   // <ambient>: The components of the desired ambient color
   void set_ambient(AT0)(AT0 /*Color*/ ambient) nothrow {
      cogl_material_set_ambient(&this, UpCast!(Color*)(ambient));
   }

   // VERSION: 1.0
   // Conveniently sets the diffuse and ambient color of @material at the same
   // time. See cogl_material_set_ambient() and cogl_material_set_diffuse().
   // 
   // The default ambient color is (0.2, 0.2, 0.2, 1.0)
   // 
   // The default diffuse color is (0.8, 0.8, 0.8, 1.0)
   // <color>: The components of the desired ambient and diffuse colors
   void set_ambient_and_diffuse(AT0)(AT0 /*Color*/ color) nothrow {
      cogl_material_set_ambient_and_diffuse(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.0
   // If not already familiar; please refer <link linkend="cogl-Blend-Strings">here</link>
   // for an overview of what blend strings are, and their syntax.
   // 
   // Blending occurs after the alpha test function, and combines fragments with
   // the framebuffer.
   // Currently the only blend function Cogl exposes is ADD(). So any valid
   // blend statements will be of the form:
   // 
   // |[
   // &lt;channel-mask&gt;=ADD(SRC_COLOR*(&lt;factor&gt;), DST_COLOR*(&lt;factor&gt;))
   // ]|
   // 
   // <warning>The brackets around blend factors are currently not
   // optional!</warning>
   // 
   // This is the list of source-names usable as blend factors:
   // <itemizedlist>
   // <listitem><para>SRC_COLOR: The color of the in comming fragment</para></listitem>
   // <listitem><para>DST_COLOR: The color of the framebuffer</para></listitem>
   // <listitem><para>CONSTANT: The constant set via cogl_material_set_blend_constant()</para></listitem>
   // </itemizedlist>
   // 
   // The source names can be used according to the
   // <link linkend="cogl-Blend-String-syntax">color-source and factor syntax</link>,
   // so for example "(1-SRC_COLOR[A])" would be a valid factor, as would
   // "(CONSTANT[RGB])"
   // 
   // These can also be used as factors:
   // <itemizedlist>
   // <listitem>0: (0, 0, 0, 0)</listitem>
   // <listitem>1: (1, 1, 1, 1)</listitem>
   // <listitem>SRC_ALPHA_SATURATE_FACTOR: (f,f,f,1) where f = MIN(SRC_COLOR[A],1-DST_COLOR[A])</listitem>
   // </itemizedlist>
   // 
   // <note>Remember; all color components are normalized to the range [0, 1]
   // before computing the result of blending.</note>
   // 
   // <example id="cogl-Blend-Strings-blend-unpremul">
   // <title>Blend Strings/1</title>
   // <para>Blend a non-premultiplied source over a destination with
   // premultiplied alpha:</para>
   // <programlisting>
   // "RGB = ADD(SRC_COLOR*(SRC_COLOR[A]), DST_COLOR*(1-SRC_COLOR[A]))"
   // "A   = ADD(SRC_COLOR, DST_COLOR*(1-SRC_COLOR[A]))"
   // </programlisting>
   // </example>
   // 
   // <example id="cogl-Blend-Strings-blend-premul">
   // <title>Blend Strings/2</title>
   // <para>Blend a premultiplied source over a destination with
   // premultiplied alpha</para>
   // <programlisting>
   // "RGBA = ADD(SRC_COLOR, DST_COLOR*(1-SRC_COLOR[A]))"
   // </programlisting>
   // </example>
   // 
   // The default blend string is:
   // |[
   // RGBA = ADD (SRC_COLOR, DST_COLOR*(1-SRC_COLOR[A]))
   // ]|
   // 
   // That gives normal alpha-blending when the calculated color for the material
   // is in premultiplied form.
   // 
   // described blending is supported by the underlying driver/hardware. If
   // there was an error, %FALSE is returned and @error is set accordingly (if
   // present).
   // RETURNS: %TRUE if the blend string was successfully parsed, and the
   // <blend_string>: A <link linkend="cogl-Blend-Strings">Cogl blend string</link> describing the desired blend function.
   int set_blend(AT0, AT1)(AT0 /*char*/ blend_string, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_material_set_blend(&this, toCString!(char*)(blend_string), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.0
   // When blending is setup to reference a CONSTANT blend factor then
   // blending will depend on the constant set with this function.
   // <constant_color>: The constant color you want
   void set_blend_constant(AT0)(AT0 /*Color*/ constant_color) nothrow {
      cogl_material_set_blend_constant(&this, UpCast!(Color*)(constant_color));
   }

   // VERSION: 1.0
   // Sets the basic color of the material, used when no lighting is enabled.
   // 
   // Note that if you don't add any layers to the material then the color
   // will be blended unmodified with the destination; the default blend
   // expects premultiplied colors: for example, use (0.5, 0.0, 0.0, 0.5) for
   // semi-transparent red. See cogl_color_premultiply().
   // 
   // The default value is (1.0, 1.0, 1.0, 1.0)
   // <color>: The components of the color
   void set_color(AT0)(AT0 /*Color*/ color) nothrow {
      cogl_material_set_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.0
   // Sets the basic color of the material, used when no lighting is enabled.
   // 
   // The default value is (1.0, 1.0, 1.0, 1.0)
   // <red>: The red component
   // <green>: The green component
   // <blue>: The blue component
   // <alpha>: The alpha component
   void set_color4f()(float red, float green, float blue, float alpha) nothrow {
      cogl_material_set_color4f(&this, red, green, blue, alpha);
   }

   // VERSION: 1.0
   // Sets the basic color of the material, used when no lighting is enabled.
   // 
   // The default value is (0xff, 0xff, 0xff, 0xff)
   // <red>: The red component
   // <green>: The green component
   // <blue>: The blue component
   // <alpha>: The alpha component
   void set_color4ub()(ubyte red, ubyte green, ubyte blue, ubyte alpha) nothrow {
      cogl_material_set_color4ub(&this, red, green, blue, alpha);
   }

   // VERSION: 1.8
   // This commits all the depth state configured in @state struct to the
   // given @material. The configuration values are copied into the
   // material so there is no requirement to keep the #CoglDepthState
   // struct around if you don't need it any more.
   // 
   // Note: Since some platforms do not support the depth range feature
   // it is possible for this function to fail and report an @error.
   // 
   // and returns an @error.
   // RETURNS: TRUE if the GPU supports all the given @state else %FALSE
   // <state>: A #CoglDepthState struct
   int set_depth_state(AT0, AT1)(AT0 /*DepthState*/ state, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_material_set_depth_state(&this, UpCast!(DepthState*)(state), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.0
   // Sets the material's diffuse color, in the standard OpenGL lighting
   // model. The diffuse color is most intense where the light hits the
   // surface directly - perpendicular to the surface.
   // 
   // The default value is (0.8, 0.8, 0.8, 1.0)
   // <diffuse>: The components of the desired diffuse color
   void set_diffuse(AT0)(AT0 /*Color*/ diffuse) nothrow {
      cogl_material_set_diffuse(&this, UpCast!(Color*)(diffuse));
   }

   // VERSION: 1.0
   // Sets the material's emissive color, in the standard OpenGL lighting
   // model. It will look like the surface is a light source emitting this
   // color.
   // 
   // The default value is (0.0, 0.0, 0.0, 1.0)
   // <emission>: The components of the desired emissive color
   void set_emission(AT0)(AT0 /*Color*/ emission) nothrow {
      cogl_material_set_emission(&this, UpCast!(Color*)(emission));
   }

   // VERSION: 1.0
   // In addition to the standard OpenGL lighting model a Cogl material may have
   // one or more layers comprised of textures that can be blended together in
   // order, with a number of different texture combine modes. This function
   // defines a new texture layer.
   // 
   // The index values of multiple layers do not have to be consecutive; it is
   // only their relative order that is important.
   // 
   // <note>In the future, we may define other types of material layers, such
   // as purely GLSL based layers.</note>
   // <layer_index>: the index of the layer
   // <texture>: a #CoglHandle for the layer object
   void set_layer()(int layer_index, Handle texture) nothrow {
      cogl_material_set_layer(&this, layer_index, texture);
   }

   // VERSION: 1.0
   // If not already familiar; you can refer
   // <link linkend="cogl-Blend-Strings">here</link> for an overview of what blend
   // strings are and there syntax.
   // 
   // These are all the functions available for texture combining:
   // <itemizedlist>
   // <listitem>REPLACE(arg0) = arg0</listitem>
   // <listitem>MODULATE(arg0, arg1) = arg0 x arg1</listitem>
   // <listitem>ADD(arg0, arg1) = arg0 + arg1</listitem>
   // <listitem>ADD_SIGNED(arg0, arg1) = arg0 + arg1 - 0.5</listitem>
   // <listitem>INTERPOLATE(arg0, arg1, arg2) = arg0 x arg2 + arg1 x (1 - arg2)</listitem>
   // <listitem>SUBTRACT(arg0, arg1) = arg0 - arg1</listitem>
   // <listitem>
   // <programlisting>
   // DOT3_RGB(arg0, arg1) = 4 x ((arg0[R] - 0.5)) * (arg1[R] - 0.5) +
   // (arg0[G] - 0.5)) * (arg1[G] - 0.5) +
   // (arg0[B] - 0.5)) * (arg1[B] - 0.5))
   // </programlisting>
   // </listitem>
   // <listitem>
   // <programlisting>
   // DOT3_RGBA(arg0, arg1) = 4 x ((arg0[R] - 0.5)) * (arg1[R] - 0.5) +
   // (arg0[G] - 0.5)) * (arg1[G] - 0.5) +
   // (arg0[B] - 0.5)) * (arg1[B] - 0.5))
   // </programlisting>
   // </listitem>
   // </itemizedlist>
   // 
   // Refer to the
   // <link linkend="cogl-Blend-String-syntax">color-source syntax</link> for
   // describing the arguments. The valid source names for texture combining
   // are:
   // <variablelist>
   // <varlistentry>
   // <term>TEXTURE</term>
   // <listitem>Use the color from the current texture layer</listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>TEXTURE_0, TEXTURE_1, etc</term>
   // <listitem>Use the color from the specified texture layer</listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>CONSTANT</term>
   // <listitem>Use the color from the constant given with
   // cogl_material_set_layer_constant()</listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>PRIMARY</term>
   // <listitem>Use the color of the material as set with
   // cogl_material_set_color()</listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>PREVIOUS</term>
   // <listitem>Either use the texture color from the previous layer, or
   // if this is layer 0, use the color of the material as set with
   // cogl_material_set_color()</listitem>
   // </varlistentry>
   // </variablelist>
   // 
   // <refsect2 id="cogl-Layer-Combine-Examples">
   // <title>Layer Combine Examples</title>
   // <para>This is effectively what the default blending is:</para>
   // <informalexample><programlisting>
   // RGBA = MODULATE (PREVIOUS, TEXTURE)
   // </programlisting></informalexample>
   // <para>This could be used to cross-fade between two images, using
   // the alpha component of a constant as the interpolator. The constant
   // color is given by calling cogl_material_set_layer_constant.</para>
   // <informalexample><programlisting>
   // RGBA = INTERPOLATE (PREVIOUS, TEXTURE, CONSTANT[A])
   // </programlisting></informalexample>
   // </refsect2>
   // 
   // <note>You can't give a multiplication factor for arguments as you can
   // with blending.</note>
   // 
   // described texture combining is supported by the underlying driver and
   // or hardware. On failure, %FALSE is returned and @error is set
   // RETURNS: %TRUE if the blend string was successfully parsed, and the
   // <layer_index>: Specifies the layer you want define a combine function for
   // <blend_string>: A <link linkend="cogl-Blend-Strings">Cogl blend string</link> describing the desired texture combine function.
   int set_layer_combine(AT0, AT1)(int layer_index, AT0 /*char*/ blend_string, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_material_set_layer_combine(&this, layer_index, toCString!(char*)(blend_string), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.0
   // When you are using the 'CONSTANT' color source in a layer combine
   // description then you can use this function to define its value.
   // <layer_index>: Specifies the layer you want to specify a constant used for texture combining
   // <constant>: The constant color you want
   void set_layer_combine_constant(AT0)(int layer_index, AT0 /*Color*/ constant) nothrow {
      cogl_material_set_layer_combine_constant(&this, layer_index, UpCast!(Color*)(constant));
   }

   // Changes the decimation and interpolation filters used when a texture is
   // drawn at other scales than 100%.
   // <layer_index>: the layer number to change.
   // <min_filter>: the filter used when scaling a texture down.
   // <mag_filter>: the filter used when magnifying a texture.
   void set_layer_filters()(int layer_index, MaterialFilter min_filter, MaterialFilter mag_filter) nothrow {
      cogl_material_set_layer_filters(&this, layer_index, min_filter, mag_filter);
   }

   // This function lets you set a matrix that can be used to e.g. translate
   // and rotate a single layer of a material used to fill your geometry.
   // <layer_index>: the index for the layer inside @material
   // <matrix>: the transformation matrix for the layer
   void set_layer_matrix(AT0)(int layer_index, AT0 /*Matrix*/ matrix) nothrow {
      cogl_material_set_layer_matrix(&this, layer_index, UpCast!(Matrix*)(matrix));
   }

   // VERSION: 1.4
   // When rendering points, if @enable is %TRUE then the texture
   // coordinates for this layer will be replaced with coordinates that
   // vary from 0.0 to 1.0 across the primitive. The top left of the
   // point will have the coordinates 0.0,0.0 and the bottom right will
   // have 1.0,1.0. If @enable is %FALSE then the coordinates will be
   // fixed for the entire point.
   // 
   // This function will only work if %COGL_FEATURE_POINT_SPRITE is
   // available. If the feature is not available then the function will
   // return %FALSE and set @error.
   // RETURNS: %TRUE if the function succeeds, %FALSE otherwise.
   // <layer_index>: the layer number to change.
   // <enable>: whether to enable point sprite coord generation.
   int set_layer_point_sprite_coords_enabled(AT0)(int layer_index, int enable, AT0 /*GLib2.Error**/ error=null) nothrow {
      return cogl_material_set_layer_point_sprite_coords_enabled(&this, layer_index, enable, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.4
   // Sets the wrap mode for all three coordinates of texture lookups on
   // this layer. This is equivalent to calling
   // cogl_material_set_layer_wrap_mode_s(),
   // cogl_material_set_layer_wrap_mode_t() and
   // cogl_material_set_layer_wrap_mode_p() separately.
   // <layer_index>: the layer number to change.
   // <mode>: the new wrap mode
   void set_layer_wrap_mode()(int layer_index, MaterialWrapMode mode) nothrow {
      cogl_material_set_layer_wrap_mode(&this, layer_index, mode);
   }

   // VERSION: 1.4
   // Sets the wrap mode for the 'p' coordinate of texture lookups on
   // this layer. 'p' is the third coordinate.
   // <layer_index>: the layer number to change.
   // <mode>: the new wrap mode
   void set_layer_wrap_mode_p()(int layer_index, MaterialWrapMode mode) nothrow {
      cogl_material_set_layer_wrap_mode_p(&this, layer_index, mode);
   }

   // VERSION: 1.4
   // Sets the wrap mode for the 's' coordinate of texture lookups on this layer.
   // <layer_index>: the layer number to change.
   // <mode>: the new wrap mode
   void set_layer_wrap_mode_s()(int layer_index, MaterialWrapMode mode) nothrow {
      cogl_material_set_layer_wrap_mode_s(&this, layer_index, mode);
   }

   // VERSION: 1.4
   // Sets the wrap mode for the 't' coordinate of texture lookups on this layer.
   // <layer_index>: the layer number to change.
   // <mode>: the new wrap mode
   void set_layer_wrap_mode_t()(int layer_index, MaterialWrapMode mode) nothrow {
      cogl_material_set_layer_wrap_mode_t(&this, layer_index, mode);
   }

   // VERSION: 1.4
   // Changes the size of points drawn when %COGL_VERTICES_MODE_POINTS is
   // used with the vertex buffer API. Note that typically the GPU will
   // only support a limited minimum and maximum range of point sizes. If
   // the chosen point size is outside that range then the nearest value
   // within that range will be used instead. The size of a point is in
   // screen space so it will be the same regardless of any
   // transformations. The default point size is 1.0.
   // <point_size>: the new point size.
   void set_point_size()(float point_size) nothrow {
      cogl_material_set_point_size(&this, point_size);
   }

   // VERSION: 1.0
   // Sets the shininess of the material, in the standard OpenGL lighting
   // model, which determines the size of the specular highlights. A
   // higher @shininess will produce smaller highlights which makes the
   // object appear more shiny.
   // 
   // The default value is 0.0
   // <shininess>: The desired shininess; must be >= 0.0
   void set_shininess()(float shininess) nothrow {
      cogl_material_set_shininess(&this, shininess);
   }

   // VERSION: 1.0
   // Sets the material's specular color, in the standard OpenGL lighting
   // model. The intensity of the specular color depends on the viewport
   // position, and is brightest along the lines of reflection.
   // 
   // The default value is (0.0, 0.0, 0.0, 1.0)
   // <specular>: The components of the desired specular color
   void set_specular(AT0)(AT0 /*Color*/ specular) nothrow {
      cogl_material_set_specular(&this, UpCast!(Color*)(specular));
   }

   // VERSION: 1.4
   // Associates a linked CoglProgram with the given material so that the
   // program can take full control of vertex and/or fragment processing.
   // 
   // This is an example of how it can be used to associate an ARBfp
   // program with a #CoglMaterial:
   // |[
   // CoglHandle shader;
   // CoglHandle program;
   // CoglMaterial *material;
   // 
   // shader = cogl_create_shader (COGL_SHADER_TYPE_FRAGMENT);
   // cogl_shader_source (shader,
   // "!!ARBfp1.0\n"
   // "MOV result.color,fragment.color;\n"
   // "END\n");
   // cogl_shader_compile (shader);
   // 
   // program = cogl_create_program ();
   // cogl_program_attach_shader (program, shader);
   // cogl_program_link (program);
   // 
   // material = cogl_material_new ();
   // cogl_material_set_user_program (material, program);
   // 
   // cogl_set_source_color4ub (0xff, 0x00, 0x00, 0xff);
   // cogl_rectangle (0, 0, 100, 100);
   // ]|
   // 
   // It is possibly worth keeping in mind that this API is not part of
   // the long term design for how we want to expose shaders to Cogl
   // developers (We are planning on deprecating the cogl_program and
   // cogl_shader APIs in favour of a "snippet" framework) but in the
   // meantime we hope this will handle most practical GLSL and ARBfp
   // requirements.
   // 
   // Also remember you need to check for either the
   // %COGL_FEATURE_SHADERS_GLSL or %COGL_FEATURE_SHADERS_ARBFP before
   // using the cogl_program or cogl_shader API.
   // <program>: A #CoglHandle to a linked CoglProgram
   void set_user_program()(Handle program) nothrow {
      cogl_material_set_user_program(&this, program);
   }

   // Unintrospectable function: new() / cogl_material_new()
   // Allocates and initializes a blank white material
   // RETURNS: a pointer to a new #CoglMaterial
   static Material* new_()() nothrow {
      return cogl_material_new();
   }

   // Unintrospectable function: ref() / cogl_material_ref()
   // VERSION: 1.0
   // DEPRECATED (v1.2) function: ref - Use cogl_object_ref() instead
   // Increment the reference count for a #CoglMaterial.
   // RETURNS: the @material.
   // <material>: a #CoglMaterial object.
   static Handle ref_()(Handle material) nothrow {
      return cogl_material_ref(material);
   }

   // VERSION: 1.0
   // DEPRECATED (v1.2) function: unref - Use cogl_object_unref() instead
   // Decrement the reference count for a #CoglMaterial.
   // <material>: a #CoglMaterial object.
   static void unref()(Handle material) nothrow {
      cogl_material_unref(material);
   }
}


// Alpha testing happens before blending primitives with the framebuffer and
// gives an opportunity to discard fragments based on a comparison with the
// incoming alpha value and a reference alpha value. The #CoglMaterialAlphaFunc
// determines how the comparison is done.
enum MaterialAlphaFunc {
   NEVER = 512,
   LESS = 513,
   EQUAL = 514,
   LEQUAL = 515,
   GREATER = 516,
   NOTEQUAL = 517,
   GEQUAL = 518,
   ALWAYS = 519
}

// Texture filtering is used whenever the current pixel maps either to more
// than one texture element (texel) or less than one. These filter enums
// correspond to different strategies used to come up with a pixel color, by
// possibly referring to multiple neighbouring texels and taking a weighted
// average or simply using the nearest texel.
enum MaterialFilter {
   NEAREST = 9728,
   LINEAR = 9729,
   NEAREST_MIPMAP_NEAREST = 9984,
   LINEAR_MIPMAP_NEAREST = 9985,
   NEAREST_MIPMAP_LINEAR = 9986,
   LINEAR_MIPMAP_LINEAR = 9987
}
struct MaterialLayer {

   // Queries the currently set downscaling filter for a material later
   // RETURNS: the current downscaling filter
   MaterialFilter get_mag_filter()() nothrow {
      return cogl_material_layer_get_mag_filter(&this);
   }

   // Queries the currently set downscaling filter for a material layer
   // RETURNS: the current downscaling filter
   MaterialFilter get_min_filter()() nothrow {
      return cogl_material_layer_get_min_filter(&this);
   }

   // Extracts a texture handle for a specific layer.
   // 
   // <note>In the future Cogl may support purely GLSL based layers; for those
   // layers this function which will likely return %COGL_INVALID_HANDLE if you
   // try to get the texture handle from them. Considering this scenario, you
   // should call cogl_material_layer_get_type() first in order check it is of
   // type %COGL_MATERIAL_LAYER_TYPE_TEXTURE before calling this function.</note>
   // RETURNS: a #CoglHandle for the texture inside the layer
   Handle get_texture()() nothrow {
      return cogl_material_layer_get_texture(&this);
   }

   // VERSION: 1.4
   // Gets the wrap mode for the 'p' coordinate of texture lookups on
   // this layer. 'p' is the third coordinate.
   // RETURNS: the wrap mode value for the p coordinate.
   MaterialWrapMode get_wrap_mode_p()() nothrow {
      return cogl_material_layer_get_wrap_mode_p(&this);
   }

   // VERSION: 1.4
   // Gets the wrap mode for the 's' coordinate of texture lookups on this layer.
   // RETURNS: the wrap mode value for the s coordinate.
   MaterialWrapMode get_wrap_mode_s()() nothrow {
      return cogl_material_layer_get_wrap_mode_s(&this);
   }

   // VERSION: 1.4
   // Gets the wrap mode for the 't' coordinate of texture lookups on this layer.
   // RETURNS: the wrap mode value for the t coordinate.
   MaterialWrapMode get_wrap_mode_t()() nothrow {
      return cogl_material_layer_get_wrap_mode_t(&this);
   }
}


// VERSION: 1.4
// The callback prototype used with cogl_material_foreach_layer() for
// iterating all the layers of a @material.
// <material>: The #CoglMaterial whos layers are being iterated
// <layer_index>: The current layer index
// <user_data>: The private data passed to cogl_material_foreach_layer()
extern (C) alias int function (Material* material, int layer_index, void* user_data) nothrow MaterialLayerCallback;


// Available types of layers for a #CoglMaterial. This enumeration
// might be expanded in later versions.
enum MaterialLayerType /* Version 1.0 */ {
   TEXTURE = 0
}

// The wrap mode specifies what happens when texture coordinates
// outside the range 0→1 are used. Note that if the filter mode is
// anything but %COGL_MATERIAL_FILTER_NEAREST then texels outside the
// range 0→1 might be used even when the coordinate is exactly 0 or 1
// because OpenGL will try to sample neighbouring pixels. For example
// if you are trying to render the full texture then you may get
// artifacts around the edges when the pixels from the other side are
// merged in if the wrap mode is set to repeat.
enum MaterialWrapMode /* Version 1.4 */ {
   REPEAT = 10497,
   CLAMP_TO_EDGE = 33071,
   AUTOMATIC = 519
}

// A CoglMatrix holds a 4x4 transform matrix. This is a single precision,
// column-major matrix which means it is compatible with what OpenGL expects.
// 
// A CoglMatrix can represent transforms such as, rotations, scaling,
// translation, sheering, and linear projections. You can combine these
// transforms by multiplying multiple matrices in the order you want them
// applied.
// 
// The transformation of a vertex (x, y, z, w) by a CoglMatrix is given by:
// 
// |[
// x_new = xx * x + xy * y + xz * z + xw * w
// y_new = yx * x + yy * y + yz * z + yw * w
// z_new = zx * x + zy * y + zz * z + zw * w
// w_new = wx * x + wy * y + wz * z + ww * w
// ]|
// 
// Where w is normally 1
// 
// <note>You must consider the members of the CoglMatrix structure read only,
// and all matrix modifications must be done via the cogl_matrix API. This
// allows Cogl to annotate the matrices internally. Violation of this will give
// undefined results. If you need to initialize a matrix with a constant other
// than the identity matrix you can use cogl_matrix_init_from_array().</note>
struct Matrix {
   float xx, yx, zx, wx, xy, yy, zy, wy, xz, yz, zz, wz, xw, yw, zw, ww;
   private float[16] inv;
   private uint type, flags, _padding3;


   // Unintrospectable method: copy() / cogl_matrix_copy()
   // VERSION: 1.6
   // Allocates a new #CoglMatrix on the heap and initializes it with
   // the same values as @matrix.
   // 
   // cogl_matrix_free()
   // RETURNS: A newly allocated #CoglMatrix which should be freed using
   Matrix* copy()() nothrow {
      return cogl_matrix_copy(&this);
   }

   // VERSION: 1.6
   // Frees a #CoglMatrix that was previously allocated via a call to
   // cogl_matrix_copy().
   void free()() nothrow {
      cogl_matrix_free(&this);
   }

   // Multiplies @matrix by the given frustum perspective matrix.
   // <left>: X position of the left clipping plane where it intersects the near clipping plane
   // <right>: X position of the right clipping plane where it intersects the near clipping plane
   // <bottom>: Y position of the bottom clipping plane where it intersects the near clipping plane
   // <top>: Y position of the top clipping plane where it intersects the near clipping plane
   // <z_near>: The distance to the near clipping plane (Must be positive)
   // <z_far>: The distance to the far clipping plane (Must be positive)
   void frustum()(float left, float right, float bottom, float top, float z_near, float z_far) nothrow {
      cogl_matrix_frustum(&this, left, right, bottom, top, z_near, z_far);
   }

   // Casts @matrix to a float array which can be directly passed to OpenGL.
   // RETURNS: a pointer to the float array
   float* get_array()() nothrow {
      return cogl_matrix_get_array(&this);
   }

   // VERSION: 1.2
   // Gets the inverse transform of a given matrix and uses it to initialize
   // a new #CoglMatrix.
   // 
   // <note>Although the first parameter is annotated as const to indicate
   // that the transform it represents isn't modified this function may
   // technically save a copy of the inverse transform within the given
   // #CoglMatrix so that subsequent requests for the inverse transform may
   // avoid costly inversion calculations.</note>
   // 
   // for degenerate transformations that can't be inverted (in this case the
   // @inverse matrix will simply be initialized with the identity matrix)
   // RETURNS: %TRUE if the inverse was successfully calculated or %FALSE
   // <inverse>: The destination for a 4x4 inverse transformation matrix
   int get_inverse(AT0)(/*out*/ AT0 /*Matrix*/ inverse) nothrow {
      return cogl_matrix_get_inverse(&this, UpCast!(Matrix*)(inverse));
   }

   // Initializes @matrix with the contents of @array
   // <array>: A linear array of 16 floats (column-major order)
   void init_from_array(AT0)(AT0 /*float*/ array) nothrow {
      cogl_matrix_init_from_array(&this, UpCast!(float*)(array));
   }

   // Initializes @matrix from a #CoglQuaternion rotation.
   // RETURNS: a pointer to the float array
   // <quaternion>: A #CoglQuaternion
   void init_from_quaternion(AT0)(AT0 /*Quaternion*/ quaternion) nothrow {
      cogl_matrix_init_from_quaternion(&this, UpCast!(Quaternion*)(quaternion));
   }

   // Resets matrix to the identity matrix:
   // 
   // |[
   // .xx=1; .xy=0; .xz=0; .xw=0;
   // .yx=0; .yy=1; .yz=0; .yw=0;
   // .zx=0; .zy=0; .zz=1; .zw=0;
   // .wx=0; .wy=0; .wz=0; .ww=1;
   // ]|
   void init_identity()() nothrow {
      cogl_matrix_init_identity(&this);
   }

   // VERSION: 1.8
   // Determines if the given matrix is an identity matrix.
   // RETURNS: %TRUE if @matrix is an identity matrix else %FALSE
   int is_identity()() nothrow {
      return cogl_matrix_is_identity(&this);
   }

   // VERSION: 1.8
   // Applies a view transform @matrix that positions the camera at
   // the coordinate (@eye_position_x, @eye_position_y, @eye_position_z)
   // looking towards an object at the coordinate (@object_x, @object_y,
   // @object_z). The top of the camera is aligned to the given world up
   // vector, which is normally simply (0, 1, 0) to map up to the
   // positive direction of the y axis.
   // 
   // Because there is a lot of missleading documentation online for
   // gluLookAt regarding the up vector we want to try and be a bit
   // clearer here.
   // 
   // The up vector should simply be relative to your world coordinates
   // and does not need to change as you move the eye and object
   // positions.  Many online sources may claim that the up vector needs
   // to be perpendicular to the vector between the eye and object
   // position (partly because the man page is somewhat missleading) but
   // that is not necessary for this function.
   // 
   // <note>You should never look directly along the world-up
   // vector.</note>
   // 
   // <note>It is assumed you are using a typical projection matrix where
   // your origin maps to the center of your viewport.</note>
   // 
   // <note>Almost always when you use this function it should be the first
   // transform applied to a new modelview transform</note>
   // <eye_position_x>: The X coordinate to look from
   // <eye_position_y>: The Y coordinate to look from
   // <eye_position_z>: The Z coordinate to look from
   // <object_x>: The X coordinate of the object to look at
   // <object_y>: The Y coordinate of the object to look at
   // <object_z>: The Z coordinate of the object to look at
   // <world_up_x>: The X component of the world's up direction vector
   // <world_up_y>: The Y component of the world's up direction vector
   // <world_up_z>: The Z component of the world's up direction vector
   void look_at()(float eye_position_x, float eye_position_y, float eye_position_z, float object_x, float object_y, float object_z, float world_up_x, float world_up_y, float world_up_z) nothrow {
      cogl_matrix_look_at(&this, eye_position_x, eye_position_y, eye_position_z, object_x, object_y, object_z, world_up_x, world_up_y, world_up_z);
   }

   // Multiplies the two supplied matrices together and stores
   // the resulting matrix inside @result.
   // 
   // <note>It is possible to multiply the @a matrix in-place, so
   // @result can be equal to @a but can't be equal to @b.</note>
   // <a>: A 4x4 transformation matrix
   // <b>: A 4x4 transformation matrix
   void multiply(AT0, AT1)(AT0 /*Matrix*/ a, AT1 /*Matrix*/ b) nothrow {
      cogl_matrix_multiply(&this, UpCast!(Matrix*)(a), UpCast!(Matrix*)(b));
   }

   // DEPRECATED (v1.10) method: ortho - Use cogl_matrix_orthographic()
   // Multiplies @matrix by a parallel projection matrix.
   // <left>: The coordinate for the left clipping plane
   // <right>: The coordinate for the right clipping plane
   // <bottom>: The coordinate for the bottom clipping plane
   // <top>: The coordinate for the top clipping plane
   // <near>: The <emphasis>distance</emphasis> to the near clipping plane (will be <emphasis>negative</emphasis> if the plane is behind the viewer)
   // <far>: The <emphasis>distance</emphasis> to the far clipping plane (will be <emphasis>negative</emphasis> if the plane is behind the viewer)
   void ortho()(float left, float right, float bottom, float top, float near, float far) nothrow {
      cogl_matrix_ortho(&this, left, right, bottom, top, near, far);
   }

   // VERSION: 1.10
   // Multiplies @matrix by a parallel projection matrix.
   // <x_1>: The x coordinate for the first vertical clipping plane
   // <y_1>: The y coordinate for the first horizontal clipping plane
   // <x_2>: The x coordinate for the second vertical clipping plane
   // <y_2>: The y coordinate for the second horizontal clipping plane
   // <near>: The <emphasis>distance</emphasis> to the near clipping plane (will be <emphasis>negative</emphasis> if the plane is behind the viewer)
   // <far>: The <emphasis>distance</emphasis> to the far clipping plane (will be <emphasis>negative</emphasis> if the plane is behind the viewer)
   void orthographic()(float x_1, float y_1, float x_2, float y_2, float near, float far) nothrow {
      cogl_matrix_orthographic(&this, x_1, y_1, x_2, y_2, near, far);
   }

   // Multiplies @matrix by the described perspective matrix
   // 
   // <note>You should be careful not to have to great a @z_far / @z_near
   // ratio since that will reduce the effectiveness of depth testing
   // since there wont be enough precision to identify the depth of
   // objects near to each other.</note>
   // <aspect>: The (width over height) aspect ratio for display
   // <z_near>: The distance to the near clipping plane (Must be positive, and must not be 0)
   // <z_far>: The distance to the far clipping plane (Must be positive)
   void perspective()(float fov_y, float aspect, float z_near, float z_far) nothrow {
      cogl_matrix_perspective(&this, fov_y, aspect, z_near, z_far);
   }

   // Projects an array of input points and writes the result to another
   // array of output points. The input points can either have 2, 3 or 4
   // components each. The output points always have 4 components (known
   // as homogenous coordinates). The output array can simply point to
   // the input array to do the transform in-place.
   // 
   // Here's an example with differing input/output strides:
   // |[
   // typedef struct {
   // float x,y;
   // guint8 r,g,b,a;
   // float s,t,p;
   // } MyInVertex;
   // typedef struct {
   // guint8 r,g,b,a;
   // float x,y,z;
   // } MyOutVertex;
   // MyInVertex vertices[N_VERTICES];
   // MyOutVertex results[N_VERTICES];
   // CoglMatrix matrix;
   // 
   // my_load_vertices (vertices);
   // my_get_matrix (&matrix);
   // 
   // cogl_matrix_project_points (&matrix,
   // 2,
   // sizeof (MyInVertex),
   // &vertices[0].x,
   // sizeof (MyOutVertex),
   // &results[0].x,
   // N_VERTICES);
   // ]|
   // <n_components>: The number of position components for each input point. (either 2, 3 or 4)
   // <stride_in>: The stride in bytes between input points.
   // <points_in>: A pointer to the first component of the first input point.
   // <stride_out>: The stride in bytes between output points.
   // <points_out>: A pointer to the first component of the first output point.
   // <n_points>: The number of points to transform.
   void project_points(AT0, AT1)(int n_components, size_t stride_in, AT0 /*void*/ points_in, size_t stride_out, AT1 /*void*/ points_out, int n_points) nothrow {
      cogl_matrix_project_points(&this, n_components, stride_in, UpCast!(void*)(points_in), stride_out, UpCast!(void*)(points_out), n_points);
   }

   // Multiplies @matrix with a rotation matrix that applies a rotation
   // of @angle degrees around the specified 3D vector.
   // <angle>: The angle you want to rotate in degrees
   // <x>: X component of your rotation vector
   // <y>: Y component of your rotation vector
   // <z>: Z component of your rotation vector
   void rotate()(float angle, float x, float y, float z) nothrow {
      cogl_matrix_rotate(&this, angle, x, y, z);
   }

   // Multiplies @matrix with a transform matrix that scales along the X,
   // Y and Z axis.
   // <sx>: The X scale factor
   // <sy>: The Y scale factor
   // <sz>: The Z scale factor
   void scale()(float sx, float sy, float sz) nothrow {
      cogl_matrix_scale(&this, sx, sy, sz);
   }

   // Transforms a point whos position is given and returned as four float
   // components.
   // <x>: The X component of your points position
   // <y>: The Y component of your points position
   // <z>: The Z component of your points position
   // <w>: The W component of your points position
   void transform_point(AT0, AT1, AT2, AT3)(/*inout*/ AT0 /*float*/ x, /*inout*/ AT1 /*float*/ y, /*inout*/ AT2 /*float*/ z, /*inout*/ AT3 /*float*/ w) nothrow {
      cogl_matrix_transform_point(&this, UpCast!(float*)(x), UpCast!(float*)(y), UpCast!(float*)(z), UpCast!(float*)(w));
   }

   // Transforms an array of input points and writes the result to
   // another array of output points. The input points can either have 2
   // or 3 components each. The output points always have 3 components.
   // The output array can simply point to the input array to do the
   // transform in-place.
   // 
   // If you need to transform 4 component points see
   // cogl_matrix_project_points().
   // 
   // Here's an example with differing input/output strides:
   // |[
   // typedef struct {
   // float x,y;
   // guint8 r,g,b,a;
   // float s,t,p;
   // } MyInVertex;
   // typedef struct {
   // guint8 r,g,b,a;
   // float x,y,z;
   // } MyOutVertex;
   // MyInVertex vertices[N_VERTICES];
   // MyOutVertex results[N_VERTICES];
   // CoglMatrix matrix;
   // 
   // my_load_vertices (vertices);
   // my_get_matrix (&matrix);
   // 
   // cogl_matrix_transform_points (&matrix,
   // 2,
   // sizeof (MyInVertex),
   // &vertices[0].x,
   // sizeof (MyOutVertex),
   // &results[0].x,
   // N_VERTICES);
   // ]|
   // <n_components>: The number of position components for each input point. (either 2 or 3)
   // <stride_in>: The stride in bytes between input points.
   // <points_in>: A pointer to the first component of the first input point.
   // <stride_out>: The stride in bytes between output points.
   // <points_out>: A pointer to the first component of the first output point.
   // <n_points>: The number of points to transform.
   void transform_points(AT0, AT1)(int n_components, size_t stride_in, AT0 /*void*/ points_in, size_t stride_out, AT1 /*void*/ points_out, int n_points) nothrow {
      cogl_matrix_transform_points(&this, n_components, stride_in, UpCast!(void*)(points_in), stride_out, UpCast!(void*)(points_out), n_points);
   }

   // Multiplies @matrix with a transform matrix that translates along
   // the X, Y and Z axis.
   // <x>: The X translation you want to apply
   // <y>: The Y translation you want to apply
   // <z>: The Z translation you want to apply
   void translate()(float x, float y, float z) nothrow {
      cogl_matrix_translate(&this, x, y, z);
   }

   // VERSION: 1.10
   // Replaces @matrix with its transpose. Ie, every element (i,j) in the
   // new matrix is taken from element (j,i) in the old matrix.
   void transpose()() nothrow {
      cogl_matrix_transpose(&this);
   }

   // VERSION: 1.8
   // Multiplies @matrix by a view transform that maps the 2D coordinates
   // (0,0) top left and (@width_2d,@height_2d) bottom right the full viewport
   // size. Geometry at a depth of 0 will now lie on this 2D plane.
   // 
   // Note: this doesn't multiply the matrix by any projection matrix,
   // but it assumes you have a perspective projection as defined by
   // passing the corresponding arguments to cogl_matrix_frustum().
   // Toolkits such as Clutter that mix 2D and 3D drawing can use this to
   // create a 2D coordinate system within a 3D perspective projected
   // view frustum.
   // <left>: coord of left vertical clipping plane
   // <right>: coord of right vertical clipping plane
   // <bottom>: coord of bottom horizontal clipping plane
   // <top>: coord of top horizontal clipping plane
   // <z_near>: The distance to the near clip plane. Never pass 0 and always pass a positive number.
   // <z_2d>: The distance to the 2D plane. (Should always be positive and be between @z_near and the z_far value that was passed to cogl_matrix_frustum())
   // <width_2d>: The width of the 2D coordinate system
   // <height_2d>: The height of the 2D coordinate system
   void view_2d_in_frustum()(float left, float right, float bottom, float top, float z_near, float z_2d, float width_2d, float height_2d) nothrow {
      cogl_matrix_view_2d_in_frustum(&this, left, right, bottom, top, z_near, z_2d, width_2d, height_2d);
   }

   // VERSION: 1.8
   // Multiplies @matrix by a view transform that maps the 2D coordinates
   // (0,0) top left and (@width_2d,@height_2d) bottom right the full viewport
   // size. Geometry at a depth of 0 will now lie on this 2D plane.
   // 
   // Note: this doesn't multiply the matrix by any projection matrix,
   // but it assumes you have a perspective projection as defined by
   // passing the corresponding arguments to cogl_matrix_perspective().
   // 
   // Toolkits such as Clutter that mix 2D and 3D drawing can use this to
   // create a 2D coordinate system within a 3D perspective projected
   // view frustum.
   // <fov_y>: A field of view angle for the Y axis
   // <aspect>: The ratio of width to height determining the field of view angle for the x axis.
   // <z_near>: The distance to the near clip plane. Never pass 0 and always pass a positive number.
   // <z_2d>: The distance to the 2D plane. (Should always be positive and be between @z_near and the z_far value that was passed to cogl_matrix_frustum())
   // <width_2d>: The width of the 2D coordinate system
   // <height_2d>: The height of the 2D coordinate system
   void view_2d_in_perspective()(float fov_y, float aspect, float z_near, float z_2d, float width_2d, float height_2d) nothrow {
      cogl_matrix_view_2d_in_perspective(&this, fov_y, aspect, z_near, z_2d, width_2d, height_2d);
   }

   // VERSION: 1.4
   // Compares two matrices to see if they represent the same
   // transformation. Although internally the matrices may have different
   // annotations associated with them and may potentially have a cached
   // inverse matrix these are not considered in the comparison.
   // <v1>: A 4x4 transformation matrix
   // <v2>: A 4x4 transformation matrix
   static int equal(AT0, AT1)(AT0 /*const(void)*/ v1, AT1 /*const(void)*/ v2) nothrow {
      return cogl_matrix_equal(UpCast!(const(void)*)(v1), UpCast!(const(void)*)(v2));
   }
}

struct MetaTexture {

   // Unintrospectable method: foreach_in_region() / cogl_meta_texture_foreach_in_region()
   // VERSION: 1.10
   // Allows you to manually iterate the low-level textures that define a
   // given region of a high-level #CoglMetaTexture.
   // 
   // For example cogl_texture_2d_sliced_new_with_size() can be used to
   // create a meta texture that may slice a large image into multiple,
   // smaller power-of-two sized textures. These high level textures are
   // not directly understood by a GPU and so this API must be used to
   // manually resolve the underlying textures for drawing.
   // 
   // All high level textures (#CoglAtlasTexture, #CoglSubTexture,
   // #CoglTexturePixmapX11, and #CoglTexture2DSliced) can be handled
   // consistently using this interface which greately simplifies
   // implementing primitives that support all texture types.
   // 
   // For example if you use the cogl_rectangle() API then Cogl will
   // internally use this API to resolve the low level textures of any
   // meta textures you have associated with CoglPipeline layers.
   // 
   // <note>The low level drawing APIs such as cogl_draw_attributes()
   // don't understand the #CoglMetaTexture interface and so it is your
   // responsibility to use this API to resolve all CoglPipeline
   // textures into low-level textures before drawing.</note>
   // 
   // For each low-level texture that makes up part of the given region
   // of the @meta_texture, @callback is called specifying how the
   // low-level texture maps to the original region.
   // <tx_1>: The top-left x coordinate of the region to iterate
   // <ty_1>: The top-left y coordinate of the region to iterate
   // <tx_2>: The bottom-right x coordinate of the region to iterate
   // <ty_2>: The bottom-right y coordinate of the region to iterate
   // <callback>: A #CoglMetaTextureCallback pointer to be called for each low-level texture within the specified region.
   // <user_data>: A private pointer that is passed to @callback.
   void foreach_in_region(AT0)(float tx_1, float ty_1, float tx_2, float ty_2, PipelineWrapMode wrap_s, PipelineWrapMode wrap_t, MetaTextureCallback callback, AT0 /*void*/ user_data) nothrow {
      cogl_meta_texture_foreach_in_region(&this, tx_1, ty_1, tx_2, ty_2, wrap_s, wrap_t, callback, UpCast!(void*)(user_data));
   }
}


// VERSION: 1.10
// A callback used with cogl_meta_texture_foreach_in_region() to
// retrieve details of all the low-level #CoglTexture<!-- -->s that
// make up a given #CoglMetaTexture.
// <sub_texture>: A low-level #CoglTexture making up part of a #CoglMetaTexture.
// <sub_texture_coords>: A float 4-tuple ordered like (tx1,ty1,tx2,ty2) defining what region of the current @sub_texture maps to a sub-region of a #CoglMetaTexture. (tx1,ty1) is the top-left sub-region coordinate and (tx2,ty2) is the bottom-right. These are low-level texture coordinates.
// <meta_coords>: A float 4-tuple ordered like (tx1,ty1,tx2,ty2) defining what sub-region of a #CoglMetaTexture this low-level @sub_texture maps too. (tx1,ty1) is the top-left sub-region coordinate and (tx2,ty2) is the bottom-right. These are high-level meta-texture coordinates.
// <user_data>: A private pointer passed to cogl_meta_texture_foreach_in_region().
extern (C) alias void function (Texture* sub_texture, float* sub_texture_coords, float* meta_coords, void* user_data) nothrow MetaTextureCallback;

struct Object {

   // Unintrospectable method: get_user_data() / cogl_object_get_user_data()
   // VERSION: 1.4
   // Finds the user data previously associated with @object using
   // the given @key. If no user data has been associated with @object
   // for the given @key this function returns NULL.
   // 
   // with @object using the given @key; or %NULL if no associated
   // data is found.
   // RETURNS: The user data previously associated
   // <key>: The address of a #CoglUserDataKey which provides a unique value with which to index the private data.
   void* get_user_data(AT0)(AT0 /*UserDataKey*/ key) nothrow {
      return cogl_object_get_user_data(&this, UpCast!(UserDataKey*)(key));
   }

   // Unintrospectable method: set_user_data() / cogl_object_set_user_data()
   // VERSION: 1.4
   // Associates some private @user_data with a given #CoglObject. To
   // later remove the association call cogl_object_set_user_data() with
   // the same @key but NULL for the @user_data.
   // <key>: The address of a #CoglUserDataKey which provides a unique value with which to index the private data.
   // <user_data>: The data to associate with the given object, or %NULL to remove a previous association.
   // <destroy>: A #CoglUserDataDestroyCallback to call if the object is destroyed or if the association is removed by later setting %NULL data for the same key.
   void set_user_data(AT0, AT1)(AT0 /*UserDataKey*/ key, AT1 /*void*/ user_data, UserDataDestroyCallback destroy) nothrow {
      cogl_object_set_user_data(&this, UpCast!(UserDataKey*)(key), UpCast!(void*)(user_data), destroy);
   }

   // Unintrospectable function: ref() / cogl_object_ref()
   // Increases the reference count of @handle by 1
   // RETURNS: the @object, with its reference count increased
   // <object>: a #CoglObject
   static void* ref_(AT0)(AT0 /*void*/ object) nothrow {
      return cogl_object_ref(UpCast!(void*)(object));
   }

   // Unintrospectable function: unref() / cogl_object_unref()
   // Drecreases the reference count of @object by 1; if the reference
   // count reaches 0, the resources allocated by @object will be freed
   // <object>: a #CoglObject
   static void unref(AT0)(AT0 /*void*/ object) nothrow {
      cogl_object_unref(UpCast!(void*)(object));
   }
}

struct Onscreen {

   // VERSION: 2.0
   // This requests to make @onscreen invisible to the user.
   // 
   // Actually the precise semantics of this function depend on the
   // window system currently in use, and if you don't have a
   // multi-windowining system this function may in-fact do nothing.
   // 
   // This function does not implicitly allocate the given @onscreen
   // framebuffer before hiding it.
   // 
   // <note>Since Cogl doesn't explicitly track the visibility status of
   // onscreen framebuffers it wont try to avoid redundant window system
   // requests e.g. to show an already visible window. This also means
   // that it's acceptable to alternatively use native APIs to show and
   // hide windows without confusing Cogl.</note>
   void hide()() nothrow {
      cogl_onscreen_hide(&this);
   }

   // VERSION: 1.8
   // Requests that the given @onscreen framebuffer should have swap buffer
   // requests (made using cogl_framebuffer_swap_buffers()) throttled either by a
   // displays vblank period or perhaps some other mechanism in a composited
   // environment.
   // <throttled>: Whether swap throttling is wanted or not.
   void set_swap_throttled()(int throttled) nothrow {
      cogl_onscreen_set_swap_throttled(&this, throttled);
   }

   // VERSION: 2.0
   // This requests to make @onscreen visible to the user.
   // 
   // Actually the precise semantics of this function depend on the
   // window system currently in use, and if you don't have a
   // multi-windowining system this function may in-fact do nothing.
   // 
   // This function will implicitly allocate the given @onscreen
   // framebuffer before showing it if it hasn't already been allocated.
   // 
   // <note>Since Cogl doesn't explicitly track the visibility status of
   // onscreen framebuffers it wont try to avoid redundant window system
   // requests e.g. to show an already visible window. This also means
   // that it's acceptable to alternatively use native APIs to show and
   // hide windows without confusing Cogl.</note>
   void show()() nothrow {
      cogl_onscreen_show(&this);
   }
   static void clutter_backend_set_size_CLUTTER()(int width, int height) nothrow {
      cogl_onscreen_clutter_backend_set_size_CLUTTER(width, height);
   }

   // Unintrospectable function: new() / cogl_onscreen_new()
   // VERSION: 1.8
   // Instantiates an "unallocated" #CoglOnscreen framebuffer that may be
   // configured before later being allocated, either implicitly when
   // it is first used or explicitly via cogl_framebuffer_allocate().
   // RETURNS: A newly instantiated #CoglOnscreen framebuffer
   // <context>: A #CoglContext
   // <width>: The desired framebuffer width
   // <height>: The desired framebuffer height
   static Onscreen* new_(AT0)(AT0 /*Cogl.Context*/ context, int width, int height) nothrow {
      return cogl_onscreen_new(UpCast!(Cogl.Context*)(context), width, height);
   }
}

struct OnscreenTemplate {

   // VERSION: 1.10
   // Requires that any future CoglOnscreen framebuffers derived from
   // this template must support making at least @n samples per pixel
   // which will all contribute to the final resolved color for that
   // pixel.
   // 
   // By default this value is usually set to 0 and that is referred to
   // as "single-sample" rendering. A value of 1 or greater is referred
   // to as "multisample" rendering.
   // 
   // <note>There are some semantic differences between single-sample
   // rendering and multisampling with just 1 point sample such as it
   // being redundant to use the cogl_framebuffer_resolve_samples() and
   // cogl_framebuffer_resolve_samples_region() apis with single-sample
   // rendering.</note>
   // <n>: The minimum number of samples per pixel
   void set_samples_per_pixel()(int n) nothrow {
      cogl_onscreen_template_set_samples_per_pixel(&this, n);
   }

   // VERSION: 1.10
   // Requests that any future #CoglOnscreen framebuffers derived from this
   // template should enable or disable swap throttling according to the given
   // @throttled argument.
   // <throttled>: Whether throttling should be enabled
   void set_swap_throttled()(int throttled) nothrow {
      cogl_onscreen_template_set_swap_throttled(&this, throttled);
   }
   // Unintrospectable function: new_EXP() / cogl_onscreen_template_new_EXP()
   static OnscreenTemplate* new_EXP(AT0)(AT0 /*SwapChain*/ swap_chain) nothrow {
      return cogl_onscreen_template_new_EXP(UpCast!(SwapChain*)(swap_chain));
   }
}

extern (C) alias void function (Onscreen* onscreen, uint event_mask, void* user_data) nothrow OnscreenX11MaskCallback;

enum int PIXEL_FORMAT_24 = 2;
enum int PIXEL_FORMAT_32 = 3;
enum int PREMULT_BIT = 128;
struct Path {

   // Unintrospectable method: copy() / cogl_path_copy()
   // VERSION: 2.0
   // Returns a new copy of the path in @path. The new path has a
   // reference count of 1 so you should unref it with
   // cogl_object_unref() if you no longer need it.
   // 
   // Internally the path will share the data until one of the paths is
   // modified so copying paths should be relatively cheap.
   // RETURNS: a copy of the path in @path.
   Path* copy()() nothrow {
      return cogl_path_copy(&this);
   }

   // VERSION: 2.0
   // Adds an elliptical arc segment to the current path. A straight line
   // segment will link the current pen location with the first vertex
   // of the arc. If you perform a move_to to the arcs start just before
   // drawing it you create a free standing arc.
   // 
   // The angles are measured in degrees where 0° is in the direction of
   // the positive X axis and 90° is in the direction of the positive Y
   // axis. The angle of the arc begins at @angle_1 and heads towards
   // @angle_2 (so if @angle_2 is less than @angle_1 it will decrease,
   // otherwise it will increase).
   // <center_x>: X coordinate of the elliptical arc center
   // <center_y>: Y coordinate of the elliptical arc center
   // <radius_x>: X radius of the elliptical arc
   // <radius_y>: Y radius of the elliptical arc
   // <angle_1>: Angle in degrees at which the arc begin
   // <angle_2>: Angle in degrees at which the arc ends
   static void arc()(float center_x, float center_y, float radius_x, float radius_y, float angle_1, float angle_2) nothrow {
      cogl_path_arc(center_x, center_y, radius_x, radius_y, angle_1, angle_2);
   }

   // VERSION: 2.0
   // Closes the path being constructed by adding a straight line segment
   // to it that ends at the first vertex of the path.
   static void close()() nothrow {
      cogl_path_close();
   }

   // VERSION: 2.0
   // Adds a cubic bezier curve segment to the current path with the given
   // second, third and fourth control points and using current pen location
   // as the first control point.
   // <x_1>: X coordinate of the second bezier control point
   // <y_1>: Y coordinate of the second bezier control point
   // <x_2>: X coordinate of the third bezier control point
   // <y_2>: Y coordinate of the third bezier control point
   // <x_3>: X coordinate of the fourth bezier control point
   // <y_3>: Y coordinate of the fourth bezier control point
   static void curve_to()(float x_1, float y_1, float x_2, float y_2, float x_3, float y_3) nothrow {
      cogl_path_curve_to(x_1, y_1, x_2, y_2, x_3, y_3);
   }

   // VERSION: 2.0
   // Constructs an ellipse shape. If there is an existing path this will
   // start a new disjoint sub-path.
   // <center_x>: X coordinate of the ellipse center
   // <center_y>: Y coordinate of the ellipse center
   // <radius_x>: X radius of the ellipse
   // <radius_y>: Y radius of the ellipse
   static void ellipse()(float center_x, float center_y, float radius_x, float radius_y) nothrow {
      cogl_path_ellipse(center_x, center_y, radius_x, radius_y);
   }

   // VERSION: 2.0
   // Fills the interior of the constructed shape using the current
   // drawing color.
   // 
   // The interior of the shape is determined using the fill rule of the
   // path. See %CoglPathFillRule for details.
   // 
   // <note>The result of referencing sliced textures in your current
   // pipeline when filling a path are undefined. You should pass
   // the %COGL_TEXTURE_NO_SLICING flag when loading any texture you will
   // use while filling a path.</note>
   static void fill()() nothrow {
      cogl_path_fill();
   }

   // VERSION: 1.0
   // Fills the interior of the constructed shape using the current
   // drawing color and preserves the path to be used again. See
   // cogl_path_fill() for a description what is considered the interior
   // of the shape.
   static void fill_preserve()() nothrow {
      cogl_path_fill_preserve();
   }

   // VERSION: 2.0
   // Retrieves the fill rule set using cogl_path_set_fill_rule().
   // RETURNS: the fill rule that is used for the current path.
   static PathFillRule get_fill_rule()() nothrow {
      return cogl_path_get_fill_rule();
   }

   // VERSION: 2.0
   // Constructs a straight line shape starting and ending at the given
   // coordinates. If there is an existing path this will start a new
   // disjoint sub-path.
   // <x_1>: X coordinate of the start line vertex
   // <y_1>: Y coordinate of the start line vertex
   // <x_2>: X coordinate of the end line vertex
   // <y_2>: Y coordinate of the end line vertex
   static void line()(float x_1, float y_1, float x_2, float y_2) nothrow {
      cogl_path_line(x_1, y_1, x_2, y_2);
   }

   // VERSION: 2.0
   // Adds a straight line segment to the current path that ends at the
   // given coordinates.
   // <x>: X coordinate of the end line vertex
   // <y>: Y coordinate of the end line vertex
   static void line_to()(float x, float y) nothrow {
      cogl_path_line_to(x, y);
   }

   // VERSION: 2.0
   // Moves the pen to the given location. If there is an existing path
   // this will start a new disjoint subpath.
   // <x>: X coordinate of the pen location to move to.
   // <y>: Y coordinate of the pen location to move to.
   static void move_to()(float x, float y) nothrow {
      cogl_path_move_to(x, y);
   }

   // VERSION: 2.0
   // Creates a new, empty path object. The default fill rule is
   // %COGL_PATH_FILL_RULE_EVEN_ODD.
   // 
   // be freed using cogl_object_unref().
   // RETURNS: A pointer to a newly allocated #CoglPath, which can
   static void new_()() nothrow {
      cogl_path_new();
   }

   // VERSION: 2.0
   // Constructs a polygonal shape of the given number of vertices. If
   // there is an existing path this will start a new disjoint sub-path.
   // 
   // The coords array must contain 2 * num_points values. The first value
   // represents the X coordinate of the first vertex, the second value
   // represents the Y coordinate of the first vertex, continuing in the same
   // fashion for the rest of the vertices.
   // <coords>: A pointer to the first element of an array of fixed-point values that specify the vertex coordinates.
   // <num_points>: The total number of vertices.
   static void polygon(AT0)(AT0 /*float*/ coords, int num_points) nothrow {
      cogl_path_polygon(UpCast!(float*)(coords), num_points);
   }

   // VERSION: 2.0
   // Constructs a series of straight line segments, starting from the
   // first given vertex coordinate. If there is an existing path this
   // will start a new disjoint sub-path. Each subsequent segment starts
   // where the previous one ended and ends at the next given vertex
   // coordinate.
   // 
   // The coords array must contain 2 * num_points values. The first value
   // represents the X coordinate of the first vertex, the second value
   // represents the Y coordinate of the first vertex, continuing in the same
   // fashion for the rest of the vertices. (num_points - 1) segments will
   // be constructed.
   // <coords>: A pointer to the first element of an array of fixed-point values that specify the vertex coordinates.
   // <num_points>: The total number of vertices.
   static void polyline(AT0)(AT0 /*float*/ coords, int num_points) nothrow {
      cogl_path_polyline(UpCast!(float*)(coords), num_points);
   }

   // VERSION: 2.0
   // Constructs a rectangular shape at the given coordinates. If there
   // is an existing path this will start a new disjoint sub-path.
   // <x_1>: X coordinate of the top-left corner.
   // <y_1>: Y coordinate of the top-left corner.
   // <x_2>: X coordinate of the bottom-right corner.
   // <y_2>: Y coordinate of the bottom-right corner.
   static void rectangle()(float x_1, float y_1, float x_2, float y_2) nothrow {
      cogl_path_rectangle(x_1, y_1, x_2, y_2);
   }

   // VERSION: 2.0
   // Adds a cubic bezier curve segment to the current path with the given
   // second, third and fourth control points and using current pen location
   // as the first control point. The given coordinates are relative to the
   // current pen location.
   // <x_1>: X coordinate of the second bezier control point
   // <y_1>: Y coordinate of the second bezier control point
   // <x_2>: X coordinate of the third bezier control point
   // <y_2>: Y coordinate of the third bezier control point
   // <x_3>: X coordinate of the fourth bezier control point
   // <y_3>: Y coordinate of the fourth bezier control point
   static void rel_curve_to()(float x_1, float y_1, float x_2, float y_2, float x_3, float y_3) nothrow {
      cogl_path_rel_curve_to(x_1, y_1, x_2, y_2, x_3, y_3);
   }

   // VERSION: 2.0
   // Adds a straight line segment to the current path that ends at the
   // given coordinates relative to the current pen location.
   // <x>: X offset from the current pen location of the end line vertex
   // <y>: Y offset from the current pen location of the end line vertex
   static void rel_line_to()(float x, float y) nothrow {
      cogl_path_rel_line_to(x, y);
   }

   // VERSION: 2.0
   // Moves the pen to the given offset relative to the current pen
   // location. If there is an existing path this will start a new
   // disjoint subpath.
   // <x>: X offset from the current pen location to move the pen to.
   // <y>: Y offset from the current pen location to move the pen to.
   static void rel_move_to()(float x, float y) nothrow {
      cogl_path_rel_move_to(x, y);
   }

   // VERSION: 2.0
   // Constructs a rectangular shape with rounded corners. If there is an
   // existing path this will start a new disjoint sub-path.
   // <x_1>: X coordinate of the top-left corner.
   // <y_1>: Y coordinate of the top-left corner.
   // <x_2>: X coordinate of the bottom-right corner.
   // <y_2>: Y coordinate of the bottom-right corner.
   // <radius>: Radius of the corner arcs.
   // <arc_step>: Angle increment resolution for subdivision of the corner arcs.
   static void round_rectangle()(float x_1, float y_1, float x_2, float y_2, float radius, float arc_step) nothrow {
      cogl_path_round_rectangle(x_1, y_1, x_2, y_2, radius, arc_step);
   }

   // VERSION: 2.0
   // Sets the fill rule of the current path to @fill_rule. This will
   // affect how the path is filled when cogl_path_fill() is later
   // called. Note that the fill rule state is attached to the path so
   // calling cogl_get_path() will preserve the fill rule and calling
   // cogl_path_new() will reset the fill rule back to the default.
   // <fill_rule>: The new fill rule.
   static void set_fill_rule()(PathFillRule fill_rule) nothrow {
      cogl_path_set_fill_rule(fill_rule);
   }

   // VERSION: 2.0
   // Strokes the constructed shape using the current drawing color and a
   // width of 1 pixel (regardless of the current transformation
   // matrix).
   static void stroke()() nothrow {
      cogl_path_stroke();
   }

   // VERSION: 1.0
   // Strokes the constructed shape using the current drawing color and
   // preserves the path to be used again.
   static void stroke_preserve()() nothrow {
      cogl_path_stroke_preserve();
   }
}


// #CoglPathFillRule is used to determine how a path is filled. There
// are two options - 'non-zero' and 'even-odd'. To work out whether any
// point will be filled imagine drawing an infinetely long line in any
// direction from that point. The number of times and the direction
// that the edges of the path crosses this line determines whether the
// line is filled as described below. Any open sub paths are treated
// as if there was an extra line joining the first point and the last
// point.
// 
// The default fill rule is %COGL_PATH_FILL_RULE_EVEN_ODD. The fill
// rule is attached to the current path so preserving a path with
// cogl_get_path() also preserves the fill rule. Calling
// cogl_path_new() resets the current fill rule to the default.
// 
// <figure id="fill-rule-non-zero">
// <title>Example of filling various paths using the non-zero rule</title>
// <graphic fileref="fill-rule-non-zero.png" format="PNG"/>
// </figure>
// 
// <figure id="fill-rule-even-odd">
// <title>Example of filling various paths using the even-odd rule</title>
// <graphic fileref="fill-rule-even-odd.png" format="PNG"/>
// </figure>
enum PathFillRule /* Version 1.4 */ {
   NON_ZERO = 0,
   EVEN_ODD = 1
}
struct Pipeline {

   // VERSION: 1.10
   // Adds a shader snippet that will hook on to the given layer of the
   // pipeline. The exact part of the pipeline that the snippet wraps
   // around depends on the hook that is given to
   // cogl_snippet_new(). Note that some hooks can't be used with a layer
   // and need to be added with cogl_pipeline_add_snippet() instead.
   // <layer>: The layer to hook the snippet to
   // <snippet>: A #CoglSnippet
   void add_layer_snippet(AT0)(int layer, AT0 /*Snippet*/ snippet) nothrow {
      cogl_pipeline_add_layer_snippet(&this, layer, UpCast!(Snippet*)(snippet));
   }

   // VERSION: 1.10
   // Adds a shader snippet to @pipeline. The snippet will wrap around or
   // replace some part of the pipeline as defined by the hook point in
   // @snippet. Note that some hook points are specific to a layer and
   // must be added with cogl_pipeline_add_layer_snippet() instead.
   // <snippet>: The #CoglSnippet to add to the vertex processing hook
   void add_snippet(AT0)(AT0 /*Snippet*/ snippet) nothrow {
      cogl_pipeline_add_snippet(&this, UpCast!(Snippet*)(snippet));
   }

   // Unintrospectable method: copy() / cogl_pipeline_copy()
   // VERSION: 2.0
   // Creates a new pipeline with the configuration copied from the
   // source pipeline.
   // 
   // We would strongly advise developers to always aim to use
   // cogl_pipeline_copy() instead of cogl_pipeline_new() whenever there will
   // be any similarity between two pipelines. Copying a pipeline helps Cogl
   // keep track of a pipelines ancestry which we may use to help minimize GPU
   // state changes.
   // RETURNS: a pointer to the newly allocated #CoglPipeline
   Pipeline* copy()() nothrow {
      return cogl_pipeline_copy(&this);
   }

   // Unintrospectable method: foreach_layer() / cogl_pipeline_foreach_layer()
   // VERSION: 2.0
   // Iterates all the layer indices of the given @pipeline.
   // <callback>: A #CoglPipelineLayerCallback to be called for each layer index
   // <user_data>: Private data that will be passed to the callback
   void foreach_layer(AT0)(PipelineLayerCallback callback, AT0 /*void*/ user_data) nothrow {
      cogl_pipeline_foreach_layer(&this, callback, UpCast!(void*)(user_data));
   }

   // VERSION: 2.0
   // 
   // RETURNS: The alpha test function of @pipeline.
   PipelineAlphaFunc get_alpha_test_function()() nothrow {
      return cogl_pipeline_get_alpha_test_function(&this);
   }

   // VERSION: 2.0
   // 
   // RETURNS: The alpha test reference value of @pipeline.
   float get_alpha_test_reference()() nothrow {
      return cogl_pipeline_get_alpha_test_reference(&this);
   }

   // VERSION: 2.0
   // Retrieves the current ambient color for @pipeline
   // <ambient>: The location to store the ambient color
   void get_ambient(AT0)(AT0 /*Color*/ ambient) nothrow {
      cogl_pipeline_get_ambient(&this, UpCast!(Color*)(ambient));
   }

   // VERSION: 2.0
   // Retrieves the current pipeline color.
   // <color>: The location to store the color
   void get_color(AT0)(/*out*/ AT0 /*Color*/ color) nothrow {
      cogl_pipeline_get_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 1.8
   // Gets the current #CoglColorMask of which channels would be written to the
   // current framebuffer. Each bit set in the mask means that the
   // corresponding color would be written.
   // RETURNS: A #CoglColorMask
   ColorMask get_color_mask()() nothrow {
      return cogl_pipeline_get_color_mask(&this);
   }

   // VERSION: 2.0
   // cogl_pipeline_set_cull_face_mode().
   // 
   // Status: Unstable
   // RETURNS: the cull face mode that was previously set with
   PipelineCullFaceMode get_cull_face_mode()() nothrow {
      return cogl_pipeline_get_cull_face_mode(&this);
   }

   // VERSION: 2.0
   // Retrieves the current depth state configuration for the given
   // @pipeline as previously set using cogl_pipeline_set_depth_state().
   void get_depth_state(AT0)(AT0 /*DepthState*/ state_out) nothrow {
      cogl_pipeline_get_depth_state(&this, UpCast!(DepthState*)(state_out));
   }

   // VERSION: 2.0
   // Retrieves the current diffuse color for @pipeline
   // <diffuse>: The location to store the diffuse color
   void get_diffuse(AT0)(AT0 /*Color*/ diffuse) nothrow {
      cogl_pipeline_get_diffuse(&this, UpCast!(Color*)(diffuse));
   }

   // VERSION: 2.0
   // Retrieves the pipelines current emission color.
   // <emission>: The location to store the emission color
   void get_emission(AT0)(AT0 /*Color*/ emission) nothrow {
      cogl_pipeline_get_emission(&this, UpCast!(Color*)(emission));
   }
   Winding get_front_face_winding()() nothrow {
      return cogl_pipeline_get_front_face_winding(&this);
   }

   // VERSION: 2.0
   // Gets whether point sprite coordinate generation is enabled for this
   // texture layer.
   // 
   // point sprite coordinates.
   // RETURNS: whether the texture coordinates will be replaced with
   // <layer_index>: the layer number to check.
   int get_layer_point_sprite_coords_enabled()(int layer_index) nothrow {
      return cogl_pipeline_get_layer_point_sprite_coords_enabled(&this, layer_index);
   }

   // VERSION: 1.6
   // Returns the wrap mode for the 'p' coordinate of texture lookups on this
   // layer.
   // 
   // this layer.
   // RETURNS: the wrap mode for the 'p' coordinate of texture lookups on
   // <layer_index>: the layer number to change.
   PipelineWrapMode get_layer_wrap_mode_p()(int layer_index) nothrow {
      return cogl_pipeline_get_layer_wrap_mode_p(&this, layer_index);
   }

   // VERSION: 1.6
   // Returns the wrap mode for the 's' coordinate of texture lookups on this
   // layer.
   // 
   // this layer.
   // RETURNS: the wrap mode for the 's' coordinate of texture lookups on
   // <layer_index>: the layer number to change.
   PipelineWrapMode get_layer_wrap_mode_s()(int layer_index) nothrow {
      return cogl_pipeline_get_layer_wrap_mode_s(&this, layer_index);
   }

   // VERSION: 1.6
   // Returns the wrap mode for the 't' coordinate of texture lookups on this
   // layer.
   // 
   // this layer.
   // RETURNS: the wrap mode for the 't' coordinate of texture lookups on
   // <layer_index>: the layer number to change.
   PipelineWrapMode get_layer_wrap_mode_t()(int layer_index) nothrow {
      return cogl_pipeline_get_layer_wrap_mode_t(&this, layer_index);
   }

   // VERSION: 2.0
   // Retrieves the number of layers defined for the given @pipeline
   // RETURNS: the number of layers
   int get_n_layers()() nothrow {
      return cogl_pipeline_get_n_layers(&this);
   }

   // VERSION: 2.0
   // Get the size of points drawn when %COGL_VERTICES_MODE_POINTS is
   // used with the vertex buffer API.
   // RETURNS: the point size of the @pipeline.
   float get_point_size()() nothrow {
      return cogl_pipeline_get_point_size(&this);
   }

   // VERSION: 2.0
   // Retrieves the pipelines current emission color.
   // RETURNS: The pipelines current shininess value
   float get_shininess()() nothrow {
      return cogl_pipeline_get_shininess(&this);
   }

   // VERSION: 2.0
   // Retrieves the pipelines current specular color.
   // <specular>: The location to store the specular color
   void get_specular(AT0)(AT0 /*Color*/ specular) nothrow {
      cogl_pipeline_get_specular(&this, UpCast!(Color*)(specular));
   }

   // VERSION: 2.0
   // This is used to get an integer representing the uniform with the
   // name @uniform_name. The integer can be passed to functions such as
   // cogl_pipeline_set_uniform_1f() to set the value of a uniform.
   // 
   // This function will always return a valid integer. Ie, unlike
   // OpenGL, it does not return -1 if the uniform is not available in
   // this pipeline so it can not be used to test whether uniforms are
   // present. It is not necessary to set the program on the pipeline
   // before calling this function.
   // RETURNS: A integer representing the location of the given uniform.
   // <uniform_name>: The name of a uniform
   int get_uniform_location(AT0)(AT0 /*char*/ uniform_name) nothrow {
      return cogl_pipeline_get_uniform_location(&this, toCString!(char*)(uniform_name));
   }

   // Unintrospectable method: get_user_program() / cogl_pipeline_get_user_program()
   // VERSION: 2.0
   // Queries what user program has been associated with the given
   // @pipeline using cogl_pipeline_set_user_program().
   // RETURNS: The current user program or %COGL_INVALID_HANDLE.
   Handle get_user_program()() nothrow {
      return cogl_pipeline_get_user_program(&this);
   }

   // VERSION: 1.10
   // This function removes a layer from your pipeline
   // <layer_index>: Specifies the layer you want to remove
   void remove_layer()(int layer_index) nothrow {
      cogl_pipeline_remove_layer(&this, layer_index);
   }

   // VERSION: 2.0
   // Before a primitive is blended with the framebuffer, it goes through an
   // alpha test stage which lets you discard fragments based on the current
   // alpha value. This function lets you change the function used to evaluate
   // the alpha channel, and thus determine which fragments are discarded
   // and which continue on to the blending stage.
   // 
   // The default is %COGL_PIPELINE_ALPHA_FUNC_ALWAYS
   // <alpha_func>: A @CoglPipelineAlphaFunc constant
   // <alpha_reference>: A reference point that the chosen alpha function uses to compare incoming fragments to.
   void set_alpha_test_function()(PipelineAlphaFunc alpha_func, float alpha_reference) nothrow {
      cogl_pipeline_set_alpha_test_function(&this, alpha_func, alpha_reference);
   }

   // VERSION: 2.0
   // Sets the pipeline's ambient color, in the standard OpenGL lighting
   // model. The ambient color affects the overall color of the object.
   // 
   // Since the diffuse color will be intense when the light hits the surface
   // directly, the ambient will be most apparent where the light hits at a
   // slant.
   // 
   // The default value is (0.2, 0.2, 0.2, 1.0)
   // <ambient>: The components of the desired ambient color
   void set_ambient(AT0)(AT0 /*Color*/ ambient) nothrow {
      cogl_pipeline_set_ambient(&this, UpCast!(Color*)(ambient));
   }

   // VERSION: 2.0
   // Conveniently sets the diffuse and ambient color of @pipeline at the same
   // time. See cogl_pipeline_set_ambient() and cogl_pipeline_set_diffuse().
   // 
   // The default ambient color is (0.2, 0.2, 0.2, 1.0)
   // 
   // The default diffuse color is (0.8, 0.8, 0.8, 1.0)
   // <color>: The components of the desired ambient and diffuse colors
   void set_ambient_and_diffuse(AT0)(AT0 /*Color*/ color) nothrow {
      cogl_pipeline_set_ambient_and_diffuse(&this, UpCast!(Color*)(color));
   }

   // VERSION: 2.0
   // If not already familiar; please refer <link linkend="cogl-Blend-Strings">here</link>
   // for an overview of what blend strings are, and their syntax.
   // 
   // Blending occurs after the alpha test function, and combines fragments with
   // the framebuffer.
   // Currently the only blend function Cogl exposes is ADD(). So any valid
   // blend statements will be of the form:
   // 
   // |[
   // &lt;channel-mask&gt;=ADD(SRC_COLOR*(&lt;factor&gt;), DST_COLOR*(&lt;factor&gt;))
   // ]|
   // 
   // This is the list of source-names usable as blend factors:
   // <itemizedlist>
   // <listitem><para>SRC_COLOR: The color of the in comming fragment</para></listitem>
   // <listitem><para>DST_COLOR: The color of the framebuffer</para></listitem>
   // <listitem><para>CONSTANT: The constant set via cogl_pipeline_set_blend_constant()</para></listitem>
   // </itemizedlist>
   // 
   // The source names can be used according to the
   // <link linkend="cogl-Blend-String-syntax">color-source and factor syntax</link>,
   // so for example "(1-SRC_COLOR[A])" would be a valid factor, as would
   // "(CONSTANT[RGB])"
   // 
   // These can also be used as factors:
   // <itemizedlist>
   // <listitem>0: (0, 0, 0, 0)</listitem>
   // <listitem>1: (1, 1, 1, 1)</listitem>
   // <listitem>SRC_ALPHA_SATURATE_FACTOR: (f,f,f,1) where f = MIN(SRC_COLOR[A],1-DST_COLOR[A])</listitem>
   // </itemizedlist>
   // 
   // <note>Remember; all color components are normalized to the range [0, 1]
   // before computing the result of blending.</note>
   // 
   // <example id="cogl-Blend-Strings-blend-unpremul">
   // <title>Blend Strings/1</title>
   // <para>Blend a non-premultiplied source over a destination with
   // premultiplied alpha:</para>
   // <programlisting>
   // "RGB = ADD(SRC_COLOR*(SRC_COLOR[A]), DST_COLOR*(1-SRC_COLOR[A]))"
   // "A   = ADD(SRC_COLOR, DST_COLOR*(1-SRC_COLOR[A]))"
   // </programlisting>
   // </example>
   // 
   // <example id="cogl-Blend-Strings-blend-premul">
   // <title>Blend Strings/2</title>
   // <para>Blend a premultiplied source over a destination with
   // premultiplied alpha</para>
   // <programlisting>
   // "RGBA = ADD(SRC_COLOR, DST_COLOR*(1-SRC_COLOR[A]))"
   // </programlisting>
   // </example>
   // 
   // The default blend string is:
   // |[
   // RGBA = ADD (SRC_COLOR, DST_COLOR*(1-SRC_COLOR[A]))
   // ]|
   // 
   // That gives normal alpha-blending when the calculated color for the pipeline
   // is in premultiplied form.
   // 
   // described blending is supported by the underlying driver/hardware. If
   // there was an error, %FALSE is returned and @error is set accordingly (if
   // present).
   // RETURNS: %TRUE if the blend string was successfully parsed, and the
   // <blend_string>: A <link linkend="cogl-Blend-Strings">Cogl blend string</link> describing the desired blend function.
   int set_blend(AT0, AT1)(AT0 /*char*/ blend_string, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_pipeline_set_blend(&this, toCString!(char*)(blend_string), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 2.0
   // When blending is setup to reference a CONSTANT blend factor then
   // blending will depend on the constant set with this function.
   // <constant_color>: The constant color you want
   void set_blend_constant(AT0)(AT0 /*Color*/ constant_color) nothrow {
      cogl_pipeline_set_blend_constant(&this, UpCast!(Color*)(constant_color));
   }

   // VERSION: 2.0
   // Sets the basic color of the pipeline, used when no lighting is enabled.
   // 
   // Note that if you don't add any layers to the pipeline then the color
   // will be blended unmodified with the destination; the default blend
   // expects premultiplied colors: for example, use (0.5, 0.0, 0.0, 0.5) for
   // semi-transparent red. See cogl_color_premultiply().
   // 
   // The default value is (1.0, 1.0, 1.0, 1.0)
   // <color>: The components of the color
   void set_color(AT0)(AT0 /*Color*/ color) nothrow {
      cogl_pipeline_set_color(&this, UpCast!(Color*)(color));
   }

   // VERSION: 2.0
   // Sets the basic color of the pipeline, used when no lighting is enabled.
   // 
   // The default value is (1.0, 1.0, 1.0, 1.0)
   // <red>: The red component
   // <green>: The green component
   // <blue>: The blue component
   // <alpha>: The alpha component
   void set_color4f()(float red, float green, float blue, float alpha) nothrow {
      cogl_pipeline_set_color4f(&this, red, green, blue, alpha);
   }

   // VERSION: 2.0
   // Sets the basic color of the pipeline, used when no lighting is enabled.
   // 
   // The default value is (0xff, 0xff, 0xff, 0xff)
   // <red>: The red component
   // <green>: The green component
   // <blue>: The blue component
   // <alpha>: The alpha component
   void set_color4ub()(ubyte red, ubyte green, ubyte blue, ubyte alpha) nothrow {
      cogl_pipeline_set_color4ub(&this, red, green, blue, alpha);
   }

   // VERSION: 1.8
   // Defines a bit mask of which color channels should be written to the
   // current framebuffer. If a bit is set in @color_mask that means that
   // color will be written.
   // <color_mask>: A #CoglColorMask of which color channels to write to the current framebuffer.
   void set_color_mask()(ColorMask color_mask) nothrow {
      cogl_pipeline_set_color_mask(&this, color_mask);
   }

   // VERSION: 2.0
   // Sets which faces will be culled when drawing. Face culling can be
   // used to increase efficiency by avoiding drawing faces that would
   // get overridden. For example, if a model has gaps so that it is
   // impossible to see the inside then faces which are facing away from
   // the screen will never be seen so there is no point in drawing
   // them. This can be acheived by setting the cull face mode to
   // %COGL_PIPELINE_CULL_FACE_MODE_BACK.
   // 
   // Face culling relies on the primitives being drawn with a specific
   // order to represent which faces are facing inside and outside the
   // model. This order can be specified by calling
   // cogl_pipeline_set_front_face_winding().
   // 
   // Status: Unstable
   // <cull_face_mode>: The new mode to set
   void set_cull_face_mode()(PipelineCullFaceMode cull_face_mode) nothrow {
      cogl_pipeline_set_cull_face_mode(&this, cull_face_mode);
   }

   // VERSION: 2.0
   // This commits all the depth state configured in @state struct to the
   // given @pipeline. The configuration values are copied into the
   // pipeline so there is no requirement to keep the #CoglDepthState
   // struct around if you don't need it any more.
   // 
   // Note: Since some platforms do not support the depth range feature
   // it is possible for this function to fail and report an @error.
   // 
   // and returns an @error.
   // RETURNS: TRUE if the GPU supports all the given @state else %FALSE
   // <state>: A #CoglDepthState struct
   int set_depth_state(AT0, AT1)(AT0 /*DepthState*/ state, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_pipeline_set_depth_state(&this, UpCast!(DepthState*)(state), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 2.0
   // Sets the pipeline's diffuse color, in the standard OpenGL lighting
   // model. The diffuse color is most intense where the light hits the
   // surface directly - perpendicular to the surface.
   // 
   // The default value is (0.8, 0.8, 0.8, 1.0)
   // <diffuse>: The components of the desired diffuse color
   void set_diffuse(AT0)(AT0 /*Color*/ diffuse) nothrow {
      cogl_pipeline_set_diffuse(&this, UpCast!(Color*)(diffuse));
   }

   // VERSION: 2.0
   // Sets the pipeline's emissive color, in the standard OpenGL lighting
   // model. It will look like the surface is a light source emitting this
   // color.
   // 
   // The default value is (0.0, 0.0, 0.0, 1.0)
   // <emission>: The components of the desired emissive color
   void set_emission(AT0)(AT0 /*Color*/ emission) nothrow {
      cogl_pipeline_set_emission(&this, UpCast!(Color*)(emission));
   }

   // VERSION: 2.0
   // The order of the vertices within a primitive specifies whether it
   // is considered to be front or back facing. This function specifies
   // which order is considered to be the front
   // faces. %COGL_WINDING_COUNTER_CLOCKWISE sets the front faces to
   // primitives with vertices in a counter-clockwise order and
   // %COGL_WINDING_CLOCKWISE sets them to be clockwise. The default is
   // %COGL_WINDING_COUNTER_CLOCKWISE.
   // 
   // Status: Unstable
   void set_front_face_winding()(Winding front_winding) nothrow {
      cogl_pipeline_set_front_face_winding(&this, front_winding);
   }

   // VERSION: 2.0
   // If not already familiar; you can refer
   // <link linkend="cogl-Blend-Strings">here</link> for an overview of what blend
   // strings are and there syntax.
   // 
   // These are all the functions available for texture combining:
   // <itemizedlist>
   // <listitem>REPLACE(arg0) = arg0</listitem>
   // <listitem>MODULATE(arg0, arg1) = arg0 x arg1</listitem>
   // <listitem>ADD(arg0, arg1) = arg0 + arg1</listitem>
   // <listitem>ADD_SIGNED(arg0, arg1) = arg0 + arg1 - 0.5</listitem>
   // <listitem>INTERPOLATE(arg0, arg1, arg2) = arg0 x arg2 + arg1 x (1 - arg2)</listitem>
   // <listitem>SUBTRACT(arg0, arg1) = arg0 - arg1</listitem>
   // <listitem>
   // <programlisting>
   // DOT3_RGB(arg0, arg1) = 4 x ((arg0[R] - 0.5)) * (arg1[R] - 0.5) +
   // (arg0[G] - 0.5)) * (arg1[G] - 0.5) +
   // (arg0[B] - 0.5)) * (arg1[B] - 0.5))
   // </programlisting>
   // </listitem>
   // <listitem>
   // <programlisting>
   // DOT3_RGBA(arg0, arg1) = 4 x ((arg0[R] - 0.5)) * (arg1[R] - 0.5) +
   // (arg0[G] - 0.5)) * (arg1[G] - 0.5) +
   // (arg0[B] - 0.5)) * (arg1[B] - 0.5))
   // </programlisting>
   // </listitem>
   // </itemizedlist>
   // 
   // Refer to the
   // <link linkend="cogl-Blend-String-syntax">color-source syntax</link> for
   // describing the arguments. The valid source names for texture combining
   // are:
   // <variablelist>
   // <varlistentry>
   // <term>TEXTURE</term>
   // <listitem>Use the color from the current texture layer</listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>TEXTURE_0, TEXTURE_1, etc</term>
   // <listitem>Use the color from the specified texture layer</listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>CONSTANT</term>
   // <listitem>Use the color from the constant given with
   // cogl_pipeline_set_layer_constant()</listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>PRIMARY</term>
   // <listitem>Use the color of the pipeline as set with
   // cogl_pipeline_set_color()</listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>PREVIOUS</term>
   // <listitem>Either use the texture color from the previous layer, or
   // if this is layer 0, use the color of the pipeline as set with
   // cogl_pipeline_set_color()</listitem>
   // </varlistentry>
   // </variablelist>
   // 
   // <refsect2 id="cogl-Layer-Combine-Examples">
   // <title>Layer Combine Examples</title>
   // <para>This is effectively what the default blending is:</para>
   // <informalexample><programlisting>
   // RGBA = MODULATE (PREVIOUS, TEXTURE)
   // </programlisting></informalexample>
   // <para>This could be used to cross-fade between two images, using
   // the alpha component of a constant as the interpolator. The constant
   // color is given by calling cogl_pipeline_set_layer_constant.</para>
   // <informalexample><programlisting>
   // RGBA = INTERPOLATE (PREVIOUS, TEXTURE, CONSTANT[A])
   // </programlisting></informalexample>
   // </refsect2>
   // 
   // <note>You can't give a multiplication factor for arguments as you can
   // with blending.</note>
   // 
   // described texture combining is supported by the underlying driver and
   // or hardware. On failure, %FALSE is returned and @error is set
   // RETURNS: %TRUE if the blend string was successfully parsed, and the
   // <layer_index>: Specifies the layer you want define a combine function for
   // <blend_string>: A <link linkend="cogl-Blend-Strings">Cogl blend string</link> describing the desired texture combine function.
   int set_layer_combine(AT0, AT1)(int layer_index, AT0 /*char*/ blend_string, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_pipeline_set_layer_combine(&this, layer_index, toCString!(char*)(blend_string), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 2.0
   // When you are using the 'CONSTANT' color source in a layer combine
   // description then you can use this function to define its value.
   // <layer_index>: Specifies the layer you want to specify a constant used for texture combining
   // <constant>: The constant color you want
   void set_layer_combine_constant(AT0)(int layer_index, AT0 /*Color*/ constant) nothrow {
      cogl_pipeline_set_layer_combine_constant(&this, layer_index, UpCast!(Color*)(constant));
   }

   // VERSION: 1.10
   // Changes the decimation and interpolation filters used when a texture is
   // drawn at other scales than 100%.
   // <layer_index>: the layer number to change.
   // <min_filter>: the filter used when scaling a texture down.
   // <mag_filter>: the filter used when magnifying a texture.
   void set_layer_filters()(int layer_index, PipelineFilter min_filter, PipelineFilter mag_filter) nothrow {
      cogl_pipeline_set_layer_filters(&this, layer_index, min_filter, mag_filter);
   }

   // VERSION: 1.10
   // This function lets you set a matrix that can be used to e.g. translate
   // and rotate a single layer of a pipeline used to fill your geometry.
   // <layer_index>: the index for the layer inside @pipeline
   // <matrix>: the transformation matrix for the layer
   void set_layer_matrix(AT0)(int layer_index, AT0 /*Matrix*/ matrix) nothrow {
      cogl_pipeline_set_layer_matrix(&this, layer_index, UpCast!(Matrix*)(matrix));
   }

   // VERSION: 2.0
   // When rendering points, if @enable is %TRUE then the texture
   // coordinates for this layer will be replaced with coordinates that
   // vary from 0.0 to 1.0 across the primitive. The top left of the
   // point will have the coordinates 0.0,0.0 and the bottom right will
   // have 1.0,1.0. If @enable is %FALSE then the coordinates will be
   // fixed for the entire point.
   // 
   // This function will only work if %COGL_FEATURE_POINT_SPRITE is
   // available. If the feature is not available then the function will
   // return %FALSE and set @error.
   // RETURNS: %TRUE if the function succeeds, %FALSE otherwise.
   // <layer_index>: the layer number to change.
   // <enable>: whether to enable point sprite coord generation.
   int set_layer_point_sprite_coords_enabled(AT0)(int layer_index, int enable, AT0 /*GLib2.Error**/ error=null) nothrow {
      return cogl_pipeline_set_layer_point_sprite_coords_enabled(&this, layer_index, enable, UpCast!(GLib2.Error**)(error));
   }
   void set_layer_texture(AT0)(int layer_index, AT0 /*Texture*/ texture) nothrow {
      cogl_pipeline_set_layer_texture(&this, layer_index, UpCast!(Texture*)(texture));
   }

   // VERSION: 2.0
   // Sets the wrap mode for all three coordinates of texture lookups on
   // this layer. This is equivalent to calling
   // cogl_pipeline_set_layer_wrap_mode_s(),
   // cogl_pipeline_set_layer_wrap_mode_t() and
   // cogl_pipeline_set_layer_wrap_mode_p() separately.
   // <layer_index>: the layer number to change.
   // <mode>: the new wrap mode
   void set_layer_wrap_mode()(int layer_index, PipelineWrapMode mode) nothrow {
      cogl_pipeline_set_layer_wrap_mode(&this, layer_index, mode);
   }

   // VERSION: 2.0
   // Sets the wrap mode for the 'p' coordinate of texture lookups on
   // this layer. 'p' is the third coordinate.
   // <layer_index>: the layer number to change.
   // <mode>: the new wrap mode
   void set_layer_wrap_mode_p()(int layer_index, PipelineWrapMode mode) nothrow {
      cogl_pipeline_set_layer_wrap_mode_p(&this, layer_index, mode);
   }

   // VERSION: 2.0
   // Sets the wrap mode for the 's' coordinate of texture lookups on this layer.
   // <layer_index>: the layer number to change.
   // <mode>: the new wrap mode
   void set_layer_wrap_mode_s()(int layer_index, PipelineWrapMode mode) nothrow {
      cogl_pipeline_set_layer_wrap_mode_s(&this, layer_index, mode);
   }

   // VERSION: 2.0
   // Sets the wrap mode for the 't' coordinate of texture lookups on this layer.
   // <layer_index>: the layer number to change.
   // <mode>: the new wrap mode
   void set_layer_wrap_mode_t()(int layer_index, PipelineWrapMode mode) nothrow {
      cogl_pipeline_set_layer_wrap_mode_t(&this, layer_index, mode);
   }

   // VERSION: 2.0
   // Changes the size of points drawn when %COGL_VERTICES_MODE_POINTS is
   // used with the vertex buffer API. Note that typically the GPU will
   // only support a limited minimum and maximum range of point sizes. If
   // the chosen point size is outside that range then the nearest value
   // within that range will be used instead. The size of a point is in
   // screen space so it will be the same regardless of any
   // transformations. The default point size is 1.0.
   // <point_size>: the new point size.
   void set_point_size()(float point_size) nothrow {
      cogl_pipeline_set_point_size(&this, point_size);
   }

   // VERSION: 2.0
   // Sets the shininess of the pipeline, in the standard OpenGL lighting
   // model, which determines the size of the specular highlights. A
   // higher @shininess will produce smaller highlights which makes the
   // object appear more shiny.
   // 
   // The default value is 0.0
   // <shininess>: The desired shininess; must be >= 0.0
   void set_shininess()(float shininess) nothrow {
      cogl_pipeline_set_shininess(&this, shininess);
   }

   // VERSION: 2.0
   // Sets the pipeline's specular color, in the standard OpenGL lighting
   // model. The intensity of the specular color depends on the viewport
   // position, and is brightest along the lines of reflection.
   // 
   // The default value is (0.0, 0.0, 0.0, 1.0)
   // <specular>: The components of the desired specular color
   void set_specular(AT0)(AT0 /*Color*/ specular) nothrow {
      cogl_pipeline_set_specular(&this, UpCast!(Color*)(specular));
   }

   // VERSION: 2.0
   // Sets a new value for the uniform at @uniform_location. If this
   // pipeline has a user program attached and is later used as a source
   // for drawing, the given value will be assigned to the uniform which
   // can be accessed from the shader's source. The value for
   // @uniform_location should be retrieved from the string name of the
   // uniform by calling cogl_pipeline_get_uniform_location().
   // 
   // This function should be used to set uniforms that are of type
   // float. It can also be used to set a single member of a float array
   // uniform.
   // <uniform_location>: The uniform's location identifier
   // <value>: The new value for the uniform
   void set_uniform_1f()(int uniform_location, float value) nothrow {
      cogl_pipeline_set_uniform_1f(&this, uniform_location, value);
   }

   // VERSION: 2.0
   // Sets a new value for the uniform at @uniform_location. If this
   // pipeline has a user program attached and is later used as a source
   // for drawing, the given value will be assigned to the uniform which
   // can be accessed from the shader's source. The value for
   // @uniform_location should be retrieved from the string name of the
   // uniform by calling cogl_pipeline_get_uniform_location().
   // 
   // This function should be used to set uniforms that are of type
   // int. It can also be used to set a single member of a int array
   // uniform or a sampler uniform.
   // <uniform_location>: The uniform's location identifier
   // <value>: The new value for the uniform
   void set_uniform_1i()(int uniform_location, int value) nothrow {
      cogl_pipeline_set_uniform_1i(&this, uniform_location, value);
   }

   // VERSION: 2.0
   // Sets new values for the uniform at @uniform_location. If this
   // pipeline has a user program attached and is later used as a source
   // for drawing, the given values will be assigned to the uniform which
   // can be accessed from the shader's source. The value for
   // @uniform_location should be retrieved from the string name of the
   // uniform by calling cogl_pipeline_get_uniform_location().
   // 
   // This function can be used to set any floating point type uniform,
   // including float arrays and float vectors. For example, to set a
   // single vec4 uniform you would use 4 for @n_components and 1 for
   // @count. To set an array of 8 float values, you could use 1 for
   // @n_components and 8 for @count.
   // <uniform_location>: The uniform's location identifier
   // <n_components>: The number of components in the corresponding uniform's type
   // <count>: The number of values to set
   // <value>: Pointer to the new values to set
   void set_uniform_float(AT0)(int uniform_location, int n_components, int count, AT0 /*float*/ value) nothrow {
      cogl_pipeline_set_uniform_float(&this, uniform_location, n_components, count, UpCast!(float*)(value));
   }

   // VERSION: 2.0
   // Sets new values for the uniform at @uniform_location. If this
   // pipeline has a user program attached and is later used as a source
   // for drawing, the given values will be assigned to the uniform which
   // can be accessed from the shader's source. The value for
   // @uniform_location should be retrieved from the string name of the
   // uniform by calling cogl_pipeline_get_uniform_location().
   // 
   // This function can be used to set any integer type uniform,
   // including int arrays and int vectors. For example, to set a single
   // ivec4 uniform you would use 4 for @n_components and 1 for
   // @count. To set an array of 8 int values, you could use 1 for
   // @n_components and 8 for @count.
   // <uniform_location>: The uniform's location identifier
   // <n_components>: The number of components in the corresponding uniform's type
   // <count>: The number of values to set
   // <value>: Pointer to the new values to set
   void set_uniform_int()(int uniform_location, int n_components, int count, int* value) nothrow {
      cogl_pipeline_set_uniform_int(&this, uniform_location, n_components, count, value);
   }

   // VERSION: 2.0
   // Sets new values for the uniform at @uniform_location. If this
   // pipeline has a user program attached and is later used as a source
   // for drawing, the given values will be assigned to the uniform which
   // can be accessed from the shader's source. The value for
   // @uniform_location should be retrieved from the string name of the
   // uniform by calling cogl_pipeline_get_uniform_location().
   // 
   // This function can be used to set any matrix type uniform, including
   // matrix arrays. For example, to set a single mat4 uniform you would
   // use 4 for @dimensions and 1 for @count. To set an array of 8
   // mat3 values, you could use 3 for @dimensions and 8 for @count.
   // 
   // If @transpose is %FALSE then the matrix is expected to be in
   // column-major order or if it is %TRUE then the matrix is in
   // row-major order. You can pass a #CoglMatrix by calling by passing
   // the result of cogl_matrix_get_array() in @value and setting
   // @transpose to %FALSE.
   // <uniform_location>: The uniform's location identifier
   // <dimensions>: The size of the matrix
   // <count>: The number of values to set
   // <transpose>: Whether to transpose the matrix
   // <value>: Pointer to the new values to set
   void set_uniform_matrix(AT0)(int uniform_location, int dimensions, int count, int transpose, AT0 /*float*/ value) nothrow {
      cogl_pipeline_set_uniform_matrix(&this, uniform_location, dimensions, count, transpose, UpCast!(float*)(value));
   }

   // VERSION: 2.0
   // Associates a linked CoglProgram with the given pipeline so that the
   // program can take full control of vertex and/or fragment processing.
   // 
   // This is an example of how it can be used to associate an ARBfp
   // program with a #CoglPipeline:
   // |[
   // CoglHandle shader;
   // CoglHandle program;
   // CoglPipeline *pipeline;
   // 
   // shader = cogl_create_shader (COGL_SHADER_TYPE_FRAGMENT);
   // cogl_shader_source (shader,
   // "!!ARBfp1.0\n"
   // "MOV result.color,fragment.color;\n"
   // "END\n");
   // cogl_shader_compile (shader);
   // 
   // program = cogl_create_program ();
   // cogl_program_attach_shader (program, shader);
   // cogl_program_link (program);
   // 
   // pipeline = cogl_pipeline_new ();
   // cogl_pipeline_set_user_program (pipeline, program);
   // 
   // cogl_set_source_color4ub (0xff, 0x00, 0x00, 0xff);
   // cogl_rectangle (0, 0, 100, 100);
   // ]|
   // 
   // It is possibly worth keeping in mind that this API is not part of
   // the long term design for how we want to expose shaders to Cogl
   // developers (We are planning on deprecating the cogl_program and
   // cogl_shader APIs in favour of a "snippet" framework) but in the
   // meantime we hope this will handle most practical GLSL and ARBfp
   // requirements.
   // 
   // Also remember you need to check for either the
   // %COGL_FEATURE_SHADERS_GLSL or %COGL_FEATURE_SHADERS_ARBFP before
   // using the cogl_program or cogl_shader API.
   // <program>: A #CoglHandle to a linked CoglProgram
   void set_user_program()(Handle program) nothrow {
      cogl_pipeline_set_user_program(&this, program);
   }

   // Unintrospectable function: new() / cogl_pipeline_new()
   // VERSION: 2.0
   // Allocates and initializes a default simple pipeline that will color
   // a primitive white.
   // RETURNS: a pointer to a new #CoglPipeline
   static Pipeline* new_()() nothrow {
      return cogl_pipeline_new();
   }
}


// Alpha testing happens before blending primitives with the framebuffer and
// gives an opportunity to discard fragments based on a comparison with the
// incoming alpha value and a reference alpha value. The #CoglPipelineAlphaFunc
// determines how the comparison is done.
enum PipelineAlphaFunc {
   NEVER = 512,
   LESS = 513,
   EQUAL = 514,
   LEQUAL = 515,
   GREATER = 516,
   NOTEQUAL = 517,
   GEQUAL = 518,
   ALWAYS = 519
}

// Specifies which faces should be culled. This can be set on a
// pipeline using cogl_pipeline_set_cull_face_mode().
enum PipelineCullFaceMode {
   NONE = 0,
   FRONT = 1,
   BACK = 2,
   BOTH = 3
}

// Texture filtering is used whenever the current pixel maps either to more
// than one texture element (texel) or less than one. These filter enums
// correspond to different strategies used to come up with a pixel color, by
// possibly referring to multiple neighbouring texels and taking a weighted
// average or simply using the nearest texel.
enum PipelineFilter {
   NEAREST = 9728,
   LINEAR = 9729,
   NEAREST_MIPMAP_NEAREST = 9984,
   LINEAR_MIPMAP_NEAREST = 9985,
   NEAREST_MIPMAP_LINEAR = 9986,
   LINEAR_MIPMAP_LINEAR = 9987
}

// VERSION: 2.0
// The callback prototype used with cogl_pipeline_foreach_layer() for
// iterating all the layers of a @pipeline.
// <pipeline>: The #CoglPipeline whos layers are being iterated
// <layer_index>: The current layer index
// <user_data>: The private data passed to cogl_pipeline_foreach_layer()
extern (C) alias int function (Pipeline* pipeline, int layer_index, void* user_data) nothrow PipelineLayerCallback;


// The wrap mode specifies what happens when texture coordinates
// outside the range 0→1 are used. Note that if the filter mode is
// anything but %COGL_PIPELINE_FILTER_NEAREST then texels outside the
// range 0→1 might be used even when the coordinate is exactly 0 or 1
// because OpenGL will try to sample neighbouring pixels. For example
// if you are trying to render the full texture then you may get
// artifacts around the edges when the pixels from the other side are
// merged in if the wrap mode is set to repeat.
enum PipelineWrapMode /* Version 2.0 */ {
   REPEAT = 10497,
   MIRRORED_REPEAT = 33648,
   CLAMP_TO_EDGE = 33071,
   AUTOMATIC = 519
}
struct PixelBuffer {
   // Unintrospectable function: new_with_size_EXP() / cogl_pixel_buffer_new_with_size_EXP()
   static PixelBuffer* new_with_size_EXP(AT0)(uint width, uint height, PixelFormat format, AT0 /*uint*/ stride) nothrow {
      return cogl_pixel_buffer_new_with_size_EXP(width, height, format, UpCast!(uint*)(stride));
   }
}


// Pixel formats used by COGL. For the formats with a byte per
// component, the order of the components specify the order in
// increasing memory addresses. So for example
// %COGL_PIXEL_FORMAT_RGB_888 would have the red component in the
// lowest address, green in the next address and blue after that
// regardless of the endinanness of the system.
// 
// For the 16-bit formats the component order specifies the order
// within a 16-bit number from most significant bit to least
// significant. So for %COGL_PIXEL_FORMAT_RGB_565, the red component
// would be in bits 11-15, the green component would be in 6-11 and
// the blue component would be in 1-5. Therefore the order in memory
// depends on the endianness of the system.
// 
// When uploading a texture %COGL_PIXEL_FORMAT_ANY can be used as the
// internal format. Cogl will try to pick the best format to use
// internally and convert the texture data if necessary.
enum PixelFormat /* Version 0.8 */ {
   ANY = 0,
   A_8 = 17,
   RGB_565 = 4,
   RGBA_4444 = 21,
   RGBA_5551 = 22,
   YUV = 7,
   G_8 = 8,
   RGB_888 = 2,
   BGR_888 = 34,
   RGBA_8888 = 19,
   BGRA_8888 = 51,
   ARGB_8888 = 83,
   ABGR_8888 = 115,
   RGBA_8888_PRE = 147,
   BGRA_8888_PRE = 179,
   ARGB_8888_PRE = 211,
   ABGR_8888_PRE = 243,
   RGBA_4444_PRE = 149,
   RGBA_5551_PRE = 150
}

// A struct for describing the state of a file descriptor that Cogl
// needs to block on. The @events field contains a bitmask of
// #CoglPollFDEvent<!-- -->s that should cause the application to wake
// up. After the application is woken up from idle it should pass back
// an array of #CoglPollFD<!-- -->s to Cogl and update the @revents
// mask to the actual events that occurred on the file descriptor.
// 
// Note that CoglPollFD is deliberately exactly the same as struct
// pollfd on Unix so that it can simply be cast when calling poll.
struct PollFD /* Version 1.10 */ {
   int fd;
   short events, revents;
}


// A bitmask of events that Cogl may need to wake on for a file
// descriptor. Note that these all have the same values as the
// corresponding defines for the poll function call on Unix so they
// may be directly passed to poll.
enum PollFDEvent /* Version 1.10 */ {
   IN = 1,
   PRI = 2,
   OUT = 4,
   ERR = 8,
   HUP = 16,
   NVAL = 32
}
struct Primitive {

   // VERSION: 1.6
   // Draw the given @primitive with the current source material.
   void draw()() nothrow {
      cogl_primitive_draw(&this);
   }
   int get_first_vertex()() nothrow {
      return cogl_primitive_get_first_vertex(&this);
   }
   VerticesMode get_mode()() nothrow {
      return cogl_primitive_get_mode(&this);
   }
   int get_n_vertices_EXP()() nothrow {
      return cogl_primitive_get_n_vertices_EXP(&this);
   }

   // VERSION: 1.6
   // Replaces all the attributes of the given #CoglPrimitive object.
   // <attributes>: A %NULL terminated array of #CoglAttribute pointers
   void set_attributes(AT0)(AT0 /*Attribute**/ attributes, int n_attributes) nothrow {
      cogl_primitive_set_attributes(&this, UpCast!(Attribute**)(attributes), n_attributes);
   }
   void set_first_vertex()(int first_vertex) nothrow {
      cogl_primitive_set_first_vertex(&this, first_vertex);
   }
   void set_indices_EXP(AT0)(AT0 /*Indices*/ indices, int n_indices) nothrow {
      cogl_primitive_set_indices_EXP(&this, UpCast!(Indices*)(indices), n_indices);
   }
   void set_mode()(VerticesMode mode) nothrow {
      cogl_primitive_set_mode(&this, mode);
   }
   void set_n_vertices_EXP()(int n_vertices) nothrow {
      cogl_primitive_set_n_vertices_EXP(&this, n_vertices);
   }

   // Unintrospectable function: new() / cogl_primitive_new()
   // VERSION: 1.6
   // Combines a set of #CoglAttribute<!-- -->s with a specific draw @mode
   // and defines a vertex count so a #CoglPrimitive object can be retained and
   // drawn later with no addition information required.
   // 
   // The value passed as @n_vertices will simply update the
   // #CoglPrimitive::n_vertices property as if
   // cogl_primitive_set_n_vertices() were called. This property defines
   // the number of vertices to read when drawing.
   // RETURNS: A newly allocated #CoglPrimitive object
   // <mode>: A #CoglVerticesMode defining how to draw the vertices
   // <n_vertices>: The number of vertices to process when drawing
   alias cogl_primitive_new new_; // Variadic

   // Unintrospectable function: new_p2() / cogl_primitive_new_p2()
   // VERSION: 1.6
   // Provides a convenient way to describe a primitive, such as a single
   // triangle strip or a triangle fan, that will internally allocate the
   // necessary #CoglAttributeBuffer storage, describe the position
   // attribute with a #CoglAttribute and upload your data.
   // 
   // For example to draw a convex polygon you can do:
   // |[
   // CoglVertexP2 triangle[] =
   // {
   // { 0,   300 },
   // { 150, 0,  },
   // { 300, 300 }
   // };
   // prim = cogl_primitive_new_p2 (COGL_VERTICES_MODE_TRIANGLE_FAN,
   // 3, triangle);
   // cogl_primitive_draw (prim);
   // ]|
   // 
   // The value passed as @n_vertices is initially used to determine how
   // much can be read from @data but it will also be used to update the
   // #CoglPrimitive::n_vertices property as if
   // cogl_primitive_set_n_vertices() were called. This property defines
   // the number of vertices to read when drawing.
   // <note>The primitive API doesn't support drawing with sliced
   // textures (since switching between slices implies changing state and
   // so that implies multiple primitives need to be submitted). You
   // should pass the %COGL_TEXTURE_NO_SLICING flag to all textures that
   // might be used while drawing with this API. If your hardware doesn't
   // support non-power of two textures (For example you are using GLES
   // 1.1) then you will need to make sure your assets are resized to a
   // power-of-two size (though they don't have to be square)</note>
   // 
   // 1. This can be freed using cogl_object_unref().
   // RETURNS: A newly allocated #CoglPrimitive with a reference of
   // <mode>: A #CoglVerticesMode defining how to draw the vertices
   // <n_vertices>: The number of vertices to read from @data and also the number of vertices to read when later drawing.
   // <data>: An array of #CoglVertexP2 vertices
   static Primitive* new_p2(AT0)(VerticesMode mode, int n_vertices, AT0 /*VertexP2*/ data) nothrow {
      return cogl_primitive_new_p2(mode, n_vertices, UpCast!(VertexP2*)(data));
   }

   // Unintrospectable function: new_p2c4() / cogl_primitive_new_p2c4()
   // VERSION: 1.6
   // Provides a convenient way to describe a primitive, such as a single
   // triangle strip or a triangle fan, that will internally allocate the
   // necessary #CoglAttributeBuffer storage, describe the position
   // and color attributes with #CoglAttribute<!-- -->s and upload
   // your data.
   // 
   // For example to draw a convex polygon with a linear gradient you
   // can do:
   // |[
   // CoglVertexP2C4 triangle[] =
   // {
   // { 0,   300,  0xff, 0x00, 0x00, 0xff },
   // { 150, 0,    0x00, 0xff, 0x00, 0xff },
   // { 300, 300,  0xff, 0x00, 0x00, 0xff }
   // };
   // prim = cogl_primitive_new_p2c4 (COGL_VERTICES_MODE_TRIANGLE_FAN,
   // 3, triangle);
   // cogl_primitive_draw (prim);
   // ]|
   // 
   // The value passed as @n_vertices is initially used to determine how
   // much can be read from @data but it will also be used to update the
   // #CoglPrimitive::n_vertices property as if
   // cogl_primitive_set_n_vertices() were called. This property defines
   // the number of vertices to read when drawing.
   // <note>The primitive API doesn't support drawing with sliced
   // textures (since switching between slices implies changing state and
   // so that implies multiple primitives need to be submitted). You
   // should pass the %COGL_TEXTURE_NO_SLICING flag to all textures that
   // might be used while drawing with this API. If your hardware doesn't
   // support non-power of two textures (For example you are using GLES
   // 1.1) then you will need to make sure your assets are resized to a
   // power-of-two size (though they don't have to be square)</note>
   // 
   // 1. This can be freed using cogl_object_unref().
   // RETURNS: A newly allocated #CoglPrimitive with a reference of
   // <mode>: A #CoglVerticesMode defining how to draw the vertices
   // <n_vertices>: The number of vertices to read from @data and also the number of vertices to read when later drawing.
   // <data>: An array of #CoglVertexP2C4 vertices
   static Primitive* new_p2c4(AT0)(VerticesMode mode, int n_vertices, AT0 /*VertexP2C4*/ data) nothrow {
      return cogl_primitive_new_p2c4(mode, n_vertices, UpCast!(VertexP2C4*)(data));
   }

   // Unintrospectable function: new_p2t2() / cogl_primitive_new_p2t2()
   // VERSION: 1.6
   // Provides a convenient way to describe a primitive, such as a single
   // triangle strip or a triangle fan, that will internally allocate the
   // necessary #CoglAttributeBuffer storage, describe the position and
   // texture coordinate attributes with #CoglAttribute<!-- -->s and
   // upload your data.
   // 
   // For example to draw a convex polygon with texture mapping you can
   // do:
   // |[
   // CoglVertexP2T2 triangle[] =
   // {
   // { 0,   300,  0.0, 1.0},
   // { 150, 0,    0.5, 0.0},
   // { 300, 300,  1.0, 1.0}
   // };
   // prim = cogl_primitive_new_p2t2 (COGL_VERTICES_MODE_TRIANGLE_FAN,
   // 3, triangle);
   // cogl_primitive_draw (prim);
   // ]|
   // 
   // The value passed as @n_vertices is initially used to determine how
   // much can be read from @data but it will also be used to update the
   // #CoglPrimitive::n_vertices property as if
   // cogl_primitive_set_n_vertices() were called. This property defines
   // the number of vertices to read when drawing.
   // <note>The primitive API doesn't support drawing with sliced
   // textures (since switching between slices implies changing state and
   // so that implies multiple primitives need to be submitted). You
   // should pass the %COGL_TEXTURE_NO_SLICING flag to all textures that
   // might be used while drawing with this API. If your hardware doesn't
   // support non-power of two textures (For example you are using GLES
   // 1.1) then you will need to make sure your assets are resized to a
   // power-of-two size (though they don't have to be square)</note>
   // 
   // 1. This can be freed using cogl_object_unref().
   // RETURNS: A newly allocated #CoglPrimitive with a reference of
   // <mode>: A #CoglVerticesMode defining how to draw the vertices
   // <n_vertices>: The number of vertices to read from @data and also the number of vertices to read when later drawing.
   // <data>: An array of #CoglVertexP2T2 vertices
   static Primitive* new_p2t2(AT0)(VerticesMode mode, int n_vertices, AT0 /*VertexP2T2*/ data) nothrow {
      return cogl_primitive_new_p2t2(mode, n_vertices, UpCast!(VertexP2T2*)(data));
   }

   // Unintrospectable function: new_p2t2c4() / cogl_primitive_new_p2t2c4()
   // VERSION: 1.6
   // Provides a convenient way to describe a primitive, such as a single
   // triangle strip or a triangle fan, that will internally allocate the
   // necessary #CoglAttributeBuffer storage, describe the position, texture
   // coordinate and color attributes with #CoglAttribute<!-- -->s and
   // upload your data.
   // 
   // For example to draw a convex polygon with texture mapping and a
   // linear gradient you can do:
   // |[
   // CoglVertexP2T2C4 triangle[] =
   // {
   // { 0,   300,  0.0, 1.0,  0xff, 0x00, 0x00, 0xff},
   // { 150, 0,    0.5, 0.0,  0x00, 0xff, 0x00, 0xff},
   // { 300, 300,  1.0, 1.0,  0xff, 0x00, 0x00, 0xff}
   // };
   // prim = cogl_primitive_new_p2t2c4 (COGL_VERTICES_MODE_TRIANGLE_FAN,
   // 3, triangle);
   // cogl_primitive_draw (prim);
   // ]|
   // 
   // The value passed as @n_vertices is initially used to determine how
   // much can be read from @data but it will also be used to update the
   // #CoglPrimitive::n_vertices property as if
   // cogl_primitive_set_n_vertices() were called. This property defines
   // the number of vertices to read when drawing.
   // <note>The primitive API doesn't support drawing with sliced
   // textures (since switching between slices implies changing state and
   // so that implies multiple primitives need to be submitted). You
   // should pass the %COGL_TEXTURE_NO_SLICING flag to all textures that
   // might be used while drawing with this API. If your hardware doesn't
   // support non-power of two textures (For example you are using GLES
   // 1.1) then you will need to make sure your assets are resized to a
   // power-of-two size (though they don't have to be square)</note>
   // 
   // 1. This can be freed using cogl_object_unref().
   // RETURNS: A newly allocated #CoglPrimitive with a reference of
   // <mode>: A #CoglVerticesMode defining how to draw the vertices
   // <n_vertices>: The number of vertices to read from @data and also the number of vertices to read when later drawing.
   // <data>: An array of #CoglVertexP2T2C4 vertices
   static Primitive* new_p2t2c4(AT0)(VerticesMode mode, int n_vertices, AT0 /*VertexP2T2C4*/ data) nothrow {
      return cogl_primitive_new_p2t2c4(mode, n_vertices, UpCast!(VertexP2T2C4*)(data));
   }

   // Unintrospectable function: new_p3() / cogl_primitive_new_p3()
   // VERSION: 1.6
   // Provides a convenient way to describe a primitive, such as a single
   // triangle strip or a triangle fan, that will internally allocate the
   // necessary #CoglAttributeBuffer storage, describe the position
   // attribute with a #CoglAttribute and upload your data.
   // 
   // For example to draw a convex polygon you can do:
   // |[
   // CoglVertexP3 triangle[] =
   // {
   // { 0,   300, 0 },
   // { 150, 0,   0 },
   // { 300, 300, 0 }
   // };
   // prim = cogl_primitive_new_p3 (COGL_VERTICES_MODE_TRIANGLE_FAN,
   // 3, triangle);
   // cogl_primitive_draw (prim);
   // ]|
   // 
   // The value passed as @n_vertices is initially used to determine how
   // much can be read from @data but it will also be used to update the
   // #CoglPrimitive::n_vertices property as if
   // cogl_primitive_set_n_vertices() were called. This property defines
   // the number of vertices to read when drawing.
   // <note>The primitive API doesn't support drawing with sliced
   // textures (since switching between slices implies changing state and
   // so that implies multiple primitives need to be submitted). You
   // should pass the %COGL_TEXTURE_NO_SLICING flag to all textures that
   // might be used while drawing with this API. If your hardware doesn't
   // support non-power of two textures (For example you are using GLES
   // 1.1) then you will need to make sure your assets are resized to a
   // power-of-two size (though they don't have to be square)</note>
   // 
   // 1. This can be freed using cogl_object_unref().
   // RETURNS: A newly allocated #CoglPrimitive with a reference of
   // <mode>: A #CoglVerticesMode defining how to draw the vertices
   // <n_vertices>: The number of vertices to read from @data and also the number of vertices to read when later drawing.
   // <data>: An array of #CoglVertexP3 vertices
   static Primitive* new_p3(AT0)(VerticesMode mode, int n_vertices, AT0 /*VertexP3*/ data) nothrow {
      return cogl_primitive_new_p3(mode, n_vertices, UpCast!(VertexP3*)(data));
   }

   // Unintrospectable function: new_p3c4() / cogl_primitive_new_p3c4()
   // VERSION: 1.6
   // Provides a convenient way to describe a primitive, such as a single
   // triangle strip or a triangle fan, that will internally allocate the
   // necessary #CoglAttributeBuffer storage, describe the position
   // and color attributes with #CoglAttribute<!-- -->s and upload
   // your data.
   // 
   // For example to draw a convex polygon with a linear gradient you
   // can do:
   // |[
   // CoglVertexP3C4 triangle[] =
   // {
   // { 0,   300, 0,  0xff, 0x00, 0x00, 0xff },
   // { 150, 0,   0,  0x00, 0xff, 0x00, 0xff },
   // { 300, 300, 0,  0xff, 0x00, 0x00, 0xff }
   // };
   // prim = cogl_primitive_new_p3c4 (COGL_VERTICES_MODE_TRIANGLE_FAN,
   // 3, triangle);
   // cogl_primitive_draw (prim);
   // ]|
   // 
   // The value passed as @n_vertices is initially used to determine how
   // much can be read from @data but it will also be used to update the
   // #CoglPrimitive::n_vertices property as if
   // cogl_primitive_set_n_vertices() were called. This property defines
   // the number of vertices to read when drawing.
   // <note>The primitive API doesn't support drawing with sliced
   // textures (since switching between slices implies changing state and
   // so that implies multiple primitives need to be submitted). You
   // should pass the %COGL_TEXTURE_NO_SLICING flag to all textures that
   // might be used while drawing with this API. If your hardware doesn't
   // support non-power of two textures (For example you are using GLES
   // 1.1) then you will need to make sure your assets are resized to a
   // power-of-two size (though they don't have to be square)</note>
   // 
   // 1. This can be freed using cogl_object_unref().
   // RETURNS: A newly allocated #CoglPrimitive with a reference of
   // <mode>: A #CoglVerticesMode defining how to draw the vertices
   // <n_vertices>: The number of vertices to read from @data and also the number of vertices to read when later drawing.
   // <data>: An array of #CoglVertexP3C4 vertices
   static Primitive* new_p3c4(AT0)(VerticesMode mode, int n_vertices, AT0 /*VertexP3C4*/ data) nothrow {
      return cogl_primitive_new_p3c4(mode, n_vertices, UpCast!(VertexP3C4*)(data));
   }

   // Unintrospectable function: new_p3t2() / cogl_primitive_new_p3t2()
   // VERSION: 1.6
   // Provides a convenient way to describe a primitive, such as a single
   // triangle strip or a triangle fan, that will internally allocate the
   // necessary #CoglAttributeBuffer storage, describe the position and
   // texture coordinate attributes with #CoglAttribute<!-- -->s and
   // upload your data.
   // 
   // For example to draw a convex polygon with texture mapping you can
   // do:
   // |[
   // CoglVertexP3T2 triangle[] =
   // {
   // { 0,   300, 0,  0.0, 1.0},
   // { 150, 0,   0,  0.5, 0.0},
   // { 300, 300, 0,  1.0, 1.0}
   // };
   // prim = cogl_primitive_new_p3t2 (COGL_VERTICES_MODE_TRIANGLE_FAN,
   // 3, triangle);
   // cogl_primitive_draw (prim);
   // ]|
   // 
   // The value passed as @n_vertices is initially used to determine how
   // much can be read from @data but it will also be used to update the
   // #CoglPrimitive::n_vertices property as if
   // cogl_primitive_set_n_vertices() were called. This property defines
   // the number of vertices to read when drawing.
   // <note>The primitive API doesn't support drawing with sliced
   // textures (since switching between slices implies changing state and
   // so that implies multiple primitives need to be submitted). You
   // should pass the %COGL_TEXTURE_NO_SLICING flag to all textures that
   // might be used while drawing with this API. If your hardware doesn't
   // support non-power of two textures (For example you are using GLES
   // 1.1) then you will need to make sure your assets are resized to a
   // power-of-two size (though they don't have to be square)</note>
   // 
   // 1. This can be freed using cogl_object_unref().
   // RETURNS: A newly allocated #CoglPrimitive with a reference of
   // <mode>: A #CoglVerticesMode defining how to draw the vertices
   // <n_vertices>: The number of vertices to read from @data and also the number of vertices to read when later drawing.
   // <data>: An array of #CoglVertexP3T2 vertices
   static Primitive* new_p3t2(AT0)(VerticesMode mode, int n_vertices, AT0 /*VertexP3T2*/ data) nothrow {
      return cogl_primitive_new_p3t2(mode, n_vertices, UpCast!(VertexP3T2*)(data));
   }

   // Unintrospectable function: new_p3t2c4() / cogl_primitive_new_p3t2c4()
   // VERSION: 1.6
   // Provides a convenient way to describe a primitive, such as a single
   // triangle strip or a triangle fan, that will internally allocate the
   // necessary #CoglAttributeBuffer storage, describe the position, texture
   // coordinate and color attributes with #CoglAttribute<!-- -->s and
   // upload your data.
   // 
   // For example to draw a convex polygon with texture mapping and a
   // linear gradient you can do:
   // |[
   // CoglVertexP3T2C4 triangle[] =
   // {
   // { 0,   300, 0,  0.0, 1.0,  0xff, 0x00, 0x00, 0xff},
   // { 150, 0,   0,  0.5, 0.0,  0x00, 0xff, 0x00, 0xff},
   // { 300, 300, 0,  1.0, 1.0,  0xff, 0x00, 0x00, 0xff}
   // };
   // prim = cogl_primitive_new_p3t2c4 (COGL_VERTICES_MODE_TRIANGLE_FAN,
   // 3, triangle);
   // cogl_primitive_draw (prim);
   // ]|
   // 
   // The value passed as @n_vertices is initially used to determine how
   // much can be read from @data but it will also be used to update the
   // #CoglPrimitive::n_vertices property as if
   // cogl_primitive_set_n_vertices() were called. This property defines
   // the number of vertices to read when drawing.
   // <note>The primitive API doesn't support drawing with sliced
   // textures (since switching between slices implies changing state and
   // so that implies multiple primitives need to be submitted). You
   // should pass the %COGL_TEXTURE_NO_SLICING flag to all textures that
   // might be used while drawing with this API. If your hardware doesn't
   // support non-power of two textures (For example you are using GLES
   // 1.1) then you will need to make sure your assets are resized to a
   // power-of-two size (though they don't have to be square)</note>
   // 
   // 1. This can be freed using cogl_object_unref().
   // RETURNS: A newly allocated #CoglPrimitive with a reference of
   // <mode>: A #CoglVerticesMode defining how to draw the vertices
   // <n_vertices>: The number of vertices to read from @data and also the number of vertices to read when later drawing.
   // <data>: An array of #CoglVertexP3T2C4 vertices
   static Primitive* new_p3t2c4(AT0)(VerticesMode mode, int n_vertices, AT0 /*VertexP3T2C4*/ data) nothrow {
      return cogl_primitive_new_p3t2c4(mode, n_vertices, UpCast!(VertexP3T2C4*)(data));
   }
   // Unintrospectable function: new_with_attributes() / cogl_primitive_new_with_attributes()
   static Primitive* new_with_attributes(AT0)(VerticesMode mode, int n_vertices, AT0 /*Attribute**/ attributes, int n_attributes) nothrow {
      return cogl_primitive_new_with_attributes(mode, n_vertices, UpCast!(Attribute**)(attributes), n_attributes);
   }
}


// A quaternion is comprised of a scalar component and a 3D vector
// component. The scalar component is normally referred to as w and the
// vector might either be referred to as v or a (for axis) or expanded
// with the individual components: (x, y, z) A full quaternion would
// then be written as <pre>[w (x, y, z)]</pre>.
// 
// Quaternions can be considered to represent an axis and angle
// pair although sadly these numbers are buried somewhat under some
// maths...
// 
// For the curious you can see here that a given axis (a) and angle (𝜃)
// pair are represented in a quaternion as follows:
// |[
// [w=cos(𝜃/2) ( x=sin(𝜃/2)*a.x, y=sin(𝜃/2)*a.y, z=sin(𝜃/2)*a.x )]
// ]|
// 
// Unit Quaternions:
// When using Quaternions to represent spatial orientations for 3D
// graphics it's always assumed you have a unit quaternion. The
// magnitude of a quaternion is defined as:
// |[
// sqrt (w² + x² + y² + z²)
// ]|
// and a unit quaternion satisfies this equation:
// |[
// w² + x² + y² + z² = 1
// ]|
// 
// Thankfully most of the time we don't actually have to worry about
// the maths that goes on behind the scenes but if you are curious to
// learn more here are some external references:
// 
// <itemizedlist>
// <listitem>
// <ulink url="http://mathworld.wolfram.com/Quaternion.html"/>
// </listitem>
// <listitem>
// <ulink url="http://www.gamedev.net/reference/articles/article1095.asp"/>
// </listitem>
// <listitem>
// <ulink url="http://www.cprogramming.com/tutorial/3d/quaternions.html"/>
// </listitem>
// <listitem>
// <ulink url="http://www.isner.com/tutorials/quatSpells/quaternion_spells_12.htm"/>
// </listitem>
// <listitem>
// 3D Maths Primer for Graphics and Game Development ISBN-10: 1556229119
// </listitem>
// <listitem>
// <ulink url="http://www.cs.caltech.edu/courses/cs171/quatut.pdf"/>
// </listitem>
// <listitem>
// <ulink url="http://www.j3d.org/matrix_faq/matrfaq_latest.html#Q56"/>
// </listitem>
// </itemizedlist>
// 
// rotation it is sin(𝜃/2)*axis.x
// rotation it is sin(𝜃/2)*axis.y
// rotation it is sin(𝜃/2)*axis.z
struct Quaternion {
   float w, x, y, z, padding0, padding1, padding2, padding3;


   // Unintrospectable method: copy() / cogl_quaternion_copy()
   // VERSION: 2.0
   // Allocates a new #CoglQuaternion on the stack and initializes it with
   // the same values as @src.
   // 
   // using cogl_quaternion_free()
   // RETURNS: A newly allocated #CoglQuaternion which should be freed
   Quaternion* copy()() nothrow {
      return cogl_quaternion_copy(&this);
   }
   // VERSION: 2.0
   float dot_product(AT0)(AT0 /*Quaternion*/ b) nothrow {
      return cogl_quaternion_dot_product(&this, UpCast!(Quaternion*)(b));
   }

   // VERSION: 2.0
   // Frees a #CoglQuaternion that was previously allocated via
   // cogl_quaternion_copy().
   void free()() nothrow {
      cogl_quaternion_free(&this);
   }
   // VERSION: 2.0
   float get_rotation_angle()() nothrow {
      return cogl_quaternion_get_rotation_angle(&this);
   }
   // VERSION: 2.0
   void get_rotation_axis(AT0)(AT0 /*float*/ vector3) nothrow {
      cogl_quaternion_get_rotation_axis(&this, UpCast!(float*)(vector3));
   }

   // VERSION: 2.0
   // Initializes a quaternion that rotates @angle degrees around the
   // axis vector (@x, @y, @z). The axis vector does not need to be
   // normalized.
   // 
   // rotated @angle degrees around the axis vector (@x, @y, @z)
   // RETURNS: A normalized, unit quaternion representing an orientation
   // <angle>: The angle you want to rotate around the given axis
   // <x>: The x component of your axis vector about which you want to rotate.
   // <y>: The y component of your axis vector about which you want to rotate.
   // <z>: The z component of your axis vector about which you want to rotate.
   void init()(float angle, float x, float y, float z) nothrow {
      cogl_quaternion_init(&this, angle, x, y, z);
   }

   // VERSION: 2.0
   // Initializes a quaternion that rotates @angle degrees around the
   // given @axis vector. The axis vector does not need to be
   // normalized.
   // 
   // rotated @angle degrees around the given @axis vector.
   // RETURNS: A normalized, unit quaternion representing an orientation
   void init_from_angle_vector(AT0)(float angle, AT0 /*float*/ axis3f) nothrow {
      cogl_quaternion_init_from_angle_vector(&this, angle, UpCast!(float*)(axis3f));
   }

   // VERSION: 2.0
   // Initializes a [w (x, y,z)] quaternion directly from an array of 4
   // floats: [w,x,y,z].
   // <array>: An array of 4 floats (x,y,z),w
   void init_from_array(AT0)(AT0 /*float*/ array) nothrow {
      cogl_quaternion_init_from_array(&this, UpCast!(float*)(array));
   }
   void init_from_euler(AT0)(AT0 /*Euler*/ euler) nothrow {
      cogl_quaternion_init_from_euler(&this, UpCast!(Euler*)(euler));
   }

   // VERSION: 2.0
   // XXX: check which direction this rotates
   // <angle>: The angle to rotate around the x axis
   void init_from_x_rotation()(float angle) nothrow {
      cogl_quaternion_init_from_x_rotation(&this, angle);
   }

   // VERSION: 2.0
   // 
   // <angle>: The angle to rotate around the y axis
   void init_from_y_rotation()(float angle) nothrow {
      cogl_quaternion_init_from_y_rotation(&this, angle);
   }

   // VERSION: 2.0
   // 
   // <angle>: The angle to rotate around the y axis
   void init_from_z_rotation()(float angle) nothrow {
      cogl_quaternion_init_from_z_rotation(&this, angle);
   }

   // VERSION: 2.0
   // Initializes the quaternion with the canonical quaternion identity
   // [1 (0, 0, 0)] which represents no rotation. Multiplying a
   // quaternion with this identity leaves the quaternion unchanged.
   // 
   // You might also want to consider using
   // cogl_get_static_identity_quaternion().
   void init_identity()() nothrow {
      cogl_quaternion_init_identity(&this);
   }
   // VERSION: 2.0
   void invert()() nothrow {
      cogl_quaternion_invert(&this);
   }

   // VERSION: 2.0
   // This combines the rotations of two quaternions into @result. The
   // operation is not commutative so the order is important because AxB
   // != BxA. Cogl follows the standard convention for quaternions here
   // so the rotations are applied @right to @left. This is similar to the
   // combining of matrices.
   // <left>: The second #CoglQuaternion rotation to apply
   // <right>: The first #CoglQuaternion rotation to apply
   void multiply(AT0, AT1)(AT0 /*Quaternion*/ left, AT1 /*Quaternion*/ right) nothrow {
      cogl_quaternion_multiply(&this, UpCast!(Quaternion*)(left), UpCast!(Quaternion*)(right));
   }

   // Performs a normalized linear interpolation between two quaternions.
   // That is it does a linear interpolation of the quaternion components
   // and then normalizes the result. This will follow the shortest arc
   // between the two orientations (just like the slerp() function) but
   // will not progress at a constant speed. Unlike slerp() nlerp is
   // commutative which is useful if you are blending animations
   // together. (I.e. nlerp (tmp, a, b) followed by nlerp (result, tmp,
   // d) is the same as nlerp (tmp, a, d) followed by nlerp (result, tmp,
   // b)). Finally nlerp is cheaper than slerp so it can be a good choice
   // if you don't need the constant speed property of the slerp() function.
   // 
   // Notable properties:
   // <itemizedlist>
   // <listitem>
   // commutative: Yes
   // </listitem>
   // <listitem>
   // constant velocity: No
   // </listitem>
   // <listitem>
   // torque minimal (travels along the surface of the 4-sphere): Yes
   // </listitem>
   // <listitem>
   // faster than cogl_quaternion_slerp()
   // </listitem>
   // </itemizedlist>
   // <a>: The first #CoglQuaternion
   // <b>: The second #CoglQuaternion
   // <t>: The factor in the range [0,1] used to interpolate between quaterion @a and @b.
   void nlerp(AT0, AT1)(AT0 /*Quaternion*/ a, AT1 /*Quaternion*/ b, float t) nothrow {
      cogl_quaternion_nlerp(&this, UpCast!(Quaternion*)(a), UpCast!(Quaternion*)(b), t);
   }
   // VERSION: 2.0
   void normalize()() nothrow {
      cogl_quaternion_normalize(&this);
   }
   // VERSION: 2.0
   void pow()(float exponent) nothrow {
      cogl_quaternion_pow(&this, exponent);
   }

   // Performs a spherical linear interpolation between two quaternions.
   // 
   // Noteable properties:
   // <itemizedlist>
   // <listitem>
   // commutative: No
   // </listitem>
   // <listitem>
   // constant velocity: Yes
   // </listitem>
   // <listitem>
   // torque minimal (travels along the surface of the 4-sphere): Yes
   // </listitem>
   // <listitem>
   // more expensive than cogl_quaternion_nlerp()
   // </listitem>
   // </itemizedlist>
   void slerp(AT0, AT1)(AT0 /*Quaternion*/ a, AT1 /*Quaternion*/ b, float t) nothrow {
      cogl_quaternion_slerp(&this, UpCast!(Quaternion*)(a), UpCast!(Quaternion*)(b), t);
   }
   // VERSION: 2.0
   void squad(AT0, AT1, AT2, AT3)(AT0 /*Quaternion*/ prev, AT1 /*Quaternion*/ a, AT2 /*Quaternion*/ b, AT3 /*Quaternion*/ next, float t) nothrow {
      cogl_quaternion_squad(&this, UpCast!(Quaternion*)(prev), UpCast!(Quaternion*)(a), UpCast!(Quaternion*)(b), UpCast!(Quaternion*)(next), t);
   }

   // VERSION: 2.0
   // Compares that all the components of quaternions @a and @b are
   // equal.
   // 
   // An epsilon value is not used to compare the float components, but
   // the == operator is at least used so that 0 and -0 are considered
   // equal.
   // RETURNS: %TRUE if the quaternions are equal else %FALSE.
   // <v1>: A #CoglQuaternion
   // <v2>: A #CoglQuaternion
   static int equal(AT0, AT1)(AT0 /*const(void)*/ v1, AT1 /*const(void)*/ v2) nothrow {
      return cogl_quaternion_equal(UpCast!(const(void)*)(v1), UpCast!(const(void)*)(v2));
   }
}

enum int RADIANS_TO_DEGREES = 3754936;
// Flags for cogl_read_pixels()
enum ReadPixelsFlags /* Version 1.0 */ {
   COLOR_BUFFER = 1
}
struct Renderer {

   // VERSION: 1.10
   // This adds a renderer selection @constraint.
   // 
   // Applications should ideally minimize how many of these constraints they
   // depend on to ensure maximum portability.
   // <constraint>: A #CoglRendererConstraint to add
   void add_contraint()(RendererConstraint constraint) nothrow {
      cogl_renderer_add_contraint(&this, constraint);
   }

   // VERSION: 1.10
   // Tests if a given @onscreen_template can be supported with the given
   // @renderer.
   // 
   // else %FALSE.
   // RETURNS: %TRUE if the @onscreen_template can be supported,
   // <onscreen_template>: A #CoglOnscreenTemplate
   int check_onscreen_template(AT0, AT1)(AT0 /*OnscreenTemplate*/ onscreen_template, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_renderer_check_onscreen_template(&this, UpCast!(OnscreenTemplate*)(onscreen_template), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.10
   // Connects the configured @renderer. Renderer connection isn't a
   // very active process, it basically just means validating that
   // any given constraint criteria can be satisfied and that a
   // usable driver and window system backend can be found.
   // 
   // given @renderer. %FALSE if there was an error.
   // RETURNS: %TRUE if there was no error while connecting the
   int connect(AT0)(AT0 /*GLib2.Error**/ error=null) nothrow {
      return cogl_renderer_connect(&this, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.8
   // Queries how many texture units can be used from fragment programs
   // RETURNS: the number of texture image units.
   int get_n_fragment_texture_units()() nothrow {
      return cogl_renderer_get_n_fragment_texture_units(&this);
   }

   // Queries which window system backend Cogl has chosen to use.
   // 
   // This may only be called on a connected #CoglRenderer.
   // 
   // system backend.
   // RETURNS: The #CoglWinsysID corresponding to the chosen window
   WinsysID get_winsys_id()() nothrow {
      return cogl_renderer_get_winsys_id(&this);
   }

   // VERSION: 1.10
   // This removes a renderer selection @constraint.
   // 
   // Applications should ideally minimize how many of these constraints they
   // depend on to ensure maximum portability.
   // <constraint>: A #CoglRendererConstraint to remove
   void remove_constraint()(RendererConstraint constraint) nothrow {
      cogl_renderer_remove_constraint(&this, constraint);
   }

   // This allows you to explicitly select a winsys backend to use instead
   // of letting Cogl automatically select a backend.
   // 
   // if you select an unsupported backend then cogl_renderer_connect()
   // will fail and report an error.
   // 
   // This may only be called on an un-connected #CoglRenderer.
   // <winsys_id>: An ID of the winsys you explicitly want to use.
   void set_winsys_id()(WinsysID winsys_id) nothrow {
      cogl_renderer_set_winsys_id(&this, winsys_id);
   }

   // Unintrospectable function: new() / cogl_renderer_new()
   // VERSION: 1.10
   // Instantiates a new (unconnected) #CoglRenderer object. A
   // #CoglRenderer represents a means to render. It encapsulates the
   // selection of an underlying driver, such as OpenGL or OpenGL-ES and
   // a selection of a window system binding API such as GLX, or EGL or
   // WGL.
   // 
   // While the renderer is unconnected it can be configured so that
   // applications may specify backend constraints, such as "must use
   // x11" for example via cogl_renderer_add_criteria().
   // 
   // There are also some platform specific configuration apis such
   // as cogl_xlib_renderer_set_foreign_display() that may also be
   // used while the renderer is unconnected.
   // 
   // Once the renderer has been configured, then it may (optionally) be
   // explicitly connected using cogl_renderer_connect() which allows
   // errors to be handled gracefully and potentially fallback
   // configurations can be tried out if there are initial failures.
   // 
   // If a renderer is not explicitly connected then cogl_display_new()
   // will automatically connect the renderer for you. If you don't
   // have any code to deal with error/fallback situations then its fine
   // to just let Cogl do the connection for you.
   // 
   // Once you have setup your renderer then the next step is to create a
   // #CoglDisplay using cogl_display_new().
   // 
   // <note>Many applications don't need to explicitly use
   // cogl_renderer_new() or cogl_display_new() and can just jump
   // straight to cogl_context_new() and pass a %NULL display argument
   // so Cogl will automatically connect and setup a renderer and
   // display.</note>
   static Cogl.Renderer* new_()() nothrow {
      return cogl_renderer_new();
   }
}


// These constraint flags are hard-coded features of the different renderer
// backends. Sometimes a platform may support multiple rendering options which
// Cogl will usually choose from automatically. Some of these features are
// important to higher level applications and frameworks though, such as
// whether a renderer is X11 based because an application might only support
// X11 based input handling. An application might also need to ensure EGL is
// used internally too if they depend on access to an EGLDisplay for some
// purpose.
// 
// Applications should ideally minimize how many of these constraints
// they depend on to ensure maximum portability.
enum RendererConstraint /* Version 1.10 */ {
   X11 = 1,
   XLIB = 2,
   EGL = 4
}
enum RendererError {
   NOT_FOUND = 0,
   XLIB_DISPLAY_OPEN = 1
}
enum int SQRTI_ARG_10_PERCENT = 5590;
enum int SQRTI_ARG_5_PERCENT = 210;
enum int SQRTI_ARG_MAX = 4194303;
// Types of shaders
enum ShaderType /* Version 1.0 */ {
   VERTEX = 0,
   FRAGMENT = 1
}
struct Snippet {

   // VERSION: 1.10
   // cogl_snippet_set_declarations() or %NULL if none was set.
   // RETURNS: the source string that was set with
   char* get_declarations()() nothrow {
      return cogl_snippet_get_declarations(&this);
   }

   // VERSION: 1.10
   // called.
   // RETURNS: the hook that was set when cogl_snippet_new() was
   SnippetHook get_hook()() nothrow {
      return cogl_snippet_get_hook(&this);
   }

   // VERSION: 1.10
   // cogl_snippet_set_post() or %NULL if none was set.
   // RETURNS: the source string that was set with
   char* get_post()() nothrow {
      return cogl_snippet_get_post(&this);
   }

   // VERSION: 1.10
   // cogl_snippet_set_pre() or %NULL if none was set.
   // RETURNS: the source string that was set with
   char* get_pre()() nothrow {
      return cogl_snippet_get_pre(&this);
   }

   // VERSION: 1.10
   // cogl_snippet_set_replace() or %NULL if none was set.
   // RETURNS: the source string that was set with
   char* get_replace()() nothrow {
      return cogl_snippet_get_replace(&this);
   }

   // VERSION: 1.10
   // Sets a source string that will be inserted in the global scope of
   // the generated shader when this snippet is used on a pipeline. This
   // string is typically used to declare uniforms, attributes or
   // functions that will be used by the other parts of the snippets.
   // 
   // This function should only be called before the snippet is attached
   // to its first pipeline. After that the snippet should be considered
   // immutable.
   // <declarations>: The new source string for the declarations section of this snippet.
   void set_declarations(AT0)(AT0 /*char*/ declarations) nothrow {
      cogl_snippet_set_declarations(&this, toCString!(char*)(declarations));
   }

   // VERSION: 1.10
   // Sets a source string that will be inserted after the hook point in
   // the generated shader for the pipeline that this snippet is attached
   // to. Please see the documentation of each hook point in
   // #CoglPipeline for a description of how this string should be used.
   // 
   // This function should only be called before the snippet is attached
   // to its first pipeline. After that the snippet should be considered
   // immutable.
   // <post>: The new source string for the post section of this snippet.
   void set_post(AT0)(AT0 /*char*/ post) nothrow {
      cogl_snippet_set_post(&this, toCString!(char*)(post));
   }

   // VERSION: 1.10
   // Sets a source string that will be inserted before the hook point in
   // the generated shader for the pipeline that this snippet is attached
   // to. Please see the documentation of each hook point in
   // #CoglPipeline for a description of how this string should be used.
   // 
   // This function should only be called before the snippet is attached
   // to its first pipeline. After that the snippet should be considered
   // immutable.
   // <pre>: The new source string for the pre section of this snippet.
   void set_pre(AT0)(AT0 /*char*/ pre) nothrow {
      cogl_snippet_set_pre(&this, toCString!(char*)(pre));
   }

   // VERSION: 1.10
   // Sets a source string that will be used instead of any generated
   // source code or any previous snippets for this hook point. Please
   // see the documentation of each hook point in #CoglPipeline for a
   // description of how this string should be used.
   // 
   // This function should only be called before the snippet is attached
   // to its first pipeline. After that the snippet should be considered
   // immutable.
   // <replace>: The new source string for the replace section of this snippet.
   void set_replace(AT0)(AT0 /*char*/ replace) nothrow {
      cogl_snippet_set_replace(&this, toCString!(char*)(replace));
   }

   // Unintrospectable function: new() / cogl_snippet_new()
   // VERSION: 1.10
   // Allocates and initializes a new snippet with the given source strings.
   // RETURNS: a pointer to a new #CoglSnippet
   // <hook>: The point in the pipeline that this snippet will wrap around or replace.
   // <declarations>: The source code for the declarations for this snippet or %NULL. See cogl_snippet_set_declarations().
   // <post>: The source code to run after the hook point where this shader snippet is attached or %NULL. See cogl_snippet_set_post().
   static Snippet* new_(AT0, AT1)(SnippetHook hook, AT0 /*char*/ declarations, AT1 /*char*/ post) nothrow {
      return cogl_snippet_new(hook, toCString!(char*)(declarations), toCString!(char*)(post));
   }
}


// #CoglSnippetHook is used to specify a location within a
// #CoglPipeline where the code of the snippet should be used when it
// is attached to a pipeline.
// 
// <glosslist>
// <glossentry>
// <glossterm>%COGL_SNIPPET_HOOK_VERTEX</glossterm>
// <glossdef>
// <para>
// Adds a shader snippet that will hook on to the vertex processing
// stage of the pipeline. This gives a chance for the application to
// modify the vertex attributes generated by the shader. Typically the
// snippet will modify cogl_color_out or cogl_position_out builtins.
// </para>
// <para>
// The ‘declarations’ string in @snippet will be inserted in the
// global scope of the shader. Use this to declare any uniforms,
// attributes or functions that the snippet requires.
// </para>
// <para>
// The ‘pre’ string in @snippet will be inserted at the top of the
// main() function before any vertex processing is done.
// </para>
// <para>
// The ‘replace’ string in @snippet will be used instead of the
// generated vertex processing if it is present. This can be used if
// the application wants to provide a complete vertex shader and
// doesn't need the generated output from Cogl.
// </para>
// <para>
// The ‘post’ string in @snippet will be inserted after all of the
// standard vertex processing is done. This can be used to modify the
// outputs.
// </para>
// </glossdef>
// </glossentry>
// <glossentry>
// <glossterm>%COGL_SNIPPET_HOOK_VERTEX_TRANSFORM</glossterm>
// <glossdef>
// <para>
// Adds a shader snippet that will hook on to the vertex transform stage.
// Typically the snippet will use the cogl_modelview_matrix,
// cogl_projection_matrix and cogl_modelview_projection_matrix matrices and the
// cogl_position_in attribute. The hook must write to cogl_position_out.
// The default processing for this hook will multiply cogl_position_in by
// the combined modelview-projection matrix and store it on cogl_position_out.
// </para>
// <para>
// The ‘declarations’ string in @snippet will be inserted in the
// global scope of the shader. Use this to declare any uniforms,
// attributes or functions that the snippet requires.
// </para>
// <para>
// The ‘pre’ string in @snippet will be inserted at the top of the
// main() function before the vertex transform is done.
// </para>
// <para>
// The ‘replace’ string in @snippet will be used instead of the
// generated vertex transform if it is present.
// </para>
// <para>
// The ‘post’ string in @snippet will be inserted after all of the
// standard vertex transformation is done. This can be used to modify the
// cogl_position_out in addition to the default processing.
// </para>
// </glossdef>
// </glossentry>
// <glossentry>
// <glossterm>%COGL_SNIPPET_HOOK_FRAGMENT</glossterm>
// <glossdef>
// <para>
// Adds a shader snippet that will hook on to the fragment processing
// stage of the pipeline. This gives a chance for the application to
// modify the fragment color generated by the shader. Typically the
// snippet will modify cogl_color_out.
// </para>
// <para>
// The ‘declarations’ string in @snippet will be inserted in the
// global scope of the shader. Use this to declare any uniforms,
// attributes or functions that the snippet requires.
// </para>
// <para>
// The ‘pre’ string in @snippet will be inserted at the top of the
// main() function before any fragment processing is done.
// </para>
// <para>
// The ‘replace’ string in @snippet will be used instead of the
// generated fragment processing if it is present. This can be used if
// the application wants to provide a complete fragment shader and
// doesn't need the generated output from Cogl.
// </para>
// <para>
// The ‘post’ string in @snippet will be inserted after all of the
// standard fragment processing is done. At this point the generated
// value for the rest of the pipeline state will already be in
// cogl_color_out so the application can modify the result by altering
// this variable.
// </para>
// </glossdef>
// </glossentry>
// <glossentry>
// <glossterm>%COGL_SNIPPET_HOOK_TEXTURE_COORD_TRANSFORM</glossterm>
// <glossdef>
// <para>
// Adds a shader snippet that will hook on to the texture coordinate
// transformation of a particular layer. This can be used to replace
// the processing for a layer or to modify the results.
// </para>
// <para>
// Within the snippet code for this hook there are two extra
// variables. The first is a mat4 called cogl_matrix which represents
// the user matrix for this layer. The second is called cogl_tex_coord
// and represents the incoming and outgoing texture coordinate. On
// entry to the hook, cogl_tex_coord contains the value of the
// corresponding texture coordinate attribute for this layer. The hook
// is expected to modify this variable. The output will be passed as a
// varying to the fragment processing stage. The default code will
// just multiply cogl_matrix by cogl_tex_coord and store the result in
// cogl_tex_coord.
// </para>
// <para>
// The ‘declarations’ string in @snippet will be inserted in the
// global scope of the shader. Use this to declare any uniforms,
// attributes or functions that the snippet requires.
// </para>
// <para>
// The ‘pre’ string in @snippet will be inserted just before the
// fragment processing for this layer. At this point cogl_tex_coord
// still contains the value of the texture coordinate attribute.
// </para>
// <para>
// If a ‘replace’ string is given then this will be used instead of
// the default fragment processing for this layer. The snippet can
// modify cogl_tex_coord or leave it as is to apply no transformation.
// </para>
// <para>
// The ‘post’ string in @snippet will be inserted just after the
// transformation. At this point cogl_tex_coord will contain the
// results of the transformation but it can be further modified by the
// snippet.
// </para>
// </glossdef>
// </glossentry>
// <glossentry>
// <glossterm>%COGL_SNIPPET_HOOK_LAYER_FRAGMENT</glossterm>
// <glossdef>
// <para>
// Adds a shader snippet that will hook on to the fragment processing
// of a particular layer. This can be used to replace the processing
// for a layer or to modify the results.
// </para>
// <para>
// Within the snippet code for this hook there is an extra vec4
// variable called ‘cogl_layer’. This contains the resulting color
// that will be used for the layer. This can be modified in the ‘post’
// section or it the default processing can be replaced entirely using
// the ‘replace’ section.
// </para>
// <para>
// The ‘declarations’ string in @snippet will be inserted in the
// global scope of the shader. Use this to declare any uniforms,
// attributes or functions that the snippet requires.
// </para>
// <para>
// The ‘pre’ string in @snippet will be inserted just before the
// fragment processing for this layer.
// </para>
// <para>
// If a ‘replace’ string is given then this will be used instead of
// the default fragment processing for this layer. The snippet must write to
// the ‘cogl_layer’ variable in that case.
// </para>
// <para>
// The ‘post’ string in @snippet will be inserted just after the
// fragment processing for the layer. The results can be modified by changing
// the value of the ‘cogl_layer’ variable.
// </para>
// </glossdef>
// </glossentry>
// <glossentry>
// <glossterm>%COGL_SNIPPET_HOOK_TEXTURE_LOOKUP</glossterm>
// <glossdef>
// <para>
// Adds a shader snippet that will hook on to the texture lookup part
// of a given layer. This gives a chance for the application to modify
// the coordinates that will be used for the texture lookup or to
// alter the returned texel.
// </para>
// <para>
// Within the snippet code for this hook there are two extra variables
// available. ‘cogl_tex_coord’ is a vec4 which contains the texture
// coordinates that will be used for the texture lookup this can be
// modified. ‘cogl_texel’ will contain the result of the texture
// lookup. This can be modified.
// </para>
// <para>
// The ‘declarations’ string in @snippet will be inserted in the
// global scope of the shader. Use this to declare any uniforms,
// attributes or functions that the snippet requires.
// </para>
// <para>
// The ‘pre’ string in @snippet will be inserted at the top of the
// main() function before any fragment processing is done. This is a
// good place to modify the cogl_tex_coord variable.
// </para>
// <para>
// If a ‘replace’ string is given then this will be used instead of a
// the default texture lookup. The snippet would typically use its own
// sampler in this case.
// </para>
// <para>
// The ‘post’ string in @snippet will be inserted after texture lookup
// has been preformed. Here the snippet can modify the cogl_texel
// variable to alter the returned texel.
// </para>
// </glossdef>
// </glossentry>
// </glosslist>
enum SnippetHook /* Version 1.10 */ {
   VERTEX = 0,
   VERTEX_TRANSFORM = 1,
   FRAGMENT = 2048,
   TEXTURE_COORD_TRANSFORM = 4096,
   LAYER_FRAGMENT = 6144,
   TEXTURE_LOOKUP = 6145
}
struct SubTexture {

   // VERSION: 1.10
   // Retrieves the parent texture that @sub_texture derives its content
   // from.  This is the texture that was passed to
   // cogl_sub_texture_new() as the parent_texture argument.
   // 
   // derives its content from.
   // RETURNS: The parent texture that @sub_texture
   Texture* get_parent()() nothrow {
      return cogl_sub_texture_get_parent(&this);
   }
   // Unintrospectable function: new_EXP() / cogl_sub_texture_new_EXP()
   static SubTexture* new_EXP(AT0, AT1)(AT0 /*Cogl.Context*/ ctx, AT1 /*Texture*/ parent_texture, int sub_x, int sub_y, int sub_width, int sub_height) nothrow {
      return cogl_sub_texture_new_EXP(UpCast!(Cogl.Context*)(ctx), UpCast!(Texture*)(parent_texture), sub_x, sub_y, sub_width, sub_height);
   }
}

extern (C) alias void function (Framebuffer* framebuffer, void* user_data) nothrow SwapBuffersNotify;

struct SwapChain {
   void set_has_alpha_EXP()(int has_alpha) nothrow {
      cogl_swap_chain_set_has_alpha_EXP(&this, has_alpha);
   }
   void set_length_EXP()(int length) nothrow {
      cogl_swap_chain_set_length_EXP(&this, length);
   }
   // Unintrospectable function: new_EXP() / cogl_swap_chain_new_EXP()
   static SwapChain* new_EXP()() nothrow {
      return cogl_swap_chain_new_EXP();
   }
}

enum int TEXTURE_MAX_WASTE = 127;
struct Texture {

   // Copies the pixel data from a cogl texture to system memory.
   // 
   // <note>Don't pass the value of cogl_texture_get_rowstride() as the
   // @rowstride argument, the rowstride should be the rowstride you
   // want for the destination @data buffer not the rowstride of the
   // source texture</note>
   // RETURNS: the size of the texture data in bytes
   // <format>: the #CoglPixelFormat to store the texture as.
   // <rowstride>: the rowstride of @data in bytes or pass 0 to calculate from the bytes-per-pixel of @format multiplied by the @texture width.
   // <data>: memory location to write the @texture's contents, or %NULL to only query the data size through the return value.
   int get_data(AT0)(PixelFormat format, uint rowstride, AT0 /*ubyte*/ data) nothrow {
      return cogl_texture_get_data(&this, format, rowstride, UpCast!(ubyte*)(data));
   }

   // Queries the #CoglPixelFormat of a cogl texture.
   // RETURNS: the #CoglPixelFormat of the GPU side texture
   PixelFormat get_format()() nothrow {
      return cogl_texture_get_format(&this);
   }

   // Queries the GL handles for a GPU side texture through its #CoglTexture.
   // 
   // If the texture is spliced the data for the first sub texture will be
   // queried.
   // 
   // if the handle was invalid
   // RETURNS: %TRUE if the handle was successfully retrieved, %FALSE
   // <out_gl_handle>: pointer to return location for the textures GL handle, or %NULL.
   // <out_gl_target>: pointer to return location for the GL target type, or %NULL.
   int get_gl_texture(AT0, AT1)(/*out*/ AT0 /*GL.uint_*/ out_gl_handle=null, /*out*/ AT1 /*GL.enum_*/ out_gl_target=null) nothrow {
      return cogl_texture_get_gl_texture(&this, UpCast!(GL.uint_*)(out_gl_handle), UpCast!(GL.enum_*)(out_gl_target));
   }

   // Queries the height of a cogl texture.
   // RETURNS: the height of the GPU side texture in pixels
   uint get_height()() nothrow {
      return cogl_texture_get_height(&this);
   }

   // Queries the maximum wasted (unused) pixels in one dimension of a GPU side
   // texture.
   // RETURNS: the maximum waste
   int get_max_waste()() nothrow {
      return cogl_texture_get_max_waste(&this);
   }

   // DEPRECATED (v1.10) method: get_rowstride - There's no replacement for the API but there's
   // Determines the bytes-per-pixel for the #CoglPixelFormat retrieved
   // from cogl_texture_get_format() and multiplies that by the texture's
   // width.
   // 
   // <note>It's very unlikely that anyone would need to use this API to
   // query the internal rowstride of a #CoglTexture which can just be
   // considered an implementation detail. Actually it's not even useful
   // internally since underlying drivers are free to use a different
   // format</note>
   // 
   // </note>This API is only here for backwards compatibility and
   // shouldn't be used in new code. In particular please don't be
   // mislead to pass the returned value to cogl_texture_get_data() for
   // the rowstride, since you should be passing the rowstride you desire
   // for your destination buffer not the rowstride of the source
   // texture.</note>
   // 
   // multiplied by the texture's width
   // 
   // also no known need for API either. It was just
   // a mistake that it was ever published.
   // RETURNS: The bytes-per-pixel for the current format
   uint get_rowstride()() nothrow {
      return cogl_texture_get_rowstride(&this);
   }

   // Queries the width of a cogl texture.
   // RETURNS: the width of the GPU side texture in pixels
   uint get_width()() nothrow {
      return cogl_texture_get_width(&this);
   }

   // Queries if a texture is sliced (stored as multiple GPU side tecture
   // objects).
   // 
   // is stored as a single GPU texture
   // RETURNS: %TRUE if the texture is sliced, %FALSE if the texture
   int is_sliced()() nothrow {
      return cogl_texture_is_sliced(&this);
   }

   // Unintrospectable method: new_from_sub_texture() / cogl_texture_new_from_sub_texture()
   // VERSION: 1.2
   // Creates a new texture which represents a subregion of another
   // texture. The GL resources will be shared so that no new texture
   // data is actually allocated.
   // 
   // Sub textures have undefined behaviour texture coordinates outside
   // of the range [0,1] are used. They also do not work with
   // CoglVertexBuffers.
   // 
   // The sub texture will keep a reference to the full texture so you do
   // not need to keep one separately if you only want to use the sub
   // texture.
   // RETURNS: A newly created #CoglTexture or %NULL on failure
   // <sub_x>: X coordinate of the top-left of the subregion
   // <sub_y>: Y coordinate of the top-left of the subregion
   // <sub_width>: Width in pixels of the subregion
   // <sub_height>: Height in pixels of the subregion
   Texture* new_from_sub_texture()(int sub_x, int sub_y, int sub_width, int sub_height) nothrow {
      return cogl_texture_new_from_sub_texture(&this, sub_x, sub_y, sub_width, sub_height);
   }

   // Sets the pixels in a rectangular subregion of @texture from an in-memory
   // buffer containing pixel data.
   // 
   // <note>The region set can't be larger than the source @data</note>
   // 
   // %FALSE otherwise
   // RETURNS: %TRUE if the subregion upload was successful, and
   // <src_x>: upper left coordinate to use from source data.
   // <src_y>: upper left coordinate to use from source data.
   // <dst_x>: upper left destination horizontal coordinate.
   // <dst_y>: upper left destination vertical coordinate.
   // <dst_width>: width of destination region to write. (Must be less than or equal to @width)
   // <dst_height>: height of destination region to write. (Must be less than or equal to @height)
   // <width>: width of source data buffer.
   // <height>: height of source data buffer.
   // <format>: the #CoglPixelFormat used in the source buffer.
   // <rowstride>: rowstride of source buffer (computed from width if none specified)
   // <data>: the actual pixel data.
   int set_region(AT0)(int src_x, int src_y, int dst_x, int dst_y, uint dst_width, uint dst_height, int width, int height, PixelFormat format, uint rowstride, AT0 /*ubyte*/ data) nothrow {
      return cogl_texture_set_region(&this, src_x, src_y, dst_x, dst_y, dst_width, dst_height, width, height, format, rowstride, UpCast!(ubyte*)(data));
   }
   int set_region_from_bitmap_EXP(AT0)(int src_x, int src_y, int dst_x, int dst_y, uint dst_width, uint dst_height, AT0 /*Bitmap*/ bitmap) nothrow {
      return cogl_texture_set_region_from_bitmap_EXP(&this, src_x, src_y, dst_x, dst_y, dst_width, dst_height, UpCast!(Bitmap*)(bitmap));
   }
   // Unintrospectable function: 2d_new_from_data_EXP() / cogl_texture_2d_new_from_data_EXP()
   static Texture2D* _2d_new_from_data_EXP(AT0, AT1, AT2)(AT0 /*Cogl.Context*/ ctx, int width, int height, PixelFormat format, PixelFormat internal_format, int rowstride, AT1 /*ubyte*/ data, AT2 /*GLib2.Error**/ error=null) nothrow {
      return cogl_texture_2d_new_from_data_EXP(UpCast!(Cogl.Context*)(ctx), width, height, format, internal_format, rowstride, UpCast!(ubyte*)(data), UpCast!(GLib2.Error**)(error));
   }
   // Unintrospectable function: 2d_new_from_foreign_EXP() / cogl_texture_2d_new_from_foreign_EXP()
   static Texture2D* _2d_new_from_foreign_EXP(AT0, AT1)(AT0 /*Cogl.Context*/ ctx, uint gl_handle, int width, int height, PixelFormat format, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_texture_2d_new_from_foreign_EXP(UpCast!(Cogl.Context*)(ctx), gl_handle, width, height, format, UpCast!(GLib2.Error**)(error));
   }
   // Unintrospectable function: 2d_new_with_size_EXP() / cogl_texture_2d_new_with_size_EXP()
   static Texture2D* _2d_new_with_size_EXP(AT0, AT1)(AT0 /*Cogl.Context*/ ctx, int width, int height, PixelFormat internal_format, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_texture_2d_new_with_size_EXP(UpCast!(Cogl.Context*)(ctx), width, height, internal_format, UpCast!(GLib2.Error**)(error));
   }

   // Unintrospectable function: 2d_sliced_new_with_size() / cogl_texture_2d_sliced_new_with_size()
   // VERSION: 1.10
   // Creates a #CoglTexture2DSliced that may internally be comprised of
   // 1 or more #CoglTexture2D textures with power-of-two sizes.
   // @max_waste is used as a threshold for recursively slicing the
   // right-most or bottom-most slices into smaller power-of-two sizes
   // until the wasted padding at the bottom and right of the
   // power-of-two textures is less than specified.
   // 
   // an error allocating any of the internal slices %NULL is
   // returned and @error is updated.
   // RETURNS: A newly allocated #CoglTexture2DSliced or if there was
   // <ctx>: A #CoglContext
   // <width>: The virtual width of your sliced texture.
   // <height>: The virtual height of your sliced texture.
   // <max_waste>: The threshold of how wide a strip of wasted texels are allowed in the non-power-of-two textures before they must be sliced to reduce the amount of waste.
   // <internal_format>: The format of the texture
   static Texture2DSliced* _2d_sliced_new_with_size(AT0, AT1)(AT0 /*Cogl.Context*/ ctx, uint width, uint height, int max_waste, PixelFormat internal_format, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_texture_2d_sliced_new_with_size(UpCast!(Cogl.Context*)(ctx), width, height, max_waste, internal_format, UpCast!(GLib2.Error**)(error));
   }
   // Unintrospectable function: 3d_new_from_data_EXP() / cogl_texture_3d_new_from_data_EXP()
   static Handle _3d_new_from_data_EXP(AT0, AT1)(uint width, uint height, uint depth, TextureFlags flags, PixelFormat format, PixelFormat internal_format, uint rowstride, uint image_stride, AT0 /*ubyte*/ data, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_texture_3d_new_from_data_EXP(width, height, depth, flags, format, internal_format, rowstride, image_stride, UpCast!(ubyte*)(data), UpCast!(GLib2.Error**)(error));
   }
   // Unintrospectable function: 3d_new_with_size_EXP() / cogl_texture_3d_new_with_size_EXP()
   static Handle _3d_new_with_size_EXP(AT0)(uint width, uint height, uint depth, TextureFlags flags, PixelFormat internal_format, AT0 /*GLib2.Error**/ error=null) nothrow {
      return cogl_texture_3d_new_with_size_EXP(width, height, depth, flags, internal_format, UpCast!(GLib2.Error**)(error));
   }

   // Unintrospectable function: new_from_bitmap() / cogl_texture_new_from_bitmap()
   // VERSION: 1.0
   // Creates a #CoglTexture from a #CoglBitmap.
   // RETURNS: A newly created #CoglTexture or %NULL on failure
   // <bitmap>: A #CoglBitmap pointer
   // <flags>: Optional flags for the texture, or %COGL_TEXTURE_NONE
   // <internal_format>: the #CoglPixelFormat to use for the GPU storage of the texture
   static Texture* new_from_bitmap(AT0)(AT0 /*Bitmap*/ bitmap, TextureFlags flags, PixelFormat internal_format) nothrow {
      return cogl_texture_new_from_bitmap(UpCast!(Bitmap*)(bitmap), flags, internal_format);
   }
   // Unintrospectable function: new_from_buffer_EXP() / cogl_texture_new_from_buffer_EXP()
   static Texture* new_from_buffer_EXP(AT0)(AT0 /*PixelBuffer*/ buffer, uint width, uint height, TextureFlags flags, PixelFormat format, PixelFormat internal_format, uint rowstride, uint offset) nothrow {
      return cogl_texture_new_from_buffer_EXP(UpCast!(PixelBuffer*)(buffer), width, height, flags, format, internal_format, rowstride, offset);
   }

   // Unintrospectable function: new_from_data() / cogl_texture_new_from_data()
   // VERSION: 0.8
   // Creates a new #CoglTexture based on data residing in memory.
   // RETURNS: A newly created #CoglTexture or %NULL on failure
   // <width>: width of texture in pixels
   // <height>: height of texture in pixels
   // <flags>: Optional flags for the texture, or %COGL_TEXTURE_NONE
   // <format>: the #CoglPixelFormat the buffer is stored in in RAM
   // <internal_format>: the #CoglPixelFormat that will be used for storing the buffer on the GPU. If COGL_PIXEL_FORMAT_ANY is given then a premultiplied format similar to the format of the source data will be used. The default blending equations of Cogl expect premultiplied color data; the main use of passing a non-premultiplied format here is if you have non-premultiplied source data and are going to adjust the blend mode (see cogl_material_set_blend()) or use the data for something other than straight blending.
   // <rowstride>: the memory offset in bytes between the starts of scanlines in @data
   // <data>: pointer the memory region where the source buffer resides
   static Texture* new_from_data(AT0)(uint width, uint height, TextureFlags flags, PixelFormat format, PixelFormat internal_format, uint rowstride, AT0 /*ubyte*/ data) nothrow {
      return cogl_texture_new_from_data(width, height, flags, format, internal_format, rowstride, UpCast!(ubyte*)(data));
   }

   // Unintrospectable function: new_from_file() / cogl_texture_new_from_file()
   // VERSION: 0.8
   // Creates a #CoglTexture from an image file.
   // RETURNS: A newly created #CoglTexture or %NULL on failure
   // <filename>: the file to load
   // <flags>: Optional flags for the texture, or %COGL_TEXTURE_NONE
   // <internal_format>: the #CoglPixelFormat to use for the GPU storage of the texture. If %COGL_PIXEL_FORMAT_ANY is given then a premultiplied format similar to the format of the source data will be used. The default blending equations of Cogl expect premultiplied color data; the main use of passing a non-premultiplied format here is if you have non-premultiplied source data and are going to adjust the blend mode (see cogl_material_set_blend()) or use the data for something other than straight blending.
   static Texture* new_from_file(AT0, AT1)(AT0 /*char*/ filename, TextureFlags flags, PixelFormat internal_format, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_texture_new_from_file(toCString!(char*)(filename), flags, internal_format, UpCast!(GLib2.Error**)(error));
   }

   // Unintrospectable function: new_from_foreign() / cogl_texture_new_from_foreign()
   // VERSION: 0.8
   // Creates a #CoglTexture based on an existing OpenGL texture; the
   // width, height and format are passed along since it is not always
   // possible to query these from OpenGL.
   // 
   // The waste arguments allow you to create a Cogl texture that maps to
   // a region smaller than the real OpenGL texture. For instance if your
   // hardware only supports power-of-two textures you may load a
   // non-power-of-two image into a larger power-of-two texture and use
   // the waste arguments to tell Cogl which region should be mapped to
   // the texture coordinate range [0:1].
   // RETURNS: A newly created #CoglTexture or %NULL on failure
   // <gl_handle>: opengl handle of foreign texture.
   // <gl_target>: opengl target type of foreign texture
   // <width>: width of foreign texture
   // <height>: height of foreign texture.
   // <x_pot_waste>: horizontal waste on the right hand edge of the texture.
   // <y_pot_waste>: vertical waste on the bottom edge of the texture.
   // <format>: format of the foreign texture.
   static Texture* new_from_foreign()(GL.uint_ gl_handle, GL.enum_ gl_target, GL.uint_ width, GL.uint_ height, GL.uint_ x_pot_waste, GL.uint_ y_pot_waste, PixelFormat format) nothrow {
      return cogl_texture_new_from_foreign(gl_handle, gl_target, width, height, x_pot_waste, y_pot_waste, format);
   }

   // Unintrospectable function: new_with_size() / cogl_texture_new_with_size()
   // VERSION: 0.8
   // Creates a new #CoglTexture with the specified dimensions and pixel format.
   // RETURNS: A newly created #CoglTexture or %NULL on failure
   // <width>: width of texture in pixels.
   // <height>: height of texture in pixels.
   // <flags>: Optional flags for the texture, or %COGL_TEXTURE_NONE
   // <internal_format>: the #CoglPixelFormat to use for the GPU storage of the texture.
   static Texture* new_with_size()(uint width, uint height, TextureFlags flags, PixelFormat internal_format) nothrow {
      return cogl_texture_new_with_size(width, height, flags, internal_format);
   }

   // Unintrospectable function: ref() / cogl_texture_ref()
   // DEPRECATED (v1.2) function: ref - Use cogl_object_ref() instead
   // Increment the reference count for a cogl texture.
   // RETURNS: the @texture pointer.
   // <texture>: a #CoglTexture.
   static void* ref_(AT0)(AT0 /*void*/ texture) nothrow {
      return cogl_texture_ref(UpCast!(void*)(texture));
   }

   // DEPRECATED (v1.2) function: unref - Use cogl_object_unref() instead
   // Decrement the reference count for a cogl texture.
   // <texture>: a #CoglTexture.
   static void unref(AT0)(AT0 /*void*/ texture) nothrow {
      cogl_texture_unref(UpCast!(void*)(texture));
   }
}

struct Texture2D {
}

struct Texture2DSliced {
}

// Error codes that can be thrown when allocating textures.
enum TextureError /* Version 2.0 */ {
   SIZE = 0,
   FORMAT = 1,
   BAD_PARAMETER = 2,
   TYPE = 3
}
// Flags to pass to the cogl_texture_new_* family of functions.
enum TextureFlags /* Version 1.0 */ {
   NONE = 0,
   NO_AUTO_MIPMAP = 1,
   NO_SLICING = 2,
   NO_ATLAS = 4
}
struct TextureRectangle {
   // Unintrospectable function: new_with_size_EXP() / cogl_texture_rectangle_new_with_size_EXP()
   static TextureRectangle* new_with_size_EXP(AT0, AT1)(AT0 /*Cogl.Context*/ ctx, int width, int height, PixelFormat internal_format, AT1 /*GLib2.Error**/ error=null) nothrow {
      return cogl_texture_rectangle_new_with_size_EXP(UpCast!(Cogl.Context*)(ctx), width, height, internal_format, UpCast!(GLib2.Error**)(error));
   }
}

// Used to specify vertex information when calling cogl_polygon()
struct TextureVertex {
   float x, y, z, tx, ty;
   Color color;
}

enum int UNORDERED_MASK = 15;
enum int UNPREMULT_MASK = 127;

// VERSION: 1.4
// When associating private data with a #CoglObject a callback can be
// given which will be called either if the object is destroyed or if
// cogl_object_set_user_data() is called with NULL user_data for the
// same key.
// <user_data>: The data whos association with a #CoglObject has been destoyed.
extern (C) alias void function (void* user_data) nothrow UserDataDestroyCallback;


// A #CoglUserDataKey is used to declare a key for attaching data to a
// #CoglObject using cogl_object_set_user_data. The typedef only exists as a
// formality to make code self documenting since only the unique address of a
// #CoglUserDataKey is used.
// 
// Typically you would declare a static #CoglUserDataKey and set private data
// on an object something like this:
// 
// |[
// static CoglUserDataKey path_private_key;
// 
// static void
// destroy_path_private_cb (void *data)
// {
// g_free (data);
// }
// 
// static void
// my_path_set_data (CoglPath *path, void *data)
// {
// cogl_object_set_user_data (COGL_OBJECT (path),
// &private_key,
// data,
// destroy_path_private_cb);
// }
// ]|
struct UserDataKey /* Version 1.4 */ {
   int unused;
}


// A convenience vertex definition that can be used with
// cogl_primitive_new_with_v2_attributes().
struct VertexP2 /* Version 1.6 */ {
   float x, y;
}


// A convenience vertex definition that can be used with
// cogl_primitive_new_with_v2c4_attributes().
struct VertexP2C4 /* Version 1.6 */ {
   float x, y;
   ubyte r, g, b, a;
}


// A convenience vertex definition that can be used with
// cogl_primitive_new_with_v2t2_attributes().
struct VertexP2T2 /* Version 1.6 */ {
   float x, y, s, t;
}


// A convenience vertex definition that can be used with
// cogl_primitive_new_with_v3t2c4_attributes().
struct VertexP2T2C4 /* Version 1.6 */ {
   float x, y, s, t;
   ubyte r, g, b, a;
}


// A convenience vertex definition that can be used with
// cogl_primitive_new_with_v3_attributes().
struct VertexP3 /* Version 1.6 */ {
   float x, y, z;
}


// A convenience vertex definition that can be used with
// cogl_primitive_new_with_v3c4_attributes().
struct VertexP3C4 /* Version 1.6 */ {
   float x, y, z;
   ubyte r, g, b, a;
}


// A convenience vertex definition that can be used with
// cogl_primitive_new_with_v3t2_attributes().
struct VertexP3T2 /* Version 1.6 */ {
   float x, y, z, s, t;
}


// A convenience vertex definition that can be used with
// cogl_primitive_new_with_v3t2c4_attributes().
struct VertexP3T2C4 /* Version 1.6 */ {
   float x, y, z, s, t;
   ubyte r, g, b, a;
}

// Different ways of interpreting vertices when drawing.
enum VerticesMode /* Version 1.0 */ {
   POINTS = 0,
   LINES = 1,
   LINE_LOOP = 2,
   LINE_STRIP = 3,
   TRIANGLES = 4,
   TRIANGLE_STRIP = 5,
   TRIANGLE_FAN = 6
}

// Enum used to represent the two directions of rotation. This can be
// used to set the front face for culling by calling
// cogl_pipeline_set_front_face_winding().
enum Winding {
   CLOCKWISE = 0,
   COUNTER_CLOCKWISE = 1
}
enum WinsysFeature {
   MULTIPLE_ONSCREEN = 0,
   SWAP_THROTTLE = 1,
   VBLANK_COUNTER = 2,
   VBLANK_WAIT = 3,
   TEXTURE_FROM_PIXMAP = 4,
   SWAP_BUFFERS_EVENT = 5,
   SWAP_REGION = 6,
   SWAP_REGION_THROTTLE = 7,
   SWAP_REGION_SYNCHRONIZED = 8,
   N_FEATURES = 9
}

// Identifies specific window system backends that Cogl supports.
// 
// These can be used to query what backend Cogl is using or to try and
// explicitly select a backend to use.
enum WinsysID {
   ANY = 0,
   STUB = 1,
   GLX = 2,
   EGL_XLIB = 3,
   EGL_NULL = 4,
   EGL_GDL = 5,
   EGL_WAYLAND = 6,
   EGL_KMS = 7,
   EGL_ANDROID = 8,
   WGL = 9,
   SDL = 10
}
// Unintrospectable callback: XlibFilterFunc() / ()
extern (C) alias FilterReturn function (XEvent* event, void* data) nothrow XlibFilterFunc;

struct _ColorSizeCheck {
   char[/*GIR: -1*/ 0] compile_time_assert_CoglColor_size;
}

struct _EulerSizeCheck {
   char[/*GIR: -1*/ 0] compile_time_assert_CoglEuler_size;
}

struct _MatrixSizeCheck {
   char[/*GIR: -1*/ 0] compile_time_assert_CoglMatrix_size;
}

struct _QuaternionSizeCheck {
   char[/*GIR: -1*/ 0] compile_time_assert_CoglQuaternion_size;
}

struct _TextureVertexSizeCheck {
   char[/*GIR: -1*/ 0] compile_time_assert_CoglTextureVertex_size;
}


// Unintrospectable function: angle_cos() / cogl_angle_cos()
// VERSION: 1.0
// Computes the cosine of @angle
// RETURNS: the cosine of the passed angle
// <angle>: an angle expressed using #CoglAngle
static Fixed angle_cos()(Angle angle) nothrow {
   return cogl_angle_cos(angle);
}


// Unintrospectable function: angle_sin() / cogl_angle_sin()
// VERSION: 1.0
// Computes the sine of @angle
// RETURNS: the sine of the passed angle
// <angle>: an angle expressed using #CoglAngle
static Fixed angle_sin()(Angle angle) nothrow {
   return cogl_angle_sin(angle);
}


// Unintrospectable function: angle_tan() / cogl_angle_tan()
// VERSION: 1.0
// Computes the tangent of @angle
// RETURNS: the tangent of the passed angle
// <angle>: an angle expressed using #CoglAngle
static Fixed angle_tan()(Angle angle) nothrow {
   return cogl_angle_tan(angle);
}


// VERSION: 1.0
// We do not advise nor reliably support the interleaving of raw GL drawing and
// Cogl drawing functions, but if you insist, cogl_begin_gl() and cogl_end_gl()
// provide a simple mechanism that may at least give you a fighting chance of
// succeeding.
// 
// Note: this doesn't help you modify the behaviour of Cogl drawing functions
// through the modification of GL state; that will never be reliably supported,
// but if you are trying to do something like:
// 
// |[
// {
// - setup some OpenGL state.
// - draw using OpenGL (e.g. glDrawArrays() )
// - reset modified OpenGL state.
// - continue using Cogl to draw
// }
// ]|
// 
// You should surround blocks of drawing using raw GL with cogl_begin_gl()
// and cogl_end_gl():
// 
// |[
// {
// cogl_begin_gl ();
// - setup some OpenGL state.
// - draw using OpenGL (e.g. glDrawArrays() )
// - reset modified OpenGL state.
// cogl_end_gl ();
// - continue using Cogl to draw
// }
// ]|
// 
// Don't ever try and do:
// 
// |[
// {
// - setup some OpenGL state.
// - use Cogl to draw
// - reset modified OpenGL state.
// }
// ]|
// 
// When the internals of Cogl evolves, this is very liable to break.
// 
// This function will flush all batched primitives, and subsequently flush
// all internal Cogl state to OpenGL as if it were going to draw something
// itself.
// 
// The result is that the OpenGL modelview matrix will be setup; the state
// corresponding to the current source material will be set up and other world
// state such as backface culling, depth and fogging enabledness will be sent
// to OpenGL.
// 
// <note>No special material state is flushed, so if you want Cogl to setup a
// simplified material state it is your responsibility to set a simple source
// material before calling cogl_begin_gl(). E.g. by calling
// cogl_set_source_color4ub().</note>
// 
// <note>It is your responsibility to restore any OpenGL state that you modify
// to how it was after calling cogl_begin_gl() if you don't do this then the
// result of further Cogl calls is undefined.</note>
// 
// <note>You can not nest begin/end blocks.</note>
// 
// Again we would like to stress, we do not advise the use of this API and if
// possible we would prefer to improve Cogl than have developers require raw
// OpenGL.
static void begin_gl()() nothrow {
   cogl_begin_gl();
}

// MOVED TO: BitmapError.quark
static GLib2.Quark bitmap_error_quark()() nothrow {
   return cogl_bitmap_error_quark();
}


// VERSION: 1.0
// MOVED TO: Bitmap.get_size_from_file
// Parses an image file enough to extract the width and height
// of the bitmap.
// RETURNS: %TRUE if the image was successfully parsed
// <filename>: the file to check
// <width>: return location for the bitmap width, or %NULL
// <height>: return location for the bitmap height, or %NULL
static int bitmap_get_size_from_file(AT0)(AT0 /*char*/ filename, /*out*/ int* width, /*out*/ int* height) nothrow {
   return cogl_bitmap_get_size_from_file(toCString!(char*)(filename), width, height);
}

// MOVED TO: BlendStringError.quark
static GLib2.Quark blend_string_error_quark()() nothrow {
   return cogl_blend_string_error_quark();
}


// DEPRECATED (v1.2) function: check_extension - OpenGL is an implementation detail for Cogl and so it's
// Check whether @name occurs in list of extensions in @ext.
// 
// 
// not appropriate to expose OpenGL extensions through the Cogl API. This
// function can be replaced by the following equivalent code:
// |[
// gboolean retval = (strstr (ext, name) != NULL) ? TRUE : FALSE;
// ]|
// RETURNS: %TRUE if the extension occurs in the list, %FALSE otherwise.
// <name>: extension to check for
// <ext>: list of extensions
static int check_extension(AT0, AT1)(AT0 /*char*/ name, AT1 /*char*/ ext) nothrow {
   return cogl_check_extension(toCString!(char*)(name), toCString!(char*)(ext));
}


// Clears all the auxiliary buffers identified in the @buffers mask, and if
// that includes the color buffer then the specified @color is used.
// <color>: Background color to clear to
// <buffers>: A mask of #CoglBufferBit<!-- -->'s identifying which auxiliary buffers to clear
static void clear(AT0)(AT0 /*Color*/ color, c_ulong buffers) nothrow {
   cogl_clear(UpCast!(Color*)(color), buffers);
}


// VERSION: 1.0
// DEPRECATED (v1.2) function: clip_ensure - Calling this function has no effect
// Ensures that the current clipping region has been set in GL. This
// will automatically be called before any Cogl primitives but it
// maybe be neccessary to call if you are using raw GL calls with
// clipping.
static void clip_ensure()() nothrow {
   cogl_clip_ensure();
}


// Reverts the clipping region to the state before the last call to
// cogl_clip_push().
static void clip_pop()() nothrow {
   cogl_clip_pop();
}


// DEPRECATED (v1.2) function: clip_push - The x, y, width, height arguments are inconsistent
// Specifies a rectangular clipping area for all subsequent drawing
// operations. Any drawing commands that extend outside the rectangle
// will be clipped so that only the portion inside the rectangle will
// be displayed. The rectangle dimensions are transformed by the
// current model-view matrix.
// 
// The rectangle is intersected with the current clip region. To undo
// the effect of this function, call cogl_clip_pop().
// 
// with other API that specify rectangles in model space, and when used
// with a coordinate space that puts the origin at the center and y+
// extending up, it's awkward to use. Please use cogl_clip_push_rectangle()
// instead
// <x_offset>: left edge of the clip rectangle
// <y_offset>: top edge of the clip rectangle
// <width>: width of the clip rectangle
// <height>: height of the clip rectangle
static void clip_push()(float x_offset, float y_offset, float width, float height) nothrow {
   cogl_clip_push(x_offset, y_offset, width, height);
}


// VERSION: 1.8
// Sets a new clipping area using the silhouette of the specified,
// filled @path.  The clipping area is intersected with the previous
// clipping area. To restore the previous clipping area, call
// call cogl_clip_pop().
static void clip_push_from_path()() nothrow {
   cogl_clip_push_from_path();
}


// VERSION: 1.0
// Sets a new clipping area using the current path. The current path
// is then cleared. The clipping area is intersected with the previous
// clipping area. To restore the previous clipping area, call
// cogl_clip_pop().
static void clip_push_from_path_preserve()() nothrow {
   cogl_clip_push_from_path_preserve();
}


// VERSION: 1.10
// Sets a new clipping area using a 2D shaped described with a
// #CoglPrimitive. The shape must not contain self overlapping
// geometry and must lie on a single 2D plane. A bounding box of the
// 2D shape in local coordinates (the same coordinates used to
// describe the shape) must be given. It is acceptable for the bounds
// to be larger than the true bounds but behaviour is undefined if the
// bounds are smaller than the true bounds.
// 
// The primitive is transformed by the current model-view matrix and
// the silhouette is intersected with the previous clipping area.  To
// restore the previous clipping area, call
// cogl_clip_pop().
// <primitive>: A #CoglPrimitive describing a flat 2D shape
// <bounds_x1>: y coordinate for the bottom-right corner of the primitives bounds.
// <bounds_y1>: y coordinate for the top-left corner of the primitives bounds
// <bounds_x2>: x coordinate for the top-left corner of the primitives bounds
// <bounds_y2>: x coordinate for the bottom-right corner of the primitives bounds.
static void clip_push_primitive(AT0)(AT0 /*Primitive*/ primitive, float bounds_x1, float bounds_y1, float bounds_x2, float bounds_y2) nothrow {
   cogl_clip_push_primitive(UpCast!(Primitive*)(primitive), bounds_x1, bounds_y1, bounds_x2, bounds_y2);
}


// VERSION: 1.2
// Specifies a rectangular clipping area for all subsequent drawing
// operations. Any drawing commands that extend outside the rectangle
// will be clipped so that only the portion inside the rectangle will
// be displayed. The rectangle dimensions are transformed by the
// current model-view matrix.
// 
// The rectangle is intersected with the current clip region. To undo
// the effect of this function, call cogl_clip_pop().
// <x0>: x coordinate for top left corner of the clip rectangle
// <y0>: y coordinate for top left corner of the clip rectangle
// <x1>: x coordinate for bottom right corner of the clip rectangle
// <y1>: y coordinate for bottom right corner of the clip rectangle
static void clip_push_rectangle()(float x0, float y0, float x1, float y1) nothrow {
   cogl_clip_push_rectangle(x0, y0, x1, y1);
}


// DEPRECATED (v1.2) function: clip_push_window_rect - Use cogl_clip_push_window_rectangle() instead
// Specifies a rectangular clipping area for all subsequent drawing
// operations. Any drawing commands that extend outside the rectangle
// will be clipped so that only the portion inside the rectangle will
// be displayed. The rectangle dimensions are not transformed by the
// current model-view matrix.
// 
// The rectangle is intersected with the current clip region. To undo
// the effect of this function, call cogl_clip_pop().
// <x_offset>: left edge of the clip rectangle in window coordinates
// <y_offset>: top edge of the clip rectangle in window coordinates
// <width>: width of the clip rectangle
// <height>: height of the clip rectangle
static void clip_push_window_rect()(float x_offset, float y_offset, float width, float height) nothrow {
   cogl_clip_push_window_rect(x_offset, y_offset, width, height);
}


// VERSION: 1.2
// Specifies a rectangular clipping area for all subsequent drawing
// operations. Any drawing commands that extend outside the rectangle
// will be clipped so that only the portion inside the rectangle will
// be displayed. The rectangle dimensions are not transformed by the
// current model-view matrix.
// 
// The rectangle is intersected with the current clip region. To undo
// the effect of this function, call cogl_clip_pop().
// <x_offset>: left edge of the clip rectangle in window coordinates
// <y_offset>: top edge of the clip rectangle in window coordinates
// <width>: width of the clip rectangle
// <height>: height of the clip rectangle
static void clip_push_window_rectangle()(int x_offset, int y_offset, int width, int height) nothrow {
   cogl_clip_push_window_rectangle(x_offset, y_offset, width, height);
}


// VERSION: 0.8.2
// DEPRECATED (v1.2) function: clip_stack_restore - This was originally added to allow us to restore
// Restore the state of the clipping stack that was previously saved
// by cogl_clip_stack_save().
// 
// the clip stack when switching back from an offscreen framebuffer,
// but it's not necessary anymore given that framebuffers now own
// separate clip stacks which will be automatically switched between
// when a new buffer is set. Calling this function has no effect
static void clip_stack_restore()() nothrow {
   cogl_clip_stack_restore();
}


// VERSION: 0.8.2
// DEPRECATED (v1.2) function: clip_stack_save - This was originally added to allow us to save the
// Save the entire state of the clipping stack and then clear all
// clipping. The previous state can be returned to with
// cogl_clip_stack_restore(). Each call to cogl_clip_push() after this
// must be matched by a call to cogl_clip_pop() before calling
// cogl_clip_stack_restore().
// 
// clip stack when switching to an offscreen framebuffer, but it's
// not necessary anymore given that framebuffers now own separate
// clip stacks which will be automatically switched between when a
// new buffer is set. Calling this function has no effect
static void clip_stack_save()() nothrow {
   cogl_clip_stack_save();
}

static int clutter_check_extension_CLUTTER(AT0, AT1)(AT0 /*char*/ name, AT1 /*char*/ ext) nothrow {
   return cogl_clutter_check_extension_CLUTTER(toCString!(char*)(name), toCString!(char*)(ext));
}

static int clutter_winsys_has_feature_CLUTTER()(WinsysFeature feature) nothrow {
   return cogl_clutter_winsys_has_feature_CLUTTER(feature);
}

// Unintrospectable function: clutter_winsys_xlib_get_visual_info_CLUTTER() / cogl_clutter_winsys_xlib_get_visual_info_CLUTTER()
static /*CTYPE*/ XVisualInfo* clutter_winsys_xlib_get_visual_info_CLUTTER()() nothrow {
   return cogl_clutter_winsys_xlib_get_visual_info_CLUTTER();
}


// VERSION: 1.0
// MOVED TO: Color.equal
// Compares two #CoglColor<!-- -->s and checks if they are the same.
// 
// This function can be passed to g_hash_table_new() as the @key_equal_func
// parameter, when using #CoglColor<!-- -->s as keys in a #GHashTable.
// RETURNS: %TRUE if the two colors are the same.
// <v1>: a #CoglColor
// <v2>: a #CoglColor
static int color_equal(AT0, AT1)(AT0 /*const(void)*/ v1, AT1 /*const(void)*/ v2) nothrow {
   return cogl_color_equal(UpCast!(const(void)*)(v1), UpCast!(const(void)*)(v2));
}


// Unintrospectable function: create_program() / cogl_create_program()
// Create a new cogl program object that can be used to replace parts of the GL
// rendering pipeline with custom code.
// RETURNS: a new cogl program.
static Handle create_program()() nothrow {
   return cogl_create_program();
}


// Unintrospectable function: create_shader() / cogl_create_shader()
// Create a new shader handle, use cogl_shader_source() to set the
// source code to be used on it.
// RETURNS: a new shader handle.
// <shader_type>: COGL_SHADER_TYPE_VERTEX or COGL_SHADER_TYPE_FRAGMENT.
static Handle create_shader()(ShaderType shader_type) nothrow {
   return cogl_create_shader(shader_type);
}

// Unintrospectable function: debug_object_foreach_type_EXP() / cogl_debug_object_foreach_type_EXP()
static void debug_object_foreach_type_EXP(AT0)(DebugObjectForeachTypeCallback func, AT0 /*void*/ user_data) nothrow {
   cogl_debug_object_foreach_type_EXP(func, UpCast!(void*)(user_data));
}

static void debug_object_print_instances_EXP()() nothrow {
   cogl_debug_object_print_instances_EXP();
}


// This function disables fogging, so primitives drawn afterwards will not be
// blended with any previously set fog color.
static void disable_fog()() nothrow {
   cogl_disable_fog();
}

// Unintrospectable function: double_to_fixed() / cogl_double_to_fixed()
static Fixed double_to_fixed()(double value) nothrow {
   return cogl_double_to_fixed(value);
}

static int double_to_int()(double value) nothrow {
   return cogl_double_to_int(value);
}

static uint double_to_uint()(double value) nothrow {
   return cogl_double_to_uint(value);
}

static void draw_attributes(AT0)(VerticesMode mode, int first_vertex, int n_vertices, AT0 /*Attribute**/ attributes, int n_attributes) nothrow {
   cogl_draw_attributes(mode, first_vertex, n_vertices, UpCast!(Attribute**)(attributes), n_attributes);
}

static void draw_indexed_attributes(AT0, AT1)(VerticesMode mode, int first_vertex, int n_vertices, AT0 /*Indices*/ indices, AT1 /*Attribute**/ attributes, int n_attributes) nothrow {
   cogl_draw_indexed_attributes(mode, first_vertex, n_vertices, UpCast!(Indices*)(indices), UpCast!(Attribute**)(attributes), n_attributes);
}


// VERSION: 1.0
// This is the counterpart to cogl_begin_gl() used to delimit blocks of drawing
// code using raw OpenGL. Please refer to cogl_begin_gl() for full details.
static void end_gl()() nothrow {
   cogl_end_gl();
}


// VERSION: 2.0
// MOVED TO: Euler.equal
// Compares the two given euler angles @v1 and @v1 and it they are
// equal returns %TRUE else %FALSE.
// 
// <note>This function only checks that all three components rotations
// are numerically equal, it does not consider that some rotations
// can be represented with different component rotations</note>
// RETURNS: %TRUE if @v1 and @v2 are equal else %FALSE.
// <v1>: The second euler angle to compare
static int euler_equal(AT0, AT1)(AT0 /*const(void)*/ v1, AT1 /*const(void)*/ v2) nothrow {
   return cogl_euler_equal(UpCast!(const(void)*)(v1), UpCast!(const(void)*)(v2));
}


// Checks whether the given COGL features are available. Multiple
// features can be checked for by or-ing them together with the '|'
// operator. %TRUE is only returned if all of the requested features
// are available.
// RETURNS: %TRUE if the features are available, %FALSE otherwise.
// <features>: A bitmask of features to check for
static int features_available()(FeatureFlags features) nothrow {
   return cogl_features_available(features);
}


// VERSION: 1.0
// This function should only need to be called in exceptional circumstances.
// 
// As an optimization Cogl drawing functions may batch up primitives
// internally, so if you are trying to use raw GL outside of Cogl you stand a
// better chance of being successful if you ask Cogl to flush any batched
// geometry before making your state changes.
// 
// It only ensure that the underlying driver is issued all the commands
// necessary to draw the batched primitives. It provides no guarantees about
// when the driver will complete the rendering.
// 
// This provides no guarantees about the GL state upon returning and to avoid
// confusing Cogl you should aim to restore any changes you make before
// resuming use of Cogl.
// 
// If you are making state changes with the intention of affecting Cogl drawing
// primitives you are 100% on your own since you stand a good chance of
// conflicting with Cogl internals. For example clutter-gst which currently
// uses direct GL calls to bind ARBfp programs will very likely break when Cogl
// starts to use ARBfb programs itself for the material API.
static void flush()() nothrow {
   cogl_flush();
}


// Unintrospectable function: foreach_feature() / cogl_foreach_feature()
// VERSION: 1.10
// Iterates through all the context level features currently supported
// for a given @context and for each feature @callback is called.
// <context>: A #CoglContext pointer
// <callback>: A #CoglFeatureCallback called for each supported feature
// <user_data>: Private data to pass to the callback
static void foreach_feature(AT0, AT1)(AT0 /*Cogl.Context*/ context, FeatureCallback callback, AT1 /*void*/ user_data) nothrow {
   cogl_foreach_feature(UpCast!(Cogl.Context*)(context), callback, UpCast!(void*)(user_data));
}

// MOVED TO: FramebufferError.quark
static GLib2.Quark framebuffer_error_quark()() nothrow {
   return cogl_framebuffer_error_quark();
}


// VERSION: 0.8.2
// Replaces the current projection matrix with a perspective matrix
// for a given viewing frustum defined by 4 side clip planes that
// all cross through the origin and 2 near and far clip planes.
// <left>: X position of the left clipping plane where it intersects the near clipping plane
// <right>: X position of the right clipping plane where it intersects the near clipping plane
// <bottom>: Y position of the bottom clipping plane where it intersects the near clipping plane
// <top>: Y position of the top clipping plane where it intersects the near clipping plane
// <z_near>: The distance to the near clipping plane (Must be positive)
// <z_far>: The distance to the far clipping plane (Must be positive)
static void frustum()(float left, float right, float bottom, float top, float z_near, float z_far) nothrow {
   cogl_frustum(left, right, bottom, top, z_near, z_far);
}


// Queries if backface culling has been enabled via
// cogl_set_backface_culling_enabled()
// RETURNS: %TRUE if backface culling is enabled, and %FALSE otherwise
static int get_backface_culling_enabled()() nothrow {
   return cogl_get_backface_culling_enabled();
}


// Gets the number of bitplanes used for each of the color components
// in the color buffer. Pass %NULL for any of the arguments if the
// value is not required.
// <red>: Return location for the number of red bits or %NULL
// <green>: Return location for the number of green bits or %NULL
// <blue>: Return location for the number of blue bits or %NULL
// <alpha>: Return location for the number of alpha bits or %NULL
static void get_bitmasks()(/*out*/ int* red, /*out*/ int* green, /*out*/ int* blue, /*out*/ int* alpha) nothrow {
   cogl_get_bitmasks(red, green, blue, alpha);
}


// DEPRECATED (v1.4) function: get_depth_test_enabled - Use cogl_material_get_depth_test_enabled()
// Queries if depth testing has been enabled via cogl_set_depth_test_enable()
// 
// 
// instead.
// RETURNS: %TRUE if depth testing is enabled, and %FALSE otherwise
static int get_depth_test_enabled()() nothrow {
   return cogl_get_depth_test_enabled();
}


// Unintrospectable function: get_draw_framebuffer() / cogl_get_draw_framebuffer()
// VERSION: 1.8
// Gets the current #CoglFramebuffer as set using
// cogl_push_framebuffer()
// RETURNS: The current #CoglFramebuffer
static Framebuffer* get_draw_framebuffer()() nothrow {
   return cogl_get_draw_framebuffer();
}


// VERSION: 0.8
// Returns all of the features supported by COGL.
// RETURNS: A logical OR of all the supported COGL features.
static FeatureFlags get_features()() nothrow {
   return cogl_get_features();
}


// Stores the current model-view matrix in @matrix.
// <matrix>: return location for the model-view matrix
static void get_modelview_matrix(AT0)(/*out*/ AT0 /*Matrix*/ matrix) nothrow {
   cogl_get_modelview_matrix(UpCast!(Matrix*)(matrix));
}


// Unintrospectable function: get_option_group() / cogl_get_option_group()
// VERSION: 1.0
// Retrieves the #GOptionGroup used by COGL to parse the command
// line options. Clutter uses this to handle the COGL command line
// options during its initialization process.
// RETURNS: a #GOptionGroup
static GLib2.OptionGroup* get_option_group()() nothrow {
   return cogl_get_option_group();
}


// Unintrospectable function: get_path() / cogl_get_path()
// VERSION: 1.4
// Gets a pointer to the current path. The path can later be used
// again by calling cogl_path_set(). Note that the path isn't copied
// so if you later call any functions to add to the path it will
// affect the returned object too. No reference is taken on the path
// so if you want to retain it you should take your own reference with
// cogl_object_ref().
// RETURNS: a pointer to the current path.
static Path* get_path()() nothrow {
   return cogl_get_path();
}


// Unintrospectable function: get_proc_address() / cogl_get_proc_address()
// Gets a pointer to a given GL or GL ES extension function. This acts
// as a wrapper around glXGetProcAddress() or whatever is the
// appropriate function for the current backend.
// 
// function is not available.
// RETURNS: a pointer to the requested function or %NULL if the
// <name>: the name of the function.
static FuncPtr get_proc_address(AT0)(AT0 /*char*/ name) nothrow {
   return cogl_get_proc_address(toCString!(char*)(name));
}


// Stores the current projection matrix in @matrix.
// <matrix>: return location for the projection matrix
static void get_projection_matrix(AT0)(/*out*/ AT0 /*Matrix*/ matrix) nothrow {
   cogl_get_projection_matrix(UpCast!(Matrix*)(matrix));
}

// Unintrospectable function: get_rectangle_indices() / cogl_get_rectangle_indices()
static Indices* get_rectangle_indices()(int n_rectangles) nothrow {
   return cogl_get_rectangle_indices(n_rectangles);
}


// Unintrospectable function: get_source() / cogl_get_source()
// VERSION: 1.6
// Returns the current source material as previously set using
// cogl_set_source().
// 
// <note>You should typically consider the returned material immutable
// and not try to change any of its properties unless you own a
// reference to that material. At times you may be able to get a
// reference to an internally managed materials and the result of
// modifying such materials is undefined.</note>
// RETURNS: The current source material.
static void* get_source()() nothrow {
   return cogl_get_source();
}


// VERSION: 2.0
// Returns a pointer to a singleton quaternion constant describing the
// canonical identity [1 (0, 0, 0)] which represents no rotation.
// 
// If you multiply a quaternion with the identity quaternion you will
// get back the same value as the original quaternion.
// RETURNS: A pointer to an identity quaternion
static Quaternion* get_static_identity_quaternion()() nothrow {
   return cogl_get_static_identity_quaternion();
}


// VERSION: 2.0
// rotation of 180 degrees around a degenerate axis:
// [0 (0, 0, 0)]
// RETURNS: a pointer to a singleton quaternion constant describing a
static Quaternion* get_static_zero_quaternion()() nothrow {
   return cogl_get_static_zero_quaternion();
}


// Stores the current viewport in @v. @v[0] and @v[1] get the x and y
// position of the viewport and @v[2] and @v[3] get the width and
// height.
// <v>: pointer to a 4 element array of #float<!-- -->s to receive the viewport dimensions.
static void get_viewport()(/*out*/ float v) nothrow {
   cogl_get_viewport(v);
}


// VERSION: 1.10
// Creates a #GSource which handles Cogl's internal system event
// processing. This can be used as a convenience instead of
// cogl_poll_get_info() and cogl_poll_dispatch() in applications that
// are already using the GLib main loop. After this is called the
// #GSource should be attached to the main loop using
// g_source_attach().
// RETURNS: a new #GSource
// <context>: A #CoglContext
// <priority>: The priority of the #GSource
static GLib2.Source* /*new*/ glib_source_new(AT0)(AT0 /*Cogl.Context*/ context, int priority) nothrow {
   return cogl_glib_source_new(UpCast!(Cogl.Context*)(context), priority);
}

static Type handle_get_type()() nothrow {
   return cogl_handle_get_type();
}


// Increases the reference count of @handle by 1
// RETURNS: the handle, with its reference count increased
// <handle>: a #CoglHandle
static Handle handle_ref()(Handle handle) nothrow {
   return cogl_handle_ref(handle);
}


// Drecreases the reference count of @handle by 1; if the reference
// count reaches 0, the resources allocated by @handle will be freed
// <handle>: a #CoglHandle
static void handle_unref()(Handle handle) nothrow {
   cogl_handle_unref(handle);
}


// VERSION: 1.10
// Checks if a given @feature is currently available
// 
// Cogl does not aim to be a lowest common denominator API, it aims to
// expose all the interesting features of GPUs to application which
// means applications have some responsibility to explicitly check
// that certain features are available before depending on them.
// 
// not.
// RETURNS: %TRUE if the @feature is currently supported or %FALSE if
// <context>: A #CoglContext pointer
// <feature>: A #CoglFeatureID
static int has_feature(AT0)(AT0 /*Cogl.Context*/ context, FeatureID feature) nothrow {
   return cogl_has_feature(UpCast!(Cogl.Context*)(context), feature);
}


// Unintrospectable function: has_features() / cogl_has_features()
// VERSION: 1.10
// Checks if a list of features are all currently available.
// 
// This checks all of the listed features using cogl_has_feature() and
// returns %TRUE if all the features are available or %FALSE
// otherwise.
// 
// otherwise.
// RETURNS: %TRUE if all the features are available, %FALSE
// <context>: A #CoglContext pointer
alias cogl_has_features has_features; // Variadic

static IndicesType indices_get_type(AT0)(AT0 /*Indices*/ indices) nothrow {
   return cogl_indices_get_type(UpCast!(Indices*)(indices));
}


// Gets whether the given object references a #CoglAttribute.
// 
// %FALSE otherwise
// RETURNS: %TRUE if the handle references a #CoglAttribute,
// <object>: A #CoglObject
static int is_attribute(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_attribute(UpCast!(void*)(object));
}


// VERSION: 1.4
// Gets whether the given object references a #CoglAttributeBuffer.
// 
// %FALSE otherwise
// RETURNS: %TRUE if the handle references a #CoglAttributeBuffer,
// <object>: A #CoglObject
static int is_attribute_buffer(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_attribute_buffer(UpCast!(void*)(object));
}


// VERSION: 1.0
// Checks whether @handle is a #CoglHandle for a bitmap
// 
// and %FALSE otherwise
// RETURNS: %TRUE if the passed handle represents a bitmap,
// <handle>: a #CoglHandle for a bitmap
static int is_bitmap()(Handle handle) nothrow {
   return cogl_is_bitmap(handle);
}


// VERSION: 1.2
// Checks whether @buffer is a buffer object.
// RETURNS: %TRUE if the handle is a CoglBuffer, and %FALSE otherwise
static int is_buffer(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_buffer(UpCast!(void*)(object));
}


// VERSION: 1.10
// Gets whether the given object references an existing context object.
// 
// %FALSE otherwise
// RETURNS: %TRUE if the handle references a #CoglContext,
// <object>: An object or %NULL
static int is_context(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_context(UpCast!(void*)(object));
}


// VERSION: 1.4
// Gets whether the given object references a #CoglIndexBuffer.
// 
// %FALSE otherwise
// RETURNS: %TRUE if the handle references a #CoglIndexBuffer,
// <object>: A #CoglObject
static int is_index_buffer(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_index_buffer(UpCast!(void*)(object));
}


// Gets whether the given handle references an existing material object.
// 
// %FALSE otherwise
// RETURNS: %TRUE if the handle references a #CoglMaterial,
// <handle>: A CoglHandle
static int is_material()(Handle handle) nothrow {
   return cogl_is_material(handle);
}


// Determines whether the given #CoglHandle references an offscreen buffer
// object.
// 
// %FALSE otherwise
// RETURNS: %TRUE if the handle references an offscreen buffer,
// <handle>: A CoglHandle for an offscreen buffer
static int is_offscreen()(Handle handle) nothrow {
   return cogl_is_offscreen(handle);
}


// VERSION: 2.0
// Gets whether the given object references an existing path object.
// 
// %FALSE otherwise.
// RETURNS: %TRUE if the object references a #CoglPath,
static int is_path()(Handle handle) nothrow {
   return cogl_is_path(handle);
}


// VERSION: 2.0
// Gets whether the given handle references an existing pipeline object.
// 
// %FALSE otherwise
// RETURNS: %TRUE if the handle references a #CoglPipeline,
// <handle>: A CoglHandle
static int is_pipeline()(Handle handle) nothrow {
   return cogl_is_pipeline(handle);
}

static int is_pixel_buffer_EXP(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_pixel_buffer_EXP(UpCast!(void*)(object));
}


// VERSION: 1.6
// Gets whether the given object references a #CoglPrimitive.
// 
// %FALSE otherwise
// RETURNS: %TRUE if the handle references a #CoglPrimitive,
// <object>: A #CoglObject
static int is_primitive(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_primitive(UpCast!(void*)(object));
}


// Gets whether the given handle references an existing program object.
// 
// %FALSE otherwise
// RETURNS: %TRUE if the handle references a program,
// <handle>: A CoglHandle
static int is_program()(Handle handle) nothrow {
   return cogl_is_program(handle);
}


// VERSION: 1.10
// Determines if the given @object is a #CoglRenderer
// RETURNS: %TRUE if @object is a #CoglRenderer, else %FALSE.
// <object>: A #CoglObject pointer
static int is_renderer(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_renderer(UpCast!(void*)(object));
}


// Gets whether the given handle references an existing shader object.
// 
// %FALSE otherwise
// RETURNS: %TRUE if the handle references a shader,
// <handle>: A CoglHandle
static int is_shader()(Handle handle) nothrow {
   return cogl_is_shader(handle);
}


// VERSION: 1.10
// Gets whether the given handle references an existing snippet object.
// 
// %FALSE otherwise
// RETURNS: %TRUE if the handle references a #CoglSnippet,
static int is_snippet(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_snippet(UpCast!(void*)(object));
}

static int is_sub_texture_EXP(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_sub_texture_EXP(UpCast!(void*)(object));
}


// Gets whether the given object references a texture object.
// 
// %FALSE otherwise
// RETURNS: %TRUE if the handle references a texture, and
// <object>: A #CoglObject pointer
static int is_texture(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_texture(UpCast!(void*)(object));
}

static int is_texture_2d_EXP(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_texture_2d_EXP(UpCast!(void*)(object));
}

static int is_texture_3d_EXP()(Handle handle) nothrow {
   return cogl_is_texture_3d_EXP(handle);
}

static int is_texture_rectangle_EXP(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_is_texture_rectangle_EXP(UpCast!(void*)(object));
}


// VERSION: 1.0
// Checks whether @handle is a Vertex Buffer Object
// 
// otherwise
// RETURNS: %TRUE if the handle is a VBO, and %FALSE
// <handle>: a #CoglHandle for a vertex buffer object
static int is_vertex_buffer()(Handle handle) nothrow {
   return cogl_is_vertex_buffer(handle);
}


// VERSION: 1.4
// Checks whether @handle is a handle to the indices for a vertex
// buffer object
// 
// otherwise
// RETURNS: %TRUE if the handle is indices, and %FALSE
// <handle>: a #CoglHandle
static int is_vertex_buffer_indices()(Handle handle) nothrow {
   return cogl_is_vertex_buffer_indices(handle);
}


// Retrieves the type of the layer
// 
// Currently there is only one type of layer defined:
// %COGL_MATERIAL_LAYER_TYPE_TEXTURE, but considering we may add purely GLSL
// based layers in the future, you should write code that checks the type
// first.
// RETURNS: the type of the layer
// <layer>: A #CoglMaterialLayer object
static MaterialLayerType material_layer_get_type(AT0)(AT0 /*MaterialLayer*/ layer) nothrow {
   return cogl_material_layer_get_type(UpCast!(MaterialLayer*)(layer));
}


// VERSION: 1.0
// DEPRECATED (v1.2) function: material_unref - Use cogl_object_unref() instead
// MOVED TO: Material.unref
// Decrement the reference count for a #CoglMaterial.
// <material>: a #CoglMaterial object.
static void material_unref()(Handle material) nothrow {
   cogl_material_unref(material);
}


// VERSION: 1.4
// MOVED TO: Matrix.equal
// Compares two matrices to see if they represent the same
// transformation. Although internally the matrices may have different
// annotations associated with them and may potentially have a cached
// inverse matrix these are not considered in the comparison.
// <v1>: A 4x4 transformation matrix
// <v2>: A 4x4 transformation matrix
static int matrix_equal(AT0, AT1)(AT0 /*const(void)*/ v1, AT1 /*const(void)*/ v2) nothrow {
   return cogl_matrix_equal(UpCast!(const(void)*)(v1), UpCast!(const(void)*)(v2));
}


// Unintrospectable function: object_ref() / cogl_object_ref()
// MOVED TO: Object.ref
// Increases the reference count of @handle by 1
// RETURNS: the @object, with its reference count increased
// <object>: a #CoglObject
static void* object_ref(AT0)(AT0 /*void*/ object) nothrow {
   return cogl_object_ref(UpCast!(void*)(object));
}


// Unintrospectable function: object_unref() / cogl_object_unref()
// MOVED TO: Object.unref
// Drecreases the reference count of @object by 1; if the reference
// count reaches 0, the resources allocated by @object will be freed
// <object>: a #CoglObject
static void object_unref(AT0)(AT0 /*void*/ object) nothrow {
   cogl_object_unref(UpCast!(void*)(object));
}


// This creates an offscreen buffer object using the given @texture as the
// primary color buffer. It doesn't just initialize the contents of the
// offscreen buffer with the @texture; they are tightly bound so that
// drawing to the offscreen buffer effectivly updates the contents of the
// given texture. You don't need to destroy the offscreen buffer before
// you can use the @texture again.
// 
// Note: This does not work with sliced Cogl textures.
// 
// buffer or %COGL_INVALID_HANDLE if it wasn't possible to create the
// buffer.
// RETURNS: a #CoglHandle for the new offscreen
// <texture>: A #CoglTexture pointer
static Handle /*new*/ offscreen_new_to_texture(AT0)(AT0 /*Texture*/ texture) nothrow {
   return cogl_offscreen_new_to_texture(UpCast!(Texture*)(texture));
}


// DEPRECATED (v1.2) function: offscreen_ref - cogl_handle_ref() should be used in new code.
// Increments the reference count on the offscreen buffer.
// RETURNS: For convenience it returns the given CoglHandle
// <handle>: A CoglHandle for an offscreen buffer
static Handle offscreen_ref()(Handle handle) nothrow {
   return cogl_offscreen_ref(handle);
}


// DEPRECATED (v1.2) function: offscreen_unref - cogl_handle_unref() should be used in new code.
// Decreases the reference count for the offscreen buffer and frees it when
// the count reaches 0.
// <handle>: A CoglHandle for an offscreen buffer
static void offscreen_unref()(Handle handle) nothrow {
   cogl_offscreen_unref(handle);
}

// MOVED TO: Onscreen.clutter_backend_set_size_CLUTTER
static void onscreen_clutter_backend_set_size_CLUTTER()(int width, int height) nothrow {
   cogl_onscreen_clutter_backend_set_size_CLUTTER(width, height);
}


// VERSION: 1.0
// Replaces the current projection matrix with an orthographic projection
// matrix. See <xref linkend="cogl-ortho-matrix"/> to see how the matrix is
// calculated.
// 
// <figure id="cogl-ortho-matrix">
// <title></title>
// <graphic fileref="cogl_ortho.png" format="PNG"/>
// </figure>
// 
// <note>This function copies the arguments from OpenGL's glOrtho() even
// though they are unnecessarily confusing due to the z near and z far
// arguments actually being a "distance" from the origin, where
// negative values are behind the viewer, instead of coordinates for
// the z clipping planes which would have been consistent with the
// left, right bottom and top arguments.</note>
// <left>: The coordinate for the left clipping plane
// <right>: The coordinate for the right clipping plane
// <bottom>: The coordinate for the bottom clipping plane
// <top>: The coordinate for the top clipping plane
// <near>: The <emphasis>distance</emphasis> to the near clipping plane (negative if the plane is behind the viewer)
// <far>: The <emphasis>distance</emphasis> for the far clipping plane (negative if the plane is behind the viewer)
static void ortho()(float left, float right, float bottom, float top, float near, float far) nothrow {
   cogl_ortho(left, right, bottom, top, near, far);
}


// VERSION: 2.0
// MOVED TO: Path.arc
// Adds an elliptical arc segment to the current path. A straight line
// segment will link the current pen location with the first vertex
// of the arc. If you perform a move_to to the arcs start just before
// drawing it you create a free standing arc.
// 
// The angles are measured in degrees where 0° is in the direction of
// the positive X axis and 90° is in the direction of the positive Y
// axis. The angle of the arc begins at @angle_1 and heads towards
// @angle_2 (so if @angle_2 is less than @angle_1 it will decrease,
// otherwise it will increase).
// <center_x>: X coordinate of the elliptical arc center
// <center_y>: Y coordinate of the elliptical arc center
// <radius_x>: X radius of the elliptical arc
// <radius_y>: Y radius of the elliptical arc
// <angle_1>: Angle in degrees at which the arc begin
// <angle_2>: Angle in degrees at which the arc ends
static void path_arc()(float center_x, float center_y, float radius_x, float radius_y, float angle_1, float angle_2) nothrow {
   cogl_path_arc(center_x, center_y, radius_x, radius_y, angle_1, angle_2);
}


// VERSION: 2.0
// MOVED TO: Path.close
// Closes the path being constructed by adding a straight line segment
// to it that ends at the first vertex of the path.
static void path_close()() nothrow {
   cogl_path_close();
}


// VERSION: 2.0
// MOVED TO: Path.curve_to
// Adds a cubic bezier curve segment to the current path with the given
// second, third and fourth control points and using current pen location
// as the first control point.
// <x_1>: X coordinate of the second bezier control point
// <y_1>: Y coordinate of the second bezier control point
// <x_2>: X coordinate of the third bezier control point
// <y_2>: Y coordinate of the third bezier control point
// <x_3>: X coordinate of the fourth bezier control point
// <y_3>: Y coordinate of the fourth bezier control point
static void path_curve_to()(float x_1, float y_1, float x_2, float y_2, float x_3, float y_3) nothrow {
   cogl_path_curve_to(x_1, y_1, x_2, y_2, x_3, y_3);
}


// VERSION: 2.0
// MOVED TO: Path.ellipse
// Constructs an ellipse shape. If there is an existing path this will
// start a new disjoint sub-path.
// <center_x>: X coordinate of the ellipse center
// <center_y>: Y coordinate of the ellipse center
// <radius_x>: X radius of the ellipse
// <radius_y>: Y radius of the ellipse
static void path_ellipse()(float center_x, float center_y, float radius_x, float radius_y) nothrow {
   cogl_path_ellipse(center_x, center_y, radius_x, radius_y);
}


// VERSION: 2.0
// MOVED TO: Path.fill
// Fills the interior of the constructed shape using the current
// drawing color.
// 
// The interior of the shape is determined using the fill rule of the
// path. See %CoglPathFillRule for details.
// 
// <note>The result of referencing sliced textures in your current
// pipeline when filling a path are undefined. You should pass
// the %COGL_TEXTURE_NO_SLICING flag when loading any texture you will
// use while filling a path.</note>
static void path_fill()() nothrow {
   cogl_path_fill();
}


// VERSION: 1.0
// MOVED TO: Path.fill_preserve
// Fills the interior of the constructed shape using the current
// drawing color and preserves the path to be used again. See
// cogl_path_fill() for a description what is considered the interior
// of the shape.
static void path_fill_preserve()() nothrow {
   cogl_path_fill_preserve();
}


// VERSION: 2.0
// MOVED TO: Path.get_fill_rule
// Retrieves the fill rule set using cogl_path_set_fill_rule().
// RETURNS: the fill rule that is used for the current path.
static PathFillRule path_get_fill_rule()() nothrow {
   return cogl_path_get_fill_rule();
}


// VERSION: 2.0
// MOVED TO: Path.line
// Constructs a straight line shape starting and ending at the given
// coordinates. If there is an existing path this will start a new
// disjoint sub-path.
// <x_1>: X coordinate of the start line vertex
// <y_1>: Y coordinate of the start line vertex
// <x_2>: X coordinate of the end line vertex
// <y_2>: Y coordinate of the end line vertex
static void path_line()(float x_1, float y_1, float x_2, float y_2) nothrow {
   cogl_path_line(x_1, y_1, x_2, y_2);
}


// VERSION: 2.0
// MOVED TO: Path.line_to
// Adds a straight line segment to the current path that ends at the
// given coordinates.
// <x>: X coordinate of the end line vertex
// <y>: Y coordinate of the end line vertex
static void path_line_to()(float x, float y) nothrow {
   cogl_path_line_to(x, y);
}


// VERSION: 2.0
// MOVED TO: Path.move_to
// Moves the pen to the given location. If there is an existing path
// this will start a new disjoint subpath.
// <x>: X coordinate of the pen location to move to.
// <y>: Y coordinate of the pen location to move to.
static void path_move_to()(float x, float y) nothrow {
   cogl_path_move_to(x, y);
}


// VERSION: 2.0
// MOVED TO: Path.new
// Creates a new, empty path object. The default fill rule is
// %COGL_PATH_FILL_RULE_EVEN_ODD.
// 
// be freed using cogl_object_unref().
// RETURNS: A pointer to a newly allocated #CoglPath, which can
static void path_new()() nothrow {
   cogl_path_new();
}


// VERSION: 2.0
// MOVED TO: Path.polygon
// Constructs a polygonal shape of the given number of vertices. If
// there is an existing path this will start a new disjoint sub-path.
// 
// The coords array must contain 2 * num_points values. The first value
// represents the X coordinate of the first vertex, the second value
// represents the Y coordinate of the first vertex, continuing in the same
// fashion for the rest of the vertices.
// <coords>: A pointer to the first element of an array of fixed-point values that specify the vertex coordinates.
// <num_points>: The total number of vertices.
static void path_polygon(AT0)(AT0 /*float*/ coords, int num_points) nothrow {
   cogl_path_polygon(UpCast!(float*)(coords), num_points);
}


// VERSION: 2.0
// MOVED TO: Path.polyline
// Constructs a series of straight line segments, starting from the
// first given vertex coordinate. If there is an existing path this
// will start a new disjoint sub-path. Each subsequent segment starts
// where the previous one ended and ends at the next given vertex
// coordinate.
// 
// The coords array must contain 2 * num_points values. The first value
// represents the X coordinate of the first vertex, the second value
// represents the Y coordinate of the first vertex, continuing in the same
// fashion for the rest of the vertices. (num_points - 1) segments will
// be constructed.
// <coords>: A pointer to the first element of an array of fixed-point values that specify the vertex coordinates.
// <num_points>: The total number of vertices.
static void path_polyline(AT0)(AT0 /*float*/ coords, int num_points) nothrow {
   cogl_path_polyline(UpCast!(float*)(coords), num_points);
}


// VERSION: 2.0
// MOVED TO: Path.rectangle
// Constructs a rectangular shape at the given coordinates. If there
// is an existing path this will start a new disjoint sub-path.
// <x_1>: X coordinate of the top-left corner.
// <y_1>: Y coordinate of the top-left corner.
// <x_2>: X coordinate of the bottom-right corner.
// <y_2>: Y coordinate of the bottom-right corner.
static void path_rectangle()(float x_1, float y_1, float x_2, float y_2) nothrow {
   cogl_path_rectangle(x_1, y_1, x_2, y_2);
}


// VERSION: 2.0
// MOVED TO: Path.rel_curve_to
// Adds a cubic bezier curve segment to the current path with the given
// second, third and fourth control points and using current pen location
// as the first control point. The given coordinates are relative to the
// current pen location.
// <x_1>: X coordinate of the second bezier control point
// <y_1>: Y coordinate of the second bezier control point
// <x_2>: X coordinate of the third bezier control point
// <y_2>: Y coordinate of the third bezier control point
// <x_3>: X coordinate of the fourth bezier control point
// <y_3>: Y coordinate of the fourth bezier control point
static void path_rel_curve_to()(float x_1, float y_1, float x_2, float y_2, float x_3, float y_3) nothrow {
   cogl_path_rel_curve_to(x_1, y_1, x_2, y_2, x_3, y_3);
}


// VERSION: 2.0
// MOVED TO: Path.rel_line_to
// Adds a straight line segment to the current path that ends at the
// given coordinates relative to the current pen location.
// <x>: X offset from the current pen location of the end line vertex
// <y>: Y offset from the current pen location of the end line vertex
static void path_rel_line_to()(float x, float y) nothrow {
   cogl_path_rel_line_to(x, y);
}


// VERSION: 2.0
// MOVED TO: Path.rel_move_to
// Moves the pen to the given offset relative to the current pen
// location. If there is an existing path this will start a new
// disjoint subpath.
// <x>: X offset from the current pen location to move the pen to.
// <y>: Y offset from the current pen location to move the pen to.
static void path_rel_move_to()(float x, float y) nothrow {
   cogl_path_rel_move_to(x, y);
}


// VERSION: 2.0
// MOVED TO: Path.round_rectangle
// Constructs a rectangular shape with rounded corners. If there is an
// existing path this will start a new disjoint sub-path.
// <x_1>: X coordinate of the top-left corner.
// <y_1>: Y coordinate of the top-left corner.
// <x_2>: X coordinate of the bottom-right corner.
// <y_2>: Y coordinate of the bottom-right corner.
// <radius>: Radius of the corner arcs.
// <arc_step>: Angle increment resolution for subdivision of the corner arcs.
static void path_round_rectangle()(float x_1, float y_1, float x_2, float y_2, float radius, float arc_step) nothrow {
   cogl_path_round_rectangle(x_1, y_1, x_2, y_2, radius, arc_step);
}


// VERSION: 2.0
// MOVED TO: Path.set_fill_rule
// Sets the fill rule of the current path to @fill_rule. This will
// affect how the path is filled when cogl_path_fill() is later
// called. Note that the fill rule state is attached to the path so
// calling cogl_get_path() will preserve the fill rule and calling
// cogl_path_new() will reset the fill rule back to the default.
// <fill_rule>: The new fill rule.
static void path_set_fill_rule()(PathFillRule fill_rule) nothrow {
   cogl_path_set_fill_rule(fill_rule);
}


// VERSION: 2.0
// MOVED TO: Path.stroke
// Strokes the constructed shape using the current drawing color and a
// width of 1 pixel (regardless of the current transformation
// matrix).
static void path_stroke()() nothrow {
   cogl_path_stroke();
}


// VERSION: 1.0
// MOVED TO: Path.stroke_preserve
// Strokes the constructed shape using the current drawing color and
// preserves the path to be used again.
static void path_stroke_preserve()() nothrow {
   cogl_path_stroke_preserve();
}


// Replaces the current projection matrix with a perspective matrix
// based on the provided values.
// 
// <note>You should be careful not to have to great a @z_far / @z_near
// ratio since that will reduce the effectiveness of depth testing
// since there wont be enough precision to identify the depth of
// objects near to each other.</note>
// <fovy>: Vertical field of view angle in degrees.
// <aspect>: The (width over height) aspect ratio for display
// <z_near>: The distance to the near clipping plane (Must be positive)
// <z_far>: The distance to the far clipping plane (Must be positive)
static void perspective()(float fovy, float aspect, float z_near, float z_far) nothrow {
   cogl_perspective(fovy, aspect, z_near, z_far);
}


// VERSION: 1.10
// This should be called whenever an application is woken up from
// going idle in its main loop. The @poll_fds array should contain a
// list of file descriptors matched with the events that occurred in
// revents. The events field is ignored. It is safe to pass in extra
// file descriptors that Cogl didn't request from
// cogl_context_begin_idle() or a shorter array missing some file
// descriptors that Cogl requested.
// <context>: A #CoglContext
// <poll_fds>: An array of #CoglPollFD<!-- -->s describing the events that have occurred since the application went idle.
// <n_poll_fds>: The length of the @poll_fds array.
static void poll_dispatch(AT0, AT1)(AT0 /*Cogl.Context*/ context, AT1 /*PollFD*/ poll_fds, int n_poll_fds) nothrow {
   cogl_poll_dispatch(UpCast!(Cogl.Context*)(context), UpCast!(PollFD*)(poll_fds), n_poll_fds);
}


// VERSION: 1.10
// This should be called whenever an application is about to go idle
// so that Cogl has a chance to describe what state it needs to be
// woken up on. The assumption is that the application is using a main
// loop with something like the poll function call on Unix or the GLib
// main loop.
// 
// After the function is called *@poll_fds will contain a pointer to
// an array of #CoglPollFD structs describing the file descriptors
// that Cogl expects. The fd and events members will be updated
// accordingly. After the application has completed its idle it is
// expected to either update the revents members directly in this
// array or to create a copy of the array and update them
// there. Either way it should pass a pointer to either array back to
// Cogl when calling cogl_poll_dispatch().
// 
// When using the %COGL_WINSYS_ID_WGL winsys (where file descriptors
// don't make any sense) or %COGL_WINSYS_ID_SDL (where the event
// handling functions of SDL don't allow blocking on a file
// descriptor) *n_poll_fds is guaranteed to be zero.
// 
// @timeout will contain a maximum amount of time to wait in
// microseconds before the application should wake up or -1 if the
// application should wait indefinitely. This can also be 0 zero if
// Cogl needs to be woken up immediately.
// <context>: A #CoglContext
// <poll_fds>: A return location for a pointer to an array of #CoglPollFD<!-- -->s
// <n_poll_fds>: A return location for the number of entries in *@poll_fds
// <timeout>: A return location for the maximum length of time to wait in microseconds, or -1 to wait indefinitely.
static void poll_get_info(AT0, AT1, AT2)(AT0 /*Cogl.Context*/ context, AT1 /*PollFD**/ poll_fds, int* n_poll_fds, AT2 /*long*/ timeout) nothrow {
   cogl_poll_get_info(UpCast!(Cogl.Context*)(context), UpCast!(PollFD**)(poll_fds), n_poll_fds, UpCast!(long*)(timeout));
}


// VERSION: 1.0
// Draws a convex polygon using the current source material to fill / texture
// with according to the texture coordinates passed.
// 
// If @use_color is %TRUE then the color will be changed for each vertex using
// the value specified in the color member of #CoglTextureVertex. This can be
// used for example to make the texture fade out by setting the alpha value of
// the color.
// 
// All of the texture coordinates must be in the range [0,1] and repeating the
// texture is not supported.
// 
// Because of the way this function is implemented it will currently
// only work if either the texture is not sliced or the backend is not
// OpenGL ES and the minifying and magnifying functions are both set
// to COGL_MATERIAL_FILTER_NEAREST.
// <vertices>: An array of #CoglTextureVertex structs
// <n_vertices>: The length of the vertices array
// <use_color>: %TRUE if the color member of #CoglTextureVertex should be used
static void polygon(AT0)(AT0 /*TextureVertex*/ vertices, uint n_vertices, int use_color) nothrow {
   cogl_polygon(UpCast!(TextureVertex*)(vertices), n_vertices, use_color);
}


// DEPRECATED (v1.2) function: pop_draw_buffer - The draw buffer API was replaced with a framebuffer API
// Restore cogl_set_draw_buffer() state.
static void pop_draw_buffer()() nothrow {
   cogl_pop_draw_buffer();
}


// VERSION: 1.2
// Restores the framebuffer that was previously at the top of the stack.
// All subsequent drawing will be redirected to this framebuffer.
static void pop_framebuffer()() nothrow {
   cogl_pop_framebuffer();
}

// Restores the current model-view matrix from the matrix stack.
static void pop_matrix()() nothrow {
   cogl_pop_matrix();
}


// VERSION: 1.6
// Removes the material at the top of the source stack. The material
// at the top of this stack defines the GPU state used to process
// later primitives as defined by cogl_set_source().
static void pop_source()() nothrow {
   cogl_pop_source();
}


// Attaches a shader to a program object. A program can have multiple
// vertex or fragment shaders but only one of them may provide a
// main() function. It is allowed to use a program with only a vertex
// shader or only a fragment shader.
// <program_handle>: a #CoglHandle for a shdaer program.
// <shader_handle>: a #CoglHandle for a vertex of fragment shader.
static void program_attach_shader()(Handle program_handle, Handle shader_handle) nothrow {
   cogl_program_attach_shader(program_handle, shader_handle);
}


// Retrieve the location (offset) of a uniform variable in a shader program,
// a uniform is a variable that is constant for all vertices/fragments for a
// shader object and is possible to modify as an external parameter.
// 
// This uniform can be set using cogl_program_uniform_1f() when the
// program is in use.
// RETURNS: the offset of a uniform in a specified program.
// <handle>: a #CoglHandle for a shader program.
// <uniform_name>: the name of a uniform.
static int program_get_uniform_location(AT0)(Handle handle, AT0 /*char*/ uniform_name) nothrow {
   return cogl_program_get_uniform_location(handle, toCString!(char*)(uniform_name));
}


// Links a program making it ready for use. Note that calling this
// function is optional. If it is not called the program will
// automatically be linked the first time it is used.
// <handle>: a #CoglHandle for a shader program.
static void program_link()(Handle handle) nothrow {
   cogl_program_link(handle);
}


// Unintrospectable function: program_ref() / cogl_program_ref()
// DEPRECATED (v1.0) function: program_ref - Please use cogl_handle_ref() instead.
// Add an extra reference to a program.
// RETURNS: @handle
// <handle>: A #CoglHandle to a program.
static Handle program_ref()(Handle handle) nothrow {
   return cogl_program_ref(handle);
}


// VERSION: 1.4
// Changes the value of a floating point uniform for the given linked
// @program.
// <program>: A #CoglHandle for a linked program
// <uniform_location>: the uniform location retrieved from cogl_program_get_uniform_location().
// <value>: the new value of the uniform.
static void program_set_uniform_1f()(Handle program, int uniform_location, float value) nothrow {
   cogl_program_set_uniform_1f(program, uniform_location, value);
}


// VERSION: 1.4
// Changes the value of an integer uniform for the given linked
// @program.
// <program>: A #CoglHandle for a linked program
// <uniform_location>: the uniform location retrieved from cogl_program_get_uniform_location().
// <value>: the new value of the uniform.
static void program_set_uniform_1i()(Handle program, int uniform_location, int value) nothrow {
   cogl_program_set_uniform_1i(program, uniform_location, value);
}


// VERSION: 1.4
// Changes the value of a float vector uniform, or uniform array for
// the given linked @program.
// <program>: A #CoglHandle for a linked program
// <uniform_location>: the uniform location retrieved from cogl_program_get_uniform_location().
// <n_components>: The number of components for the uniform. For example with glsl you'd use 3 for a vec3 or 4 for a vec4.
// <count>: For uniform arrays this is the array length otherwise just pass 1
// <value>: the new value of the uniform[s].
static void program_set_uniform_float(AT0)(Handle program, int uniform_location, int n_components, int count, AT0 /*float*/ value) nothrow {
   cogl_program_set_uniform_float(program, uniform_location, n_components, count, UpCast!(float*)(value));
}


// VERSION: 1.4
// Changes the value of a int vector uniform, or uniform array for
// the given linked @program.
// <program>: A #CoglHandle for a linked program
// <uniform_location>: the uniform location retrieved from cogl_program_get_uniform_location().
// <n_components>: The number of components for the uniform. For example with glsl you'd use 3 for a vec3 or 4 for a vec4.
// <count>: For uniform arrays this is the array length otherwise just pass 1
// <value>: the new value of the uniform[s].
static void program_set_uniform_int()(Handle program, int uniform_location, int n_components, int count, int* value) nothrow {
   cogl_program_set_uniform_int(program, uniform_location, n_components, count, value);
}


// VERSION: 1.4
// Changes the value of a matrix uniform, or uniform array in the
// given linked @program.
// <program>: A #CoglHandle for a linked program
// <uniform_location>: the uniform location retrieved from cogl_program_get_uniform_location().
// <dimensions>: The dimensions of the matrix. So for for example pass 2 for a 2x2 matrix or 3 for 3x3.
// <count>: For uniform arrays this is the array length otherwise just pass 1
// <transpose>: Whether to transpose the matrix when setting the uniform.
// <value>: the new value of the uniform.
static void program_set_uniform_matrix(AT0)(Handle program, int uniform_location, int dimensions, int count, int transpose, AT0 /*float*/ value) nothrow {
   cogl_program_set_uniform_matrix(program, uniform_location, dimensions, count, transpose, UpCast!(float*)(value));
}


// DEPRECATED (v1.4) function: program_uniform_1f - Use cogl_program_set_uniform_1f() instead.
// Changes the value of a floating point uniform in the currently
// used (see cogl_program_use()) shader program.
// <uniform_no>: the uniform to set.
// <value>: the new value of the uniform.
static void program_uniform_1f()(int uniform_no, float value) nothrow {
   cogl_program_uniform_1f(uniform_no, value);
}


// DEPRECATED (v1.4) function: program_uniform_1i - Use cogl_program_set_uniform_1i() instead.
// Changes the value of an integer uniform in the currently
// used (see cogl_program_use()) shader program.
// <uniform_no>: the uniform to set.
// <value>: the new value of the uniform.
static void program_uniform_1i()(int uniform_no, int value) nothrow {
   cogl_program_uniform_1i(uniform_no, value);
}


// DEPRECATED (v1.4) function: program_uniform_float - Use cogl_program_set_uniform_float() instead.
// Changes the value of a float vector uniform, or uniform array in the
// currently used (see cogl_program_use()) shader program.
// <uniform_no>: the uniform to set.
// <size>: Size of float vector.
// <count>: Size of array of uniforms.
// <value>: the new value of the uniform.
static void program_uniform_float(AT0)(int uniform_no, int size, int count, AT0 /*float*/ value) nothrow {
   cogl_program_uniform_float(uniform_no, size, count, UpCast!(float*)(value));
}


// Changes the value of a int vector uniform, or uniform array in the
// currently used (see cogl_program_use()) shader program.
// <uniform_no>: the uniform to set.
// <size>: Size of int vector.
// <count>: Size of array of uniforms.
// <value>: the new value of the uniform.
static void program_uniform_int()(int uniform_no, int size, int count, int* value) nothrow {
   cogl_program_uniform_int(uniform_no, size, count, value);
}


// Changes the value of a matrix uniform, or uniform array in the
// currently used (see cogl_program_use()) shader program. The @size
// parameter is used to determine the square size of the matrix.
// <uniform_no>: the uniform to set.
// <size>: Size of matrix.
// <count>: Size of array of uniforms.
// <transpose>: Whether to transpose the matrix when setting the uniform.
// <value>: the new value of the uniform.
static void program_uniform_matrix(AT0)(int uniform_no, int size, int count, int transpose, AT0 /*float*/ value) nothrow {
   cogl_program_uniform_matrix(uniform_no, size, count, transpose, UpCast!(float*)(value));
}


// DEPRECATED (v1.0) function: program_unref - Please use cogl_handle_unref() instead.
// Removes a reference to a program. If it was the last reference the
// program object will be destroyed.
// <handle>: A #CoglHandle to a program.
static void program_unref()(Handle handle) nothrow {
   cogl_program_unref(handle);
}


// Activate a specific shader program replacing that part of the GL
// rendering pipeline, if passed in %COGL_INVALID_HANDLE the default
// behavior of GL is reinstated.
// 
// This function affects the global state of the current Cogl
// context. It is much more efficient to attach the shader to a
// specific material used for rendering instead by calling
// cogl_material_set_user_program().
// <handle>: a #CoglHandle for a shader program or %COGL_INVALID_HANDLE.
static void program_use()(Handle handle) nothrow {
   cogl_program_use(handle);
}


// DEPRECATED (v1.2) function: push_draw_buffer - The draw buffer API was replaced with a framebuffer API
// Save cogl_set_draw_buffer() state.
static void push_draw_buffer()() nothrow {
   cogl_push_draw_buffer();
}


// VERSION: 1.2
// Redirects all subsequent drawing to the specified framebuffer. This can
// either be an offscreen buffer created with cogl_offscreen_new_to_texture ()
// or in the future it may be an onscreen framebuffer too.
// 
// You should understand that a framebuffer owns the following state:
// <itemizedlist>
// <listitem><simpara>The projection matrix</simpara></listitem>
// <listitem><simpara>The modelview matrix stack</simpara></listitem>
// <listitem><simpara>The viewport</simpara></listitem>
// <listitem><simpara>The clip stack</simpara></listitem>
// </itemizedlist>
// So these items will automatically be saved and restored when you
// push and pop between different framebuffers.
// 
// Also remember a newly allocated framebuffer will have an identity matrix for
// the projection and modelview matrices which gives you a coordinate space
// like OpenGL with (-1, -1) corresponding to the top left of the viewport,
// (1, 1) corresponding to the bottom right and +z coming out towards the
// viewer.
// 
// If you want to set up a coordinate space like Clutter does with (0, 0)
// corresponding to the top left and (framebuffer_width, framebuffer_height)
// corresponding to the bottom right you can do so like this:
// 
// |[
// static void
// setup_viewport (unsigned int width,
// unsigned int height,
// float fovy,
// float aspect,
// float z_near,
// float z_far)
// {
// float z_camera;
// CoglMatrix projection_matrix;
// CoglMatrix mv_matrix;
// 
// cogl_set_viewport (0, 0, width, height);
// cogl_perspective (fovy, aspect, z_near, z_far);
// 
// cogl_get_projection_matrix (&amp;projection_matrix);
// z_camera = 0.5 * projection_matrix.xx;
// 
// cogl_matrix_init_identity (&amp;mv_matrix);
// cogl_matrix_translate (&amp;mv_matrix, -0.5f, -0.5f, -z_camera);
// cogl_matrix_scale (&amp;mv_matrix, 1.0f / width, -1.0f / height, 1.0f / width);
// cogl_matrix_translate (&amp;mv_matrix, 0.0f, -1.0 * height, 0.0f);
// cogl_set_modelview_matrix (&amp;mv_matrix);
// }
// 
// static void
// my_init_framebuffer (ClutterStage *stage,
// CoglFramebuffer *framebuffer,
// unsigned int framebuffer_width,
// unsigned int framebuffer_height)
// {
// ClutterPerspective perspective;
// 
// clutter_stage_get_perspective (stage, &perspective);
// 
// cogl_push_framebuffer (framebuffer);
// setup_viewport (framebuffer_width,
// framebuffer_height,
// perspective.fovy,
// perspective.aspect,
// perspective.z_near,
// perspective.z_far);
// }
// ]|
// 
// The previous framebuffer can be restored by calling cogl_pop_framebuffer()
// <buffer>: A #CoglFramebuffer object, either onscreen or offscreen.
static void push_framebuffer(AT0)(AT0 /*Framebuffer*/ buffer) nothrow {
   cogl_push_framebuffer(UpCast!(Framebuffer*)(buffer));
}


// Stores the current model-view matrix on the matrix stack. The matrix
// can later be restored with cogl_pop_matrix().
static void push_matrix()() nothrow {
   cogl_push_matrix();
}


// VERSION: 1.6
// Pushes the given @material to the top of the source stack. The
// material at the top of this stack defines the GPU state used to
// process later primitives as defined by cogl_set_source().
// <material>: A #CoglMaterial
static void push_source(AT0)(AT0 /*void*/ material) nothrow {
   cogl_push_source(UpCast!(void*)(material));
}


// VERSION: 2.0
// MOVED TO: Quaternion.equal
// Compares that all the components of quaternions @a and @b are
// equal.
// 
// An epsilon value is not used to compare the float components, but
// the == operator is at least used so that 0 and -0 are considered
// equal.
// RETURNS: %TRUE if the quaternions are equal else %FALSE.
// <v1>: A #CoglQuaternion
// <v2>: A #CoglQuaternion
static int quaternion_equal(AT0, AT1)(AT0 /*const(void)*/ v1, AT1 /*const(void)*/ v2) nothrow {
   return cogl_quaternion_equal(UpCast!(const(void)*)(v1), UpCast!(const(void)*)(v2));
}


// This reads a rectangle of pixels from the current framebuffer where
// position (0, 0) is the top left. The pixel at (x, y) is the first
// read, and the data is returned with a rowstride of (width * 4).
// 
// Currently Cogl assumes that the framebuffer is in a premultiplied
// format so if @format is non-premultiplied it will convert it. To
// read the pixel values without any conversion you should either
// specify a format that doesn't use an alpha channel or use one of
// the formats ending in PRE.
// <x>: The window x position to start reading from
// <y>: The window y position to start reading from
// <width>: The width of the rectangle you want to read
// <height>: The height of the rectangle you want to read
// <source>: Identifies which auxillary buffer you want to read (only COGL_READ_PIXELS_COLOR_BUFFER supported currently)
// <format>: The pixel format you want the result in (only COGL_PIXEL_FORMAT_RGBA_8888 supported currently)
// <pixels>: The location to write the pixel data.
static void read_pixels(AT0)(int x, int y, int width, int height, ReadPixelsFlags source, PixelFormat format, AT0 /*ubyte*/ pixels) nothrow {
   cogl_read_pixels(x, y, width, height, source, format, UpCast!(ubyte*)(pixels));
}


// Fills a rectangle at the given coordinates with the current source material
// <x_1>: X coordinate of the top-left corner
// <y_1>: Y coordinate of the top-left corner
// <x_2>: X coordinate of the bottom-right corner
// <y_2>: Y coordinate of the bottom-right corner
static void rectangle()(float x_1, float y_1, float x_2, float y_2) nothrow {
   cogl_rectangle(x_1, y_1, x_2, y_2);
}


// VERSION: 1.0
// This function draws a rectangle using the current source material to
// texture or fill with. As a material may contain multiple texture layers
// this interface lets you supply texture coordinates for each layer of the
// material.
// 
// The first pair of coordinates are for the first layer (with the smallest
// layer index) and if you supply less texture coordinates than there are
// layers in the current source material then default texture coordinates
// (0.0, 0.0, 1.0, 1.0) are generated.
// <x1>: x coordinate upper left on screen.
// <y1>: y coordinate upper left on screen.
// <x2>: x coordinate lower right on screen.
// <y2>: y coordinate lower right on screen.
// <tex_coords>: An array containing groups of 4 float values: [tx1, ty1, tx2, ty2] that are interpreted as two texture coordinates; one for the upper left texel, and one for the lower right texel. Each value should be between 0.0 and 1.0, where the coordinate (0.0, 0.0) represents the top left of the texture, and (1.0, 1.0) the bottom right.
// <tex_coords_len>: The length of the tex_coords array. (e.g. for one layer and one group of texture coordinates, this would be 4)
static void rectangle_with_multitexture_coords(AT0)(float x1, float y1, float x2, float y2, AT0 /*float*/ tex_coords, int tex_coords_len) nothrow {
   cogl_rectangle_with_multitexture_coords(x1, y1, x2, y2, UpCast!(float*)(tex_coords), tex_coords_len);
}


// VERSION: 1.0
// Draw a rectangle using the current material and supply texture coordinates
// to be used for the first texture layer of the material. To draw the entire
// texture pass in @tx1=0.0 @ty1=0.0 @tx2=1.0 @ty2=1.0.
// <x1>: x coordinate upper left on screen.
// <y1>: y coordinate upper left on screen.
// <x2>: x coordinate lower right on screen.
// <y2>: y coordinate lower right on screen.
// <tx1>: x part of texture coordinate to use for upper left pixel
// <ty1>: y part of texture coordinate to use for upper left pixel
// <tx2>: x part of texture coordinate to use for lower right pixel
// <ty2>: y part of texture coordinate to use for left pixel
static void rectangle_with_texture_coords()(float x1, float y1, float x2, float y2, float tx1, float ty1, float tx2, float ty2) nothrow {
   cogl_rectangle_with_texture_coords(x1, y1, x2, y2, tx1, ty1, tx2, ty2);
}


// VERSION: 1.0
// Draws a series of rectangles in the same way that
// cogl_rectangle() does. In some situations it can give a
// significant performance boost to use this function rather than
// calling cogl_rectangle() separately for each rectangle.
// 
// @verts should point to an array of #float<!-- -->s with
// @n_rects * 4 elements. Each group of 4 values corresponds to the
// parameters x1, y1, x2, and y2, and have the same
// meaning as in cogl_rectangle().
// <verts>: an array of vertices
// <n_rects>: number of rectangles to draw
static void rectangles(AT0)(AT0 /*float*/ verts, uint n_rects) nothrow {
   cogl_rectangles(UpCast!(float*)(verts), n_rects);
}


// VERSION: 0.8.6
// Draws a series of rectangles in the same way that
// cogl_rectangle_with_texture_coords() does. In some situations it can give a
// significant performance boost to use this function rather than
// calling cogl_rectangle_with_texture_coords() separately for each rectangle.
// 
// @verts should point to an array of #float<!-- -->s with
// @n_rects * 8 elements. Each group of 8 values corresponds to the
// parameters x1, y1, x2, y2, tx1, ty1, tx2 and ty2 and have the same
// meaning as in cogl_rectangle_with_texture_coords().
// <verts>: an array of vertices
// <n_rects>: number of rectangles to draw
static void rectangles_with_texture_coords(AT0)(AT0 /*float*/ verts, uint n_rects) nothrow {
   cogl_rectangles_with_texture_coords(UpCast!(float*)(verts), n_rects);
}

// MOVED TO: RendererError.quark
static GLib2.Quark renderer_error_quark()() nothrow {
   return cogl_renderer_error_quark();
}


// Multiplies the current model-view matrix by one that rotates the
// model around the vertex specified by @x, @y and @z. The rotation
// follows the right-hand thumb rule so for example rotating by 10
// degrees about the vertex (0, 0, 1) causes a small counter-clockwise
// rotation.
// <angle>: Angle in degrees to rotate.
// <x>: X-component of vertex to rotate around.
// <y>: Y-component of vertex to rotate around.
// <z>: Z-component of vertex to rotate around.
static void rotate()(float angle, float x, float y, float z) nothrow {
   cogl_rotate(angle, x, y, z);
}


// Multiplies the current model-view matrix by one that scales the x,
// y and z axes by the given values.
// <x>: Amount to scale along the x-axis
// <y>: Amount to scale along the y-axis
// <z>: Amount to scale along the z-axis
static void scale()(float x, float y, float z) nothrow {
   cogl_scale(x, y, z);
}


// Sets whether textures positioned so that their backface is showing
// should be hidden. This can be used to efficiently draw two-sided
// textures or fully closed cubes without enabling depth testing. This
// only affects calls to the cogl_rectangle* family of functions and
// cogl_vertex_buffer_draw*. Backface culling is disabled by default.
// <setting>: %TRUE to enable backface culling or %FALSE to disable.
static void set_backface_culling_enabled()(int setting) nothrow {
   cogl_set_backface_culling_enabled(setting);
}


// DEPRECATED (v1.4) function: set_depth_test_enabled - Use cogl_material_set_depth_test_enabled()
// Sets whether depth testing is enabled. If it is disabled then the
// order that actors are layered on the screen depends solely on the
// order specified using clutter_actor_raise() and
// clutter_actor_lower(), otherwise it will also take into account the
// actor's depth. Depth testing is disabled by default.
// 
// instead.
// <setting>: %TRUE to enable depth testing or %FALSE to disable.
static void set_depth_test_enabled()(int setting) nothrow {
   cogl_set_depth_test_enabled(setting);
}


// DEPRECATED (v1.2) function: set_draw_buffer - The target argument was redundant since we could look at
// Redirects all subsequent drawing to the specified framebuffer. This
// can either be an offscreen buffer created with
// cogl_offscreen_new_to_texture () or you can revert to your original
// on screen window buffer.
// 
// the type of CoglHandle given instead.
// <target>: A #CoglBufferTarget that specifies what kind of framebuffer you are setting as the render target.
// <offscreen>: If you are setting a framebuffer of type COGL_OFFSCREEN_BUFFER then this is a CoglHandle for the offscreen buffer.
static void set_draw_buffer()(BufferTarget target, Handle offscreen) nothrow {
   cogl_set_draw_buffer(target, offscreen);
}


// Enables fogging. Fogging causes vertices that are further away from the eye
// to be rendered with a different color. The color is determined according to
// the chosen fog mode; at it's simplest the color is linearly interpolated so
// that vertices at @z_near are drawn fully with their original color and
// vertices at @z_far are drawn fully with @fog_color. Fogging will remain
// enabled until you call cogl_disable_fog().
// 
// <note>The fogging functions only work correctly when primitives use
// unmultiplied alpha colors. By default Cogl will premultiply textures
// and cogl_set_source_color() will premultiply colors, so unless you
// explicitly load your textures requesting an unmultiplied internal format
// and use cogl_material_set_color() you can only use fogging with fully
// opaque primitives. This might improve in the future when we can depend
// on fragment shaders.</note>
// <fog_color>: The color of the fog
// <mode>: A #CoglFogMode that determines the equation used to calculate the fogging blend factor.
// <density>: Used by %COGL_FOG_MODE_EXPONENTIAL and by %COGL_FOG_MODE_EXPONENTIAL_SQUARED equations.
// <z_near>: Position along Z axis where no fogging should be applied
// <z_far>: Position along Z axis where full fogging should be applied
static void set_fog(AT0)(AT0 /*Color*/ fog_color, FogMode mode, float density, float z_near, float z_far) nothrow {
   cogl_set_fog(UpCast!(Color*)(fog_color), mode, density, z_near, z_far);
}


// VERSION: 1.2
// This redirects all subsequent drawing to the specified framebuffer. This can
// either be an offscreen buffer created with cogl_offscreen_new_to_texture ()
// or in the future it may be an onscreen framebuffers too.
// <buffer>: A #CoglFramebuffer object, either onscreen or offscreen.
static void set_framebuffer(AT0)(AT0 /*Framebuffer*/ buffer) nothrow {
   cogl_set_framebuffer(UpCast!(Framebuffer*)(buffer));
}


// Loads @matrix as the new model-view matrix.
// <matrix>: the new model-view matrix
static void set_modelview_matrix(AT0)(AT0 /*Matrix*/ matrix) nothrow {
   cogl_set_modelview_matrix(UpCast!(Matrix*)(matrix));
}


// Unintrospectable function: set_path() / cogl_set_path()
// VERSION: 1.4
// Replaces the current path with @path. A reference is taken on the
// object so if you no longer need the path you should unref with
// cogl_object_unref().
// <path>: A #CoglPath object
static void set_path(AT0)(AT0 /*Path*/ path) nothrow {
   cogl_set_path(UpCast!(Path*)(path));
}


// VERSION: 1.10
// Sets @matrix as the new projection matrix.
// <matrix>: the new projection matrix
static void set_projection_matrix(AT0)(AT0 /*Matrix*/ matrix) nothrow {
   cogl_set_projection_matrix(UpCast!(Matrix*)(matrix));
}


// VERSION: 1.0
// This function changes the material at the top of the source stack.
// The material at the top of this stack defines the GPU state used to
// process subsequent primitives, such as rectangles drawn with
// cogl_rectangle() or vertices drawn using cogl_vertex_buffer_draw().
// <material>: A #CoglMaterial
static void set_source(AT0)(AT0 /*void*/ material) nothrow {
   cogl_set_source(UpCast!(void*)(material));
}


// VERSION: 1.0
// This is a convenience function for creating a solid fill source material
// from the given color. This color will be used for any subsequent drawing
// operation.
// 
// The color will be premultiplied by Cogl, so the color should be
// non-premultiplied. For example: use (1.0, 0.0, 0.0, 0.5) for
// semi-transparent red.
// 
// See also cogl_set_source_color4ub() and cogl_set_source_color4f()
// if you already have the color components.
// <color>: a #CoglColor
static void set_source_color(AT0)(AT0 /*Color*/ color) nothrow {
   cogl_set_source_color(UpCast!(Color*)(color));
}


// VERSION: 1.0
// This is a convenience function for creating a solid fill source material
// from the given color using normalized values for each component. This color
// will be used for any subsequent drawing operation.
// 
// The value for each component is a fixed point number in the range
// between 0 and %1.0. If the values passed in are outside that
// range, they will be clamped.
// <red>: value of the red channel, between 0 and %1.0
// <green>: value of the green channel, between 0 and %1.0
// <blue>: value of the blue channel, between 0 and %1.0
// <alpha>: value of the alpha channel, between 0 and %1.0
static void set_source_color4f()(float red, float green, float blue, float alpha) nothrow {
   cogl_set_source_color4f(red, green, blue, alpha);
}


// VERSION: 1.0
// This is a convenience function for creating a solid fill source material
// from the given color using unsigned bytes for each component. This
// color will be used for any subsequent drawing operation.
// 
// The value for each component is an unsigned byte in the range
// between 0 and 255.
// <red>: value of the red channel, between 0 and 255
// <green>: value of the green channel, between 0 and 255
// <blue>: value of the blue channel, between 0 and 255
// <alpha>: value of the alpha channel, between 0 and 255
static void set_source_color4ub()(ubyte red, ubyte green, ubyte blue, ubyte alpha) nothrow {
   cogl_set_source_color4ub(red, green, blue, alpha);
}


// VERSION: 1.0
// This is a convenience function for creating a material with the first
// layer set to @texture and setting that material as the source with
// cogl_set_source.
// 
// Note: There is no interaction between calls to cogl_set_source_color
// and cogl_set_source_texture. If you need to blend a texture with a color then
// you can create a simple material like this:
// <programlisting>
// material = cogl_material_new ();
// cogl_material_set_color4ub (material, 0xff, 0x00, 0x00, 0x80);
// cogl_material_set_layer (material, 0, tex_handle);
// cogl_set_source (material);
// </programlisting>
// <texture>: The #CoglTexture you want as your source
static void set_source_texture(AT0)(AT0 /*Texture*/ texture) nothrow {
   cogl_set_source_texture(UpCast!(Texture*)(texture));
}


// VERSION: 1.2
// Replaces the current viewport with the given values.
// <x>: X offset of the viewport
// <y>: Y offset of the viewport
// <width>: Width of the viewport
// <height>: Height of the viewport
static void set_viewport()(int x, int y, int width, int height) nothrow {
   cogl_set_viewport(x, y, width, height);
}


// Compiles the shader, no return value, but the shader is now ready
// for linking into a program. Note that calling this function is
// optional. If it is not called then the shader will be automatically
// compiled when it is linked.
// <handle>: #CoglHandle for a shader.
static void shader_compile()(Handle handle) nothrow {
   cogl_shader_compile(handle);
}


// Retrieves the information log for a coglobject, can be used in conjunction
// with cogl_shader_get_parameteriv() to retrieve the compiler warnings/error
// messages that caused a shader to not compile correctly, mainly useful for
// debugging purposes.
// 
// g_free() to free it
// RETURNS: a newly allocated string containing the info log. Use
// <handle>: #CoglHandle for a shader.
static char* /*new*/ shader_get_info_log()(Handle handle) nothrow {
   return cogl_shader_get_info_log(handle);
}


// Retrieves the type of a shader #CoglHandle
// 
// or %COGL_SHADER_TYPE_FRAGMENT if the shader is a frament processor
// RETURNS: %COGL_SHADER_TYPE_VERTEX if the shader is a vertex processor
// <handle>: #CoglHandle for a shader.
static ShaderType shader_get_type()(Handle handle) nothrow {
   return cogl_shader_get_type(handle);
}


// Retrieves whether a shader #CoglHandle has been compiled
// RETURNS: %TRUE if the shader object has sucessfully be compiled
// <handle>: #CoglHandle for a shader.
static int shader_is_compiled()(Handle handle) nothrow {
   return cogl_shader_is_compiled(handle);
}


// Unintrospectable function: shader_ref() / cogl_shader_ref()
// DEPRECATED (v1.0) function: shader_ref - Please use cogl_handle_ref() instead.
// Add an extra reference to a shader.
// RETURNS: @handle
// <handle>: A #CoglHandle to a shader.
static Handle shader_ref()(Handle handle) nothrow {
   return cogl_shader_ref(handle);
}


// Replaces the current source associated with a shader with a new
// one.
// 
// Please see <link
// linkend="cogl-Shaders-and-Programmable-Pipeline.description">above</link>
// for a description of the recommended format for the shader code.
// <shader>: #CoglHandle for a shader.
// <source>: Shader source.
static void shader_source(AT0)(Handle shader, AT0 /*char*/ source) nothrow {
   cogl_shader_source(shader, toCString!(char*)(source));
}


// DEPRECATED (v1.0) function: shader_unref - Please use cogl_handle_unref() instead.
// Removes a reference to a shader. If it was the last reference the
// shader object will be destroyed.
// <handle>: A #CoglHandle to a shader.
static void shader_unref()(Handle handle) nothrow {
   cogl_shader_unref(handle);
}


// VERSION: 1.0
// Very fast fixed point implementation of square root for integers.
// 
// This function is at least 6x faster than clib sqrt() on x86, and (this is
// not a typo!) about 500x faster on ARM without FPU. It's error is less than
// 5% for arguments smaller than %COGL_SQRTI_ARG_5_PERCENT and less than 10%
// for narguments smaller than %COGL_SQRTI_ARG_10_PERCENT. The maximum
// argument that can be passed to this function is %COGL_SQRTI_ARG_MAX.
// RETURNS: integer square root.
// <x>: integer value
static int sqrti()(int x) nothrow {
   return cogl_sqrti(x);
}

// MOVED TO: TextureError.quark
static GLib2.Quark texture_error_quark()() nothrow {
   return cogl_texture_error_quark();
}


// DEPRECATED (v1.2) function: texture_unref - Use cogl_object_unref() instead
// MOVED TO: Texture.unref
// Decrement the reference count for a cogl texture.
// <texture>: a #CoglTexture.
static void texture_unref(AT0)(AT0 /*void*/ texture) nothrow {
   cogl_texture_unref(UpCast!(void*)(texture));
}


// VERSION: 1.4
// Multiplies the current model-view matrix by the given matrix.
// <matrix>: the matrix to multiply with the current model-view
static void transform(AT0)(AT0 /*Matrix*/ matrix) nothrow {
   cogl_transform(UpCast!(Matrix*)(matrix));
}


// Multiplies the current model-view matrix by one that translates the
// model along all three axes according to the given values.
// <x>: Distance to translate along the x-axis
// <y>: Distance to translate along the y-axis
// <z>: Distance to translate along the z-axis
static void translate()(float x, float y, float z) nothrow {
   cogl_translate(x, y, z);
}

// Unintrospectable function: vdraw_attributes() / cogl_vdraw_attributes()
alias cogl_vdraw_attributes vdraw_attributes; // Variadic

// Unintrospectable function: vdraw_indexed_attributes() / cogl_vdraw_indexed_attributes()
alias cogl_vdraw_indexed_attributes vdraw_indexed_attributes; // Variadic


// VERSION: 1.4
// Adds each of the corresponding components in vectors @a and @b
// storing the results in @result.
// <result>: Where you want the result written
// <a>: The first vector operand
// <b>: The second vector operand
static void vector3_add(AT0, AT1, AT2)(AT0 /*float*/ result, AT1 /*float*/ a, AT2 /*float*/ b) nothrow {
   cogl_vector3_add(UpCast!(float*)(result), UpCast!(float*)(a), UpCast!(float*)(b));
}


// VERSION: 1.4
// Allocates a new 3 component float vector on the heap initializing
// the components from the given @vector and returns a pointer to the
// newly allocated vector. You should free the memory using
// cogl_vector3_free()
// RETURNS: A newly allocated 3 component float vector
// <vector>: The 3 component vector you want to copy
static float* vector3_copy(AT0)(AT0 /*float*/ vector) nothrow {
   return cogl_vector3_copy(UpCast!(float*)(vector));
}


// VERSION: 1.4
// Calculates the cross product between the two vectors @u and @v.
// 
// The cross product is a vector perpendicular to both @u and @v. This
// can be useful for calculating the normal of a polygon by creating
// two vectors in its plane using the polygons vertices and taking
// their cross product.
// 
// If the two vectors are parallel then the cross product is 0.
// 
// You can use a right hand rule to determine which direction the
// perpendicular vector will point: If you place the two vectors tail,
// to tail and imagine grabbing the perpendicular line that extends
// through the common tail with your right hand such that you fingers
// rotate in the direction from @u to @v then the resulting vector
// points along your extended thumb.
// RETURNS: The cross product between two vectors @u and @v.
// <result>: Where you want the result written
// <u>: Your first 3 component vector
// <v>: Your second 3 component vector
static void vector3_cross_product(AT0, AT1, AT2)(AT0 /*float*/ result, AT1 /*float*/ u, AT2 /*float*/ v) nothrow {
   cogl_vector3_cross_product(UpCast!(float*)(result), UpCast!(float*)(u), UpCast!(float*)(v));
}


// VERSION: 1.4
// If you consider the two given vectors as (x,y,z) points instead
// then this will compute the distance between those two points.
// 
// vectors.
// RETURNS: The distance between two points given as 3 component
// <a>: The first point
// <b>: The second point
static float vector3_distance(AT0, AT1)(AT0 /*float*/ a, AT1 /*float*/ b) nothrow {
   return cogl_vector3_distance(UpCast!(float*)(a), UpCast!(float*)(b));
}


// VERSION: 1.4
// Divides each of the @vector components by the given scalar.
// <vector>: The 3 component vector you want to manipulate
// <scalar>: The scalar you want to divide the vector components by
static void vector3_divide_scalar(AT0)(AT0 /*float*/ vector, float scalar) nothrow {
   cogl_vector3_divide_scalar(UpCast!(float*)(vector), scalar);
}


// VERSION: 1.4
// Calculates the dot product of the two 3 component vectors. This
// can be used to determine the magnitude of one vector projected onto
// another. (for example a surface normal)
// 
// For example if you have a polygon with a given normal vector and
// some other point for which you want to calculate its distance from
// the polygon, you can create a vector between one of the polygon
// vertices and that point and use the dot product to calculate the
// magnitude for that vector but projected onto the normal of the
// polygon. This way you don't just get the distance from the point to
// the edge of the polygon you get the distance from the point to the
// nearest part of the polygon.
// 
// <note>If you don't use a unit length normal in the above example
// then you would then also have to divide the result by the magnitude
// of the normal</note>
// 
// The dot product is calculated as:
// |[
// (a->x * b->x + a->y * b->y + a->z * b->z)
// ]|
// 
// For reference, the dot product can also be calculated from the
// angle between two vectors as:
// |[
// |a||b|cos𝜃
// ]|
// RETURNS: The dot product of two vectors.
// <a>: Your first 3 component vector
// <b>: Your second 3 component vector
static float vector3_dot_product(AT0, AT1)(AT0 /*float*/ a, AT1 /*float*/ b) nothrow {
   return cogl_vector3_dot_product(UpCast!(float*)(a), UpCast!(float*)(b));
}


// VERSION: 1.4
// Compares the components of two vectors and returns TRUE if they are
// the same.
// 
// The comparison of the components is done with the '==' operator
// such that -0 is considered equal to 0, but otherwise there is no
// fuzziness such as an epsilon to consider vectors that are
// essentially identical except for some minor precision error
// differences due to the way they have been manipulated.
// RETURNS: TRUE if the vectors are equal else FALSE.
// <v1>: The first 3 component vector you want to compare
// <v2>: The second 3 component vector you want to compare
static int vector3_equal(AT0, AT1)(AT0 /*const(void)*/ v1, AT1 /*const(void)*/ v2) nothrow {
   return cogl_vector3_equal(UpCast!(const(void)*)(v1), UpCast!(const(void)*)(v2));
}


// VERSION: 1.4
// Compares the components of two vectors using the given epsilon and
// returns TRUE if they are the same, using an internal epsilon for
// comparing the floats.
// 
// Each component is compared against the epsilon value in this way:
// |[
// if (fabsf (vector0->x - vector1->x) < epsilon)
// ]|
// RETURNS: TRUE if the vectors are equal else FALSE.
// <vector0>: The first 3 component vector you want to compare
// <vector1>: The second 3 component vector you want to compare
// <epsilon>: The allowable difference between components to still be considered equal
static int vector3_equal_with_epsilon(AT0, AT1)(AT0 /*float*/ vector0, AT1 /*float*/ vector1, float epsilon) nothrow {
   return cogl_vector3_equal_with_epsilon(UpCast!(float*)(vector0), UpCast!(float*)(vector1), epsilon);
}


// VERSION: 1.4
// Frees a 3 component vector that was previously allocated with
// cogl_vector_copy()
// <vector>: The 3 component you want to free
static void vector3_free(AT0)(AT0 /*float*/ vector) nothrow {
   cogl_vector3_free(UpCast!(float*)(vector));
}


// VERSION: 1.4
// Initializes a 3 component, single precision float vector which can
// then be manipulated with the cogl_vector convenience APIs. Vectors
// can also be used in places where a "point" is often desired.
// <vector>: The 3 component vector you want to initialize
// <x>: The x component
// <y>: The y component
// <z>: The z component
static void vector3_init(AT0)(AT0 /*float*/ vector, float x, float y, float z) nothrow {
   cogl_vector3_init(UpCast!(float*)(vector), x, y, z);
}


// VERSION: 1.4
// Initializes a 3 component, single precision float vector with zero
// for each component.
// <vector>: The 3 component vector you want to initialize
static void vector3_init_zero(AT0)(AT0 /*float*/ vector) nothrow {
   cogl_vector3_init_zero(UpCast!(float*)(vector));
}


// VERSION: 1.4
// Inverts/negates all the components of the given @vector.
// <vector>: The 3 component vector you want to manipulate
static void vector3_invert(AT0)(AT0 /*float*/ vector) nothrow {
   cogl_vector3_invert(UpCast!(float*)(vector));
}


// VERSION: 1.4
// Calculates the scalar magnitude or length of @vector.
// RETURNS: The magnitude of @vector.
// <vector>: The 3 component vector you want the magnitude for
static float vector3_magnitude(AT0)(AT0 /*float*/ vector) nothrow {
   return cogl_vector3_magnitude(UpCast!(float*)(vector));
}


// VERSION: 1.4
// Multiplies each of the @vector components by the given scalar.
// <vector>: The 3 component vector you want to manipulate
// <scalar>: The scalar you want to multiply the vector components by
static void vector3_multiply_scalar(AT0)(AT0 /*float*/ vector, float scalar) nothrow {
   cogl_vector3_multiply_scalar(UpCast!(float*)(vector), scalar);
}


// VERSION: 1.4
// Updates the vector so it is a "unit vector" such that the
// @vector<!-- -->s magnitude or length is equal to 1.
// <vector>: The 3 component vector you want to manipulate
static void vector3_normalize(AT0)(AT0 /*float*/ vector) nothrow {
   cogl_vector3_normalize(UpCast!(float*)(vector));
}


// VERSION: 1.4
// Subtracts each of the corresponding components in vector @b from
// @a storing the results in @result.
// <result>: Where you want the result written
// <a>: The first vector operand
// <b>: The second vector operand
static void vector3_subtract(AT0, AT1, AT2)(AT0 /*float*/ result, AT1 /*float*/ a, AT2 /*float*/ b) nothrow {
   cogl_vector3_subtract(UpCast!(float*)(result), UpCast!(float*)(a), UpCast!(float*)(b));
}


// Adds an attribute to a buffer, or replaces a previously added
// attribute with the same name.
// 
// You either can use one of the built-in names such as "gl_Vertex", or
// "gl_MultiTexCoord0" to add standard attributes, like positions, colors
// and normals, or you can add custom attributes for use in shaders.
// 
// The number of vertices declared when calling cogl_vertex_buffer_new()
// determines how many attribute values will be read from the supplied
// @pointer.
// 
// The data for your attribute isn't copied anywhere until you call
// cogl_vertex_buffer_submit(), or issue a draw call which automatically
// submits pending attribute changes. so the supplied pointer must remain
// valid until then. If you are updating an existing attribute (done by
// re-adding it) then you still need to re-call cogl_vertex_buffer_submit()
// to commit the changes to the GPU. Be carefull to minimize the number
// of calls to cogl_vertex_buffer_submit(), though.
// 
// <note>If you are interleving attributes it is assumed that each interleaved
// attribute starts no farther than +- stride bytes from the other attributes
// it is interleved with. I.e. this is ok:
// <programlisting>
// |-0-0-0-0-0-0-0-0-0-0|
// </programlisting>
// This is not ok:
// <programlisting>
// |- - - - -0-0-0-0-0-0 0 0 0 0|
// </programlisting>
// (Though you can have multiple groups of interleved attributes)</note>
// <handle>: A vertex buffer handle
// <attribute_name>: The name of your attribute. It should be a valid GLSL variable name and standard attribute types must use one of following built-in names: (Note: they correspond to the built-in names of GLSL) <itemizedlist> <listitem>"gl_Color"</listitem> <listitem>"gl_Normal"</listitem> <listitem>"gl_MultiTexCoord0, gl_MultiTexCoord1, ..."</listitem> <listitem>"gl_Vertex"</listitem> </itemizedlist> To support adding multiple variations of the same attribute the name can have a detail component, E.g. "gl_Color::active" or "gl_Color::inactive"
// <n_components>: The number of components per attribute and must be 1, 2, 3 or 4
// <type>: a #CoglAttributeType specifying the data type of each component.
// <normalized>: If %TRUE, this specifies that values stored in an integer format should be mapped into the range [-1.0, 1.0] or [0.0, 1.0] for unsigned values. If %FALSE they are converted to floats directly.
// <stride>: This specifies the number of bytes from the start of one attribute value to the start of the next value (for the same attribute). So, for example, with a position interleved with color like this: XYRGBAXYRGBAXYRGBA, then if each letter represents a byte, the stride for both attributes is 6. The special value 0 means the values are stored sequentially in memory.
// <pointer>: This addresses the first attribute in the vertex array. This must remain valid until you either call cogl_vertex_buffer_submit() or issue a draw call.
static void vertex_buffer_add(AT0, AT1)(Handle handle, AT0 /*char*/ attribute_name, ubyte n_components, AttributeType type, int normalized, ushort stride, AT1 /*void*/ pointer) nothrow {
   cogl_vertex_buffer_add(handle, toCString!(char*)(attribute_name), n_components, type, normalized, stride, UpCast!(void*)(pointer));
}


// Deletes an attribute from a buffer. You will need to call
// cogl_vertex_buffer_submit() or issue a draw call to commit this
// change to the GPU.
// <handle>: A vertex buffer handle
// <attribute_name>: The name of a previously added attribute
static void vertex_buffer_delete(AT0)(Handle handle, AT0 /*char*/ attribute_name) nothrow {
   cogl_vertex_buffer_delete(handle, toCString!(char*)(attribute_name));
}


// Disables a previosuly added attribute.
// 
// Since it can be costly to add and remove new attributes to buffers; to make
// individual buffers more reuseable it is possible to enable and disable
// attributes before using a buffer for drawing.
// 
// You don't need to call cogl_vertex_buffer_submit() after using this
// function.
// <handle>: A vertex buffer handle
// <attribute_name>: The name of the attribute you want to disable
static void vertex_buffer_disable(AT0)(Handle handle, AT0 /*char*/ attribute_name) nothrow {
   cogl_vertex_buffer_disable(handle, toCString!(char*)(attribute_name));
}


// Allows you to draw geometry using all or a subset of the
// vertices in a vertex buffer.
// 
// Any un-submitted attribute changes are automatically submitted before
// drawing.
// <handle>: A vertex buffer handle
// <mode>: A #CoglVerticesMode specifying how the vertices should be interpreted.
// <first>: Specifies the index of the first vertex you want to draw with
// <count>: Specifies the number of vertices you want to draw.
static void vertex_buffer_draw()(Handle handle, VerticesMode mode, int first, int count) nothrow {
   cogl_vertex_buffer_draw(handle, mode, first, count);
}


// This function lets you use an array of indices to specify the vertices
// within your vertex buffer that you want to draw. The indices themselves
// are created by calling cogl_vertex_buffer_indices_new ()
// 
// Any un-submitted attribute changes are automatically submitted before
// drawing.
// <handle>: A vertex buffer handle
// <mode>: A #CoglVerticesMode specifying how the vertices should be interpreted.
// <indices>: A CoglHandle for a set of indices allocated via cogl_vertex_buffer_indices_new ()
// <min_index>: Specifies the minimum vertex index contained in indices
// <max_index>: Specifies the maximum vertex index contained in indices
// <indices_offset>: An offset into named indices. The offset marks the first index to use for drawing.
// <count>: Specifies the number of vertices you want to draw.
static void vertex_buffer_draw_elements()(Handle handle, VerticesMode mode, Handle indices, int min_index, int max_index, int indices_offset, int count) nothrow {
   cogl_vertex_buffer_draw_elements(handle, mode, indices, min_index, max_index, indices_offset, count);
}


// Enables a previosuly disabled attribute.
// 
// Since it can be costly to add and remove new attributes to buffers; to make
// individual buffers more reuseable it is possible to enable and disable
// attributes before using a buffer for drawing.
// 
// You don't need to call cogl_vertex_buffer_submit() after using this function
// <handle>: A vertex buffer handle
// <attribute_name>: The name of the attribute you want to enable
static void vertex_buffer_enable(AT0)(Handle handle, AT0 /*char*/ attribute_name) nothrow {
   cogl_vertex_buffer_enable(handle, toCString!(char*)(attribute_name));
}


// Retrieves the number of vertices that @handle represents
// RETURNS: the number of vertices
// <handle>: A vertex buffer handle
static uint vertex_buffer_get_n_vertices()(Handle handle) nothrow {
   return cogl_vertex_buffer_get_n_vertices(handle);
}


// Unintrospectable function: vertex_buffer_indices_get_for_quads() / cogl_vertex_buffer_indices_get_for_quads()
// Creates a vertex buffer containing the indices needed to draw pairs
// of triangles from a list of vertices grouped as quads. There will
// be at least @n_indices entries in the buffer (but there may be
// more).
// 
// The indices will follow this pattern:
// 
// 0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7 ... etc
// 
// For example, if you submit vertices for a quad like like that shown
// in <xref linkend="quad-indices-order"/> then you can request 6
// indices to render two triangles like those shown in <xref
// linkend="quad-indices-triangles"/>.
// 
// <figure id="quad-indices-order">
// <title>Example of vertices submitted to form a quad</title>
// <graphic fileref="quad-indices-order.png" format="PNG"/>
// </figure>
// 
// <figure id="quad-indices-triangles">
// <title>Illustration of the triangle indices that will be generated</title>
// <graphic fileref="quad-indices-triangles.png" format="PNG"/>
// </figure>
// 
// owned by Cogl and should not be modified or unref'd.
// RETURNS: A %CoglHandle containing the indices. The handled is
// <n_indices>: the number of indices in the vertex buffer.
static Handle vertex_buffer_indices_get_for_quads()(uint n_indices) nothrow {
   return cogl_vertex_buffer_indices_get_for_quads(n_indices);
}


// Queries back the data type used for the given indices
// RETURNS: The CoglIndicesType used
// <indices>: An indices handle
static IndicesType vertex_buffer_indices_get_type()(Handle indices) nothrow {
   return cogl_vertex_buffer_indices_get_type(indices);
}


// Unintrospectable function: vertex_buffer_indices_new() / cogl_vertex_buffer_indices_new()
// Depending on how much geometry you are submitting it can be worthwhile
// optimizing the number of redundant vertices you submit. Using an index
// array allows you to reference vertices multiple times, for example
// during triangle strips.
// 
// cogl_vertex_buffer_draw_elements().
// RETURNS: A CoglHandle for the indices which you can pass to
// <indices_type>: a #CoglIndicesType specifying the data type used for the indices.
// <indices_array>: Specifies the address of your array of indices
// <indices_len>: The number of indices in indices_array
static Handle vertex_buffer_indices_new(AT0)(IndicesType indices_type, AT0 /*void*/ indices_array, int indices_len) nothrow {
   return cogl_vertex_buffer_indices_new(indices_type, UpCast!(void*)(indices_array), indices_len);
}


// Unintrospectable function: vertex_buffer_new() / cogl_vertex_buffer_new()
// Creates a new vertex buffer that you can use to add attributes.
// RETURNS: a new #CoglHandle
// <n_vertices>: The number of vertices that your attributes will correspond to.
static Handle vertex_buffer_new()(uint n_vertices) nothrow {
   return cogl_vertex_buffer_new(n_vertices);
}


// Unintrospectable function: vertex_buffer_ref() / cogl_vertex_buffer_ref()
// DEPRECATED (v1.2) function: vertex_buffer_ref - Use cogl_handle_ref() instead
// Increment the reference count for a vertex buffer
// RETURNS: the @handle.
// <handle>: a @CoglHandle.
static Handle vertex_buffer_ref()(Handle handle) nothrow {
   return cogl_vertex_buffer_ref(handle);
}


// Submits all the user added attributes to the GPU; once submitted, the
// attributes can be used for drawing.
// 
// You should aim to minimize calls to this function since it implies
// validating your data; it potentially incurs a transport cost (especially if
// you are using GLX indirect rendering) and potentially a format conversion
// cost if the GPU doesn't natively support any of the given attribute formats.
// <handle>: A vertex buffer handle
static void vertex_buffer_submit()(Handle handle) nothrow {
   cogl_vertex_buffer_submit(handle);
}


// DEPRECATED (v1.2) function: vertex_buffer_unref - Use cogl_handle_unref() instead
// Decrement the reference count for a vertex buffer
// <handle>: a @CoglHandle.
static void vertex_buffer_unref()(Handle handle) nothrow {
   cogl_vertex_buffer_unref(handle);
}


// VERSION: 0.8.2
// DEPRECATED (v1.2) function: viewport - Use cogl_set_viewport() instead
// Replace the current viewport with the given values.
// <width>: Width of the viewport
// <height>: Height of the viewport
static void viewport()(uint width, uint height) nothrow {
   cogl_viewport(width, height);
}

static uint x11_onscreen_get_visual_xid(AT0)(AT0 /*Onscreen*/ onscreen) nothrow {
   return cogl_x11_onscreen_get_visual_xid(UpCast!(Onscreen*)(onscreen));
}


// VERSION: 1.10
// Assuming you know the given @onscreen framebuffer is based on an x11 window
// this queries the XID of that window. If
// cogl_x11_onscreen_set_foreign_window_xid() was previously called then it
// will return that same XID otherwise it will be the XID of a window Cogl
// created internally. If the window has not been allocated yet and a foreign
// xid has not been set then it's undefined what value will be returned.
// 
// It's undefined what this function does if called when not using an x11 based
// renderer.
// <onscreen>: A #CoglOnscreen framebuffer
static uint x11_onscreen_get_window_xid(AT0)(AT0 /*Onscreen*/ onscreen) nothrow {
   return cogl_x11_onscreen_get_window_xid(UpCast!(Onscreen*)(onscreen));
}


// Unintrospectable function: x11_onscreen_set_foreign_window_xid() / cogl_x11_onscreen_set_foreign_window_xid()
// VERSION: 2.0
// Ideally we would recommend that you let Cogl be responsible for
// creating any X window required to back an onscreen framebuffer but
// if you really need to target a window created manually this
// function can be called before @onscreen has been allocated to set a
// foreign XID for your existing X window.
// 
// Since Cogl needs, for example, to track changes to the size of an X
// window it requires that certain events be selected for via the core
// X protocol. This requirement may also be changed asynchronously so
// you must pass in an @update callback to inform you of Cogl's
// required event mask.
// 
// For example if you are using Xlib you could use this API roughly
// as follows:
// [{
// static void
// my_update_cogl_x11_event_mask (CoglOnscreen *onscreen,
// guint32 event_mask,
// void *user_data)
// {
// XSetWindowAttributes attrs;
// MyData *data = user_data;
// attrs.event_mask = event_mask | data->my_event_mask;
// XChangeWindowAttributes (data->xdpy,
// data->xwin,
// CWEventMask,
// &attrs);
// }
// 
// {
// *snip*
// cogl_x11_onscreen_set_foreign_window_xid (onscreen,
// data->xwin,
// my_update_cogl_x11_event_mask,
// data);
// *snip*
// }
// }]
// <onscreen>: The unallocated framebuffer to associated with an X window.
// <xid>: The XID of an existing X window
// <update>: A callback that notifies of updates to what Cogl requires to be in the core X protocol event mask.
static void x11_onscreen_set_foreign_window_xid(AT0, AT1)(AT0 /*Onscreen*/ onscreen, uint xid, OnscreenX11MaskCallback update, AT1 /*void*/ user_data) nothrow {
   cogl_x11_onscreen_set_foreign_window_xid(UpCast!(Onscreen*)(onscreen), xid, update, UpCast!(void*)(user_data));
}

// Unintrospectable function: xlib_renderer_add_filter_EXP() / cogl_xlib_renderer_add_filter_EXP()
static void xlib_renderer_add_filter_EXP(AT0, AT1)(AT0 /*Cogl.Renderer*/ renderer, XlibFilterFunc func, AT1 /*void*/ data) nothrow {
   cogl_xlib_renderer_add_filter_EXP(UpCast!(Cogl.Renderer*)(renderer), func, UpCast!(void*)(data));
}

// Unintrospectable function: xlib_renderer_get_display_EXP() / cogl_xlib_renderer_get_display_EXP()
static /*CTYPE*/ Display* xlib_renderer_get_display_EXP(AT0)(AT0 /*Cogl.Renderer*/ renderer) nothrow {
   return cogl_xlib_renderer_get_display_EXP(UpCast!(Cogl.Renderer*)(renderer));
}

// Unintrospectable function: xlib_renderer_get_foreign_display_EXP() / cogl_xlib_renderer_get_foreign_display_EXP()
static /*CTYPE*/ Display* xlib_renderer_get_foreign_display_EXP(AT0)(AT0 /*Cogl.Renderer*/ renderer) nothrow {
   return cogl_xlib_renderer_get_foreign_display_EXP(UpCast!(Cogl.Renderer*)(renderer));
}

// Unintrospectable function: xlib_renderer_handle_event_EXP() / cogl_xlib_renderer_handle_event_EXP()
static FilterReturn xlib_renderer_handle_event_EXP(AT0, AT1)(AT0 /*Cogl.Renderer*/ renderer, AT1 /*XEvent*/ event) nothrow {
   return cogl_xlib_renderer_handle_event_EXP(UpCast!(Cogl.Renderer*)(renderer), UpCast!(XEvent*)(event));
}

// Unintrospectable function: xlib_renderer_remove_filter_EXP() / cogl_xlib_renderer_remove_filter_EXP()
static void xlib_renderer_remove_filter_EXP(AT0, AT1)(AT0 /*Cogl.Renderer*/ renderer, XlibFilterFunc func, AT1 /*void*/ data) nothrow {
   cogl_xlib_renderer_remove_filter_EXP(UpCast!(Cogl.Renderer*)(renderer), func, UpCast!(void*)(data));
}


// VERSION: 1.10
// Sets whether Cogl should automatically retrieve events from the X
// display. This defaults to %TRUE unless
// cogl_xlib_renderer_set_foreign_display() is called. It can be set
// to %FALSE if the application wants to handle its own event
// retrieval. Note that Cogl still needs to see all of the X events to
// function properly so the application should call
// cogl_xlib_renderer_handle_event() for each event if it disables
// automatic event retrieval.
// <renderer>: A #CoglRenderer
// <enable>: The new value
static void xlib_renderer_set_event_retrieval_enabled(AT0)(AT0 /*Cogl.Renderer*/ renderer, int enable) nothrow {
   cogl_xlib_renderer_set_event_retrieval_enabled(UpCast!(Cogl.Renderer*)(renderer), enable);
}

// Unintrospectable function: xlib_renderer_set_foreign_display_EXP() / cogl_xlib_renderer_set_foreign_display_EXP()
static void xlib_renderer_set_foreign_display_EXP(AT0, AT1)(AT0 /*Cogl.Renderer*/ renderer, AT1 /*Display*/ display) nothrow {
   cogl_xlib_renderer_set_foreign_display_EXP(UpCast!(Cogl.Renderer*)(renderer), UpCast!(Display*)(display));
}


// C prototypes:

extern (C) {
AttributeBuffer* cogl_attribute_get_buffer(Attribute* this_) nothrow;
int cogl_attribute_get_normalized(Attribute* this_) nothrow;
void cogl_attribute_set_buffer(Attribute* this_, AttributeBuffer* attribute_buffer) nothrow;
void cogl_attribute_set_normalized(Attribute* this_, int normalized) nothrow;
Attribute* cogl_attribute_new(AttributeBuffer* attribute_buffer, char* name, size_t stride, size_t offset, int components, AttributeType type) nothrow;
AttributeBuffer* cogl_attribute_buffer_new(size_t bytes, void* data) nothrow;
int cogl_bitmap_get_size_from_file(char* filename, /*out*/ int* width, /*out*/ int* height) nothrow;
Bitmap* cogl_bitmap_new_from_buffer(Buffer* buffer, PixelFormat format, int width, int height, int rowstride, int offset) nothrow;
Bitmap* cogl_bitmap_new_from_file(char* filename, GLib2.Error** error) nothrow;
uint cogl_buffer_get_size(Buffer* this_) nothrow;
BufferUpdateHint cogl_buffer_get_update_hint(Buffer* this_) nothrow;
void* cogl_buffer_map(Buffer* this_, BufferAccess access, BufferMapHint hints) nothrow;
int cogl_buffer_set_data(Buffer* this_, size_t offset, void* data, size_t size) nothrow;
void cogl_buffer_set_update_hint(Buffer* this_, BufferUpdateHint hint) nothrow;
void cogl_buffer_unmap(Buffer* this_) nothrow;
Color* cogl_color_copy(Color* this_) nothrow;
void cogl_color_free(Color* this_) nothrow;
float cogl_color_get_alpha(Color* this_) nothrow;
ubyte cogl_color_get_alpha_byte(Color* this_) nothrow;
float cogl_color_get_alpha_float(Color* this_) nothrow;
float cogl_color_get_blue(Color* this_) nothrow;
ubyte cogl_color_get_blue_byte(Color* this_) nothrow;
float cogl_color_get_blue_float(Color* this_) nothrow;
float cogl_color_get_green(Color* this_) nothrow;
ubyte cogl_color_get_green_byte(Color* this_) nothrow;
float cogl_color_get_green_float(Color* this_) nothrow;
float cogl_color_get_red(Color* this_) nothrow;
ubyte cogl_color_get_red_byte(Color* this_) nothrow;
float cogl_color_get_red_float(Color* this_) nothrow;
void cogl_color_init_from_4f(Color* this_, float red, float green, float blue, float alpha) nothrow;
void cogl_color_init_from_4fv(Color* this_, float* color_array) nothrow;
void cogl_color_init_from_4ub(Color* this_, ubyte red, ubyte green, ubyte blue, ubyte alpha) nothrow;
void cogl_color_premultiply(Color* this_) nothrow;
void cogl_color_set_alpha(Color* this_, float alpha) nothrow;
void cogl_color_set_alpha_byte(Color* this_, ubyte alpha) nothrow;
void cogl_color_set_alpha_float(Color* this_, float alpha) nothrow;
void cogl_color_set_blue(Color* this_, float blue) nothrow;
void cogl_color_set_blue_byte(Color* this_, ubyte blue) nothrow;
void cogl_color_set_blue_float(Color* this_, float blue) nothrow;
void cogl_color_set_from_4f(Color* this_, float red, float green, float blue, float alpha) nothrow;
void cogl_color_set_from_4ub(Color* this_, ubyte red, ubyte green, ubyte blue, ubyte alpha) nothrow;
void cogl_color_set_green(Color* this_, float green) nothrow;
void cogl_color_set_green_byte(Color* this_, ubyte green) nothrow;
void cogl_color_set_green_float(Color* this_, float green) nothrow;
void cogl_color_set_red(Color* this_, float red) nothrow;
void cogl_color_set_red_byte(Color* this_, ubyte red) nothrow;
void cogl_color_set_red_float(Color* this_, float red) nothrow;
void cogl_color_unpremultiply(Color* this_) nothrow;
int cogl_color_equal(const(void)* v1, const(void)* v2) nothrow;
Color* cogl_color_new() nothrow;
Cogl.Display* cogl_context_get_display(Context* this_) nothrow;
Cogl.Context* /*new*/ cogl_context_new(Cogl.Display* display, GLib2.Error** error) nothrow;
void cogl_depth_state_get_range(DepthState* this_, float* near_val, float* far_val) nothrow;
int cogl_depth_state_get_test_enabled(DepthState* this_) nothrow;
DepthTestFunction cogl_depth_state_get_test_function(DepthState* this_) nothrow;
int cogl_depth_state_get_write_enabled(DepthState* this_) nothrow;
void cogl_depth_state_init(DepthState* this_) nothrow;
void cogl_depth_state_set_range(DepthState* this_, float near_val, float far_val) nothrow;
void cogl_depth_state_set_test_enabled(DepthState* this_, int enable) nothrow;
void cogl_depth_state_set_test_function(DepthState* this_, DepthTestFunction function_) nothrow;
void cogl_depth_state_set_write_enabled(DepthState* this_, int enable) nothrow;
Cogl.Renderer* cogl_display_get_renderer_EXP(Display* this_) nothrow;
int cogl_display_setup_EXP(Display* this_, GLib2.Error** error) nothrow;
Cogl.Display* cogl_display_new_EXP(Cogl.Renderer* renderer, OnscreenTemplate* onscreen_template) nothrow;
Euler* cogl_euler_copy(Euler* this_) nothrow;
void cogl_euler_free(Euler* this_) nothrow;
void cogl_euler_init(Euler* this_, float heading, float pitch, float roll) nothrow;
void cogl_euler_init_from_matrix(Euler* this_, Matrix* matrix) nothrow;
void cogl_euler_init_from_quaternion(Euler* this_, Quaternion* quaternion) nothrow;
int cogl_euler_equal(const(void)* v1, const(void)* v2) nothrow;
Fixed cogl_fixed_log2(uint x) nothrow;
uint cogl_fixed_pow(uint x, Fixed y) nothrow;
Fixed cogl_fixed_atan(Fixed* this_) nothrow;
Fixed cogl_fixed_atan2(Fixed* this_, Fixed b) nothrow;
Fixed cogl_fixed_cos(Fixed* this_) nothrow;
Fixed cogl_fixed_div(Fixed* this_, Fixed b) nothrow;
Fixed cogl_fixed_mul(Fixed* this_, Fixed b) nothrow;
Fixed cogl_fixed_mul_div(Fixed* this_, Fixed b, Fixed c) nothrow;
uint cogl_fixed_pow2(Fixed* this_) nothrow;
Fixed cogl_fixed_sin(Fixed* this_) nothrow;
Fixed cogl_fixed_sqrt(Fixed* this_) nothrow;
Fixed cogl_fixed_tan(Fixed* this_) nothrow;
uint cogl_framebuffer_add_swap_buffers_callback(Framebuffer* this_, SwapBuffersNotify callback, void* user_data) nothrow;
int cogl_framebuffer_allocate(Framebuffer* this_, GLib2.Error** error) nothrow;
void cogl_framebuffer_clear(Framebuffer* this_, c_ulong buffers, Color* color) nothrow;
void cogl_framebuffer_clear4f(Framebuffer* this_, c_ulong buffers, float red, float green, float blue, float alpha) nothrow;
void cogl_framebuffer_discard_buffers(Framebuffer* this_, c_ulong buffers) nothrow;
void cogl_framebuffer_finish(Framebuffer* this_) nothrow;
void cogl_framebuffer_frustum(Framebuffer* this_, float left, float right, float bottom, float top, float z_near, float z_far) nothrow;
int cogl_framebuffer_get_alpha_bits(Framebuffer* this_) nothrow;
int cogl_framebuffer_get_blue_bits(Framebuffer* this_) nothrow;
PixelFormat cogl_framebuffer_get_color_format(Framebuffer* this_) nothrow;
ColorMask cogl_framebuffer_get_color_mask(Framebuffer* this_) nothrow;
Cogl.Context* cogl_framebuffer_get_context(Framebuffer* this_) nothrow;
int cogl_framebuffer_get_dither_enabled(Framebuffer* this_) nothrow;
int cogl_framebuffer_get_green_bits(Framebuffer* this_) nothrow;
int cogl_framebuffer_get_height(Framebuffer* this_) nothrow;
void cogl_framebuffer_get_modelview_matrix(Framebuffer* this_, /*out*/ Matrix* matrix) nothrow;
void cogl_framebuffer_get_projection_matrix(Framebuffer* this_, /*out*/ Matrix* matrix) nothrow;
int cogl_framebuffer_get_red_bits(Framebuffer* this_) nothrow;
int cogl_framebuffer_get_samples_per_pixel(Framebuffer* this_) nothrow;
void cogl_framebuffer_get_viewport4fv(Framebuffer* this_, float* viewport) nothrow;
float cogl_framebuffer_get_viewport_height(Framebuffer* this_) nothrow;
float cogl_framebuffer_get_viewport_width(Framebuffer* this_) nothrow;
float cogl_framebuffer_get_viewport_x(Framebuffer* this_) nothrow;
float cogl_framebuffer_get_viewport_y(Framebuffer* this_) nothrow;
int cogl_framebuffer_get_width(Framebuffer* this_) nothrow;
void cogl_framebuffer_identity_matrix(Framebuffer* this_) nothrow;
void cogl_framebuffer_orthographic(Framebuffer* this_, float x_1, float y_1, float x_2, float y_2, float near, float far) nothrow;
void cogl_framebuffer_perspective(Framebuffer* this_, float fov_y, float aspect, float z_near, float z_far) nothrow;
void cogl_framebuffer_pop_clip(Framebuffer* this_) nothrow;
void cogl_framebuffer_pop_matrix(Framebuffer* this_) nothrow;
void cogl_framebuffer_push_matrix(Framebuffer* this_) nothrow;
void cogl_framebuffer_push_path_clip(Framebuffer* this_, Path* path) nothrow;
void cogl_framebuffer_push_primitive_clip(Framebuffer* this_, Primitive* primitive, float bounds_x1, float bounds_y1, float bounds_x2, float bounds_y2) nothrow;
void cogl_framebuffer_push_rectangle_clip(Framebuffer* this_, float x_1, float y_1, float x_2, float y_2) nothrow;
void cogl_framebuffer_push_scissor_clip(Framebuffer* this_, int x, int y, int width, int height) nothrow;
void cogl_framebuffer_remove_swap_buffers_callback(Framebuffer* this_, uint id) nothrow;
void cogl_framebuffer_resolve_samples(Framebuffer* this_) nothrow;
void cogl_framebuffer_resolve_samples_region(Framebuffer* this_, int x, int y, int width, int height) nothrow;
void cogl_framebuffer_rotate(Framebuffer* this_, float angle, float x, float y, float z) nothrow;
void cogl_framebuffer_scale(Framebuffer* this_, float x, float y, float z) nothrow;
void cogl_framebuffer_set_color_mask(Framebuffer* this_, ColorMask color_mask) nothrow;
void cogl_framebuffer_set_dither_enabled(Framebuffer* this_, int dither_enabled) nothrow;
void cogl_framebuffer_set_modelview_matrix(Framebuffer* this_, Matrix* matrix) nothrow;
void cogl_framebuffer_set_projection_matrix(Framebuffer* this_, Matrix* matrix) nothrow;
void cogl_framebuffer_set_samples_per_pixel(Framebuffer* this_, int samples_per_pixel) nothrow;
void cogl_framebuffer_set_viewport(Framebuffer* this_, float x, float y, float width, float height) nothrow;
void cogl_framebuffer_swap_buffers(Framebuffer* this_) nothrow;
void cogl_framebuffer_swap_region(Framebuffer* this_, int* rectangles, int n_rectangles) nothrow;
void cogl_framebuffer_transform(Framebuffer* this_, Matrix* matrix) nothrow;
void cogl_framebuffer_translate(Framebuffer* this_, float x, float y, float z) nothrow;
IndexBuffer* cogl_index_buffer_new(size_t bytes) nothrow;
IndexBuffer* cogl_indices_get_buffer(Indices* this_) nothrow;
size_t cogl_indices_get_offset(Indices* this_) nothrow;
void cogl_indices_set_offset(Indices* this_, size_t offset) nothrow;
Indices* cogl_indices_new(IndicesType type, void* indices_data, int n_indices) nothrow;
Indices* cogl_indices_new_for_buffer(IndicesType type, IndexBuffer* buffer, size_t offset) nothrow;
Material* cogl_material_copy(Material* this_) nothrow;
void cogl_material_foreach_layer(Material* this_, MaterialLayerCallback callback, void* user_data) nothrow;
void cogl_material_get_ambient(Material* this_, Color* ambient) nothrow;
void cogl_material_get_color(Material* this_, /*out*/ Color* color) nothrow;
void cogl_material_get_depth_state(Material* this_, DepthState* state_out) nothrow;
void cogl_material_get_diffuse(Material* this_, Color* diffuse) nothrow;
void cogl_material_get_emission(Material* this_, Color* emission) nothrow;
int cogl_material_get_layer_point_sprite_coords_enabled(Material* this_, int layer_index) nothrow;
MaterialWrapMode cogl_material_get_layer_wrap_mode_p(Material* this_, int layer_index) nothrow;
MaterialWrapMode cogl_material_get_layer_wrap_mode_s(Material* this_, int layer_index) nothrow;
MaterialWrapMode cogl_material_get_layer_wrap_mode_t(Material* this_, int layer_index) nothrow;
GLib2.List* cogl_material_get_layers(Material* this_) nothrow;
int cogl_material_get_n_layers(Material* this_) nothrow;
float cogl_material_get_point_size(Material* this_) nothrow;
float cogl_material_get_shininess(Material* this_) nothrow;
void cogl_material_get_specular(Material* this_, Color* specular) nothrow;
Handle cogl_material_get_user_program(Material* this_) nothrow;
void cogl_material_remove_layer(Material* this_, int layer_index) nothrow;
void cogl_material_set_alpha_test_function(Material* this_, MaterialAlphaFunc alpha_func, float alpha_reference) nothrow;
void cogl_material_set_ambient(Material* this_, Color* ambient) nothrow;
void cogl_material_set_ambient_and_diffuse(Material* this_, Color* color) nothrow;
int cogl_material_set_blend(Material* this_, char* blend_string, GLib2.Error** error) nothrow;
void cogl_material_set_blend_constant(Material* this_, Color* constant_color) nothrow;
void cogl_material_set_color(Material* this_, Color* color) nothrow;
void cogl_material_set_color4f(Material* this_, float red, float green, float blue, float alpha) nothrow;
void cogl_material_set_color4ub(Material* this_, ubyte red, ubyte green, ubyte blue, ubyte alpha) nothrow;
int cogl_material_set_depth_state(Material* this_, DepthState* state, GLib2.Error** error) nothrow;
void cogl_material_set_diffuse(Material* this_, Color* diffuse) nothrow;
void cogl_material_set_emission(Material* this_, Color* emission) nothrow;
void cogl_material_set_layer(Material* this_, int layer_index, Handle texture) nothrow;
int cogl_material_set_layer_combine(Material* this_, int layer_index, char* blend_string, GLib2.Error** error) nothrow;
void cogl_material_set_layer_combine_constant(Material* this_, int layer_index, Color* constant) nothrow;
void cogl_material_set_layer_filters(Material* this_, int layer_index, MaterialFilter min_filter, MaterialFilter mag_filter) nothrow;
void cogl_material_set_layer_matrix(Material* this_, int layer_index, Matrix* matrix) nothrow;
int cogl_material_set_layer_point_sprite_coords_enabled(Material* this_, int layer_index, int enable, GLib2.Error** error) nothrow;
void cogl_material_set_layer_wrap_mode(Material* this_, int layer_index, MaterialWrapMode mode) nothrow;
void cogl_material_set_layer_wrap_mode_p(Material* this_, int layer_index, MaterialWrapMode mode) nothrow;
void cogl_material_set_layer_wrap_mode_s(Material* this_, int layer_index, MaterialWrapMode mode) nothrow;
void cogl_material_set_layer_wrap_mode_t(Material* this_, int layer_index, MaterialWrapMode mode) nothrow;
void cogl_material_set_point_size(Material* this_, float point_size) nothrow;
void cogl_material_set_shininess(Material* this_, float shininess) nothrow;
void cogl_material_set_specular(Material* this_, Color* specular) nothrow;
void cogl_material_set_user_program(Material* this_, Handle program) nothrow;
Material* cogl_material_new() nothrow;
Handle cogl_material_ref(Handle material) nothrow;
void cogl_material_unref(Handle material) nothrow;
MaterialFilter cogl_material_layer_get_mag_filter(MaterialLayer* this_) nothrow;
MaterialFilter cogl_material_layer_get_min_filter(MaterialLayer* this_) nothrow;
Handle cogl_material_layer_get_texture(MaterialLayer* this_) nothrow;
MaterialWrapMode cogl_material_layer_get_wrap_mode_p(MaterialLayer* this_) nothrow;
MaterialWrapMode cogl_material_layer_get_wrap_mode_s(MaterialLayer* this_) nothrow;
MaterialWrapMode cogl_material_layer_get_wrap_mode_t(MaterialLayer* this_) nothrow;
Matrix* cogl_matrix_copy(Matrix* this_) nothrow;
void cogl_matrix_free(Matrix* this_) nothrow;
void cogl_matrix_frustum(Matrix* this_, float left, float right, float bottom, float top, float z_near, float z_far) nothrow;
float* cogl_matrix_get_array(Matrix* this_) nothrow;
int cogl_matrix_get_inverse(Matrix* this_, /*out*/ Matrix* inverse) nothrow;
void cogl_matrix_init_from_array(Matrix* this_, float* array) nothrow;
void cogl_matrix_init_from_quaternion(Matrix* this_, Quaternion* quaternion) nothrow;
void cogl_matrix_init_identity(Matrix* this_) nothrow;
int cogl_matrix_is_identity(Matrix* this_) nothrow;
void cogl_matrix_look_at(Matrix* this_, float eye_position_x, float eye_position_y, float eye_position_z, float object_x, float object_y, float object_z, float world_up_x, float world_up_y, float world_up_z) nothrow;
void cogl_matrix_multiply(Matrix* this_, Matrix* a, Matrix* b) nothrow;
void cogl_matrix_ortho(Matrix* this_, float left, float right, float bottom, float top, float near, float far) nothrow;
void cogl_matrix_orthographic(Matrix* this_, float x_1, float y_1, float x_2, float y_2, float near, float far) nothrow;
void cogl_matrix_perspective(Matrix* this_, float fov_y, float aspect, float z_near, float z_far) nothrow;
void cogl_matrix_project_points(Matrix* this_, int n_components, size_t stride_in, void* points_in, size_t stride_out, void* points_out, int n_points) nothrow;
void cogl_matrix_rotate(Matrix* this_, float angle, float x, float y, float z) nothrow;
void cogl_matrix_scale(Matrix* this_, float sx, float sy, float sz) nothrow;
void cogl_matrix_transform_point(Matrix* this_, /*inout*/ float* x, /*inout*/ float* y, /*inout*/ float* z, /*inout*/ float* w) nothrow;
void cogl_matrix_transform_points(Matrix* this_, int n_components, size_t stride_in, void* points_in, size_t stride_out, void* points_out, int n_points) nothrow;
void cogl_matrix_translate(Matrix* this_, float x, float y, float z) nothrow;
void cogl_matrix_transpose(Matrix* this_) nothrow;
void cogl_matrix_view_2d_in_frustum(Matrix* this_, float left, float right, float bottom, float top, float z_near, float z_2d, float width_2d, float height_2d) nothrow;
void cogl_matrix_view_2d_in_perspective(Matrix* this_, float fov_y, float aspect, float z_near, float z_2d, float width_2d, float height_2d) nothrow;
int cogl_matrix_equal(const(void)* v1, const(void)* v2) nothrow;
void cogl_meta_texture_foreach_in_region(MetaTexture* this_, float tx_1, float ty_1, float tx_2, float ty_2, PipelineWrapMode wrap_s, PipelineWrapMode wrap_t, MetaTextureCallback callback, void* user_data) nothrow;
void* cogl_object_get_user_data(Object* this_, UserDataKey* key) nothrow;
void cogl_object_set_user_data(Object* this_, UserDataKey* key, void* user_data, UserDataDestroyCallback destroy) nothrow;
void* cogl_object_ref(void* object) nothrow;
void cogl_object_unref(void* object) nothrow;
void cogl_onscreen_hide(Onscreen* this_) nothrow;
void cogl_onscreen_set_swap_throttled(Onscreen* this_, int throttled) nothrow;
void cogl_onscreen_show(Onscreen* this_) nothrow;
void cogl_onscreen_clutter_backend_set_size_CLUTTER(int width, int height) nothrow;
Onscreen* cogl_onscreen_new(Cogl.Context* context, int width, int height) nothrow;
void cogl_onscreen_template_set_samples_per_pixel(OnscreenTemplate* this_, int n) nothrow;
void cogl_onscreen_template_set_swap_throttled(OnscreenTemplate* this_, int throttled) nothrow;
OnscreenTemplate* cogl_onscreen_template_new_EXP(SwapChain* swap_chain) nothrow;
Path* cogl_path_copy(Path* this_) nothrow;
void cogl_path_arc(float center_x, float center_y, float radius_x, float radius_y, float angle_1, float angle_2) nothrow;
void cogl_path_close() nothrow;
void cogl_path_curve_to(float x_1, float y_1, float x_2, float y_2, float x_3, float y_3) nothrow;
void cogl_path_ellipse(float center_x, float center_y, float radius_x, float radius_y) nothrow;
void cogl_path_fill() nothrow;
void cogl_path_fill_preserve() nothrow;
PathFillRule cogl_path_get_fill_rule() nothrow;
void cogl_path_line(float x_1, float y_1, float x_2, float y_2) nothrow;
void cogl_path_line_to(float x, float y) nothrow;
void cogl_path_move_to(float x, float y) nothrow;
void cogl_path_new() nothrow;
void cogl_path_polygon(float* coords, int num_points) nothrow;
void cogl_path_polyline(float* coords, int num_points) nothrow;
void cogl_path_rectangle(float x_1, float y_1, float x_2, float y_2) nothrow;
void cogl_path_rel_curve_to(float x_1, float y_1, float x_2, float y_2, float x_3, float y_3) nothrow;
void cogl_path_rel_line_to(float x, float y) nothrow;
void cogl_path_rel_move_to(float x, float y) nothrow;
void cogl_path_round_rectangle(float x_1, float y_1, float x_2, float y_2, float radius, float arc_step) nothrow;
void cogl_path_set_fill_rule(PathFillRule fill_rule) nothrow;
void cogl_path_stroke() nothrow;
void cogl_path_stroke_preserve() nothrow;
void cogl_pipeline_add_layer_snippet(Pipeline* this_, int layer, Snippet* snippet) nothrow;
void cogl_pipeline_add_snippet(Pipeline* this_, Snippet* snippet) nothrow;
Pipeline* cogl_pipeline_copy(Pipeline* this_) nothrow;
void cogl_pipeline_foreach_layer(Pipeline* this_, PipelineLayerCallback callback, void* user_data) nothrow;
PipelineAlphaFunc cogl_pipeline_get_alpha_test_function(Pipeline* this_) nothrow;
float cogl_pipeline_get_alpha_test_reference(Pipeline* this_) nothrow;
void cogl_pipeline_get_ambient(Pipeline* this_, Color* ambient) nothrow;
void cogl_pipeline_get_color(Pipeline* this_, /*out*/ Color* color) nothrow;
ColorMask cogl_pipeline_get_color_mask(Pipeline* this_) nothrow;
PipelineCullFaceMode cogl_pipeline_get_cull_face_mode(Pipeline* this_) nothrow;
void cogl_pipeline_get_depth_state(Pipeline* this_, DepthState* state_out) nothrow;
void cogl_pipeline_get_diffuse(Pipeline* this_, Color* diffuse) nothrow;
void cogl_pipeline_get_emission(Pipeline* this_, Color* emission) nothrow;
Winding cogl_pipeline_get_front_face_winding(Pipeline* this_) nothrow;
int cogl_pipeline_get_layer_point_sprite_coords_enabled(Pipeline* this_, int layer_index) nothrow;
PipelineWrapMode cogl_pipeline_get_layer_wrap_mode_p(Pipeline* this_, int layer_index) nothrow;
PipelineWrapMode cogl_pipeline_get_layer_wrap_mode_s(Pipeline* this_, int layer_index) nothrow;
PipelineWrapMode cogl_pipeline_get_layer_wrap_mode_t(Pipeline* this_, int layer_index) nothrow;
int cogl_pipeline_get_n_layers(Pipeline* this_) nothrow;
float cogl_pipeline_get_point_size(Pipeline* this_) nothrow;
float cogl_pipeline_get_shininess(Pipeline* this_) nothrow;
void cogl_pipeline_get_specular(Pipeline* this_, Color* specular) nothrow;
int cogl_pipeline_get_uniform_location(Pipeline* this_, char* uniform_name) nothrow;
Handle cogl_pipeline_get_user_program(Pipeline* this_) nothrow;
void cogl_pipeline_remove_layer(Pipeline* this_, int layer_index) nothrow;
void cogl_pipeline_set_alpha_test_function(Pipeline* this_, PipelineAlphaFunc alpha_func, float alpha_reference) nothrow;
void cogl_pipeline_set_ambient(Pipeline* this_, Color* ambient) nothrow;
void cogl_pipeline_set_ambient_and_diffuse(Pipeline* this_, Color* color) nothrow;
int cogl_pipeline_set_blend(Pipeline* this_, char* blend_string, GLib2.Error** error) nothrow;
void cogl_pipeline_set_blend_constant(Pipeline* this_, Color* constant_color) nothrow;
void cogl_pipeline_set_color(Pipeline* this_, Color* color) nothrow;
void cogl_pipeline_set_color4f(Pipeline* this_, float red, float green, float blue, float alpha) nothrow;
void cogl_pipeline_set_color4ub(Pipeline* this_, ubyte red, ubyte green, ubyte blue, ubyte alpha) nothrow;
void cogl_pipeline_set_color_mask(Pipeline* this_, ColorMask color_mask) nothrow;
void cogl_pipeline_set_cull_face_mode(Pipeline* this_, PipelineCullFaceMode cull_face_mode) nothrow;
int cogl_pipeline_set_depth_state(Pipeline* this_, DepthState* state, GLib2.Error** error) nothrow;
void cogl_pipeline_set_diffuse(Pipeline* this_, Color* diffuse) nothrow;
void cogl_pipeline_set_emission(Pipeline* this_, Color* emission) nothrow;
void cogl_pipeline_set_front_face_winding(Pipeline* this_, Winding front_winding) nothrow;
int cogl_pipeline_set_layer_combine(Pipeline* this_, int layer_index, char* blend_string, GLib2.Error** error) nothrow;
void cogl_pipeline_set_layer_combine_constant(Pipeline* this_, int layer_index, Color* constant) nothrow;
void cogl_pipeline_set_layer_filters(Pipeline* this_, int layer_index, PipelineFilter min_filter, PipelineFilter mag_filter) nothrow;
void cogl_pipeline_set_layer_matrix(Pipeline* this_, int layer_index, Matrix* matrix) nothrow;
int cogl_pipeline_set_layer_point_sprite_coords_enabled(Pipeline* this_, int layer_index, int enable, GLib2.Error** error) nothrow;
void cogl_pipeline_set_layer_texture(Pipeline* this_, int layer_index, Texture* texture) nothrow;
void cogl_pipeline_set_layer_wrap_mode(Pipeline* this_, int layer_index, PipelineWrapMode mode) nothrow;
void cogl_pipeline_set_layer_wrap_mode_p(Pipeline* this_, int layer_index, PipelineWrapMode mode) nothrow;
void cogl_pipeline_set_layer_wrap_mode_s(Pipeline* this_, int layer_index, PipelineWrapMode mode) nothrow;
void cogl_pipeline_set_layer_wrap_mode_t(Pipeline* this_, int layer_index, PipelineWrapMode mode) nothrow;
void cogl_pipeline_set_point_size(Pipeline* this_, float point_size) nothrow;
void cogl_pipeline_set_shininess(Pipeline* this_, float shininess) nothrow;
void cogl_pipeline_set_specular(Pipeline* this_, Color* specular) nothrow;
void cogl_pipeline_set_uniform_1f(Pipeline* this_, int uniform_location, float value) nothrow;
void cogl_pipeline_set_uniform_1i(Pipeline* this_, int uniform_location, int value) nothrow;
void cogl_pipeline_set_uniform_float(Pipeline* this_, int uniform_location, int n_components, int count, float* value) nothrow;
void cogl_pipeline_set_uniform_int(Pipeline* this_, int uniform_location, int n_components, int count, int* value) nothrow;
void cogl_pipeline_set_uniform_matrix(Pipeline* this_, int uniform_location, int dimensions, int count, int transpose, float* value) nothrow;
void cogl_pipeline_set_user_program(Pipeline* this_, Handle program) nothrow;
Pipeline* cogl_pipeline_new() nothrow;
PixelBuffer* cogl_pixel_buffer_new_with_size_EXP(uint width, uint height, PixelFormat format, uint* stride) nothrow;
void cogl_primitive_draw(Primitive* this_) nothrow;
int cogl_primitive_get_first_vertex(Primitive* this_) nothrow;
VerticesMode cogl_primitive_get_mode(Primitive* this_) nothrow;
int cogl_primitive_get_n_vertices_EXP(Primitive* this_) nothrow;
void cogl_primitive_set_attributes(Primitive* this_, Attribute** attributes, int n_attributes) nothrow;
void cogl_primitive_set_first_vertex(Primitive* this_, int first_vertex) nothrow;
void cogl_primitive_set_indices_EXP(Primitive* this_, Indices* indices, int n_indices) nothrow;
void cogl_primitive_set_mode(Primitive* this_, VerticesMode mode) nothrow;
void cogl_primitive_set_n_vertices_EXP(Primitive* this_, int n_vertices) nothrow;
Primitive* cogl_primitive_new(VerticesMode mode, int n_vertices, ...) nothrow;
Primitive* cogl_primitive_new_p2(VerticesMode mode, int n_vertices, VertexP2* data) nothrow;
Primitive* cogl_primitive_new_p2c4(VerticesMode mode, int n_vertices, VertexP2C4* data) nothrow;
Primitive* cogl_primitive_new_p2t2(VerticesMode mode, int n_vertices, VertexP2T2* data) nothrow;
Primitive* cogl_primitive_new_p2t2c4(VerticesMode mode, int n_vertices, VertexP2T2C4* data) nothrow;
Primitive* cogl_primitive_new_p3(VerticesMode mode, int n_vertices, VertexP3* data) nothrow;
Primitive* cogl_primitive_new_p3c4(VerticesMode mode, int n_vertices, VertexP3C4* data) nothrow;
Primitive* cogl_primitive_new_p3t2(VerticesMode mode, int n_vertices, VertexP3T2* data) nothrow;
Primitive* cogl_primitive_new_p3t2c4(VerticesMode mode, int n_vertices, VertexP3T2C4* data) nothrow;
Primitive* cogl_primitive_new_with_attributes(VerticesMode mode, int n_vertices, Attribute** attributes, int n_attributes) nothrow;
Quaternion* cogl_quaternion_copy(Quaternion* this_) nothrow;
float cogl_quaternion_dot_product(Quaternion* this_, Quaternion* b) nothrow;
void cogl_quaternion_free(Quaternion* this_) nothrow;
float cogl_quaternion_get_rotation_angle(Quaternion* this_) nothrow;
void cogl_quaternion_get_rotation_axis(Quaternion* this_, float* vector3) nothrow;
void cogl_quaternion_init(Quaternion* this_, float angle, float x, float y, float z) nothrow;
void cogl_quaternion_init_from_angle_vector(Quaternion* this_, float angle, float* axis3f) nothrow;
void cogl_quaternion_init_from_array(Quaternion* this_, float* array) nothrow;
void cogl_quaternion_init_from_euler(Quaternion* this_, Euler* euler) nothrow;
void cogl_quaternion_init_from_x_rotation(Quaternion* this_, float angle) nothrow;
void cogl_quaternion_init_from_y_rotation(Quaternion* this_, float angle) nothrow;
void cogl_quaternion_init_from_z_rotation(Quaternion* this_, float angle) nothrow;
void cogl_quaternion_init_identity(Quaternion* this_) nothrow;
void cogl_quaternion_invert(Quaternion* this_) nothrow;
void cogl_quaternion_multiply(Quaternion* this_, Quaternion* left, Quaternion* right) nothrow;
void cogl_quaternion_nlerp(Quaternion* this_, Quaternion* a, Quaternion* b, float t) nothrow;
void cogl_quaternion_normalize(Quaternion* this_) nothrow;
void cogl_quaternion_pow(Quaternion* this_, float exponent) nothrow;
void cogl_quaternion_slerp(Quaternion* this_, Quaternion* a, Quaternion* b, float t) nothrow;
void cogl_quaternion_squad(Quaternion* this_, Quaternion* prev, Quaternion* a, Quaternion* b, Quaternion* next, float t) nothrow;
int cogl_quaternion_equal(const(void)* v1, const(void)* v2) nothrow;
void cogl_renderer_add_contraint(Renderer* this_, RendererConstraint constraint) nothrow;
int cogl_renderer_check_onscreen_template(Renderer* this_, OnscreenTemplate* onscreen_template, GLib2.Error** error) nothrow;
int cogl_renderer_connect(Renderer* this_, GLib2.Error** error) nothrow;
int cogl_renderer_get_n_fragment_texture_units(Renderer* this_) nothrow;
WinsysID cogl_renderer_get_winsys_id(Renderer* this_) nothrow;
void cogl_renderer_remove_constraint(Renderer* this_, RendererConstraint constraint) nothrow;
void cogl_renderer_set_winsys_id(Renderer* this_, WinsysID winsys_id) nothrow;
Cogl.Renderer* cogl_renderer_new() nothrow;
char* cogl_snippet_get_declarations(Snippet* this_) nothrow;
SnippetHook cogl_snippet_get_hook(Snippet* this_) nothrow;
char* cogl_snippet_get_post(Snippet* this_) nothrow;
char* cogl_snippet_get_pre(Snippet* this_) nothrow;
char* cogl_snippet_get_replace(Snippet* this_) nothrow;
void cogl_snippet_set_declarations(Snippet* this_, char* declarations) nothrow;
void cogl_snippet_set_post(Snippet* this_, char* post) nothrow;
void cogl_snippet_set_pre(Snippet* this_, char* pre) nothrow;
void cogl_snippet_set_replace(Snippet* this_, char* replace) nothrow;
Snippet* cogl_snippet_new(SnippetHook hook, char* declarations, char* post) nothrow;
Texture* cogl_sub_texture_get_parent(SubTexture* this_) nothrow;
SubTexture* cogl_sub_texture_new_EXP(Cogl.Context* ctx, Texture* parent_texture, int sub_x, int sub_y, int sub_width, int sub_height) nothrow;
void cogl_swap_chain_set_has_alpha_EXP(SwapChain* this_, int has_alpha) nothrow;
void cogl_swap_chain_set_length_EXP(SwapChain* this_, int length) nothrow;
SwapChain* cogl_swap_chain_new_EXP() nothrow;
int cogl_texture_get_data(Texture* this_, PixelFormat format, uint rowstride, ubyte* data) nothrow;
PixelFormat cogl_texture_get_format(Texture* this_) nothrow;
int cogl_texture_get_gl_texture(Texture* this_, /*out*/ GL.uint_* out_gl_handle=null, /*out*/ GL.enum_* out_gl_target=null) nothrow;
uint cogl_texture_get_height(Texture* this_) nothrow;
int cogl_texture_get_max_waste(Texture* this_) nothrow;
uint cogl_texture_get_rowstride(Texture* this_) nothrow;
uint cogl_texture_get_width(Texture* this_) nothrow;
int cogl_texture_is_sliced(Texture* this_) nothrow;
Texture* cogl_texture_new_from_sub_texture(Texture* this_, int sub_x, int sub_y, int sub_width, int sub_height) nothrow;
int cogl_texture_set_region(Texture* this_, int src_x, int src_y, int dst_x, int dst_y, uint dst_width, uint dst_height, int width, int height, PixelFormat format, uint rowstride, ubyte* data) nothrow;
int cogl_texture_set_region_from_bitmap_EXP(Texture* this_, int src_x, int src_y, int dst_x, int dst_y, uint dst_width, uint dst_height, Bitmap* bitmap) nothrow;
Texture2D* cogl_texture_2d_new_from_data_EXP(Cogl.Context* ctx, int width, int height, PixelFormat format, PixelFormat internal_format, int rowstride, ubyte* data, GLib2.Error** error) nothrow;
Texture2D* cogl_texture_2d_new_from_foreign_EXP(Cogl.Context* ctx, uint gl_handle, int width, int height, PixelFormat format, GLib2.Error** error) nothrow;
Texture2D* cogl_texture_2d_new_with_size_EXP(Cogl.Context* ctx, int width, int height, PixelFormat internal_format, GLib2.Error** error) nothrow;
Texture2DSliced* cogl_texture_2d_sliced_new_with_size(Cogl.Context* ctx, uint width, uint height, int max_waste, PixelFormat internal_format, GLib2.Error** error) nothrow;
Handle cogl_texture_3d_new_from_data_EXP(uint width, uint height, uint depth, TextureFlags flags, PixelFormat format, PixelFormat internal_format, uint rowstride, uint image_stride, ubyte* data, GLib2.Error** error) nothrow;
Handle cogl_texture_3d_new_with_size_EXP(uint width, uint height, uint depth, TextureFlags flags, PixelFormat internal_format, GLib2.Error** error) nothrow;
Texture* cogl_texture_new_from_bitmap(Bitmap* bitmap, TextureFlags flags, PixelFormat internal_format) nothrow;
Texture* cogl_texture_new_from_buffer_EXP(PixelBuffer* buffer, uint width, uint height, TextureFlags flags, PixelFormat format, PixelFormat internal_format, uint rowstride, uint offset) nothrow;
Texture* cogl_texture_new_from_data(uint width, uint height, TextureFlags flags, PixelFormat format, PixelFormat internal_format, uint rowstride, ubyte* data) nothrow;
Texture* cogl_texture_new_from_file(char* filename, TextureFlags flags, PixelFormat internal_format, GLib2.Error** error) nothrow;
Texture* cogl_texture_new_from_foreign(GL.uint_ gl_handle, GL.enum_ gl_target, GL.uint_ width, GL.uint_ height, GL.uint_ x_pot_waste, GL.uint_ y_pot_waste, PixelFormat format) nothrow;
Texture* cogl_texture_new_with_size(uint width, uint height, TextureFlags flags, PixelFormat internal_format) nothrow;
void* cogl_texture_ref(void* texture) nothrow;
void cogl_texture_unref(void* texture) nothrow;
TextureRectangle* cogl_texture_rectangle_new_with_size_EXP(Cogl.Context* ctx, int width, int height, PixelFormat internal_format, GLib2.Error** error) nothrow;
Fixed cogl_angle_cos(Angle angle) nothrow;
Fixed cogl_angle_sin(Angle angle) nothrow;
Fixed cogl_angle_tan(Angle angle) nothrow;
void cogl_begin_gl() nothrow;
GLib2.Quark cogl_bitmap_error_quark() nothrow;
GLib2.Quark cogl_blend_string_error_quark() nothrow;
int cogl_check_extension(char* name, char* ext) nothrow;
void cogl_clear(Color* color, c_ulong buffers) nothrow;
void cogl_clip_ensure() nothrow;
void cogl_clip_pop() nothrow;
void cogl_clip_push(float x_offset, float y_offset, float width, float height) nothrow;
void cogl_clip_push_from_path() nothrow;
void cogl_clip_push_from_path_preserve() nothrow;
void cogl_clip_push_primitive(Primitive* primitive, float bounds_x1, float bounds_y1, float bounds_x2, float bounds_y2) nothrow;
void cogl_clip_push_rectangle(float x0, float y0, float x1, float y1) nothrow;
void cogl_clip_push_window_rect(float x_offset, float y_offset, float width, float height) nothrow;
void cogl_clip_push_window_rectangle(int x_offset, int y_offset, int width, int height) nothrow;
void cogl_clip_stack_restore() nothrow;
void cogl_clip_stack_save() nothrow;
int cogl_clutter_check_extension_CLUTTER(char* name, char* ext) nothrow;
int cogl_clutter_winsys_has_feature_CLUTTER(WinsysFeature feature) nothrow;
/*CTYPE*/ XVisualInfo* cogl_clutter_winsys_xlib_get_visual_info_CLUTTER() nothrow;
Handle cogl_create_program() nothrow;
Handle cogl_create_shader(ShaderType shader_type) nothrow;
void cogl_debug_object_foreach_type_EXP(DebugObjectForeachTypeCallback func, void* user_data) nothrow;
void cogl_debug_object_print_instances_EXP() nothrow;
void cogl_disable_fog() nothrow;
Fixed cogl_double_to_fixed(double value) nothrow;
int cogl_double_to_int(double value) nothrow;
uint cogl_double_to_uint(double value) nothrow;
void cogl_draw_attributes(VerticesMode mode, int first_vertex, int n_vertices, Attribute** attributes, int n_attributes) nothrow;
void cogl_draw_indexed_attributes(VerticesMode mode, int first_vertex, int n_vertices, Indices* indices, Attribute** attributes, int n_attributes) nothrow;
void cogl_end_gl() nothrow;
int cogl_features_available(FeatureFlags features) nothrow;
void cogl_flush() nothrow;
void cogl_foreach_feature(Cogl.Context* context, FeatureCallback callback, void* user_data) nothrow;
GLib2.Quark cogl_framebuffer_error_quark() nothrow;
void cogl_frustum(float left, float right, float bottom, float top, float z_near, float z_far) nothrow;
int cogl_get_backface_culling_enabled() nothrow;
void cogl_get_bitmasks(/*out*/ int* red, /*out*/ int* green, /*out*/ int* blue, /*out*/ int* alpha) nothrow;
int cogl_get_depth_test_enabled() nothrow;
Framebuffer* cogl_get_draw_framebuffer() nothrow;
FeatureFlags cogl_get_features() nothrow;
void cogl_get_modelview_matrix(/*out*/ Matrix* matrix) nothrow;
GLib2.OptionGroup* cogl_get_option_group() nothrow;
Path* cogl_get_path() nothrow;
FuncPtr cogl_get_proc_address(char* name) nothrow;
void cogl_get_projection_matrix(/*out*/ Matrix* matrix) nothrow;
Indices* cogl_get_rectangle_indices(int n_rectangles) nothrow;
void* cogl_get_source() nothrow;
Quaternion* cogl_get_static_identity_quaternion() nothrow;
Quaternion* cogl_get_static_zero_quaternion() nothrow;
void cogl_get_viewport(/*out*/ float v) nothrow;
GLib2.Source* /*new*/ cogl_glib_source_new(Cogl.Context* context, int priority) nothrow;
Type cogl_handle_get_type() nothrow;
Handle cogl_handle_ref(Handle handle) nothrow;
void cogl_handle_unref(Handle handle) nothrow;
int cogl_has_feature(Cogl.Context* context, FeatureID feature) nothrow;
int cogl_has_features(Cogl.Context* context, ...) nothrow;
IndicesType cogl_indices_get_type(Indices* indices) nothrow;
int cogl_is_attribute(void* object) nothrow;
int cogl_is_attribute_buffer(void* object) nothrow;
int cogl_is_bitmap(Handle handle) nothrow;
int cogl_is_buffer(void* object) nothrow;
int cogl_is_context(void* object) nothrow;
int cogl_is_index_buffer(void* object) nothrow;
int cogl_is_material(Handle handle) nothrow;
int cogl_is_offscreen(Handle handle) nothrow;
int cogl_is_path(Handle handle) nothrow;
int cogl_is_pipeline(Handle handle) nothrow;
int cogl_is_pixel_buffer_EXP(void* object) nothrow;
int cogl_is_primitive(void* object) nothrow;
int cogl_is_program(Handle handle) nothrow;
int cogl_is_renderer(void* object) nothrow;
int cogl_is_shader(Handle handle) nothrow;
int cogl_is_snippet(void* object) nothrow;
int cogl_is_sub_texture_EXP(void* object) nothrow;
int cogl_is_texture(void* object) nothrow;
int cogl_is_texture_2d_EXP(void* object) nothrow;
int cogl_is_texture_3d_EXP(Handle handle) nothrow;
int cogl_is_texture_rectangle_EXP(void* object) nothrow;
int cogl_is_vertex_buffer(Handle handle) nothrow;
int cogl_is_vertex_buffer_indices(Handle handle) nothrow;
MaterialLayerType cogl_material_layer_get_type(MaterialLayer* layer) nothrow;
Handle /*new*/ cogl_offscreen_new_to_texture(Texture* texture) nothrow;
Handle cogl_offscreen_ref(Handle handle) nothrow;
void cogl_offscreen_unref(Handle handle) nothrow;
void cogl_ortho(float left, float right, float bottom, float top, float near, float far) nothrow;
void cogl_perspective(float fovy, float aspect, float z_near, float z_far) nothrow;
void cogl_poll_dispatch(Cogl.Context* context, PollFD* poll_fds, int n_poll_fds) nothrow;
void cogl_poll_get_info(Cogl.Context* context, PollFD** poll_fds, int* n_poll_fds, long* timeout) nothrow;
void cogl_polygon(TextureVertex* vertices, uint n_vertices, int use_color) nothrow;
void cogl_pop_draw_buffer() nothrow;
void cogl_pop_framebuffer() nothrow;
void cogl_pop_matrix() nothrow;
void cogl_pop_source() nothrow;
void cogl_program_attach_shader(Handle program_handle, Handle shader_handle) nothrow;
int cogl_program_get_uniform_location(Handle handle, char* uniform_name) nothrow;
void cogl_program_link(Handle handle) nothrow;
Handle cogl_program_ref(Handle handle) nothrow;
void cogl_program_set_uniform_1f(Handle program, int uniform_location, float value) nothrow;
void cogl_program_set_uniform_1i(Handle program, int uniform_location, int value) nothrow;
void cogl_program_set_uniform_float(Handle program, int uniform_location, int n_components, int count, float* value) nothrow;
void cogl_program_set_uniform_int(Handle program, int uniform_location, int n_components, int count, int* value) nothrow;
void cogl_program_set_uniform_matrix(Handle program, int uniform_location, int dimensions, int count, int transpose, float* value) nothrow;
void cogl_program_uniform_1f(int uniform_no, float value) nothrow;
void cogl_program_uniform_1i(int uniform_no, int value) nothrow;
void cogl_program_uniform_float(int uniform_no, int size, int count, float* value) nothrow;
void cogl_program_uniform_int(int uniform_no, int size, int count, int* value) nothrow;
void cogl_program_uniform_matrix(int uniform_no, int size, int count, int transpose, float* value) nothrow;
void cogl_program_unref(Handle handle) nothrow;
void cogl_program_use(Handle handle) nothrow;
void cogl_push_draw_buffer() nothrow;
void cogl_push_framebuffer(Framebuffer* buffer) nothrow;
void cogl_push_matrix() nothrow;
void cogl_push_source(void* material) nothrow;
void cogl_read_pixels(int x, int y, int width, int height, ReadPixelsFlags source, PixelFormat format, ubyte* pixels) nothrow;
void cogl_rectangle(float x_1, float y_1, float x_2, float y_2) nothrow;
void cogl_rectangle_with_multitexture_coords(float x1, float y1, float x2, float y2, float* tex_coords, int tex_coords_len) nothrow;
void cogl_rectangle_with_texture_coords(float x1, float y1, float x2, float y2, float tx1, float ty1, float tx2, float ty2) nothrow;
void cogl_rectangles(float* verts, uint n_rects) nothrow;
void cogl_rectangles_with_texture_coords(float* verts, uint n_rects) nothrow;
GLib2.Quark cogl_renderer_error_quark() nothrow;
void cogl_rotate(float angle, float x, float y, float z) nothrow;
void cogl_scale(float x, float y, float z) nothrow;
void cogl_set_backface_culling_enabled(int setting) nothrow;
void cogl_set_depth_test_enabled(int setting) nothrow;
void cogl_set_draw_buffer(BufferTarget target, Handle offscreen) nothrow;
void cogl_set_fog(Color* fog_color, FogMode mode, float density, float z_near, float z_far) nothrow;
void cogl_set_framebuffer(Framebuffer* buffer) nothrow;
void cogl_set_modelview_matrix(Matrix* matrix) nothrow;
void cogl_set_path(Path* path) nothrow;
void cogl_set_projection_matrix(Matrix* matrix) nothrow;
void cogl_set_source(void* material) nothrow;
void cogl_set_source_color(Color* color) nothrow;
void cogl_set_source_color4f(float red, float green, float blue, float alpha) nothrow;
void cogl_set_source_color4ub(ubyte red, ubyte green, ubyte blue, ubyte alpha) nothrow;
void cogl_set_source_texture(Texture* texture) nothrow;
void cogl_set_viewport(int x, int y, int width, int height) nothrow;
void cogl_shader_compile(Handle handle) nothrow;
char* /*new*/ cogl_shader_get_info_log(Handle handle) nothrow;
ShaderType cogl_shader_get_type(Handle handle) nothrow;
int cogl_shader_is_compiled(Handle handle) nothrow;
Handle cogl_shader_ref(Handle handle) nothrow;
void cogl_shader_source(Handle shader, char* source) nothrow;
void cogl_shader_unref(Handle handle) nothrow;
int cogl_sqrti(int x) nothrow;
GLib2.Quark cogl_texture_error_quark() nothrow;
void cogl_transform(Matrix* matrix) nothrow;
void cogl_translate(float x, float y, float z) nothrow;
void cogl_vdraw_attributes(VerticesMode mode, int first_vertex, int n_vertices, ...) nothrow;
void cogl_vdraw_indexed_attributes(VerticesMode mode, int first_vertex, int n_vertices, Indices* indices, ...) nothrow;
void cogl_vector3_add(float* result, float* a, float* b) nothrow;
float* cogl_vector3_copy(float* vector) nothrow;
void cogl_vector3_cross_product(float* result, float* u, float* v) nothrow;
float cogl_vector3_distance(float* a, float* b) nothrow;
void cogl_vector3_divide_scalar(float* vector, float scalar) nothrow;
float cogl_vector3_dot_product(float* a, float* b) nothrow;
int cogl_vector3_equal(const(void)* v1, const(void)* v2) nothrow;
int cogl_vector3_equal_with_epsilon(float* vector0, float* vector1, float epsilon) nothrow;
void cogl_vector3_free(float* vector) nothrow;
void cogl_vector3_init(float* vector, float x, float y, float z) nothrow;
void cogl_vector3_init_zero(float* vector) nothrow;
void cogl_vector3_invert(float* vector) nothrow;
float cogl_vector3_magnitude(float* vector) nothrow;
void cogl_vector3_multiply_scalar(float* vector, float scalar) nothrow;
void cogl_vector3_normalize(float* vector) nothrow;
void cogl_vector3_subtract(float* result, float* a, float* b) nothrow;
void cogl_vertex_buffer_add(Handle handle, char* attribute_name, ubyte n_components, AttributeType type, int normalized, ushort stride, void* pointer) nothrow;
void cogl_vertex_buffer_delete(Handle handle, char* attribute_name) nothrow;
void cogl_vertex_buffer_disable(Handle handle, char* attribute_name) nothrow;
void cogl_vertex_buffer_draw(Handle handle, VerticesMode mode, int first, int count) nothrow;
void cogl_vertex_buffer_draw_elements(Handle handle, VerticesMode mode, Handle indices, int min_index, int max_index, int indices_offset, int count) nothrow;
void cogl_vertex_buffer_enable(Handle handle, char* attribute_name) nothrow;
uint cogl_vertex_buffer_get_n_vertices(Handle handle) nothrow;
Handle cogl_vertex_buffer_indices_get_for_quads(uint n_indices) nothrow;
IndicesType cogl_vertex_buffer_indices_get_type(Handle indices) nothrow;
Handle cogl_vertex_buffer_indices_new(IndicesType indices_type, void* indices_array, int indices_len) nothrow;
Handle cogl_vertex_buffer_new(uint n_vertices) nothrow;
Handle cogl_vertex_buffer_ref(Handle handle) nothrow;
void cogl_vertex_buffer_submit(Handle handle) nothrow;
void cogl_vertex_buffer_unref(Handle handle) nothrow;
void cogl_viewport(uint width, uint height) nothrow;
uint cogl_x11_onscreen_get_visual_xid(Onscreen* onscreen) nothrow;
uint cogl_x11_onscreen_get_window_xid(Onscreen* onscreen) nothrow;
void cogl_x11_onscreen_set_foreign_window_xid(Onscreen* onscreen, uint xid, OnscreenX11MaskCallback update, void* user_data) nothrow;
void cogl_xlib_renderer_add_filter_EXP(Cogl.Renderer* renderer, XlibFilterFunc func, void* data) nothrow;
/*CTYPE*/ Display* cogl_xlib_renderer_get_display_EXP(Cogl.Renderer* renderer) nothrow;
/*CTYPE*/ Display* cogl_xlib_renderer_get_foreign_display_EXP(Cogl.Renderer* renderer) nothrow;
FilterReturn cogl_xlib_renderer_handle_event_EXP(Cogl.Renderer* renderer, XEvent* event) nothrow;
void cogl_xlib_renderer_remove_filter_EXP(Cogl.Renderer* renderer, XlibFilterFunc func, void* data) nothrow;
void cogl_xlib_renderer_set_event_retrieval_enabled(Cogl.Renderer* renderer, int enable) nothrow;
void cogl_xlib_renderer_set_foreign_display_EXP(Cogl.Renderer* renderer, Display* display) nothrow;
}

// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/CoglPango-1.0.gir"

module CoglPango;
public import gtk2.cogl;
alias gtk2.cogl Cogl;
public import gtk2.gl;
alias gtk2.gl GL;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;
public import gtk2.pango;
alias gtk2.pango Pango;
public import gtk2.pangocairo;
alias gtk2.pangocairo PangoCairo;
public import gtk2.pangoft2;
alias gtk2.pangoft2 PangoFT2;
public import gtk2.cairo;
alias gtk2.cairo cairo;
public import gtk2.fontconfig2;
alias gtk2.fontconfig2 fontconfig2;
public import gtk2.freetype2;
alias gtk2.freetype2 freetype2;

// package: "cogl-pango-1.0";
// C header: "cogl-pango/cogl-pango.h";

// c:symbol-prefixes: ["cogl_pango"]
// c:identifier-prefixes: ["CoglPango"]

// module CoglPango;

alias PangoCairo.FontMap FontMap;
struct Renderer /* : Pango.Renderer */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent renderer;
   Pango.Renderer method_parent;
}

struct RendererClass {
}

static void ensure_glyph_cache_for_layout(AT0)(AT0 /*Pango.Layout*/ layout) nothrow {
   cogl_pango_ensure_glyph_cache_for_layout(UpCast!(Pango.Layout*)(layout));
}


// VERSION: 1.0
// Clears the glyph cache for @fm.
// <fm>: a #CoglPangoFontMap
static void font_map_clear_glyph_cache(AT0)(AT0 /*FontMap*/ fm) nothrow {
   cogl_pango_font_map_clear_glyph_cache(UpCast!(FontMap*)(fm));
}


// VERSION: 1.0
// Creates a new #PangoContext from the passed font map.
// RETURNS: the newly created #PangoContext
// <fm>: a #CoglPangoFontMap
static Pango.Context* /*new*/ font_map_create_context(AT0)(AT0 /*FontMap*/ fm) nothrow {
   return cogl_pango_font_map_create_context(UpCast!(FontMap*)(fm));
}


// VERSION: 1.0
// Retrieves the #CoglPangoRenderer for the passed font map.
// RETURNS: a #PangoRenderer
// <fm>: a #CoglPangoFontMap
static Pango.Renderer* font_map_get_renderer(AT0)(AT0 /*FontMap*/ fm) nothrow {
   return cogl_pango_font_map_get_renderer(UpCast!(FontMap*)(fm));
}


// VERSION: 1.0
// Retrieves whether the #CoglPangoRenderer used by @fm will
// use mipmapping when rendering the glyphs.
// RETURNS: %TRUE if mipmapping is used, %FALSE otherwise.
// <fm>: a #CoglPangoFontMap
static int font_map_get_use_mipmapping(AT0)(AT0 /*FontMap*/ fm) nothrow {
   return cogl_pango_font_map_get_use_mipmapping(UpCast!(FontMap*)(fm));
}


// VERSION: 1.0
// Creates a new font map.
// RETURNS: the newly created #PangoFontMap
static Pango.FontMap* /*new*/ font_map_new()() nothrow {
   return cogl_pango_font_map_new();
}


// VERSION: 1.0
// Sets the resolution to be used by @font_map at @dpi.
// <font_map>: a #CoglPangoFontMap
// <dpi>: DPI to set
static void font_map_set_resolution(AT0)(AT0 /*FontMap*/ font_map, double dpi) nothrow {
   cogl_pango_font_map_set_resolution(UpCast!(FontMap*)(font_map), dpi);
}


// VERSION: 1.0
// Sets whether the renderer for the passed font map should use
// mipmapping when rendering a #PangoLayout.
// <fm>: a #CoglPangoFontMap
// <value>: %TRUE to enable the use of mipmapping
static void font_map_set_use_mipmapping(AT0)(AT0 /*FontMap*/ fm, int value) nothrow {
   cogl_pango_font_map_set_use_mipmapping(UpCast!(FontMap*)(fm), value);
}


// VERSION: 1.0
// Renders @layout.
// <layout>: a #PangoLayout
// <x>: X coordinate to render the layout at
// <y>: Y coordinate to render the layout at
// <color>: color to use when rendering the layout
// <flags>: flags to pass to the renderer
static void render_layout(AT0, AT1)(AT0 /*Pango.Layout*/ layout, int x, int y, AT1 /*Cogl.Color*/ color, int flags) nothrow {
   cogl_pango_render_layout(UpCast!(Pango.Layout*)(layout), x, y, UpCast!(Cogl.Color*)(color), flags);
}


// VERSION: 1.0
// Renders @line at the given coordinates using the given color.
// <line>: a #PangoLayoutLine
// <x>: X coordinate to render the line at
// <y>: Y coordinate to render the line at
// <color>: color to use when rendering the line
static void render_layout_line(AT0, AT1)(AT0 /*Pango.LayoutLine*/ line, int x, int y, AT1 /*Cogl.Color*/ color) nothrow {
   cogl_pango_render_layout_line(UpCast!(Pango.LayoutLine*)(line), x, y, UpCast!(Cogl.Color*)(color));
}


// VERSION: 1.0
// FIXME
// <layout>: a #PangoLayout
// <x>: FIXME
// <y>: FIXME
// <color>: color to use when rendering the layout
// <flags>: flags to pass to the renderer
static void render_layout_subpixel(AT0, AT1)(AT0 /*Pango.Layout*/ layout, int x, int y, AT1 /*Cogl.Color*/ color, int flags) nothrow {
   cogl_pango_render_layout_subpixel(UpCast!(Pango.Layout*)(layout), x, y, UpCast!(Cogl.Color*)(color), flags);
}


// C prototypes:

extern (C) {
void cogl_pango_ensure_glyph_cache_for_layout(Pango.Layout* layout) nothrow;
void cogl_pango_font_map_clear_glyph_cache(FontMap* fm) nothrow;
Pango.Context* /*new*/ cogl_pango_font_map_create_context(FontMap* fm) nothrow;
Pango.Renderer* cogl_pango_font_map_get_renderer(FontMap* fm) nothrow;
int cogl_pango_font_map_get_use_mipmapping(FontMap* fm) nothrow;
Pango.FontMap* /*new*/ cogl_pango_font_map_new() nothrow;
void cogl_pango_font_map_set_resolution(FontMap* font_map, double dpi) nothrow;
void cogl_pango_font_map_set_use_mipmapping(FontMap* fm, int value) nothrow;
void cogl_pango_render_layout(Pango.Layout* layout, int x, int y, Cogl.Color* color, int flags) nothrow;
void cogl_pango_render_layout_line(Pango.LayoutLine* line, int x, int y, Cogl.Color* color) nothrow;
void cogl_pango_render_layout_subpixel(Pango.Layout* layout, int x, int y, Cogl.Color* color, int flags) nothrow;
}

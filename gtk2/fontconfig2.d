// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/fontconfig-2.0.gir"

module fontconfig2;

// c:symbol-prefixes: ["fc"]
// c:identifier-prefixes: ["Fc"]

// module fontconfig2;

struct Pattern {
}

struct CharSet {
}

static void init()() nothrow {
   FcInit();
}


// C prototypes:

extern (C) {
void FcInit() nothrow;
}

// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/freetype2-2.0.gir"

module freetype2;

// c:symbol-prefixes: ["FT"]
// c:identifier-prefixes: ["FT"]

// module freetype2;

struct Bitmap {
}

struct Face {
}

struct Library {
}


// alias "Int32" removed: -

static void library_version()() nothrow {
   FT_Library_Version();
}


// C prototypes:

extern (C) {
void FT_Library_Version() nothrow;
}

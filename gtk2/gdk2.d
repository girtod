// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/Gdk-2.0.gir"

module Gdk2;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gmodule2;
alias gtk2.gmodule2 GModule2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;
public import gtk2.gdkpixbuf2;
alias gtk2.gdkpixbuf2 GdkPixbuf2;
public import gtk2.gio2;
alias gtk2.gio2 Gio2;
public import gtk2.pango;
alias gtk2.pango Pango;
public import gtk2.cairo;
alias gtk2.cairo cairo;

// c:symbol-prefixes: ["gdk"]
// c:identifier-prefixes: ["Gdk"]

// module Gdk2;

alias uint NativeWindow;
alias Atom Selection;
alias Atom SelectionType;
alias Atom Target;
alias uint WChar;
alias void* XEvent;
struct AppLaunchContext /* : Gio.AppLaunchContext */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance applaunchcontext;
   Gio2.AppLaunchContext parent_instance;
   AppLaunchContextPrivate* priv;


   // VERSION: 2.14
   // Creates a new #GdkAppLaunchContext.
   // RETURNS: a new #GdkAppLaunchContext
   static AppLaunchContext* /*new*/ new_()() nothrow {
      return gdk_app_launch_context_new();
   }
   static auto opCall()() {
      return gdk_app_launch_context_new();
   }

   // VERSION: 2.14
   // Sets the workspace on which applications will be launched when
   // using this context when running under a window manager that 
   // supports multiple workspaces, as described in the 
   // <ulink url="http://www.freedesktop.org/Standards/wm-spec">Extended 
   // Window Manager Hints</ulink>. 
   // 
   // When the workspace is not specified or @desktop is set to -1, 
   // it is up to the window manager to pick one, typically it will
   // be the current workspace.
   // <desktop>: the number of a workspace, or -1
   void set_desktop()(int desktop) nothrow {
      gdk_app_launch_context_set_desktop(&this, desktop);
   }

   // VERSION: 2.14
   // Sets the display on which applications will be launched when
   // using this context. See also gdk_app_launch_context_set_screen().
   // <display>: a #GdkDisplay
   void set_display(AT0)(AT0 /*Display*/ display) nothrow {
      gdk_app_launch_context_set_display(&this, UpCast!(Display*)(display));
   }

   // VERSION: 2.14
   // Sets the icon for applications that are launched with this
   // context.
   // 
   // Window Managers can use this information when displaying startup
   // notification.
   // 
   // See also gdk_app_launch_context_set_icon_name().
   // <icon>: a #GIcon, or %NULL
   void set_icon(AT0)(AT0 /*Gio2.Icon*/ icon=null) nothrow {
      gdk_app_launch_context_set_icon(&this, UpCast!(Gio2.Icon*)(icon));
   }

   // VERSION: 2.14
   // Sets the icon for applications that are launched with this context. 
   // The @icon_name will be interpreted in the same way as the Icon field 
   // in desktop files. See also gdk_app_launch_context_set_icon(). 
   // 
   // If both @icon and @icon_name are set, the @icon_name takes priority.
   // If neither @icon or @icon_name is set, the icon is taken from either 
   // the file that is passed to launched application or from the #GAppInfo 
   // for the launched application itself.
   // <icon_name>: an icon name, or %NULL
   void set_icon_name(AT0)(AT0 /*char*/ icon_name=null) nothrow {
      gdk_app_launch_context_set_icon_name(&this, toCString!(char*)(icon_name));
   }

   // VERSION: 2.14
   // Sets the screen on which applications will be launched when
   // using this context. See also gdk_app_launch_context_set_display().
   // 
   // If both @screen and @display are set, the @screen takes priority.
   // If neither @screen or @display are set, the default screen and
   // display are used.
   // <screen>: a #GdkScreen
   void set_screen(AT0)(AT0 /*Screen*/ screen) nothrow {
      gdk_app_launch_context_set_screen(&this, UpCast!(Screen*)(screen));
   }

   // VERSION: 2.14
   // Sets the timestamp of @context. The timestamp should ideally
   // be taken from the event that triggered the launch. 
   // 
   // Window managers can use this information to avoid moving the
   // focus to the newly launched application when the user is busy
   // typing in another window. This is also known as 'focus stealing
   // prevention'.
   // <timestamp>: a timestamp
   void set_timestamp()(uint timestamp) nothrow {
      gdk_app_launch_context_set_timestamp(&this, timestamp);
   }
}

struct AppLaunchContextClass {
   Gio2.AppLaunchContextClass parent_class;
}

struct AppLaunchContextPrivate {
}

struct Atom {
   char* /*new*/ name()() nothrow {
      return gdk_atom_name(&this);
   }
   // Unintrospectable function: intern() / gdk_atom_intern()
   static Atom intern(AT0)(AT0 /*char*/ atom_name, int only_if_exists) nothrow {
      return gdk_atom_intern(toCString!(char*)(atom_name), only_if_exists);
   }

   // Unintrospectable function: intern_static_string() / gdk_atom_intern_static_string()
   // VERSION: 2.10
   // Finds or creates an atom corresponding to a given string.
   // 
   // Note that this function is identical to gdk_atom_intern() except
   // that if a new #GdkAtom is created the string itself is used rather 
   // than a copy. This saves memory, but can only be used if the string 
   // will <emphasis>always</emphasis> exist. It can be used with statically
   // allocated strings in the main program, but not with statically 
   // allocated memory in dynamically loaded modules, if you expect to
   // ever unload the module again (e.g. do not use this function in
   // GTK+ theme engines).
   // RETURNS: the atom corresponding to @atom_name
   // <atom_name>: a static string
   static Atom intern_static_string(AT0)(AT0 /*char*/ atom_name) nothrow {
      return gdk_atom_intern_static_string(toCString!(char*)(atom_name));
   }
}

enum AxisUse {
   IGNORE = 0,
   X = 1,
   Y = 2,
   PRESSURE = 3,
   XTILT = 4,
   YTILT = 5,
   WHEEL = 6,
   LAST = 7
}
struct Bitmap {
   // Unintrospectable function: create_from_data() / gdk_bitmap_create_from_data()
   static Bitmap* create_from_data(AT0, AT1)(AT0 /*Drawable*/ drawable, AT1 /*char*/ data, int width, int height) nothrow {
      return gdk_bitmap_create_from_data(UpCast!(Drawable*)(drawable), toCString!(char*)(data), width, height);
   }
}

enum ByteOrder {
   LSB_FIRST = 0,
   MSB_FIRST = 1
}
enum int CURRENT_TIME = 0;
enum CapStyle {
   NOT_LAST = 0,
   BUTT = 1,
   ROUND = 2,
   PROJECTING = 3
}
struct Color {
   uint pixel;
   ushort red, green, blue;


   // Makes a copy of a color structure. The result
   // must be freed using gdk_color_free().
   // RETURNS: a copy of @color.
   Color* /*new*/ copy()() nothrow {
      return gdk_color_copy(&this);
   }

   // Compares two colors.
   // RETURNS: %TRUE if the two colors compare equal
   // <colorb>: another #GdkColor.
   int equal(AT0)(AT0 /*Color*/ colorb) nothrow {
      return gdk_color_equal(&this, UpCast!(Color*)(colorb));
   }

   // Frees a color structure created with 
   // gdk_color_copy().
   void free()() nothrow {
      gdk_color_free(&this);
   }

   // A hash function suitable for using for a hash
   // table that stores #GdkColor's.
   // RETURNS: The hash function applied to @colora
   uint hash()() nothrow {
      return gdk_color_hash(&this);
   }

   // VERSION: 2.12
   // Returns a textual specification of @color in the hexadecimal form
   // <literal>&num;rrrrggggbbbb</literal>, where <literal>r</literal>,
   // <literal>g</literal> and <literal>b</literal> are hex digits
   // representing the red, green and blue components respectively.
   // RETURNS: a newly-allocated text string
   char* /*new*/ to_string()() nothrow {
      return gdk_color_to_string(&this);
   }

   // DEPRECATED (v2.2) function: alloc - Use gdk_colormap_alloc_color() instead.
   // Allocates a single color from a colormap.
   // RETURNS: %TRUE if the allocation succeeded.
   // <colormap>: a #GdkColormap.
   // <color>: The color to allocate. On return, the <structfield>pixel</structfield> field will be filled in.
   static int alloc(AT0, AT1)(AT0 /*Colormap*/ colormap, AT1 /*Color*/ color) nothrow {
      return gdk_color_alloc(UpCast!(Colormap*)(colormap), UpCast!(Color*)(color));
   }

   // Returns the black color for a given colormap. The resulting
   // value has already been allocated.
   // RETURNS: %TRUE if the allocation succeeded.
   // <colormap>: a #GdkColormap.
   // <color>: the location to store the color.
   static int black(AT0, AT1)(AT0 /*Colormap*/ colormap, AT1 /*Color*/ color) nothrow {
      return gdk_color_black(UpCast!(Colormap*)(colormap), UpCast!(Color*)(color));
   }

   // Changes the value of a color that has already
   // been allocated. If @colormap is not a private
   // colormap, then the color must have been allocated
   // using gdk_colormap_alloc_colors() with the 
   // @writeable set to %TRUE.
   // RETURNS: %TRUE if the color was successfully changed.
   // <colormap>: a #GdkColormap.
   // <color>: a #GdkColor, with the color to change in the <structfield>pixel</structfield> field, and the new value in the remaining fields.
   static int change(AT0, AT1)(AT0 /*Colormap*/ colormap, AT1 /*Color*/ color) nothrow {
      return gdk_color_change(UpCast!(Colormap*)(colormap), UpCast!(Color*)(color));
   }

   // Parses a textual specification of a color and fill in the
   // <structfield>red</structfield>, <structfield>green</structfield>,
   // and <structfield>blue</structfield> fields of a #GdkColor
   // structure. The color is <emphasis>not</emphasis> allocated, you
   // must call gdk_colormap_alloc_color() yourself. The string can
   // either one of a large set of standard names. (Taken from the X11
   // <filename>rgb.txt</filename> file), or it can be a hex value in the
   // form '&num;rgb' '&num;rrggbb' '&num;rrrgggbbb' or
   // '&num;rrrrggggbbbb' where 'r', 'g' and 'b' are hex digits of the
   // red, green, and blue components of the color, respectively. (White
   // in the four forms is '&num;fff' '&num;ffffff' '&num;fffffffff' and
   // '&num;ffffffffffff')
   // RETURNS: %TRUE if the parsing succeeded.
   // <spec>: the string specifying the color.
   // <color>: the #GdkColor to fill in
   static int parse(AT0, AT1)(AT0 /*char*/ spec, /*out*/ AT1 /*Color*/ color) nothrow {
      return gdk_color_parse(toCString!(char*)(spec), UpCast!(Color*)(color));
   }

   // Returns the white color for a given colormap. The resulting
   // value has already allocated been allocated.
   // RETURNS: %TRUE if the allocation succeeded.
   // <colormap>: a #GdkColormap.
   // <color>: the location to store the color.
   static int white(AT0, AT1)(AT0 /*Colormap*/ colormap, AT1 /*Color*/ color) nothrow {
      return gdk_color_white(UpCast!(Colormap*)(colormap), UpCast!(Color*)(color));
   }
}

struct Colormap /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   int size;
   Color* colors;
   private Visual* visual;
   private void* windowing_data;


   // Creates a new colormap for the given visual.
   // RETURNS: the new #GdkColormap.
   // <visual>: a #GdkVisual.
   // <allocate>: if %TRUE, the newly created colormap will be a private colormap, and all colors in it will be allocated for the applications use.
   static Colormap* /*new*/ new_(AT0)(AT0 /*Visual*/ visual, int allocate) nothrow {
      return gdk_colormap_new(UpCast!(Visual*)(visual), allocate);
   }
   static auto opCall(AT0)(AT0 /*Visual*/ visual, int allocate) {
      return gdk_colormap_new(UpCast!(Visual*)(visual), allocate);
   }

   // Unintrospectable function: get_system() / gdk_colormap_get_system()
   // Gets the system's default colormap for the default screen. (See
   // gdk_colormap_get_system_for_screen ())
   // RETURNS: the default colormap.
   static Colormap* get_system()() nothrow {
      return gdk_colormap_get_system();
   }

   // Returns the size of the system's default colormap.
   // (See the description of struct #GdkColormap for an
   // explanation of the size of a colormap.)
   // RETURNS: the size of the system's default colormap.
   static int get_system_size()() nothrow {
      return gdk_colormap_get_system_size();
   }

   // Allocates a single color from a colormap.
   // RETURNS: %TRUE if the allocation succeeded.
   // <color>: the color to allocate. On return the <structfield>pixel</structfield> field will be filled in if allocation succeeds.
   // <writeable>: If %TRUE, the color is allocated writeable (their values can later be changed using gdk_color_change()). Writeable colors cannot be shared between applications.
   // <best_match>: If %TRUE, GDK will attempt to do matching against existing colors if the color cannot be allocated as requested.
   int alloc_color(AT0)(AT0 /*Color*/ color, int writeable, int best_match) nothrow {
      return gdk_colormap_alloc_color(&this, UpCast!(Color*)(color), writeable, best_match);
   }

   // Allocates colors from a colormap.
   // 
   // allocated.
   // RETURNS: The number of colors that were not successfully
   // <colors>: The color values to allocate. On return, the pixel values for allocated colors will be filled in.
   // <n_colors>: The number of colors in @colors.
   // <writeable>: If %TRUE, the colors are allocated writeable (their values can later be changed using gdk_color_change()). Writeable colors cannot be shared between applications.
   // <best_match>: If %TRUE, GDK will attempt to do matching against existing colors if the colors cannot be allocated as requested.
   // <success>: An array of length @ncolors. On return, this indicates whether the corresponding color in @colors was successfully allocated or not.
   int alloc_colors(AT0)(AT0 /*Color*/ colors, int n_colors, int writeable, int best_match, int* success) nothrow {
      return gdk_colormap_alloc_colors(&this, UpCast!(Color*)(colors), n_colors, writeable, best_match, success);
   }

   // Changes the value of the first @ncolors in a private colormap
   // to match the values in the <structfield>colors</structfield>
   // array in the colormap. This function is obsolete and
   // should not be used. See gdk_color_change().
   // <ncolors>: the number of colors to change.
   void change()(int ncolors) nothrow {
      gdk_colormap_change(&this, ncolors);
   }

   // Frees previously allocated colors.
   // <colors>: the colors to free.
   // <n_colors>: the number of colors in @colors.
   void free_colors(AT0)(AT0 /*Color*/ colors, int n_colors) nothrow {
      gdk_colormap_free_colors(&this, UpCast!(Color*)(colors), n_colors);
   }

   // Unintrospectable method: get_screen() / gdk_colormap_get_screen()
   // VERSION: 2.2
   // Gets the screen for which this colormap was created.
   // RETURNS: the screen for which this colormap was created.
   Screen* get_screen()() nothrow {
      return gdk_colormap_get_screen(&this);
   }

   // Unintrospectable method: get_visual() / gdk_colormap_get_visual()
   // Returns the visual for which a given colormap was created.
   // RETURNS: the visual of the colormap.
   Visual* get_visual()() nothrow {
      return gdk_colormap_get_visual(&this);
   }

   // Locates the RGB color in @colormap corresponding to the given
   // hardware pixel @pixel. @pixel must be a valid pixel in the
   // colormap; it's a programmer error to call this function with a
   // pixel which is not in the colormap. Hardware pixels are normally
   // obtained from gdk_colormap_alloc_colors(), or from a #GdkImage. (A
   // #GdkImage contains image data in hardware format, a #GdkPixbuf
   // contains image data in a canonical 24-bit RGB format.)
   // 
   // This function is rarely useful; it's used for example to
   // implement the eyedropper feature in #GtkColorSelection.
   // <pixel>: pixel value in hardware display format
   // <result>: #GdkColor with red, green, blue fields initialized
   void query_color(AT0)(c_ulong pixel, AT0 /*Color*/ result) nothrow {
      gdk_colormap_query_color(&this, pixel, UpCast!(Color*)(result));
   }

   // Unintrospectable method: ref() / gdk_colormap_ref()
   // DEPRECATED (v2.0) method: ref - Use g_object_ref() instead.
   // Deprecated function; use g_object_ref() instead.
   // RETURNS: the colormap
   Colormap* ref_()() nothrow {
      return gdk_colormap_ref(&this);
   }

   // DEPRECATED (v2.0) method: unref - Use g_object_unref() instead.
   // Deprecated function; use g_object_unref() instead.
   void unref()() nothrow {
      gdk_colormap_unref(&this);
   }
}

struct ColormapClass {
   GObject2.ObjectClass parent_class;
}

enum CrossingMode {
   NORMAL = 0,
   GRAB = 1,
   UNGRAB = 2,
   GTK_GRAB = 3,
   GTK_UNGRAB = 4,
   STATE_CHANGED = 5
}
struct Cursor {
   CursorType type;
   private uint ref_count;


   // Creates a new cursor from the set of builtin cursors for the default display.
   // See gdk_cursor_new_for_display().
   // 
   // To make the cursor invisible, use %GDK_BLANK_CURSOR.
   // RETURNS: a new #GdkCursor
   // <cursor_type>: cursor to create
   static Cursor* /*new*/ new_()(CursorType cursor_type) nothrow {
      return gdk_cursor_new(cursor_type);
   }
   static auto opCall()(CursorType cursor_type) {
      return gdk_cursor_new(cursor_type);
   }

   // VERSION: 2.2
   // Creates a new cursor from the set of builtin cursors.
   // Some useful ones are:
   // <itemizedlist>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="right_ptr.png"></inlinegraphic> #GDK_RIGHT_PTR (right-facing arrow)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="crosshair.png"></inlinegraphic> #GDK_CROSSHAIR (crosshair)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="xterm.png"></inlinegraphic> #GDK_XTERM (I-beam)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="watch.png"></inlinegraphic> #GDK_WATCH (busy)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="fleur.png"></inlinegraphic> #GDK_FLEUR (for moving objects)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="hand1.png"></inlinegraphic> #GDK_HAND1 (a right-pointing hand)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="hand2.png"></inlinegraphic> #GDK_HAND2 (a left-pointing hand)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="left_side.png"></inlinegraphic> #GDK_LEFT_SIDE (resize left side)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="right_side.png"></inlinegraphic> #GDK_RIGHT_SIDE (resize right side)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="top_left_corner.png"></inlinegraphic> #GDK_TOP_LEFT_CORNER (resize northwest corner)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="top_right_corner.png"></inlinegraphic> #GDK_TOP_RIGHT_CORNER (resize northeast corner)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="bottom_left_corner.png"></inlinegraphic> #GDK_BOTTOM_LEFT_CORNER (resize southwest corner)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="bottom_right_corner.png"></inlinegraphic> #GDK_BOTTOM_RIGHT_CORNER (resize southeast corner)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="top_side.png"></inlinegraphic> #GDK_TOP_SIDE (resize top side)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="bottom_side.png"></inlinegraphic> #GDK_BOTTOM_SIDE (resize bottom side)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="sb_h_double_arrow.png"></inlinegraphic> #GDK_SB_H_DOUBLE_ARROW (move vertical splitter)
   // </para></listitem>
   // <listitem><para>
   // <inlinegraphic format="PNG" fileref="sb_v_double_arrow.png"></inlinegraphic> #GDK_SB_V_DOUBLE_ARROW (move horizontal splitter)
   // </para></listitem>
   // <listitem><para>
   // #GDK_BLANK_CURSOR (Blank cursor). Since 2.16
   // </para></listitem>
   // </itemizedlist>
   // RETURNS: a new #GdkCursor
   // <display>: the #GdkDisplay for which the cursor will be created
   // <cursor_type>: cursor to create
   static Cursor* /*new*/ new_for_display(AT0)(AT0 /*Display*/ display, CursorType cursor_type) nothrow {
      return gdk_cursor_new_for_display(UpCast!(Display*)(display), cursor_type);
   }
   static auto opCall(AT0)(AT0 /*Display*/ display, CursorType cursor_type) {
      return gdk_cursor_new_for_display(UpCast!(Display*)(display), cursor_type);
   }

   // VERSION: 2.8
   // Creates a new cursor by looking up @name in the current cursor
   // theme. 
   // 
   // the given name
   // RETURNS: a new #GdkCursor, or %NULL if there is no cursor with
   // <display>: the #GdkDisplay for which the cursor will be created
   // <name>: the name of the cursor
   static Cursor* /*new*/ new_from_name(AT0, AT1)(AT0 /*Display*/ display, AT1 /*char*/ name) nothrow {
      return gdk_cursor_new_from_name(UpCast!(Display*)(display), toCString!(char*)(name));
   }
   static auto opCall(AT0, AT1)(AT0 /*Display*/ display, AT1 /*char*/ name) {
      return gdk_cursor_new_from_name(UpCast!(Display*)(display), toCString!(char*)(name));
   }

   // VERSION: 2.4
   // Creates a new cursor from a pixbuf. 
   // 
   // Not all GDK backends support RGBA cursors. If they are not 
   // supported, a monochrome approximation will be displayed. 
   // The functions gdk_display_supports_cursor_alpha() and 
   // gdk_display_supports_cursor_color() can be used to determine
   // whether RGBA cursors are supported; 
   // gdk_display_get_default_cursor_size() and 
   // gdk_display_get_maximal_cursor_size() give information about 
   // cursor sizes.
   // 
   // If @x or @y are <literal>-1</literal>, the pixbuf must have
   // options named "x_hot" and "y_hot", resp., containing
   // integer values between %0 and the width resp. height of
   // the pixbuf. (Since: 3.0)
   // 
   // On the X backend, support for RGBA cursors requires a
   // sufficently new version of the X Render extension.
   // RETURNS: a new #GdkCursor.
   // <display>: the #GdkDisplay for which the cursor will be created
   // <pixbuf>: the #GdkPixbuf containing the cursor image
   // <x>: the horizontal offset of the 'hotspot' of the cursor.
   // <y>: the vertical offset of the 'hotspot' of the cursor.
   static Cursor* /*new*/ new_from_pixbuf(AT0, AT1)(AT0 /*Display*/ display, AT1 /*GdkPixbuf2.Pixbuf*/ pixbuf, int x, int y) nothrow {
      return gdk_cursor_new_from_pixbuf(UpCast!(Display*)(display), UpCast!(GdkPixbuf2.Pixbuf*)(pixbuf), x, y);
   }
   static auto opCall(AT0, AT1)(AT0 /*Display*/ display, AT1 /*GdkPixbuf2.Pixbuf*/ pixbuf, int x, int y) {
      return gdk_cursor_new_from_pixbuf(UpCast!(Display*)(display), UpCast!(GdkPixbuf2.Pixbuf*)(pixbuf), x, y);
   }

   // Creates a new cursor from a given pixmap and mask. Both the pixmap and mask
   // must have a depth of 1 (i.e. each pixel has only 2 values - on or off).
   // The standard cursor size is 16 by 16 pixels. You can create a bitmap 
   // from inline data as in the below example.
   // 
   // <example><title>Creating a custom cursor</title>
   // <programlisting>
   // /<!-- -->* This data is in X bitmap format, and can be created with the 'bitmap'
   // utility. *<!-- -->/
   // &num;define cursor1_width 16
   // &num;define cursor1_height 16
   // static unsigned char cursor1_bits[] = {
   // 0x80, 0x01, 0x40, 0x02, 0x20, 0x04, 0x10, 0x08, 0x08, 0x10, 0x04, 0x20,
   // 0x82, 0x41, 0x41, 0x82, 0x41, 0x82, 0x82, 0x41, 0x04, 0x20, 0x08, 0x10,
   // 0x10, 0x08, 0x20, 0x04, 0x40, 0x02, 0x80, 0x01};
   // 
   // static unsigned char cursor1mask_bits[] = {
   // 0x80, 0x01, 0xc0, 0x03, 0x60, 0x06, 0x30, 0x0c, 0x18, 0x18, 0x8c, 0x31,
   // 0xc6, 0x63, 0x63, 0xc6, 0x63, 0xc6, 0xc6, 0x63, 0x8c, 0x31, 0x18, 0x18,
   // 0x30, 0x0c, 0x60, 0x06, 0xc0, 0x03, 0x80, 0x01};
   // 
   // 
   // GdkCursor *cursor;
   // GdkPixmap *source, *mask;
   // GdkColor fg = { 0, 65535, 0, 0 }; /<!-- -->* Red. *<!-- -->/
   // GdkColor bg = { 0, 0, 0, 65535 }; /<!-- -->* Blue. *<!-- -->/
   // 
   // 
   // source = gdk_bitmap_create_from_data (NULL, cursor1_bits,
   // cursor1_width, cursor1_height);
   // mask = gdk_bitmap_create_from_data (NULL, cursor1mask_bits,
   // cursor1_width, cursor1_height);
   // cursor = gdk_cursor_new_from_pixmap (source, mask, &amp;fg, &amp;bg, 8, 8);
   // g_object_unref (source);
   // g_object_unref (mask);
   // 
   // 
   // gdk_window_set_cursor (widget->window, cursor);
   // </programlisting>
   // </example>
   // RETURNS: a new #GdkCursor.
   // <source>: the pixmap specifying the cursor.
   // <mask>: the pixmap specifying the mask, which must be the same size as @source.
   // <fg>: the foreground color, used for the bits in the source which are 1. The color does not have to be allocated first.
   // <bg>: the background color, used for the bits in the source which are 0. The color does not have to be allocated first.
   // <x>: the horizontal offset of the 'hotspot' of the cursor.
   // <y>: the vertical offset of the 'hotspot' of the cursor.
   static Cursor* /*new*/ new_from_pixmap(AT0, AT1, AT2, AT3)(AT0 /*Pixmap*/ source, AT1 /*Pixmap*/ mask, AT2 /*Color*/ fg, AT3 /*Color*/ bg, int x, int y) nothrow {
      return gdk_cursor_new_from_pixmap(UpCast!(Pixmap*)(source), UpCast!(Pixmap*)(mask), UpCast!(Color*)(fg), UpCast!(Color*)(bg), x, y);
   }
   static auto opCall(AT0, AT1, AT2, AT3)(AT0 /*Pixmap*/ source, AT1 /*Pixmap*/ mask, AT2 /*Color*/ fg, AT3 /*Color*/ bg, int x, int y) {
      return gdk_cursor_new_from_pixmap(UpCast!(Pixmap*)(source), UpCast!(Pixmap*)(mask), UpCast!(Color*)(fg), UpCast!(Color*)(bg), x, y);
   }

   // VERSION: 2.22
   // Returns the cursor type for this cursor.
   // RETURNS: a #GdkCursorType
   CursorType get_cursor_type()() nothrow {
      return gdk_cursor_get_cursor_type(&this);
   }

   // Unintrospectable method: get_display() / gdk_cursor_get_display()
   // VERSION: 2.2
   // Returns the display on which the #GdkCursor is defined.
   // RETURNS: the #GdkDisplay associated to @cursor
   Display* get_display()() nothrow {
      return gdk_cursor_get_display(&this);
   }

   // Unintrospectable method: get_image() / gdk_cursor_get_image()
   // VERSION: 2.8
   // Returns a #GdkPixbuf with the image used to display the cursor.
   // 
   // Note that depending on the capabilities of the windowing system and 
   // on the cursor, GDK may not be able to obtain the image data. In this 
   // case, %NULL is returned.
   // RETURNS: a #GdkPixbuf representing @cursor, or %NULL
   GdkPixbuf2.Pixbuf* get_image()() nothrow {
      return gdk_cursor_get_image(&this);
   }

   // Adds a reference to @cursor.
   // RETURNS: Same @cursor that was passed in
   Cursor* /*new*/ ref_()() nothrow {
      return gdk_cursor_ref(&this);
   }

   // Removes a reference from @cursor, deallocating the cursor
   // if no references remain.
   void unref()() nothrow {
      gdk_cursor_unref(&this);
   }
}

enum CursorType {
   X_CURSOR = 0,
   ARROW = 2,
   BASED_ARROW_DOWN = 4,
   BASED_ARROW_UP = 6,
   BOAT = 8,
   BOGOSITY = 10,
   BOTTOM_LEFT_CORNER = 12,
   BOTTOM_RIGHT_CORNER = 14,
   BOTTOM_SIDE = 16,
   BOTTOM_TEE = 18,
   BOX_SPIRAL = 20,
   CENTER_PTR = 22,
   CIRCLE = 24,
   CLOCK = 26,
   COFFEE_MUG = 28,
   CROSS = 30,
   CROSS_REVERSE = 32,
   CROSSHAIR = 34,
   DIAMOND_CROSS = 36,
   DOT = 38,
   DOTBOX = 40,
   DOUBLE_ARROW = 42,
   DRAFT_LARGE = 44,
   DRAFT_SMALL = 46,
   DRAPED_BOX = 48,
   EXCHANGE = 50,
   FLEUR = 52,
   GOBBLER = 54,
   GUMBY = 56,
   HAND1 = 58,
   HAND2 = 60,
   HEART = 62,
   ICON = 64,
   IRON_CROSS = 66,
   LEFT_PTR = 68,
   LEFT_SIDE = 70,
   LEFT_TEE = 72,
   LEFTBUTTON = 74,
   LL_ANGLE = 76,
   LR_ANGLE = 78,
   MAN = 80,
   MIDDLEBUTTON = 82,
   MOUSE = 84,
   PENCIL = 86,
   PIRATE = 88,
   PLUS = 90,
   QUESTION_ARROW = 92,
   RIGHT_PTR = 94,
   RIGHT_SIDE = 96,
   RIGHT_TEE = 98,
   RIGHTBUTTON = 100,
   RTL_LOGO = 102,
   SAILBOAT = 104,
   SB_DOWN_ARROW = 106,
   SB_H_DOUBLE_ARROW = 108,
   SB_LEFT_ARROW = 110,
   SB_RIGHT_ARROW = 112,
   SB_UP_ARROW = 114,
   SB_V_DOUBLE_ARROW = 116,
   SHUTTLE = 118,
   SIZING = 120,
   SPIDER = 122,
   SPRAYCAN = 124,
   STAR = 126,
   TARGET = 128,
   TCROSS = 130,
   TOP_LEFT_ARROW = 132,
   TOP_LEFT_CORNER = 134,
   TOP_RIGHT_CORNER = 136,
   TOP_SIDE = 138,
   TOP_TEE = 140,
   TREK = 142,
   UL_ANGLE = 144,
   UMBRELLA = 146,
   UR_ANGLE = 148,
   WATCH = 150,
   XTERM = 152,
   LAST_CURSOR = 153,
   BLANK_CURSOR = -2,
   CURSOR_IS_PIXMAP = -1
}
extern (C) alias void function (void* data) nothrow DestroyNotify;

struct Device /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   char* name;
   InputSource source;
   InputMode mode;
   int has_cursor;
   int num_axes;
   DeviceAxis* axes;
   int num_keys;
   DeviceKey* keys;


   // Frees an array of #GdkTimeCoord that was returned by gdk_device_get_history().
   // <events>: an array of #GdkTimeCoord.
   // <n_events>: the length of the array.
   static void free_history(AT0)(/*inout*/ AT0 /*TimeCoord**/ events, int n_events) nothrow {
      gdk_device_free_history(UpCast!(TimeCoord**)(events), n_events);
   }

   // Unintrospectable function: get_core_pointer() / gdk_device_get_core_pointer()
   // Returns the core pointer device for the default display.
   // 
   // display and should not be freed.
   // RETURNS: the core pointer device; this is owned by the
   static Device* get_core_pointer()() nothrow {
      return gdk_device_get_core_pointer();
   }

   // Interprets an array of double as axis values for a given device,
   // and locates the value in the array for a given axis use.
   // RETURNS: %TRUE if the given axis use was found, otherwise %FALSE
   // <axes>: pointer to an array of axes
   // <use>: the use to look for
   // <value>: location to store the found value.
   int get_axis(AT0, AT1)(AT0 /*double*/ axes, AxisUse use, AT1 /*double*/ value) nothrow {
      return gdk_device_get_axis(&this, UpCast!(double*)(axes), use, UpCast!(double*)(value));
   }

   // VERSION: 2.22
   // Returns the axis use for @index.
   // RETURNS: a #GdkAxisUse specifying how the axis is used.
   // <index>: the index of the axis.
   AxisUse get_axis_use()(uint index) nothrow {
      return gdk_device_get_axis_use(&this, index);
   }

   // VERSION: 2.22
   // Determines whether the pointer follows device motion.
   // RETURNS: %TRUE if the pointer follows device motion
   int get_has_cursor()() nothrow {
      return gdk_device_get_has_cursor(&this);
   }

   // Obtains the motion history for a device; given a starting and
   // ending timestamp, return all events in the motion history for
   // the device in the given range of time. Some windowing systems
   // do not support motion history, in which case, %FALSE will
   // be returned. (This is not distinguishable from the case where
   // motion history is supported and no events were found.)
   // 
   // at least one event was found.
   // RETURNS: %TRUE if the windowing system supports motion history and
   // <window>: the window with respect to which which the event coordinates will be reported
   // <start>: starting timestamp for range of events to return
   // <stop>: ending timestamp for the range of events to return
   // <events>: location to store a newly-allocated array of #GdkTimeCoord, or %NULL
   // <n_events>: location to store the length of @events, or %NULL
   int get_history(AT0, AT1)(AT0 /*Window*/ window, uint start, uint stop, /*out*/ AT1 /*TimeCoord***/ events, /*out*/ int* n_events) nothrow {
      return gdk_device_get_history(&this, UpCast!(Window*)(window), start, stop, UpCast!(TimeCoord***)(events), n_events);
   }

   // VERSION: 2.22
   // If @index has a valid keyval, this function will
   // fill in @keyval and @modifiers with the keyval settings.
   // <index>: the index of the macro button to get.
   // <keyval>: return value for the keyval.
   // <modifiers>: return value for modifiers.
   void get_key(AT0, AT1)(uint index, AT0 /*uint*/ keyval, AT1 /*ModifierType*/ modifiers) nothrow {
      gdk_device_get_key(&this, index, UpCast!(uint*)(keyval), UpCast!(ModifierType*)(modifiers));
   }

   // VERSION: 2.22
   // Determines the mode of the device.
   // RETURNS: a #GdkInputSource
   InputMode get_mode()() nothrow {
      return gdk_device_get_mode(&this);
   }

   // VERSION: 2.22
   // Gets the number of axes of a device.
   // RETURNS: the number of axes of @device
   int get_n_axes()() nothrow {
      return gdk_device_get_n_axes(&this);
   }

   // VERSION: 2.24
   // Gets the number of keys of a device.
   // RETURNS: the number of keys of @device
   int get_n_keys()() nothrow {
      return gdk_device_get_n_keys(&this);
   }

   // VERSION: 2.22
   // Determines the name of the device.
   // RETURNS: a name
   char* get_name()() nothrow {
      return gdk_device_get_name(&this);
   }

   // VERSION: 2.22
   // Determines the type of the device.
   // RETURNS: a #GdkInputSource
   InputSource get_source()() nothrow {
      return gdk_device_get_source(&this);
   }
   void get_state(AT0, AT1, AT2)(AT0 /*Window*/ window, AT1 /*double*/ axes, AT2 /*ModifierType*/ mask) nothrow {
      gdk_device_get_state(&this, UpCast!(Window*)(window), UpCast!(double*)(axes), UpCast!(ModifierType*)(mask));
   }
   void set_axis_use()(uint index_, AxisUse use) nothrow {
      gdk_device_set_axis_use(&this, index_, use);
   }
   void set_key()(uint index_, uint keyval, ModifierType modifiers) nothrow {
      gdk_device_set_key(&this, index_, keyval, modifiers);
   }
   int set_mode()(InputMode mode) nothrow {
      return gdk_device_set_mode(&this, mode);
   }
   void set_source()(InputSource source) nothrow {
      gdk_device_set_source(&this, source);
   }
}

struct DeviceAxis {
   AxisUse use;
   double min, max;
}

struct DeviceClass {
}

struct DeviceKey {
   uint keyval;
   ModifierType modifiers;
}

struct Display /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private GLib2.List* queued_events, queued_tail;
   private uint[2] button_click_time;
   private Window*[2] button_window;
   private int[2] button_number;
   private uint double_click_time;
   private Device* core_pointer;
   private DisplayPointerHooks* pointer_hooks;
    static import std.bitmanip; mixin(std.bitmanip.bitfields!(
   uint, "closed_", 1,
   uint, "ignore_core_events", 1,
    uint, "__dummy32A", 30));
   private uint double_click_distance;
   private int[2] button_x, button_y;
   private GLib2.List* pointer_grabs;
   private KeyboardGrabInfo keyboard_grab;
   private PointerWindowInfo pointer_info;
   private uint last_event_time;


   // VERSION: 2.2
   // Gets the default #GdkDisplay. This is a convenience
   // function for
   // <literal>gdk_display_manager_get_default_display (gdk_display_manager_get ())</literal>.
   // 
   // display.
   // RETURNS: a #GdkDisplay, or %NULL if there is no default
   static Display* get_default()() nothrow {
      return gdk_display_get_default();
   }

   // Unintrospectable function: open() / gdk_display_open()
   // VERSION: 2.2
   // Opens a display.
   // RETURNS: a #GdkDisplay, or %NULL if the display could not be opened.
   // <display_name>: the name of the display to open
   static Display* open(AT0)(AT0 /*char*/ display_name) nothrow {
      return gdk_display_open(toCString!(char*)(display_name));
   }

   // Unintrospectable function: open_default_libgtk_only() / gdk_display_open_default_libgtk_only()
   // Opens the default display specified by command line arguments or
   // environment variables, sets it as the default display, and returns
   // it.  gdk_parse_args must have been called first. If the default
   // display has previously been set, simply returns that. An internal
   // function that should not be used by applications.
   // 
   // otherwise %NULL.
   // RETURNS: the default display, if it could be opened,
   static Display* open_default_libgtk_only()() nothrow {
      return gdk_display_open_default_libgtk_only();
   }
   // Unintrospectable method: add_client_message_filter() / gdk_display_add_client_message_filter()
   void add_client_message_filter(AT0)(Atom message_type, FilterFunc func, AT0 /*void*/ data) nothrow {
      gdk_display_add_client_message_filter(&this, message_type, func, UpCast!(void*)(data));
   }

   // VERSION: 2.2
   // Emits a short beep on @display
   void beep()() nothrow {
      gdk_display_beep(&this);
   }

   // VERSION: 2.2
   // Closes the connection to the windowing system for the given display,
   // and cleans up associated resources.
   void close()() nothrow {
      gdk_display_close(&this);
   }

   // VERSION: 2.4
   // Flushes any requests queued for the windowing system; this happens automatically
   // when the main loop blocks waiting for new events, but if your application
   // is drawing without returning control to the main loop, you may need
   // to call this function explicitely. A common case where this function
   // needs to be called is when an application is executing drawing commands
   // from a thread other than the thread where the main loop is running.
   // 
   // This is most useful for X11. On windowing systems where requests are
   // handled synchronously, this function will do nothing.
   void flush()() nothrow {
      gdk_display_flush(&this);
   }

   // Unintrospectable method: get_core_pointer() / gdk_display_get_core_pointer()
   // VERSION: 2.2
   // Returns the core pointer device for the given display
   // 
   // display and should not be freed.
   // RETURNS: the core pointer device; this is owned by the
   Device* get_core_pointer()() nothrow {
      return gdk_display_get_core_pointer(&this);
   }

   // VERSION: 2.4
   // Returns the default size to use for cursors on @display.
   // RETURNS: the default cursor size.
   uint get_default_cursor_size()() nothrow {
      return gdk_display_get_default_cursor_size(&this);
   }

   // Unintrospectable method: get_default_group() / gdk_display_get_default_group()
   // VERSION: 2.4
   // Returns the default group leader window for all toplevel windows
   // on @display. This window is implicitly created by GDK. 
   // See gdk_window_set_group().
   // RETURNS: The default group leader window for @display
   Window* get_default_group()() nothrow {
      return gdk_display_get_default_group(&this);
   }

   // Unintrospectable method: get_default_screen() / gdk_display_get_default_screen()
   // VERSION: 2.2
   // Get the default #GdkScreen for @display.
   // RETURNS: the default #GdkScreen object for @display
   Screen* get_default_screen()() nothrow {
      return gdk_display_get_default_screen(&this);
   }

   // VERSION: 2.2
   // Gets the next #GdkEvent to be processed for @display, fetching events from the
   // windowing system if necessary.
   // 
   // are pending. The returned #GdkEvent should be freed with gdk_event_free().
   // RETURNS: the next #GdkEvent to be processed, or %NULL if no events
   Event* /*new*/ get_event()() nothrow {
      return gdk_display_get_event(&this);
   }

   // VERSION: 2.4
   // Gets the maximal size to use for cursors on @display.
   // <width>: the return location for the maximal cursor width
   // <height>: the return location for the maximal cursor height
   void get_maximal_cursor_size(AT0, AT1)(/*out*/ AT0 /*uint*/ width, /*out*/ AT1 /*uint*/ height) nothrow {
      gdk_display_get_maximal_cursor_size(&this, UpCast!(uint*)(width), UpCast!(uint*)(height));
   }

   // VERSION: 2.2
   // Gets the number of screen managed by the @display.
   // RETURNS: number of screens.
   int get_n_screens()() nothrow {
      return gdk_display_get_n_screens(&this);
   }

   // VERSION: 2.2
   // Gets the name of the display.
   // 
   // by GDK and should not be modified or freed.
   // RETURNS: a string representing the display name. This string is owned
   char* get_name()() nothrow {
      return gdk_display_get_name(&this);
   }

   // VERSION: 2.2
   // Gets the current location of the pointer and the current modifier
   // mask for a given display.
   // <screen>: location to store the screen that the cursor is on, or %NULL.
   // <x>: location to store root window X coordinate of pointer, or %NULL.
   // <y>: location to store root window Y coordinate of pointer, or %NULL.
   // <mask>: location to store current modifier mask, or %NULL
   void get_pointer(AT0, AT1)(/*out*/ AT0 /*Screen**/ screen=null, /*out*/ int* x=null, /*out*/ int* y=null, /*out*/ AT1 /*ModifierType*/ mask=null) nothrow {
      gdk_display_get_pointer(&this, UpCast!(Screen**)(screen), x, y, UpCast!(ModifierType*)(mask));
   }

   // Unintrospectable method: get_screen() / gdk_display_get_screen()
   // VERSION: 2.2
   // Returns a screen object for one of the screens of the display.
   // RETURNS: the #GdkScreen object
   // <screen_num>: the screen number
   Screen* get_screen()(int screen_num) nothrow {
      return gdk_display_get_screen(&this, screen_num);
   }

   // VERSION: 2.2
   // Obtains the window underneath the mouse pointer, returning the location
   // of the pointer in that window in @win_x, @win_y for @screen. Returns %NULL
   // if the window under the mouse pointer is not known to GDK (for example, 
   // belongs to another application).
   // RETURNS: the window under the mouse pointer, or %NULL
   // <win_x>: return location for x coordinate of the pointer location relative to the window origin, or %NULL
   // <win_y>: return location for y coordinate of the pointer location relative
   Window* get_window_at_pointer()(/*out*/ int* win_x=null, /*out*/ int* win_y=null) nothrow {
      return gdk_display_get_window_at_pointer(&this, win_x, win_y);
   }

   // VERSION: 2.22
   // Finds out if the display has been closed.
   // RETURNS: %TRUE if the display is closed.
   int is_closed()() nothrow {
      return gdk_display_is_closed(&this);
   }

   // VERSION: 2.2
   // Release any keyboard grab
   // <time_>: a timestap (e.g #GDK_CURRENT_TIME).
   void keyboard_ungrab()(uint time_) nothrow {
      gdk_display_keyboard_ungrab(&this, time_);
   }

   // Unintrospectable method: list_devices() / gdk_display_list_devices()
   // VERSION: 2.2
   // Returns the list of available input devices attached to @display.
   // The list is statically allocated and should not be freed.
   // RETURNS: a list of #GdkDevice
   GLib2.List* list_devices()() nothrow {
      return gdk_display_list_devices(&this);
   }

   // VERSION: 2.2
   // Gets a copy of the first #GdkEvent in the @display's event queue, without
   // removing the event from the queue.  (Note that this function will
   // not get more events from the windowing system.  It only checks the events
   // that have already been moved to the GDK event queue.)
   // 
   // if no events are in the queue. The returned #GdkEvent should be freed with
   // gdk_event_free().
   // RETURNS: a copy of the first #GdkEvent on the event queue, or %NULL
   Event* /*new*/ peek_event()() nothrow {
      return gdk_display_peek_event(&this);
   }

   // VERSION: 2.2
   // Test if the pointer is grabbed.
   // RETURNS: %TRUE if an active X pointer grab is in effect
   int pointer_is_grabbed()() nothrow {
      return gdk_display_pointer_is_grabbed(&this);
   }

   // VERSION: 2.2
   // Release any pointer grab.
   // <time_>: a timestap (e.g. %GDK_CURRENT_TIME).
   void pointer_ungrab()(uint time_) nothrow {
      gdk_display_pointer_ungrab(&this, time_);
   }

   // VERSION: 2.2
   // Appends a copy of the given event onto the front of the event
   // queue for @display.
   // <event>: a #GdkEvent.
   void put_event(AT0)(AT0 /*Event*/ event) nothrow {
      gdk_display_put_event(&this, UpCast!(Event*)(event));
   }

   // VERSION: 2.6
   // Request #GdkEventOwnerChange events for ownership changes
   // of the selection named by the given atom.
   // 
   // be sent.
   // RETURNS: whether #GdkEventOwnerChange events will
   // <selection>: the #GdkAtom naming the selection for which ownership change notification is requested
   int request_selection_notification()(Atom selection) nothrow {
      return gdk_display_request_selection_notification(&this, selection);
   }

   // VERSION: 2.4
   // Sets the double click distance (two clicks within this distance
   // count as a double click and result in a #GDK_2BUTTON_PRESS event).
   // See also gdk_display_set_double_click_time().
   // Applications should <emphasis>not</emphasis> set this, it is a global 
   // user-configured setting.
   // <distance>: distance in pixels
   void set_double_click_distance()(uint distance) nothrow {
      gdk_display_set_double_click_distance(&this, distance);
   }

   // VERSION: 2.2
   // Sets the double click time (two clicks within this time interval
   // count as a double click and result in a #GDK_2BUTTON_PRESS event).
   // Applications should <emphasis>not</emphasis> set this, it is a global 
   // user-configured setting.
   // <msec>: double click time in milliseconds (thousandths of a second)
   void set_double_click_time()(uint msec) nothrow {
      gdk_display_set_double_click_time(&this, msec);
   }

   // Unintrospectable method: set_pointer_hooks() / gdk_display_set_pointer_hooks()
   // VERSION: 2.2
   // DEPRECATED (v2.24) method: set_pointer_hooks - This function will go away in GTK 3 for lack of use cases.
   // This function allows for hooking into the operation
   // of getting the current location of the pointer on a particular
   // display. This is only useful for such low-level tools as an
   // event recorder. Applications should never have any
   // reason to use this facility.
   // RETURNS: the previous pointer hook table
   // <new_hooks>: a table of pointers to functions for getting quantities related to the current pointer position, or %NULL to restore the default table.
   DisplayPointerHooks* set_pointer_hooks(AT0)(AT0 /*DisplayPointerHooks*/ new_hooks) nothrow {
      return gdk_display_set_pointer_hooks(&this, UpCast!(DisplayPointerHooks*)(new_hooks));
   }

   // VERSION: 2.6
   // Issues a request to the clipboard manager to store the
   // clipboard data. On X11, this is a special program that works
   // according to the freedesktop clipboard specification, available at
   // <ulink url="http://www.freedesktop.org/Standards/clipboard-manager-spec">
   // http://www.freedesktop.org/Standards/clipboard-manager-spec</ulink>.
   // <clipboard_window>: a #GdkWindow belonging to the clipboard owner
   // <time_>: a timestamp if all available targets should be saved.
   // <n_targets>: length of the @targets array
   void store_clipboard(AT0, AT1)(AT0 /*Window*/ clipboard_window, uint time_, AT1 /*Atom*/ targets, int n_targets) nothrow {
      gdk_display_store_clipboard(&this, UpCast!(Window*)(clipboard_window), time_, UpCast!(Atom*)(targets), n_targets);
   }

   // VERSION: 2.6
   // Returns whether the speicifed display supports clipboard
   // persistance; i.e. if it's possible to store the clipboard data after an
   // application has quit. On X11 this checks if a clipboard daemon is
   // running.
   // RETURNS: %TRUE if the display supports clipboard persistance.
   int supports_clipboard_persistence()() nothrow {
      return gdk_display_supports_clipboard_persistence(&this);
   }

   // VERSION: 2.12
   // Returns %TRUE if gdk_window_set_composited() can be used
   // to redirect drawing on the window using compositing.
   // 
   // Currently this only works on X11 with XComposite and
   // XDamage extensions available.
   // RETURNS: %TRUE if windows may be composited.
   int supports_composite()() nothrow {
      return gdk_display_supports_composite(&this);
   }

   // VERSION: 2.4
   // Returns %TRUE if cursors can use an 8bit alpha channel 
   // on @display. Otherwise, cursors are restricted to bilevel 
   // alpha (i.e. a mask).
   // RETURNS: whether cursors can have alpha channels.
   int supports_cursor_alpha()() nothrow {
      return gdk_display_supports_cursor_alpha(&this);
   }

   // VERSION: 2.4
   // Returns %TRUE if multicolored cursors are supported
   // on @display. Otherwise, cursors have only a forground
   // and a background color.
   // RETURNS: whether cursors can have multiple colors.
   int supports_cursor_color()() nothrow {
      return gdk_display_supports_cursor_color(&this);
   }

   // VERSION: 2.10
   // Returns %TRUE if gdk_window_input_shape_combine_mask() can
   // be used to modify the input shape of windows on @display.
   // RETURNS: %TRUE if windows with modified input shape are supported
   int supports_input_shapes()() nothrow {
      return gdk_display_supports_input_shapes(&this);
   }

   // VERSION: 2.6
   // Returns whether #GdkEventOwnerChange events will be 
   // sent when the owner of a selection changes.
   // 
   // be sent.
   // RETURNS: whether #GdkEventOwnerChange events will
   int supports_selection_notification()() nothrow {
      return gdk_display_supports_selection_notification(&this);
   }

   // VERSION: 2.10
   // Returns %TRUE if gdk_window_shape_combine_mask() can
   // be used to create shaped windows on @display.
   // RETURNS: %TRUE if shaped windows are supported
   int supports_shapes()() nothrow {
      return gdk_display_supports_shapes(&this);
   }

   // VERSION: 2.2
   // Flushes any requests queued for the windowing system and waits until all
   // requests have been handled. This is often used for making sure that the
   // display is synchronized with the current state of the program. Calling
   // gdk_display_sync() before gdk_error_trap_pop() makes sure that any errors
   // generated from earlier requests are handled before the error trap is 
   // removed.
   // 
   // This is most useful for X11. On windowing systems where requests are
   // handled synchronously, this function will do nothing.
   void sync()() nothrow {
      gdk_display_sync(&this);
   }

   // VERSION: 2.8
   // Warps the pointer of @display to the point @x,@y on 
   // the screen @screen, unless the pointer is confined
   // to a window by a grab, in which case it will be moved
   // as far as allowed by the grab. Warping the pointer 
   // creates events as if the user had moved the mouse 
   // instantaneously to the destination.
   // 
   // Note that the pointer should normally be under the
   // control of the user. This function was added to cover
   // some rare use cases like keyboard navigation support
   // for the color picker in the #GtkColorSelectionDialog.
   // <screen>: the screen of @display to warp the pointer to
   // <x>: the x coordinate of the destination
   // <y>: the y coordinate of the destination
   void warp_pointer(AT0)(AT0 /*Screen*/ screen, int x, int y) nothrow {
      gdk_display_warp_pointer(&this, UpCast!(Screen*)(screen), x, y);
   }

   // VERSION: 2.2
   // The ::closed signal is emitted when the connection to the windowing
   // system for @display is closed.
   // <is_error>: %TRUE if the display was closed due to an error
   extern (C) alias static void function (Display* this_, c_int is_error, void* user_data=null) nothrow signal_closed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"closed", CB/*:signal_closed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_closed)||_ttmm!(CB, signal_closed)()) {
      return signal_connect_data!()(&this, cast(char*)"closed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct DisplayClass {
   GObject2.ObjectClass parent_class;
   extern (C) char* function (Display* display) nothrow get_display_name;
   // RETURNS: number of screens.
   extern (C) int function (Display* display) nothrow get_n_screens;

   // Unintrospectable functionp: get_screen() / ()
   // 
   // RETURNS: the #GdkScreen object
   // <screen_num>: the screen number
   extern (C) Screen* function (Display* display, int screen_num) nothrow get_screen;

   // Unintrospectable functionp: get_default_screen() / ()
   // 
   // RETURNS: the default #GdkScreen object for @display
   extern (C) Screen* function (Display* display) nothrow get_default_screen;
   extern (C) void function (Display* display, int is_error) nothrow closed;
}

struct DisplayManager /* : GObject.Object */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // VERSION: 2.2
   // Gets the singleton #GdkDisplayManager object.
   // 
   // gdk_init(), or gdk_init_check() must have been called first.
   // RETURNS: The global #GdkDisplayManager singleton; gdk_parse_pargs(),
   static DisplayManager* get()() nothrow {
      return gdk_display_manager_get();
   }

   // VERSION: 2.2
   // Gets the default #GdkDisplay.
   // 
   // display.
   // RETURNS: a #GdkDisplay, or %NULL if there is no default
   Display* get_default_display()() nothrow {
      return gdk_display_manager_get_default_display(&this);
   }

   // VERSION: 2.2
   // List all currently open displays.
   // 
   // #GSList of #GdkDisplay objects. Free this list with g_slist_free() when you
   // are done with it.
   // RETURNS: a newly allocated
   GLib2.SList* /*new container*/ list_displays()() nothrow {
      return gdk_display_manager_list_displays(&this);
   }

   // VERSION: 2.2
   // Sets @display as the default display.
   // <display>: a #GdkDisplay
   void set_default_display(AT0)(AT0 /*Display*/ display) nothrow {
      gdk_display_manager_set_default_display(&this, UpCast!(Display*)(display));
   }

   // VERSION: 2.2
   // The ::display_opened signal is emitted when a display is opened.
   // <display>: the opened display
   extern (C) alias static void function (DisplayManager* this_, Display* display, void* user_data=null) nothrow signal_display_opened;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"display-opened", CB/*:signal_display_opened*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_display_opened)||_ttmm!(CB, signal_display_opened)()) {
      return signal_connect_data!()(&this, cast(char*)"display-opened",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct DisplayManagerClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function (DisplayManager* display_manager, Display* display) nothrow display_opened;
}

struct DisplayPointerHooks {
   extern (C) void function (Display* display, Screen** screen, int* x, int* y, ModifierType* mask) nothrow get_pointer;
   // Unintrospectable functionp: window_get_pointer() / ()
   extern (C) Window* function (Display* display, Window* window, int* x, int* y, ModifierType* mask) nothrow window_get_pointer;
   // Unintrospectable functionp: window_at_pointer() / ()
   extern (C) Window* function (Display* display, int* win_x, int* win_y) nothrow window_at_pointer;
}

enum DragAction {
   DEFAULT = 1,
   COPY = 2,
   MOVE = 4,
   LINK = 8,
   PRIVATE = 16,
   ASK = 32
}
struct DragContext /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   DragProtocol protocol;
   int is_source;
   Window* source_window, dest_window;
   GLib2.List* targets;
   DragAction actions, suggested_action, action;
   uint start_time;
   private void* windowing_data;


   // DEPRECATED (v2.24) constructor: new - This function is not useful, you always
   // Creates a new #GdkDragContext.
   // 
   // 
   // obtain drag contexts by gdk_drag_begin() or similar.
   // RETURNS: the newly created #GdkDragContext.
   static DragContext* /*new*/ new_()() nothrow {
      return gdk_drag_context_new();
   }
   static auto opCall()() {
      return gdk_drag_context_new();
   }

   // VERSION: 2.22
   // Determines the bitmask of actions proposed by the source if
   // gdk_drag_context_suggested_action() returns GDK_ACTION_ASK.
   // RETURNS: the #GdkDragAction flags
   DragAction get_actions()() nothrow {
      return gdk_drag_context_get_actions(&this);
   }

   // VERSION: 2.24
   // Returns the destination windw for the DND operation.
   // RETURNS: a #GdkWindow
   Window* get_dest_window()() nothrow {
      return gdk_drag_context_get_dest_window(&this);
   }

   // VERSION: 2.24
   // Returns the drag protocol thats used by this context.
   // RETURNS: the drag protocol
   DragProtocol get_protocol()() nothrow {
      return gdk_drag_context_get_protocol(&this);
   }

   // VERSION: 2.22
   // Determines the action chosen by the drag destination.
   // RETURNS: a #GdkDragAction value
   DragAction get_selected_action()() nothrow {
      return gdk_drag_context_get_selected_action(&this);
   }

   // VERSION: 2.22
   // Returns the #GdkWindow where the DND operation started.
   // RETURNS: a #GdkWindow
   Window* get_source_window()() nothrow {
      return gdk_drag_context_get_source_window(&this);
   }

   // VERSION: 2.22
   // Determines the suggested drag action of the context.
   // RETURNS: a #GdkDragAction value
   DragAction get_suggested_action()() nothrow {
      return gdk_drag_context_get_suggested_action(&this);
   }

   // VERSION: 2.22
   // Retrieves the list of targets of the context.
   // RETURNS: a #GList of targets
   GLib2.List* list_targets()() nothrow {
      return gdk_drag_context_list_targets(&this);
   }

   // DEPRECATED (v2.2) method: ref - Use g_object_ref() instead.
   // Deprecated function; use g_object_ref() instead.
   void ref_()() nothrow {
      gdk_drag_context_ref(&this);
   }

   // DEPRECATED (v2.2) method: unref - Use g_object_unref() instead.
   // Deprecated function; use g_object_unref() instead.
   void unref()() nothrow {
      gdk_drag_context_unref(&this);
   }
}

struct DragContextClass {
   GObject2.ObjectClass parent_class;
}

enum DragProtocol {
   MOTIF = 0,
   XDND = 1,
   ROOTWIN = 2,
   NONE = 3,
   WIN32_DROPFILES = 4,
   OLE2 = 5,
   LOCAL = 6
}
struct Drawable /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;


   // Unintrospectable method: copy_to_image() / gdk_drawable_copy_to_image()
   // VERSION: 2.4
   // DEPRECATED (v2.22) method: copy_to_image - Use @drawable as the source and draw to a Cairo image
   // Copies a portion of @drawable into the client side image structure
   // @image. If @image is %NULL, creates a new image of size @width x @height
   // and copies into that. See gdk_drawable_get_image() for further details.
   // 
   // of @drawable
   // 
   // 
   // surface if you want to download contents to the client.
   // RETURNS: @image, or a new a #GdkImage containing the contents
   // <image>: a #GdkDrawable, or %NULL if a new @image should be created.
   // <src_x>: x coordinate on @drawable
   // <src_y>: y coordinate on @drawable
   // <dest_x>: x coordinate within @image. Must be 0 if @image is %NULL
   // <dest_y>: y coordinate within @image. Must be 0 if @image is %NULL
   // <width>: width of region to get
   // <height>: height or region to get
   Image* copy_to_image(AT0)(AT0 /*Image*/ image, int src_x, int src_y, int dest_x, int dest_y, int width, int height) nothrow {
      return gdk_drawable_copy_to_image(&this, UpCast!(Image*)(image), src_x, src_y, dest_x, dest_y, width, height);
   }

   // Unintrospectable method: get_clip_region() / gdk_drawable_get_clip_region()
   // Computes the region of a drawable that potentially can be written
   // to by drawing primitives. This region will not take into account
   // the clip region for the GC, and may also not take into account
   // other factors such as if the window is obscured by other windows,
   // but no area outside of this region will be affected by drawing
   // primitives.
   // 
   // when you are done.
   // RETURNS: a #GdkRegion. This must be freed with gdk_region_destroy()
   Region* get_clip_region()() nothrow {
      return gdk_drawable_get_clip_region(&this);
   }

   // Unintrospectable method: get_colormap() / gdk_drawable_get_colormap()
   // Gets the colormap for @drawable, if one is set; returns
   // %NULL otherwise.
   // RETURNS: the colormap, or %NULL
   Colormap* get_colormap()() nothrow {
      return gdk_drawable_get_colormap(&this);
   }

   // Unintrospectable method: get_data() / gdk_drawable_get_data()
   // Equivalent to g_object_get_data(); the #GObject variant should be
   // used instead.
   // RETURNS: the data stored at @key
   // <key>: name the data was stored under
   void* get_data(AT0)(AT0 /*char*/ key) nothrow {
      return gdk_drawable_get_data(&this, toCString!(char*)(key));
   }

   // Obtains the bit depth of the drawable, that is, the number of bits
   // that make up a pixel in the drawable's visual. Examples are 8 bits
   // per pixel, 24 bits per pixel, etc.
   // RETURNS: number of bits per pixel
   int get_depth()() nothrow {
      return gdk_drawable_get_depth(&this);
   }

   // Unintrospectable method: get_display() / gdk_drawable_get_display()
   // VERSION: 2.2
   // DEPRECATED (v2.24) method: get_display - Use gdk_window_get_display() instead
   // Gets the #GdkDisplay associated with a #GdkDrawable.
   // RETURNS: the #GdkDisplay associated with @drawable
   Display* get_display()() nothrow {
      return gdk_drawable_get_display(&this);
   }

   // Unintrospectable method: get_image() / gdk_drawable_get_image()
   // DEPRECATED (v2.22) method: get_image - Use @drawable as the source and draw to a Cairo image
   // A #GdkImage stores client-side image data (pixels). In contrast,
   // #GdkPixmap and #GdkWindow are server-side
   // objects. gdk_drawable_get_image() obtains the pixels from a
   // server-side drawable as a client-side #GdkImage.  The format of a
   // #GdkImage depends on the #GdkVisual of the current display, which
   // makes manipulating #GdkImage extremely difficult; therefore, in
   // most cases you should use gdk_pixbuf_get_from_drawable() instead of
   // this lower-level function. A #GdkPixbuf contains image data in a
   // canonicalized RGB format, rather than a display-dependent format.
   // Of course, there's a convenience vs. speed tradeoff here, so you'll
   // want to think about what makes sense for your application.
   // 
   // @x, @y, @width, and @height define the region of @drawable to
   // obtain as an image.
   // 
   // You would usually copy image data to the client side if you intend
   // to examine the values of individual pixels, for example to darken
   // an image or add a red tint. It would be prohibitively slow to
   // make a round-trip request to the windowing system for each pixel,
   // so instead you get all of them at once, modify them, then copy
   // them all back at once.
   // 
   // If the X server or other windowing system backend is on the local
   // machine, this function may use shared memory to avoid copying
   // the image data.
   // 
   // If the source drawable is a #GdkWindow and partially offscreen
   // or obscured, then the obscured portions of the returned image
   // will contain undefined data.
   // 
   // 
   // surface if you want to download contents to the client.
   // RETURNS: a #GdkImage containing the contents of @drawable
   // <x>: x coordinate on @drawable
   // <y>: y coordinate on @drawable
   // <width>: width of region to get
   // <height>: height or region to get
   Image* get_image()(int x, int y, int width, int height) nothrow {
      return gdk_drawable_get_image(&this, x, y, width, height);
   }

   // Unintrospectable method: get_screen() / gdk_drawable_get_screen()
   // VERSION: 2.2
   // DEPRECATED (v2.24) method: get_screen - Use gdk_window_get_screen() instead
   // Gets the #GdkScreen associated with a #GdkDrawable.
   // RETURNS: the #GdkScreen associated with @drawable
   Screen* get_screen()() nothrow {
      return gdk_drawable_get_screen(&this);
   }

   // DEPRECATED (v2.24) method: get_size - Use gdk_window_get_width() and gdk_window_get_height() for
   // Fills *@width and *@height with the size of @drawable.
   // @width or @height can be %NULL if you only want the other one.
   // 
   // On the X11 platform, if @drawable is a #GdkWindow, the returned
   // size is the size reported in the most-recently-processed configure
   // event, rather than the current size on the X server.
   // 
   // #GdkWindows. Use gdk_pixmap_get_size() for #GdkPixmaps.
   // <width>: location to store drawable's width, or %NULL
   // <height>: location to store drawable's height, or %NULL
   void get_size()(/*out*/ int* width=null, /*out*/ int* height=null) nothrow {
      gdk_drawable_get_size(&this, width, height);
   }

   // Unintrospectable method: get_visible_region() / gdk_drawable_get_visible_region()
   // Computes the region of a drawable that is potentially visible.
   // This does not necessarily take into account if the window is
   // obscured by other windows, but no area outside of this region
   // is visible.
   // 
   // when you are done.
   // RETURNS: a #GdkRegion. This must be freed with gdk_region_destroy()
   Region* get_visible_region()() nothrow {
      return gdk_drawable_get_visible_region(&this);
   }

   // Unintrospectable method: get_visual() / gdk_drawable_get_visual()
   // DEPRECATED (v2.24) method: get_visual - Use gdk_window_get_visual()
   // Gets the #GdkVisual describing the pixel format of @drawable.
   // RETURNS: a #GdkVisual
   Visual* get_visual()() nothrow {
      return gdk_drawable_get_visual(&this);
   }

   // Unintrospectable method: ref() / gdk_drawable_ref()
   // DEPRECATED (v2.0) method: ref - Use g_object_ref() instead.
   // Deprecated equivalent of calling g_object_ref() on @drawable.
   // (Drawables were not objects in previous versions of GDK.)
   // RETURNS: the same @drawable passed in
   Drawable* ref_()() nothrow {
      return gdk_drawable_ref(&this);
   }

   // Sets the colormap associated with @drawable. Normally this will
   // happen automatically when the drawable is created; you only need to
   // use this function if the drawable-creating function did not have a
   // way to determine the colormap, and you then use drawable operations
   // that require a colormap. The colormap for all drawables and
   // graphics contexts you intend to use together should match. i.e.
   // when using a #GdkGC to draw to a drawable, or copying one drawable
   // to another, the colormaps should match.
   // <colormap>: a #GdkColormap
   void set_colormap(AT0)(AT0 /*Colormap*/ colormap) nothrow {
      gdk_drawable_set_colormap(&this, UpCast!(Colormap*)(colormap));
   }

   // This function is equivalent to g_object_set_data(),
   // the #GObject variant should be used instead.
   // <key>: name to store the data under
   // <data>: arbitrary data
   // <destroy_func>: function to free @data, or %NULL
   void set_data(AT0, AT1)(AT0 /*char*/ key, AT1 /*void*/ data, GLib2.DestroyNotify destroy_func=null) nothrow {
      gdk_drawable_set_data(&this, toCString!(char*)(key), UpCast!(void*)(data), destroy_func);
   }

   // DEPRECATED (v2.0) method: unref - Use g_object_unref() instead.
   // Deprecated equivalent of calling g_object_unref() on @drawable.
   void unref()() nothrow {
      gdk_drawable_unref(&this);
   }
}

struct DrawableClass {
   GObject2.ObjectClass parent_class;
   // Unintrospectable functionp: create_gc() / ()
   extern (C) GC* function (Drawable* drawable, GCValues* values, GCValuesMask mask) nothrow create_gc;
   extern (C) void function (Drawable* drawable, GC* gc, int filled, int x, int y, int width, int height) nothrow draw_rectangle;
   extern (C) void function (Drawable* drawable, GC* gc, int filled, int x, int y, int width, int height, int angle1, int angle2) nothrow draw_arc;
   extern (C) void function (Drawable* drawable, GC* gc, int filled, Point* points, int npoints) nothrow draw_polygon;
   extern (C) void function (Drawable* drawable, Font* font, GC* gc, int x, int y, char* text, int text_length) nothrow draw_text;
   extern (C) void function (Drawable* drawable, Font* font, GC* gc, int x, int y, WChar* text, int text_length) nothrow draw_text_wc;
   extern (C) void function (Drawable* drawable, GC* gc, Drawable* src, int xsrc, int ysrc, int xdest, int ydest, int width, int height) nothrow draw_drawable;
   extern (C) void function (Drawable* drawable, GC* gc, Point* points, int npoints) nothrow draw_points;
   extern (C) void function (Drawable* drawable, GC* gc, Segment* segs, int nsegs) nothrow draw_segments;
   extern (C) void function (Drawable* drawable, GC* gc, Point* points, int npoints) nothrow draw_lines;
   extern (C) void function (Drawable* drawable, GC* gc, Pango.Font* font, int x, int y, Pango.GlyphString* glyphs) nothrow draw_glyphs;
   extern (C) void function (Drawable* drawable, GC* gc, Image* image, int xsrc, int ysrc, int xdest, int ydest, int width, int height) nothrow draw_image;
   // RETURNS: number of bits per pixel
   extern (C) int function (Drawable* drawable) nothrow get_depth;

   // <width>: location to store drawable's width, or %NULL
   // <height>: location to store drawable's height, or %NULL
   extern (C) void function (Drawable* drawable, /*out*/ int* width=null, /*out*/ int* height=null) nothrow get_size;
   extern (C) void function (Drawable* drawable, Colormap* cmap) nothrow set_colormap;

   // Unintrospectable functionp: get_colormap() / ()
   // 
   // RETURNS: the colormap, or %NULL
   extern (C) Colormap* function (Drawable* drawable) nothrow get_colormap;

   // Unintrospectable functionp: get_visual() / ()
   // 
   // RETURNS: a #GdkVisual
   extern (C) Visual* function (Drawable* drawable) nothrow get_visual;

   // Unintrospectable functionp: get_screen() / ()
   // 
   // RETURNS: the #GdkScreen associated with @drawable
   extern (C) Screen* function (Drawable* drawable) nothrow get_screen;

   // Unintrospectable functionp: get_image() / ()
   // 
   // RETURNS: a #GdkImage containing the contents of @drawable
   // <x>: x coordinate on @drawable
   // <y>: y coordinate on @drawable
   // <width>: width of region to get
   // <height>: height or region to get
   extern (C) Image* function (Drawable* drawable, int x, int y, int width, int height) nothrow get_image;

   // Unintrospectable functionp: get_clip_region() / ()
   // 
   // RETURNS: a #GdkRegion. This must be freed with gdk_region_destroy()
   extern (C) Region* function (Drawable* drawable) nothrow get_clip_region;

   // Unintrospectable functionp: get_visible_region() / ()
   // 
   // RETURNS: a #GdkRegion. This must be freed with gdk_region_destroy()
   extern (C) Region* function (Drawable* drawable) nothrow get_visible_region;
   // Unintrospectable functionp: get_composite_drawable() / ()
   extern (C) Drawable* function (Drawable* drawable, int x, int y, int width, int height, int* composite_x_offset, int* composite_y_offset) nothrow get_composite_drawable;
   extern (C) void function (Drawable* drawable, GC* gc, GdkPixbuf2.Pixbuf* pixbuf, int src_x, int src_y, int dest_x, int dest_y, int width, int height, RgbDither dither, int x_dither, int y_dither) nothrow draw_pixbuf;
   // Unintrospectable functionp: _copy_to_image() / ()
   extern (C) Image* function (Drawable* drawable, Image* image, int src_x, int src_y, int dest_x, int dest_y, int width, int height) nothrow _copy_to_image;
   extern (C) void function (Drawable* drawable, GC* gc, Pango.Matrix* matrix, Pango.Font* font, int x, int y, Pango.GlyphString* glyphs) nothrow draw_glyphs_transformed;
   extern (C) void function (Drawable* drawable, GC* gc, Trapezoid* trapezoids, int n_trapezoids) nothrow draw_trapezoids;
   extern (C) cairo.Surface* /*new*/ function (Drawable* drawable) nothrow ref_cairo_surface;
   // Unintrospectable functionp: get_source_drawable() / ()
   extern (C) Drawable* function (Drawable* drawable) nothrow get_source_drawable;
   extern (C) void function (Drawable* drawable, cairo.Context* cr) nothrow set_cairo_clip;
   extern (C) cairo.Surface* /*new*/ function (Drawable* drawable, int width, int height) nothrow create_cairo_surface;
   extern (C) void function (Drawable* drawable, GC* gc, Drawable* src, int xsrc, int ysrc, int xdest, int ydest, int width, int height, Drawable* original_src) nothrow draw_drawable_with_src;
   extern (C) void function () nothrow _gdk_reserved7;
   extern (C) void function () nothrow _gdk_reserved9;
   extern (C) void function () nothrow _gdk_reserved10;
   extern (C) void function () nothrow _gdk_reserved11;
   extern (C) void function () nothrow _gdk_reserved12;
   extern (C) void function () nothrow _gdk_reserved13;
   extern (C) void function () nothrow _gdk_reserved14;
   extern (C) void function () nothrow _gdk_reserved15;
}

union Event {
   EventType type;
   EventAny any;
   EventExpose expose;
   EventNoExpose no_expose;
   EventVisibility visibility;
   EventMotion motion;
   EventButton button;
   EventScroll scroll;
   EventKey key;
   EventCrossing crossing;
   EventFocus focus_change;
   EventConfigure configure;
   EventProperty property;
   EventSelection selection;
   EventOwnerChange owner_change;
   EventProximity proximity;
   EventClient client;
   EventDND dnd;
   EventWindowState window_state;
   EventSetting setting;
   EventGrabBroken grab_broken;


   // VERSION: 2.2
   // Creates a new event of the given type. All fields are set to 0.
   // 
   // should be freed with gdk_event_free().
   // RETURNS: a newly-allocated #GdkEvent. The returned #GdkEvent
   // <type>: a #GdkEventType
   static Event* /*new*/ new_()(EventType type) nothrow {
      return gdk_event_new(type);
   }
   static auto opCall()(EventType type) {
      return gdk_event_new(type);
   }

   // Copies a #GdkEvent, copying or incrementing the reference count of the
   // resources associated with it (e.g. #GdkWindow's and strings).
   // 
   // gdk_event_free().
   // RETURNS: a copy of @event. The returned #GdkEvent should be freed with
   Event* /*new*/ copy()() nothrow {
      return gdk_event_copy(&this);
   }

   // Frees a #GdkEvent, freeing or decrementing any resources associated with it.
   // Note that this function should only be called with events returned from
   // functions such as gdk_event_peek(), gdk_event_get(),
   // gdk_event_get_graphics_expose() and gdk_event_copy() and gdk_event_new().
   void free()() nothrow {
      gdk_event_free(&this);
   }

   // Extract the axis value for a particular axis use from
   // an event structure.
   // RETURNS: %TRUE if the specified axis was found, otherwise %FALSE
   // <axis_use>: the axis use to look for
   // <value>: location to store the value found
   int get_axis(AT0)(AxisUse axis_use, /*out*/ AT0 /*double*/ value) nothrow {
      return gdk_event_get_axis(&this, axis_use, UpCast!(double*)(value));
   }

   // Extract the event window relative x/y coordinates from an event.
   // RETURNS: %TRUE if the event delivered event window coordinates
   // <x_win>: location to put event window x coordinate
   // <y_win>: location to put event window y coordinate
   int get_coords(AT0, AT1)(/*out*/ AT0 /*double*/ x_win, /*out*/ AT1 /*double*/ y_win) nothrow {
      return gdk_event_get_coords(&this, UpCast!(double*)(x_win), UpCast!(double*)(y_win));
   }

   // Extract the root window relative x/y coordinates from an event.
   // RETURNS: %TRUE if the event delivered root window coordinates
   // <x_root>: location to put root window x coordinate
   // <y_root>: location to put root window y coordinate
   int get_root_coords(AT0, AT1)(/*out*/ AT0 /*double*/ x_root, /*out*/ AT1 /*double*/ y_root) nothrow {
      return gdk_event_get_root_coords(&this, UpCast!(double*)(x_root), UpCast!(double*)(y_root));
   }

   // Unintrospectable method: get_screen() / gdk_event_get_screen()
   // VERSION: 2.2
   // Returns the screen for the event. The screen is
   // typically the screen for <literal>event->any.window</literal>, but
   // for events such as mouse events, it is the screen
   // where the pointer was when the event occurs -
   // that is, the screen which has the root window 
   // to which <literal>event->motion.x_root</literal> and
   // <literal>event->motion.y_root</literal> are relative.
   // RETURNS: the screen for the event
   Screen* get_screen()() nothrow {
      return gdk_event_get_screen(&this);
   }

   // If the event contains a "state" field, puts that field in @state. Otherwise
   // stores an empty state (0). Returns %TRUE if there was a state field
   // in the event. @event may be %NULL, in which case it's treated
   // as if the event had no state field.
   // RETURNS: %TRUE if there was a state field in the event
   // <state>: return location for state
   int get_state(AT0)(/*out*/ AT0 /*ModifierType*/ state) nothrow {
      return gdk_event_get_state(&this, UpCast!(ModifierType*)(state));
   }

   // Returns the time stamp from @event, if there is one; otherwise
   // returns #GDK_CURRENT_TIME. If @event is %NULL, returns #GDK_CURRENT_TIME.
   // RETURNS: time stamp field from @event
   uint get_time()() nothrow {
      return gdk_event_get_time(&this);
   }

   // Appends a copy of the given event onto the front of the event
   // queue for event->any.window's display, or the default event
   // queue if event->any.window is %NULL. See gdk_display_put_event().
   void put()() nothrow {
      gdk_event_put(&this);
   }

   // Sends an X ClientMessage event to a given window (which must be
   // on the default #GdkDisplay.)
   // This could be used for communicating between different applications,
   // though the amount of data is limited to 20 bytes.
   // RETURNS: non-zero on success.
   // <winid>: the window to send the X ClientMessage event to.
   int send_client_message()(NativeWindow winid) nothrow {
      return gdk_event_send_client_message(&this, winid);
   }

   // Sends an X ClientMessage event to all toplevel windows on the default
   // #GdkScreen.
   // 
   // Toplevel windows are determined by checking for the WM_STATE property, as
   // described in the Inter-Client Communication Conventions Manual (ICCCM).
   // If no windows are found with the WM_STATE property set, the message is sent
   // to all children of the root window.
   void send_clientmessage_toall()() nothrow {
      gdk_event_send_clientmessage_toall(&this);
   }

   // VERSION: 2.2
   // Sets the screen for @event to @screen. The event must
   // have been allocated by GTK+, for instance, by
   // gdk_event_copy().
   // <screen>: a #GdkScreen
   void set_screen(AT0)(AT0 /*Screen*/ screen) nothrow {
      gdk_event_set_screen(&this, UpCast!(Screen*)(screen));
   }

   // Checks all open displays for a #GdkEvent to process,to be processed
   // on, fetching events from the windowing system if necessary.
   // See gdk_display_get_event().
   // 
   // are pending. The returned #GdkEvent should be freed with gdk_event_free().
   // RETURNS: the next #GdkEvent to be processed, or %NULL if no events
   static Event* /*new*/ get()() nothrow {
      return gdk_event_get();
   }
   static Event* /*new*/ get_graphics_expose(AT0)(AT0 /*Window*/ window) nothrow {
      return gdk_event_get_graphics_expose(UpCast!(Window*)(window));
   }

   // Sets the function to call to handle all events from GDK.
   // 
   // Note that GTK+ uses this to install its own event handler, so it is
   // usually not useful for GTK+ applications. (Although an application
   // can call this function then call gtk_main_do_event() to pass
   // events to GTK+.)
   // <func>: the function to call to handle events from GDK.
   // <data>: user data to pass to the function.
   // <notify>: the function to call when the handler function is removed, i.e. when gdk_event_handler_set() is called with another event handler.
   static void handler_set(AT0)(EventFunc func, AT0 /*void*/ data, GLib2.DestroyNotify notify) nothrow {
      gdk_event_handler_set(func, UpCast!(void*)(data), notify);
   }

   // If there is an event waiting in the event queue of some open
   // display, returns a copy of it. See gdk_display_peek_event().
   // 
   // events are in any queues. The returned #GdkEvent should be freed with
   // gdk_event_free().
   // RETURNS: a copy of the first #GdkEvent on some event queue, or %NULL if no
   static Event* /*new*/ peek()() nothrow {
      return gdk_event_peek();
   }

   // VERSION: 2.12
   // Request more motion notifies if @event is a motion notify hint event.
   // This function should be used instead of gdk_window_get_pointer() to
   // request further motion notifies, because it also works for extension
   // events where motion notifies are provided for devices other than the
   // core pointer. Coordinate extraction, processing and requesting more
   // motion events from a %GDK_MOTION_NOTIFY event usually works like this:
   // 
   // |[
   // { 
   // /&ast; motion_event handler &ast;/
   // x = motion_event->x;
   // y = motion_event->y;
   // /&ast; handle (x,y) motion &ast;/
   // gdk_event_request_motions (motion_event); /&ast; handles is_hint events &ast;/
   // }
   // ]|
   // <event>: a valid #GdkEvent
   static void request_motions(AT0)(AT0 /*EventMotion*/ event) nothrow {
      gdk_event_request_motions(UpCast!(EventMotion*)(event));
   }
   static int send_client_message_for_display(AT0, AT1)(AT0 /*Display*/ display, AT1 /*Event*/ event, NativeWindow winid) nothrow {
      return gdk_event_send_client_message_for_display(UpCast!(Display*)(display), UpCast!(Event*)(event), winid);
   }

   //  --- mixin/Gdk2_Event.d --->

   void toString(FT)(scope void delegate(const(char)[]) sink, FT fmt) {
      import std.format;
      with (EventType)
      switch (type) {
      case EXPOSE:            formatValue(sink, expose, fmt); return;
      case MOTION_NOTIFY:     formatValue(sink, motion, fmt); return;
      case BUTTON_PRESS, _2BUTTON_PRESS, _3BUTTON_PRESS, BUTTON_RELEASE:
                              formatValue(sink, button, fmt); return; // XXX check, GIR has this as just Event.
      case KEY_PRESS, KEY_RELEASE: formatValue(sink, key, fmt); return;
      case ENTER_NOTIFY, LEAVE_NOTIFY: formatValue(sink, crossing, fmt); return;
      case FOCUS_CHANGE:      formatValue(sink, focus_change, fmt); return;
      case CONFIGURE:         formatValue(sink, configure, fmt); return;
      case PROPERTY_NOTIFY:   formatValue(sink, property, fmt); return;
      case SELECTION_CLEAR, SELECTION_REQUEST, SELECTION_NOTIFY:
                              formatValue(sink, selection, fmt); return;
      case PROXIMITY_IN, PROXIMITY_OUT:
                              formatValue(sink, proximity, fmt); return;
      case VISIBILITY_NOTIFY: formatValue(sink, visibility, fmt); return;
      case SCROLL:            formatValue(sink, scroll, fmt); return;
      
      case WINDOW_STATE: formatValue(sink, window_state, fmt); return;
      any: case DELETE, DESTROY, MAP, UNMAP:
         formatValue(sink, any, fmt); return;
      default: sink("/*FIXME*/"); goto any;
      }
   }
   
   // <--- mixin/Gdk2_Event.d ---
}

struct EventAny {
   EventType type;
   Window* window;
   byte send_event;
}

struct EventButton {
   EventType type;
   Window* window;
   byte send_event;
   uint time;
   double x, y;
   double* axes;
   uint state, button;
   Device* device;
   double x_root, y_root;
}

struct EventClient {
   EventType type;
   Window* window;
   byte send_event;
   Atom message_type;
   ushort data_format;

   union data {
      char[20] b;
      short[10] s;
      c_long[5] l;
   }
}

struct EventConfigure {
   EventType type;
   Window* window;
   byte send_event;
   int x, y, width, height;
}

struct EventCrossing {
   EventType type;
   Window* window;
   byte send_event;
   Window* subwindow;
   uint time;
   double x, y, x_root, y_root;
   CrossingMode mode;
   NotifyType detail;
   int focus;
   uint state;
}

struct EventDND {
   EventType type;
   Window* window;
   byte send_event;
   DragContext* context;
   uint time;
   short x_root, y_root;
}

struct EventExpose {
   EventType type;
   Window* window;
   byte send_event;
   Rectangle area;
   Region* region;
   int count;
}

struct EventFocus {
   EventType type;
   Window* window;
   byte send_event;
   short in_;
}

extern (C) alias void function (Event* event, void* data) nothrow EventFunc;

struct EventGrabBroken {
   EventType type;
   Window* window;
   byte send_event;
   int keyboard, implicit;
   Window* grab_window;
}

struct EventKey {
   EventType type;
   Window* window;
   byte send_event;
   uint time;
   uint state, keyval;
   int length;
   char* string_;
   ushort hardware_keycode;
   ubyte group;
    static import std.bitmanip; mixin(std.bitmanip.bitfields!(
   uint, "is_modifier", 1,
    uint, "__dummy32A", 31));
}

enum EventMask {
   EXPOSURE_MASK = 2,
   POINTER_MOTION_MASK = 4,
   POINTER_MOTION_HINT_MASK = 8,
   BUTTON_MOTION_MASK = 16,
   BUTTON1_MOTION_MASK = 32,
   BUTTON2_MOTION_MASK = 64,
   BUTTON3_MOTION_MASK = 128,
   BUTTON_PRESS_MASK = 256,
   BUTTON_RELEASE_MASK = 512,
   KEY_PRESS_MASK = 1024,
   KEY_RELEASE_MASK = 2048,
   ENTER_NOTIFY_MASK = 4096,
   LEAVE_NOTIFY_MASK = 8192,
   FOCUS_CHANGE_MASK = 16384,
   STRUCTURE_MASK = 32768,
   PROPERTY_CHANGE_MASK = 65536,
   VISIBILITY_NOTIFY_MASK = 131072,
   PROXIMITY_IN_MASK = 262144,
   PROXIMITY_OUT_MASK = 524288,
   SUBSTRUCTURE_MASK = 1048576,
   SCROLL_MASK = 2097152,
   ALL_EVENTS_MASK = 4194302
}
struct EventMotion {
   EventType type;
   Window* window;
   byte send_event;
   uint time;
   double x, y;
   double* axes;
   uint state;
   short is_hint;
   Device* device;
   double x_root, y_root;
}

struct EventNoExpose {
   EventType type;
   Window* window;
   byte send_event;
}

struct EventOwnerChange {
   EventType type;
   Window* window;
   byte send_event;
   NativeWindow owner;
   OwnerChange reason;
   Atom selection;
   uint time, selection_time;
}

struct EventProperty {
   EventType type;
   Window* window;
   byte send_event;
   Atom atom;
   uint time;
   uint state;
}

struct EventProximity {
   EventType type;
   Window* window;
   byte send_event;
   uint time;
   Device* device;
}

struct EventScroll {
   EventType type;
   Window* window;
   byte send_event;
   uint time;
   double x, y;
   uint state;
   ScrollDirection direction;
   Device* device;
   double x_root, y_root;
}

struct EventSelection {
   EventType type;
   Window* window;
   byte send_event;
   Atom selection, target, property;
   uint time;
   NativeWindow requestor;
}

struct EventSetting {
   EventType type;
   Window* window;
   byte send_event;
   SettingAction action;
   char* name;
}

enum EventType {
   NOTHING = -1,
   DELETE = 0,
   DESTROY = 1,
   EXPOSE = 2,
   MOTION_NOTIFY = 3,
   BUTTON_PRESS = 4,
   _2BUTTON_PRESS = 5,
   _3BUTTON_PRESS = 6,
   BUTTON_RELEASE = 7,
   KEY_PRESS = 8,
   KEY_RELEASE = 9,
   ENTER_NOTIFY = 10,
   LEAVE_NOTIFY = 11,
   FOCUS_CHANGE = 12,
   CONFIGURE = 13,
   MAP = 14,
   UNMAP = 15,
   PROPERTY_NOTIFY = 16,
   SELECTION_CLEAR = 17,
   SELECTION_REQUEST = 18,
   SELECTION_NOTIFY = 19,
   PROXIMITY_IN = 20,
   PROXIMITY_OUT = 21,
   DRAG_ENTER = 22,
   DRAG_LEAVE = 23,
   DRAG_MOTION = 24,
   DRAG_STATUS = 25,
   DROP_START = 26,
   DROP_FINISHED = 27,
   CLIENT_EVENT = 28,
   VISIBILITY_NOTIFY = 29,
   NO_EXPOSE = 30,
   SCROLL = 31,
   WINDOW_STATE = 32,
   SETTING = 33,
   OWNER_CHANGE = 34,
   GRAB_BROKEN = 35,
   DAMAGE = 36,
   EVENT_LAST = 37
}
struct EventVisibility {
   EventType type;
   Window* window;
   byte send_event;
   VisibilityState state;
}

struct EventWindowState {
   EventType type;
   Window* window;
   byte send_event;
   WindowState changed_mask, new_window_state;
}

enum ExtensionMode {
   NONE = 0,
   ALL = 1,
   CURSOR = 2
}
enum Fill {
   SOLID = 0,
   TILED = 1,
   STIPPLED = 2,
   OPAQUE_STIPPLED = 3
}
enum FillRule {
   EVEN_ODD_RULE = 0,
   WINDING_RULE = 1
}
extern (C) alias FilterReturn function (XEvent* xevent, Event* event, void* data) nothrow FilterFunc;

enum FilterReturn {
   CONTINUE = 0,
   TRANSLATE = 1,
   REMOVE = 2
}
struct Font {
   FontType type;
   int ascent, descent;

   int equal(AT0)(AT0 /*Font*/ fontb) nothrow {
      return gdk_font_equal(&this, UpCast!(Font*)(fontb));
   }
   // Unintrospectable method: get_display() / gdk_font_get_display()
   Display* get_display()() nothrow {
      return gdk_font_get_display(&this);
   }
   int id()() nothrow {
      return gdk_font_id(&this);
   }

   // Increases the reference count of a font by one.
   // RETURNS: @font
   Font* /*new*/ ref_()() nothrow {
      return gdk_font_ref(&this);
   }

   // Decreases the reference count of a font by one.
   // If the result is zero, destroys the font.
   void unref()() nothrow {
      gdk_font_unref(&this);
   }

   // Load a #GdkFont based on a Pango font description. This font will
   // only be an approximation of the Pango font, and
   // internationalization will not be handled correctly. This function
   // should only be used for legacy code that cannot be easily converted
   // to use Pango. Using Pango directly will produce better results.
   // 
   // cannot be loaded.
   // RETURNS: the newly loaded font, or %NULL if the font
   // <font_desc>: a #PangoFontDescription.
   static Font* /*new*/ from_description(AT0)(AT0 /*Pango.FontDescription*/ font_desc) nothrow {
      return gdk_font_from_description(UpCast!(Pango.FontDescription*)(font_desc));
   }
   static Font* /*new*/ from_description_for_display(AT0, AT1)(AT0 /*Display*/ display, AT1 /*Pango.FontDescription*/ font_desc) nothrow {
      return gdk_font_from_description_for_display(UpCast!(Display*)(display), UpCast!(Pango.FontDescription*)(font_desc));
   }

   // Loads a font.
   // 
   // The font may be newly loaded or looked up the font in a cache. 
   // You should make no assumptions about the initial reference count.
   // RETURNS: a #GdkFont, or %NULL if the font could not be loaded.
   // <font_name>: a XLFD describing the font to load.
   static Font* /*new*/ load(AT0)(AT0 /*char*/ font_name) nothrow {
      return gdk_font_load(toCString!(char*)(font_name));
   }
   static Font* /*new*/ load_for_display(AT0, AT1)(AT0 /*Display*/ display, AT1 /*char*/ font_name) nothrow {
      return gdk_font_load_for_display(UpCast!(Display*)(display), toCString!(char*)(font_name));
   }
}

enum FontType {
   FONT = 0,
   FONTSET = 1
}
enum Function {
   COPY = 0,
   INVERT = 1,
   XOR = 2,
   CLEAR = 3,
   AND = 4,
   AND_REVERSE = 5,
   AND_INVERT = 6,
   NOOP = 7,
   OR = 8,
   EQUIV = 9,
   OR_REVERSE = 10,
   COPY_INVERT = 11,
   OR_INVERT = 12,
   NAND = 13,
   NOR = 14,
   SET = 15
}
struct GC /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   int clip_x_origin, clip_y_origin, ts_x_origin, ts_y_origin;
   Colormap* colormap;


   // DEPRECATED (v2.22) constructor: new - Use Cairo for rendering.
   // Create a new graphics context with default values.
   // RETURNS: the new graphics context.
   // <drawable>: a #GdkDrawable. The created GC must always be used with drawables of the same depth as this one.
   static GC* /*new*/ new_(AT0)(AT0 /*Drawable*/ drawable) nothrow {
      return gdk_gc_new(UpCast!(Drawable*)(drawable));
   }
   static auto opCall(AT0)(AT0 /*Drawable*/ drawable) {
      return gdk_gc_new(UpCast!(Drawable*)(drawable));
   }

   // DEPRECATED (v2.22) constructor: new_with_values - Use Cairo for rendering.
   // Create a new GC with the given initial values.
   // RETURNS: the new graphics context.
   // <drawable>: a #GdkDrawable. The created GC must always be used with drawables of the same depth as this one.
   // <values>: a structure containing initial values for the GC.
   // <values_mask>: a bit mask indicating which fields in @values are set.
   static GC* /*new*/ new_with_values(AT0, AT1)(AT0 /*Drawable*/ drawable, AT1 /*GCValues*/ values, GCValuesMask values_mask) nothrow {
      return gdk_gc_new_with_values(UpCast!(Drawable*)(drawable), UpCast!(GCValues*)(values), values_mask);
   }
   static auto opCall(AT0, AT1)(AT0 /*Drawable*/ drawable, AT1 /*GCValues*/ values, GCValuesMask values_mask) {
      return gdk_gc_new_with_values(UpCast!(Drawable*)(drawable), UpCast!(GCValues*)(values), values_mask);
   }

   // DEPRECATED (v2.22) method: copy - Use Cairo for drawing. cairo_save() and cairo_restore()
   // Copy the set of values from one graphics context
   // onto another graphics context.
   // 
   // can be helpful in cases where you'd have copied a #GdkGC.
   // <src_gc>: the source graphics context.
   void copy(AT0)(AT0 /*GC*/ src_gc) nothrow {
      gdk_gc_copy(&this, UpCast!(GC*)(src_gc));
   }

   // Unintrospectable method: get_colormap() / gdk_gc_get_colormap()
   // DEPRECATED (v2.22) method: get_colormap - There is no replacement. Cairo handles colormaps
   // Retrieves the colormap for a given GC, if it exists.
   // A GC will have a colormap if the drawable for which it was created
   // has a colormap, or if a colormap was set explicitely with
   // gdk_gc_set_colormap.
   // 
   // 
   // automatically, so there is no need to care about them.
   // RETURNS: the colormap of @gc, or %NULL if @gc doesn't have one.
   Colormap* get_colormap()() nothrow {
      return gdk_gc_get_colormap(&this);
   }

   // Unintrospectable method: get_screen() / gdk_gc_get_screen()
   // VERSION: 2.2
   // Gets the #GdkScreen for which @gc was created
   // RETURNS: the #GdkScreen for @gc.
   Screen* get_screen()() nothrow {
      return gdk_gc_get_screen(&this);
   }

   // DEPRECATED (v2.22) method: get_values - Use Cairo for rendering.
   // Retrieves the current values from a graphics context. Note that 
   // only the pixel values of the @values->foreground and @values->background
   // are filled, use gdk_colormap_query_color() to obtain the rgb values
   // if you need them.
   // <values>: the #GdkGCValues structure in which to store the results.
   void get_values(AT0)(AT0 /*GCValues*/ values) nothrow {
      gdk_gc_get_values(&this, UpCast!(GCValues*)(values));
   }

   // DEPRECATED (v2.22) method: offset - There is no direct replacement, as this is just a
   // Offset attributes such as the clip and tile-stipple origins
   // of the GC so that drawing at x - x_offset, y - y_offset with
   // the offset GC  has the same effect as drawing at x, y with the original
   // GC.
   // 
   // convenience function for gdk_gc_set_ts_origin and gdk_gc_set_clip_origin().
   // <x_offset>: amount by which to offset the GC in the X direction
   // <y_offset>: amount by which to offset the GC in the Y direction
   void offset()(int x_offset, int y_offset) nothrow {
      gdk_gc_offset(&this, x_offset, y_offset);
   }

   // Unintrospectable method: ref() / gdk_gc_ref()
   // DEPRECATED (v2.0) method: ref - Use g_object_ref() instead.
   // Deprecated function; use g_object_ref() instead.
   // RETURNS: the gc.
   GC* ref_()() nothrow {
      return gdk_gc_ref(&this);
   }

   // DEPRECATED (v2.22) method: set_background - Use gdk_cairo_set_source_color() to use a #GdkColor
   // Sets the background color for a graphics context.
   // Note that this function uses @color->pixel, use 
   // gdk_gc_set_rgb_bg_color() to specify the background 
   // color as red, green, blue components.
   // 
   // as the source in Cairo. Note that if you want to draw a background and a
   // foreground in Cairo, you need to call drawing functions (like cairo_fill())
   // twice.
   // <color>: the new background color.
   void set_background(AT0)(AT0 /*Color*/ color) nothrow {
      gdk_gc_set_background(&this, UpCast!(Color*)(color));
   }

   // DEPRECATED (v2.22) method: set_clip_mask - Use cairo_mask() instead.
   // Sets the clip mask for a graphics context from a bitmap.
   // The clip mask is interpreted relative to the clip
   // origin. (See gdk_gc_set_clip_origin()).
   // <mask>: a bitmap.
   void set_clip_mask(AT0)(AT0 /*Bitmap*/ mask) nothrow {
      gdk_gc_set_clip_mask(&this, UpCast!(Bitmap*)(mask));
   }

   // DEPRECATED (v2.22) method: set_clip_origin - Use cairo_translate() before applying the clip path in
   // Sets the origin of the clip mask. The coordinates are
   // interpreted relative to the upper-left corner of
   // the destination drawable of the current operation.
   // 
   // Cairo.
   // <x>: the x-coordinate of the origin.
   // <y>: the y-coordinate of the origin.
   void set_clip_origin()(int x, int y) nothrow {
      gdk_gc_set_clip_origin(&this, x, y);
   }

   // DEPRECATED (v2.22) method: set_clip_rectangle - Use cairo_rectangle() and cairo_clip() in Cairo.
   // Sets the clip mask for a graphics context from a
   // rectangle. The clip mask is interpreted relative to the clip
   // origin. (See gdk_gc_set_clip_origin()).
   // <rectangle>: the rectangle to clip to.
   void set_clip_rectangle(AT0)(AT0 /*Rectangle*/ rectangle) nothrow {
      gdk_gc_set_clip_rectangle(&this, UpCast!(Rectangle*)(rectangle));
   }

   // DEPRECATED (v2.22) method: set_clip_region - Use gdk_cairo_region() and cairo_clip() in Cairo.
   // Sets the clip mask for a graphics context from a region structure.
   // The clip mask is interpreted relative to the clip origin. (See
   // gdk_gc_set_clip_origin()).
   // <region>: the #GdkRegion.
   void set_clip_region(AT0)(AT0 /*Region*/ region) nothrow {
      gdk_gc_set_clip_region(&this, UpCast!(Region*)(region));
   }

   // DEPRECATED (v2.22) method: set_colormap - There is no replacement. Cairo handles colormaps
   // Sets the colormap for the GC to the given colormap. The depth
   // of the colormap's visual must match the depth of the drawable
   // for which the GC was created.
   // 
   // automatically, so there is no need to care about them.
   // <colormap>: a #GdkColormap
   void set_colormap(AT0)(AT0 /*Colormap*/ colormap) nothrow {
      gdk_gc_set_colormap(&this, UpCast!(Colormap*)(colormap));
   }

   // DEPRECATED (v2.22) method: set_dashes - Use cairo_set_dash() to set the dash in Cairo.
   // Sets the way dashed-lines are drawn. Lines will be
   // drawn with alternating on and off segments of the
   // lengths specified in @dash_list. The manner in
   // which the on and off segments are drawn is determined
   // by the @line_style value of the GC. (This can
   // be changed with gdk_gc_set_line_attributes().)
   // 
   // The @dash_offset defines the phase of the pattern, 
   // specifying how many pixels into the dash-list the pattern 
   // should actually begin.
   // <dash_offset>: the phase of the dash pattern.
   // <dash_list>: an array of dash lengths.
   // <n>: the number of elements in @dash_list.
   void set_dashes()(int dash_offset, byte dash_list, int n) nothrow {
      gdk_gc_set_dashes(&this, dash_offset, dash_list, n);
   }

   // DEPRECATED (v2.22) method: set_exposures - There is no replacement. If you need to control
   // Sets whether copying non-visible portions of a drawable
   // using this graphics context generate exposure events
   // for the corresponding regions of the destination
   // drawable. (See gdk_draw_drawable()).
   // 
   // exposures, you must use drawing operations of the underlying window
   // system or use gdk_window_invalidate_rect(). Cairo will never
   // generate exposures.
   // <exposures>: if %TRUE, exposure events will be generated.
   void set_exposures()(int exposures) nothrow {
      gdk_gc_set_exposures(&this, exposures);
   }

   // DEPRECATED (v2.22) method: set_fill - You can achieve tiling in Cairo by using
   // Set the fill mode for a graphics context.
   // 
   // cairo_pattern_set_extend() on the source. For stippling, see the
   // deprecation comments on gdk_gc_set_stipple().
   // <fill>: the new fill mode.
   void set_fill()(Fill fill) nothrow {
      gdk_gc_set_fill(&this, fill);
   }

   // Sets the font for a graphics context. (Note that
   // all text-drawing functions in GDK take a @font
   // argument; the value set here is used when that
   // argument is %NULL.)
   // <font>: the new font.
   void set_font(AT0)(AT0 /*Font*/ font) nothrow {
      gdk_gc_set_font(&this, UpCast!(Font*)(font));
   }

   // DEPRECATED (v2.22) method: set_foreground - Use gdk_cairo_set_source_color() to use a #GdkColor
   // Sets the foreground color for a graphics context.
   // Note that this function uses @color->pixel, use 
   // gdk_gc_set_rgb_fg_color() to specify the foreground 
   // color as red, green, blue components.
   // 
   // as the source in Cairo.
   // <color>: the new foreground color.
   void set_foreground(AT0)(AT0 /*Color*/ color) nothrow {
      gdk_gc_set_foreground(&this, UpCast!(Color*)(color));
   }

   // DEPRECATED (v2.22) method: set_function - Use cairo_set_operator() with Cairo.
   // Determines how the current pixel values and the
   // pixel values being drawn are combined to produce
   // the final pixel values.
   // <function>: the #GdkFunction to use
   void set_function()(Function function_) nothrow {
      gdk_gc_set_function(&this, function_);
   }

   // DEPRECATED (v2.22) method: set_line_attributes - Use the Cairo functions cairo_set_line_width(),
   // Sets various attributes of how lines are drawn. See
   // the corresponding members of #GdkGCValues for full
   // explanations of the arguments.
   // 
   // cairo_set_line_join(), cairo_set_line_cap() and cairo_set_dash()
   // to affect the stroking behavior in Cairo. Keep in mind that the default
   // attributes of a #cairo_t are different from the default attributes of
   // a #GdkGC.
   // <line_width>: the width of lines.
   // <line_style>: the dash-style for lines.
   // <cap_style>: the manner in which the ends of lines are drawn.
   // <join_style>: the in which lines are joined together.
   void set_line_attributes()(int line_width, LineStyle line_style, CapStyle cap_style, JoinStyle join_style) nothrow {
      gdk_gc_set_line_attributes(&this, line_width, line_style, cap_style, join_style);
   }

   // DEPRECATED (v2.22) method: set_rgb_bg_color - Use gdk_cairo_set_source_color() instead.
   // Set the background color of a GC using an unallocated color. The
   // pixel value for the color will be determined using GdkRGB. If the
   // colormap for the GC has not previously been initialized for GdkRGB,
   // then for pseudo-color colormaps (colormaps with a small modifiable
   // number of colors), a colorcube will be allocated in the colormap.
   // 
   // Calling this function for a GC without a colormap is an error.
   // <color>: an unallocated #GdkColor.
   void set_rgb_bg_color(AT0)(AT0 /*Color*/ color) nothrow {
      gdk_gc_set_rgb_bg_color(&this, UpCast!(Color*)(color));
   }

   // DEPRECATED (v2.22) method: set_rgb_fg_color - Use gdk_cairo_set_source_color() instead.
   // Set the foreground color of a GC using an unallocated color. The
   // pixel value for the color will be determined using GdkRGB. If the
   // colormap for the GC has not previously been initialized for GdkRGB,
   // then for pseudo-color colormaps (colormaps with a small modifiable
   // number of colors), a colorcube will be allocated in the colormap.
   // 
   // Calling this function for a GC without a colormap is an error.
   // <color>: an unallocated #GdkColor.
   void set_rgb_fg_color(AT0)(AT0 /*Color*/ color) nothrow {
      gdk_gc_set_rgb_fg_color(&this, UpCast!(Color*)(color));
   }

   // DEPRECATED (v2.22) method: set_stipple - Stippling has no direct replacement in Cairo. If you
   // Set the stipple bitmap for a graphics context. The
   // stipple will only be used if the fill mode is
   // %GDK_STIPPLED or %GDK_OPAQUE_STIPPLED.
   // 
   // want to achieve an identical look, you can use the stipple bitmap as a
   // mask. Most likely, this involves rendering the source to an intermediate
   // surface using cairo_push_group() first, so that you can then use
   // cairo_mask() to achieve the stippled look.
   // <stipple>: the new stipple bitmap.
   void set_stipple(AT0)(AT0 /*Pixmap*/ stipple) nothrow {
      gdk_gc_set_stipple(&this, UpCast!(Pixmap*)(stipple));
   }

   // DEPRECATED (v2.22) method: set_subwindow - There is no replacement. If you need to control
   // Sets how drawing with this GC on a window will affect child
   // windows of that window. 
   // 
   // subwindows, you must use drawing operations of the underlying window
   // system manually. Cairo will always use %GDK_INCLUDE_INFERIORS on sources
   // and masks and %GDK_CLIP_BY_CHILDREN on targets.
   // <mode>: the subwindow mode.
   void set_subwindow()(SubwindowMode mode) nothrow {
      gdk_gc_set_subwindow(&this, mode);
   }

   // DEPRECATED (v2.22) method: set_tile - The following code snippet sets a tiling #GdkPixmap
   // Set a tile pixmap for a graphics context.
   // This will only be used if the fill mode
   // is %GDK_TILED.
   // 
   // as the source in Cairo:
   // |[gdk_cairo_set_source_pixmap (cr, tile, ts_origin_x, ts_origin_y);
   // cairo_pattern_set_extend (cairo_get_source (cr), CAIRO_EXTEND_REPEAT);]|
   // <tile>: the new tile pixmap.
   void set_tile(AT0)(AT0 /*Pixmap*/ tile) nothrow {
      gdk_gc_set_tile(&this, UpCast!(Pixmap*)(tile));
   }

   // DEPRECATED (v2.22) method: set_ts_origin - You can set the origin for tiles and stipples in Cairo
   // Set the origin when using tiles or stipples with
   // the GC. The tile or stipple will be aligned such
   // that the upper left corner of the tile or stipple
   // will coincide with this point.
   // 
   // by changing the source's matrix using cairo_pattern_set_matrix(). Or you
   // can specify it with gdk_cairo_set_source_pixmap() as shown in the example
   // for gdk_gc_set_tile().
   // <x>: the x-coordinate of the origin.
   // <y>: the y-coordinate of the origin.
   void set_ts_origin()(int x, int y) nothrow {
      gdk_gc_set_ts_origin(&this, x, y);
   }

   // DEPRECATED (v2.22) method: set_values - Use Cairo for rendering.
   // Sets attributes of a graphics context in bulk. For each flag set in
   // @values_mask, the corresponding field will be read from @values and
   // set as the new value for @gc. If you're only setting a few values
   // on @gc, calling individual "setter" functions is likely more
   // convenient.
   // <values>: struct containing the new values
   // <values_mask>: mask indicating which struct fields are to be used
   void set_values(AT0)(AT0 /*GCValues*/ values, GCValuesMask values_mask) nothrow {
      gdk_gc_set_values(&this, UpCast!(GCValues*)(values), values_mask);
   }

   // DEPRECATED (v2.0) method: unref - Use g_object_unref() instead.
   // Decrement the reference count of @gc.
   void unref()() nothrow {
      gdk_gc_unref(&this);
   }
}

struct GCClass {
   GObject2.ObjectClass parent_class;
   // <values>: the #GdkGCValues structure in which to store the results.
   extern (C) void function (GC* gc, GCValues* values) nothrow get_values;
   // <values>: struct containing the new values
   extern (C) void function (GC* gc, GCValues* values, GCValuesMask mask) nothrow set_values;

   // <dash_offset>: the phase of the dash pattern.
   // <dash_list>: an array of dash lengths.
   // <n>: the number of elements in @dash_list.
   extern (C) void function (GC* gc, int dash_offset, byte dash_list, int n) nothrow set_dashes;
   extern (C) void function () nothrow _gdk_reserved1;
   extern (C) void function () nothrow _gdk_reserved2;
   extern (C) void function () nothrow _gdk_reserved3;
   extern (C) void function () nothrow _gdk_reserved4;
}

struct GCValues {
   Color foreground, background;
   Font* font;
   Function function_;
   Fill fill;
   Pixmap* tile, stipple, clip_mask;
   SubwindowMode subwindow_mode;
   int ts_x_origin, ts_y_origin, clip_x_origin, clip_y_origin, graphics_exposures, line_width;
   LineStyle line_style;
   CapStyle cap_style;
   JoinStyle join_style;
}

enum GCValuesMask {
   FOREGROUND = 1,
   BACKGROUND = 2,
   FONT = 4,
   FUNCTION = 8,
   FILL = 16,
   TILE = 32,
   STIPPLE = 64,
   CLIP_MASK = 128,
   SUBWINDOW = 256,
   TS_X_ORIGIN = 512,
   TS_Y_ORIGIN = 1024,
   CLIP_X_ORIGIN = 2048,
   CLIP_Y_ORIGIN = 4096,
   EXPOSURES = 8192,
   LINE_WIDTH = 16384,
   LINE_STYLE = 32768,
   CAP_STYLE = 65536,
   JOIN_STYLE = 131072
}
struct Geometry {
   int min_width, min_height, max_width, max_height, base_width, base_height, width_inc, height_inc;
   double min_aspect, max_aspect;
   Gravity win_gravity;
}

enum GrabStatus {
   SUCCESS = 0,
   ALREADY_GRABBED = 1,
   INVALID_TIME = 2,
   NOT_VIEWABLE = 3,
   FROZEN = 4
}
enum Gravity {
   NORTH_WEST = 1,
   NORTH = 2,
   NORTH_EAST = 3,
   WEST = 4,
   CENTER = 5,
   EAST = 6,
   SOUTH_WEST = 7,
   SOUTH = 8,
   SOUTH_EAST = 9,
   STATIC = 10
}
struct Image /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   ImageType type;
   Visual* visual;
   ByteOrder byte_order;
   int width, height;
   ushort depth, bpp, bpl, bits_per_pixel;
   void* mem;
   Colormap* colormap;
   private void* windowing_data;

   static Image* /*new*/ new_(AT0)(ImageType type, AT0 /*Visual*/ visual, int width, int height) nothrow {
      return gdk_image_new(type, UpCast!(Visual*)(visual), width, height);
   }
   static auto opCall(AT0)(ImageType type, AT0 /*Visual*/ visual, int width, int height) {
      return gdk_image_new(type, UpCast!(Visual*)(visual), width, height);
   }

   // Unintrospectable function: get() / gdk_image_get()
   // This is a deprecated wrapper for gdk_drawable_get_image();
   // gdk_drawable_get_image() should be used instead. Or even better: in
   // most cases gdk_pixbuf_get_from_drawable() is the most convenient
   // choice.
   // RETURNS: a new #GdkImage or %NULL
   // <drawable>: a #GdkDrawable
   // <x>: x coordinate in @window
   // <y>: y coordinate in @window
   // <width>: width of area in @window
   // <height>: height of area in @window
   static Image* get(AT0)(AT0 /*Drawable*/ drawable, int x, int y, int width, int height) nothrow {
      return gdk_image_get(UpCast!(Drawable*)(drawable), x, y, width, height);
   }

   // VERSION: 2.22
   // DEPRECATED (v2.22) method: get_bits_per_pixel - #GdkImage should not be used anymore.
   // Determines the number of bits per pixel of the image.
   // RETURNS: the bits per pixel
   ushort get_bits_per_pixel()() nothrow {
      return gdk_image_get_bits_per_pixel(&this);
   }

   // VERSION: 2.22
   // DEPRECATED (v2.22) method: get_byte_order - #GdkImage should not be used anymore.
   // Determines the byte order of the image.
   // RETURNS: a #GdkVisual
   ByteOrder get_byte_order()() nothrow {
      return gdk_image_get_byte_order(&this);
   }

   // VERSION: 2.22
   // DEPRECATED (v2.22) method: get_bytes_per_line - #GdkImage should not be used anymore.
   // Determines the number of bytes per line of the image.
   // RETURNS: the bytes per line
   ushort get_bytes_per_line()() nothrow {
      return gdk_image_get_bytes_per_line(&this);
   }

   // VERSION: 2.22
   // DEPRECATED (v2.22) method: get_bytes_per_pixel - #GdkImage should not be used anymore.
   // Determines the number of bytes per pixel of the image.
   // RETURNS: the bytes per pixel
   ushort get_bytes_per_pixel()() nothrow {
      return gdk_image_get_bytes_per_pixel(&this);
   }

   // Unintrospectable method: get_colormap() / gdk_image_get_colormap()
   // DEPRECATED (v2.22) method: get_colormap - #GdkImage should not be used anymore.
   // Retrieves the colormap for a given image, if it exists.  An image
   // will have a colormap if the drawable from which it was created has
   // a colormap, or if a colormap was set explicitely with
   // gdk_image_set_colormap().
   // RETURNS: colormap for the image
   Colormap* get_colormap()() nothrow {
      return gdk_image_get_colormap(&this);
   }

   // VERSION: 2.22
   // DEPRECATED (v2.22) method: get_depth - #GdkImage should not be used anymore.
   // Determines the depth of the image.
   // RETURNS: the depth
   ushort get_depth()() nothrow {
      return gdk_image_get_depth(&this);
   }

   // VERSION: 2.22
   // DEPRECATED (v2.22) method: get_height - #GdkImage should not be used anymore.
   // Determines the height of the image.
   // RETURNS: the height
   int get_height()() nothrow {
      return gdk_image_get_height(&this);
   }

   // VERSION: 2.22
   // DEPRECATED (v2.22) method: get_image_type - #GdkImage should not be used anymore.
   // Determines the type of a given image.
   // RETURNS: the #GdkImageType of the image
   ImageType get_image_type()() nothrow {
      return gdk_image_get_image_type(&this);
   }
   uint get_pixel()(int x, int y) nothrow {
      return gdk_image_get_pixel(&this, x, y);
   }

   // Unintrospectable method: get_pixels() / gdk_image_get_pixels()
   // VERSION: 2.22
   // DEPRECATED (v2.22) method: get_pixels - #GdkImage should not be used anymore.
   // Returns a pointer to the pixel data of the image.
   // RETURNS: the pixel data of the image
   void* get_pixels()() nothrow {
      return gdk_image_get_pixels(&this);
   }

   // Unintrospectable method: get_visual() / gdk_image_get_visual()
   // VERSION: 2.22
   // DEPRECATED (v2.22) method: get_visual - #GdkImage should not be used anymore.
   // Determines the visual that was used to create the image.
   // RETURNS: a #GdkVisual
   Visual* get_visual()() nothrow {
      return gdk_image_get_visual(&this);
   }

   // VERSION: 2.22
   // DEPRECATED (v2.22) method: get_width - #GdkImage should not be used anymore.
   // Determines the width of the image.
   // RETURNS: the width
   int get_width()() nothrow {
      return gdk_image_get_width(&this);
   }
   void put_pixel()(int x, int y, uint pixel) nothrow {
      gdk_image_put_pixel(&this, x, y, pixel);
   }

   // Unintrospectable method: ref() / gdk_image_ref()
   // DEPRECATED (v2.0) method: ref - Use g_object_ref() instead.
   // Deprecated function; use g_object_ref() instead.
   // RETURNS: the image
   Image* ref_()() nothrow {
      return gdk_image_ref(&this);
   }

   // DEPRECATED (v2.22) method: set_colormap - #GdkImage should not be used anymore.
   // Sets the colormap for the image to the given colormap.  Normally
   // there's no need to use this function, images are created with the
   // correct colormap if you get the image from a drawable. If you
   // create the image from scratch, use the colormap of the drawable you
   // intend to render the image to.
   // <colormap>: a #GdkColormap
   void set_colormap(AT0)(AT0 /*Colormap*/ colormap) nothrow {
      gdk_image_set_colormap(&this, UpCast!(Colormap*)(colormap));
   }

   // DEPRECATED (v2.0) method: unref - Use g_object_unref() instead.
   // Deprecated function; use g_object_unref() instead.
   void unref()() nothrow {
      gdk_image_unref(&this);
   }
}

struct ImageClass {
   GObject2.ObjectClass parent_class;
}

enum ImageType {
   NORMAL = 0,
   SHARED = 1,
   FASTEST = 2
}
enum InputCondition {
   READ = 1,
   WRITE = 2,
   EXCEPTION = 4
}
extern (C) alias void function (void* data, int source, InputCondition condition) nothrow InputFunction;

enum InputMode {
   DISABLED = 0,
   SCREEN = 1,
   WINDOW = 2
}
enum InputSource {
   MOUSE = 0,
   PEN = 1,
   ERASER = 2,
   CURSOR = 3
}
enum JoinStyle {
   MITER = 0,
   ROUND = 1,
   BEVEL = 2
}
enum int KEY_0 = 48;
enum int KEY_1 = 49;
enum int KEY_2 = 50;
enum int KEY_3 = 51;
enum int KEY_3270_AltCursor = 64784;
enum int KEY_3270_Attn = 64782;
enum int KEY_3270_BackTab = 64773;
enum int KEY_3270_ChangeScreen = 64793;
enum int KEY_3270_Copy = 64789;
enum int KEY_3270_CursorBlink = 64783;
enum int KEY_3270_CursorSelect = 64796;
enum int KEY_3270_DeleteWord = 64794;
enum int KEY_3270_Duplicate = 64769;
enum int KEY_3270_Enter = 64798;
enum int KEY_3270_EraseEOF = 64774;
enum int KEY_3270_EraseInput = 64775;
enum int KEY_3270_ExSelect = 64795;
enum int KEY_3270_FieldMark = 64770;
enum int KEY_3270_Ident = 64787;
enum int KEY_3270_Jump = 64786;
enum int KEY_3270_KeyClick = 64785;
enum int KEY_3270_Left2 = 64772;
enum int KEY_3270_PA1 = 64778;
enum int KEY_3270_PA2 = 64779;
enum int KEY_3270_PA3 = 64780;
enum int KEY_3270_Play = 64790;
enum int KEY_3270_PrintScreen = 64797;
enum int KEY_3270_Quit = 64777;
enum int KEY_3270_Record = 64792;
enum int KEY_3270_Reset = 64776;
enum int KEY_3270_Right2 = 64771;
enum int KEY_3270_Rule = 64788;
enum int KEY_3270_Setup = 64791;
enum int KEY_3270_Test = 64781;
enum int KEY_4 = 52;
enum int KEY_5 = 53;
enum int KEY_6 = 54;
enum int KEY_7 = 55;
enum int KEY_8 = 56;
enum int KEY_9 = 57;
enum int KEY_A = 65;
enum int KEY_AE = 198;
enum int KEY_Aacute = 193;
enum int KEY_Abelowdot = 16785056;
enum int KEY_Abreve = 451;
enum int KEY_Abreveacute = 16785070;
enum int KEY_Abrevebelowdot = 16785078;
enum int KEY_Abrevegrave = 16785072;
enum int KEY_Abrevehook = 16785074;
enum int KEY_Abrevetilde = 16785076;
enum int KEY_AccessX_Enable = 65136;
enum int KEY_AccessX_Feedback_Enable = 65137;
enum int KEY_Acircumflex = 194;
enum int KEY_Acircumflexacute = 16785060;
enum int KEY_Acircumflexbelowdot = 16785068;
enum int KEY_Acircumflexgrave = 16785062;
enum int KEY_Acircumflexhook = 16785064;
enum int KEY_Acircumflextilde = 16785066;
enum int KEY_AddFavorite = 269025081;
enum int KEY_Adiaeresis = 196;
enum int KEY_Agrave = 192;
enum int KEY_Ahook = 16785058;
enum int KEY_Alt_L = 65513;
enum int KEY_Alt_R = 65514;
enum int KEY_Amacron = 960;
enum int KEY_Aogonek = 417;
enum int KEY_ApplicationLeft = 269025104;
enum int KEY_ApplicationRight = 269025105;
enum int KEY_Arabic_0 = 16778848;
enum int KEY_Arabic_1 = 16778849;
enum int KEY_Arabic_2 = 16778850;
enum int KEY_Arabic_3 = 16778851;
enum int KEY_Arabic_4 = 16778852;
enum int KEY_Arabic_5 = 16778853;
enum int KEY_Arabic_6 = 16778854;
enum int KEY_Arabic_7 = 16778855;
enum int KEY_Arabic_8 = 16778856;
enum int KEY_Arabic_9 = 16778857;
enum int KEY_Arabic_ain = 1497;
enum int KEY_Arabic_alef = 1479;
enum int KEY_Arabic_alefmaksura = 1513;
enum int KEY_Arabic_beh = 1480;
enum int KEY_Arabic_comma = 1452;
enum int KEY_Arabic_dad = 1494;
enum int KEY_Arabic_dal = 1487;
enum int KEY_Arabic_damma = 1519;
enum int KEY_Arabic_dammatan = 1516;
enum int KEY_Arabic_ddal = 16778888;
enum int KEY_Arabic_farsi_yeh = 16778956;
enum int KEY_Arabic_fatha = 1518;
enum int KEY_Arabic_fathatan = 1515;
enum int KEY_Arabic_feh = 1505;
enum int KEY_Arabic_fullstop = 16778964;
enum int KEY_Arabic_gaf = 16778927;
enum int KEY_Arabic_ghain = 1498;
enum int KEY_Arabic_ha = 1511;
enum int KEY_Arabic_hah = 1485;
enum int KEY_Arabic_hamza = 1473;
enum int KEY_Arabic_hamza_above = 16778836;
enum int KEY_Arabic_hamza_below = 16778837;
enum int KEY_Arabic_hamzaonalef = 1475;
enum int KEY_Arabic_hamzaonwaw = 1476;
enum int KEY_Arabic_hamzaonyeh = 1478;
enum int KEY_Arabic_hamzaunderalef = 1477;
enum int KEY_Arabic_heh = 1511;
enum int KEY_Arabic_heh_doachashmee = 16778942;
enum int KEY_Arabic_heh_goal = 16778945;
enum int KEY_Arabic_jeem = 1484;
enum int KEY_Arabic_jeh = 16778904;
enum int KEY_Arabic_kaf = 1507;
enum int KEY_Arabic_kasra = 1520;
enum int KEY_Arabic_kasratan = 1517;
enum int KEY_Arabic_keheh = 16778921;
enum int KEY_Arabic_khah = 1486;
enum int KEY_Arabic_lam = 1508;
enum int KEY_Arabic_madda_above = 16778835;
enum int KEY_Arabic_maddaonalef = 1474;
enum int KEY_Arabic_meem = 1509;
enum int KEY_Arabic_noon = 1510;
enum int KEY_Arabic_noon_ghunna = 16778938;
enum int KEY_Arabic_peh = 16778878;
enum int KEY_Arabic_percent = 16778858;
enum int KEY_Arabic_qaf = 1506;
enum int KEY_Arabic_question_mark = 1471;
enum int KEY_Arabic_ra = 1489;
enum int KEY_Arabic_rreh = 16778897;
enum int KEY_Arabic_sad = 1493;
enum int KEY_Arabic_seen = 1491;
enum int KEY_Arabic_semicolon = 1467;
enum int KEY_Arabic_shadda = 1521;
enum int KEY_Arabic_sheen = 1492;
enum int KEY_Arabic_sukun = 1522;
enum int KEY_Arabic_superscript_alef = 16778864;
enum int KEY_Arabic_switch = 65406;
enum int KEY_Arabic_tah = 1495;
enum int KEY_Arabic_tatweel = 1504;
enum int KEY_Arabic_tcheh = 16778886;
enum int KEY_Arabic_teh = 1482;
enum int KEY_Arabic_tehmarbuta = 1481;
enum int KEY_Arabic_thal = 1488;
enum int KEY_Arabic_theh = 1483;
enum int KEY_Arabic_tteh = 16778873;
enum int KEY_Arabic_veh = 16778916;
enum int KEY_Arabic_waw = 1512;
enum int KEY_Arabic_yeh = 1514;
enum int KEY_Arabic_yeh_baree = 16778962;
enum int KEY_Arabic_zah = 1496;
enum int KEY_Arabic_zain = 1490;
enum int KEY_Aring = 197;
enum int KEY_Armenian_AT = 16778552;
enum int KEY_Armenian_AYB = 16778545;
enum int KEY_Armenian_BEN = 16778546;
enum int KEY_Armenian_CHA = 16778569;
enum int KEY_Armenian_DA = 16778548;
enum int KEY_Armenian_DZA = 16778561;
enum int KEY_Armenian_E = 16778551;
enum int KEY_Armenian_FE = 16778582;
enum int KEY_Armenian_GHAT = 16778562;
enum int KEY_Armenian_GIM = 16778547;
enum int KEY_Armenian_HI = 16778565;
enum int KEY_Armenian_HO = 16778560;
enum int KEY_Armenian_INI = 16778555;
enum int KEY_Armenian_JE = 16778571;
enum int KEY_Armenian_KE = 16778580;
enum int KEY_Armenian_KEN = 16778559;
enum int KEY_Armenian_KHE = 16778557;
enum int KEY_Armenian_LYUN = 16778556;
enum int KEY_Armenian_MEN = 16778564;
enum int KEY_Armenian_NU = 16778566;
enum int KEY_Armenian_O = 16778581;
enum int KEY_Armenian_PE = 16778570;
enum int KEY_Armenian_PYUR = 16778579;
enum int KEY_Armenian_RA = 16778572;
enum int KEY_Armenian_RE = 16778576;
enum int KEY_Armenian_SE = 16778573;
enum int KEY_Armenian_SHA = 16778567;
enum int KEY_Armenian_TCHE = 16778563;
enum int KEY_Armenian_TO = 16778553;
enum int KEY_Armenian_TSA = 16778558;
enum int KEY_Armenian_TSO = 16778577;
enum int KEY_Armenian_TYUN = 16778575;
enum int KEY_Armenian_VEV = 16778574;
enum int KEY_Armenian_VO = 16778568;
enum int KEY_Armenian_VYUN = 16778578;
enum int KEY_Armenian_YECH = 16778549;
enum int KEY_Armenian_ZA = 16778550;
enum int KEY_Armenian_ZHE = 16778554;
enum int KEY_Armenian_accent = 16778587;
enum int KEY_Armenian_amanak = 16778588;
enum int KEY_Armenian_apostrophe = 16778586;
enum int KEY_Armenian_at = 16778600;
enum int KEY_Armenian_ayb = 16778593;
enum int KEY_Armenian_ben = 16778594;
enum int KEY_Armenian_but = 16778589;
enum int KEY_Armenian_cha = 16778617;
enum int KEY_Armenian_da = 16778596;
enum int KEY_Armenian_dza = 16778609;
enum int KEY_Armenian_e = 16778599;
enum int KEY_Armenian_exclam = 16778588;
enum int KEY_Armenian_fe = 16778630;
enum int KEY_Armenian_full_stop = 16778633;
enum int KEY_Armenian_ghat = 16778610;
enum int KEY_Armenian_gim = 16778595;
enum int KEY_Armenian_hi = 16778613;
enum int KEY_Armenian_ho = 16778608;
enum int KEY_Armenian_hyphen = 16778634;
enum int KEY_Armenian_ini = 16778603;
enum int KEY_Armenian_je = 16778619;
enum int KEY_Armenian_ke = 16778628;
enum int KEY_Armenian_ken = 16778607;
enum int KEY_Armenian_khe = 16778605;
enum int KEY_Armenian_ligature_ew = 16778631;
enum int KEY_Armenian_lyun = 16778604;
enum int KEY_Armenian_men = 16778612;
enum int KEY_Armenian_nu = 16778614;
enum int KEY_Armenian_o = 16778629;
enum int KEY_Armenian_paruyk = 16778590;
enum int KEY_Armenian_pe = 16778618;
enum int KEY_Armenian_pyur = 16778627;
enum int KEY_Armenian_question = 16778590;
enum int KEY_Armenian_ra = 16778620;
enum int KEY_Armenian_re = 16778624;
enum int KEY_Armenian_se = 16778621;
enum int KEY_Armenian_separation_mark = 16778589;
enum int KEY_Armenian_sha = 16778615;
enum int KEY_Armenian_shesht = 16778587;
enum int KEY_Armenian_tche = 16778611;
enum int KEY_Armenian_to = 16778601;
enum int KEY_Armenian_tsa = 16778606;
enum int KEY_Armenian_tso = 16778625;
enum int KEY_Armenian_tyun = 16778623;
enum int KEY_Armenian_verjaket = 16778633;
enum int KEY_Armenian_vev = 16778622;
enum int KEY_Armenian_vo = 16778616;
enum int KEY_Armenian_vyun = 16778626;
enum int KEY_Armenian_yech = 16778597;
enum int KEY_Armenian_yentamna = 16778634;
enum int KEY_Armenian_za = 16778598;
enum int KEY_Armenian_zhe = 16778602;
enum int KEY_Atilde = 195;
enum int KEY_AudibleBell_Enable = 65146;
enum int KEY_AudioCycleTrack = 269025179;
enum int KEY_AudioForward = 269025175;
enum int KEY_AudioLowerVolume = 269025041;
enum int KEY_AudioMedia = 269025074;
enum int KEY_AudioMute = 269025042;
enum int KEY_AudioNext = 269025047;
enum int KEY_AudioPause = 269025073;
enum int KEY_AudioPlay = 269025044;
enum int KEY_AudioPrev = 269025046;
enum int KEY_AudioRaiseVolume = 269025043;
enum int KEY_AudioRandomPlay = 269025177;
enum int KEY_AudioRecord = 269025052;
enum int KEY_AudioRepeat = 269025176;
enum int KEY_AudioRewind = 269025086;
enum int KEY_AudioStop = 269025045;
enum int KEY_Away = 269025165;
enum int KEY_B = 66;
enum int KEY_Babovedot = 16784898;
enum int KEY_Back = 269025062;
enum int KEY_BackForward = 269025087;
enum int KEY_BackSpace = 65288;
enum int KEY_Battery = 269025171;
enum int KEY_Begin = 65368;
enum int KEY_Blue = 269025190;
enum int KEY_Bluetooth = 269025172;
enum int KEY_Book = 269025106;
enum int KEY_BounceKeys_Enable = 65140;
enum int KEY_Break = 65387;
enum int KEY_BrightnessAdjust = 269025083;
enum int KEY_Byelorussian_SHORTU = 1726;
enum int KEY_Byelorussian_shortu = 1710;
enum int KEY_C = 67;
enum int KEY_CD = 269025107;
enum int KEY_Cabovedot = 709;
enum int KEY_Cacute = 454;
enum int KEY_Calculator = 269025053;
enum int KEY_Calendar = 269025056;
enum int KEY_Cancel = 65385;
enum int KEY_Caps_Lock = 65509;
enum int KEY_Ccaron = 456;
enum int KEY_Ccedilla = 199;
enum int KEY_Ccircumflex = 710;
enum int KEY_Clear = 65291;
enum int KEY_ClearGrab = 269024801;
enum int KEY_Close = 269025110;
enum int KEY_Codeinput = 65335;
enum int KEY_ColonSign = 16785569;
enum int KEY_Community = 269025085;
enum int KEY_ContrastAdjust = 269025058;
enum int KEY_Control_L = 65507;
enum int KEY_Control_R = 65508;
enum int KEY_Copy = 269025111;
enum int KEY_CruzeiroSign = 16785570;
enum int KEY_Cut = 269025112;
enum int KEY_CycleAngle = 269025180;
enum int KEY_Cyrillic_A = 1761;
enum int KEY_Cyrillic_BE = 1762;
enum int KEY_Cyrillic_CHE = 1790;
enum int KEY_Cyrillic_CHE_descender = 16778422;
enum int KEY_Cyrillic_CHE_vertstroke = 16778424;
enum int KEY_Cyrillic_DE = 1764;
enum int KEY_Cyrillic_DZHE = 1727;
enum int KEY_Cyrillic_E = 1788;
enum int KEY_Cyrillic_EF = 1766;
enum int KEY_Cyrillic_EL = 1772;
enum int KEY_Cyrillic_EM = 1773;
enum int KEY_Cyrillic_EN = 1774;
enum int KEY_Cyrillic_EN_descender = 16778402;
enum int KEY_Cyrillic_ER = 1778;
enum int KEY_Cyrillic_ES = 1779;
enum int KEY_Cyrillic_GHE = 1767;
enum int KEY_Cyrillic_GHE_bar = 16778386;
enum int KEY_Cyrillic_HA = 1768;
enum int KEY_Cyrillic_HARDSIGN = 1791;
enum int KEY_Cyrillic_HA_descender = 16778418;
enum int KEY_Cyrillic_I = 1769;
enum int KEY_Cyrillic_IE = 1765;
enum int KEY_Cyrillic_IO = 1715;
enum int KEY_Cyrillic_I_macron = 16778466;
enum int KEY_Cyrillic_JE = 1720;
enum int KEY_Cyrillic_KA = 1771;
enum int KEY_Cyrillic_KA_descender = 16778394;
enum int KEY_Cyrillic_KA_vertstroke = 16778396;
enum int KEY_Cyrillic_LJE = 1721;
enum int KEY_Cyrillic_NJE = 1722;
enum int KEY_Cyrillic_O = 1775;
enum int KEY_Cyrillic_O_bar = 16778472;
enum int KEY_Cyrillic_PE = 1776;
enum int KEY_Cyrillic_SCHWA = 16778456;
enum int KEY_Cyrillic_SHA = 1787;
enum int KEY_Cyrillic_SHCHA = 1789;
enum int KEY_Cyrillic_SHHA = 16778426;
enum int KEY_Cyrillic_SHORTI = 1770;
enum int KEY_Cyrillic_SOFTSIGN = 1784;
enum int KEY_Cyrillic_TE = 1780;
enum int KEY_Cyrillic_TSE = 1763;
enum int KEY_Cyrillic_U = 1781;
enum int KEY_Cyrillic_U_macron = 16778478;
enum int KEY_Cyrillic_U_straight = 16778414;
enum int KEY_Cyrillic_U_straight_bar = 16778416;
enum int KEY_Cyrillic_VE = 1783;
enum int KEY_Cyrillic_YA = 1777;
enum int KEY_Cyrillic_YERU = 1785;
enum int KEY_Cyrillic_YU = 1760;
enum int KEY_Cyrillic_ZE = 1786;
enum int KEY_Cyrillic_ZHE = 1782;
enum int KEY_Cyrillic_ZHE_descender = 16778390;
enum int KEY_Cyrillic_a = 1729;
enum int KEY_Cyrillic_be = 1730;
enum int KEY_Cyrillic_che = 1758;
enum int KEY_Cyrillic_che_descender = 16778423;
enum int KEY_Cyrillic_che_vertstroke = 16778425;
enum int KEY_Cyrillic_de = 1732;
enum int KEY_Cyrillic_dzhe = 1711;
enum int KEY_Cyrillic_e = 1756;
enum int KEY_Cyrillic_ef = 1734;
enum int KEY_Cyrillic_el = 1740;
enum int KEY_Cyrillic_em = 1741;
enum int KEY_Cyrillic_en = 1742;
enum int KEY_Cyrillic_en_descender = 16778403;
enum int KEY_Cyrillic_er = 1746;
enum int KEY_Cyrillic_es = 1747;
enum int KEY_Cyrillic_ghe = 1735;
enum int KEY_Cyrillic_ghe_bar = 16778387;
enum int KEY_Cyrillic_ha = 1736;
enum int KEY_Cyrillic_ha_descender = 16778419;
enum int KEY_Cyrillic_hardsign = 1759;
enum int KEY_Cyrillic_i = 1737;
enum int KEY_Cyrillic_i_macron = 16778467;
enum int KEY_Cyrillic_ie = 1733;
enum int KEY_Cyrillic_io = 1699;
enum int KEY_Cyrillic_je = 1704;
enum int KEY_Cyrillic_ka = 1739;
enum int KEY_Cyrillic_ka_descender = 16778395;
enum int KEY_Cyrillic_ka_vertstroke = 16778397;
enum int KEY_Cyrillic_lje = 1705;
enum int KEY_Cyrillic_nje = 1706;
enum int KEY_Cyrillic_o = 1743;
enum int KEY_Cyrillic_o_bar = 16778473;
enum int KEY_Cyrillic_pe = 1744;
enum int KEY_Cyrillic_schwa = 16778457;
enum int KEY_Cyrillic_sha = 1755;
enum int KEY_Cyrillic_shcha = 1757;
enum int KEY_Cyrillic_shha = 16778427;
enum int KEY_Cyrillic_shorti = 1738;
enum int KEY_Cyrillic_softsign = 1752;
enum int KEY_Cyrillic_te = 1748;
enum int KEY_Cyrillic_tse = 1731;
enum int KEY_Cyrillic_u = 1749;
enum int KEY_Cyrillic_u_macron = 16778479;
enum int KEY_Cyrillic_u_straight = 16778415;
enum int KEY_Cyrillic_u_straight_bar = 16778417;
enum int KEY_Cyrillic_ve = 1751;
enum int KEY_Cyrillic_ya = 1745;
enum int KEY_Cyrillic_yeru = 1753;
enum int KEY_Cyrillic_yu = 1728;
enum int KEY_Cyrillic_ze = 1754;
enum int KEY_Cyrillic_zhe = 1750;
enum int KEY_Cyrillic_zhe_descender = 16778391;
enum int KEY_D = 68;
enum int KEY_DOS = 269025114;
enum int KEY_Dabovedot = 16784906;
enum int KEY_Dcaron = 463;
enum int KEY_Delete = 65535;
enum int KEY_Display = 269025113;
enum int KEY_Documents = 269025115;
enum int KEY_DongSign = 16785579;
enum int KEY_Down = 65364;
enum int KEY_Dstroke = 464;
enum int KEY_E = 69;
enum int KEY_ENG = 957;
enum int KEY_ETH = 208;
enum int KEY_Eabovedot = 972;
enum int KEY_Eacute = 201;
enum int KEY_Ebelowdot = 16785080;
enum int KEY_Ecaron = 460;
enum int KEY_Ecircumflex = 202;
enum int KEY_Ecircumflexacute = 16785086;
enum int KEY_Ecircumflexbelowdot = 16785094;
enum int KEY_Ecircumflexgrave = 16785088;
enum int KEY_Ecircumflexhook = 16785090;
enum int KEY_Ecircumflextilde = 16785092;
enum int KEY_EcuSign = 16785568;
enum int KEY_Ediaeresis = 203;
enum int KEY_Egrave = 200;
enum int KEY_Ehook = 16785082;
enum int KEY_Eisu_Shift = 65327;
enum int KEY_Eisu_toggle = 65328;
enum int KEY_Eject = 269025068;
enum int KEY_Emacron = 938;
enum int KEY_End = 65367;
enum int KEY_Eogonek = 458;
enum int KEY_Escape = 65307;
enum int KEY_Eth = 208;
enum int KEY_Etilde = 16785084;
enum int KEY_EuroSign = 8364;
enum int KEY_Excel = 269025116;
enum int KEY_Execute = 65378;
enum int KEY_Explorer = 269025117;
enum int KEY_F = 70;
enum int KEY_F1 = 65470;
enum int KEY_F10 = 65479;
enum int KEY_F11 = 65480;
enum int KEY_F12 = 65481;
enum int KEY_F13 = 65482;
enum int KEY_F14 = 65483;
enum int KEY_F15 = 65484;
enum int KEY_F16 = 65485;
enum int KEY_F17 = 65486;
enum int KEY_F18 = 65487;
enum int KEY_F19 = 65488;
enum int KEY_F2 = 65471;
enum int KEY_F20 = 65489;
enum int KEY_F21 = 65490;
enum int KEY_F22 = 65491;
enum int KEY_F23 = 65492;
enum int KEY_F24 = 65493;
enum int KEY_F25 = 65494;
enum int KEY_F26 = 65495;
enum int KEY_F27 = 65496;
enum int KEY_F28 = 65497;
enum int KEY_F29 = 65498;
enum int KEY_F3 = 65472;
enum int KEY_F30 = 65499;
enum int KEY_F31 = 65500;
enum int KEY_F32 = 65501;
enum int KEY_F33 = 65502;
enum int KEY_F34 = 65503;
enum int KEY_F35 = 65504;
enum int KEY_F4 = 65473;
enum int KEY_F5 = 65474;
enum int KEY_F6 = 65475;
enum int KEY_F7 = 65476;
enum int KEY_F8 = 65477;
enum int KEY_F9 = 65478;
enum int KEY_FFrancSign = 16785571;
enum int KEY_Fabovedot = 16784926;
enum int KEY_Farsi_0 = 16778992;
enum int KEY_Farsi_1 = 16778993;
enum int KEY_Farsi_2 = 16778994;
enum int KEY_Farsi_3 = 16778995;
enum int KEY_Farsi_4 = 16778996;
enum int KEY_Farsi_5 = 16778997;
enum int KEY_Farsi_6 = 16778998;
enum int KEY_Farsi_7 = 16778999;
enum int KEY_Farsi_8 = 16779000;
enum int KEY_Farsi_9 = 16779001;
enum int KEY_Farsi_yeh = 16778956;
enum int KEY_Favorites = 269025072;
enum int KEY_Finance = 269025084;
enum int KEY_Find = 65384;
enum int KEY_First_Virtual_Screen = 65232;
enum int KEY_Forward = 269025063;
enum int KEY_FrameBack = 269025181;
enum int KEY_FrameForward = 269025182;
enum int KEY_G = 71;
enum int KEY_Gabovedot = 725;
enum int KEY_Game = 269025118;
enum int KEY_Gbreve = 683;
enum int KEY_Gcaron = 16777702;
enum int KEY_Gcedilla = 939;
enum int KEY_Gcircumflex = 728;
enum int KEY_Georgian_an = 16781520;
enum int KEY_Georgian_ban = 16781521;
enum int KEY_Georgian_can = 16781546;
enum int KEY_Georgian_char = 16781549;
enum int KEY_Georgian_chin = 16781545;
enum int KEY_Georgian_cil = 16781548;
enum int KEY_Georgian_don = 16781523;
enum int KEY_Georgian_en = 16781524;
enum int KEY_Georgian_fi = 16781558;
enum int KEY_Georgian_gan = 16781522;
enum int KEY_Georgian_ghan = 16781542;
enum int KEY_Georgian_hae = 16781552;
enum int KEY_Georgian_har = 16781556;
enum int KEY_Georgian_he = 16781553;
enum int KEY_Georgian_hie = 16781554;
enum int KEY_Georgian_hoe = 16781557;
enum int KEY_Georgian_in = 16781528;
enum int KEY_Georgian_jhan = 16781551;
enum int KEY_Georgian_jil = 16781547;
enum int KEY_Georgian_kan = 16781529;
enum int KEY_Georgian_khar = 16781541;
enum int KEY_Georgian_las = 16781530;
enum int KEY_Georgian_man = 16781531;
enum int KEY_Georgian_nar = 16781532;
enum int KEY_Georgian_on = 16781533;
enum int KEY_Georgian_par = 16781534;
enum int KEY_Georgian_phar = 16781540;
enum int KEY_Georgian_qar = 16781543;
enum int KEY_Georgian_rae = 16781536;
enum int KEY_Georgian_san = 16781537;
enum int KEY_Georgian_shin = 16781544;
enum int KEY_Georgian_tan = 16781527;
enum int KEY_Georgian_tar = 16781538;
enum int KEY_Georgian_un = 16781539;
enum int KEY_Georgian_vin = 16781525;
enum int KEY_Georgian_we = 16781555;
enum int KEY_Georgian_xan = 16781550;
enum int KEY_Georgian_zen = 16781526;
enum int KEY_Georgian_zhar = 16781535;
enum int KEY_Go = 269025119;
enum int KEY_Greek_ALPHA = 1985;
enum int KEY_Greek_ALPHAaccent = 1953;
enum int KEY_Greek_BETA = 1986;
enum int KEY_Greek_CHI = 2007;
enum int KEY_Greek_DELTA = 1988;
enum int KEY_Greek_EPSILON = 1989;
enum int KEY_Greek_EPSILONaccent = 1954;
enum int KEY_Greek_ETA = 1991;
enum int KEY_Greek_ETAaccent = 1955;
enum int KEY_Greek_GAMMA = 1987;
enum int KEY_Greek_IOTA = 1993;
enum int KEY_Greek_IOTAaccent = 1956;
enum int KEY_Greek_IOTAdiaeresis = 1957;
enum int KEY_Greek_IOTAdieresis = 1957;
enum int KEY_Greek_KAPPA = 1994;
enum int KEY_Greek_LAMBDA = 1995;
enum int KEY_Greek_LAMDA = 1995;
enum int KEY_Greek_MU = 1996;
enum int KEY_Greek_NU = 1997;
enum int KEY_Greek_OMEGA = 2009;
enum int KEY_Greek_OMEGAaccent = 1963;
enum int KEY_Greek_OMICRON = 1999;
enum int KEY_Greek_OMICRONaccent = 1959;
enum int KEY_Greek_PHI = 2006;
enum int KEY_Greek_PI = 2000;
enum int KEY_Greek_PSI = 2008;
enum int KEY_Greek_RHO = 2001;
enum int KEY_Greek_SIGMA = 2002;
enum int KEY_Greek_TAU = 2004;
enum int KEY_Greek_THETA = 1992;
enum int KEY_Greek_UPSILON = 2005;
enum int KEY_Greek_UPSILONaccent = 1960;
enum int KEY_Greek_UPSILONdieresis = 1961;
enum int KEY_Greek_XI = 1998;
enum int KEY_Greek_ZETA = 1990;
enum int KEY_Greek_accentdieresis = 1966;
enum int KEY_Greek_alpha = 2017;
enum int KEY_Greek_alphaaccent = 1969;
enum int KEY_Greek_beta = 2018;
enum int KEY_Greek_chi = 2039;
enum int KEY_Greek_delta = 2020;
enum int KEY_Greek_epsilon = 2021;
enum int KEY_Greek_epsilonaccent = 1970;
enum int KEY_Greek_eta = 2023;
enum int KEY_Greek_etaaccent = 1971;
enum int KEY_Greek_finalsmallsigma = 2035;
enum int KEY_Greek_gamma = 2019;
enum int KEY_Greek_horizbar = 1967;
enum int KEY_Greek_iota = 2025;
enum int KEY_Greek_iotaaccent = 1972;
enum int KEY_Greek_iotaaccentdieresis = 1974;
enum int KEY_Greek_iotadieresis = 1973;
enum int KEY_Greek_kappa = 2026;
enum int KEY_Greek_lambda = 2027;
enum int KEY_Greek_lamda = 2027;
enum int KEY_Greek_mu = 2028;
enum int KEY_Greek_nu = 2029;
enum int KEY_Greek_omega = 2041;
enum int KEY_Greek_omegaaccent = 1979;
enum int KEY_Greek_omicron = 2031;
enum int KEY_Greek_omicronaccent = 1975;
enum int KEY_Greek_phi = 2038;
enum int KEY_Greek_pi = 2032;
enum int KEY_Greek_psi = 2040;
enum int KEY_Greek_rho = 2033;
enum int KEY_Greek_sigma = 2034;
enum int KEY_Greek_switch = 65406;
enum int KEY_Greek_tau = 2036;
enum int KEY_Greek_theta = 2024;
enum int KEY_Greek_upsilon = 2037;
enum int KEY_Greek_upsilonaccent = 1976;
enum int KEY_Greek_upsilonaccentdieresis = 1978;
enum int KEY_Greek_upsilondieresis = 1977;
enum int KEY_Greek_xi = 2030;
enum int KEY_Greek_zeta = 2022;
enum int KEY_Green = 269025188;
enum int KEY_H = 72;
enum int KEY_Hangul = 65329;
enum int KEY_Hangul_A = 3775;
enum int KEY_Hangul_AE = 3776;
enum int KEY_Hangul_AraeA = 3830;
enum int KEY_Hangul_AraeAE = 3831;
enum int KEY_Hangul_Banja = 65337;
enum int KEY_Hangul_Cieuc = 3770;
enum int KEY_Hangul_Codeinput = 65335;
enum int KEY_Hangul_Dikeud = 3751;
enum int KEY_Hangul_E = 3780;
enum int KEY_Hangul_EO = 3779;
enum int KEY_Hangul_EU = 3793;
enum int KEY_Hangul_End = 65331;
enum int KEY_Hangul_Hanja = 65332;
enum int KEY_Hangul_Hieuh = 3774;
enum int KEY_Hangul_I = 3795;
enum int KEY_Hangul_Ieung = 3767;
enum int KEY_Hangul_J_Cieuc = 3818;
enum int KEY_Hangul_J_Dikeud = 3802;
enum int KEY_Hangul_J_Hieuh = 3822;
enum int KEY_Hangul_J_Ieung = 3816;
enum int KEY_Hangul_J_Jieuj = 3817;
enum int KEY_Hangul_J_Khieuq = 3819;
enum int KEY_Hangul_J_Kiyeog = 3796;
enum int KEY_Hangul_J_KiyeogSios = 3798;
enum int KEY_Hangul_J_KkogjiDalrinIeung = 3833;
enum int KEY_Hangul_J_Mieum = 3811;
enum int KEY_Hangul_J_Nieun = 3799;
enum int KEY_Hangul_J_NieunHieuh = 3801;
enum int KEY_Hangul_J_NieunJieuj = 3800;
enum int KEY_Hangul_J_PanSios = 3832;
enum int KEY_Hangul_J_Phieuf = 3821;
enum int KEY_Hangul_J_Pieub = 3812;
enum int KEY_Hangul_J_PieubSios = 3813;
enum int KEY_Hangul_J_Rieul = 3803;
enum int KEY_Hangul_J_RieulHieuh = 3810;
enum int KEY_Hangul_J_RieulKiyeog = 3804;
enum int KEY_Hangul_J_RieulMieum = 3805;
enum int KEY_Hangul_J_RieulPhieuf = 3809;
enum int KEY_Hangul_J_RieulPieub = 3806;
enum int KEY_Hangul_J_RieulSios = 3807;
enum int KEY_Hangul_J_RieulTieut = 3808;
enum int KEY_Hangul_J_Sios = 3814;
enum int KEY_Hangul_J_SsangKiyeog = 3797;
enum int KEY_Hangul_J_SsangSios = 3815;
enum int KEY_Hangul_J_Tieut = 3820;
enum int KEY_Hangul_J_YeorinHieuh = 3834;
enum int KEY_Hangul_Jamo = 65333;
enum int KEY_Hangul_Jeonja = 65336;
enum int KEY_Hangul_Jieuj = 3768;
enum int KEY_Hangul_Khieuq = 3771;
enum int KEY_Hangul_Kiyeog = 3745;
enum int KEY_Hangul_KiyeogSios = 3747;
enum int KEY_Hangul_KkogjiDalrinIeung = 3827;
enum int KEY_Hangul_Mieum = 3761;
enum int KEY_Hangul_MultipleCandidate = 65341;
enum int KEY_Hangul_Nieun = 3748;
enum int KEY_Hangul_NieunHieuh = 3750;
enum int KEY_Hangul_NieunJieuj = 3749;
enum int KEY_Hangul_O = 3783;
enum int KEY_Hangul_OE = 3786;
enum int KEY_Hangul_PanSios = 3826;
enum int KEY_Hangul_Phieuf = 3773;
enum int KEY_Hangul_Pieub = 3762;
enum int KEY_Hangul_PieubSios = 3764;
enum int KEY_Hangul_PostHanja = 65339;
enum int KEY_Hangul_PreHanja = 65338;
enum int KEY_Hangul_PreviousCandidate = 65342;
enum int KEY_Hangul_Rieul = 3753;
enum int KEY_Hangul_RieulHieuh = 3760;
enum int KEY_Hangul_RieulKiyeog = 3754;
enum int KEY_Hangul_RieulMieum = 3755;
enum int KEY_Hangul_RieulPhieuf = 3759;
enum int KEY_Hangul_RieulPieub = 3756;
enum int KEY_Hangul_RieulSios = 3757;
enum int KEY_Hangul_RieulTieut = 3758;
enum int KEY_Hangul_RieulYeorinHieuh = 3823;
enum int KEY_Hangul_Romaja = 65334;
enum int KEY_Hangul_SingleCandidate = 65340;
enum int KEY_Hangul_Sios = 3765;
enum int KEY_Hangul_Special = 65343;
enum int KEY_Hangul_SsangDikeud = 3752;
enum int KEY_Hangul_SsangJieuj = 3769;
enum int KEY_Hangul_SsangKiyeog = 3746;
enum int KEY_Hangul_SsangPieub = 3763;
enum int KEY_Hangul_SsangSios = 3766;
enum int KEY_Hangul_Start = 65330;
enum int KEY_Hangul_SunkyeongeumMieum = 3824;
enum int KEY_Hangul_SunkyeongeumPhieuf = 3828;
enum int KEY_Hangul_SunkyeongeumPieub = 3825;
enum int KEY_Hangul_Tieut = 3772;
enum int KEY_Hangul_U = 3788;
enum int KEY_Hangul_WA = 3784;
enum int KEY_Hangul_WAE = 3785;
enum int KEY_Hangul_WE = 3790;
enum int KEY_Hangul_WEO = 3789;
enum int KEY_Hangul_WI = 3791;
enum int KEY_Hangul_YA = 3777;
enum int KEY_Hangul_YAE = 3778;
enum int KEY_Hangul_YE = 3782;
enum int KEY_Hangul_YEO = 3781;
enum int KEY_Hangul_YI = 3794;
enum int KEY_Hangul_YO = 3787;
enum int KEY_Hangul_YU = 3792;
enum int KEY_Hangul_YeorinHieuh = 3829;
enum int KEY_Hangul_switch = 65406;
enum int KEY_Hankaku = 65321;
enum int KEY_Hcircumflex = 678;
enum int KEY_Hebrew_switch = 65406;
enum int KEY_Help = 65386;
enum int KEY_Henkan = 65315;
enum int KEY_Henkan_Mode = 65315;
enum int KEY_Hibernate = 269025192;
enum int KEY_Hiragana = 65317;
enum int KEY_Hiragana_Katakana = 65319;
enum int KEY_History = 269025079;
enum int KEY_Home = 65360;
enum int KEY_HomePage = 269025048;
enum int KEY_HotLinks = 269025082;
enum int KEY_Hstroke = 673;
enum int KEY_Hyper_L = 65517;
enum int KEY_Hyper_R = 65518;
enum int KEY_I = 73;
enum int KEY_ISO_Center_Object = 65075;
enum int KEY_ISO_Continuous_Underline = 65072;
enum int KEY_ISO_Discontinuous_Underline = 65073;
enum int KEY_ISO_Emphasize = 65074;
enum int KEY_ISO_Enter = 65076;
enum int KEY_ISO_Fast_Cursor_Down = 65071;
enum int KEY_ISO_Fast_Cursor_Left = 65068;
enum int KEY_ISO_Fast_Cursor_Right = 65069;
enum int KEY_ISO_Fast_Cursor_Up = 65070;
enum int KEY_ISO_First_Group = 65036;
enum int KEY_ISO_First_Group_Lock = 65037;
enum int KEY_ISO_Group_Latch = 65030;
enum int KEY_ISO_Group_Lock = 65031;
enum int KEY_ISO_Group_Shift = 65406;
enum int KEY_ISO_Last_Group = 65038;
enum int KEY_ISO_Last_Group_Lock = 65039;
enum int KEY_ISO_Left_Tab = 65056;
enum int KEY_ISO_Level2_Latch = 65026;
enum int KEY_ISO_Level3_Latch = 65028;
enum int KEY_ISO_Level3_Lock = 65029;
enum int KEY_ISO_Level3_Shift = 65027;
enum int KEY_ISO_Level5_Latch = 65042;
enum int KEY_ISO_Level5_Lock = 65043;
enum int KEY_ISO_Level5_Shift = 65041;
enum int KEY_ISO_Lock = 65025;
enum int KEY_ISO_Move_Line_Down = 65058;
enum int KEY_ISO_Move_Line_Up = 65057;
enum int KEY_ISO_Next_Group = 65032;
enum int KEY_ISO_Next_Group_Lock = 65033;
enum int KEY_ISO_Partial_Line_Down = 65060;
enum int KEY_ISO_Partial_Line_Up = 65059;
enum int KEY_ISO_Partial_Space_Left = 65061;
enum int KEY_ISO_Partial_Space_Right = 65062;
enum int KEY_ISO_Prev_Group = 65034;
enum int KEY_ISO_Prev_Group_Lock = 65035;
enum int KEY_ISO_Release_Both_Margins = 65067;
enum int KEY_ISO_Release_Margin_Left = 65065;
enum int KEY_ISO_Release_Margin_Right = 65066;
enum int KEY_ISO_Set_Margin_Left = 65063;
enum int KEY_ISO_Set_Margin_Right = 65064;
enum int KEY_Iabovedot = 681;
enum int KEY_Iacute = 205;
enum int KEY_Ibelowdot = 16785098;
enum int KEY_Ibreve = 16777516;
enum int KEY_Icircumflex = 206;
enum int KEY_Idiaeresis = 207;
enum int KEY_Igrave = 204;
enum int KEY_Ihook = 16785096;
enum int KEY_Imacron = 975;
enum int KEY_Insert = 65379;
enum int KEY_Iogonek = 967;
enum int KEY_Itilde = 933;
enum int KEY_J = 74;
enum int KEY_Jcircumflex = 684;
enum int KEY_K = 75;
enum int KEY_KP_0 = 65456;
enum int KEY_KP_1 = 65457;
enum int KEY_KP_2 = 65458;
enum int KEY_KP_3 = 65459;
enum int KEY_KP_4 = 65460;
enum int KEY_KP_5 = 65461;
enum int KEY_KP_6 = 65462;
enum int KEY_KP_7 = 65463;
enum int KEY_KP_8 = 65464;
enum int KEY_KP_9 = 65465;
enum int KEY_KP_Add = 65451;
enum int KEY_KP_Begin = 65437;
enum int KEY_KP_Decimal = 65454;
enum int KEY_KP_Delete = 65439;
enum int KEY_KP_Divide = 65455;
enum int KEY_KP_Down = 65433;
enum int KEY_KP_End = 65436;
enum int KEY_KP_Enter = 65421;
enum int KEY_KP_Equal = 65469;
enum int KEY_KP_F1 = 65425;
enum int KEY_KP_F2 = 65426;
enum int KEY_KP_F3 = 65427;
enum int KEY_KP_F4 = 65428;
enum int KEY_KP_Home = 65429;
enum int KEY_KP_Insert = 65438;
enum int KEY_KP_Left = 65430;
enum int KEY_KP_Multiply = 65450;
enum int KEY_KP_Next = 65435;
enum int KEY_KP_Page_Down = 65435;
enum int KEY_KP_Page_Up = 65434;
enum int KEY_KP_Prior = 65434;
enum int KEY_KP_Right = 65432;
enum int KEY_KP_Separator = 65452;
enum int KEY_KP_Space = 65408;
enum int KEY_KP_Subtract = 65453;
enum int KEY_KP_Tab = 65417;
enum int KEY_KP_Up = 65431;
enum int KEY_Kana_Lock = 65325;
enum int KEY_Kana_Shift = 65326;
enum int KEY_Kanji = 65313;
enum int KEY_Kanji_Bangou = 65335;
enum int KEY_Katakana = 65318;
enum int KEY_KbdBrightnessDown = 269025030;
enum int KEY_KbdBrightnessUp = 269025029;
enum int KEY_KbdLightOnOff = 269025028;
enum int KEY_Kcedilla = 979;
enum int KEY_Korean_Won = 3839;
enum int KEY_L = 76;
enum int KEY_L1 = 65480;
enum int KEY_L10 = 65489;
enum int KEY_L2 = 65481;
enum int KEY_L3 = 65482;
enum int KEY_L4 = 65483;
enum int KEY_L5 = 65484;
enum int KEY_L6 = 65485;
enum int KEY_L7 = 65486;
enum int KEY_L8 = 65487;
enum int KEY_L9 = 65488;
enum int KEY_Lacute = 453;
enum int KEY_Last_Virtual_Screen = 65236;
enum int KEY_Launch0 = 269025088;
enum int KEY_Launch1 = 269025089;
enum int KEY_Launch2 = 269025090;
enum int KEY_Launch3 = 269025091;
enum int KEY_Launch4 = 269025092;
enum int KEY_Launch5 = 269025093;
enum int KEY_Launch6 = 269025094;
enum int KEY_Launch7 = 269025095;
enum int KEY_Launch8 = 269025096;
enum int KEY_Launch9 = 269025097;
enum int KEY_LaunchA = 269025098;
enum int KEY_LaunchB = 269025099;
enum int KEY_LaunchC = 269025100;
enum int KEY_LaunchD = 269025101;
enum int KEY_LaunchE = 269025102;
enum int KEY_LaunchF = 269025103;
enum int KEY_Lbelowdot = 16784950;
enum int KEY_Lcaron = 421;
enum int KEY_Lcedilla = 934;
enum int KEY_Left = 65361;
enum int KEY_LightBulb = 269025077;
enum int KEY_Linefeed = 65290;
enum int KEY_LiraSign = 16785572;
enum int KEY_LogOff = 269025121;
enum int KEY_Lstroke = 419;
enum int KEY_M = 77;
enum int KEY_Mabovedot = 16784960;
enum int KEY_Macedonia_DSE = 1717;
enum int KEY_Macedonia_GJE = 1714;
enum int KEY_Macedonia_KJE = 1724;
enum int KEY_Macedonia_dse = 1701;
enum int KEY_Macedonia_gje = 1698;
enum int KEY_Macedonia_kje = 1708;
enum int KEY_Mae_Koho = 65342;
enum int KEY_Mail = 269025049;
enum int KEY_MailForward = 269025168;
enum int KEY_Market = 269025122;
enum int KEY_Massyo = 65324;
enum int KEY_Meeting = 269025123;
enum int KEY_Memo = 269025054;
enum int KEY_Menu = 65383;
enum int KEY_MenuKB = 269025125;
enum int KEY_MenuPB = 269025126;
enum int KEY_Messenger = 269025166;
enum int KEY_Meta_L = 65511;
enum int KEY_Meta_R = 65512;
enum int KEY_MillSign = 16785573;
enum int KEY_ModeLock = 269025025;
enum int KEY_Mode_switch = 65406;
enum int KEY_MonBrightnessDown = 269025027;
enum int KEY_MonBrightnessUp = 269025026;
enum int KEY_MouseKeys_Accel_Enable = 65143;
enum int KEY_MouseKeys_Enable = 65142;
enum int KEY_Muhenkan = 65314;
enum int KEY_Multi_key = 65312;
enum int KEY_MultipleCandidate = 65341;
enum int KEY_Music = 269025170;
enum int KEY_MyComputer = 269025075;
enum int KEY_MySites = 269025127;
enum int KEY_N = 78;
enum int KEY_Nacute = 465;
enum int KEY_NairaSign = 16785574;
enum int KEY_Ncaron = 466;
enum int KEY_Ncedilla = 977;
enum int KEY_New = 269025128;
enum int KEY_NewSheqelSign = 16785578;
enum int KEY_News = 269025129;
enum int KEY_Next = 65366;
enum int KEY_Next_VMode = 269024802;
enum int KEY_Next_Virtual_Screen = 65234;
enum int KEY_Ntilde = 209;
enum int KEY_Num_Lock = 65407;
enum int KEY_O = 79;
enum int KEY_OE = 5052;
enum int KEY_Oacute = 211;
enum int KEY_Obarred = 16777631;
enum int KEY_Obelowdot = 16785100;
enum int KEY_Ocaron = 16777681;
enum int KEY_Ocircumflex = 212;
enum int KEY_Ocircumflexacute = 16785104;
enum int KEY_Ocircumflexbelowdot = 16785112;
enum int KEY_Ocircumflexgrave = 16785106;
enum int KEY_Ocircumflexhook = 16785108;
enum int KEY_Ocircumflextilde = 16785110;
enum int KEY_Odiaeresis = 214;
enum int KEY_Odoubleacute = 469;
enum int KEY_OfficeHome = 269025130;
enum int KEY_Ograve = 210;
enum int KEY_Ohook = 16785102;
enum int KEY_Ohorn = 16777632;
enum int KEY_Ohornacute = 16785114;
enum int KEY_Ohornbelowdot = 16785122;
enum int KEY_Ohorngrave = 16785116;
enum int KEY_Ohornhook = 16785118;
enum int KEY_Ohorntilde = 16785120;
enum int KEY_Omacron = 978;
enum int KEY_Ooblique = 216;
enum int KEY_Open = 269025131;
enum int KEY_OpenURL = 269025080;
enum int KEY_Option = 269025132;
enum int KEY_Oslash = 216;
enum int KEY_Otilde = 213;
enum int KEY_Overlay1_Enable = 65144;
enum int KEY_Overlay2_Enable = 65145;
enum int KEY_P = 80;
enum int KEY_Pabovedot = 16784982;
enum int KEY_Page_Down = 65366;
enum int KEY_Page_Up = 65365;
enum int KEY_Paste = 269025133;
enum int KEY_Pause = 65299;
enum int KEY_PesetaSign = 16785575;
enum int KEY_Phone = 269025134;
enum int KEY_Pictures = 269025169;
enum int KEY_Pointer_Accelerate = 65274;
enum int KEY_Pointer_Button1 = 65257;
enum int KEY_Pointer_Button2 = 65258;
enum int KEY_Pointer_Button3 = 65259;
enum int KEY_Pointer_Button4 = 65260;
enum int KEY_Pointer_Button5 = 65261;
enum int KEY_Pointer_Button_Dflt = 65256;
enum int KEY_Pointer_DblClick1 = 65263;
enum int KEY_Pointer_DblClick2 = 65264;
enum int KEY_Pointer_DblClick3 = 65265;
enum int KEY_Pointer_DblClick4 = 65266;
enum int KEY_Pointer_DblClick5 = 65267;
enum int KEY_Pointer_DblClick_Dflt = 65262;
enum int KEY_Pointer_DfltBtnNext = 65275;
enum int KEY_Pointer_DfltBtnPrev = 65276;
enum int KEY_Pointer_Down = 65251;
enum int KEY_Pointer_DownLeft = 65254;
enum int KEY_Pointer_DownRight = 65255;
enum int KEY_Pointer_Drag1 = 65269;
enum int KEY_Pointer_Drag2 = 65270;
enum int KEY_Pointer_Drag3 = 65271;
enum int KEY_Pointer_Drag4 = 65272;
enum int KEY_Pointer_Drag5 = 65277;
enum int KEY_Pointer_Drag_Dflt = 65268;
enum int KEY_Pointer_EnableKeys = 65273;
enum int KEY_Pointer_Left = 65248;
enum int KEY_Pointer_Right = 65249;
enum int KEY_Pointer_Up = 65250;
enum int KEY_Pointer_UpLeft = 65252;
enum int KEY_Pointer_UpRight = 65253;
enum int KEY_PowerDown = 269025057;
enum int KEY_PowerOff = 269025066;
enum int KEY_Prev_VMode = 269024803;
enum int KEY_Prev_Virtual_Screen = 65233;
enum int KEY_PreviousCandidate = 65342;
enum int KEY_Print = 65377;
enum int KEY_Prior = 65365;
enum int KEY_Q = 81;
enum int KEY_R = 82;
enum int KEY_R1 = 65490;
enum int KEY_R10 = 65499;
enum int KEY_R11 = 65500;
enum int KEY_R12 = 65501;
enum int KEY_R13 = 65502;
enum int KEY_R14 = 65503;
enum int KEY_R15 = 65504;
enum int KEY_R2 = 65491;
enum int KEY_R3 = 65492;
enum int KEY_R4 = 65493;
enum int KEY_R5 = 65494;
enum int KEY_R6 = 65495;
enum int KEY_R7 = 65496;
enum int KEY_R8 = 65497;
enum int KEY_R9 = 65498;
enum int KEY_Racute = 448;
enum int KEY_Rcaron = 472;
enum int KEY_Rcedilla = 931;
enum int KEY_Red = 269025187;
enum int KEY_Redo = 65382;
enum int KEY_Refresh = 269025065;
enum int KEY_Reload = 269025139;
enum int KEY_RepeatKeys_Enable = 65138;
enum int KEY_Reply = 269025138;
enum int KEY_Return = 65293;
enum int KEY_Right = 65363;
enum int KEY_RockerDown = 269025060;
enum int KEY_RockerEnter = 269025061;
enum int KEY_RockerUp = 269025059;
enum int KEY_Romaji = 65316;
enum int KEY_RotateWindows = 269025140;
enum int KEY_RotationKB = 269025142;
enum int KEY_RotationPB = 269025141;
enum int KEY_RupeeSign = 16785576;
enum int KEY_S = 83;
enum int KEY_SCHWA = 16777615;
enum int KEY_Sabovedot = 16784992;
enum int KEY_Sacute = 422;
enum int KEY_Save = 269025143;
enum int KEY_Scaron = 425;
enum int KEY_Scedilla = 426;
enum int KEY_Scircumflex = 734;
enum int KEY_ScreenSaver = 269025069;
enum int KEY_ScrollClick = 269025146;
enum int KEY_ScrollDown = 269025145;
enum int KEY_ScrollUp = 269025144;
enum int KEY_Scroll_Lock = 65300;
enum int KEY_Search = 269025051;
enum int KEY_Select = 65376;
enum int KEY_SelectButton = 269025184;
enum int KEY_Send = 269025147;
enum int KEY_Serbian_DJE = 1713;
enum int KEY_Serbian_DZE = 1727;
enum int KEY_Serbian_JE = 1720;
enum int KEY_Serbian_LJE = 1721;
enum int KEY_Serbian_NJE = 1722;
enum int KEY_Serbian_TSHE = 1723;
enum int KEY_Serbian_dje = 1697;
enum int KEY_Serbian_dze = 1711;
enum int KEY_Serbian_je = 1704;
enum int KEY_Serbian_lje = 1705;
enum int KEY_Serbian_nje = 1706;
enum int KEY_Serbian_tshe = 1707;
enum int KEY_Shift_L = 65505;
enum int KEY_Shift_Lock = 65510;
enum int KEY_Shift_R = 65506;
enum int KEY_Shop = 269025078;
enum int KEY_SingleCandidate = 65340;
enum int KEY_Sleep = 269025071;
enum int KEY_SlowKeys_Enable = 65139;
enum int KEY_Spell = 269025148;
enum int KEY_SplitScreen = 269025149;
enum int KEY_Standby = 269025040;
enum int KEY_Start = 269025050;
enum int KEY_StickyKeys_Enable = 65141;
enum int KEY_Stop = 269025064;
enum int KEY_Subtitle = 269025178;
enum int KEY_Super_L = 65515;
enum int KEY_Super_R = 65516;
enum int KEY_Support = 269025150;
enum int KEY_Suspend = 269025191;
enum int KEY_Switch_VT_1 = 269024769;
enum int KEY_Switch_VT_10 = 269024778;
enum int KEY_Switch_VT_11 = 269024779;
enum int KEY_Switch_VT_12 = 269024780;
enum int KEY_Switch_VT_2 = 269024770;
enum int KEY_Switch_VT_3 = 269024771;
enum int KEY_Switch_VT_4 = 269024772;
enum int KEY_Switch_VT_5 = 269024773;
enum int KEY_Switch_VT_6 = 269024774;
enum int KEY_Switch_VT_7 = 269024775;
enum int KEY_Switch_VT_8 = 269024776;
enum int KEY_Switch_VT_9 = 269024777;
enum int KEY_Sys_Req = 65301;
enum int KEY_T = 84;
enum int KEY_THORN = 222;
enum int KEY_Tab = 65289;
enum int KEY_Tabovedot = 16785002;
enum int KEY_TaskPane = 269025151;
enum int KEY_Tcaron = 427;
enum int KEY_Tcedilla = 478;
enum int KEY_Terminal = 269025152;
enum int KEY_Terminate_Server = 65237;
enum int KEY_Thai_baht = 3551;
enum int KEY_Thai_bobaimai = 3514;
enum int KEY_Thai_chochan = 3496;
enum int KEY_Thai_chochang = 3498;
enum int KEY_Thai_choching = 3497;
enum int KEY_Thai_chochoe = 3500;
enum int KEY_Thai_dochada = 3502;
enum int KEY_Thai_dodek = 3508;
enum int KEY_Thai_fofa = 3517;
enum int KEY_Thai_fofan = 3519;
enum int KEY_Thai_hohip = 3531;
enum int KEY_Thai_honokhuk = 3534;
enum int KEY_Thai_khokhai = 3490;
enum int KEY_Thai_khokhon = 3493;
enum int KEY_Thai_khokhuat = 3491;
enum int KEY_Thai_khokhwai = 3492;
enum int KEY_Thai_khorakhang = 3494;
enum int KEY_Thai_kokai = 3489;
enum int KEY_Thai_lakkhangyao = 3557;
enum int KEY_Thai_lekchet = 3575;
enum int KEY_Thai_lekha = 3573;
enum int KEY_Thai_lekhok = 3574;
enum int KEY_Thai_lekkao = 3577;
enum int KEY_Thai_leknung = 3569;
enum int KEY_Thai_lekpaet = 3576;
enum int KEY_Thai_leksam = 3571;
enum int KEY_Thai_leksi = 3572;
enum int KEY_Thai_leksong = 3570;
enum int KEY_Thai_leksun = 3568;
enum int KEY_Thai_lochula = 3532;
enum int KEY_Thai_loling = 3525;
enum int KEY_Thai_lu = 3526;
enum int KEY_Thai_maichattawa = 3563;
enum int KEY_Thai_maiek = 3560;
enum int KEY_Thai_maihanakat = 3537;
enum int KEY_Thai_maihanakat_maitho = 3550;
enum int KEY_Thai_maitaikhu = 3559;
enum int KEY_Thai_maitho = 3561;
enum int KEY_Thai_maitri = 3562;
enum int KEY_Thai_maiyamok = 3558;
enum int KEY_Thai_moma = 3521;
enum int KEY_Thai_ngongu = 3495;
enum int KEY_Thai_nikhahit = 3565;
enum int KEY_Thai_nonen = 3507;
enum int KEY_Thai_nonu = 3513;
enum int KEY_Thai_oang = 3533;
enum int KEY_Thai_paiyannoi = 3535;
enum int KEY_Thai_phinthu = 3546;
enum int KEY_Thai_phophan = 3518;
enum int KEY_Thai_phophung = 3516;
enum int KEY_Thai_phosamphao = 3520;
enum int KEY_Thai_popla = 3515;
enum int KEY_Thai_rorua = 3523;
enum int KEY_Thai_ru = 3524;
enum int KEY_Thai_saraa = 3536;
enum int KEY_Thai_saraaa = 3538;
enum int KEY_Thai_saraae = 3553;
enum int KEY_Thai_saraaimaimalai = 3556;
enum int KEY_Thai_saraaimaimuan = 3555;
enum int KEY_Thai_saraam = 3539;
enum int KEY_Thai_sarae = 3552;
enum int KEY_Thai_sarai = 3540;
enum int KEY_Thai_saraii = 3541;
enum int KEY_Thai_sarao = 3554;
enum int KEY_Thai_sarau = 3544;
enum int KEY_Thai_saraue = 3542;
enum int KEY_Thai_sarauee = 3543;
enum int KEY_Thai_sarauu = 3545;
enum int KEY_Thai_sorusi = 3529;
enum int KEY_Thai_sosala = 3528;
enum int KEY_Thai_soso = 3499;
enum int KEY_Thai_sosua = 3530;
enum int KEY_Thai_thanthakhat = 3564;
enum int KEY_Thai_thonangmontho = 3505;
enum int KEY_Thai_thophuthao = 3506;
enum int KEY_Thai_thothahan = 3511;
enum int KEY_Thai_thothan = 3504;
enum int KEY_Thai_thothong = 3512;
enum int KEY_Thai_thothung = 3510;
enum int KEY_Thai_topatak = 3503;
enum int KEY_Thai_totao = 3509;
enum int KEY_Thai_wowaen = 3527;
enum int KEY_Thai_yoyak = 3522;
enum int KEY_Thai_yoying = 3501;
enum int KEY_Thorn = 222;
enum int KEY_Time = 269025183;
enum int KEY_ToDoList = 269025055;
enum int KEY_Tools = 269025153;
enum int KEY_TopMenu = 269025186;
enum int KEY_TouchpadToggle = 269025193;
enum int KEY_Touroku = 65323;
enum int KEY_Travel = 269025154;
enum int KEY_Tslash = 940;
enum int KEY_U = 85;
enum int KEY_UWB = 269025174;
enum int KEY_Uacute = 218;
enum int KEY_Ubelowdot = 16785124;
enum int KEY_Ubreve = 733;
enum int KEY_Ucircumflex = 219;
enum int KEY_Udiaeresis = 220;
enum int KEY_Udoubleacute = 475;
enum int KEY_Ugrave = 217;
enum int KEY_Uhook = 16785126;
enum int KEY_Uhorn = 16777647;
enum int KEY_Uhornacute = 16785128;
enum int KEY_Uhornbelowdot = 16785136;
enum int KEY_Uhorngrave = 16785130;
enum int KEY_Uhornhook = 16785132;
enum int KEY_Uhorntilde = 16785134;
enum int KEY_Ukrainian_GHE_WITH_UPTURN = 1725;
enum int KEY_Ukrainian_I = 1718;
enum int KEY_Ukrainian_IE = 1716;
enum int KEY_Ukrainian_YI = 1719;
enum int KEY_Ukrainian_ghe_with_upturn = 1709;
enum int KEY_Ukrainian_i = 1702;
enum int KEY_Ukrainian_ie = 1700;
enum int KEY_Ukrainian_yi = 1703;
enum int KEY_Ukranian_I = 1718;
enum int KEY_Ukranian_JE = 1716;
enum int KEY_Ukranian_YI = 1719;
enum int KEY_Ukranian_i = 1702;
enum int KEY_Ukranian_je = 1700;
enum int KEY_Ukranian_yi = 1703;
enum int KEY_Umacron = 990;
enum int KEY_Undo = 65381;
enum int KEY_Ungrab = 269024800;
enum int KEY_Uogonek = 985;
enum int KEY_Up = 65362;
enum int KEY_Uring = 473;
enum int KEY_User1KB = 269025157;
enum int KEY_User2KB = 269025158;
enum int KEY_UserPB = 269025156;
enum int KEY_Utilde = 989;
enum int KEY_V = 86;
enum int KEY_VendorHome = 269025076;
enum int KEY_Video = 269025159;
enum int KEY_View = 269025185;
enum int KEY_VoidSymbol = 16777215;
enum int KEY_W = 87;
enum int KEY_WLAN = 269025173;
enum int KEY_WWW = 269025070;
enum int KEY_Wacute = 16785026;
enum int KEY_WakeUp = 269025067;
enum int KEY_Wcircumflex = 16777588;
enum int KEY_Wdiaeresis = 16785028;
enum int KEY_WebCam = 269025167;
enum int KEY_Wgrave = 16785024;
enum int KEY_WheelButton = 269025160;
enum int KEY_WindowClear = 269025109;
enum int KEY_WonSign = 16785577;
enum int KEY_Word = 269025161;
enum int KEY_X = 88;
enum int KEY_Xabovedot = 16785034;
enum int KEY_Xfer = 269025162;
enum int KEY_Y = 89;
enum int KEY_Yacute = 221;
enum int KEY_Ybelowdot = 16785140;
enum int KEY_Ycircumflex = 16777590;
enum int KEY_Ydiaeresis = 5054;
enum int KEY_Yellow = 269025189;
enum int KEY_Ygrave = 16785138;
enum int KEY_Yhook = 16785142;
enum int KEY_Ytilde = 16785144;
enum int KEY_Z = 90;
enum int KEY_Zabovedot = 431;
enum int KEY_Zacute = 428;
enum int KEY_Zcaron = 430;
enum int KEY_Zen_Koho = 65341;
enum int KEY_Zenkaku = 65320;
enum int KEY_Zenkaku_Hankaku = 65322;
enum int KEY_ZoomIn = 269025163;
enum int KEY_ZoomOut = 269025164;
enum int KEY_Zstroke = 16777653;
enum int KEY_a = 97;
enum int KEY_aacute = 225;
enum int KEY_abelowdot = 16785057;
enum int KEY_abovedot = 511;
enum int KEY_abreve = 483;
enum int KEY_abreveacute = 16785071;
enum int KEY_abrevebelowdot = 16785079;
enum int KEY_abrevegrave = 16785073;
enum int KEY_abrevehook = 16785075;
enum int KEY_abrevetilde = 16785077;
enum int KEY_acircumflex = 226;
enum int KEY_acircumflexacute = 16785061;
enum int KEY_acircumflexbelowdot = 16785069;
enum int KEY_acircumflexgrave = 16785063;
enum int KEY_acircumflexhook = 16785065;
enum int KEY_acircumflextilde = 16785067;
enum int KEY_acute = 180;
enum int KEY_adiaeresis = 228;
enum int KEY_ae = 230;
enum int KEY_agrave = 224;
enum int KEY_ahook = 16785059;
enum int KEY_amacron = 992;
enum int KEY_ampersand = 38;
enum int KEY_aogonek = 433;
enum int KEY_apostrophe = 39;
enum int KEY_approxeq = 16785992;
enum int KEY_approximate = 2248;
enum int KEY_aring = 229;
enum int KEY_asciicircum = 94;
enum int KEY_asciitilde = 126;
enum int KEY_asterisk = 42;
enum int KEY_at = 64;
enum int KEY_atilde = 227;
enum int KEY_b = 98;
enum int KEY_babovedot = 16784899;
enum int KEY_backslash = 92;
enum int KEY_ballotcross = 2804;
enum int KEY_bar = 124;
enum int KEY_because = 16785973;
enum int KEY_blank = 2527;
enum int KEY_botintegral = 2213;
enum int KEY_botleftparens = 2220;
enum int KEY_botleftsqbracket = 2216;
enum int KEY_botleftsummation = 2226;
enum int KEY_botrightparens = 2222;
enum int KEY_botrightsqbracket = 2218;
enum int KEY_botrightsummation = 2230;
enum int KEY_bott = 2550;
enum int KEY_botvertsummationconnector = 2228;
enum int KEY_braceleft = 123;
enum int KEY_braceright = 125;
enum int KEY_bracketleft = 91;
enum int KEY_bracketright = 93;
enum int KEY_braille_blank = 16787456;
enum int KEY_braille_dot_1 = 65521;
enum int KEY_braille_dot_10 = 65530;
enum int KEY_braille_dot_2 = 65522;
enum int KEY_braille_dot_3 = 65523;
enum int KEY_braille_dot_4 = 65524;
enum int KEY_braille_dot_5 = 65525;
enum int KEY_braille_dot_6 = 65526;
enum int KEY_braille_dot_7 = 65527;
enum int KEY_braille_dot_8 = 65528;
enum int KEY_braille_dot_9 = 65529;
enum int KEY_braille_dots_1 = 16787457;
enum int KEY_braille_dots_12 = 16787459;
enum int KEY_braille_dots_123 = 16787463;
enum int KEY_braille_dots_1234 = 16787471;
enum int KEY_braille_dots_12345 = 16787487;
enum int KEY_braille_dots_123456 = 16787519;
enum int KEY_braille_dots_1234567 = 16787583;
enum int KEY_braille_dots_12345678 = 16787711;
enum int KEY_braille_dots_1234568 = 16787647;
enum int KEY_braille_dots_123457 = 16787551;
enum int KEY_braille_dots_1234578 = 16787679;
enum int KEY_braille_dots_123458 = 16787615;
enum int KEY_braille_dots_12346 = 16787503;
enum int KEY_braille_dots_123467 = 16787567;
enum int KEY_braille_dots_1234678 = 16787695;
enum int KEY_braille_dots_123468 = 16787631;
enum int KEY_braille_dots_12347 = 16787535;
enum int KEY_braille_dots_123478 = 16787663;
enum int KEY_braille_dots_12348 = 16787599;
enum int KEY_braille_dots_1235 = 16787479;
enum int KEY_braille_dots_12356 = 16787511;
enum int KEY_braille_dots_123567 = 16787575;
enum int KEY_braille_dots_1235678 = 16787703;
enum int KEY_braille_dots_123568 = 16787639;
enum int KEY_braille_dots_12357 = 16787543;
enum int KEY_braille_dots_123578 = 16787671;
enum int KEY_braille_dots_12358 = 16787607;
enum int KEY_braille_dots_1236 = 16787495;
enum int KEY_braille_dots_12367 = 16787559;
enum int KEY_braille_dots_123678 = 16787687;
enum int KEY_braille_dots_12368 = 16787623;
enum int KEY_braille_dots_1237 = 16787527;
enum int KEY_braille_dots_12378 = 16787655;
enum int KEY_braille_dots_1238 = 16787591;
enum int KEY_braille_dots_124 = 16787467;
enum int KEY_braille_dots_1245 = 16787483;
enum int KEY_braille_dots_12456 = 16787515;
enum int KEY_braille_dots_124567 = 16787579;
enum int KEY_braille_dots_1245678 = 16787707;
enum int KEY_braille_dots_124568 = 16787643;
enum int KEY_braille_dots_12457 = 16787547;
enum int KEY_braille_dots_124578 = 16787675;
enum int KEY_braille_dots_12458 = 16787611;
enum int KEY_braille_dots_1246 = 16787499;
enum int KEY_braille_dots_12467 = 16787563;
enum int KEY_braille_dots_124678 = 16787691;
enum int KEY_braille_dots_12468 = 16787627;
enum int KEY_braille_dots_1247 = 16787531;
enum int KEY_braille_dots_12478 = 16787659;
enum int KEY_braille_dots_1248 = 16787595;
enum int KEY_braille_dots_125 = 16787475;
enum int KEY_braille_dots_1256 = 16787507;
enum int KEY_braille_dots_12567 = 16787571;
enum int KEY_braille_dots_125678 = 16787699;
enum int KEY_braille_dots_12568 = 16787635;
enum int KEY_braille_dots_1257 = 16787539;
enum int KEY_braille_dots_12578 = 16787667;
enum int KEY_braille_dots_1258 = 16787603;
enum int KEY_braille_dots_126 = 16787491;
enum int KEY_braille_dots_1267 = 16787555;
enum int KEY_braille_dots_12678 = 16787683;
enum int KEY_braille_dots_1268 = 16787619;
enum int KEY_braille_dots_127 = 16787523;
enum int KEY_braille_dots_1278 = 16787651;
enum int KEY_braille_dots_128 = 16787587;
enum int KEY_braille_dots_13 = 16787461;
enum int KEY_braille_dots_134 = 16787469;
enum int KEY_braille_dots_1345 = 16787485;
enum int KEY_braille_dots_13456 = 16787517;
enum int KEY_braille_dots_134567 = 16787581;
enum int KEY_braille_dots_1345678 = 16787709;
enum int KEY_braille_dots_134568 = 16787645;
enum int KEY_braille_dots_13457 = 16787549;
enum int KEY_braille_dots_134578 = 16787677;
enum int KEY_braille_dots_13458 = 16787613;
enum int KEY_braille_dots_1346 = 16787501;
enum int KEY_braille_dots_13467 = 16787565;
enum int KEY_braille_dots_134678 = 16787693;
enum int KEY_braille_dots_13468 = 16787629;
enum int KEY_braille_dots_1347 = 16787533;
enum int KEY_braille_dots_13478 = 16787661;
enum int KEY_braille_dots_1348 = 16787597;
enum int KEY_braille_dots_135 = 16787477;
enum int KEY_braille_dots_1356 = 16787509;
enum int KEY_braille_dots_13567 = 16787573;
enum int KEY_braille_dots_135678 = 16787701;
enum int KEY_braille_dots_13568 = 16787637;
enum int KEY_braille_dots_1357 = 16787541;
enum int KEY_braille_dots_13578 = 16787669;
enum int KEY_braille_dots_1358 = 16787605;
enum int KEY_braille_dots_136 = 16787493;
enum int KEY_braille_dots_1367 = 16787557;
enum int KEY_braille_dots_13678 = 16787685;
enum int KEY_braille_dots_1368 = 16787621;
enum int KEY_braille_dots_137 = 16787525;
enum int KEY_braille_dots_1378 = 16787653;
enum int KEY_braille_dots_138 = 16787589;
enum int KEY_braille_dots_14 = 16787465;
enum int KEY_braille_dots_145 = 16787481;
enum int KEY_braille_dots_1456 = 16787513;
enum int KEY_braille_dots_14567 = 16787577;
enum int KEY_braille_dots_145678 = 16787705;
enum int KEY_braille_dots_14568 = 16787641;
enum int KEY_braille_dots_1457 = 16787545;
enum int KEY_braille_dots_14578 = 16787673;
enum int KEY_braille_dots_1458 = 16787609;
enum int KEY_braille_dots_146 = 16787497;
enum int KEY_braille_dots_1467 = 16787561;
enum int KEY_braille_dots_14678 = 16787689;
enum int KEY_braille_dots_1468 = 16787625;
enum int KEY_braille_dots_147 = 16787529;
enum int KEY_braille_dots_1478 = 16787657;
enum int KEY_braille_dots_148 = 16787593;
enum int KEY_braille_dots_15 = 16787473;
enum int KEY_braille_dots_156 = 16787505;
enum int KEY_braille_dots_1567 = 16787569;
enum int KEY_braille_dots_15678 = 16787697;
enum int KEY_braille_dots_1568 = 16787633;
enum int KEY_braille_dots_157 = 16787537;
enum int KEY_braille_dots_1578 = 16787665;
enum int KEY_braille_dots_158 = 16787601;
enum int KEY_braille_dots_16 = 16787489;
enum int KEY_braille_dots_167 = 16787553;
enum int KEY_braille_dots_1678 = 16787681;
enum int KEY_braille_dots_168 = 16787617;
enum int KEY_braille_dots_17 = 16787521;
enum int KEY_braille_dots_178 = 16787649;
enum int KEY_braille_dots_18 = 16787585;
enum int KEY_braille_dots_2 = 16787458;
enum int KEY_braille_dots_23 = 16787462;
enum int KEY_braille_dots_234 = 16787470;
enum int KEY_braille_dots_2345 = 16787486;
enum int KEY_braille_dots_23456 = 16787518;
enum int KEY_braille_dots_234567 = 16787582;
enum int KEY_braille_dots_2345678 = 16787710;
enum int KEY_braille_dots_234568 = 16787646;
enum int KEY_braille_dots_23457 = 16787550;
enum int KEY_braille_dots_234578 = 16787678;
enum int KEY_braille_dots_23458 = 16787614;
enum int KEY_braille_dots_2346 = 16787502;
enum int KEY_braille_dots_23467 = 16787566;
enum int KEY_braille_dots_234678 = 16787694;
enum int KEY_braille_dots_23468 = 16787630;
enum int KEY_braille_dots_2347 = 16787534;
enum int KEY_braille_dots_23478 = 16787662;
enum int KEY_braille_dots_2348 = 16787598;
enum int KEY_braille_dots_235 = 16787478;
enum int KEY_braille_dots_2356 = 16787510;
enum int KEY_braille_dots_23567 = 16787574;
enum int KEY_braille_dots_235678 = 16787702;
enum int KEY_braille_dots_23568 = 16787638;
enum int KEY_braille_dots_2357 = 16787542;
enum int KEY_braille_dots_23578 = 16787670;
enum int KEY_braille_dots_2358 = 16787606;
enum int KEY_braille_dots_236 = 16787494;
enum int KEY_braille_dots_2367 = 16787558;
enum int KEY_braille_dots_23678 = 16787686;
enum int KEY_braille_dots_2368 = 16787622;
enum int KEY_braille_dots_237 = 16787526;
enum int KEY_braille_dots_2378 = 16787654;
enum int KEY_braille_dots_238 = 16787590;
enum int KEY_braille_dots_24 = 16787466;
enum int KEY_braille_dots_245 = 16787482;
enum int KEY_braille_dots_2456 = 16787514;
enum int KEY_braille_dots_24567 = 16787578;
enum int KEY_braille_dots_245678 = 16787706;
enum int KEY_braille_dots_24568 = 16787642;
enum int KEY_braille_dots_2457 = 16787546;
enum int KEY_braille_dots_24578 = 16787674;
enum int KEY_braille_dots_2458 = 16787610;
enum int KEY_braille_dots_246 = 16787498;
enum int KEY_braille_dots_2467 = 16787562;
enum int KEY_braille_dots_24678 = 16787690;
enum int KEY_braille_dots_2468 = 16787626;
enum int KEY_braille_dots_247 = 16787530;
enum int KEY_braille_dots_2478 = 16787658;
enum int KEY_braille_dots_248 = 16787594;
enum int KEY_braille_dots_25 = 16787474;
enum int KEY_braille_dots_256 = 16787506;
enum int KEY_braille_dots_2567 = 16787570;
enum int KEY_braille_dots_25678 = 16787698;
enum int KEY_braille_dots_2568 = 16787634;
enum int KEY_braille_dots_257 = 16787538;
enum int KEY_braille_dots_2578 = 16787666;
enum int KEY_braille_dots_258 = 16787602;
enum int KEY_braille_dots_26 = 16787490;
enum int KEY_braille_dots_267 = 16787554;
enum int KEY_braille_dots_2678 = 16787682;
enum int KEY_braille_dots_268 = 16787618;
enum int KEY_braille_dots_27 = 16787522;
enum int KEY_braille_dots_278 = 16787650;
enum int KEY_braille_dots_28 = 16787586;
enum int KEY_braille_dots_3 = 16787460;
enum int KEY_braille_dots_34 = 16787468;
enum int KEY_braille_dots_345 = 16787484;
enum int KEY_braille_dots_3456 = 16787516;
enum int KEY_braille_dots_34567 = 16787580;
enum int KEY_braille_dots_345678 = 16787708;
enum int KEY_braille_dots_34568 = 16787644;
enum int KEY_braille_dots_3457 = 16787548;
enum int KEY_braille_dots_34578 = 16787676;
enum int KEY_braille_dots_3458 = 16787612;
enum int KEY_braille_dots_346 = 16787500;
enum int KEY_braille_dots_3467 = 16787564;
enum int KEY_braille_dots_34678 = 16787692;
enum int KEY_braille_dots_3468 = 16787628;
enum int KEY_braille_dots_347 = 16787532;
enum int KEY_braille_dots_3478 = 16787660;
enum int KEY_braille_dots_348 = 16787596;
enum int KEY_braille_dots_35 = 16787476;
enum int KEY_braille_dots_356 = 16787508;
enum int KEY_braille_dots_3567 = 16787572;
enum int KEY_braille_dots_35678 = 16787700;
enum int KEY_braille_dots_3568 = 16787636;
enum int KEY_braille_dots_357 = 16787540;
enum int KEY_braille_dots_3578 = 16787668;
enum int KEY_braille_dots_358 = 16787604;
enum int KEY_braille_dots_36 = 16787492;
enum int KEY_braille_dots_367 = 16787556;
enum int KEY_braille_dots_3678 = 16787684;
enum int KEY_braille_dots_368 = 16787620;
enum int KEY_braille_dots_37 = 16787524;
enum int KEY_braille_dots_378 = 16787652;
enum int KEY_braille_dots_38 = 16787588;
enum int KEY_braille_dots_4 = 16787464;
enum int KEY_braille_dots_45 = 16787480;
enum int KEY_braille_dots_456 = 16787512;
enum int KEY_braille_dots_4567 = 16787576;
enum int KEY_braille_dots_45678 = 16787704;
enum int KEY_braille_dots_4568 = 16787640;
enum int KEY_braille_dots_457 = 16787544;
enum int KEY_braille_dots_4578 = 16787672;
enum int KEY_braille_dots_458 = 16787608;
enum int KEY_braille_dots_46 = 16787496;
enum int KEY_braille_dots_467 = 16787560;
enum int KEY_braille_dots_4678 = 16787688;
enum int KEY_braille_dots_468 = 16787624;
enum int KEY_braille_dots_47 = 16787528;
enum int KEY_braille_dots_478 = 16787656;
enum int KEY_braille_dots_48 = 16787592;
enum int KEY_braille_dots_5 = 16787472;
enum int KEY_braille_dots_56 = 16787504;
enum int KEY_braille_dots_567 = 16787568;
enum int KEY_braille_dots_5678 = 16787696;
enum int KEY_braille_dots_568 = 16787632;
enum int KEY_braille_dots_57 = 16787536;
enum int KEY_braille_dots_578 = 16787664;
enum int KEY_braille_dots_58 = 16787600;
enum int KEY_braille_dots_6 = 16787488;
enum int KEY_braille_dots_67 = 16787552;
enum int KEY_braille_dots_678 = 16787680;
enum int KEY_braille_dots_68 = 16787616;
enum int KEY_braille_dots_7 = 16787520;
enum int KEY_braille_dots_78 = 16787648;
enum int KEY_braille_dots_8 = 16787584;
enum int KEY_breve = 418;
enum int KEY_brokenbar = 166;
enum int KEY_c = 99;
enum int KEY_cabovedot = 741;
enum int KEY_cacute = 486;
enum int KEY_careof = 2744;
enum int KEY_caret = 2812;
enum int KEY_caron = 439;
enum int KEY_ccaron = 488;
enum int KEY_ccedilla = 231;
enum int KEY_ccircumflex = 742;
enum int KEY_cedilla = 184;
enum int KEY_cent = 162;
enum int KEY_checkerboard = 2529;
enum int KEY_checkmark = 2803;
enum int KEY_circle = 3023;
enum int KEY_club = 2796;
enum int KEY_colon = 58;
enum int KEY_comma = 44;
enum int KEY_containsas = 16785931;
enum int KEY_copyright = 169;
enum int KEY_cr = 2532;
enum int KEY_crossinglines = 2542;
enum int KEY_cuberoot = 16785947;
enum int KEY_currency = 164;
enum int KEY_cursor = 2815;
enum int KEY_d = 100;
enum int KEY_dabovedot = 16784907;
enum int KEY_dagger = 2801;
enum int KEY_dcaron = 495;
enum int KEY_dead_A = 65153;
enum int KEY_dead_E = 65155;
enum int KEY_dead_I = 65157;
enum int KEY_dead_O = 65159;
enum int KEY_dead_U = 65161;
enum int KEY_dead_a = 65152;
enum int KEY_dead_abovecomma = 65124;
enum int KEY_dead_abovedot = 65110;
enum int KEY_dead_abovereversedcomma = 65125;
enum int KEY_dead_abovering = 65112;
enum int KEY_dead_acute = 65105;
enum int KEY_dead_belowbreve = 65131;
enum int KEY_dead_belowcircumflex = 65129;
enum int KEY_dead_belowcomma = 65134;
enum int KEY_dead_belowdiaeresis = 65132;
enum int KEY_dead_belowdot = 65120;
enum int KEY_dead_belowmacron = 65128;
enum int KEY_dead_belowring = 65127;
enum int KEY_dead_belowtilde = 65130;
enum int KEY_dead_breve = 65109;
enum int KEY_dead_capital_schwa = 65163;
enum int KEY_dead_caron = 65114;
enum int KEY_dead_cedilla = 65115;
enum int KEY_dead_circumflex = 65106;
enum int KEY_dead_currency = 65135;
enum int KEY_dead_dasia = 65125;
enum int KEY_dead_diaeresis = 65111;
enum int KEY_dead_doubleacute = 65113;
enum int KEY_dead_doublegrave = 65126;
enum int KEY_dead_e = 65154;
enum int KEY_dead_grave = 65104;
enum int KEY_dead_hook = 65121;
enum int KEY_dead_horn = 65122;
enum int KEY_dead_i = 65156;
enum int KEY_dead_invertedbreve = 65133;
enum int KEY_dead_iota = 65117;
enum int KEY_dead_macron = 65108;
enum int KEY_dead_o = 65158;
enum int KEY_dead_ogonek = 65116;
enum int KEY_dead_perispomeni = 65107;
enum int KEY_dead_psili = 65124;
enum int KEY_dead_semivoiced_sound = 65119;
enum int KEY_dead_small_schwa = 65162;
enum int KEY_dead_stroke = 65123;
enum int KEY_dead_tilde = 65107;
enum int KEY_dead_u = 65160;
enum int KEY_dead_voiced_sound = 65118;
enum int KEY_decimalpoint = 2749;
enum int KEY_degree = 176;
enum int KEY_diaeresis = 168;
enum int KEY_diamond = 2797;
enum int KEY_digitspace = 2725;
enum int KEY_dintegral = 16785964;
enum int KEY_division = 247;
enum int KEY_dollar = 36;
enum int KEY_doubbaselinedot = 2735;
enum int KEY_doubleacute = 445;
enum int KEY_doubledagger = 2802;
enum int KEY_doublelowquotemark = 2814;
enum int KEY_downarrow = 2302;
enum int KEY_downcaret = 2984;
enum int KEY_downshoe = 3030;
enum int KEY_downstile = 3012;
enum int KEY_downtack = 3010;
enum int KEY_dstroke = 496;
enum int KEY_e = 101;
enum int KEY_eabovedot = 1004;
enum int KEY_eacute = 233;
enum int KEY_ebelowdot = 16785081;
enum int KEY_ecaron = 492;
enum int KEY_ecircumflex = 234;
enum int KEY_ecircumflexacute = 16785087;
enum int KEY_ecircumflexbelowdot = 16785095;
enum int KEY_ecircumflexgrave = 16785089;
enum int KEY_ecircumflexhook = 16785091;
enum int KEY_ecircumflextilde = 16785093;
enum int KEY_ediaeresis = 235;
enum int KEY_egrave = 232;
enum int KEY_ehook = 16785083;
enum int KEY_eightsubscript = 16785544;
enum int KEY_eightsuperior = 16785528;
enum int KEY_elementof = 16785928;
enum int KEY_ellipsis = 2734;
enum int KEY_em3space = 2723;
enum int KEY_em4space = 2724;
enum int KEY_emacron = 954;
enum int KEY_emdash = 2729;
enum int KEY_emfilledcircle = 2782;
enum int KEY_emfilledrect = 2783;
enum int KEY_emopencircle = 2766;
enum int KEY_emopenrectangle = 2767;
enum int KEY_emptyset = 16785925;
enum int KEY_emspace = 2721;
enum int KEY_endash = 2730;
enum int KEY_enfilledcircbullet = 2790;
enum int KEY_enfilledsqbullet = 2791;
enum int KEY_eng = 959;
enum int KEY_enopencircbullet = 2784;
enum int KEY_enopensquarebullet = 2785;
enum int KEY_enspace = 2722;
enum int KEY_eogonek = 490;
enum int KEY_equal = 61;
enum int KEY_eth = 240;
enum int KEY_etilde = 16785085;
enum int KEY_exclam = 33;
enum int KEY_exclamdown = 161;
enum int KEY_f = 102;
enum int KEY_fabovedot = 16784927;
enum int KEY_femalesymbol = 2808;
enum int KEY_ff = 2531;
enum int KEY_figdash = 2747;
enum int KEY_filledlefttribullet = 2780;
enum int KEY_filledrectbullet = 2779;
enum int KEY_filledrighttribullet = 2781;
enum int KEY_filledtribulletdown = 2793;
enum int KEY_filledtribulletup = 2792;
enum int KEY_fiveeighths = 2757;
enum int KEY_fivesixths = 2743;
enum int KEY_fivesubscript = 16785541;
enum int KEY_fivesuperior = 16785525;
enum int KEY_fourfifths = 2741;
enum int KEY_foursubscript = 16785540;
enum int KEY_foursuperior = 16785524;
enum int KEY_fourthroot = 16785948;
enum int KEY_function = 2294;
enum int KEY_g = 103;
enum int KEY_gabovedot = 757;
enum int KEY_gbreve = 699;
enum int KEY_gcaron = 16777703;
enum int KEY_gcedilla = 955;
enum int KEY_gcircumflex = 760;
enum int KEY_grave = 96;
enum int KEY_greater = 62;
enum int KEY_greaterthanequal = 2238;
enum int KEY_guillemotleft = 171;
enum int KEY_guillemotright = 187;
enum int KEY_h = 104;
enum int KEY_hairspace = 2728;
enum int KEY_hcircumflex = 694;
enum int KEY_heart = 2798;
enum int KEY_hebrew_aleph = 3296;
enum int KEY_hebrew_ayin = 3314;
enum int KEY_hebrew_bet = 3297;
enum int KEY_hebrew_beth = 3297;
enum int KEY_hebrew_chet = 3303;
enum int KEY_hebrew_dalet = 3299;
enum int KEY_hebrew_daleth = 3299;
enum int KEY_hebrew_doublelowline = 3295;
enum int KEY_hebrew_finalkaph = 3306;
enum int KEY_hebrew_finalmem = 3309;
enum int KEY_hebrew_finalnun = 3311;
enum int KEY_hebrew_finalpe = 3315;
enum int KEY_hebrew_finalzade = 3317;
enum int KEY_hebrew_finalzadi = 3317;
enum int KEY_hebrew_gimel = 3298;
enum int KEY_hebrew_gimmel = 3298;
enum int KEY_hebrew_he = 3300;
enum int KEY_hebrew_het = 3303;
enum int KEY_hebrew_kaph = 3307;
enum int KEY_hebrew_kuf = 3319;
enum int KEY_hebrew_lamed = 3308;
enum int KEY_hebrew_mem = 3310;
enum int KEY_hebrew_nun = 3312;
enum int KEY_hebrew_pe = 3316;
enum int KEY_hebrew_qoph = 3319;
enum int KEY_hebrew_resh = 3320;
enum int KEY_hebrew_samech = 3313;
enum int KEY_hebrew_samekh = 3313;
enum int KEY_hebrew_shin = 3321;
enum int KEY_hebrew_taf = 3322;
enum int KEY_hebrew_taw = 3322;
enum int KEY_hebrew_tet = 3304;
enum int KEY_hebrew_teth = 3304;
enum int KEY_hebrew_waw = 3301;
enum int KEY_hebrew_yod = 3305;
enum int KEY_hebrew_zade = 3318;
enum int KEY_hebrew_zadi = 3318;
enum int KEY_hebrew_zain = 3302;
enum int KEY_hebrew_zayin = 3302;
enum int KEY_hexagram = 2778;
enum int KEY_horizconnector = 2211;
enum int KEY_horizlinescan1 = 2543;
enum int KEY_horizlinescan3 = 2544;
enum int KEY_horizlinescan5 = 2545;
enum int KEY_horizlinescan7 = 2546;
enum int KEY_horizlinescan9 = 2547;
enum int KEY_hstroke = 689;
enum int KEY_ht = 2530;
enum int KEY_hyphen = 173;
enum int KEY_i = 105;
enum int KEY_iTouch = 269025120;
enum int KEY_iacute = 237;
enum int KEY_ibelowdot = 16785099;
enum int KEY_ibreve = 16777517;
enum int KEY_icircumflex = 238;
enum int KEY_identical = 2255;
enum int KEY_idiaeresis = 239;
enum int KEY_idotless = 697;
enum int KEY_ifonlyif = 2253;
enum int KEY_igrave = 236;
enum int KEY_ihook = 16785097;
enum int KEY_imacron = 1007;
enum int KEY_implies = 2254;
enum int KEY_includedin = 2266;
enum int KEY_includes = 2267;
enum int KEY_infinity = 2242;
enum int KEY_integral = 2239;
enum int KEY_intersection = 2268;
enum int KEY_iogonek = 999;
enum int KEY_itilde = 949;
enum int KEY_j = 106;
enum int KEY_jcircumflex = 700;
enum int KEY_jot = 3018;
enum int KEY_k = 107;
enum int KEY_kana_A = 1201;
enum int KEY_kana_CHI = 1217;
enum int KEY_kana_E = 1204;
enum int KEY_kana_FU = 1228;
enum int KEY_kana_HA = 1226;
enum int KEY_kana_HE = 1229;
enum int KEY_kana_HI = 1227;
enum int KEY_kana_HO = 1230;
enum int KEY_kana_HU = 1228;
enum int KEY_kana_I = 1202;
enum int KEY_kana_KA = 1206;
enum int KEY_kana_KE = 1209;
enum int KEY_kana_KI = 1207;
enum int KEY_kana_KO = 1210;
enum int KEY_kana_KU = 1208;
enum int KEY_kana_MA = 1231;
enum int KEY_kana_ME = 1234;
enum int KEY_kana_MI = 1232;
enum int KEY_kana_MO = 1235;
enum int KEY_kana_MU = 1233;
enum int KEY_kana_N = 1245;
enum int KEY_kana_NA = 1221;
enum int KEY_kana_NE = 1224;
enum int KEY_kana_NI = 1222;
enum int KEY_kana_NO = 1225;
enum int KEY_kana_NU = 1223;
enum int KEY_kana_O = 1205;
enum int KEY_kana_RA = 1239;
enum int KEY_kana_RE = 1242;
enum int KEY_kana_RI = 1240;
enum int KEY_kana_RO = 1243;
enum int KEY_kana_RU = 1241;
enum int KEY_kana_SA = 1211;
enum int KEY_kana_SE = 1214;
enum int KEY_kana_SHI = 1212;
enum int KEY_kana_SO = 1215;
enum int KEY_kana_SU = 1213;
enum int KEY_kana_TA = 1216;
enum int KEY_kana_TE = 1219;
enum int KEY_kana_TI = 1217;
enum int KEY_kana_TO = 1220;
enum int KEY_kana_TSU = 1218;
enum int KEY_kana_TU = 1218;
enum int KEY_kana_U = 1203;
enum int KEY_kana_WA = 1244;
enum int KEY_kana_WO = 1190;
enum int KEY_kana_YA = 1236;
enum int KEY_kana_YO = 1238;
enum int KEY_kana_YU = 1237;
enum int KEY_kana_a = 1191;
enum int KEY_kana_closingbracket = 1187;
enum int KEY_kana_comma = 1188;
enum int KEY_kana_conjunctive = 1189;
enum int KEY_kana_e = 1194;
enum int KEY_kana_fullstop = 1185;
enum int KEY_kana_i = 1192;
enum int KEY_kana_middledot = 1189;
enum int KEY_kana_o = 1195;
enum int KEY_kana_openingbracket = 1186;
enum int KEY_kana_switch = 65406;
enum int KEY_kana_tsu = 1199;
enum int KEY_kana_tu = 1199;
enum int KEY_kana_u = 1193;
enum int KEY_kana_ya = 1196;
enum int KEY_kana_yo = 1198;
enum int KEY_kana_yu = 1197;
enum int KEY_kappa = 930;
enum int KEY_kcedilla = 1011;
enum int KEY_kra = 930;
enum int KEY_l = 108;
enum int KEY_lacute = 485;
enum int KEY_latincross = 2777;
enum int KEY_lbelowdot = 16784951;
enum int KEY_lcaron = 437;
enum int KEY_lcedilla = 950;
enum int KEY_leftanglebracket = 2748;
enum int KEY_leftarrow = 2299;
enum int KEY_leftcaret = 2979;
enum int KEY_leftdoublequotemark = 2770;
enum int KEY_leftmiddlecurlybrace = 2223;
enum int KEY_leftopentriangle = 2764;
enum int KEY_leftpointer = 2794;
enum int KEY_leftradical = 2209;
enum int KEY_leftshoe = 3034;
enum int KEY_leftsinglequotemark = 2768;
enum int KEY_leftt = 2548;
enum int KEY_lefttack = 3036;
enum int KEY_less = 60;
enum int KEY_lessthanequal = 2236;
enum int KEY_lf = 2533;
enum int KEY_logicaland = 2270;
enum int KEY_logicalor = 2271;
enum int KEY_lowleftcorner = 2541;
enum int KEY_lowrightcorner = 2538;
enum int KEY_lstroke = 435;
enum int KEY_m = 109;
enum int KEY_mabovedot = 16784961;
enum int KEY_macron = 175;
enum int KEY_malesymbol = 2807;
enum int KEY_maltesecross = 2800;
enum int KEY_marker = 2751;
enum int KEY_masculine = 186;
enum int KEY_minus = 45;
enum int KEY_minutes = 2774;
enum int KEY_mu = 181;
enum int KEY_multiply = 215;
enum int KEY_musicalflat = 2806;
enum int KEY_musicalsharp = 2805;
enum int KEY_n = 110;
enum int KEY_nabla = 2245;
enum int KEY_nacute = 497;
enum int KEY_ncaron = 498;
enum int KEY_ncedilla = 1009;
enum int KEY_ninesubscript = 16785545;
enum int KEY_ninesuperior = 16785529;
enum int KEY_nl = 2536;
enum int KEY_nobreakspace = 160;
enum int KEY_notapproxeq = 16785991;
enum int KEY_notelementof = 16785929;
enum int KEY_notequal = 2237;
enum int KEY_notidentical = 16786018;
enum int KEY_notsign = 172;
enum int KEY_ntilde = 241;
enum int KEY_numbersign = 35;
enum int KEY_numerosign = 1712;
enum int KEY_o = 111;
enum int KEY_oacute = 243;
enum int KEY_obarred = 16777845;
enum int KEY_obelowdot = 16785101;
enum int KEY_ocaron = 16777682;
enum int KEY_ocircumflex = 244;
enum int KEY_ocircumflexacute = 16785105;
enum int KEY_ocircumflexbelowdot = 16785113;
enum int KEY_ocircumflexgrave = 16785107;
enum int KEY_ocircumflexhook = 16785109;
enum int KEY_ocircumflextilde = 16785111;
enum int KEY_odiaeresis = 246;
enum int KEY_odoubleacute = 501;
enum int KEY_oe = 5053;
enum int KEY_ogonek = 434;
enum int KEY_ograve = 242;
enum int KEY_ohook = 16785103;
enum int KEY_ohorn = 16777633;
enum int KEY_ohornacute = 16785115;
enum int KEY_ohornbelowdot = 16785123;
enum int KEY_ohorngrave = 16785117;
enum int KEY_ohornhook = 16785119;
enum int KEY_ohorntilde = 16785121;
enum int KEY_omacron = 1010;
enum int KEY_oneeighth = 2755;
enum int KEY_onefifth = 2738;
enum int KEY_onehalf = 189;
enum int KEY_onequarter = 188;
enum int KEY_onesixth = 2742;
enum int KEY_onesubscript = 16785537;
enum int KEY_onesuperior = 185;
enum int KEY_onethird = 2736;
enum int KEY_ooblique = 248;
enum int KEY_openrectbullet = 2786;
enum int KEY_openstar = 2789;
enum int KEY_opentribulletdown = 2788;
enum int KEY_opentribulletup = 2787;
enum int KEY_ordfeminine = 170;
enum int KEY_oslash = 248;
enum int KEY_otilde = 245;
enum int KEY_overbar = 3008;
enum int KEY_overline = 1150;
enum int KEY_p = 112;
enum int KEY_pabovedot = 16784983;
enum int KEY_paragraph = 182;
enum int KEY_parenleft = 40;
enum int KEY_parenright = 41;
enum int KEY_partdifferential = 16785922;
enum int KEY_partialderivative = 2287;
enum int KEY_percent = 37;
enum int KEY_period = 46;
enum int KEY_periodcentered = 183;
enum int KEY_phonographcopyright = 2811;
enum int KEY_plus = 43;
enum int KEY_plusminus = 177;
enum int KEY_prescription = 2772;
enum int KEY_prolongedsound = 1200;
enum int KEY_punctspace = 2726;
enum int KEY_q = 113;
enum int KEY_quad = 3020;
enum int KEY_question = 63;
enum int KEY_questiondown = 191;
enum int KEY_quotedbl = 34;
enum int KEY_quoteleft = 96;
enum int KEY_quoteright = 39;
enum int KEY_r = 114;
enum int KEY_racute = 480;
enum int KEY_radical = 2262;
enum int KEY_rcaron = 504;
enum int KEY_rcedilla = 947;
enum int KEY_registered = 174;
enum int KEY_rightanglebracket = 2750;
enum int KEY_rightarrow = 2301;
enum int KEY_rightcaret = 2982;
enum int KEY_rightdoublequotemark = 2771;
enum int KEY_rightmiddlecurlybrace = 2224;
enum int KEY_rightmiddlesummation = 2231;
enum int KEY_rightopentriangle = 2765;
enum int KEY_rightpointer = 2795;
enum int KEY_rightshoe = 3032;
enum int KEY_rightsinglequotemark = 2769;
enum int KEY_rightt = 2549;
enum int KEY_righttack = 3068;
enum int KEY_s = 115;
enum int KEY_sabovedot = 16784993;
enum int KEY_sacute = 438;
enum int KEY_scaron = 441;
enum int KEY_scedilla = 442;
enum int KEY_schwa = 16777817;
enum int KEY_scircumflex = 766;
enum int KEY_script_switch = 65406;
enum int KEY_seconds = 2775;
enum int KEY_section = 167;
enum int KEY_semicolon = 59;
enum int KEY_semivoicedsound = 1247;
enum int KEY_seveneighths = 2758;
enum int KEY_sevensubscript = 16785543;
enum int KEY_sevensuperior = 16785527;
enum int KEY_signaturemark = 2762;
enum int KEY_signifblank = 2732;
enum int KEY_similarequal = 2249;
enum int KEY_singlelowquotemark = 2813;
enum int KEY_sixsubscript = 16785542;
enum int KEY_sixsuperior = 16785526;
enum int KEY_slash = 47;
enum int KEY_soliddiamond = 2528;
enum int KEY_space = 32;
enum int KEY_squareroot = 16785946;
enum int KEY_ssharp = 223;
enum int KEY_sterling = 163;
enum int KEY_stricteq = 16786019;
enum int KEY_t = 116;
enum int KEY_tabovedot = 16785003;
enum int KEY_tcaron = 443;
enum int KEY_tcedilla = 510;
enum int KEY_telephone = 2809;
enum int KEY_telephonerecorder = 2810;
enum int KEY_therefore = 2240;
enum int KEY_thinspace = 2727;
enum int KEY_thorn = 254;
enum int KEY_threeeighths = 2756;
enum int KEY_threefifths = 2740;
enum int KEY_threequarters = 190;
enum int KEY_threesubscript = 16785539;
enum int KEY_threesuperior = 179;
enum int KEY_tintegral = 16785965;
enum int KEY_topintegral = 2212;
enum int KEY_topleftparens = 2219;
enum int KEY_topleftradical = 2210;
enum int KEY_topleftsqbracket = 2215;
enum int KEY_topleftsummation = 2225;
enum int KEY_toprightparens = 2221;
enum int KEY_toprightsqbracket = 2217;
enum int KEY_toprightsummation = 2229;
enum int KEY_topt = 2551;
enum int KEY_topvertsummationconnector = 2227;
enum int KEY_trademark = 2761;
enum int KEY_trademarkincircle = 2763;
enum int KEY_tslash = 956;
enum int KEY_twofifths = 2739;
enum int KEY_twosubscript = 16785538;
enum int KEY_twosuperior = 178;
enum int KEY_twothirds = 2737;
enum int KEY_u = 117;
enum int KEY_uacute = 250;
enum int KEY_ubelowdot = 16785125;
enum int KEY_ubreve = 765;
enum int KEY_ucircumflex = 251;
enum int KEY_udiaeresis = 252;
enum int KEY_udoubleacute = 507;
enum int KEY_ugrave = 249;
enum int KEY_uhook = 16785127;
enum int KEY_uhorn = 16777648;
enum int KEY_uhornacute = 16785129;
enum int KEY_uhornbelowdot = 16785137;
enum int KEY_uhorngrave = 16785131;
enum int KEY_uhornhook = 16785133;
enum int KEY_uhorntilde = 16785135;
enum int KEY_umacron = 1022;
enum int KEY_underbar = 3014;
enum int KEY_underscore = 95;
enum int KEY_union = 2269;
enum int KEY_uogonek = 1017;
enum int KEY_uparrow = 2300;
enum int KEY_upcaret = 2985;
enum int KEY_upleftcorner = 2540;
enum int KEY_uprightcorner = 2539;
enum int KEY_upshoe = 3011;
enum int KEY_upstile = 3027;
enum int KEY_uptack = 3022;
enum int KEY_uring = 505;
enum int KEY_utilde = 1021;
enum int KEY_v = 118;
enum int KEY_variation = 2241;
enum int KEY_vertbar = 2552;
enum int KEY_vertconnector = 2214;
enum int KEY_voicedsound = 1246;
enum int KEY_vt = 2537;
enum int KEY_w = 119;
enum int KEY_wacute = 16785027;
enum int KEY_wcircumflex = 16777589;
enum int KEY_wdiaeresis = 16785029;
enum int KEY_wgrave = 16785025;
enum int KEY_x = 120;
enum int KEY_xabovedot = 16785035;
enum int KEY_y = 121;
enum int KEY_yacute = 253;
enum int KEY_ybelowdot = 16785141;
enum int KEY_ycircumflex = 16777591;
enum int KEY_ydiaeresis = 255;
enum int KEY_yen = 165;
enum int KEY_ygrave = 16785139;
enum int KEY_yhook = 16785143;
enum int KEY_ytilde = 16785145;
enum int KEY_z = 122;
enum int KEY_zabovedot = 447;
enum int KEY_zacute = 444;
enum int KEY_zcaron = 446;
enum int KEY_zerosubscript = 16785536;
enum int KEY_zerosuperior = 16785520;
enum int KEY_zstroke = 16777654;
struct KeyboardGrabInfo {
   Window* window, native_window;
   c_ulong serial;
   int owner_events;
   uint time;


   // Determines information about the current keyboard grab.
   // This is not public API and must not be used by applications.
   // 
   // keyboard grabbed.
   // RETURNS: %TRUE if this application currently has the
   // <display>: the display for which to get the grab information
   // <grab_window>: location to store current grab window
   // <owner_events>: location to store boolean indicating whether the @owner_events flag to gdk_keyboard_grab() was %TRUE.
   static int libgtk_only(AT0, AT1)(AT0 /*Display*/ display, AT1 /*Window**/ grab_window, int* owner_events) nothrow {
      return gdk_keyboard_grab_info_libgtk_only(UpCast!(Display*)(display), UpCast!(Window**)(grab_window), owner_events);
   }
}

struct Keymap /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   Display* display;


   // Unintrospectable function: get_default() / gdk_keymap_get_default()
   // Returns the #GdkKeymap attached to the default display.
   // RETURNS: the #GdkKeymap attached to the default display.
   static Keymap* get_default()() nothrow {
      return gdk_keymap_get_default();
   }

   // Unintrospectable function: get_for_display() / gdk_keymap_get_for_display()
   // VERSION: 2.2
   // Returns the #GdkKeymap attached to @display.
   // RETURNS: the #GdkKeymap attached to @display.
   // <display>: the #GdkDisplay.
   static Keymap* get_for_display(AT0)(AT0 /*Display*/ display) nothrow {
      return gdk_keymap_get_for_display(UpCast!(Display*)(display));
   }

   // VERSION: 2.20
   // Adds virtual modifiers (i.e. Super, Hyper and Meta) which correspond
   // to the real modifiers (i.e Mod2, Mod3, ...) in @modifiers.
   // are set in @state to their non-virtual counterparts (i.e. Mod2,
   // Mod3,...) and set the corresponding bits in @state.
   // 
   // GDK already does this before delivering key events, but for
   // compatibility reasons, it only sets the first virtual modifier
   // it finds, whereas this function sets all matching virtual modifiers.
   // 
   // This function is useful when matching key events against
   // accelerators.
   // <state>: pointer to the modifier mask to change
   void add_virtual_modifiers(AT0)(AT0 /*ModifierType*/ state) nothrow {
      gdk_keymap_add_virtual_modifiers(&this, UpCast!(ModifierType*)(state));
   }

   // VERSION: 2.16
   // Returns whether the Caps Lock modifer is locked.
   // RETURNS: %TRUE if Caps Lock is on
   int get_caps_lock_state()() nothrow {
      return gdk_keymap_get_caps_lock_state(&this);
   }

   // Returns the direction of effective layout of the keymap.
   // 
   // Note that passing %NULL for @keymap is deprecated and will stop
   // to work in GTK+ 3.0. Use gdk_keymap_get_for_display() instead.
   // 
   // if it can determine the direction. %PANGO_DIRECTION_NEUTRAL
   // otherwise.
   // RETURNS: %PANGO_DIRECTION_LTR or %PANGO_DIRECTION_RTL
   Pango.Direction get_direction()() nothrow {
      return gdk_keymap_get_direction(&this);
   }

   // Returns the keyvals bound to @hardware_keycode.
   // The Nth #GdkKeymapKey in @keys is bound to the Nth
   // keyval in @keyvals. Free the returned arrays with g_free().
   // When a keycode is pressed by the user, the keyval from
   // this list of entries is selected by considering the effective
   // keyboard group and level. See gdk_keymap_translate_keyboard_state().
   // 
   // Note that passing %NULL for @keymap is deprecated and will stop
   // to work in GTK+ 3.0. Use gdk_keymap_get_for_display() instead.
   // RETURNS: %TRUE if there were any entries
   // <hardware_keycode>: a keycode
   // <keys>: return location for array of #GdkKeymapKey, or %NULL
   // <keyvals>: return location for array of keyvals, or %NULL
   // <n_entries>: length of @keys and @keyvals
   int get_entries_for_keycode(AT0, AT1)(uint hardware_keycode, /*out*/ AT0 /*KeymapKey**/ keys, /*out*/ AT1 /*uint**/ keyvals, int* n_entries) nothrow {
      return gdk_keymap_get_entries_for_keycode(&this, hardware_keycode, UpCast!(KeymapKey**)(keys), UpCast!(uint**)(keyvals), n_entries);
   }

   // Obtains a list of keycode/group/level combinations that will
   // generate @keyval. Groups and levels are two kinds of keyboard mode;
   // in general, the level determines whether the top or bottom symbol
   // on a key is used, and the group determines whether the left or
   // right symbol is used. On US keyboards, the shift key changes the
   // keyboard level, and there are no groups. A group switch key might
   // convert a keyboard between Hebrew to English modes, for example.
   // #GdkEventKey contains a %group field that indicates the active
   // keyboard group. The level is computed from the modifier mask.
   // The returned array should be freed
   // with g_free().
   // 
   // Note that passing %NULL for @keymap is deprecated and will stop
   // to work in GTK+ 3.0. Use gdk_keymap_get_for_display() instead.
   // RETURNS: %TRUE if keys were found and returned
   // <keyval>: a keyval, such as %GDK_a, %GDK_Up, %GDK_Return, etc.
   // <keys>: return location for an array of #GdkKeymapKey
   // <n_keys>: return location for number of elements in returned array
   int get_entries_for_keyval(AT0)(uint keyval, /*out*/ AT0 /*KeymapKey**/ keys, /*out*/ int* n_keys) nothrow {
      return gdk_keymap_get_entries_for_keyval(&this, keyval, UpCast!(KeymapKey**)(keys), n_keys);
   }

   // VERSION: 2.12
   // Determines if keyboard layouts for both right-to-left and left-to-right
   // languages are in use.
   // 
   // Note that passing %NULL for @keymap is deprecated and will stop
   // to work in GTK+ 3.0. Use gdk_keymap_get_for_display() instead.
   // RETURNS: %TRUE if there are layouts in both directions, %FALSE otherwise
   int have_bidi_layouts()() nothrow {
      return gdk_keymap_have_bidi_layouts(&this);
   }

   // Looks up the keyval mapped to a keycode/group/level triplet.
   // If no keyval is bound to @key, returns 0. For normal user input,
   // you want to use gdk_keymap_translate_keyboard_state() instead of
   // this function, since the effective group/level may not be
   // the same as the current keyboard state.
   // 
   // Note that passing %NULL for @keymap is deprecated and will stop
   // to work in GTK+ 3.0. Use gdk_keymap_get_for_display() instead.
   // RETURNS: a keyval, or 0 if none was mapped to the given @key
   // <key>: a #GdkKeymapKey with keycode, group, and level initialized
   uint lookup_key(AT0)(AT0 /*KeymapKey*/ key) nothrow {
      return gdk_keymap_lookup_key(&this, UpCast!(KeymapKey*)(key));
   }

   // VERSION: 2.20
   // Maps the virtual modifiers (i.e. Super, Hyper and Meta) which
   // are set in @state to their non-virtual counterparts (i.e. Mod2,
   // Mod3,...) and set the corresponding bits in @state.
   // 
   // This function is useful when matching key events against
   // accelerators.
   // 
   // same non-virtual modifier. Note that %FALSE is also returned
   // if a virtual modifier is mapped to a non-virtual modifier that
   // was already set in @state.
   // RETURNS: %TRUE if no virtual modifiers were mapped to the
   // <state>: pointer to the modifier state to map
   int map_virtual_modifiers(AT0)(AT0 /*ModifierType*/ state) nothrow {
      return gdk_keymap_map_virtual_modifiers(&this, UpCast!(ModifierType*)(state));
   }

   // Translates the contents of a #GdkEventKey into a keyval, effective
   // group, and level. Modifiers that affected the translation and
   // are thus unavailable for application use are returned in
   // @consumed_modifiers.  See <xref linkend="key-group-explanation"/> for an explanation of
   // groups and levels.  The @effective_group is the group that was
   // actually used for the translation; some keys such as Enter are not
   // affected by the active keyboard group. The @level is derived from
   // @state. For convenience, #GdkEventKey already contains the translated
   // keyval, so this function isn't as useful as you might think.
   // 
   // <note><para>
   // @consumed_modifiers gives modifiers that should be masked out
   // from @state when comparing this key press to a hot key. For
   // instance, on a US keyboard, the <literal>plus</literal>
   // symbol is shifted, so when comparing a key press to a
   // <literal>&lt;Control&gt;plus</literal> accelerator &lt;Shift&gt; should
   // be masked out.
   // </para>
   // <informalexample><programlisting>
   // &sol;* We want to ignore irrelevant modifiers like ScrollLock *&sol;
   // &num;define ALL_ACCELS_MASK (GDK_CONTROL_MASK | GDK_SHIFT_MASK | GDK_MOD1_MASK)
   // gdk_keymap_translate_keyboard_state (keymap, event->hardware_keycode,
   // event->state, event->group,
   // &amp;keyval, NULL, NULL, &amp;consumed);
   // if (keyval == GDK_PLUS &&
   // (event->state &amp; ~consumed &amp; ALL_ACCELS_MASK) == GDK_CONTROL_MASK)
   // &sol;* Control was pressed *&sol;
   // </programlisting></informalexample>
   // <para>
   // An older interpretation @consumed_modifiers was that it contained
   // all modifiers that might affect the translation of the key;
   // this allowed accelerators to be stored with irrelevant consumed
   // modifiers, by doing:</para>
   // <informalexample><programlisting>
   // &sol;* XXX Don't do this XXX *&sol;
   // if (keyval == accel_keyval &&
   // (event->state &amp; ~consumed &amp; ALL_ACCELS_MASK) == (accel_mods &amp; ~consumed))
   // &sol;* Accelerator was pressed *&sol;
   // </programlisting></informalexample>
   // <para>
   // However, this did not work if multi-modifier combinations were
   // used in the keymap, since, for instance, <literal>&lt;Control&gt;</literal>
   // would be masked out even if only <literal>&lt;Control&gt;&lt;Alt&gt;</literal>
   // was used in the keymap. To support this usage as well as well as
   // possible, all <emphasis>single modifier</emphasis> combinations
   // that could affect the key for any combination of modifiers will
   // be returned in @consumed_modifiers; multi-modifier combinations
   // are returned only when actually found in @state. When you store
   // accelerators, you should always store them with consumed modifiers
   // removed. Store <literal>&lt;Control&gt;plus</literal>,
   // not <literal>&lt;Control&gt;&lt;Shift&gt;plus</literal>,
   // </para></note>
   // 
   // Note that passing %NULL for @keymap is deprecated and will stop
   // to work in GTK+ 3.0. Use gdk_keymap_get_for_display() instead.
   // RETURNS: %TRUE if there was a keyval bound to the keycode/state/group
   // <hardware_keycode>: a keycode
   // <state>: a modifier state
   // <group>: active keyboard group
   // <keyval>: return location for keyval, or %NULL
   // <effective_group>: return location for effective group, or %NULL
   // <level>: return location for level, or %NULL
   // <consumed_modifiers>: return location for modifiers that were used to determine the group or level, or %NULL
   int translate_keyboard_state(AT0, AT1)(uint hardware_keycode, ModifierType state, int group, /*out*/ AT0 /*uint*/ keyval=null, /*out*/ int* effective_group=null, /*out*/ int* level=null, /*out*/ AT1 /*ModifierType*/ consumed_modifiers=null) nothrow {
      return gdk_keymap_translate_keyboard_state(&this, hardware_keycode, state, group, UpCast!(uint*)(keyval), effective_group, level, UpCast!(ModifierType*)(consumed_modifiers));
   }

   // VERSION: 2.0
   // The ::direction-changed signal gets emitted when the direction of
   // the keymap changes.
   extern (C) alias static void function (Keymap* this_, void* user_data=null) nothrow signal_direction_changed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"direction-changed", CB/*:signal_direction_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_direction_changed)||_ttmm!(CB, signal_direction_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"direction-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 2.2
   // The ::keys-changed signal is emitted when the mapping represented by
   // @keymap changes.
   extern (C) alias static void function (Keymap* this_, void* user_data=null) nothrow signal_keys_changed;
   ulong signal_connect(string name:"keys-changed", CB/*:signal_keys_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_keys_changed)||_ttmm!(CB, signal_keys_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"keys-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 2.16
   // The ::state-changed signal is emitted when the state of the
   // keyboard changes, e.g when Caps Lock is turned on or off.
   // See gdk_keymap_get_caps_lock_state().
   extern (C) alias static void function (Keymap* this_, void* user_data=null) nothrow signal_state_changed;
   ulong signal_connect(string name:"state-changed", CB/*:signal_state_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_state_changed)||_ttmm!(CB, signal_state_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"state-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct KeymapClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function (Keymap* keymap) nothrow direction_changed;
   extern (C) void function (Keymap* keymap) nothrow keys_changed;
   extern (C) void function (Keymap* keymap) nothrow state_changed;
}

struct KeymapKey {
   uint keycode;
   int group, level;
}

enum LineStyle {
   SOLID = 0,
   ON_OFF_DASH = 1,
   DOUBLE_DASH = 2
}
enum int MAX_TIMECOORD_AXES = 128;
enum ModifierType {
   SHIFT_MASK = 1,
   LOCK_MASK = 2,
   CONTROL_MASK = 4,
   MOD1_MASK = 8,
   MOD2_MASK = 16,
   MOD3_MASK = 32,
   MOD4_MASK = 64,
   MOD5_MASK = 128,
   BUTTON1_MASK = 256,
   BUTTON2_MASK = 512,
   BUTTON3_MASK = 1024,
   BUTTON4_MASK = 2048,
   BUTTON5_MASK = 4096,
   SUPER_MASK = 67108864,
   HYPER_MASK = 134217728,
   META_MASK = 268435456,
   RELEASE_MASK = 1073741824,
   MODIFIER_MASK = 1543512063
}
enum NotifyType {
   ANCESTOR = 0,
   VIRTUAL = 1,
   INFERIOR = 2,
   NONLINEAR = 3,
   NONLINEAR_VIRTUAL = 4,
   UNKNOWN = 5
}
enum OverlapType {
   IN = 0,
   OUT = 1,
   PART = 2
}
enum OwnerChange {
   NEW_OWNER = 0,
   DESTROY = 1,
   CLOSE = 2
}
enum int PARENT_RELATIVE = 1;
enum int PRIORITY_REDRAW = 20;
struct PangoAttrEmbossColor {
   Pango.Attribute attr;
   Pango.Color color;


   // Unintrospectable function: new() / gdk_pango_attr_emboss_color_new()
   // VERSION: 2.12
   // Creates a new attribute specifying the color to emboss text with.
   // RETURNS: new #PangoAttribute
   // <color>: a GdkColor representing the color to emboss with
   static Pango.Attribute* new_(AT0)(AT0 /*Color*/ color) nothrow {
      return gdk_pango_attr_emboss_color_new(UpCast!(Color*)(color));
   }
}

struct PangoAttrEmbossed {
   Pango.Attribute attr;
   int embossed;


   // Unintrospectable function: new() / gdk_pango_attr_embossed_new()
   // Creates a new attribute flagging a region as embossed or not.
   // RETURNS: new #PangoAttribute
   // <embossed>: if the region should be embossed
   static Pango.Attribute* new_()(int embossed) nothrow {
      return gdk_pango_attr_embossed_new(embossed);
   }
}

struct PangoAttrStipple {
   Pango.Attribute attr;
   Bitmap* stipple;


   // Unintrospectable function: new() / gdk_pango_attr_stipple_new()
   // Creates a new attribute containing a stipple bitmap to be used when
   // rendering the text.
   // RETURNS: new #PangoAttribute
   // <stipple>: a bitmap to be set as stipple
   static Pango.Attribute* new_(AT0)(AT0 /*Bitmap*/ stipple) nothrow {
      return gdk_pango_attr_stipple_new(UpCast!(Bitmap*)(stipple));
   }
}


// #GdkPangoRenderer is a subclass of #PangoRenderer used for rendering
// Pango objects into GDK drawables. The default renderer for a particular
// screen is obtained with gdk_pango_renderer_get_default(); Pango
// functions like pango_renderer_draw_layout() and
// pango_renderer_draw_layout_line() are then used to draw objects with
// the renderer.
// 
// In most simple cases, applications can just use gdk_draw_layout(), and
// don't need to directly use #GdkPangoRenderer at all. Using the
// #GdkPangoRenderer directly is most useful when working with a
// transformation such as a rotation, because the Pango drawing functions
// take user space coordinates (coordinates before the transformation)
// instead of device coordinates.
// 
// In certain cases it can be useful to subclass #GdkPangoRenderer. Examples
// of reasons to do this are to add handling of custom attributes by
// overriding 'prepare_run' or to do custom drawing of embedded objects
// by overriding 'draw_shape'.
struct PangoRenderer /* : Pango.Renderer */ /* Version 2.6 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance renderer;
   Pango.Renderer parent_instance;
   private PangoRendererPrivate* priv;


   // VERSION: 2.6
   // Creates a new #PangoRenderer for @screen. Normally you can use the
   // results of gdk_pango_renderer_get_default() rather than creating a new
   // renderer.
   // RETURNS: a newly created #PangoRenderer. Free with g_object_unref().
   // <screen>: a #GdkScreen
   static PangoRenderer* /*new*/ new_(AT0)(AT0 /*Screen*/ screen) nothrow {
      return gdk_pango_renderer_new(UpCast!(Screen*)(screen));
   }
   static auto opCall(AT0)(AT0 /*Screen*/ screen) {
      return gdk_pango_renderer_new(UpCast!(Screen*)(screen));
   }

   // Unintrospectable function: get_default() / gdk_pango_renderer_get_default()
   // VERSION: 2.6
   // Gets the default #PangoRenderer for a screen. This default renderer
   // is shared by all users of the display, so properties such as the color
   // or transformation matrix set for the renderer may be overwritten
   // by functions such as gdk_draw_layout().
   // 
   // Before using the renderer, you need to call gdk_pango_renderer_set_drawable()
   // and gdk_pango_renderer_set_gc() to set the drawable and graphics context
   // to use for drawing.
   // 
   // renderer is owned by GTK+ and will be kept around until the
   // screen is closed.
   // RETURNS: the default #PangoRenderer for @screen. The
   // <screen>: a #GdkScreen
   static Pango.Renderer* get_default(AT0)(AT0 /*Screen*/ screen) nothrow {
      return gdk_pango_renderer_get_default(UpCast!(Screen*)(screen));
   }

   // VERSION: 2.6
   // Sets the drawable the renderer draws to.
   // <drawable>: the new target drawable, or %NULL
   void set_drawable(AT0)(AT0 /*Drawable*/ drawable=null) nothrow {
      gdk_pango_renderer_set_drawable(&this, UpCast!(Drawable*)(drawable));
   }

   // VERSION: 2.6
   // Sets the GC the renderer draws with. Note that the GC must not be
   // modified until it is unset by calling the function again with
   // %NULL for the @gc parameter, since GDK may make internal copies
   // of the GC which won't be updated to follow changes to the
   // original GC.
   // <gc>: the new GC to use for drawing, or %NULL
   void set_gc(AT0)(AT0 /*GC*/ gc=null) nothrow {
      gdk_pango_renderer_set_gc(&this, UpCast!(GC*)(gc));
   }

   // VERSION: 2.6
   // Sets the color for a particular render part (foreground,
   // background, underline, etc.), overriding any attributes on the layouts
   // renderered with this renderer.
   // <part>: the part to render to set the color of
   // <color>: the color to use, or %NULL to unset a previously set override color.
   void set_override_color(AT0)(Pango.RenderPart part, AT0 /*Color*/ color=null) nothrow {
      gdk_pango_renderer_set_override_color(&this, part, UpCast!(Color*)(color));
   }

   // VERSION: 2.6
   // Sets the stipple for one render part (foreground, background, underline,
   // etc.) Note that this is overwritten when iterating through the individual
   // styled runs of a #PangoLayout or #PangoLayoutLine. This function is thus
   // only useful when you call low level functions like pango_renderer_draw_glyphs()
   // directly, or in the 'prepare_run' virtual function of a subclass of
   // #GdkPangoRenderer.
   // <part>: the part to render with the stipple
   // <stipple>: the new stipple value.
   void set_stipple(AT0)(Pango.RenderPart part, AT0 /*Bitmap*/ stipple) nothrow {
      gdk_pango_renderer_set_stipple(&this, part, UpCast!(Bitmap*)(stipple));
   }
}

// #GdkPangoRenderer is the class structure for #GdkPangoRenderer.
struct PangoRendererClass /* Version 2.6 */ {
   private Pango.RendererClass parent_class;
}

struct PangoRendererPrivate {
}

struct Pixmap /* : Drawable */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent drawable;
   Drawable method_parent;


   // Wraps a native window for the default display in a #GdkPixmap.
   // This may fail if the pixmap has been destroyed.
   // 
   // For example in the X backend, a native pixmap handle is an Xlib
   // <type>XID</type>.
   // 
   // native pixmap or %NULL if the pixmap has been destroyed.
   // RETURNS: the newly-created #GdkPixmap wrapper for the
   // <anid>: a native pixmap handle.
   static Pixmap* /*new*/ foreign_new()(NativeWindow anid) nothrow {
      return gdk_pixmap_foreign_new(anid);
   }
   static auto opCall()(NativeWindow anid) {
      return gdk_pixmap_foreign_new(anid);
   }

   // VERSION: 2.2
   // Wraps a native pixmap in a #GdkPixmap.
   // This may fail if the pixmap has been destroyed.
   // 
   // For example in the X backend, a native pixmap handle is an Xlib
   // <type>XID</type>.
   // 
   // native pixmap or %NULL if the pixmap has been destroyed.
   // RETURNS: the newly-created #GdkPixmap wrapper for the
   // <display>: The #GdkDisplay where @anid is located.
   // <anid>: a native pixmap handle.
   static Pixmap* /*new*/ foreign_new_for_display(AT0)(AT0 /*Display*/ display, NativeWindow anid) nothrow {
      return gdk_pixmap_foreign_new_for_display(UpCast!(Display*)(display), anid);
   }
   static auto opCall(AT0)(AT0 /*Display*/ display, NativeWindow anid) {
      return gdk_pixmap_foreign_new_for_display(UpCast!(Display*)(display), anid);
   }

   // VERSION: 2.10
   // Wraps a native pixmap in a #GdkPixmap.
   // This may fail if the pixmap has been destroyed.
   // 
   // For example in the X backend, a native pixmap handle is an Xlib
   // <type>XID</type>.
   // 
   // This function is an alternative to gdk_pixmap_foreign_new_for_display()
   // for cases where the dimensions of the pixmap are known. For the X
   // backend, this avoids a roundtrip to the server.
   // 
   // native pixmap or %NULL if the pixmap has been destroyed.
   // RETURNS: the newly-created #GdkPixmap wrapper for the
   // <screen>: a #GdkScreen
   // <anid>: a native pixmap handle
   // <width>: the width of the pixmap identified by @anid
   // <height>: the height of the pixmap identified by @anid
   // <depth>: the depth of the pixmap identified by @anid
   static Pixmap* /*new*/ foreign_new_for_screen(AT0)(AT0 /*Screen*/ screen, NativeWindow anid, int width, int height, int depth) nothrow {
      return gdk_pixmap_foreign_new_for_screen(UpCast!(Screen*)(screen), anid, width, height, depth);
   }
   static auto opCall(AT0)(AT0 /*Screen*/ screen, NativeWindow anid, int width, int height, int depth) {
      return gdk_pixmap_foreign_new_for_screen(UpCast!(Screen*)(screen), anid, width, height, depth);
   }
   static Pixmap* /*new*/ new_(AT0)(AT0 /*Drawable*/ drawable, int width, int height, int depth) nothrow {
      return gdk_pixmap_new(UpCast!(Drawable*)(drawable), width, height, depth);
   }
   static auto opCall(AT0)(AT0 /*Drawable*/ drawable, int width, int height, int depth) {
      return gdk_pixmap_new(UpCast!(Drawable*)(drawable), width, height, depth);
   }

   // DEPRECATED (v2.22) function: colormap_create_from_xpm - Use a #GdkPixbuf instead. You can use
   // Create a pixmap from a XPM file using a particular colormap.
   // 
   // 
   // gdk_pixbuf_new_from_file() to create it.
   // If you must use a pixmap, use gdk_pixmap_new() to
   // create it and Cairo to draw the pixbuf onto it.
   // RETURNS: the #GdkPixmap.
   // <drawable>: a #GdkDrawable, used to determine default values for the new pixmap. Can be %NULL if @colormap is given.
   // <colormap>: the #GdkColormap that the new pixmap will be use. If omitted, the colormap for @window will be used.
   // <mask>: a pointer to a place to store a bitmap representing the transparency mask of the XPM file. Can be %NULL, in which case transparency will be ignored.
   // <transparent_color>: the color to be used for the pixels that are transparent in the input file. Can be %NULL, in which case a default color will be used.
   // <filename>: the filename of a file containing XPM data.
   static Pixmap* colormap_create_from_xpm(AT0, AT1, AT2, AT3, AT4)(AT0 /*Drawable*/ drawable, AT1 /*Colormap*/ colormap, AT2 /*Bitmap**/ mask, AT3 /*Color*/ transparent_color, AT4 /*char*/ filename) nothrow {
      return gdk_pixmap_colormap_create_from_xpm(UpCast!(Drawable*)(drawable), UpCast!(Colormap*)(colormap), UpCast!(Bitmap**)(mask), UpCast!(Color*)(transparent_color), toCString!(char*)(filename));
   }

   // DEPRECATED (v2.22) function: colormap_create_from_xpm_d - Use a #GdkPixbuf instead. You can use
   // Create a pixmap from data in XPM format using a particular
   // colormap.
   // 
   // 
   // gdk_pixbuf_new_from_xpm_data() to create it.
   // If you must use a pixmap, use gdk_pixmap_new() to
   // create it and Cairo to draw the pixbuf onto it.
   // RETURNS: the #GdkPixmap.
   // <drawable>: a #GdkDrawable, used to determine default values for the new pixmap. Can be %NULL if @colormap is given.
   // <colormap>: the #GdkColormap that the new pixmap will be use. If omitted, the colormap for @window will be used.
   // <mask>: a pointer to a place to store a bitmap representing the transparency mask of the XPM file. Can be %NULL, in which case transparency will be ignored.
   // <transparent_color>: the color to be used for the pixels that are transparent in the input file. Can be %NULL, in which case a default color will be used.
   // <data>: Pointer to a string containing the XPM data.
   static Pixmap* colormap_create_from_xpm_d(AT0, AT1, AT2, AT3, AT4)(AT0 /*Drawable*/ drawable, AT1 /*Colormap*/ colormap, AT2 /*Bitmap**/ mask, AT3 /*Color*/ transparent_color, AT4 /*char**/ data) nothrow {
      return gdk_pixmap_colormap_create_from_xpm_d(UpCast!(Drawable*)(drawable), UpCast!(Colormap*)(colormap), UpCast!(Bitmap**)(mask), UpCast!(Color*)(transparent_color), toCString!(char**)(data));
   }
   // Unintrospectable function: create_from_data() / gdk_pixmap_create_from_data()
   static Pixmap* create_from_data(AT0, AT1, AT2, AT3)(AT0 /*Drawable*/ drawable, AT1 /*char*/ data, int width, int height, int depth, AT2 /*Color*/ fg, AT3 /*Color*/ bg) nothrow {
      return gdk_pixmap_create_from_data(UpCast!(Drawable*)(drawable), toCString!(char*)(data), width, height, depth, UpCast!(Color*)(fg), UpCast!(Color*)(bg));
   }

   // DEPRECATED (v2.22) function: create_from_xpm - Use a #GdkPixbuf instead. You can use
   // Create a pixmap from a XPM file.
   // 
   // 
   // gdk_pixbuf_new_from_file() to create it.
   // If you must use a pixmap, use gdk_pixmap_new() to
   // create it and Cairo to draw the pixbuf onto it.
   // RETURNS: the #GdkPixmap
   // <drawable>: a #GdkDrawable, used to determine default values for the new pixmap.
   // <mask>: (out) a pointer to a place to store a bitmap representing the transparency mask of the XPM file. Can be %NULL, in which case transparency will be ignored.
   // <transparent_color>: the color to be used for the pixels that are transparent in the input file. Can be %NULL, in which case a default color will be used.
   // <filename>: the filename of a file containing XPM data.
   static Pixmap* create_from_xpm(AT0, AT1, AT2, AT3)(AT0 /*Drawable*/ drawable, AT1 /*Bitmap**/ mask, AT2 /*Color*/ transparent_color, AT3 /*char*/ filename) nothrow {
      return gdk_pixmap_create_from_xpm(UpCast!(Drawable*)(drawable), UpCast!(Bitmap**)(mask), UpCast!(Color*)(transparent_color), toCString!(char*)(filename));
   }

   // DEPRECATED (v2.22) function: create_from_xpm_d - Use a #GdkPixbuf instead. You can use
   // Create a pixmap from data in XPM format.
   // 
   // 
   // gdk_pixbuf_new_from_xpm_data() to create it.
   // If you must use a pixmap, use gdk_pixmap_new() to
   // create it and Cairo to draw the pixbuf onto it.
   // RETURNS: the #GdkPixmap.
   // <drawable>: a #GdkDrawable, used to determine default values for the new pixmap.
   // <mask>: Pointer to a place to store a bitmap representing the transparency mask of the XPM file. Can be %NULL, in which case transparency will be ignored.
   // <transparent_color>: This color will be used for the pixels that are transparent in the input file. Can be %NULL in which case a default color will be used.
   // <data>: Pointer to a string containing the XPM data.
   static Pixmap* create_from_xpm_d(AT0, AT1, AT2, AT3)(AT0 /*Drawable*/ drawable, /*out*/ AT1 /*Bitmap**/ mask, AT2 /*Color*/ transparent_color, AT3 /*char**/ data) nothrow {
      return gdk_pixmap_create_from_xpm_d(UpCast!(Drawable*)(drawable), UpCast!(Bitmap**)(mask), UpCast!(Color*)(transparent_color), toCString!(char**)(data));
   }

   // Unintrospectable function: lookup() / gdk_pixmap_lookup()
   // Looks up the #GdkPixmap that wraps the given native pixmap handle.
   // 
   // For example in the X backend, a native pixmap handle is an Xlib
   // <type>XID</type>.
   // 
   // or %NULL if there is none.
   // RETURNS: the #GdkPixmap wrapper for the native pixmap,
   // <anid>: a native pixmap handle.
   static Pixmap* lookup()(NativeWindow anid) nothrow {
      return gdk_pixmap_lookup(anid);
   }

   // Unintrospectable function: lookup_for_display() / gdk_pixmap_lookup_for_display()
   // VERSION: 2.2
   // Looks up the #GdkPixmap that wraps the given native pixmap handle.
   // 
   // For example in the X backend, a native pixmap handle is an Xlib
   // <type>XID</type>.
   // 
   // or %NULL if there is none.
   // RETURNS: the #GdkPixmap wrapper for the native pixmap,
   // <display>: the #GdkDisplay associated with @anid
   // <anid>: a native pixmap handle.
   static Pixmap* lookup_for_display(AT0)(AT0 /*Display*/ display, NativeWindow anid) nothrow {
      return gdk_pixmap_lookup_for_display(UpCast!(Display*)(display), anid);
   }

   // VERSION: 2.24
   // This function is purely to make it possible to query the size of pixmaps
   // even when compiling without deprecated symbols and you must use pixmaps.
   // It is identical to gdk_drawable_get_size(), but for pixmaps.
   // <width>: location to store @pixmap's width, or %NULL
   // <height>: location to store @pixmap's height, or %NULL
   void get_size()(/*out*/ int* width=null, /*out*/ int* height=null) nothrow {
      gdk_pixmap_get_size(&this, width, height);
   }
}

struct PixmapObject {
   Drawable parent_instance;
   Drawable* impl;
   int depth;
}

struct PixmapObjectClass {
   DrawableClass parent_class;
}

struct Point {
   int x, y;
}

struct PointerHooks {
   // Unintrospectable functionp: get_pointer() / ()
   extern (C) Window* function (Window* window, int* x, int* y, ModifierType* mask) nothrow get_pointer;
   // Unintrospectable functionp: window_at_pointer() / ()
   extern (C) Window* function (Screen* screen, int* win_x, int* win_y) nothrow window_at_pointer;
}

struct PointerWindowInfo {
   Window* toplevel_under_pointer, window_under_pointer;
   double toplevel_x, toplevel_y;
   uint state, button;
   c_ulong motion_hint_serial;
}

enum PropMode {
   REPLACE = 0,
   PREPEND = 1,
   APPEND = 2
}
enum PropertyState {
   NEW_VALUE = 0,
   DELETE = 1
}
struct Rectangle {
   int x, y, width, height;


   // Calculates the intersection of two rectangles. It is allowed for
   // @dest to be the same as either @src1 or @src2. If the rectangles 
   // do not intersect, @dest's width and height is set to 0 and its x 
   // and y values are undefined. If you are only interested in whether
   // the rectangles intersect, but not in the intersecting area itself,
   // pass %NULL for @dest.
   // RETURNS: %TRUE if the rectangles intersect.
   // <src2>: a #GdkRectangle
   // <dest>: return location for the intersection of @src1 and @src2, or %NULL
   int intersect(AT0, AT1)(AT0 /*Rectangle*/ src2, /*out*/ AT1 /*Rectangle*/ dest=null) nothrow {
      return gdk_rectangle_intersect(&this, UpCast!(Rectangle*)(src2), UpCast!(Rectangle*)(dest));
   }

   // Calculates the union of two rectangles.
   // The union of rectangles @src1 and @src2 is the smallest rectangle which
   // includes both @src1 and @src2 within it.
   // It is allowed for @dest to be the same as either @src1 or @src2.
   // <src2>: a #GdkRectangle
   // <dest>: return location for the union of @src1 and @src2
   void union_(AT0, AT1)(AT0 /*Rectangle*/ src2, /*out*/ AT1 /*Rectangle*/ dest) nothrow {
      gdk_rectangle_union(&this, UpCast!(Rectangle*)(src2), UpCast!(Rectangle*)(dest));
   }

   //  --- mixin/Gdk2_Rectangle.d --->

   void union_()(Rectangle* src2) {
      gdk_rectangle_union(&this, src2, &this);
   }
   
   // <--- mixin/Gdk2_Rectangle.d ---
}

struct Region {

   // Unintrospectable method: copy() / gdk_region_copy()
   // Copies @region, creating an identical new region.
   // RETURNS: a new region identical to @region
   Region* copy()() nothrow {
      return gdk_region_copy(&this);
   }
   // Destroys a #GdkRegion.
   void destroy()() nothrow {
      gdk_region_destroy(&this);
   }

   // Finds out if the #GdkRegion is empty.
   // RETURNS: %TRUE if @region is empty.
   int empty()() nothrow {
      return gdk_region_empty(&this);
   }

   // Finds out if the two regions are the same.
   // RETURNS: %TRUE if @region1 and @region2 are equal.
   // <region2>: a #GdkRegion
   int equal(AT0)(AT0 /*Region*/ region2) nothrow {
      return gdk_region_equal(&this, UpCast!(Region*)(region2));
   }

   // Obtains the smallest rectangle which includes the entire #GdkRegion.
   // <rectangle>: return location for the clipbox
   void get_clipbox(AT0)(AT0 /*Rectangle*/ rectangle) nothrow {
      gdk_region_get_clipbox(&this, UpCast!(Rectangle*)(rectangle));
   }

   // Obtains the area covered by the region as a list of rectangles.
   // The array returned in @rectangles must be freed with g_free().
   // <rectangles>: return location for an array of rectangles
   // <n_rectangles>: length of returned array
   void get_rectangles(AT0)(AT0 /*Rectangle**/ rectangles, int* n_rectangles) nothrow {
      gdk_region_get_rectangles(&this, UpCast!(Rectangle**)(rectangles), n_rectangles);
   }

   // Sets the area of @source1 to the intersection of the areas of @source1
   // and @source2. The resulting area is the set of pixels contained in
   // both @source1 and @source2.
   // <source2>: another #GdkRegion
   void intersect(AT0)(AT0 /*Region*/ source2) nothrow {
      gdk_region_intersect(&this, UpCast!(Region*)(source2));
   }

   // Moves a region the specified distance.
   // <dx>: the distance to move the region horizontally
   // <dy>: the distance to move the region vertically
   void offset()(int dx, int dy) nothrow {
      gdk_region_offset(&this, dx, dy);
   }

   // Finds out if a point is in a region.
   // RETURNS: %TRUE if the point is in @region.
   // <x>: the x coordinate of a point
   // <y>: the y coordinate of a point
   int point_in()(int x, int y) nothrow {
      return gdk_region_point_in(&this, x, y);
   }

   // VERSION: 2.18
   // DEPRECATED (v2.22) method: rect_equal - Use gdk_region_new_rect() and gdk_region_equal() to 
   // Finds out if a regions is the same as a rectangle.
   // 
   // 
   // 
   // achieve the same effect.
   // RETURNS: %TRUE if @region and @rectangle are equal.
   // <rectangle>: a #GdkRectangle
   int rect_equal(AT0)(AT0 /*Rectangle*/ rectangle) nothrow {
      return gdk_region_rect_equal(&this, UpCast!(Rectangle*)(rectangle));
   }

   // Tests whether a rectangle is within a region.
   // 
   // %GDK_OVERLAP_RECTANGLE_PART, depending on whether the rectangle is inside,
   // outside, or partly inside the #GdkRegion, respectively.
   // RETURNS: %GDK_OVERLAP_RECTANGLE_IN, %GDK_OVERLAP_RECTANGLE_OUT, or
   // <rectangle>: a #GdkRectangle.
   OverlapType rect_in(AT0)(AT0 /*Rectangle*/ rectangle) nothrow {
      return gdk_region_rect_in(&this, UpCast!(Rectangle*)(rectangle));
   }

   // DEPRECATED (v2.22) method: shrink - There is no replacement for this function.
   // Resizes a region by the specified amount.
   // Positive values shrink the region. Negative values expand it.
   // <dx>: the number of pixels to shrink the region horizontally
   // <dy>: the number of pixels to shrink the region vertically
   void shrink()(int dx, int dy) nothrow {
      gdk_region_shrink(&this, dx, dy);
   }

   // Unintrospectable method: spans_intersect_foreach() / gdk_region_spans_intersect_foreach()
   // DEPRECATED (v2.22) method: spans_intersect_foreach - There is no replacement.
   // Calls a function on each span in the intersection of @region and @spans.
   // <spans>: an array of #GdkSpans
   // <n_spans>: the length of @spans
   // <sorted>: %TRUE if @spans is sorted wrt. the y coordinate
   // <function>: function to call on each span in the intersection
   // <data>: data to pass to @function
   void spans_intersect_foreach(AT0, AT1)(AT0 /*Span*/ spans, int n_spans, int sorted, SpanFunc function_, AT1 /*void*/ data) nothrow {
      gdk_region_spans_intersect_foreach(&this, UpCast!(Span*)(spans), n_spans, sorted, function_, UpCast!(void*)(data));
   }

   // Subtracts the area of @source2 from the area @source1. The resulting
   // area is the set of pixels contained in @source1 but not in @source2.
   // <source2>: another #GdkRegion
   void subtract(AT0)(AT0 /*Region*/ source2) nothrow {
      gdk_region_subtract(&this, UpCast!(Region*)(source2));
   }

   // Sets the area of @source1 to the union of the areas of @source1 and
   // @source2. The resulting area is the set of pixels contained in
   // either @source1 or @source2.
   // <source2>: a #GdkRegion
   void union_(AT0)(AT0 /*Region*/ source2) nothrow {
      gdk_region_union(&this, UpCast!(Region*)(source2));
   }

   // Sets the area of @region to the union of the areas of @region and
   // @rect. The resulting area is the set of pixels contained in
   // either @region or @rect.
   // <rect>: a #GdkRectangle.
   void union_with_rect(AT0)(AT0 /*Rectangle*/ rect) nothrow {
      gdk_region_union_with_rect(&this, UpCast!(Rectangle*)(rect));
   }

   // Sets the area of @source1 to the exclusive-OR of the areas of @source1
   // and @source2. The resulting area is the set of pixels contained in one
   // or the other of the two sources but not in both.
   // <source2>: another #GdkRegion
   void xor(AT0)(AT0 /*Region*/ source2) nothrow {
      gdk_region_xor(&this, UpCast!(Region*)(source2));
   }

   // Unintrospectable function: new() / gdk_region_new()
   // Creates a new empty #GdkRegion.
   // RETURNS: a new empty #GdkRegion
   static Region* new_()() nothrow {
      return gdk_region_new();
   }

   // Unintrospectable function: polygon() / gdk_region_polygon()
   // DEPRECATED (v2.22) function: polygon - There is no replacement. For working with paths, please
   // Creates a new #GdkRegion using the polygon defined by a 
   // number of points.
   // 
   // 
   // use Cairo.
   // RETURNS: a new #GdkRegion based on the given polygon
   // <points>: an array of #GdkPoint structs
   // <n_points>: the number of elements in the @points array
   // <fill_rule>: specifies which pixels are included in the region when the polygon overlaps itself.
   static Region* polygon(AT0)(AT0 /*Point*/ points, int n_points, FillRule fill_rule) nothrow {
      return gdk_region_polygon(UpCast!(Point*)(points), n_points, fill_rule);
   }

   // Unintrospectable function: rectangle() / gdk_region_rectangle()
   // Creates a new region containing the area @rectangle.
   // RETURNS: a new region
   // <rectangle>: a #GdkRectangle
   static Region* rectangle(AT0)(AT0 /*Rectangle*/ rectangle) nothrow {
      return gdk_region_rectangle(UpCast!(Rectangle*)(rectangle));
   }
}

struct RgbCmap {
   uint[256] colors;
   int n_colors;
   private GLib2.SList* info_list;

   void free()() nothrow {
      gdk_rgb_cmap_free(&this);
   }
   // Unintrospectable function: new() / gdk_rgb_cmap_new()
   static RgbCmap* new_(AT0)(AT0 /*uint*/ colors, int n_colors) nothrow {
      return gdk_rgb_cmap_new(UpCast!(uint*)(colors), n_colors);
   }
}

enum RgbDither {
   NONE = 0,
   NORMAL = 1,
   MAX = 2
}
struct Screen /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
    static import std.bitmanip; mixin(std.bitmanip.bitfields!(
   uint, "closed", 1,
    uint, "__dummy32A", 31));
   GC*[32] normal_gcs, exposure_gcs, subwindow_gcs;
   cairo.FontOptions* font_options;
   double resolution;


   // VERSION: 2.2
   // Gets the default screen for the default display. (See
   // gdk_display_get_default ()).
   // RETURNS: a #GdkScreen, or %NULL if there is no default display.
   static Screen* get_default()() nothrow {
      return gdk_screen_get_default();
   }

   // Returns the height of the default screen in pixels.
   // RETURNS: the height of the default screen in pixels.
   static int height()() nothrow {
      return gdk_screen_height();
   }

   // Returns the height of the default screen in millimeters.
   // Note that on many X servers this value will not be correct.
   // 
   // though it is not always correct.
   // RETURNS: the height of the default screen in millimeters,
   static int height_mm()() nothrow {
      return gdk_screen_height_mm();
   }

   // Returns the width of the default screen in pixels.
   // RETURNS: the width of the default screen in pixels.
   static int width()() nothrow {
      return gdk_screen_width();
   }

   // Returns the width of the default screen in millimeters.
   // Note that on many X servers this value will not be correct.
   // 
   // though it is not always correct.
   // RETURNS: the width of the default screen in millimeters,
   static int width_mm()() nothrow {
      return gdk_screen_width_mm();
   }
   void broadcast_client_message(AT0)(AT0 /*Event*/ event) nothrow {
      gdk_screen_broadcast_client_message(&this, UpCast!(Event*)(event));
   }

   // Unintrospectable method: get_active_window() / gdk_screen_get_active_window()
   // VERSION: 2.10
   // Returns the screen's currently active window.
   // 
   // On X11, this is done by inspecting the _NET_ACTIVE_WINDOW property
   // on the root window, as described in the <ulink
   // url="http://www.freedesktop.org/Standards/wm-spec">Extended Window
   // Manager Hints</ulink>. If there is no currently currently active
   // window, or the window manager does not support the
   // _NET_ACTIVE_WINDOW hint, this function returns %NULL.
   // 
   // On other platforms, this function may return %NULL, depending on whether
   // it is implementable on that platform.
   // 
   // The returned window should be unrefed using g_object_unref() when
   // no longer needed.
   // RETURNS: the currently active window, or %NULL.
   Window* get_active_window()() nothrow {
      return gdk_screen_get_active_window(&this);
   }

   // VERSION: 2.2
   // Gets the default colormap for @screen.
   // RETURNS: the default #GdkColormap.
   Colormap* get_default_colormap()() nothrow {
      return gdk_screen_get_default_colormap(&this);
   }

   // Unintrospectable method: get_display() / gdk_screen_get_display()
   // VERSION: 2.2
   // Gets the display to which the @screen belongs.
   // RETURNS: the display to which @screen belongs
   Display* get_display()() nothrow {
      return gdk_screen_get_display(&this);
   }

   // VERSION: 2.10
   // Gets any options previously set with gdk_screen_set_font_options().
   // 
   // font options have been set.
   // RETURNS: the current font options, or %NULL if no default
   cairo.FontOptions* get_font_options()() nothrow {
      return gdk_screen_get_font_options(&this);
   }

   // VERSION: 2.2
   // Gets the height of @screen in pixels
   // RETURNS: the height of @screen in pixels.
   int get_height()() nothrow {
      return gdk_screen_get_height(&this);
   }

   // VERSION: 2.2
   // Returns the height of @screen in millimeters. 
   // Note that on some X servers this value will not be correct.
   // RETURNS: the heigth of @screen in millimeters.
   int get_height_mm()() nothrow {
      return gdk_screen_get_height_mm(&this);
   }

   // VERSION: 2.2
   // Returns the monitor number in which the point (@x,@y) is located.
   // 
   // a monitor close to (@x,@y) if the point is not in any monitor.
   // RETURNS: the monitor number in which the point (@x,@y) lies, or
   // <x>: the x coordinate in the virtual screen.
   // <y>: the y coordinate in the virtual screen.
   int get_monitor_at_point()(int x, int y) nothrow {
      return gdk_screen_get_monitor_at_point(&this, x, y);
   }

   // VERSION: 2.2
   // Returns the number of the monitor in which the largest area of the 
   // bounding rectangle of @window resides.
   // RETURNS: the monitor number in which most of @window is located, or if @window does not intersect any monitors, a monitor, close to @window.
   // <window>: a #GdkWindow
   int get_monitor_at_window(AT0)(AT0 /*Window*/ window) nothrow {
      return gdk_screen_get_monitor_at_window(&this, UpCast!(Window*)(window));
   }

   // VERSION: 2.2
   // Retrieves the #GdkRectangle representing the size and position of
   // the individual monitor within the entire screen area.
   // 
   // Note that the size of the entire screen area can be retrieved via
   // gdk_screen_get_width() and gdk_screen_get_height().
   // <monitor_num>: the monitor number, between 0 and gdk_screen_get_n_monitors (screen)
   // <dest>: a #GdkRectangle to be filled with the monitor geometry
   void get_monitor_geometry(AT0)(int monitor_num, AT0 /*Rectangle*/ dest) nothrow {
      gdk_screen_get_monitor_geometry(&this, monitor_num, UpCast!(Rectangle*)(dest));
   }

   // VERSION: 2.14
   // Gets the height in millimeters of the specified monitor.
   // RETURNS: the height of the monitor, or -1 if not available
   // <monitor_num>: number of the monitor, between 0 and gdk_screen_get_n_monitors (screen)
   int get_monitor_height_mm()(int monitor_num) nothrow {
      return gdk_screen_get_monitor_height_mm(&this, monitor_num);
   }

   // VERSION: 2.14
   // Returns the output name of the specified monitor.
   // Usually something like VGA, DVI, or TV, not the actual
   // product name of the display device.
   // 
   // or %NULL if the name cannot be determined
   // RETURNS: a newly-allocated string containing the name of the monitor,
   // <monitor_num>: number of the monitor, between 0 and gdk_screen_get_n_monitors (screen)
   char* /*new*/ get_monitor_plug_name()(int monitor_num) nothrow {
      return gdk_screen_get_monitor_plug_name(&this, monitor_num);
   }

   // VERSION: 2.14
   // Gets the width in millimeters of the specified monitor, if available.
   // RETURNS: the width of the monitor, or -1 if not available
   // <monitor_num>: number of the monitor, between 0 and gdk_screen_get_n_monitors (screen)
   int get_monitor_width_mm()(int monitor_num) nothrow {
      return gdk_screen_get_monitor_width_mm(&this, monitor_num);
   }

   // VERSION: 2.2
   // Returns the number of monitors which @screen consists of.
   // RETURNS: number of monitors which @screen consists of
   int get_n_monitors()() nothrow {
      return gdk_screen_get_n_monitors(&this);
   }

   // VERSION: 2.2
   // Gets the index of @screen among the screens in the display
   // to which it belongs. (See gdk_screen_get_display())
   // RETURNS: the index
   int get_number()() nothrow {
      return gdk_screen_get_number(&this);
   }

   // VERSION: 2.20
   // Gets the primary monitor for @screen.  The primary monitor
   // is considered the monitor where the 'main desktop' lives.
   // While normal application windows typically allow the window
   // manager to place the windows, specialized desktop applications
   // such as panels should place themselves on the primary monitor.
   // 
   // If no primary monitor is configured by the user, the return value
   // will be 0, defaulting to the first monitor.
   // RETURNS: An integer index for the primary monitor, or 0 if none is configured.
   int get_primary_monitor()() nothrow {
      return gdk_screen_get_primary_monitor(&this);
   }

   // VERSION: 2.10
   // Gets the resolution for font handling on the screen; see
   // gdk_screen_set_resolution() for full details.
   // 
   // has been set.
   // RETURNS: the current resolution, or -1 if no resolution
   double get_resolution()() nothrow {
      return gdk_screen_get_resolution(&this);
   }

   // VERSION: 2.2
   // DEPRECATED (v2.22) method: get_rgb_colormap - Use gdk_screen_get_system_colormap()
   // Gets the preferred colormap for rendering image data on @screen.
   // Not a very useful function; historically, GDK could only render RGB
   // image data to one colormap and visual, but in the current version
   // it can render to any colormap and visual. So there's no need to
   // call this function.
   // RETURNS: the preferred colormap
   Colormap* get_rgb_colormap()() nothrow {
      return gdk_screen_get_rgb_colormap(&this);
   }

   // VERSION: 2.2
   // DEPRECATED (v2.22) method: get_rgb_visual - Use gdk_screen_get_system_visual()
   // Gets a "preferred visual" chosen by GdkRGB for rendering image data
   // on @screen. In previous versions of
   // GDK, this was the only visual GdkRGB could use for rendering. In
   // current versions, it's simply the visual GdkRGB would have chosen as 
   // the optimal one in those previous versions. GdkRGB can now render to 
   // drawables with any visual.
   // RETURNS: The #GdkVisual chosen by GdkRGB.
   Visual* get_rgb_visual()() nothrow {
      return gdk_screen_get_rgb_visual(&this);
   }

   // VERSION: 2.8
   // Gets a colormap to use for creating windows or pixmaps with an
   // alpha channel. The windowing system on which GTK+ is running
   // may not support this capability, in which case %NULL will
   // be returned. Even if a non-%NULL value is returned, its
   // possible that the window's alpha channel won't be honored
   // when displaying the window on the screen: in particular, for
   // X an appropriate windowing manager and compositing manager
   // must be running to provide appropriate display.
   // 
   // This functionality is not implemented in the Windows backend.
   // 
   // For setting an overall opacity for a top-level window, see
   // gdk_window_set_opacity().
   // an alpha channel or %NULL if the capability is not available.
   // RETURNS: a colormap to use for windows with
   Colormap* get_rgba_colormap()() nothrow {
      return gdk_screen_get_rgba_colormap(&this);
   }

   // VERSION: 2.8
   // Gets a visual to use for creating windows or pixmaps with an
   // alpha channel. See the docs for gdk_screen_get_rgba_colormap()
   // for caveats.
   // 
   // alpha channel or %NULL if the capability is not available.
   // RETURNS: a visual to use for windows with an
   Visual* get_rgba_visual()() nothrow {
      return gdk_screen_get_rgba_visual(&this);
   }

   // VERSION: 2.2
   // Gets the root window of @screen.
   // RETURNS: the root window
   Window* get_root_window()() nothrow {
      return gdk_screen_get_root_window(&this);
   }
   int get_setting(AT0, AT1)(AT0 /*char*/ name, AT1 /*GObject2.Value*/ value) nothrow {
      return gdk_screen_get_setting(&this, toCString!(char*)(name), UpCast!(GObject2.Value*)(value));
   }

   // VERSION: 2.2
   // Gets the system's default colormap for @screen
   // RETURNS: the default colormap for @screen.
   Colormap* get_system_colormap()() nothrow {
      return gdk_screen_get_system_colormap(&this);
   }

   // VERSION: 2.2
   // Get the system's default visual for @screen.
   // This is the visual for the root window of the display.
   // The return value should not be freed.
   // RETURNS: the system visual
   Visual* get_system_visual()() nothrow {
      return gdk_screen_get_system_visual(&this);
   }

   // VERSION: 2.2
   // Obtains a list of all toplevel windows known to GDK on the screen @screen.
   // A toplevel window is a child of the root window (see
   // gdk_get_default_root_window()).
   // 
   // The returned list should be freed with g_list_free(), but
   // its elements need not be freed.
   // 
   // list of toplevel windows, free with g_list_free()
   GLib2.List* /*new container*/ get_toplevel_windows()() nothrow {
      return gdk_screen_get_toplevel_windows(&this);
   }

   // VERSION: 2.2
   // Gets the width of @screen in pixels
   // RETURNS: the width of @screen in pixels.
   int get_width()() nothrow {
      return gdk_screen_get_width(&this);
   }

   // VERSION: 2.2
   // Gets the width of @screen in millimeters. 
   // Note that on some X servers this value will not be correct.
   // RETURNS: the width of @screen in millimeters.
   int get_width_mm()() nothrow {
      return gdk_screen_get_width_mm(&this);
   }

   // VERSION: 2.10
   // Returns a #GList of #GdkWindow<!-- -->s representing the current
   // window stack.
   // 
   // On X11, this is done by inspecting the _NET_CLIENT_LIST_STACKING
   // property on the root window, as described in the <ulink
   // url="http://www.freedesktop.org/Standards/wm-spec">Extended Window
   // Manager Hints</ulink>. If the window manager does not support the
   // _NET_CLIENT_LIST_STACKING hint, this function returns %NULL.
   // 
   // On other platforms, this function may return %NULL, depending on whether
   // it is implementable on that platform.
   // 
   // The returned list is newly allocated and owns references to the
   // windows it contains, so it should be freed using g_list_free() and
   // its windows unrefed using g_object_unref() when no longer needed.
   // 
   // a list of #GdkWindow<!-- -->s for the current window stack,
   // or %NULL.
   GLib2.List* /*new*/ get_window_stack()() nothrow {
      return gdk_screen_get_window_stack(&this);
   }

   // VERSION: 2.10
   // Returns whether windows with an RGBA visual can reasonably
   // be expected to have their alpha channel drawn correctly on
   // the screen.
   // 
   // On X11 this function returns whether a compositing manager is
   // compositing @screen.
   // 
   // expected to have their alpha channels drawn correctly on the screen.
   // RETURNS: Whether windows with RGBA visuals can reasonably be
   int is_composited()() nothrow {
      return gdk_screen_is_composited(&this);
   }

   // VERSION: 2.2
   // Lists the available visuals for the specified @screen.
   // A visual describes a hardware image data format.
   // For example, a visual might support 24-bit color, or 8-bit color,
   // and might expect pixels to be in a certain format.
   // 
   // Call g_list_free() on the return value when you're finished with it.
   // 
   // a list of visuals; the list must be freed, but not its contents
   GLib2.List* /*new container*/ list_visuals()() nothrow {
      return gdk_screen_list_visuals(&this);
   }

   // VERSION: 2.2
   // Determines the name to pass to gdk_display_open() to get
   // a #GdkDisplay with this screen as the default screen.
   // RETURNS: a newly allocated string, free with g_free()
   char* /*new*/ make_display_name()() nothrow {
      return gdk_screen_make_display_name(&this);
   }

   // VERSION: 2.2
   // Sets the default @colormap for @screen.
   // <colormap>: a #GdkColormap
   void set_default_colormap(AT0)(AT0 /*Colormap*/ colormap) nothrow {
      gdk_screen_set_default_colormap(&this, UpCast!(Colormap*)(colormap));
   }

   // VERSION: 2.10
   // Sets the default font options for the screen. These
   // options will be set on any #PangoContext's newly created
   // with gdk_pango_context_get_for_screen(). Changing the
   // default set of font options does not affect contexts that
   // have already been created.
   // <options>: a #cairo_font_options_t, or %NULL to unset any previously set default font options.
   void set_font_options(AT0)(AT0 /*cairo.FontOptions*/ options=null) nothrow {
      gdk_screen_set_font_options(&this, UpCast!(cairo.FontOptions*)(options));
   }

   // VERSION: 2.10
   // 
   // <dpi>: the resolution in "dots per inch". (Physical inches aren't actually involved; the terminology is conventional.) Sets the resolution for font handling on the screen. This is a scale factor between points specified in a #PangoFontDescription and cairo units. The default value is 96, meaning that a 10 point font will be 13 units high. (10 * 96. / 72. = 13.3).
   void set_resolution()(double dpi) nothrow {
      gdk_screen_set_resolution(&this, dpi);
   }

   // VERSION: 2.10
   // The ::composited-changed signal is emitted when the composited
   // status of the screen changes
   extern (C) alias static void function (Screen* this_, void* user_data=null) nothrow signal_composited_changed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"composited-changed", CB/*:signal_composited_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_composited_changed)||_ttmm!(CB, signal_composited_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"composited-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 2.14
   // The ::monitors-changed signal is emitted when the number, size
   // or position of the monitors attached to the screen change. 
   // 
   // Only for X11 and OS X for now. A future implementation for Win32
   // may be a possibility.
   extern (C) alias static void function (Screen* this_, void* user_data=null) nothrow signal_monitors_changed;
   ulong signal_connect(string name:"monitors-changed", CB/*:signal_monitors_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_monitors_changed)||_ttmm!(CB, signal_monitors_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"monitors-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 2.2
   // The ::size-changed signal is emitted when the pixel width or 
   // height of a screen changes.
   extern (C) alias static void function (Screen* this_, void* user_data=null) nothrow signal_size_changed;
   ulong signal_connect(string name:"size-changed", CB/*:signal_size_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_size_changed)||_ttmm!(CB, signal_size_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"size-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct ScreenClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function (Screen* screen) nothrow size_changed;
   extern (C) void function (Screen* screen) nothrow composited_changed;
   extern (C) void function (Screen* screen) nothrow monitors_changed;
}

enum ScrollDirection {
   UP = 0,
   DOWN = 1,
   LEFT = 2,
   RIGHT = 3
}
struct Segment {
   int x1, y1, x2, y2;
}

enum SettingAction {
   NEW = 0,
   CHANGED = 1,
   DELETED = 2
}
struct Span {
   int x, y, width;
}

extern (C) alias void function (Span* span, void* data) nothrow SpanFunc;

enum Status {
   OK = 0,
   ERROR = -1,
   ERROR_PARAM = -2,
   ERROR_FILE = -3,
   ERROR_MEM = -4
}
enum SubwindowMode {
   CLIP_BY_CHILDREN = 0,
   INCLUDE_INFERIORS = 1
}
struct TimeCoord {
   uint time;
   double[128] axes;
}

struct Trapezoid {
   double y1, x11, x21, y2, x12, x22;
}

enum VisibilityState {
   UNOBSCURED = 0,
   PARTIAL = 1,
   FULLY_OBSCURED = 2
}
struct Visual /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   VisualType type;
   int depth;
   ByteOrder byte_order;
   int colormap_size, bits_per_rgb;
   uint red_mask;
   int red_shift, red_prec;
   uint green_mask;
   int green_shift, green_prec;
   uint blue_mask;
   int blue_shift, blue_prec;


   // Get the visual with the most available colors for the default
   // GDK screen. The return value should not be freed.
   // RETURNS: best visual
   static Visual* get_best()() nothrow {
      return gdk_visual_get_best();
   }

   // Get the best available depth for the default GDK screen.  "Best"
   // means "largest," i.e. 32 preferred over 24 preferred over 8 bits
   // per pixel.
   // RETURNS: best available depth
   static int get_best_depth()() nothrow {
      return gdk_visual_get_best_depth();
   }

   // Return the best available visual type for the default GDK screen.
   // RETURNS: best visual type
   static VisualType get_best_type()() nothrow {
      return gdk_visual_get_best_type();
   }

   // Combines gdk_visual_get_best_with_depth() and gdk_visual_get_best_with_type().
   // 
   // @visual_type, or %NULL if none
   // RETURNS: best visual with both @depth and
   // <depth>: a bit depth
   // <visual_type>: a visual type
   static Visual* get_best_with_both()(int depth, VisualType visual_type) nothrow {
      return gdk_visual_get_best_with_both(depth, visual_type);
   }

   // Get the best visual with depth @depth for the default GDK screen.
   // Color visuals and visuals with mutable colormaps are preferred
   // over grayscale or fixed-colormap visuals. The return value should not
   // be freed. %NULL may be returned if no visual supports @depth.
   // RETURNS: best visual for the given depth
   // <depth>: a bit depth
   static Visual* get_best_with_depth()(int depth) nothrow {
      return gdk_visual_get_best_with_depth(depth);
   }

   // Get the best visual of the given @visual_type for the default GDK screen.
   // Visuals with higher color depths are considered better. The return value
   // should not be freed. %NULL may be returned if no visual has type
   // @visual_type.
   // RETURNS: best visual of the given type
   // <visual_type>: a visual type
   static Visual* get_best_with_type()(VisualType visual_type) nothrow {
      return gdk_visual_get_best_with_type(visual_type);
   }

   // Get the system's default visual for the default GDK screen.
   // This is the visual for the root window of the display.
   // The return value should not be freed.
   // RETURNS: system visual
   static Visual* get_system()() nothrow {
      return gdk_visual_get_system();
   }

   // VERSION: 2.22
   // Returns the number of significant bits per red, green and blue value.
   // RETURNS: The number of significant bits per color value for @visual.
   int get_bits_per_rgb()() nothrow {
      return gdk_visual_get_bits_per_rgb(&this);
   }

   // VERSION: 2.22
   // Obtains values that are needed to calculate blue pixel values in TrueColor
   // and DirectColor.  The "mask" is the significant bits within the pixel.
   // The "shift" is the number of bits left we must shift a primary for it
   // to be in position (according to the "mask").  Finally, "precision" refers
   // to how much precision the pixel value contains for a particular primary.
   // <mask>: A pointer to a #guint32 to be filled in, or %NULL.
   // <shift>: A pointer to a #gint to be filled in, or %NULL.
   // <precision>: A pointer to a #gint to be filled in, or %NULL.
   void get_blue_pixel_details(AT0)(/*out*/ AT0 /*uint*/ mask=null, /*out*/ int* shift=null, /*out*/ int* precision=null) nothrow {
      gdk_visual_get_blue_pixel_details(&this, UpCast!(uint*)(mask), shift, precision);
   }

   // VERSION: 2.22
   // Returns the byte order of this visual.
   // RETURNS: A #GdkByteOrder stating the byte order of @visual.
   ByteOrder get_byte_order()() nothrow {
      return gdk_visual_get_byte_order(&this);
   }

   // VERSION: 2.22
   // Returns the size of a colormap for this visual.
   // RETURNS: The size of a colormap that is suitable for @visual.
   int get_colormap_size()() nothrow {
      return gdk_visual_get_colormap_size(&this);
   }

   // VERSION: 2.22
   // Returns the bit depth of this visual.
   // RETURNS: The bit depth of this visual.
   int get_depth()() nothrow {
      return gdk_visual_get_depth(&this);
   }

   // VERSION: 2.22
   // Obtains values that are needed to calculate green pixel values in TrueColor
   // and DirectColor.  The "mask" is the significant bits within the pixel.
   // The "shift" is the number of bits left we must shift a primary for it
   // to be in position (according to the "mask").  Finally, "precision" refers
   // to how much precision the pixel value contains for a particular primary.
   // <mask>: A pointer to a #guint32 to be filled in, or %NULL.
   // <shift>: A pointer to a #gint to be filled in, or %NULL.
   // <precision>: A pointer to a #gint to be filled in, or %NULL.
   void get_green_pixel_details(AT0)(/*out*/ AT0 /*uint*/ mask=null, /*out*/ int* shift=null, /*out*/ int* precision=null) nothrow {
      gdk_visual_get_green_pixel_details(&this, UpCast!(uint*)(mask), shift, precision);
   }

   // VERSION: 2.22
   // Obtains values that are needed to calculate red pixel values in TrueColor
   // and DirectColor.  The "mask" is the significant bits within the pixel.
   // The "shift" is the number of bits left we must shift a primary for it
   // to be in position (according to the "mask").  Finally, "precision" refers
   // to how much precision the pixel value contains for a particular primary.
   // <mask>: A pointer to a #guint32 to be filled in, or %NULL.
   // <shift>: A pointer to a #gint to be filled in, or %NULL.
   // <precision>: A pointer to a #gint to be filled in, or %NULL.
   void get_red_pixel_details(AT0)(/*out*/ AT0 /*uint*/ mask=null, /*out*/ int* shift=null, /*out*/ int* precision=null) nothrow {
      gdk_visual_get_red_pixel_details(&this, UpCast!(uint*)(mask), shift, precision);
   }

   // VERSION: 2.2
   // Gets the screen to which this visual belongs
   // RETURNS: the screen to which this visual belongs.
   Screen* get_screen()() nothrow {
      return gdk_visual_get_screen(&this);
   }

   // VERSION: 2.22
   // Returns the type of visual this is (PseudoColor, TrueColor, etc).
   // RETURNS: A #GdkVisualType stating the type of @visual.
   VisualType get_visual_type()() nothrow {
      return gdk_visual_get_visual_type(&this);
   }
}

struct VisualClass {
}

enum VisualType {
   STATIC_GRAY = 0,
   GRAYSCALE = 1,
   STATIC_COLOR = 2,
   PSEUDO_COLOR = 3,
   TRUE_COLOR = 4,
   DIRECT_COLOR = 5
}
enum WMDecoration {
   ALL = 1,
   BORDER = 2,
   RESIZEH = 4,
   TITLE = 8,
   MENU = 16,
   MINIMIZE = 32,
   MAXIMIZE = 64
}
enum WMFunction {
   ALL = 1,
   RESIZE = 2,
   MOVE = 4,
   MINIMIZE = 8,
   MAXIMIZE = 16,
   CLOSE = 32
}
struct Window /* : Drawable */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent drawable;
   Drawable method_parent;


   // Unintrospectable method: add_filter() / gdk_window_add_filter()
   // Adds an event filter to @window, allowing you to intercept events
   // before they reach GDK. This is a low-level operation and makes it
   // easy to break GDK and/or GTK+, so you have to know what you're
   // doing. Pass %NULL for @window to get all events for all windows,
   // instead of events for a specific window.
   // 
   // See gdk_display_add_client_message_filter() if you are interested
   // in X ClientMessage events.
   // <function>: filter callback
   // <data>: data to pass to filter callback
   void add_filter(AT0)(FilterFunc function_, AT0 /*void*/ data) nothrow {
      gdk_window_add_filter(&this, function_, UpCast!(void*)(data));
   }

   // VERSION: 2.12
   // Emits a short beep associated to @window in the appropriate
   // display, if supported. Otherwise, emits a short beep on
   // the display just as gdk_display_beep().
   void beep()() nothrow {
      gdk_window_beep(&this);
   }

   // Begins a window move operation (for a toplevel window).  You might
   // use this function to implement a "window move grip," for
   // example. The function works best with window managers that support
   // the <ulink url="http://www.freedesktop.org/Standards/wm-spec">Extended 
   // Window Manager Hints</ulink>, but has a fallback implementation for
   // other window managers.
   // <button>: the button being used to drag
   // <root_x>: root window X coordinate of mouse click that began the drag
   // <root_y>: root window Y coordinate of mouse click that began the drag
   // <timestamp>: timestamp of mouse click that began the drag
   void begin_move_drag()(int button, int root_x, int root_y, uint timestamp) nothrow {
      gdk_window_begin_move_drag(&this, button, root_x, root_y, timestamp);
   }

   // A convenience wrapper around gdk_window_begin_paint_region() which
   // creates a rectangular region for you. See
   // gdk_window_begin_paint_region() for details.
   // <rectangle>: rectangle you intend to draw to
   void begin_paint_rect(AT0)(AT0 /*Rectangle*/ rectangle) nothrow {
      gdk_window_begin_paint_rect(&this, UpCast!(Rectangle*)(rectangle));
   }

   // Indicates that you are beginning the process of redrawing @region.
   // A backing store (offscreen buffer) large enough to contain @region
   // will be created. The backing store will be initialized with the
   // background color or background pixmap for @window. Then, all
   // drawing operations performed on @window will be diverted to the
   // backing store.  When you call gdk_window_end_paint(), the backing
   // store will be copied to @window, making it visible onscreen. Only
   // the part of @window contained in @region will be modified; that is,
   // drawing operations are clipped to @region.
   // 
   // The net result of all this is to remove flicker, because the user
   // sees the finished product appear all at once when you call
   // gdk_window_end_paint(). If you draw to @window directly without
   // calling gdk_window_begin_paint_region(), the user may see flicker
   // as individual drawing operations are performed in sequence.  The
   // clipping and background-initializing features of
   // gdk_window_begin_paint_region() are conveniences for the
   // programmer, so you can avoid doing that work yourself.
   // 
   // When using GTK+, the widget system automatically places calls to
   // gdk_window_begin_paint_region() and gdk_window_end_paint() around
   // emissions of the expose_event signal. That is, if you're writing an
   // expose event handler, you can assume that the exposed area in
   // #GdkEventExpose has already been cleared to the window background,
   // is already set as the clip region, and already has a backing store.
   // Therefore in most cases, application code need not call
   // gdk_window_begin_paint_region(). (You can disable the automatic
   // calls around expose events on a widget-by-widget basis by calling
   // gtk_widget_set_double_buffered().)
   // 
   // If you call this function multiple times before calling the
   // matching gdk_window_end_paint(), the backing stores are pushed onto
   // a stack. gdk_window_end_paint() copies the topmost backing store
   // onscreen, subtracts the topmost region from all other regions in
   // the stack, and pops the stack. All drawing operations affect only
   // the topmost backing store in the stack. One matching call to
   // gdk_window_end_paint() is required for each call to
   // gdk_window_begin_paint_region().
   // <region>: region you intend to draw to
   void begin_paint_region(AT0)(AT0 /*Region*/ region) nothrow {
      gdk_window_begin_paint_region(&this, UpCast!(Region*)(region));
   }

   // Begins a window resize operation (for a toplevel window).
   // You might use this function to implement a "window resize grip," for
   // example; in fact #GtkStatusbar uses it. The function works best
   // with window managers that support the <ulink url="http://www.freedesktop.org/Standards/wm-spec">Extended Window Manager Hints</ulink>, but has a 
   // fallback implementation for other window managers.
   // <edge>: the edge or corner from which the drag is started
   // <button>: the button being used to drag
   // <root_x>: root window X coordinate of mouse click that began the drag
   // <root_y>: root window Y coordinate of mouse click that began the drag
   // <timestamp>: timestamp of mouse click that began the drag (use gdk_event_get_time())
   void begin_resize_drag()(WindowEdge edge, int button, int root_x, int root_y, uint timestamp) nothrow {
      gdk_window_begin_resize_drag(&this, edge, button, root_x, root_y, timestamp);
   }
   // Clears an entire @window to the background color or background pixmap.
   void clear()() nothrow {
      gdk_window_clear(&this);
   }

   // Clears an area of @window to the background color or background pixmap.
   // <x>: x coordinate of rectangle to clear
   // <y>: y coordinate of rectangle to clear
   // <width>: width of rectangle to clear
   // <height>: height of rectangle to clear
   void clear_area()(int x, int y, int width, int height) nothrow {
      gdk_window_clear_area(&this, x, y, width, height);
   }

   // Like gdk_window_clear_area(), but also generates an expose event for
   // the cleared area.
   // 
   // This function has a stupid name because it dates back to the mists
   // time, pre-GDK-1.0.
   // <x>: x coordinate of rectangle to clear
   // <y>: y coordinate of rectangle to clear
   // <width>: width of rectangle to clear
   // <height>: height of rectangle to clear
   void clear_area_e()(int x, int y, int width, int height) nothrow {
      gdk_window_clear_area_e(&this, x, y, width, height);
   }

   // VERSION: 2.6
   // Signal to the window system that the application has finished
   // handling Configure events it has received. Window Managers can
   // use this to better synchronize the frame repaint with the
   // application. GTK+ applications will automatically call this
   // function when appropriate.
   // 
   // This function can only be called if gdk_window_enable_synchronized_configure()
   // was called previously.
   void configure_finished()() nothrow {
      gdk_window_configure_finished(&this);
   }

   // VERSION: 2.22
   // Transforms window coordinates from a parent window to a child
   // window, where the parent window is the normal parent as returned by
   // gdk_window_get_parent() for normal windows, and the window's
   // embedder as returned by gdk_offscreen_window_get_embedder() for
   // offscreen windows.
   // 
   // For normal windows, calling this function is equivalent to subtracting
   // the return values of gdk_window_get_position() from the parent coordinates.
   // For offscreen windows however (which can be arbitrarily transformed),
   // this function calls the GdkWindow::from-embedder: signal to translate
   // the coordinates.
   // 
   // You should always use this function when writing generic code that
   // walks down a window hierarchy.
   // 
   // See also: gdk_window_coords_to_parent()
   // <parent_x>: X coordinate in parent's coordinate system
   // <parent_y>: Y coordinate in parent's coordinate system
   // <x>: return location for X coordinate in child's coordinate system
   // <y>: return location for Y coordinate in child's coordinate system
   void coords_from_parent(AT0, AT1)(double parent_x, double parent_y, /*out*/ AT0 /*double*/ x=null, /*out*/ AT1 /*double*/ y=null) nothrow {
      gdk_window_coords_from_parent(&this, parent_x, parent_y, UpCast!(double*)(x), UpCast!(double*)(y));
   }

   // VERSION: 2.22
   // Transforms window coordinates from a child window to its parent
   // window, where the parent window is the normal parent as returned by
   // gdk_window_get_parent() for normal windows, and the window's
   // embedder as returned by gdk_offscreen_window_get_embedder() for
   // offscreen windows.
   // 
   // For normal windows, calling this function is equivalent to adding
   // the return values of gdk_window_get_position() to the child coordinates.
   // For offscreen windows however (which can be arbitrarily transformed),
   // this function calls the GdkWindow::to-embedder: signal to translate
   // the coordinates.
   // 
   // You should always use this function when writing generic code that
   // walks up a window hierarchy.
   // 
   // See also: gdk_window_coords_from_parent()
   // <x>: X coordinate in child's coordinate system
   // <y>: Y coordinate in child's coordinate system
   // <parent_x>: return location for X coordinate in parent's coordinate system, or %NULL
   // <parent_y>: return location for Y coordinate in parent's coordinate system, or %NULL
   void coords_to_parent(AT0, AT1)(double x, double y, /*out*/ AT0 /*double*/ parent_x=null, /*out*/ AT1 /*double*/ parent_y=null) nothrow {
      gdk_window_coords_to_parent(&this, x, y, UpCast!(double*)(parent_x), UpCast!(double*)(parent_y));
   }

   // VERSION: 2.22
   // Create a new surface that is as compatible as possible with the
   // given @window. For example the new surface will have the same
   // fallback resolution and font options as @window. Generally, the new
   // surface will also use the same backend as @window, unless that is
   // not possible for some reason. The type of the returned surface may
   // be examined with cairo_surface_get_type().
   // 
   // Initially the surface contents are all 0 (transparent if contents
   // have transparency, black otherwise.)
   // 
   // owns the surface and should call cairo_surface_destroy() when done
   // with it.
   // 
   // This function always returns a valid pointer, but it will return a
   // pointer to a "nil" surface if @other is already in an error state
   // or any other error occurs.
   // RETURNS: a pointer to the newly allocated surface. The caller
   // <content>: the content for the new surface
   // <width>: width of the new surface
   // <height>: height of the new surface
   cairo.Surface* /*new*/ create_similar_surface()(cairo.Content content, int width, int height) nothrow {
      return gdk_window_create_similar_surface(&this, content, width, height);
   }

   // Attempt to deiconify (unminimize) @window. On X11 the window manager may
   // choose to ignore the request to deiconify. When using GTK+,
   // use gtk_window_deiconify() instead of the #GdkWindow variant. Or better yet,
   // you probably want to use gtk_window_present(), which raises the window, focuses it,
   // unminimizes it, and puts it on the current desktop.
   void deiconify()() nothrow {
      gdk_window_deiconify(&this);
   }

   // Destroys the window system resources associated with @window and decrements @window's
   // reference count. The window system resources for all children of @window are also
   // destroyed, but the children's reference counts are not decremented.
   // 
   // Note that a window will not be destroyed automatically when its reference count
   // reaches zero. You must call this function yourself before that happens.
   void destroy()() nothrow {
      gdk_window_destroy(&this);
   }
   void destroy_notify()() nothrow {
      gdk_window_destroy_notify(&this);
   }

   // VERSION: 2.6
   // Indicates that the application will cooperate with the window
   // system in synchronizing the window repaint with the window
   // manager during resizing operations. After an application calls
   // this function, it must call gdk_window_configure_finished() every
   // time it has finished all processing associated with a set of
   // Configure events. Toplevel GTK+ windows automatically use this
   // protocol.
   // 
   // On X, calling this function makes @window participate in the
   // _NET_WM_SYNC_REQUEST window manager protocol.
   void enable_synchronized_configure()() nothrow {
      gdk_window_enable_synchronized_configure(&this);
   }

   // Indicates that the backing store created by the most recent call to
   // gdk_window_begin_paint_region() should be copied onscreen and
   // deleted, leaving the next-most-recent backing store or no backing
   // store at all as the active paint region. See
   // gdk_window_begin_paint_region() for full details. It is an error to
   // call this function without a matching
   // gdk_window_begin_paint_region() first.
   void end_paint()() nothrow {
      gdk_window_end_paint(&this);
   }

   // VERSION: 2.18
   // Tries to ensure that there is a window-system native window for this
   // GdkWindow. This may fail in some situations, returning %FALSE.
   // 
   // Offscreen window and children of them can never have native windows.
   // 
   // Some backends may not support native child windows.
   // RETURNS: %TRUE if the window has a native window, %FALSE otherwise
   int ensure_native()() nothrow {
      return gdk_window_ensure_native(&this);
   }

   // VERSION: 2.18
   // Flush all outstanding cached operations on a window, leaving the
   // window in a state which reflects all that has been drawn before.
   // 
   // Gdk uses multiple kinds of caching to get better performance and
   // nicer drawing. For instance, during exposes all paints to a window
   // using double buffered rendering are keep on a pixmap until the last
   // window has been exposed. It also delays window moves/scrolls until
   // as long as possible until next update to avoid tearing when moving
   // windows.
   // 
   // Normally this should be completely invisible to applications, as
   // we automatically flush the windows when required, but this might
   // be needed if you for instance mix direct native drawing with
   // gdk drawing. For Gtk widgets that don't use double buffering this
   // will be called automatically before sending the expose event.
   void flush()() nothrow {
      gdk_window_flush(&this);
   }

   // Sets keyboard focus to @window. In most cases, gtk_window_present() 
   // should be used on a #GtkWindow, rather than calling this function.
   // <timestamp>: timestamp of the event triggering the window focus
   void focus()(uint timestamp) nothrow {
      gdk_window_focus(&this, timestamp);
   }

   // Temporarily freezes a window and all its descendants such that it won't
   // receive expose events.  The window will begin receiving expose events
   // again when gdk_window_thaw_toplevel_updates_libgtk_only() is called. If
   // gdk_window_freeze_toplevel_updates_libgtk_only()
   // has been called more than once,
   // gdk_window_thaw_toplevel_updates_libgtk_only() must be called
   // an equal number of times to begin processing exposes.
   // 
   // This function is not part of the GDK public API and is only
   // for use by GTK+.
   void freeze_toplevel_updates_libgtk_only()() nothrow {
      gdk_window_freeze_toplevel_updates_libgtk_only(&this);
   }

   // Temporarily freezes a window such that it won't receive expose
   // events.  The window will begin receiving expose events again when
   // gdk_window_thaw_updates() is called. If gdk_window_freeze_updates()
   // has been called more than once, gdk_window_thaw_updates() must be called
   // an equal number of times to begin processing exposes.
   void freeze_updates()() nothrow {
      gdk_window_freeze_updates(&this);
   }

   // VERSION: 2.2
   // Moves the window into fullscreen mode. This means the
   // window covers the entire screen and is above any panels
   // or task bars.
   // 
   // If the window was already fullscreen, then this function does nothing.
   // 
   // On X11, asks the window manager to put @window in a fullscreen
   // state, if the window manager supports this operation. Not all
   // window managers support this, and some deliberately ignore it or
   // don't have a concept of "fullscreen"; so you can't rely on the
   // fullscreenification actually happening. But it will happen with
   // most standard window managers, and GDK makes a best effort to get
   // it to happen.
   void fullscreen()() nothrow {
      gdk_window_fullscreen(&this);
   }

   // VERSION: 2.18
   // This function informs GDK that the geometry of an embedded
   // offscreen window has changed. This is necessary for GDK to keep
   // track of which offscreen window the pointer is in.
   void geometry_changed()() nothrow {
      gdk_window_geometry_changed(&this);
   }

   // VERSION: 2.22
   // Determines whether or not the desktop environment shuld be hinted that
   // the window does not want to receive input focus.
   // RETURNS: whether or not the window should receive input focus.
   int get_accept_focus()() nothrow {
      return gdk_window_get_accept_focus(&this);
   }

   // VERSION: 2.22
   // Gets the pattern used to clear the background on @window. If @window
   // does not have its own background and reuses the parent's, %NULL is
   // returned and you'll have to query it yourself.
   // 
   // %NULL to use the parent's background.
   // RETURNS: The pattern to use for the background or
   cairo.Pattern* get_background_pattern()() nothrow {
      return gdk_window_get_background_pattern(&this);
   }

   // Gets the list of children of @window known to GDK.
   // This function only returns children created via GDK,
   // so for example it's useless when used with the root window;
   // it only returns windows an application created itself.
   // 
   // The returned list must be freed, but the elements in the
   // list need not be.
   // 
   // list of child windows inside @window
   GLib2.List* /*new container*/ get_children()() nothrow {
      return gdk_window_get_children(&this);
   }

   // VERSION: 2.22
   // Determines whether @window is composited.
   // 
   // See gdk_window_set_composited().
   // RETURNS: %TRUE if the window is composited.
   int get_composited()() nothrow {
      return gdk_window_get_composited(&this);
   }

   // VERSION: 2.18
   // Retrieves a #GdkCursor pointer for the cursor currently set on the
   // specified #GdkWindow, or %NULL.  If the return value is %NULL then
   // there is no custom cursor set on the specified window, and it is
   // using the cursor for its parent window.
   // 
   // object is owned by the #GdkWindow and should not be unreferenced
   // directly. Use gdk_window_set_cursor() to unset the cursor of the
   // window
   // RETURNS: a #GdkCursor, or %NULL. The returned
   Cursor* get_cursor()() nothrow {
      return gdk_window_get_cursor(&this);
   }

   // Returns the decorations set on the GdkWindow with #gdk_window_set_decorations
   // RETURNS: TRUE if the window has decorations set, FALSE otherwise.
   // <decorations>: The window decorations will be written here
   int get_decorations(AT0)(AT0 /*WMDecoration*/ decorations) nothrow {
      return gdk_window_get_decorations(&this, UpCast!(WMDecoration*)(decorations));
   }

   // This gets the origin of a #GdkWindow relative to
   // an Enlightenment-window-manager desktop. As long as you don't
   // assume that the user's desktop/workspace covers the entire
   // root window (i.e. you don't assume that the desktop begins
   // at root window coordinate 0,0) this function is not necessary.
   // It's deprecated for that reason.
   // RETURNS: not meaningful
   // <x>: return location for X coordinate
   // <y>: return location for Y coordinate
   int get_deskrelative_origin()(int* x, int* y) nothrow {
      return gdk_window_get_deskrelative_origin(&this, x, y);
   }

   // Unintrospectable method: get_display() / gdk_window_get_display()
   // VERSION: 2.24
   // Gets the #GdkDisplay associated with a #GdkWindow.
   // RETURNS: the #GdkDisplay associated with @window
   Display* get_display()() nothrow {
      return gdk_window_get_display(&this);
   }

   // Unintrospectable method: get_effective_parent() / gdk_window_get_effective_parent()
   // VERSION: 2.22
   // Obtains the parent of @window, as known to GDK. Works like
   // gdk_window_get_parent() for normal windows, but returns the
   // window's embedder for offscreen windows.
   // 
   // See also: gdk_offscreen_window_get_embedder()
   // RETURNS: effective parent of @window
   Window* get_effective_parent()() nothrow {
      return gdk_window_get_effective_parent(&this);
   }

   // Unintrospectable method: get_effective_toplevel() / gdk_window_get_effective_toplevel()
   // VERSION: 2.22
   // Gets the toplevel window that's an ancestor of @window.
   // 
   // Works like gdk_window_get_toplevel(), but treats an offscreen window's
   // embedder as its parent, using gdk_window_get_effective_parent().
   // 
   // See also: gdk_offscreen_window_get_embedder()
   // RETURNS: the effective toplevel window containing @window
   Window* get_effective_toplevel()() nothrow {
      return gdk_window_get_effective_toplevel(&this);
   }

   // Gets the event mask for @window. See gdk_window_set_events().
   // RETURNS: event mask for @window
   EventMask get_events()() nothrow {
      return gdk_window_get_events(&this);
   }

   // VERSION: 2.22
   // Determines whether or not the desktop environment should be hinted that the
   // window does not want to receive input focus when it is mapped.
   // 
   // it is mapped.
   // RETURNS: whether or not the window wants to receive input focus when
   int get_focus_on_map()() nothrow {
      return gdk_window_get_focus_on_map(&this);
   }

   // Obtains the bounding box of the window, including window manager
   // titlebar/borders if any. The frame position is given in root window
   // coordinates. To get the position of the window itself (rather than
   // the frame) in root window coordinates, use gdk_window_get_origin().
   // <rect>: rectangle to fill with bounding box of the window frame
   void get_frame_extents(AT0)(AT0 /*Rectangle*/ rect) nothrow {
      gdk_window_get_frame_extents(&this, UpCast!(Rectangle*)(rect));
   }

   // Any of the return location arguments to this function may be %NULL,
   // if you aren't interested in getting the value of that field.
   // 
   // The X and Y coordinates returned are relative to the parent window
   // of @window, which for toplevels usually means relative to the
   // window decorations (titlebar, etc.) rather than relative to the
   // root window (screen-size background window).
   // 
   // On the X11 platform, the geometry is obtained from the X server,
   // so reflects the latest position of @window; this may be out-of-sync
   // with the position of @window delivered in the most-recently-processed
   // #GdkEventConfigure. gdk_window_get_position() in contrast gets the
   // position from the most recent configure event.
   // 
   // <note>
   // If @window is not a toplevel, it is <emphasis>much</emphasis> better
   // to call gdk_window_get_position() and gdk_drawable_get_size() instead,
   // because it avoids the roundtrip to the X server and because
   // gdk_drawable_get_size() supports the full 32-bit coordinate space,
   // whereas gdk_window_get_geometry() is restricted to the 16-bit
   // coordinates of X11.
   // </note>
   // <x>: return location for X coordinate of window (relative to its parent)
   // <y>: return location for Y coordinate of window (relative to its parent)
   // <width>: return location for width of window
   // <height>: return location for height of window
   // <depth>: return location for bit depth of window
   void get_geometry()(int* x, int* y, int* width, int* height, int* depth) nothrow {
      gdk_window_get_geometry(&this, x, y, width, height, depth);
   }

   // Unintrospectable method: get_group() / gdk_window_get_group()
   // VERSION: 2.4
   // Returns the group leader window for @window. See gdk_window_set_group().
   // RETURNS: the group leader window for @window
   Window* get_group()() nothrow {
      return gdk_window_get_group(&this);
   }

   // VERSION: 2.24
   // Returns the height of the given @window.
   // 
   // On the X11 platform the returned size is the size reported in the
   // most-recently-processed configure event, rather than the current
   // size on the X server.
   // RETURNS: The height of @window
   int get_height()() nothrow {
      return gdk_window_get_height(&this);
   }

   // If you bypass the GDK layer and use windowing system primitives to
   // draw directly onto a #GdkWindow, then you need to deal with two
   // details: there may be an offset between GDK coordinates and windowing
   // system coordinates, and GDK may have redirected drawing to a offscreen
   // pixmap as the result of a gdk_window_begin_paint_region() calls.
   // This function allows retrieving the information you need to compensate
   // for these effects.
   // 
   // This function exposes details of the GDK implementation, and is thus
   // likely to change in future releases of GDK.
   // <real_drawable>: location to store the drawable to which drawing should be done.
   // <x_offset>: location to store the X offset between coordinates in @window, and the underlying window system primitive coordinates for *@real_drawable.
   // <y_offset>: location to store the Y offset between coordinates in @window, and the underlying window system primitive coordinates for *@real_drawable.
   void get_internal_paint_info(AT0)(/*out*/ AT0 /*Drawable**/ real_drawable, /*out*/ int* x_offset, /*out*/ int* y_offset) nothrow {
      gdk_window_get_internal_paint_info(&this, UpCast!(Drawable**)(real_drawable), x_offset, y_offset);
   }

   // VERSION: 2.22
   // Determines whether or not the window manager is hinted that @window
   // has modal behaviour.
   // RETURNS: whether or not the window has the modal hint set.
   int get_modal_hint()() nothrow {
      return gdk_window_get_modal_hint(&this);
   }

   // Obtains the position of a window in root window coordinates.
   // (Compare with gdk_window_get_position() and
   // gdk_window_get_geometry() which return the position of a window
   // relative to its parent window.)
   // RETURNS: not meaningful, ignore
   // <x>: return location for X coordinate
   // <y>: return location for Y coordinate
   int get_origin()(int* x, int* y) nothrow {
      return gdk_window_get_origin(&this, x, y);
   }

   // Unintrospectable method: get_parent() / gdk_window_get_parent()
   // Obtains the parent of @window, as known to GDK. Does not query the
   // X server; thus this returns the parent as passed to gdk_window_new(),
   // not the actual parent. This should never matter unless you're using
   // Xlib calls mixed with GDK calls on the X11 platform. It may also
   // matter for toplevel windows, because the window manager may choose
   // to reparent them.
   // 
   // Note that you should use gdk_window_get_effective_parent() when
   // writing generic code that walks up a window hierarchy, because
   // gdk_window_get_parent() will most likely not do what you expect if
   // there are offscreen windows in the hierarchy.
   // RETURNS: parent of @window
   Window* get_parent()() nothrow {
      return gdk_window_get_parent(&this);
   }

   // Obtains the current pointer position and modifier state.
   // The position is given in coordinates relative to the upper left
   // corner of @window.
   // 
   // gdk_window_at_pointer()), or %NULL if the window containing the
   // pointer isn't known to GDK
   // RETURNS: the window containing the pointer (as with
   // <x>: return location for X coordinate of pointer or %NULL to not return the X coordinate
   // <y>: return location for Y coordinate of pointer or %NULL to not return the Y coordinate
   // <mask>: return location for modifier mask or %NULL to not return the modifier mask
   Window* get_pointer(AT0)(/*out*/ int* x=null, /*out*/ int* y=null, /*out*/ AT0 /*ModifierType*/ mask=null) nothrow {
      return gdk_window_get_pointer(&this, x, y, UpCast!(ModifierType*)(mask));
   }

   // Obtains the position of the window as reported in the
   // most-recently-processed #GdkEventConfigure. Contrast with
   // gdk_window_get_geometry() which queries the X server for the
   // current window position, regardless of which events have been
   // received or processed.
   // 
   // The position coordinates are relative to the window's parent window.
   // <x>: X coordinate of window
   // <y>: Y coordinate of window
   void get_position()(/*out*/ int* x=null, /*out*/ int* y=null) nothrow {
      gdk_window_get_position(&this, x, y);
   }

   // VERSION: 2.18
   // Obtains the position of a window position in root
   // window coordinates. This is similar to
   // gdk_window_get_origin() but allows you go pass
   // in any position in the window, not just the origin.
   // <x>: X coordinate in window
   // <y>: Y coordinate in window
   // <root_x>: return location for X coordinate
   // <root_y>: return location for Y coordinate
   void get_root_coords()(int x, int y, /*out*/ int* root_x, /*out*/ int* root_y) nothrow {
      gdk_window_get_root_coords(&this, x, y, root_x, root_y);
   }

   // Obtains the top-left corner of the window manager frame in root
   // window coordinates.
   // <x>: return location for X position of window frame
   // <y>: return location for Y position of window frame
   void get_root_origin()(int* x, int* y) nothrow {
      gdk_window_get_root_origin(&this, x, y);
   }

   // Unintrospectable method: get_screen() / gdk_window_get_screen()
   // Gets the #GdkScreen associated with a #GdkWindow.
   // RETURNS: the #GdkScreen associated with @window
   Screen* get_screen()() nothrow {
      return gdk_window_get_screen(&this);
   }

   // Gets the bitwise OR of the currently active window state flags,
   // from the #GdkWindowState enumeration.
   // RETURNS: window state bitfield
   WindowState get_state()() nothrow {
      return gdk_window_get_state(&this);
   }

   // Unintrospectable method: get_toplevel() / gdk_window_get_toplevel()
   // Gets the toplevel window that's an ancestor of @window.
   // 
   // Any window type but %GDK_WINDOW_CHILD is considered a
   // toplevel window, as is a %GDK_WINDOW_CHILD window that
   // has a root window as parent.
   // 
   // Note that you should use gdk_window_get_effective_toplevel() when
   // you want to get to a window's toplevel as seen on screen, because
   // gdk_window_get_toplevel() will most likely not do what you expect
   // if there are offscreen windows in the hierarchy.
   // RETURNS: the toplevel window containing @window
   Window* get_toplevel()() nothrow {
      return gdk_window_get_toplevel(&this);
   }

   // VERSION: 2.10
   // This function returns the type hint set for a window.
   // RETURNS: The type hint set for @window
   WindowTypeHint get_type_hint()() nothrow {
      return gdk_window_get_type_hint(&this);
   }

   // Unintrospectable method: get_update_area() / gdk_window_get_update_area()
   // Transfers ownership of the update area from @window to the caller
   // of the function. That is, after calling this function, @window will
   // no longer have an invalid/dirty region; the update area is removed
   // from @window and handed to you. If a window has no update area,
   // gdk_window_get_update_area() returns %NULL. You are responsible for
   // calling gdk_region_destroy() on the returned region if it's non-%NULL.
   // RETURNS: the update area for @window
   Region* get_update_area()() nothrow {
      return gdk_window_get_update_area(&this);
   }

   // Retrieves the user data for @window, which is normally the widget
   // that @window belongs to. See gdk_window_set_user_data().
   // <data>: return location for user data
   void get_user_data(AT0)(/*out*/ AT0 /*void**/ data) nothrow {
      gdk_window_get_user_data(&this, UpCast!(void**)(data));
   }

   // Unintrospectable method: get_visual() / gdk_window_get_visual()
   // VERSION: 2.24
   // Gets the #GdkVisual describing the pixel format of @window.
   // RETURNS: a #GdkVisual
   Visual* get_visual()() nothrow {
      return gdk_window_get_visual(&this);
   }

   // VERSION: 2.24
   // Returns the width of the given @window.
   // 
   // On the X11 platform the returned size is the size reported in the
   // most-recently-processed configure event, rather than the current
   // size on the X server.
   // RETURNS: The width of @window
   int get_width()() nothrow {
      return gdk_window_get_width(&this);
   }

   // Gets the type of the window. See #GdkWindowType.
   // RETURNS: type of window
   WindowType get_window_type()() nothrow {
      return gdk_window_get_window_type(&this);
   }

   // VERSION: 2.22
   // Checks whether the window has a native window or not. Note that
   // you can use gdk_window_ensure_native() if a native window is needed.
   // RETURNS: %TRUE if the %window has a native window, %FALSE otherwise.
   int has_native()() nothrow {
      return gdk_window_has_native(&this);
   }

   // For toplevel windows, withdraws them, so they will no longer be
   // known to the window manager; for all windows, unmaps them, so
   // they won't be displayed. Normally done automatically as
   // part of gtk_widget_hide().
   void hide()() nothrow {
      gdk_window_hide(&this);
   }

   // Asks to iconify (minimize) @window. The window manager may choose
   // to ignore the request, but normally will honor it. Using
   // gtk_window_iconify() is preferred, if you have a #GtkWindow widget.
   // 
   // This function only makes sense when @window is a toplevel window.
   void iconify()() nothrow {
      gdk_window_iconify(&this);
   }

   // VERSION: 2.10
   // Like gdk_window_shape_combine_mask(), but the shape applies
   // only to event handling. Mouse events which happen while
   // the pointer position corresponds to an unset bit in the
   // mask will be passed on the window below @window.
   // 
   // An input shape is typically used with RGBA windows.
   // The alpha channel of the window defines which pixels are
   // invisible and allows for nicely antialiased borders,
   // and the input shape controls where the window is
   // "clickable".
   // 
   // On the X11 platform, this requires version 1.1 of the
   // shape extension.
   // 
   // On the Win32 platform, this functionality is not present and the
   // function does nothing.
   // <mask>: shape mask, or %NULL
   // <x>: X position of shape mask with respect to @window
   // <y>: Y position of shape mask with respect to @window
   void input_shape_combine_mask(AT0)(AT0 /*Bitmap*/ mask, int x, int y) nothrow {
      gdk_window_input_shape_combine_mask(&this, UpCast!(Bitmap*)(mask), x, y);
   }

   // VERSION: 2.10
   // Like gdk_window_shape_combine_region(), but the shape applies
   // only to event handling. Mouse events which happen while
   // the pointer position corresponds to an unset bit in the
   // mask will be passed on the window below @window.
   // 
   // An input shape is typically used with RGBA windows.
   // The alpha channel of the window defines which pixels are
   // invisible and allows for nicely antialiased borders,
   // and the input shape controls where the window is
   // "clickable".
   // 
   // On the X11 platform, this requires version 1.1 of the
   // shape extension.
   // 
   // On the Win32 platform, this functionality is not present and the
   // function does nothing.
   // <shape_region>: region of window to be non-transparent
   // <offset_x>: X position of @shape_region in @window coordinates
   // <offset_y>: Y position of @shape_region in @window coordinates
   void input_shape_combine_region(AT0)(AT0 /*Region*/ shape_region, int offset_x, int offset_y) nothrow {
      gdk_window_input_shape_combine_region(&this, UpCast!(Region*)(shape_region), offset_x, offset_y);
   }

   // Adds @region to the update area for @window. The update area is the
   // region that needs to be redrawn, or "dirty region." The call
   // gdk_window_process_updates() sends one or more expose events to the
   // window, which together cover the entire update area. An
   // application would normally redraw the contents of @window in
   // response to those expose events.
   // 
   // GDK will call gdk_window_process_all_updates() on your behalf
   // whenever your program returns to the main loop and becomes idle, so
   // normally there's no need to do that manually, you just need to
   // invalidate regions that you know should be redrawn.
   // 
   // The @child_func parameter controls whether the region of
   // each child window that intersects @region will also be invalidated.
   // Only children for which @child_func returns TRUE will have the area
   // invalidated.
   // <region>: a #GdkRegion
   // <child_func>: function to use to decide if to recurse to a child, %NULL means never recurse.
   // <user_data>: data passed to @child_func
   void invalidate_maybe_recurse(AT0, AT1, AT2)(AT0 /*Region*/ region, AT1 /*void**/ child_func, AT2 /*void*/ user_data) nothrow {
      gdk_window_invalidate_maybe_recurse(&this, UpCast!(Region*)(region), UpCast!(void**)(child_func), UpCast!(void*)(user_data));
   }

   // A convenience wrapper around gdk_window_invalidate_region() which
   // invalidates a rectangular region. See
   // gdk_window_invalidate_region() for details.
   // <rect>: rectangle to invalidate or %NULL to invalidate the whole window
   // <invalidate_children>: whether to also invalidate child windows
   void invalidate_rect(AT0)(AT0 /*Rectangle*/ rect, int invalidate_children) nothrow {
      gdk_window_invalidate_rect(&this, UpCast!(Rectangle*)(rect), invalidate_children);
   }

   // Adds @region to the update area for @window. The update area is the
   // region that needs to be redrawn, or "dirty region." The call
   // gdk_window_process_updates() sends one or more expose events to the
   // window, which together cover the entire update area. An
   // application would normally redraw the contents of @window in
   // response to those expose events.
   // 
   // GDK will call gdk_window_process_all_updates() on your behalf
   // whenever your program returns to the main loop and becomes idle, so
   // normally there's no need to do that manually, you just need to
   // invalidate regions that you know should be redrawn.
   // 
   // The @invalidate_children parameter controls whether the region of
   // each child window that intersects @region will also be invalidated.
   // If %FALSE, then the update area for child windows will remain
   // unaffected. See gdk_window_invalidate_maybe_recurse if you need
   // fine grained control over which children are invalidated.
   // <region>: a #GdkRegion
   // <invalidate_children>: %TRUE to also invalidate child windows
   void invalidate_region(AT0)(AT0 /*Region*/ region, int invalidate_children) nothrow {
      gdk_window_invalidate_region(&this, UpCast!(Region*)(region), invalidate_children);
   }

   // VERSION: 2.18
   // Check to see if a window is destroyed..
   // RETURNS: %TRUE if the window is destroyed
   int is_destroyed()() nothrow {
      return gdk_window_is_destroyed(&this);
   }

   // VERSION: 2.22
   // Determines whether or not the window is an input only window.
   // RETURNS: %TRUE if @window is input only
   int is_input_only()() nothrow {
      return gdk_window_is_input_only(&this);
   }

   // VERSION: 2.22
   // Determines whether or not the window is shaped.
   // RETURNS: %TRUE if @window is shaped
   int is_shaped()() nothrow {
      return gdk_window_is_shaped(&this);
   }

   // Check if the window and all ancestors of the window are
   // mapped. (This is not necessarily "viewable" in the X sense, since
   // we only check as far as we have GDK window parents, not to the root
   // window.)
   // RETURNS: %TRUE if the window is viewable
   int is_viewable()() nothrow {
      return gdk_window_is_viewable(&this);
   }

   // Checks whether the window has been mapped (with gdk_window_show() or
   // gdk_window_show_unraised()).
   // RETURNS: %TRUE if the window is mapped
   int is_visible()() nothrow {
      return gdk_window_is_visible(&this);
   }

   // Lowers @window to the bottom of the Z-order (stacking order), so that
   // other windows with the same parent window appear above @window.
   // This is true whether or not the other windows are visible.
   // 
   // If @window is a toplevel, the window manager may choose to deny the
   // request to move the window in the Z-order, gdk_window_lower() only
   // requests the restack, does not guarantee it.
   // 
   // Note that gdk_window_show() raises the window again, so don't call this
   // function before gdk_window_show(). (Try gdk_window_show_unraised().)
   void lower()() nothrow {
      gdk_window_lower(&this);
   }

   // Maximizes the window. If the window was already maximized, then
   // this function does nothing.
   // 
   // On X11, asks the window manager to maximize @window, if the window
   // manager supports this operation. Not all window managers support
   // this, and some deliberately ignore it or don't have a concept of
   // "maximized"; so you can't rely on the maximization actually
   // happening. But it will happen with most standard window managers,
   // and GDK makes a best effort to get it to happen.
   // 
   // On Windows, reliably maximizes the window.
   void maximize()() nothrow {
      gdk_window_maximize(&this);
   }

   // VERSION: 2.10
   // Merges the input shape masks for any child windows into the
   // input shape mask for @window. i.e. the union of all input masks
   // for @window and its children will become the new input mask
   // for @window. See gdk_window_input_shape_combine_mask().
   // 
   // This function is distinct from gdk_window_set_child_input_shapes()
   // because it includes @window's input shape mask in the set of
   // shapes to be merged.
   void merge_child_input_shapes()() nothrow {
      gdk_window_merge_child_input_shapes(&this);
   }

   // Merges the shape masks for any child windows into the
   // shape mask for @window. i.e. the union of all masks
   // for @window and its children will become the new mask
   // for @window. See gdk_window_shape_combine_mask().
   // 
   // This function is distinct from gdk_window_set_child_shapes()
   // because it includes @window's shape mask in the set of shapes to
   // be merged.
   void merge_child_shapes()() nothrow {
      gdk_window_merge_child_shapes(&this);
   }

   // Repositions a window relative to its parent window.
   // For toplevel windows, window managers may ignore or modify the move;
   // you should probably use gtk_window_move() on a #GtkWindow widget
   // anyway, instead of using GDK functions. For child windows,
   // the move will reliably succeed.
   // 
   // If you're also planning to resize the window, use gdk_window_move_resize()
   // to both move and resize simultaneously, for a nicer visual effect.
   // <x>: X coordinate relative to window's parent
   // <y>: Y coordinate relative to window's parent
   void move()(int x, int y) nothrow {
      gdk_window_move(&this, x, y);
   }

   // VERSION: 2.8
   // Move the part of @window indicated by @region by @dy pixels in the Y
   // direction and @dx pixels in the X direction. The portions of @region
   // that not covered by the new position of @region are invalidated.
   // 
   // Child windows are not moved.
   // <region>: The #GdkRegion to move
   // <dx>: Amount to move in the X direction
   // <dy>: Amount to move in the Y direction
   void move_region(AT0)(AT0 /*Region*/ region, int dx, int dy) nothrow {
      gdk_window_move_region(&this, UpCast!(Region*)(region), dx, dy);
   }

   // Equivalent to calling gdk_window_move() and gdk_window_resize(),
   // except that both operations are performed at once, avoiding strange
   // visual effects. (i.e. the user may be able to see the window first
   // move, then resize, if you don't use gdk_window_move_resize().)
   // <x>: new X position relative to window's parent
   // <y>: new Y position relative to window's parent
   // <width>: new width
   // <height>: new height
   void move_resize()(int x, int y, int width, int height) nothrow {
      gdk_window_move_resize(&this, x, y, width, height);
   }

   // Creates a new #GdkWindow using the attributes from
   // @attributes. See #GdkWindowAttr and #GdkWindowAttributesType for
   // more details.  Note: to use this on displays other than the default
   // display, @parent must be specified.
   // RETURNS: the new #GdkWindow
   // <attributes>: attributes of the new window
   // <attributes_mask>: mask indicating which fields in @attributes are valid
   Window* new_(AT0)(AT0 /*WindowAttr*/ attributes, int attributes_mask) nothrow {
      return gdk_window_new(&this, UpCast!(WindowAttr*)(attributes), attributes_mask);
   }

   // Like gdk_window_get_children(), but does not copy the list of
   // children, so the list does not need to be freed.
   // 
   // a reference to the list of child windows in @window
   GLib2.List* peek_children()() nothrow {
      return gdk_window_peek_children(&this);
   }

   // Sends one or more expose events to @window. The areas in each
   // expose event will cover the entire update area for the window (see
   // gdk_window_invalidate_region() for details). Normally GDK calls
   // gdk_window_process_all_updates() on your behalf, so there's no
   // need to call this function unless you want to force expose events
   // to be delivered immediately and synchronously (vs. the usual
   // case, where GDK delivers them in an idle handler). Occasionally
   // this is useful to produce nicer scrolling behavior, for example.
   // <update_children>: whether to also process updates for child windows
   void process_updates()(int update_children) nothrow {
      gdk_window_process_updates(&this, update_children);
   }

   // Raises @window to the top of the Z-order (stacking order), so that
   // other windows with the same parent window appear below @window.
   // This is true whether or not the windows are visible.
   // 
   // If @window is a toplevel, the window manager may choose to deny the
   // request to move the window in the Z-order, gdk_window_raise() only
   // requests the restack, does not guarantee it.
   void raise()() nothrow {
      gdk_window_raise(&this);
   }

   // VERSION: 2.14
   // Redirects drawing into @window so that drawing to the
   // window in the rectangle specified by @src_x, @src_y,
   // @width and @height is also drawn into @drawable at
   // @dest_x, @dest_y.
   // 
   // Only drawing between gdk_window_begin_paint_region() or
   // gdk_window_begin_paint_rect() and gdk_window_end_paint() is
   // redirected.
   // 
   // Redirection is active until gdk_window_remove_redirection()
   // is called.
   // <drawable>: a #GdkDrawable
   // <src_x>: x position in @window
   // <src_y>: y position in @window
   // <dest_x>: x position in @drawable
   // <dest_y>: y position in @drawable
   // <width>: width of redirection, or -1 to use the width of @window
   // <height>: height of redirection or -1 to use the height of @window
   void redirect_to_drawable(AT0)(AT0 /*Drawable*/ drawable, int src_x, int src_y, int dest_x, int dest_y, int width, int height) nothrow {
      gdk_window_redirect_to_drawable(&this, UpCast!(Drawable*)(drawable), src_x, src_y, dest_x, dest_y, width, height);
   }
   void register_dnd()() nothrow {
      gdk_window_register_dnd(&this);
   }

   // Unintrospectable method: remove_filter() / gdk_window_remove_filter()
   // Remove a filter previously added with gdk_window_add_filter().
   // <function>: previously-added filter function
   // <data>: user data for previously-added filter function
   void remove_filter(AT0)(FilterFunc function_, AT0 /*void*/ data) nothrow {
      gdk_window_remove_filter(&this, function_, UpCast!(void*)(data));
   }

   // VERSION: 2.14
   // Removes any active redirection started by
   // gdk_window_redirect_to_drawable().
   void remove_redirection()() nothrow {
      gdk_window_remove_redirection(&this);
   }

   // Reparents @window into the given @new_parent. The window being
   // reparented will be unmapped as a side effect.
   // <new_parent>: new parent to move @window into
   // <x>: X location inside the new parent
   // <y>: Y location inside the new parent
   void reparent(AT0)(AT0 /*Window*/ new_parent, int x, int y) nothrow {
      gdk_window_reparent(&this, UpCast!(Window*)(new_parent), x, y);
   }

   // Resizes @window; for toplevel windows, asks the window manager to resize
   // the window. The window manager may not allow the resize. When using GTK+,
   // use gtk_window_resize() instead of this low-level GDK function.
   // 
   // Windows may not be resized below 1x1.
   // 
   // If you're also planning to move the window, use gdk_window_move_resize()
   // to both move and resize simultaneously, for a nicer visual effect.
   // <width>: new width of the window
   // <height>: new height of the window
   void resize()(int width, int height) nothrow {
      gdk_window_resize(&this, width, height);
   }

   // VERSION: 2.18
   // Changes the position of  @window in the Z-order (stacking order), so that
   // it is above @sibling (if @above is %TRUE) or below @sibling (if @above is
   // %FALSE).
   // 
   // If @sibling is %NULL, then this either raises (if @above is %TRUE) or
   // lowers the window.
   // 
   // If @window is a toplevel, the window manager may choose to deny the
   // request to move the window in the Z-order, gdk_window_restack() only
   // requests the restack, does not guarantee it.
   // <sibling>: a #GdkWindow that is a sibling of @window, or %NULL
   // <above>: a boolean
   void restack(AT0)(AT0 /*Window*/ sibling, int above) nothrow {
      gdk_window_restack(&this, UpCast!(Window*)(sibling), above);
   }

   // Scroll the contents of @window, both pixels and children, by the
   // given amount. @window itself does not move. Portions of the window
   // that the scroll operation brings in from offscreen areas are
   // invalidated. The invalidated region may be bigger than what would
   // strictly be necessary.
   // 
   // For X11, a minimum area will be invalidated if the window has no
   // subwindows, or if the edges of the window's parent do not extend
   // beyond the edges of the window. In other cases, a multi-step process
   // is used to scroll the window which may produce temporary visual
   // artifacts and unnecessary invalidations.
   // <dx>: Amount to scroll in the X direction
   // <dy>: Amount to scroll in the Y direction
   void scroll()(int dx, int dy) nothrow {
      gdk_window_scroll(&this, dx, dy);
   }

   // VERSION: 2.4 
   // Setting @accept_focus to %FALSE hints the desktop environment that the
   // window doesn't want to receive input focus. 
   // 
   // On X, it is the responsibility of the window manager to interpret this 
   // hint. ICCCM-compliant window manager usually respect it.
   // <accept_focus>: %TRUE if the window should receive input focus
   void set_accept_focus()(int accept_focus) nothrow {
      gdk_window_set_accept_focus(&this, accept_focus);
   }

   // Sets the background pixmap of @window. May also be used to set a
   // background of "None" on @window, by setting a background pixmap
   // of %NULL.
   // 
   // A background pixmap will be tiled, positioning the first tile at
   // the origin of @window, or if @parent_relative is %TRUE, the tiling
   // will be done based on the origin of the parent window (useful to
   // align tiles in a parent with tiles in a child).
   // 
   // A background pixmap of %NULL means that the window will have no
   // background.  A window with no background will never have its
   // background filled by the windowing system, instead the window will
   // contain whatever pixels were already in the corresponding area of
   // the display.
   // 
   // The windowing system will normally fill a window with its background
   // when the window is obscured then exposed, and when you call
   // gdk_window_clear().
   // <pixmap>: a #GdkPixmap, or %NULL
   // <parent_relative>: whether the tiling origin is at the origin of @window's parent
   void set_back_pixmap(AT0)(AT0 /*Pixmap*/ pixmap, int parent_relative) nothrow {
      gdk_window_set_back_pixmap(&this, UpCast!(Pixmap*)(pixmap), parent_relative);
   }

   // Sets the background color of @window. (However, when using GTK+,
   // set the background of a widget with gtk_widget_modify_bg() - if
   // you're an application - or gtk_style_set_background() - if you're
   // implementing a custom widget.)
   // 
   // The @color must be allocated; gdk_rgb_find_color() is the best way
   // to allocate a color.
   // 
   // See also gdk_window_set_back_pixmap().
   // <color>: an allocated #GdkColor
   void set_background(AT0)(AT0 /*Color*/ color) nothrow {
      gdk_window_set_background(&this, UpCast!(Color*)(color));
   }

   // VERSION: 2.10
   // Sets the input shape mask of @window to the union of input shape masks
   // for all children of @window, ignoring the input shape mask of @window
   // itself. Contrast with gdk_window_merge_child_input_shapes() which includes
   // the input shape mask of @window in the masks to be merged.
   void set_child_input_shapes()() nothrow {
      gdk_window_set_child_input_shapes(&this);
   }

   // Sets the shape mask of @window to the union of shape masks
   // for all children of @window, ignoring the shape mask of @window
   // itself. Contrast with gdk_window_merge_child_shapes() which includes
   // the shape mask of @window in the masks to be merged.
   void set_child_shapes()() nothrow {
      gdk_window_set_child_shapes(&this);
   }

   // VERSION: 2.12
   // Sets a #GdkWindow as composited, or unsets it. Composited
   // windows do not automatically have their contents drawn to
   // the screen. Drawing is redirected to an offscreen buffer
   // and an expose event is emitted on the parent of the composited
   // window. It is the responsibility of the parent's expose handler
   // to manually merge the off-screen content onto the screen in
   // whatever way it sees fit. See <xref linkend="composited-window-example"/>
   // for an example.
   // 
   // It only makes sense for child windows to be composited; see
   // gdk_window_set_opacity() if you need translucent toplevel
   // windows.
   // 
   // An additional effect of this call is that the area of this
   // window is no longer clipped from regions marked for
   // invalidation on its parent. Draws done on the parent
   // window are also no longer clipped by the child.
   // 
   // This call is only supported on some systems (currently,
   // only X11 with new enough Xcomposite and Xdamage extensions).
   // You must call gdk_display_supports_composite() to check if
   // setting a window as composited is supported before
   // attempting to do so.
   // <composited>: %TRUE to set the window as composited
   void set_composited()(int composited) nothrow {
      gdk_window_set_composited(&this, composited);
   }

   // Sets the mouse pointer for a #GdkWindow. Use gdk_cursor_new_for_display()
   // or gdk_cursor_new_from_pixmap() to create the cursor. To make the cursor
   // invisible, use %GDK_BLANK_CURSOR. Passing %NULL for the @cursor argument
   // to gdk_window_set_cursor() means that @window will use the cursor of its
   // parent window. Most windows should use this default.
   // <cursor>: a cursor
   void set_cursor(AT0)(AT0 /*Cursor*/ cursor=null) nothrow {
      gdk_window_set_cursor(&this, UpCast!(Cursor*)(cursor));
   }

   // "Decorations" are the features the window manager adds to a toplevel #GdkWindow.
   // This function sets the traditional Motif window manager hints that tell the
   // window manager which decorations you would like your window to have.
   // Usually you should use gtk_window_set_decorated() on a #GtkWindow instead of
   // using the GDK function directly.
   // 
   // The @decorations argument is the logical OR of the fields in
   // the #GdkWMDecoration enumeration. If #GDK_DECOR_ALL is included in the
   // mask, the other bits indicate which decorations should be turned off.
   // If #GDK_DECOR_ALL is not included, then the other bits indicate
   // which decorations should be turned on.
   // 
   // Most window managers honor a decorations hint of 0 to disable all decorations,
   // but very few honor all possible combinations of bits.
   // <decorations>: decoration hint mask
   void set_decorations()(WMDecoration decorations) nothrow {
      gdk_window_set_decorations(&this, decorations);
   }

   // The event mask for a window determines which events will be reported
   // for that window. For example, an event mask including #GDK_BUTTON_PRESS_MASK
   // means the window should report button press events. The event mask
   // is the bitwise OR of values from the #GdkEventMask enumeration.
   // <event_mask>: event mask for @window
   void set_events()(EventMask event_mask) nothrow {
      gdk_window_set_events(&this, event_mask);
   }

   // VERSION: 2.6 
   // Setting @focus_on_map to %FALSE hints the desktop environment that the
   // window doesn't want to receive input focus when it is mapped.  
   // focus_on_map should be turned off for windows that aren't triggered
   // interactively (such as popups from network activity).
   // 
   // On X, it is the responsibility of the window manager to interpret
   // this hint. Window managers following the freedesktop.org window
   // manager extension specification should respect it.
   // <focus_on_map>: %TRUE if the window should receive input focus when mapped
   void set_focus_on_map()(int focus_on_map) nothrow {
      gdk_window_set_focus_on_map(&this, focus_on_map);
   }

   // Sets hints about the window management functions to make available
   // via buttons on the window frame.
   // 
   // On the X backend, this function sets the traditional Motif window 
   // manager hint for this purpose. However, few window managers do
   // anything reliable or interesting with this hint. Many ignore it
   // entirely.
   // 
   // The @functions argument is the logical OR of values from the
   // #GdkWMFunction enumeration. If the bitmask includes #GDK_FUNC_ALL,
   // then the other bits indicate which functions to disable; if
   // it doesn't include #GDK_FUNC_ALL, it indicates which functions to
   // enable.
   // <functions>: bitmask of operations to allow on @window
   void set_functions()(WMFunction functions) nothrow {
      gdk_window_set_functions(&this, functions);
   }

   // Sets the geometry hints for @window. Hints flagged in @geom_mask
   // are set, hints not flagged in @geom_mask are unset.
   // To unset all hints, use a @geom_mask of 0 and a @geometry of %NULL.
   // 
   // This function provides hints to the windowing system about
   // acceptable sizes for a toplevel window. The purpose of 
   // this is to constrain user resizing, but the windowing system
   // will typically  (but is not required to) also constrain the
   // current size of the window to the provided values and
   // constrain programatic resizing via gdk_window_resize() or
   // gdk_window_move_resize().
   // 
   // Note that on X11, this effect has no effect on windows
   // of type %GDK_WINDOW_TEMP or windows where override redirect
   // has been turned on via gdk_window_set_override_redirect()
   // since these windows are not resizable by the user.
   // 
   // Since you can't count on the windowing system doing the
   // constraints for programmatic resizes, you should generally
   // call gdk_window_constrain_size() yourself to determine
   // appropriate sizes.
   // <geometry>: geometry hints
   // <geom_mask>: bitmask indicating fields of @geometry to pay attention to
   void set_geometry_hints(AT0)(AT0 /*Geometry*/ geometry, WindowHints geom_mask) nothrow {
      gdk_window_set_geometry_hints(&this, UpCast!(Geometry*)(geometry), geom_mask);
   }

   // Sets the group leader window for @window. By default,
   // GDK sets the group leader for all toplevel windows
   // to a global window implicitly created by GDK. With this function
   // you can override this default.
   // 
   // The group leader window allows the window manager to distinguish
   // all windows that belong to a single application. It may for example
   // allow users to minimize/unminimize all windows belonging to an
   // application at once. You should only set a non-default group window
   // if your application pretends to be multiple applications.
   // <leader>: group leader window, or %NULL to restore the default group leader window
   void set_group(AT0)(AT0 /*Window*/ leader) nothrow {
      gdk_window_set_group(&this, UpCast!(Window*)(leader));
   }

   // This function is broken and useless and you should ignore it.
   // If using GTK+, use functions such as gtk_window_resize(), gtk_window_set_size_request(),
   // gtk_window_move(), gtk_window_parse_geometry(), and gtk_window_set_geometry_hints(),
   // depending on what you're trying to do.
   // 
   // If using GDK directly, use gdk_window_set_geometry_hints().
   // <x>: ignored field, does not matter
   // <y>: ignored field, does not matter
   // <min_width>: minimum width hint
   // <min_height>: minimum height hint
   // <max_width>: max width hint
   // <max_height>: max height hint
   // <flags>: logical OR of GDK_HINT_POS, GDK_HINT_MIN_SIZE, and/or GDK_HINT_MAX_SIZE
   void set_hints()(int x, int y, int min_width, int min_height, int max_width, int max_height, int flags) nothrow {
      gdk_window_set_hints(&this, x, y, min_width, min_height, max_width, max_height, flags);
   }

   // Sets the icon of @window as a pixmap or window. If using GTK+, investigate
   // gtk_window_set_default_icon_list() first, and then gtk_window_set_icon_list()
   // and gtk_window_set_icon(). If those don't meet your needs, look at
   // gdk_window_set_icon_list(). Only if all those are too high-level do you
   // want to fall back to gdk_window_set_icon().
   // <icon_window>: a #GdkWindow to use for the icon, or %NULL to unset
   // <pixmap>: a #GdkPixmap to use as the icon, or %NULL to unset
   // <mask>: a 1-bit pixmap (#GdkBitmap) to use as mask for @pixmap, or %NULL to have none
   void set_icon(AT0, AT1, AT2)(AT0 /*Window*/ icon_window, AT1 /*Pixmap*/ pixmap, AT2 /*Bitmap*/ mask) nothrow {
      gdk_window_set_icon(&this, UpCast!(Window*)(icon_window), UpCast!(Pixmap*)(pixmap), UpCast!(Bitmap*)(mask));
   }

   // Sets a list of icons for the window. One of these will be used
   // to represent the window when it has been iconified. The icon is
   // usually shown in an icon box or some sort of task bar. Which icon
   // size is shown depends on the window manager. The window manager
   // can scale the icon  but setting several size icons can give better
   // image quality since the window manager may only need to scale the
   // icon by a small amount or not at all.
   // <pixbufs>:  A list of pixbufs, of different sizes.
   void set_icon_list(AT0)(AT0 /*GLib2.List*/ pixbufs) nothrow {
      gdk_window_set_icon_list(&this, UpCast!(GLib2.List*)(pixbufs));
   }

   // Windows may have a name used while minimized, distinct from the
   // name they display in their titlebar. Most of the time this is a bad
   // idea from a user interface standpoint. But you can set such a name
   // with this function, if you like.
   // 
   // After calling this with a non-%NULL @name, calls to gdk_window_set_title()
   // will not update the icon title.
   // 
   // Using %NULL for @name unsets the icon title; further calls to
   // gdk_window_set_title() will again update the icon title as well.
   // <name>: name of window while iconified (minimized)
   void set_icon_name(AT0)(AT0 /*char*/ name) nothrow {
      gdk_window_set_icon_name(&this, toCString!(char*)(name));
   }

   // VERSION: 2.4
   // Set if @window must be kept above other windows. If the
   // window was already above, then this function does nothing.
   // 
   // On X11, asks the window manager to keep @window above, if the window
   // manager supports this operation. Not all window managers support
   // this, and some deliberately ignore it or don't have a concept of
   // "keep above"; so you can't rely on the window being kept above.
   // But it will happen with most standard window managers,
   // and GDK makes a best effort to get it to happen.
   // <setting>: whether to keep @window above other windows
   void set_keep_above()(int setting) nothrow {
      gdk_window_set_keep_above(&this, setting);
   }

   // VERSION: 2.4
   // Set if @window must be kept below other windows. If the
   // window was already below, then this function does nothing.
   // 
   // On X11, asks the window manager to keep @window below, if the window
   // manager supports this operation. Not all window managers support
   // this, and some deliberately ignore it or don't have a concept of
   // "keep below"; so you can't rely on the window being kept below.
   // But it will happen with most standard window managers,
   // and GDK makes a best effort to get it to happen.
   // <setting>: whether to keep @window below other windows
   void set_keep_below()(int setting) nothrow {
      gdk_window_set_keep_below(&this, setting);
   }

   // The application can use this hint to tell the window manager
   // that a certain window has modal behaviour. The window manager
   // can use this information to handle modal windows in a special
   // way.
   // 
   // You should only use this on windows for which you have
   // previously called gdk_window_set_transient_for()
   // <modal>: %TRUE if the window is modal, %FALSE otherwise.
   void set_modal_hint()(int modal) nothrow {
      gdk_window_set_modal_hint(&this, modal);
   }

   // VERSION: 2.12
   // Request the windowing system to make @window partially transparent,
   // with opacity 0 being fully transparent and 1 fully opaque. (Values
   // of the opacity parameter are clamped to the [0,1] range.) 
   // 
   // On X11, this works only on X screens with a compositing manager 
   // running.
   // 
   // For setting up per-pixel alpha, see gdk_screen_get_rgba_colormap().
   // For making non-toplevel windows translucent, see 
   // gdk_window_set_composited().
   // <opacity>: opacity
   void set_opacity()(double opacity) nothrow {
      gdk_window_set_opacity(&this, opacity);
   }

   // An override redirect window is not under the control of the window manager.
   // This means it won't have a titlebar, won't be minimizable, etc. - it will
   // be entirely under the control of the application. The window manager
   // can't see the override redirect window at all.
   // 
   // Override redirect should only be used for short-lived temporary
   // windows, such as popup menus. #GtkMenu uses an override redirect
   // window in its implementation, for example.
   // <override_redirect>: %TRUE if window should be override redirect
   void set_override_redirect()(int override_redirect) nothrow {
      gdk_window_set_override_redirect(&this, override_redirect);
   }

   // When using GTK+, typically you should use gtk_window_set_role() instead
   // of this low-level function.
   // 
   // The window manager and session manager use a window's role to
   // distinguish it from other kinds of window in the same application.
   // When an application is restarted after being saved in a previous
   // session, all windows with the same title and role are treated as
   // interchangeable.  So if you have two windows with the same title
   // that should be distinguished for session management purposes, you
   // should set the role on those windows. It doesn't matter what string
   // you use for the role, as long as you have a different role for each
   // non-interchangeable kind of window.
   // <role>: a string indicating its role
   void set_role(AT0)(AT0 /*char*/ role) nothrow {
      gdk_window_set_role(&this, toCString!(char*)(role));
   }

   // VERSION: 2.2
   // Toggles whether a window should appear in a pager (workspace
   // switcher, or other desktop utility program that displays a small
   // thumbnail representation of the windows on the desktop). If a
   // window's semantic type as specified with gdk_window_set_type_hint()
   // already fully describes the window, this function should 
   // <emphasis>not</emphasis> be called in addition, instead you should 
   // allow the window to be treated according to standard policy for 
   // its semantic type.
   // <skips_pager>: %TRUE to skip the pager
   void set_skip_pager_hint()(int skips_pager) nothrow {
      gdk_window_set_skip_pager_hint(&this, skips_pager);
   }

   // VERSION: 2.2
   // Toggles whether a window should appear in a task list or window
   // list. If a window's semantic type as specified with
   // gdk_window_set_type_hint() already fully describes the window, this
   // function should <emphasis>not</emphasis> be called in addition, 
   // instead you should allow the window to be treated according to 
   // standard policy for its semantic type.
   // <skips_taskbar>: %TRUE to skip the taskbar
   void set_skip_taskbar_hint()(int skips_taskbar) nothrow {
      gdk_window_set_skip_taskbar_hint(&this, skips_taskbar);
   }

   // VERSION: 2.12
   // When using GTK+, typically you should use gtk_window_set_startup_id()
   // instead of this low-level function.
   // <startup_id>: a string with startup-notification identifier
   void set_startup_id(AT0)(AT0 /*char*/ startup_id) nothrow {
      gdk_window_set_startup_id(&this, toCString!(char*)(startup_id));
   }

   // Set the bit gravity of the given window to static, and flag it so
   // all children get static subwindow gravity. This is used if you are
   // implementing scary features that involve deep knowledge of the
   // windowing system. Don't worry about it unless you have to.
   // RETURNS: %TRUE if the server supports static gravity
   // <use_static>: %TRUE to turn on static gravity
   int set_static_gravities()(int use_static) nothrow {
      return gdk_window_set_static_gravities(&this, use_static);
   }

   // Sets the title of a toplevel window, to be displayed in the titlebar.
   // If you haven't explicitly set the icon name for the window
   // (using gdk_window_set_icon_name()), the icon name will be set to
   // @title as well. @title must be in UTF-8 encoding (as with all
   // user-readable strings in GDK/GTK+). @title may not be %NULL.
   // <title>: title of @window
   void set_title(AT0)(AT0 /*char*/ title) nothrow {
      gdk_window_set_title(&this, toCString!(char*)(title));
   }

   // Indicates to the window manager that @window is a transient dialog
   // associated with the application window @parent. This allows the
   // window manager to do things like center @window on @parent and
   // keep @window above @parent.
   // 
   // See gtk_window_set_transient_for() if you're using #GtkWindow or
   // #GtkDialog.
   // <parent>: another toplevel #GdkWindow
   void set_transient_for(AT0)(AT0 /*Window*/ parent) nothrow {
      gdk_window_set_transient_for(&this, UpCast!(Window*)(parent));
   }

   // The application can use this call to provide a hint to the window
   // manager about the functionality of a window. The window manager
   // can use this information when determining the decoration and behaviour
   // of the window.
   // 
   // The hint must be set before the window is mapped.
   // <hint>: A hint of the function this window will have
   void set_type_hint()(WindowTypeHint hint) nothrow {
      gdk_window_set_type_hint(&this, hint);
   }

   // VERSION: 2.8
   // Toggles whether a window needs the user's
   // urgent attention.
   // <urgent>: %TRUE if the window is urgent
   void set_urgency_hint()(int urgent) nothrow {
      gdk_window_set_urgency_hint(&this, urgent);
   }

   // For most purposes this function is deprecated in favor of
   // g_object_set_data(). However, for historical reasons GTK+ stores
   // the #GtkWidget that owns a #GdkWindow as user data on the
   // #GdkWindow. So, custom widget implementations should use
   // this function for that. If GTK+ receives an event for a #GdkWindow,
   // and the user data for the window is non-%NULL, GTK+ will assume the
   // user data is a #GtkWidget, and forward the event to that widget.
   // <user_data>: user data
   void set_user_data(AT0)(AT0 /*void*/ user_data) nothrow {
      gdk_window_set_user_data(&this, UpCast!(void*)(user_data));
   }

   // Applies a shape mask to @window. Pixels in @window corresponding to
   // set bits in the @mask will be visible; pixels in @window
   // corresponding to unset bits in the @mask will be transparent. This
   // gives a non-rectangular window.
   // 
   // If @mask is %NULL, the shape mask will be unset, and the @x/@y
   // parameters are not used.
   // 
   // On the X11 platform, this uses an X server extension which is
   // widely available on most common platforms, but not available on
   // very old X servers, and occasionally the implementation will be
   // buggy. On servers without the shape extension, this function
   // will do nothing.
   // 
   // This function works on both toplevel and child windows.
   // <mask>: shape mask
   // <x>: X position of shape mask with respect to @window
   // <y>: Y position of shape mask with respect to @window
   void shape_combine_mask(AT0)(AT0 /*Bitmap*/ mask, int x, int y) nothrow {
      gdk_window_shape_combine_mask(&this, UpCast!(Bitmap*)(mask), x, y);
   }

   // Makes pixels in @window outside @shape_region be transparent,
   // so that the window may be nonrectangular. See also
   // gdk_window_shape_combine_mask() to use a bitmap as the mask.
   // 
   // If @shape_region is %NULL, the shape will be unset, so the whole
   // window will be opaque again. @offset_x and @offset_y are ignored
   // if @shape_region is %NULL.
   // 
   // On the X11 platform, this uses an X server extension which is
   // widely available on most common platforms, but not available on
   // very old X servers, and occasionally the implementation will be
   // buggy. On servers without the shape extension, this function
   // will do nothing.
   // 
   // This function works on both toplevel and child windows.
   // <shape_region>: region of window to be non-transparent
   // <offset_x>: X position of @shape_region in @window coordinates
   // <offset_y>: Y position of @shape_region in @window coordinates
   void shape_combine_region(AT0)(AT0 /*Region*/ shape_region, int offset_x, int offset_y) nothrow {
      gdk_window_shape_combine_region(&this, UpCast!(Region*)(shape_region), offset_x, offset_y);
   }

   // Like gdk_window_show_unraised(), but also raises the window to the
   // top of the window stack (moves the window to the front of the
   // Z-order).
   // 
   // This function maps a window so it's visible onscreen. Its opposite
   // is gdk_window_hide().
   // 
   // When implementing a #GtkWidget, you should call this function on the widget's
   // #GdkWindow as part of the "map" method.
   void show()() nothrow {
      gdk_window_show(&this);
   }

   // Shows a #GdkWindow onscreen, but does not modify its stacking
   // order. In contrast, gdk_window_show() will raise the window
   // to the top of the window stack.
   // 
   // On the X11 platform, in Xlib terms, this function calls
   // XMapWindow() (it also updates some internal GDK state, which means
   // that you can't really use XMapWindow() directly on a GDK window).
   void show_unraised()() nothrow {
      gdk_window_show_unraised(&this);
   }

   // "Pins" a window such that it's on all workspaces and does not scroll
   // with viewports, for window managers that have scrollable viewports.
   // (When using #GtkWindow, gtk_window_stick() may be more useful.)
   // 
   // On the X11 platform, this function depends on window manager
   // support, so may have no effect with many window managers. However,
   // GDK will do the best it can to convince the window manager to stick
   // the window. For window managers that don't support this operation,
   // there's nothing you can do to force it to happen.
   void stick()() nothrow {
      gdk_window_stick(&this);
   }

   // Thaws a window frozen with
   // gdk_window_freeze_toplevel_updates_libgtk_only().
   // 
   // This function is not part of the GDK public API and is only
   // for use by GTK+.
   void thaw_toplevel_updates_libgtk_only()() nothrow {
      gdk_window_thaw_toplevel_updates_libgtk_only(&this);
   }
   // Thaws a window frozen with gdk_window_freeze_updates().
   void thaw_updates()() nothrow {
      gdk_window_thaw_updates(&this);
   }

   // VERSION: 2.2
   // Moves the window out of fullscreen mode. If the window was not
   // fullscreen, does nothing.
   // 
   // On X11, asks the window manager to move @window out of the fullscreen
   // state, if the window manager supports this operation. Not all
   // window managers support this, and some deliberately ignore it or
   // don't have a concept of "fullscreen"; so you can't rely on the
   // unfullscreenification actually happening. But it will happen with
   // most standard window managers, and GDK makes a best effort to get
   // it to happen.
   void unfullscreen()() nothrow {
      gdk_window_unfullscreen(&this);
   }

   // Unmaximizes the window. If the window wasn't maximized, then this
   // function does nothing.
   // 
   // On X11, asks the window manager to unmaximize @window, if the
   // window manager supports this operation. Not all window managers
   // support this, and some deliberately ignore it or don't have a
   // concept of "maximized"; so you can't rely on the unmaximization
   // actually happening. But it will happen with most standard window
   // managers, and GDK makes a best effort to get it to happen.
   // 
   // On Windows, reliably unmaximizes the window.
   void unmaximize()() nothrow {
      gdk_window_unmaximize(&this);
   }

   // Reverse operation for gdk_window_stick(); see gdk_window_stick(),
   // and gtk_window_unstick().
   void unstick()() nothrow {
      gdk_window_unstick(&this);
   }

   // Withdraws a window (unmaps it and asks the window manager to forget about it).
   // This function is not really useful as gdk_window_hide() automatically
   // withdraws toplevel windows before hiding them.
   void withdraw()() nothrow {
      gdk_window_withdraw(&this);
   }

   // VERSION: 2.18
   // The ::from-embedder signal is emitted to translate coordinates
   // in the embedder of an offscreen window to the offscreen window.
   // 
   // See also #GtkWindow::to-embedder.
   // <embedder-x>: x coordinate in the embedder window
   // <embedder-y>: y coordinate in the embedder window
   // <offscreen-x>: return location for the x coordinate in the offscreen window
   // <offscreen-y>: return location for the y coordinate in the offscreen window
   extern (C) alias static void function (Window* this_, double embedder_x, double embedder_y, /*out*/ /*POINTER*/ double* offscreen_x, /*out*/ /*POINTER*/ double* offscreen_y, void* user_data=null) nothrow signal_from_embedder;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"from-embedder", CB/*:signal_from_embedder*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_from_embedder)||_ttmm!(CB, signal_from_embedder)()) {
      return signal_connect_data!()(&this, cast(char*)"from-embedder",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 2.18
   // The ::pick-embedded-child signal is emitted to find an embedded
   // child at the given position.
   // 
   // @x, @y, or %NULL
   // RETURNS: the #GdkWindow of the embedded child at
   // <x>: x coordinate in the window
   // <y>: y coordinate in the window
   extern (C) alias static Window* function (Window* this_, double x, double y, void* user_data=null) nothrow signal_pick_embedded_child;
   ulong signal_connect(string name:"pick-embedded-child", CB/*:signal_pick_embedded_child*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_pick_embedded_child)||_ttmm!(CB, signal_pick_embedded_child)()) {
      return signal_connect_data!()(&this, cast(char*)"pick-embedded-child",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 2.18
   // The ::to-embedder signal is emitted to translate coordinates
   // in an offscreen window to its embedder.
   // 
   // See also #GtkWindow::from-embedder.
   // <offscreen-x>: x coordinate in the offscreen window
   // <offscreen-y>: y coordinate in the offscreen window
   // <embedder-x>: return location for the x coordinate in the embedder window
   // <embedder-y>: return location for the y coordinate in the embedder window
   extern (C) alias static void function (Window* this_, double offscreen_x, double offscreen_y, /*out*/ /*POINTER*/ double* embedder_x, /*out*/ /*POINTER*/ double* embedder_y, void* user_data=null) nothrow signal_to_embedder;
   ulong signal_connect(string name:"to-embedder", CB/*:signal_to_embedder*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_to_embedder)||_ttmm!(CB, signal_to_embedder)()) {
      return signal_connect_data!()(&this, cast(char*)"to-embedder",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct WindowAttr {
   char* title;
   int event_mask, x, y, width, height;
   WindowClass wclass;
   Visual* visual;
   Colormap* colormap;
   WindowType window_type;
   Cursor* cursor;
   char* wmclass_name, wmclass_class;
   int override_redirect;
   WindowTypeHint type_hint;
}

enum WindowAttributesType {
   TITLE = 2,
   X = 4,
   Y = 8,
   CURSOR = 16,
   COLORMAP = 32,
   VISUAL = 64,
   WMCLASS = 128,
   NOREDIR = 256,
   TYPE_HINT = 512
}
enum WindowClass {
   OUTPUT = 0,
   ONLY = 1
}
enum WindowEdge {
   NORTH_WEST = 0,
   NORTH = 1,
   NORTH_EAST = 2,
   WEST = 3,
   EAST = 4,
   SOUTH_WEST = 5,
   SOUTH = 6,
   SOUTH_EAST = 7
}
enum WindowHints {
   POS = 1,
   MIN_SIZE = 2,
   MAX_SIZE = 4,
   BASE_SIZE = 8,
   ASPECT = 16,
   RESIZE_INC = 32,
   WIN_GRAVITY = 64,
   USER_POS = 128,
   USER_SIZE = 256
}
struct WindowObject {
}

struct WindowObjectClass {
   DrawableClass parent_class;
}

struct WindowRedirect {
}

enum WindowState {
   WITHDRAWN = 1,
   ICONIFIED = 2,
   MAXIMIZED = 4,
   STICKY = 8,
   FULLSCREEN = 16,
   ABOVE = 32,
   BELOW = 64
}
enum WindowType {
   ROOT = 0,
   TOPLEVEL = 1,
   CHILD = 2,
   DIALOG = 3,
   TEMP = 4,
   FOREIGN = 5,
   OFFSCREEN = 6
}
enum WindowTypeHint {
   NORMAL = 0,
   DIALOG = 1,
   MENU = 2,
   TOOLBAR = 3,
   SPLASHSCREEN = 4,
   UTILITY = 5,
   DOCK = 6,
   DESKTOP = 7,
   DROPDOWN_MENU = 8,
   POPUP_MENU = 9,
   TOOLTIP = 10,
   NOTIFICATION = 11,
   COMBO = 12,
   DND = 13
}
// Unintrospectable function: add_client_message_filter() / gdk_add_client_message_filter()
static void add_client_message_filter(AT0)(Atom message_type, FilterFunc func, AT0 /*void*/ data) nothrow {
   gdk_add_client_message_filter(message_type, func, UpCast!(void*)(data));
}


// Appends gdk option entries to the passed in option group. This is
// not public API and must not be used by applications.
// <group>: An option group.
static void add_option_entries_libgtk_only(AT0)(AT0 /*GLib2.OptionGroup*/ group) nothrow {
   gdk_add_option_entries_libgtk_only(UpCast!(GLib2.OptionGroup*)(group));
}

// Emits a short beep on the default display.
static void beep()() nothrow {
   gdk_beep();
}


// VERSION: 2.8
// Creates a Cairo context for drawing to @drawable.
// 
// <note><para>
// Note that due to double-buffering, Cairo contexts created 
// in a GTK+ expose event handler cannot be cached and reused 
// between different expose events. 
// </para></note>
// 
// cairo_destroy() when you are done drawing.
// RETURNS: A newly created Cairo context. Free with
// <drawable>: a #GdkDrawable
static cairo.Context* /*new*/ cairo_create(AT0)(AT0 /*Drawable*/ drawable) nothrow {
   return gdk_cairo_create(UpCast!(Drawable*)(drawable));
}


// VERSION: 2.8
// Adds the given rectangle to the current path of @cr.
// <cr>: a #cairo_t
// <rectangle>: a #GdkRectangle
static void cairo_rectangle(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Rectangle*/ rectangle) nothrow {
   gdk_cairo_rectangle(UpCast!(cairo.Context*)(cr), UpCast!(Rectangle*)(rectangle));
}


// VERSION: 2.8
// Adds the given region to the current path of @cr.
// <cr>: a #cairo_t
// <region>: a #GdkRegion
static void cairo_region(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Region*/ region) nothrow {
   gdk_cairo_region(UpCast!(cairo.Context*)(cr), UpCast!(Region*)(region));
}


// VERSION: 2.18
// Resets the clip region for a Cairo context created by gdk_cairo_create().
// 
// This resets the clip region to the "empty" state for the given drawable.
// This is required for non-native windows since a direct call to
// cairo_reset_clip() would unset the clip region inherited from the
// drawable (i.e. the window clip region), and thus let you e.g.
// draw outside your window.
// 
// This is rarely needed though, since most code just create a new cairo_t
// using gdk_cairo_create() each time they want to draw something.
// <cr>: a #cairo_t
// <drawable>: a #GdkDrawable
static void cairo_reset_clip(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Drawable*/ drawable) nothrow {
   gdk_cairo_reset_clip(UpCast!(cairo.Context*)(cr), UpCast!(Drawable*)(drawable));
}


// VERSION: 2.8
// Sets the specified #GdkColor as the source color of @cr.
// <cr>: a #cairo_t
// <color>: a #GdkColor
static void cairo_set_source_color(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Color*/ color) nothrow {
   gdk_cairo_set_source_color(UpCast!(cairo.Context*)(cr), UpCast!(Color*)(color));
}


// VERSION: 2.8
// Sets the given pixbuf as the source pattern for the Cairo context.
// The pattern has an extend mode of %CAIRO_EXTEND_NONE and is aligned
// so that the origin of @pixbuf is @pixbuf_x, @pixbuf_y
// <cr>: a #Cairo context
// <pixbuf>: a #GdkPixbuf
// <pixbuf_x>: X coordinate of location to place upper left corner of @pixbuf
// <pixbuf_y>: Y coordinate of location to place upper left corner of @pixbuf
static void cairo_set_source_pixbuf(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*GdkPixbuf2.Pixbuf*/ pixbuf, double pixbuf_x, double pixbuf_y) nothrow {
   gdk_cairo_set_source_pixbuf(UpCast!(cairo.Context*)(cr), UpCast!(GdkPixbuf2.Pixbuf*)(pixbuf), pixbuf_x, pixbuf_y);
}


// VERSION: 2.10
// DEPRECATED (v2.24) function: cairo_set_source_pixmap - This function is being removed in GTK+ 3 (together
// Sets the given pixmap as the source pattern for the Cairo context.
// The pattern has an extend mode of %CAIRO_EXTEND_NONE and is aligned
// so that the origin of @pixmap is @pixmap_x, @pixmap_y
// 
// 
// with #GdkPixmap). Instead, use gdk_cairo_set_source_window() where
// appropriate.
// <cr>: a #Cairo context
// <pixmap>: a #GdkPixmap
// <pixmap_x>: X coordinate of location to place upper left corner of @pixmap
// <pixmap_y>: Y coordinate of location to place upper left corner of @pixmap
static void cairo_set_source_pixmap(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Pixmap*/ pixmap, double pixmap_x, double pixmap_y) nothrow {
   gdk_cairo_set_source_pixmap(UpCast!(cairo.Context*)(cr), UpCast!(Pixmap*)(pixmap), pixmap_x, pixmap_y);
}


// VERSION: 2.24
// Sets the given window as the source pattern for the Cairo context.
// The pattern has an extend mode of %CAIRO_EXTEND_NONE and is aligned
// so that the origin of @window is @x, @y. The window contains all its
// subwindows when rendering.
// 
// Note that the contents of @window are undefined outside of the
// visible part of @window, so use this function with care.
// <cr>: a #Cairo context
// <window>: a #GdkWindow
// <x>: X coordinate of location to place upper left corner of @window
// <y>: Y coordinate of location to place upper left corner of @window
static void cairo_set_source_window(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Window*/ window, double x, double y) nothrow {
   gdk_cairo_set_source_window(UpCast!(cairo.Context*)(cr), UpCast!(Window*)(window), x, y);
}


// DEPRECATED (v2.2) function: char_height - Use gdk_text_extents() instead.
// Determines the total height of a given character.
// This value is not generally useful, because you cannot
// determine how this total height will be drawn in
// relation to the baseline. See gdk_text_extents().
// RETURNS: the height of the character in pixels.
// <font>: a #GdkFont
// <character>: the character to measure.
static int char_height(AT0)(AT0 /*Font*/ font, char character) nothrow {
   return gdk_char_height(UpCast!(Font*)(font), character);
}


// Determines the distance from the origin to the rightmost
// portion of a character when drawn. This is not the
// correct value for determining the origin of the next
// portion when drawing text in multiple pieces.
// RETURNS: the right bearing of the character in pixels.
// <font>: a #GdkFont
// <character>: the character to measure.
static int char_measure(AT0)(AT0 /*Font*/ font, char character) nothrow {
   return gdk_char_measure(UpCast!(Font*)(font), character);
}


// DEPRECATED (v2.2) function: char_width - Use gdk_text_extents() instead.
// Determines the width of a given character.
// RETURNS: the width of the character in pixels.
// <font>: a #GdkFont
// <character>: the character to measure.
static int char_width(AT0)(AT0 /*Font*/ font, char character) nothrow {
   return gdk_char_width(UpCast!(Font*)(font), character);
}


// Determines the width of a given wide character. (Encoded
// in the wide-character encoding of the current locale).
// RETURNS: the width of the character in pixels.
// <font>: a #GdkFont
// <character>: the character to measure.
static int char_width_wc(AT0)(AT0 /*Font*/ font, WChar character) nothrow {
   return gdk_char_width_wc(UpCast!(Font*)(font), character);
}


// DEPRECATED (v2.2) function: color_alloc - Use gdk_colormap_alloc_color() instead.
// MOVED TO: Color.alloc
// Allocates a single color from a colormap.
// RETURNS: %TRUE if the allocation succeeded.
// <colormap>: a #GdkColormap.
// <color>: The color to allocate. On return, the <structfield>pixel</structfield> field will be filled in.
static int color_alloc(AT0, AT1)(AT0 /*Colormap*/ colormap, AT1 /*Color*/ color) nothrow {
   return gdk_color_alloc(UpCast!(Colormap*)(colormap), UpCast!(Color*)(color));
}


// MOVED TO: Color.black
// Returns the black color for a given colormap. The resulting
// value has already been allocated.
// RETURNS: %TRUE if the allocation succeeded.
// <colormap>: a #GdkColormap.
// <color>: the location to store the color.
static int color_black(AT0, AT1)(AT0 /*Colormap*/ colormap, AT1 /*Color*/ color) nothrow {
   return gdk_color_black(UpCast!(Colormap*)(colormap), UpCast!(Color*)(color));
}


// MOVED TO: Color.change
// Changes the value of a color that has already
// been allocated. If @colormap is not a private
// colormap, then the color must have been allocated
// using gdk_colormap_alloc_colors() with the 
// @writeable set to %TRUE.
// RETURNS: %TRUE if the color was successfully changed.
// <colormap>: a #GdkColormap.
// <color>: a #GdkColor, with the color to change in the <structfield>pixel</structfield> field, and the new value in the remaining fields.
static int color_change(AT0, AT1)(AT0 /*Colormap*/ colormap, AT1 /*Color*/ color) nothrow {
   return gdk_color_change(UpCast!(Colormap*)(colormap), UpCast!(Color*)(color));
}


// MOVED TO: Color.parse
// Parses a textual specification of a color and fill in the
// <structfield>red</structfield>, <structfield>green</structfield>,
// and <structfield>blue</structfield> fields of a #GdkColor
// structure. The color is <emphasis>not</emphasis> allocated, you
// must call gdk_colormap_alloc_color() yourself. The string can
// either one of a large set of standard names. (Taken from the X11
// <filename>rgb.txt</filename> file), or it can be a hex value in the
// form '&num;rgb' '&num;rrggbb' '&num;rrrgggbbb' or
// '&num;rrrrggggbbbb' where 'r', 'g' and 'b' are hex digits of the
// red, green, and blue components of the color, respectively. (White
// in the four forms is '&num;fff' '&num;ffffff' '&num;fffffffff' and
// '&num;ffffffffffff')
// RETURNS: %TRUE if the parsing succeeded.
// <spec>: the string specifying the color.
// <color>: the #GdkColor to fill in
static int color_parse(AT0, AT1)(AT0 /*char*/ spec, /*out*/ AT1 /*Color*/ color) nothrow {
   return gdk_color_parse(toCString!(char*)(spec), UpCast!(Color*)(color));
}


// MOVED TO: Color.white
// Returns the white color for a given colormap. The resulting
// value has already allocated been allocated.
// RETURNS: %TRUE if the allocation succeeded.
// <colormap>: a #GdkColormap.
// <color>: the location to store the color.
static int color_white(AT0, AT1)(AT0 /*Colormap*/ colormap, AT1 /*Color*/ color) nothrow {
   return gdk_color_white(UpCast!(Colormap*)(colormap), UpCast!(Color*)(color));
}


// Allocates colors from a colormap. This function
// is obsolete. See gdk_colormap_alloc_colors().
// For full documentation of the fields, see 
// the Xlib documentation for <function>XAllocColorCells()</function>.
// RETURNS: %TRUE if the allocation was successful
// <colormap>: a #GdkColormap.
// <contiguous>: if %TRUE, the colors should be allocated in contiguous color cells.
// <planes>: an array in which to store the plane masks.
// <nplanes>: the number of planes to allocate. (Or zero, to indicate that the color allocation should not be planar.)
// <pixels>: an array into which to store allocated pixel values.
// <npixels>: the number of pixels in each plane to allocate.
static int colors_alloc(AT0, AT1, AT2)(AT0 /*Colormap*/ colormap, int contiguous, AT1 /*c_ulong*/ planes, int nplanes, AT2 /*c_ulong*/ pixels, int npixels) nothrow {
   return gdk_colors_alloc(UpCast!(Colormap*)(colormap), contiguous, UpCast!(c_ulong*)(planes), nplanes, UpCast!(c_ulong*)(pixels), npixels);
}


// Frees colors allocated with gdk_colors_alloc(). This
// function is obsolete. See gdk_colormap_free_colors().
// <colormap>: a #GdkColormap.
// <pixels>: the pixel values of the colors to free.
// <npixels>: the number of values in @pixels.
// <planes>: the plane masks for all planes to free, OR'd together.
static void colors_free(AT0, AT1)(AT0 /*Colormap*/ colormap, AT1 /*c_ulong*/ pixels, int npixels, c_ulong planes) nothrow {
   gdk_colors_free(UpCast!(Colormap*)(colormap), UpCast!(c_ulong*)(pixels), npixels, planes);
}


// Changes the value of the first @ncolors colors in
// a private colormap. This function is obsolete and
// should not be used. See gdk_color_change().
// <colormap>: a #GdkColormap.
// <colors>: the new color values.
// <ncolors>: the number of colors to change.
static void colors_store(AT0, AT1)(AT0 /*Colormap*/ colormap, AT1 /*Color*/ colors, int ncolors) nothrow {
   gdk_colors_store(UpCast!(Colormap*)(colormap), UpCast!(Color*)(colors), ncolors);
}


// Returns the list of available input devices for the default display.
// The list is statically allocated and should not be freed.
// RETURNS: a list of #GdkDevice
static GLib2.List* devices_list()() nothrow {
   return gdk_devices_list();
}


// Aborts a drag without dropping. 
// 
// This function is called by the drag source.
// <context>: a #GdkDragContext.
// <time_>: the timestamp for this operation.
static void drag_abort(AT0)(AT0 /*DragContext*/ context, uint time_) nothrow {
   gdk_drag_abort(UpCast!(DragContext*)(context), time_);
}


// Unintrospectable function: drag_begin() / gdk_drag_begin()
// Starts a drag and creates a new drag context for it.
// 
// This function is called by the drag source.
// RETURNS: a newly created #GdkDragContext.
// <window>: the source window for this drag.
// <targets>: the offered targets, as list of #GdkAtom<!-- -->s
static DragContext* drag_begin(AT0, AT1)(AT0 /*Window*/ window, AT1 /*GLib2.List*/ targets) nothrow {
   return gdk_drag_begin(UpCast!(Window*)(window), UpCast!(GLib2.List*)(targets));
}


// Drops on the current destination.
// 
// This function is called by the drag source.
// <context>: a #GdkDragContext.
// <time_>: the timestamp for this operation.
static void drag_drop(AT0)(AT0 /*DragContext*/ context, uint time_) nothrow {
   gdk_drag_drop(UpCast!(DragContext*)(context), time_);
}


// VERSION: 2.6
// Returns whether the dropped data has been successfully 
// transferred. This function is intended to be used while 
// handling a %GDK_DROP_FINISHED event, its return value is
// meaningless at other times.
// RETURNS: %TRUE if the drop was successful.
// <context>: a #GdkDragContext
static int drag_drop_succeeded(AT0)(AT0 /*DragContext*/ context) nothrow {
   return gdk_drag_drop_succeeded(UpCast!(DragContext*)(context));
}


// DEPRECATED (v2.24) function: drag_find_window - Use gdk_drag_find_window_for_screen() instead.
// Finds the destination window and DND protocol to use at the
// given pointer position.
// 
// This function is called by the drag source to obtain the 
// @dest_window and @protocol parameters for gdk_drag_motion().
// <context>: a #GdkDragContext.
// <drag_window>: a window which may be at the pointer position, but should be ignored, since it is put up by the drag source as an icon.
// <x_root>: the x position of the pointer in root coordinates.
// <y_root>: the y position of the pointer in root coordinates.
// <dest_window>: location to store the destination window in.
// <protocol>: location to store the DND protocol in.
static void drag_find_window(AT0, AT1, AT2, AT3)(AT0 /*DragContext*/ context, AT1 /*Window*/ drag_window, int x_root, int y_root, /*out*/ AT2 /*Window**/ dest_window, /*out*/ AT3 /*DragProtocol*/ protocol) nothrow {
   gdk_drag_find_window(UpCast!(DragContext*)(context), UpCast!(Window*)(drag_window), x_root, y_root, UpCast!(Window**)(dest_window), UpCast!(DragProtocol*)(protocol));
}


// VERSION: 2.2
// Finds the destination window and DND protocol to use at the
// given pointer position.
// 
// This function is called by the drag source to obtain the 
// @dest_window and @protocol parameters for gdk_drag_motion().
// <context>: a #GdkDragContext
// <drag_window>: a window which may be at the pointer position, but should be ignored, since it is put up by the drag source as an icon.
// <screen>: the screen where the destination window is sought.
// <x_root>: the x position of the pointer in root coordinates.
// <y_root>: the y position of the pointer in root coordinates.
// <dest_window>: location to store the destination window in.
// <protocol>: location to store the DND protocol in.
static void drag_find_window_for_screen(AT0, AT1, AT2, AT3, AT4)(AT0 /*DragContext*/ context, AT1 /*Window*/ drag_window, AT2 /*Screen*/ screen, int x_root, int y_root, /*out*/ AT3 /*Window**/ dest_window, /*out*/ AT4 /*DragProtocol*/ protocol) nothrow {
   gdk_drag_find_window_for_screen(UpCast!(DragContext*)(context), UpCast!(Window*)(drag_window), UpCast!(Screen*)(screen), x_root, y_root, UpCast!(Window**)(dest_window), UpCast!(DragProtocol*)(protocol));
}


// DEPRECATED (v2.24) function: drag_get_protocol - Use gdk_drag_get_protocol_for_display() instead
// Finds out the DND protocol supported by a window.
// 
// the drop should happen. This may be @xid or the id of a proxy
// window, or zero if @xid doesn't support Drag and Drop.
// RETURNS: the windowing system specific id for the window where
// <xid>: the windowing system id of the destination window.
// <protocol>: location where the supported DND protocol is returned.
static NativeWindow drag_get_protocol(AT0)(NativeWindow xid, AT0 /*DragProtocol*/ protocol) nothrow {
   return gdk_drag_get_protocol(xid, UpCast!(DragProtocol*)(protocol));
}


// VERSION: 2.2
// Finds out the DND protocol supported by a window.
// RETURNS: the windowing system id of the window where the drop should happen. This may be @xid or the id of a proxy window, or zero if @xid doesn't support Drag and Drop.
// <display>: the #GdkDisplay where the destination window resides
// <xid>: the windowing system id of the destination window.
// <protocol>: location where the supported DND protocol is returned.
static NativeWindow drag_get_protocol_for_display(AT0, AT1)(AT0 /*Display*/ display, NativeWindow xid, AT1 /*DragProtocol*/ protocol) nothrow {
   return gdk_drag_get_protocol_for_display(UpCast!(Display*)(display), xid, UpCast!(DragProtocol*)(protocol));
}


// Unintrospectable function: drag_get_selection() / gdk_drag_get_selection()
// Returns the selection atom for the current source window.
// RETURNS: the selection atom.
// <context>: a #GdkDragContext.
static Atom drag_get_selection(AT0)(AT0 /*DragContext*/ context) nothrow {
   return gdk_drag_get_selection(UpCast!(DragContext*)(context));
}


// Updates the drag context when the pointer moves or the 
// set of actions changes.
// 
// This function is called by the drag source.
// RETURNS: FIXME
// <context>: a #GdkDragContext.
// <dest_window>: the new destination window, obtained by gdk_drag_find_window().
// <protocol>: the DND protocol in use, obtained by gdk_drag_find_window().
// <x_root>: the x position of the pointer in root coordinates.
// <y_root>: the y position of the pointer in root coordinates.
// <suggested_action>: the suggested action.
// <possible_actions>: the possible actions.
// <time_>: the timestamp for this operation.
static int drag_motion(AT0, AT1)(AT0 /*DragContext*/ context, AT1 /*Window*/ dest_window, DragProtocol protocol, int x_root, int y_root, DragAction suggested_action, DragAction possible_actions, uint time_) nothrow {
   return gdk_drag_motion(UpCast!(DragContext*)(context), UpCast!(Window*)(dest_window), protocol, x_root, y_root, suggested_action, possible_actions, time_);
}


// Selects one of the actions offered by the drag source.
// 
// This function is called by the drag destination in response to
// gdk_drag_motion() called by the drag source.
// <context>: a #GdkDragContext.
// <action>: the selected action which will be taken when a drop happens, or 0 to indicate that a drop will not be accepted.
// <time_>: the timestamp for this operation.
static void drag_status(AT0)(AT0 /*DragContext*/ context, DragAction action, uint time_) nothrow {
   gdk_drag_status(UpCast!(DragContext*)(context), action, time_);
}


// DEPRECATED (v2.22) function: draw_arc - Use cairo_arc() and cairo_fill() or cairo_stroke()
// Draws an arc or a filled 'pie slice'. The arc is defined by the bounding
// rectangle of the entire ellipse, and the start and end angles of the part 
// of the ellipse to be drawn.
// 
// instead. Note that arcs just like any drawing operation in Cairo are
// antialiased unless you call cairo_set_antialias().
// <drawable>: a #GdkDrawable (a #GdkWindow or a #GdkPixmap).
// <gc>: a #GdkGC.
// <filled>: %TRUE if the arc should be filled, producing a 'pie slice'.
// <x>: the x coordinate of the left edge of the bounding rectangle.
// <y>: the y coordinate of the top edge of the bounding rectangle.
// <width>: the width of the bounding rectangle.
// <height>: the height of the bounding rectangle.
// <angle1>: the start angle of the arc, relative to the 3 o'clock position, counter-clockwise, in 1/64ths of a degree.
// <angle2>: the end angle of the arc, relative to @angle1, in 1/64ths of a degree.
static void draw_arc(AT0, AT1)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int filled, int x, int y, int width, int height, int angle1, int angle2) nothrow {
   gdk_draw_arc(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), filled, x, y, width, height, angle1, angle2);
}


// DEPRECATED (v2.22) function: draw_drawable - Use gdk_cairo_set_source_pixmap(), cairo_rectangle()
// Copies the @width x @height region of @src at coordinates (@xsrc,
// @ysrc) to coordinates (@xdest, @ydest) in @drawable.
// @width and/or @height may be given as -1, in which case the entire
// @src drawable will be copied.
// 
// Most fields in @gc are not used for this operation, but notably the
// clip mask or clip region will be honored.
// 
// The source and destination drawables must have the same visual and
// colormap, or errors will result. (On X11, failure to match
// visual/colormap results in a BadMatch error from the X server.)
// A common cause of this problem is an attempt to draw a bitmap to
// a color drawable. The way to draw a bitmap is to set the bitmap as 
// the stipple on the #GdkGC, set the fill mode to %GDK_STIPPLED, and 
// then draw the rectangle.
// 
// and cairo_fill() to draw pixmap on top of other drawables. Also keep
// in mind that the limitations on allowed sources do not apply to Cairo.
// <drawable>: a #GdkDrawable
// <gc>: a #GdkGC sharing the drawable's visual and colormap
// <src>: the source #GdkDrawable, which may be the same as @drawable
// <xsrc>: X position in @src of rectangle to draw
// <ysrc>: Y position in @src of rectangle to draw
// <xdest>: X position in @drawable where the rectangle should be drawn
// <ydest>: Y position in @drawable where the rectangle should be drawn
// <width>: width of rectangle to draw, or -1 for entire @src width
// <height>: height of rectangle to draw, or -1 for entire @src height
static void draw_drawable(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, AT2 /*Drawable*/ src, int xsrc, int ysrc, int xdest, int ydest, int width, int height) nothrow {
   gdk_draw_drawable(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), UpCast!(Drawable*)(src), xsrc, ysrc, xdest, ydest, width, height);
}


// DEPRECATED (v2.22) function: draw_glyphs - Use pango_cairo_show_glyphs() instead.
// This is a low-level function; 99% of text rendering should be done
// using gdk_draw_layout() instead.
// 
// A glyph is a single image in a font. This function draws a sequence of
// glyphs.  To obtain a sequence of glyphs you have to understand a
// lot about internationalized text handling, which you don't want to
// understand; thus, use gdk_draw_layout() instead of this function,
// gdk_draw_layout() handles the details.
// <drawable>: a #GdkDrawable
// <gc>: a #GdkGC
// <font>: font to be used
// <x>: X coordinate of baseline origin
// <y>: Y coordinate of baseline origin
// <glyphs>: the glyph string to draw
static void draw_glyphs(AT0, AT1, AT2, AT3)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, AT2 /*Pango.Font*/ font, int x, int y, AT3 /*Pango.GlyphString*/ glyphs) nothrow {
   gdk_draw_glyphs(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), UpCast!(Pango.Font*)(font), x, y, UpCast!(Pango.GlyphString*)(glyphs));
}


// VERSION: 2.6
// DEPRECATED (v2.22) function: draw_glyphs_transformed - Use pango_cairo_show_glyphs() instead.
// Renders a #PangoGlyphString onto a drawable, possibly
// transforming the layed-out coordinates through a transformation
// matrix. Note that the transformation matrix for @font is not
// changed, so to produce correct rendering results, the @font
// must have been loaded using a #PangoContext with an identical
// transformation matrix to that passed in to this function.
// 
// See also gdk_draw_glyphs(), gdk_draw_layout().
// <drawable>: a #GdkDrawable
// <gc>: a #GdkGC
// <matrix>: a #PangoMatrix, or %NULL to use an identity transformation
// <font>: the font in which to draw the string
// <x>: the x position of the start of the string (in Pango units in user space coordinates)
// <y>: the y position of the baseline (in Pango units in user space coordinates)
// <glyphs>: the glyph string to draw
static void draw_glyphs_transformed(AT0, AT1, AT2, AT3, AT4)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, AT2 /*Pango.Matrix*/ matrix, AT3 /*Pango.Font*/ font, int x, int y, AT4 /*Pango.GlyphString*/ glyphs) nothrow {
   gdk_draw_glyphs_transformed(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), UpCast!(Pango.Matrix*)(matrix), UpCast!(Pango.Font*)(font), x, y, UpCast!(Pango.GlyphString*)(glyphs));
}

static void draw_gray_image(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int x, int y, int width, int height, RgbDither dith, AT2 /*ubyte*/ buf, int rowstride) nothrow {
   gdk_draw_gray_image(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), x, y, width, height, dith, UpCast!(ubyte*)(buf), rowstride);
}


// DEPRECATED (v2.22) function: draw_image - Do not use #GdkImage anymore, instead use Cairo image
// Draws a #GdkImage onto a drawable.
// The depth of the #GdkImage must match the depth of the #GdkDrawable.
// 
// surfaces.
// <drawable>: a #GdkDrawable (a #GdkWindow or a #GdkPixmap).
// <gc>: a #GdkGC.
// <image>: the #GdkImage to draw.
// <xsrc>: the left edge of the source rectangle within @image.
// <ysrc>: the top of the source rectangle within @image.
// <xdest>: the x coordinate of the destination within @drawable.
// <ydest>: the y coordinate of the destination within @drawable.
// <width>: the width of the area to be copied, or -1 to make the area extend to the right edge of @image.
// <height>: the height of the area to be copied, or -1 to make the area extend to the bottom edge of @image.
static void draw_image(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, AT2 /*Image*/ image, int xsrc, int ysrc, int xdest, int ydest, int width, int height) nothrow {
   gdk_draw_image(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), UpCast!(Image*)(image), xsrc, ysrc, xdest, ydest, width, height);
}

static void draw_indexed_image(AT0, AT1, AT2, AT3)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int x, int y, int width, int height, RgbDither dith, AT2 /*ubyte*/ buf, int rowstride, AT3 /*RgbCmap*/ cmap) nothrow {
   gdk_draw_indexed_image(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), x, y, width, height, dith, UpCast!(ubyte*)(buf), rowstride, UpCast!(RgbCmap*)(cmap));
}


// Render a #PangoLayout onto a GDK drawable
// 
// If the layout's #PangoContext has a transformation matrix set, then
// @x and @y specify the position of the top left corner of the
// bounding box (in device space) of the transformed layout.
// 
// If you're using GTK+, the usual way to obtain a #PangoLayout
// is gtk_widget_create_pango_layout().
// <drawable>: the drawable on which to draw string
// <gc>: base graphics context to use
// <x>: the X position of the left of the layout (in pixels)
// <y>: the Y position of the top of the layout (in pixels)
// <layout>: a #PangoLayout
static void draw_layout(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int x, int y, AT2 /*Pango.Layout*/ layout) nothrow {
   gdk_draw_layout(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), x, y, UpCast!(Pango.Layout*)(layout));
}


// Render a #PangoLayoutLine onto an GDK drawable
// 
// If the layout's #PangoContext has a transformation matrix set, then
// @x and @y specify the position of the left edge of the baseline
// (left is in before-tranform user coordinates) in after-transform
// device coordinates.
// <drawable>: the drawable on which to draw the line
// <gc>: base graphics to use
// <x>: the x position of start of string (in pixels)
// <y>: the y position of baseline (in pixels)
// <line>: a #PangoLayoutLine
static void draw_layout_line(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int x, int y, AT2 /*Pango.LayoutLine*/ line) nothrow {
   gdk_draw_layout_line(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), x, y, UpCast!(Pango.LayoutLine*)(line));
}


// Render a #PangoLayoutLine onto a #GdkDrawable, overriding the
// layout's normal colors with @foreground and/or @background.
// @foreground and @background need not be allocated.
// 
// If the layout's #PangoContext has a transformation matrix set, then
// @x and @y specify the position of the left edge of the baseline
// (left is in before-tranform user coordinates) in after-transform
// device coordinates.
// <drawable>: the drawable on which to draw the line
// <gc>: base graphics to use
// <x>: the x position of start of string (in pixels)
// <y>: the y position of baseline (in pixels)
// <line>: a #PangoLayoutLine
// <foreground>: foreground override color, or %NULL for none
// <background>: background override color, or %NULL for none
static void draw_layout_line_with_colors(AT0, AT1, AT2, AT3, AT4)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int x, int y, AT2 /*Pango.LayoutLine*/ line, AT3 /*Color*/ foreground=null, AT4 /*Color*/ background=null) nothrow {
   gdk_draw_layout_line_with_colors(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), x, y, UpCast!(Pango.LayoutLine*)(line), UpCast!(Color*)(foreground), UpCast!(Color*)(background));
}


// Render a #PangoLayout onto a #GdkDrawable, overriding the
// layout's normal colors with @foreground and/or @background.
// @foreground and @background need not be allocated.
// 
// If the layout's #PangoContext has a transformation matrix set, then
// @x and @y specify the position of the top left corner of the
// bounding box (in device space) of the transformed layout.
// 
// If you're using GTK+, the ususal way to obtain a #PangoLayout
// is gtk_widget_create_pango_layout().
// <drawable>: the drawable on which to draw string
// <gc>: base graphics context to use
// <x>: the X position of the left of the layout (in pixels)
// <y>: the Y position of the top of the layout (in pixels)
// <layout>: a #PangoLayout
// <foreground>: foreground override color, or %NULL for none
// <background>: background override color, or %NULL for none
static void draw_layout_with_colors(AT0, AT1, AT2, AT3, AT4)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int x, int y, AT2 /*Pango.Layout*/ layout, AT3 /*Color*/ foreground=null, AT4 /*Color*/ background=null) nothrow {
   gdk_draw_layout_with_colors(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), x, y, UpCast!(Pango.Layout*)(layout), UpCast!(Color*)(foreground), UpCast!(Color*)(background));
}


// DEPRECATED (v2.22) function: draw_line - Use cairo_line_to() and cairo_stroke() instead.
// Draws a line, using the foreground color and other attributes of 
// the #GdkGC.
// 
// Be aware that the default line width in Cairo is 2 pixels and that your
// coordinates need to describe the center of the line. To draw a single
// pixel wide pixel-aligned line, you would use:
// |[cairo_set_line_width (cr, 1.0);
// cairo_set_line_cap (cr, CAIRO_LINE_CAP_SQUARE);
// cairo_move_to (cr, 0.5, 0.5);
// cairo_line_to (cr, 9.5, 0.5);
// cairo_stroke (cr);]|
// See also <ulink url="http://cairographics.org/FAQ/#sharp_lines">the Cairo
// FAQ</ulink> on this topic.
// <drawable>: a #GdkDrawable (a #GdkWindow or a #GdkPixmap).
// <gc>: a #GdkGC.
// <x1_>: the x coordinate of the start point.
// <y1_>: the y coordinate of the start point.
// <x2_>: the x coordinate of the end point.
// <y2_>: the y coordinate of the end point.
static void draw_line(AT0, AT1)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int x1_, int y1_, int x2_, int y2_) nothrow {
   gdk_draw_line(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), x1_, y1_, x2_, y2_);
}


// DEPRECATED (v2.22) function: draw_lines - Use cairo_line_to() and cairo_stroke() instead. See the
// Draws a series of lines connecting the given points.
// The way in which joins between lines are draw is determined by the
// #GdkCapStyle value in the #GdkGC. This can be set with
// gdk_gc_set_line_attributes().
// 
// documentation of gdk_draw_line() for notes on line drawing with Cairo.
// <drawable>: a #GdkDrawable (a #GdkWindow or a #GdkPixmap).
// <gc>: a #GdkGC.
// <points>: an array of #GdkPoint structures specifying the endpoints of the
// <n_points>: the size of the @points array.
static void draw_lines(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, AT2 /*Point*/ points, int n_points) nothrow {
   gdk_draw_lines(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), UpCast!(Point*)(points), n_points);
}


// VERSION: 2.2
// DEPRECATED (v2.22) function: draw_pixbuf - Use gdk_cairo_set_source_pixbuf() and cairo_paint() or
// Renders a rectangular portion of a pixbuf to a drawable.  The destination
// drawable must have a colormap. All windows have a colormap, however, pixmaps
// only have colormap by default if they were created with a non-%NULL window 
// argument. Otherwise a colormap must be set on them with 
// gdk_drawable_set_colormap().
// 
// On older X servers, rendering pixbufs with an alpha channel involves round 
// trips to the X server, and may be somewhat slow.
// 
// If GDK is built with the Sun mediaLib library, the gdk_draw_pixbuf
// function is accelerated using mediaLib, which provides hardware
// acceleration on Intel, AMD, and Sparc chipsets.  If desired, mediaLib
// support can be turned off by setting the GDK_DISABLE_MEDIALIB environment
// variable.
// 
// 
// cairo_rectangle() and cairo_fill() instead.
// <drawable>: Destination drawable.
// <gc>: a #GdkGC, used for clipping, or %NULL
// <pixbuf>: a #GdkPixbuf
// <src_x>: Source X coordinate within pixbuf.
// <src_y>: Source Y coordinates within pixbuf.
// <dest_x>: Destination X coordinate within drawable.
// <dest_y>: Destination Y coordinate within drawable.
// <width>: Width of region to render, in pixels, or -1 to use pixbuf width.
// <height>: Height of region to render, in pixels, or -1 to use pixbuf height.
// <dither>: Dithering mode for #GdkRGB.
// <x_dither>: X offset for dither.
// <y_dither>: Y offset for dither.
static void draw_pixbuf(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, AT2 /*GdkPixbuf2.Pixbuf*/ pixbuf, int src_x, int src_y, int dest_x, int dest_y, int width, int height, RgbDither dither, int x_dither, int y_dither) nothrow {
   gdk_draw_pixbuf(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), UpCast!(GdkPixbuf2.Pixbuf*)(pixbuf), src_x, src_y, dest_x, dest_y, width, height, dither, x_dither, y_dither);
}


// DEPRECATED (v2.22) function: draw_point - Use cairo_rectangle() and cairo_fill() or 
// Draws a point, using the foreground color and other attributes of 
// the #GdkGC.
// 
// cairo_move_to() and cairo_stroke() instead.
// <drawable>: a #GdkDrawable (a #GdkWindow or a #GdkPixmap).
// <gc>: a #GdkGC.
// <x>: the x coordinate of the point.
// <y>: the y coordinate of the point.
static void draw_point(AT0, AT1)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int x, int y) nothrow {
   gdk_draw_point(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), x, y);
}


// DEPRECATED (v2.22) function: draw_points - Use @n_points calls to cairo_rectangle() and
// Draws a number of points, using the foreground color and other 
// attributes of the #GdkGC.
// 
// cairo_fill() instead.
// <drawable>: a #GdkDrawable (a #GdkWindow or a #GdkPixmap).
// <gc>: a #GdkGC.
// <points>: an array of #GdkPoint structures.
// <n_points>: the number of points to be drawn.
static void draw_points(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, AT2 /*Point*/ points, int n_points) nothrow {
   gdk_draw_points(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), UpCast!(Point*)(points), n_points);
}


// DEPRECATED (v2.22) function: draw_polygon - Use cairo_line_to() or cairo_append_path() and
// Draws an outlined or filled polygon.
// 
// cairo_fill() or cairo_stroke() instead.
// <drawable>: a #GdkDrawable (a #GdkWindow or a #GdkPixmap).
// <gc>: a #GdkGC.
// <filled>: %TRUE if the polygon should be filled. The polygon is closed automatically, connecting the last point to the first point if necessary.
// <points>: an array of #GdkPoint structures specifying the points making up the polygon.
// <n_points>: the number of points.
static void draw_polygon(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int filled, AT2 /*Point*/ points, int n_points) nothrow {
   gdk_draw_polygon(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), filled, UpCast!(Point*)(points), n_points);
}


// DEPRECATED (v2.22) function: draw_rectangle - Use cairo_rectangle() and cairo_fill() or cairo_stroke()
// Draws a rectangular outline or filled rectangle, using the foreground color
// and other attributes of the #GdkGC.
// 
// A rectangle drawn filled is 1 pixel smaller in both dimensions than a 
// rectangle outlined. Calling 
// <literal>gdk_draw_rectangle (window, gc, TRUE, 0, 0, 20, 20)</literal> 
// results in a filled rectangle 20 pixels wide and 20 pixels high. Calling
// <literal>gdk_draw_rectangle (window, gc, FALSE, 0, 0, 20, 20)</literal> 
// results in an outlined rectangle with corners at (0, 0), (0, 20), (20, 20),
// and (20, 0), which makes it 21 pixels wide and 21 pixels high.
// 
// instead. For stroking, the same caveats for converting code apply as for
// gdk_draw_line().
// <drawable>: a #GdkDrawable (a #GdkWindow or a #GdkPixmap).
// <gc>: a #GdkGC.
// <filled>: %TRUE if the rectangle should be filled.
// <x>: the x coordinate of the left edge of the rectangle.
// <y>: the y coordinate of the top edge of the rectangle.
// <width>: the width of the rectangle.
// <height>: the height of the rectangle.
static void draw_rectangle(AT0, AT1)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int filled, int x, int y, int width, int height) nothrow {
   gdk_draw_rectangle(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), filled, x, y, width, height);
}

static void draw_rgb_32_image(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int x, int y, int width, int height, RgbDither dith, AT2 /*ubyte*/ buf, int rowstride) nothrow {
   gdk_draw_rgb_32_image(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), x, y, width, height, dith, UpCast!(ubyte*)(buf), rowstride);
}


// DEPRECATED (v2.22) function: draw_rgb_32_image_dithalign - Cairo handles colors automatically.
// Like gdk_draw_rgb_32_image(), but allows you to specify the dither
// offsets. See gdk_draw_rgb_image_dithalign() for more details.
// <drawable>: a #GdkDrawable
// <gc>: a #GdkGC
// <x>: X coordinate on @drawable where image should go
// <y>: Y coordinate on @drawable where image should go
// <width>: width of area of image to draw
// <height>: height of area of image to draw
// <dith>: dithering mode
// <buf>: RGB image data
// <rowstride>: rowstride of RGB image data
// <xdith>: X dither offset
// <ydith>: Y dither offset
static void draw_rgb_32_image_dithalign(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int x, int y, int width, int height, RgbDither dith, AT2 /*ubyte*/ buf, int rowstride, int xdith, int ydith) nothrow {
   gdk_draw_rgb_32_image_dithalign(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), x, y, width, height, dith, UpCast!(ubyte*)(buf), rowstride, xdith, ydith);
}

static void draw_rgb_image(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int x, int y, int width, int height, RgbDither dith, AT2 /*ubyte*/ rgb_buf, int rowstride) nothrow {
   gdk_draw_rgb_image(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), x, y, width, height, dith, UpCast!(ubyte*)(rgb_buf), rowstride);
}

static void draw_rgb_image_dithalign(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, int x, int y, int width, int height, RgbDither dith, AT2 /*ubyte*/ rgb_buf, int rowstride, int xdith, int ydith) nothrow {
   gdk_draw_rgb_image_dithalign(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), x, y, width, height, dith, UpCast!(ubyte*)(rgb_buf), rowstride, xdith, ydith);
}


// DEPRECATED (v2.22) function: draw_segments - Use cairo_move_to(), cairo_line_to() and cairo_stroke()
// Draws a number of unconnected lines.
// 
// instead. See the documentation of gdk_draw_line() for notes on line drawing
// with Cairo.
// <drawable>: a #GdkDrawable (a #GdkWindow or a #GdkPixmap).
// <gc>: a #GdkGC.
// <segs>: an array of #GdkSegment structures specifying the start and end points of the lines to be drawn.
// <n_segs>: the number of line segments to draw, i.e. the size of the @segs array.
static void draw_segments(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, AT2 /*Segment*/ segs, int n_segs) nothrow {
   gdk_draw_segments(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), UpCast!(Segment*)(segs), n_segs);
}


// DEPRECATED (v2.4) function: draw_string - Use gdk_draw_layout() instead.
// Draws a string of characters in the given font or fontset.
// <drawable>: a #GdkDrawable (a #GdkWindow or a #GdkPixmap).
// <font>: a #GdkFont.
// <gc>: a #GdkGC.
// <x>: the x coordinate of the left edge of the text.
// <y>: the y coordinate of the baseline of the text.
// <string>: the string of characters to draw.
static void draw_string(AT0, AT1, AT2, AT3)(AT0 /*Drawable*/ drawable, AT1 /*Font*/ font, AT2 /*GC*/ gc, int x, int y, AT3 /*char*/ string_) nothrow {
   gdk_draw_string(UpCast!(Drawable*)(drawable), UpCast!(Font*)(font), UpCast!(GC*)(gc), x, y, toCString!(char*)(string_));
}


// DEPRECATED (v2.4) function: draw_text - Use gdk_draw_layout() instead.
// Draws a number of characters in the given font or fontset.
// <drawable>: a #GdkDrawable (a #GdkWindow or a #GdkPixmap).
// <font>: a #GdkFont.
// <gc>: a #GdkGC.
// <x>: the x coordinate of the left edge of the text.
// <y>: the y coordinate of the baseline of the text.
// <text>: the characters to draw.
// <text_length>: the number of characters of @text to draw.
static void draw_text(AT0, AT1, AT2, AT3)(AT0 /*Drawable*/ drawable, AT1 /*Font*/ font, AT2 /*GC*/ gc, int x, int y, AT3 /*char*/ text, int text_length) nothrow {
   gdk_draw_text(UpCast!(Drawable*)(drawable), UpCast!(Font*)(font), UpCast!(GC*)(gc), x, y, toCString!(char*)(text), text_length);
}


// DEPRECATED (v2.4) function: draw_text_wc - Use gdk_draw_layout() instead.
// Draws a number of wide characters using the given font of fontset.
// If the font is a 1-byte font, the string is converted into 1-byte 
// characters (discarding the high bytes) before output.
// <drawable>: a #GdkDrawable (a #GdkWindow or a #GdkPixmap).
// <font>: a #GdkFont.
// <gc>: a #GdkGC.
// <x>: the x coordinate of the left edge of the text.
// <y>: the y coordinate of the baseline of the text.
// <text>: the wide characters to draw.
// <text_length>: the number of characters to draw.
static void draw_text_wc(AT0, AT1, AT2, AT3)(AT0 /*Drawable*/ drawable, AT1 /*Font*/ font, AT2 /*GC*/ gc, int x, int y, AT3 /*WChar*/ text, int text_length) nothrow {
   gdk_draw_text_wc(UpCast!(Drawable*)(drawable), UpCast!(Font*)(font), UpCast!(GC*)(gc), x, y, UpCast!(WChar*)(text), text_length);
}


// VERSION: 2.6
// DEPRECATED (v2.22) function: draw_trapezoids - Use Cairo path contruction functions and cairo_fill()
// Draws a set of anti-aliased trapezoids. The trapezoids are
// combined using saturation addition, then drawn over the background
// as a set. This is low level functionality used internally to implement
// rotated underlines and backgrouds when rendering a PangoLayout and is
// likely not useful for applications.
// 
// 
// instead.
// <drawable>: a #GdkDrawable
// <gc>: a #GdkGC
// <trapezoids>: an array of #GdkTrapezoid structures
// <n_trapezoids>: the number of trapezoids to draw
static void draw_trapezoids(AT0, AT1, AT2)(AT0 /*Drawable*/ drawable, AT1 /*GC*/ gc, AT2 /*Trapezoid*/ trapezoids, int n_trapezoids) nothrow {
   gdk_draw_trapezoids(UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), UpCast!(Trapezoid*)(trapezoids), n_trapezoids);
}


// Ends the drag operation after a drop.
// 
// This function is called by the drag destination.
// <context>: a #GtkDragContext.
// <success>: %TRUE if the data was successfully received.
// <time_>: the timestamp for this operation.
static void drop_finish(AT0)(AT0 /*DragContext*/ context, int success, uint time_) nothrow {
   gdk_drop_finish(UpCast!(DragContext*)(context), success, time_);
}


// Accepts or rejects a drop. 
// 
// This function is called by the drag destination in response
// to a drop initiated by the drag source.
// <context>: a #GdkDragContext.
// <ok>: %TRUE if the drop is accepted.
// <time_>: the timestamp for this operation.
static void drop_reply(AT0)(AT0 /*DragContext*/ context, int ok, uint time_) nothrow {
   gdk_drop_reply(UpCast!(DragContext*)(context), ok, time_);
}

static int error_trap_pop()() nothrow {
   return gdk_error_trap_pop();
}

static void error_trap_push()() nothrow {
   gdk_error_trap_push();
}


// MOVED TO: Event.get
// Checks all open displays for a #GdkEvent to process,to be processed
// on, fetching events from the windowing system if necessary.
// See gdk_display_get_event().
// 
// are pending. The returned #GdkEvent should be freed with gdk_event_free().
// RETURNS: the next #GdkEvent to be processed, or %NULL if no events
static Event* /*new*/ event_get()() nothrow {
   return gdk_event_get();
}

// MOVED TO: Event.get_graphics_expose
static Event* /*new*/ event_get_graphics_expose(AT0)(AT0 /*Window*/ window) nothrow {
   return gdk_event_get_graphics_expose(UpCast!(Window*)(window));
}


// MOVED TO: Event.handler_set
// Sets the function to call to handle all events from GDK.
// 
// Note that GTK+ uses this to install its own event handler, so it is
// usually not useful for GTK+ applications. (Although an application
// can call this function then call gtk_main_do_event() to pass
// events to GTK+.)
// <func>: the function to call to handle events from GDK.
// <data>: user data to pass to the function.
// <notify>: the function to call when the handler function is removed, i.e. when gdk_event_handler_set() is called with another event handler.
static void event_handler_set(AT0)(EventFunc func, AT0 /*void*/ data, GLib2.DestroyNotify notify) nothrow {
   gdk_event_handler_set(func, UpCast!(void*)(data), notify);
}


// MOVED TO: Event.peek
// If there is an event waiting in the event queue of some open
// display, returns a copy of it. See gdk_display_peek_event().
// 
// events are in any queues. The returned #GdkEvent should be freed with
// gdk_event_free().
// RETURNS: a copy of the first #GdkEvent on some event queue, or %NULL if no
static Event* /*new*/ event_peek()() nothrow {
   return gdk_event_peek();
}


// VERSION: 2.12
// MOVED TO: Event.request_motions
// Request more motion notifies if @event is a motion notify hint event.
// This function should be used instead of gdk_window_get_pointer() to
// request further motion notifies, because it also works for extension
// events where motion notifies are provided for devices other than the
// core pointer. Coordinate extraction, processing and requesting more
// motion events from a %GDK_MOTION_NOTIFY event usually works like this:
// 
// |[
// { 
// /&ast; motion_event handler &ast;/
// x = motion_event->x;
// y = motion_event->y;
// /&ast; handle (x,y) motion &ast;/
// gdk_event_request_motions (motion_event); /&ast; handles is_hint events &ast;/
// }
// ]|
// <event>: a valid #GdkEvent
static void event_request_motions(AT0)(AT0 /*EventMotion*/ event) nothrow {
   gdk_event_request_motions(UpCast!(EventMotion*)(event));
}

// MOVED TO: Event.send_client_message_for_display
static int event_send_client_message_for_display(AT0, AT1)(AT0 /*Display*/ display, AT1 /*Event*/ event, NativeWindow winid) nothrow {
   return gdk_event_send_client_message_for_display(UpCast!(Display*)(display), UpCast!(Event*)(event), winid);
}

static int events_pending()() nothrow {
   return gdk_events_pending();
}

static void exit()(int error_code) nothrow {
   gdk_exit(error_code);
}

static void flush()() nothrow {
   gdk_flush();
}


// MOVED TO: Font.from_description
// Load a #GdkFont based on a Pango font description. This font will
// only be an approximation of the Pango font, and
// internationalization will not be handled correctly. This function
// should only be used for legacy code that cannot be easily converted
// to use Pango. Using Pango directly will produce better results.
// 
// cannot be loaded.
// RETURNS: the newly loaded font, or %NULL if the font
// <font_desc>: a #PangoFontDescription.
static Font* /*new*/ font_from_description(AT0)(AT0 /*Pango.FontDescription*/ font_desc) nothrow {
   return gdk_font_from_description(UpCast!(Pango.FontDescription*)(font_desc));
}

// MOVED TO: Font.from_description_for_display
static Font* /*new*/ font_from_description_for_display(AT0, AT1)(AT0 /*Display*/ display, AT1 /*Pango.FontDescription*/ font_desc) nothrow {
   return gdk_font_from_description_for_display(UpCast!(Display*)(display), UpCast!(Pango.FontDescription*)(font_desc));
}


// MOVED TO: Font.load
// Loads a font.
// 
// The font may be newly loaded or looked up the font in a cache. 
// You should make no assumptions about the initial reference count.
// RETURNS: a #GdkFont, or %NULL if the font could not be loaded.
// <font_name>: a XLFD describing the font to load.
static Font* /*new*/ font_load(AT0)(AT0 /*char*/ font_name) nothrow {
   return gdk_font_load(toCString!(char*)(font_name));
}

// MOVED TO: Font.load_for_display
static Font* /*new*/ font_load_for_display(AT0, AT1)(AT0 /*Display*/ display, AT1 /*char*/ font_name) nothrow {
   return gdk_font_load_for_display(UpCast!(Display*)(display), toCString!(char*)(font_name));
}

static Font* /*new*/ fontset_load(AT0)(AT0 /*char*/ fontset_name) nothrow {
   return gdk_fontset_load(toCString!(char*)(fontset_name));
}

static Font* /*new*/ fontset_load_for_display(AT0, AT1)(AT0 /*Display*/ display, AT1 /*char*/ fontset_name) nothrow {
   return gdk_fontset_load_for_display(UpCast!(Display*)(display), toCString!(char*)(fontset_name));
}

static void free_compound_text(AT0)(AT0 /*ubyte*/ ctext) nothrow {
   gdk_free_compound_text(UpCast!(ubyte*)(ctext));
}

static void free_text_list(AT0)(AT0 /*char**/ list) nothrow {
   gdk_free_text_list(toCString!(char**)(list));
}


// Unintrospectable function: get_default_root_window() / gdk_get_default_root_window()
// Obtains the root window (parent all other windows are inside)
// for the default display and screen.
// RETURNS: the default root window
static Window* get_default_root_window()() nothrow {
   return gdk_get_default_root_window();
}

static char* /*new*/ get_display()() nothrow {
   return gdk_get_display();
}


// VERSION: 2.2
// Gets the display name specified in the command line arguments passed
// to gdk_init() or gdk_parse_args(), if any.
// 
// this string is owned by GTK+ and must not be modified or freed.
// RETURNS: the display name, if specified explicitely, otherwise %NULL
static char* get_display_arg_name()() nothrow {
   return gdk_get_display_arg_name();
}

static char* get_program_class()() nothrow {
   return gdk_get_program_class();
}


// Gets whether event debugging output is enabled.
// RETURNS: %TRUE if event debugging output is enabled.
static int get_show_events()() nothrow {
   return gdk_get_show_events();
}

static int get_use_xshm()() nothrow {
   return gdk_get_use_xshm();
}

static void init(AT0)(/*inout*/ int* argc, /*inout*/ AT0 /*char***/ argv) nothrow {
   gdk_init(argc, toCString!(char***)(argv));
}


// Initialize the library for use.
// 
// Arguments:
// "argc" is the number of arguments.
// "argv" is an array of strings.
// 
// Results:
// "argc" and "argv" are modified to reflect any arguments
// which were not handled. (Such arguments should either
// be handled by the application or dismissed). If initialization
// fails, returns FALSE, otherwise TRUE.
// 
// Side effects:
// The library is initialized.
// 
// --------------------------------------------------------------
static int init_check(AT0)(/*inout*/ int* argc, /*inout*/ AT0 /*char***/ argv) nothrow {
   return gdk_init_check(argc, toCString!(char***)(argv));
}


// Unintrospectable function: input_add() / gdk_input_add()
// DEPRECATED (v2.14) function: input_add - Use g_io_add_watch() on a #GIOChannel
// Establish a callback when a condition becomes true on
// a file descriptor.
// 
// gdk_input_remove().
// RETURNS: a tag that can later be used as an argument to
// <source>: a file descriptor.
// <condition>: the condition.
// <function>: the callback function.
// <data>: callback data passed to @function.
static int input_add(AT0)(int source, InputCondition condition, InputFunction function_, AT0 /*void*/ data) nothrow {
   return gdk_input_add(source, condition, function_, UpCast!(void*)(data));
}


// DEPRECATED (v2.14) function: input_add_full - Use g_io_add_watch_full() on a #GIOChannel
// Establish a callback when a condition becomes true on
// a file descriptor.
// 
// gdk_input_remove().
// RETURNS: a tag that can later be used as an argument to
// <source>: a file descriptor.
// <condition>: the condition.
// <function>: the callback function.
// <data>: callback data passed to @function.
// <destroy>: callback function to call with @data when the input handler is removed.
static int input_add_full(AT0)(int source, InputCondition condition, InputFunction function_, AT0 /*void*/ data, GLib2.DestroyNotify destroy) nothrow {
   return gdk_input_add_full(source, condition, function_, UpCast!(void*)(data), destroy);
}

static void input_remove()(int tag) nothrow {
   gdk_input_remove(tag);
}

static void input_set_extension_events(AT0)(AT0 /*Window*/ window, int mask, ExtensionMode mode) nothrow {
   gdk_input_set_extension_events(UpCast!(Window*)(window), mask, mode);
}

static GrabStatus keyboard_grab(AT0)(AT0 /*Window*/ window, int owner_events, uint time_) nothrow {
   return gdk_keyboard_grab(UpCast!(Window*)(window), owner_events, time_);
}


// MOVED TO: KeyboardGrabInfo.libgtk_only
// Determines information about the current keyboard grab.
// This is not public API and must not be used by applications.
// 
// keyboard grabbed.
// RETURNS: %TRUE if this application currently has the
// <display>: the display for which to get the grab information
// <grab_window>: location to store current grab window
// <owner_events>: location to store boolean indicating whether the @owner_events flag to gdk_keyboard_grab() was %TRUE.
static int keyboard_grab_info_libgtk_only(AT0, AT1)(AT0 /*Display*/ display, AT1 /*Window**/ grab_window, int* owner_events) nothrow {
   return gdk_keyboard_grab_info_libgtk_only(UpCast!(Display*)(display), UpCast!(Window**)(grab_window), owner_events);
}


// Ungrabs the keyboard on the default display, if it is grabbed by this 
// application.
// <time_>: a timestamp from a #GdkEvent, or %GDK_CURRENT_TIME if no timestamp is available.
static void keyboard_ungrab()(uint time_) nothrow {
   gdk_keyboard_ungrab(time_);
}


// Obtains the upper- and lower-case versions of the keyval @symbol.
// Examples of keyvals are #GDK_a, #GDK_Enter, #GDK_F1, etc.
// <symbol>: a keyval
// <lower>: return location for lowercase version of @symbol
// <upper>: return location for uppercase version of @symbol
static void keyval_convert_case(AT0, AT1)(uint symbol, /*out*/ AT0 /*uint*/ lower, /*out*/ AT1 /*uint*/ upper) nothrow {
   gdk_keyval_convert_case(symbol, UpCast!(uint*)(lower), UpCast!(uint*)(upper));
}


// Converts a key name to a key value.
// 
// The names are the same as those in the
// <filename>&lt;gdk/gdkkeysyms.h&gt;</filename> header file
// but without the leading "GDK_KEY_".
// 
// if the key name is not a valid key
// RETURNS: the corresponding key value, or %GDK_KEY_VoidSymbol
// <keyval_name>: a key name
static uint keyval_from_name(AT0)(AT0 /*char*/ keyval_name) nothrow {
   return gdk_keyval_from_name(toCString!(char*)(keyval_name));
}

static int keyval_is_lower()(uint keyval) nothrow {
   return gdk_keyval_is_lower(keyval);
}

static int keyval_is_upper()(uint keyval) nothrow {
   return gdk_keyval_is_upper(keyval);
}


// Converts a key value into a symbolic name.
// 
// The names are the same as those in the
// <filename>&lt;gdk/gdkkeysyms.h&gt;</filename> header file
// but without the leading "GDK_KEY_".
// 
// or %NULL if @keyval is not a valid key. The string should not be
// modified.
// RETURNS: a string containing the name of the key,
// <keyval>: a key value
static char* keyval_name()(uint keyval) nothrow {
   return gdk_keyval_name(keyval);
}

static uint keyval_to_lower()(uint keyval) nothrow {
   return gdk_keyval_to_lower(keyval);
}


// Convert from a GDK key symbol to the corresponding ISO10646 (Unicode)
// character.
// 
// is no corresponding character.
// RETURNS: the corresponding unicode character, or 0 if there
// <keyval>: a GDK key symbol
static uint keyval_to_unicode()(uint keyval) nothrow {
   return gdk_keyval_to_unicode(keyval);
}

static uint keyval_to_upper()(uint keyval) nothrow {
   return gdk_keyval_to_upper(keyval);
}


// Lists the available visuals for the default screen.
// (See gdk_screen_list_visuals())
// A visual describes a hardware image data format.
// For example, a visual might support 24-bit color, or 8-bit color,
// and might expect pixels to be in a certain format.
// 
// Call g_list_free() on the return value when you're finished with it.
// 
// a list of visuals; the list must be freed, but not its contents
static GLib2.List* /*new container*/ list_visuals()() nothrow {
   return gdk_list_visuals();
}


// Converts a multi-byte string to a wide character string.
// (The function name comes from an acronym of 'Multi-Byte String TO Wide
// Character String').
// 
// the conversion failed.
// RETURNS: the number of wide characters written into @dest, or -1 if
// <dest>: the space to place the converted wide character string into.
// <src>: the multi-byte string to convert, which must be nul-terminated.
// <dest_max>: the maximum number of wide characters to place in @dest.
static int mbstowcs(AT0, AT1)(AT0 /*WChar*/ dest, AT1 /*char*/ src, int dest_max) nothrow {
   return gdk_mbstowcs(UpCast!(WChar*)(dest), toCString!(char*)(src), dest_max);
}


// VERSION: 2.2
// Indicates to the GUI environment that the application has finished
// loading. If the applications opens windows, this function is
// normally called after opening the application's initial set of
// windows.
// 
// GTK+ will call this function automatically after opening the first
// #GtkWindow unless gtk_window_set_auto_startup_notification() is called 
// to disable that feature.
static void notify_startup_complete()() nothrow {
   gdk_notify_startup_complete();
}


// VERSION: 2.12
// Indicates to the GUI environment that the application has finished
// loading, using a given identifier.
// 
// GTK+ will call this function automatically for #GtkWindow with custom
// startup-notification identifier unless
// gtk_window_set_auto_startup_notification() is called to disable
// that feature.
// <startup_id>: a startup-notification identifier, for which notification process should be completed
static void notify_startup_complete_with_id(AT0)(AT0 /*char*/ startup_id) nothrow {
   gdk_notify_startup_complete_with_id(toCString!(char*)(startup_id));
}


// Unintrospectable function: offscreen_window_get_embedder() / gdk_offscreen_window_get_embedder()
// VERSION: 2.18
// Gets the window that @window is embedded in.
// 
// embedded offscreen window
// RETURNS: the embedding #GdkWindow, or %NULL if @window is not an
// <window>: a #GdkWindow
static Window* offscreen_window_get_embedder(AT0)(AT0 /*Window*/ window) nothrow {
   return gdk_offscreen_window_get_embedder(UpCast!(Window*)(window));
}


// Unintrospectable function: offscreen_window_get_pixmap() / gdk_offscreen_window_get_pixmap()
// VERSION: 2.18
// Gets the offscreen pixmap that an offscreen window renders into.
// If you need to keep this around over window resizes, you need to
// add a reference to it.
// RETURNS: The offscreen pixmap, or %NULL if not offscreen
// <window>: a #GdkWindow
static Pixmap* offscreen_window_get_pixmap(AT0)(AT0 /*Window*/ window) nothrow {
   return gdk_offscreen_window_get_pixmap(UpCast!(Window*)(window));
}


// VERSION: 2.18
// Sets @window to be embedded in @embedder.
// 
// To fully embed an offscreen window, in addition to calling this
// function, it is also necessary to handle the #GdkWindow::pick-embedded-child
// signal on the @embedder and the #GdkWindow::to-embedder and
// #GdkWindow::from-embedder signals on @window.
// <window>: a #GdkWindow
// <embedder>: the #GdkWindow that @window gets embedded in
static void offscreen_window_set_embedder(AT0, AT1)(AT0 /*Window*/ window, AT1 /*Window*/ embedder) nothrow {
   gdk_offscreen_window_set_embedder(UpCast!(Window*)(window), UpCast!(Window*)(embedder));
}


// Unintrospectable function: pango_context_get() / gdk_pango_context_get()
// Creates a #PangoContext for the default GDK screen.
// 
// The context must be freed when you're finished with it.
// 
// When using GTK+, normally you should use gtk_widget_get_pango_context()
// instead of this function, to get the appropriate context for
// the widget you intend to render text onto.
// 
// The newly created context will have the default font options (see
// #cairo_font_options_t) for the default screen; if these options
// change it will not be updated. Using gtk_widget_get_pango_context()
// is more convenient if you want to keep a context around and track
// changes to the screen's font rendering settings.
// RETURNS: a new #PangoContext for the default display
static Pango.Context* pango_context_get()() nothrow {
   return gdk_pango_context_get();
}


// Unintrospectable function: pango_context_get_for_screen() / gdk_pango_context_get_for_screen()
// VERSION: 2.2
// Creates a #PangoContext for @screen.
// 
// The context must be freed when you're finished with it.
// 
// When using GTK+, normally you should use gtk_widget_get_pango_context()
// instead of this function, to get the appropriate context for
// the widget you intend to render text onto.
// 
// The newly created context will have the default font options
// (see #cairo_font_options_t) for the screen; if these options
// change it will not be updated. Using gtk_widget_get_pango_context()
// is more convenient if you want to keep a context around and track
// changes to the screen's font rendering settings.
// RETURNS: a new #PangoContext for @screen
// <screen>: the #GdkScreen for which the context is to be created.
static Pango.Context* pango_context_get_for_screen(AT0)(AT0 /*Screen*/ screen) nothrow {
   return gdk_pango_context_get_for_screen(UpCast!(Screen*)(screen));
}


// This function used to set the colormap to be used for drawing with
// @context. The colormap is now always derived from the graphics
// context used for drawing, so calling this function is no longer
// necessary.
// <context>: a #PangoContext
// <colormap>: a #GdkColormap
static void pango_context_set_colormap(AT0, AT1)(AT0 /*Pango.Context*/ context, AT1 /*Colormap*/ colormap) nothrow {
   gdk_pango_context_set_colormap(UpCast!(Pango.Context*)(context), UpCast!(Colormap*)(colormap));
}


// Unintrospectable function: pango_layout_get_clip_region() / gdk_pango_layout_get_clip_region()
// Obtains a clip region which contains the areas where the given ranges
// of text would be drawn. @x_origin and @y_origin are the same position
// you would pass to gdk_draw_layout_line(). @index_ranges should contain
// ranges of bytes in the layout's text.
// 
// Note that the regions returned correspond to logical extents of the text
// ranges, not ink extents. So the drawn layout may in fact touch areas out of
// the clip region.  The clip region is mainly useful for highlightling parts
// of text, such as when text is selected.
// RETURNS: a clip region containing the given ranges
// <layout>: a #PangoLayout
// <x_origin>: X pixel where you intend to draw the layout with this clip
// <y_origin>: Y pixel where you intend to draw the layout with this clip
// <index_ranges>: array of byte indexes into the layout, where even members of array are start indexes and odd elements are end indexes
// <n_ranges>: number of ranges in @index_ranges, i.e. half the size of @index_ranges
static Region* pango_layout_get_clip_region(AT0)(AT0 /*Pango.Layout*/ layout, int x_origin, int y_origin, int* index_ranges, int n_ranges) nothrow {
   return gdk_pango_layout_get_clip_region(UpCast!(Pango.Layout*)(layout), x_origin, y_origin, index_ranges, n_ranges);
}


// Unintrospectable function: pango_layout_line_get_clip_region() / gdk_pango_layout_line_get_clip_region()
// Obtains a clip region which contains the areas where the given
// ranges of text would be drawn. @x_origin and @y_origin are the same
// position you would pass to gdk_draw_layout_line(). @index_ranges
// should contain ranges of bytes in the layout's text. The clip
// region will include space to the left or right of the line (to the
// layout bounding box) if you have indexes above or below the indexes
// contained inside the line. This is to draw the selection all the way
// to the side of the layout. However, the clip region is in line coordinates,
// not layout coordinates.
// 
// Note that the regions returned correspond to logical extents of the text
// ranges, not ink extents. So the drawn line may in fact touch areas out of
// the clip region.  The clip region is mainly useful for highlightling parts
// of text, such as when text is selected.
// RETURNS: a clip region containing the given ranges
// <line>: a #PangoLayoutLine
// <x_origin>: X pixel where you intend to draw the layout line with this clip
// <y_origin>: baseline pixel where you intend to draw the layout line with this clip
// <index_ranges>: array of byte indexes into the layout, where even members of array are start indexes and odd elements are end indexes
// <n_ranges>: number of ranges in @index_ranges, i.e. half the size of @index_ranges
static Region* pango_layout_line_get_clip_region(AT0)(AT0 /*Pango.LayoutLine*/ line, int x_origin, int y_origin, int* index_ranges, int n_ranges) nothrow {
   return gdk_pango_layout_line_get_clip_region(UpCast!(Pango.LayoutLine*)(line), x_origin, y_origin, index_ranges, n_ranges);
}


// VERSION: 2.2
// Parse command line arguments, and store for future
// use by calls to gdk_display_open().
// 
// Any arguments used by GDK are removed from the array and @argc and @argv are
// updated accordingly.
// 
// You shouldn't call this function explicitely if you are using
// gtk_init(), gtk_init_check(), gdk_init(), or gdk_init_check().
// <argc>: the number of command line arguments.
// <argv>: the array of command line arguments.
static void parse_args(AT0)(/*inout*/ int* argc, /*inout*/ AT0 /*char***/ argv) nothrow {
   gdk_parse_args(argc, toCString!(char***)(argv));
}


// Unintrospectable function: pixbuf_get_from_drawable() / gdk_pixbuf_get_from_drawable()
// Transfers image data from a #GdkDrawable and converts it to an RGB(A)
// representation inside a #GdkPixbuf. In other words, copies
// image data from a server-side drawable to a client-side RGB(A) buffer.
// This allows you to efficiently read individual pixels on the client side.
// 
// If the drawable @src has no colormap (gdk_drawable_get_colormap()
// returns %NULL), then a suitable colormap must be specified.
// Typically a #GdkWindow or a pixmap created by passing a #GdkWindow
// to gdk_pixmap_new() will already have a colormap associated with
// it.  If the drawable has a colormap, the @cmap argument will be
// ignored.  If the drawable is a bitmap (1 bit per pixel pixmap),
// then a colormap is not required; pixels with a value of 1 are
// assumed to be white, and pixels with a value of 0 are assumed to be
// black. For taking screenshots, gdk_colormap_get_system() returns
// the correct colormap to use.
// 
// If the specified destination pixbuf @dest is %NULL, then this
// function will create an RGB pixbuf with 8 bits per channel and no
// alpha, with the same size specified by the @width and @height
// arguments.  In this case, the @dest_x and @dest_y arguments must be
// specified as 0.  If the specified destination pixbuf is not %NULL
// and it contains alpha information, then the filled pixels will be
// set to full opacity (alpha = 255).
// 
// If the specified drawable is a pixmap, then the requested source
// rectangle must be completely contained within the pixmap, otherwise
// the function will return %NULL. For pixmaps only (not for windows)
// passing -1 for width or height is allowed to mean the full width
// or height of the pixmap.
// 
// If the specified drawable is a window, and the window is off the
// screen, then there is no image data in the obscured/offscreen
// regions to be placed in the pixbuf. The contents of portions of the
// pixbuf corresponding to the offscreen region are undefined.
// 
// If the window you're obtaining data from is partially obscured by
// other windows, then the contents of the pixbuf areas corresponding
// to the obscured regions are undefined.
// 
// If the target drawable is not mapped (typically because it's
// iconified/minimized or not on the current workspace), then %NULL
// will be returned.
// 
// If memory can't be allocated for the return value, %NULL will be returned
// instead.
// 
// (In short, there are several ways this function can fail, and if it fails
// it returns %NULL; so check the return value.)
// 
// This function calls gdk_drawable_get_image() internally and
// converts the resulting image to a #GdkPixbuf, so the
// documentation for gdk_drawable_get_image() may also be relevant.
// 
// pixbuf with a reference count of 1 if no destination pixbuf was specified, or %NULL on error
// RETURNS: The same pixbuf as @dest if it was non-%NULL, or a newly-created
// <dest>: Destination pixbuf, or %NULL if a new pixbuf should be created.
// <src>: Source drawable.
// <cmap>: A colormap if @src doesn't have one set.
// <src_x>: Source X coordinate within drawable.
// <src_y>: Source Y coordinate within drawable.
// <dest_x>: Destination X coordinate in pixbuf, or 0 if @dest is NULL.
// <dest_y>: Destination Y coordinate in pixbuf, or 0 if @dest is NULL.
// <width>: Width in pixels of region to get.
// <height>: Height in pixels of region to get.
static GdkPixbuf2.Pixbuf* pixbuf_get_from_drawable(AT0, AT1, AT2)(AT0 /*GdkPixbuf2.Pixbuf*/ dest, AT1 /*Drawable*/ src, AT2 /*Colormap*/ cmap, int src_x, int src_y, int dest_x, int dest_y, int width, int height) nothrow {
   return gdk_pixbuf_get_from_drawable(UpCast!(GdkPixbuf2.Pixbuf*)(dest), UpCast!(Drawable*)(src), UpCast!(Colormap*)(cmap), src_x, src_y, dest_x, dest_y, width, height);
}


// Unintrospectable function: pixbuf_get_from_image() / gdk_pixbuf_get_from_image()
// Same as gdk_pixbuf_get_from_drawable() but gets the pixbuf from
// an image.
// RETURNS: @dest, newly-created pixbuf if @dest was %NULL, %NULL on error
// <dest>: Destination pixbuf, or %NULL if a new pixbuf should be created.
// <src>: Source #GdkImage.
// <cmap>: A colormap, or %NULL to use the one for @src
// <src_x>: Source X coordinate within drawable.
// <src_y>: Source Y coordinate within drawable.
// <dest_x>: Destination X coordinate in pixbuf, or 0 if @dest is NULL.
// <dest_y>: Destination Y coordinate in pixbuf, or 0 if @dest is NULL.
// <width>: Width in pixels of region to get.
// <height>: Height in pixels of region to get.
static GdkPixbuf2.Pixbuf* pixbuf_get_from_image(AT0, AT1, AT2)(AT0 /*GdkPixbuf2.Pixbuf*/ dest, AT1 /*Image*/ src, AT2 /*Colormap*/ cmap, int src_x, int src_y, int dest_x, int dest_y, int width, int height) nothrow {
   return gdk_pixbuf_get_from_image(UpCast!(GdkPixbuf2.Pixbuf*)(dest), UpCast!(Image*)(src), UpCast!(Colormap*)(cmap), src_x, src_y, dest_x, dest_y, width, height);
}


// Creates a pixmap and a mask bitmap which are returned in the @pixmap_return
// and @mask_return arguments, respectively, and renders a pixbuf and its
// corresponding thresholded alpha mask to them.  This is merely a convenience
// function; applications that need to render pixbufs with dither offsets or to
// given drawables should use gdk_draw_pixbuf() and gdk_pixbuf_render_threshold_alpha().
// 
// The pixmap that is created is created for the colormap returned
// by gdk_rgb_get_colormap(). You normally will want to instead use
// the actual colormap for a widget, and use
// gdk_pixbuf_render_pixmap_and_mask_for_colormap().
// 
// If the pixbuf does not have an alpha channel, then *@mask_return will be set
// to %NULL.
// <pixbuf>: A pixbuf.
// <pixmap_return>: Location to store a pointer to the created pixmap, or %NULL if the pixmap is not needed.
// <mask_return>: Location to store a pointer to the created mask, or %NULL if the mask is not needed.
// <alpha_threshold>: Threshold value for opacity values.
static void pixbuf_render_pixmap_and_mask(AT0, AT1, AT2)(AT0 /*GdkPixbuf2.Pixbuf*/ pixbuf, AT1 /*Pixmap**/ pixmap_return, AT2 /*Bitmap**/ mask_return, int alpha_threshold) nothrow {
   gdk_pixbuf_render_pixmap_and_mask(UpCast!(GdkPixbuf2.Pixbuf*)(pixbuf), UpCast!(Pixmap**)(pixmap_return), UpCast!(Bitmap**)(mask_return), alpha_threshold);
}


// Creates a pixmap and a mask bitmap which are returned in the @pixmap_return
// and @mask_return arguments, respectively, and renders a pixbuf and its
// corresponding tresholded alpha mask to them.  This is merely a convenience
// function; applications that need to render pixbufs with dither offsets or to
// given drawables should use gdk_draw_pixbuf(), and gdk_pixbuf_render_threshold_alpha().
// 
// The pixmap that is created uses the #GdkColormap specified by @colormap.
// This colormap must match the colormap of the window where the pixmap
// will eventually be used or an error will result.
// 
// If the pixbuf does not have an alpha channel, then *@mask_return will be set
// to %NULL.
// <pixbuf>: A pixbuf.
// <colormap>: A #GdkColormap
// <pixmap_return>: Location to store a pointer to the created pixmap, or %NULL if the pixmap is not needed.
// <mask_return>: Location to store a pointer to the created mask, or %NULL if the mask is not needed.
// <alpha_threshold>: Threshold value for opacity values.
static void pixbuf_render_pixmap_and_mask_for_colormap(AT0, AT1, AT2, AT3)(AT0 /*GdkPixbuf2.Pixbuf*/ pixbuf, AT1 /*Colormap*/ colormap, AT2 /*Pixmap**/ pixmap_return, AT3 /*Bitmap**/ mask_return, int alpha_threshold) nothrow {
   gdk_pixbuf_render_pixmap_and_mask_for_colormap(UpCast!(GdkPixbuf2.Pixbuf*)(pixbuf), UpCast!(Colormap*)(colormap), UpCast!(Pixmap**)(pixmap_return), UpCast!(Bitmap**)(mask_return), alpha_threshold);
}


// Takes the opacity values in a rectangular portion of a pixbuf and thresholds
// them to produce a bi-level alpha mask that can be used as a clipping mask for
// a drawable.
// <pixbuf>: A pixbuf.
// <bitmap>: Bitmap where the bilevel mask will be painted to.
// <src_x>: Source X coordinate.
// <src_y>: source Y coordinate.
// <dest_x>: Destination X coordinate.
// <dest_y>: Destination Y coordinate.
// <width>: Width of region to threshold, or -1 to use pixbuf width
// <height>: Height of region to threshold, or -1 to use pixbuf height
// <alpha_threshold>: Opacity values below this will be painted as zero; all other values will be painted as one.
static void pixbuf_render_threshold_alpha(AT0, AT1)(AT0 /*GdkPixbuf2.Pixbuf*/ pixbuf, AT1 /*Bitmap*/ bitmap, int src_x, int src_y, int dest_x, int dest_y, int width, int height, int alpha_threshold) nothrow {
   gdk_pixbuf_render_threshold_alpha(UpCast!(GdkPixbuf2.Pixbuf*)(pixbuf), UpCast!(Bitmap*)(bitmap), src_x, src_y, dest_x, dest_y, width, height, alpha_threshold);
}


// DEPRECATED (v2.4) function: pixbuf_render_to_drawable - This function is obsolete. Use gdk_draw_pixbuf() instead.
// Renders a rectangular portion of a pixbuf to a drawable while using the
// specified GC.  This is done using GdkRGB, so the specified drawable must have
// the GdkRGB visual and colormap.  Note that this function will ignore the
// opacity information for images with an alpha channel; the GC must already
// have the clipping mask set if you want transparent regions to show through.
// 
// For an explanation of dither offsets, see the GdkRGB documentation.  In
// brief, the dither offset is important when re-rendering partial regions of an
// image to a rendered version of the full image, or for when the offsets to a
// base position change, as in scrolling.  The dither matrix has to be shifted
// for consistent visual results.  If you do not have any of these cases, the
// dither offsets can be both zero.
// <pixbuf>: A pixbuf.
// <drawable>: Destination drawable.
// <gc>: GC used for rendering.
// <src_x>: Source X coordinate within pixbuf.
// <src_y>: Source Y coordinate within pixbuf.
// <dest_x>: Destination X coordinate within drawable.
// <dest_y>: Destination Y coordinate within drawable.
// <width>: Width of region to render, in pixels, or -1 to use pixbuf width
// <height>: Height of region to render, in pixels, or -1 to use pixbuf height
// <dither>: Dithering mode for GdkRGB.
// <x_dither>: X offset for dither.
// <y_dither>: Y offset for dither.
static void pixbuf_render_to_drawable(AT0, AT1, AT2)(AT0 /*GdkPixbuf2.Pixbuf*/ pixbuf, AT1 /*Drawable*/ drawable, AT2 /*GC*/ gc, int src_x, int src_y, int dest_x, int dest_y, int width, int height, RgbDither dither, int x_dither, int y_dither) nothrow {
   gdk_pixbuf_render_to_drawable(UpCast!(GdkPixbuf2.Pixbuf*)(pixbuf), UpCast!(Drawable*)(drawable), UpCast!(GC*)(gc), src_x, src_y, dest_x, dest_y, width, height, dither, x_dither, y_dither);
}


// DEPRECATED (v2.4) function: pixbuf_render_to_drawable_alpha - This function is obsolete. Use gdk_draw_pixbuf() instead.
// Renders a rectangular portion of a pixbuf to a drawable.  The destination
// drawable must have a colormap. All windows have a colormap, however, pixmaps
// only have colormap by default if they were created with a non-%NULL window argument.
// Otherwise a colormap must be set on them with gdk_drawable_set_colormap.
// 
// On older X servers, rendering pixbufs with an alpha channel involves round trips
// to the X server, and may be somewhat slow.
// <pixbuf>: A pixbuf.
// <drawable>: Destination drawable.
// <src_x>: Source X coordinate within pixbuf.
// <src_y>: Source Y coordinates within pixbuf.
// <dest_x>: Destination X coordinate within drawable.
// <dest_y>: Destination Y coordinate within drawable.
// <width>: Width of region to render, in pixels, or -1 to use pixbuf width.
// <height>: Height of region to render, in pixels, or -1 to use pixbuf height.
// <alpha_mode>: Ignored. Present for backwards compatibility.
// <alpha_threshold>: Ignored. Present for backwards compatibility
// <dither>: Dithering mode for GdkRGB.
// <x_dither>: X offset for dither.
// <y_dither>: Y offset for dither.
static void pixbuf_render_to_drawable_alpha(AT0, AT1)(AT0 /*GdkPixbuf2.Pixbuf*/ pixbuf, AT1 /*Drawable*/ drawable, int src_x, int src_y, int dest_x, int dest_y, int width, int height, GdkPixbuf2.PixbufAlphaMode alpha_mode, int alpha_threshold, RgbDither dither, int x_dither, int y_dither) nothrow {
   gdk_pixbuf_render_to_drawable_alpha(UpCast!(GdkPixbuf2.Pixbuf*)(pixbuf), UpCast!(Drawable*)(drawable), src_x, src_y, dest_x, dest_y, width, height, alpha_mode, alpha_threshold, dither, x_dither, y_dither);
}

static GrabStatus pointer_grab(AT0, AT1, AT2)(AT0 /*Window*/ window, int owner_events, EventMask event_mask, AT1 /*Window*/ confine_to, AT2 /*Cursor*/ cursor, uint time_) nothrow {
   return gdk_pointer_grab(UpCast!(Window*)(window), owner_events, event_mask, UpCast!(Window*)(confine_to), UpCast!(Cursor*)(cursor), time_);
}


// Determines information about the current pointer grab.
// This is not public API and must not be used by applications.
// 
// pointer grabbed.
// RETURNS: %TRUE if this application currently has the
// <display>: the #GdkDisplay for which to get the grab information
// <grab_window>: location to store current grab window
// <owner_events>: location to store boolean indicating whether the @owner_events flag to gdk_pointer_grab() was %TRUE.
static int pointer_grab_info_libgtk_only(AT0, AT1)(AT0 /*Display*/ display, AT1 /*Window**/ grab_window, int* owner_events) nothrow {
   return gdk_pointer_grab_info_libgtk_only(UpCast!(Display*)(display), UpCast!(Window**)(grab_window), owner_events);
}


// Returns %TRUE if the pointer on the default display is currently 
// grabbed by this application.
// 
// Note that this does not take the inmplicit pointer grab on button
// presses into account.
// RETURNS: %TRUE if the pointer is currently grabbed by this application.*
static int pointer_is_grabbed()() nothrow {
   return gdk_pointer_is_grabbed();
}


// Ungrabs the pointer on the default display, if it is grabbed by this 
// application.
// <time_>: a timestamp from a #GdkEvent, or %GDK_CURRENT_TIME if no timestamp is available.
static void pointer_ungrab()(uint time_) nothrow {
   gdk_pointer_ungrab(time_);
}

static void pre_parse_libgtk_only()() nothrow {
   gdk_pre_parse_libgtk_only();
}

static void property_change(AT0, AT1)(AT0 /*Window*/ window, Atom property, Atom type, int format, PropMode mode, AT1 /*ubyte*/ data, int nelements) nothrow {
   gdk_property_change(UpCast!(Window*)(window), property, type, format, mode, UpCast!(ubyte*)(data), nelements);
}

static void property_delete(AT0)(AT0 /*Window*/ window, Atom property) nothrow {
   gdk_property_delete(UpCast!(Window*)(window), property);
}

static int property_get(AT0, AT1, AT2)(AT0 /*Window*/ window, Atom property, Atom type, c_ulong offset, c_ulong length, int pdelete, AT1 /*Atom*/ actual_property_type, int* actual_format, int* actual_length, AT2 /*ubyte**/ data) nothrow {
   return gdk_property_get(UpCast!(Window*)(window), property, type, offset, length, pdelete, UpCast!(Atom*)(actual_property_type), actual_format, actual_length, UpCast!(ubyte**)(data));
}


// This function returns the available bit depths for the default
// screen. It's equivalent to listing the visuals
// (gdk_list_visuals()) and then looking at the depth field in each
// visual, removing duplicates.
// 
// The array returned by this function should not be freed.
// <depths>: return location for available depths
// <count>: return location for number of available depths
static void query_depths()(/*out*/ int** depths, /*out*/ int* count) nothrow {
   gdk_query_depths(depths, count);
}


// This function returns the available visual types for the default
// screen. It's equivalent to listing the visuals
// (gdk_list_visuals()) and then looking at the type field in each
// visual, removing duplicates.
// 
// The array returned by this function should not be freed.
// <visual_types>: return location for the available visual types
// <count>: return location for the number of available visual types
static void query_visual_types(AT0)(AT0 /*VisualType**/ visual_types, int* count) nothrow {
   gdk_query_visual_types(UpCast!(VisualType**)(visual_types), count);
}

static int rgb_colormap_ditherable(AT0)(AT0 /*Colormap*/ cmap) nothrow {
   return gdk_rgb_colormap_ditherable(UpCast!(Colormap*)(cmap));
}

static int rgb_ditherable()() nothrow {
   return gdk_rgb_ditherable();
}


// DEPRECATED (v2.22) function: rgb_find_color - Cairo handles colors automatically.
// @colormap should be the colormap for the graphics context and
// drawable you're using to draw. If you're drawing to a #GtkWidget,
// call gtk_widget_get_colormap().
// 
// @color should have its %red, %green, and %blue fields initialized;
// gdk_rgb_find_color() will fill in the %pixel field with the best
// matching pixel from a color cube. The color is then ready to be
// used for drawing, e.g. you can call gdk_gc_set_foreground() which
// expects %pixel to be initialized.
// 
// In many cases, you can avoid this whole issue by calling
// gdk_gc_set_rgb_fg_color() or gdk_gc_set_rgb_bg_color(), which
// do not expect %pixel to be initialized in advance. If you use those
// functions, there's no need for gdk_rgb_find_color().
// <colormap>: a #GdkColormap
// <color>: a #GdkColor
static void rgb_find_color(AT0, AT1)(AT0 /*Colormap*/ colormap, AT1 /*Color*/ color) nothrow {
   gdk_rgb_find_color(UpCast!(Colormap*)(colormap), UpCast!(Color*)(color));
}

static void rgb_gc_set_background(AT0)(AT0 /*GC*/ gc, uint rgb) nothrow {
   gdk_rgb_gc_set_background(UpCast!(GC*)(gc), rgb);
}

static void rgb_gc_set_foreground(AT0)(AT0 /*GC*/ gc, uint rgb) nothrow {
   gdk_rgb_gc_set_foreground(UpCast!(GC*)(gc), rgb);
}


// DEPRECATED (v2.22) function: rgb_get_colormap - Use gdk_screen_get_system_colormap (gdk_screen_get_default ()) instead.
// Get the preferred colormap for rendering image data.  Not a
// very useful function; historically, GDK could only render RGB image
// data to one colormap and visual, but in the current version it can
// render to any colormap and visual. So there's no need to call this
// function.
// RETURNS: the preferred colormap
static Colormap* rgb_get_colormap()() nothrow {
   return gdk_rgb_get_colormap();
}


// DEPRECATED (v2.22) function: rgb_get_visual - Use gdk_visual_get_system_visual (gdk_screen_get_default ()) instead.
// Gets a "preferred visual" chosen by GdkRGB for rendering image data
// on the default screen. In previous versions of GDK, this was the
// only visual GdkRGB could use for rendering. In current versions,
// it's simply the visual GdkRGB would have chosen as the optimal one
// in those previous versions. GdkRGB can now render to drawables with
// any visual.
// RETURNS: The #GdkVisual chosen by GdkRGB.
static Visual* rgb_get_visual()() nothrow {
   return gdk_rgb_get_visual();
}

static void rgb_init()() nothrow {
   gdk_rgb_init();
}

static void rgb_set_install()(int install) nothrow {
   gdk_rgb_set_install(install);
}

static void rgb_set_min_colors()(int min_colors) nothrow {
   gdk_rgb_set_min_colors(min_colors);
}

static void rgb_set_verbose()(int verbose) nothrow {
   gdk_rgb_set_verbose(verbose);
}

static c_ulong rgb_xpixel_from_rgb()(uint rgb) nothrow {
   return gdk_rgb_xpixel_from_rgb(rgb);
}

static void selection_convert(AT0)(AT0 /*Window*/ requestor, Atom selection, Atom target, uint time_) nothrow {
   gdk_selection_convert(UpCast!(Window*)(requestor), selection, target, time_);
}

// Unintrospectable function: selection_owner_get() / gdk_selection_owner_get()
static Window* selection_owner_get()(Atom selection) nothrow {
   return gdk_selection_owner_get(selection);
}


// Unintrospectable function: selection_owner_get_for_display() / gdk_selection_owner_get_for_display()
// VERSION: 2.2
// Determine the owner of the given selection.
// 
// Note that the return value may be owned by a different 
// process if a foreign window was previously created for that
// window, but a new foreign window will never be created by this call. 
// 
// window known to the current process, the #GdkWindow that owns the 
// selection, otherwise %NULL.
// RETURNS: if there is a selection owner for this window, and it is a
// <display>: a #GdkDisplay.
// <selection>: an atom indentifying a selection.
static Window* selection_owner_get_for_display(AT0)(AT0 /*Display*/ display, Atom selection) nothrow {
   return gdk_selection_owner_get_for_display(UpCast!(Display*)(display), selection);
}

static int selection_owner_set(AT0)(AT0 /*Window*/ owner, Atom selection, uint time_, int send_event) nothrow {
   return gdk_selection_owner_set(UpCast!(Window*)(owner), selection, time_, send_event);
}


// VERSION: 2.2
// Sets the #GdkWindow @owner as the current owner of the selection @selection.
// 
// otherwise %FALSE.
// RETURNS: %TRUE if the selection owner was successfully changed to owner,
// <display>: the #GdkDisplay.
// <owner>: a #GdkWindow or %NULL to indicate that the owner for the given should be unset.
// <selection>: an atom identifying a selection.
// <time_>: timestamp to use when setting the selection. If this is older than the timestamp given last time the owner was set for the given selection, the request will be ignored.
// <send_event>: if %TRUE, and the new owner is different from the current owner, the current owner will be sent a SelectionClear event.
static int selection_owner_set_for_display(AT0, AT1)(AT0 /*Display*/ display, AT1 /*Window*/ owner, Atom selection, uint time_, int send_event) nothrow {
   return gdk_selection_owner_set_for_display(UpCast!(Display*)(display), UpCast!(Window*)(owner), selection, time_, send_event);
}


// Retrieves selection data that was stored by the selection
// data in response to a call to gdk_selection_convert(). This function
// will not be used by applications, who should use the #GtkClipboard
// API instead.
// RETURNS: the length of the retrieved data.
// <requestor>: the window on which the data is stored
// <data>: location to store a pointer to the retrieved data.
// <prop_type>: location to store the type of the property.
// <prop_format>: location to store the format of the property.
static int selection_property_get(AT0, AT1, AT2)(AT0 /*Window*/ requestor, AT1 /*ubyte**/ data, AT2 /*Atom*/ prop_type, int* prop_format) nothrow {
   return gdk_selection_property_get(UpCast!(Window*)(requestor), UpCast!(ubyte**)(data), UpCast!(Atom*)(prop_type), prop_format);
}

static void selection_send_notify()(NativeWindow requestor, Atom selection, Atom target, Atom property, uint time_) nothrow {
   gdk_selection_send_notify(requestor, selection, target, property, time_);
}


// VERSION: 2.2
// Send a response to SelectionRequest event.
// <display>: the #GdkDisplay where @requestor is realized
// <requestor>: window to which to deliver response.
// <selection>: selection that was requested.
// <target>: target that was selected.
// <property>: property in which the selection owner stored the data, or %GDK_NONE to indicate that the request was rejected.
// <time_>: timestamp.
static void selection_send_notify_for_display(AT0)(AT0 /*Display*/ display, NativeWindow requestor, Atom selection, Atom target, Atom property, uint time_) nothrow {
   gdk_selection_send_notify_for_display(UpCast!(Display*)(display), requestor, selection, target, property, time_);
}


// Set the double click time for the default display. See
// gdk_display_set_double_click_time(). 
// See also gdk_display_set_double_click_distance().
// Applications should <emphasis>not</emphasis> set this, it is a 
// global user-configured setting.
// <msec>: double click time in milliseconds (thousandths of a second)
static void set_double_click_time()(uint msec) nothrow {
   gdk_set_double_click_time(msec);
}

static char* /*new*/ set_locale()() nothrow {
   return gdk_set_locale();
}


// Unintrospectable function: set_pointer_hooks() / gdk_set_pointer_hooks()
// DEPRECATED (v2.24) function: set_pointer_hooks - This function will go away in GTK 3 for lack of use cases.
// This function allows for hooking into the operation
// of getting the current location of the pointer. This
// is only useful for such low-level tools as an
// event recorder. Applications should never have any
// reason to use this facility.
// 
// This function is not multihead safe. For multihead operation,
// see gdk_display_set_pointer_hooks().
// RETURNS: the previous pointer hook table
// <new_hooks>: a table of pointers to functions for getting quantities related to the current pointer position, or %NULL to restore the default table.
static PointerHooks* set_pointer_hooks(AT0)(AT0 /*PointerHooks*/ new_hooks) nothrow {
   return gdk_set_pointer_hooks(UpCast!(PointerHooks*)(new_hooks));
}

static void set_program_class(AT0)(AT0 /*char*/ program_class) nothrow {
   gdk_set_program_class(toCString!(char*)(program_class));
}


// Sets whether a trace of received events is output.
// Note that GTK+ must be compiled with debugging (that is,
// configured using the <option>--enable-debug</option> option)
// to use this option.
// <show_events>: %TRUE to output event debugging information.
static void set_show_events()(int show_events) nothrow {
   gdk_set_show_events(show_events);
}


// Sets the <literal>SM_CLIENT_ID</literal> property on the application's leader window so that
// the window manager can save the application's state using the X11R6 ICCCM
// session management protocol.
// 
// See the X Session Management Library documentation for more information on
// session management and the Inter-Client Communication Conventions Manual
// (ICCCM) for information on the <literal>WM_CLIENT_LEADER</literal> property. 
// (Both documents are part of the X Window System distribution.)
// 
// Deprecated:2.24: Use gdk_x11_set_sm_client_id() instead
// <sm_client_id>: the client id assigned by the session manager when the connection was opened, or %NULL to remove the property.
static void set_sm_client_id(AT0)(AT0 /*char*/ sm_client_id) nothrow {
   gdk_set_sm_client_id(toCString!(char*)(sm_client_id));
}

static void set_use_xshm()(int use_xshm) nothrow {
   gdk_set_use_xshm(use_xshm);
}


// Obtains a desktop-wide setting, such as the double-click time,
// for the default screen. See gdk_screen_get_setting().
// 
// in @value, %FALSE otherwise.
// RETURNS: %TRUE if the setting existed and a value was stored
// <name>: the name of the setting.
// <value>: location to store the value of the setting.
static int setting_get(AT0, AT1)(AT0 /*char*/ name, AT1 /*GObject2.Value*/ value) nothrow {
   return gdk_setting_get(toCString!(char*)(name), UpCast!(GObject2.Value*)(value));
}


// VERSION: 2.4
// DEPRECATED (v2.24) function: spawn_command_line_on_screen - This function is being removed in 3.0. Use
// Like g_spawn_command_line_async(), except the child process is
// spawned in such an environment that on calling gdk_display_open()
// it would be returned a #GdkDisplay with @screen as the default
// screen.
// 
// This is useful for applications which wish to launch an application
// on a specific screen.
// 
// 
// 
// either g_spawn_command_line_sync(), g_spawn_command_line_async() or 
// #GdkAppLaunchContext instead.
// RETURNS: %TRUE on success, %FALSE if error is set.
// <screen>: a #GdkScreen
// <command_line>: a command line
static int spawn_command_line_on_screen(AT0, AT1, AT2)(AT0 /*Screen*/ screen, AT1 /*char*/ command_line, AT2 /*GLib2.Error**/ error=null) nothrow {
   return gdk_spawn_command_line_on_screen(UpCast!(Screen*)(screen), toCString!(char*)(command_line), UpCast!(GLib2.Error**)(error));
}


// Unintrospectable function: spawn_on_screen() / gdk_spawn_on_screen()
// VERSION: 2.4
// DEPRECATED (v2.24) function: spawn_on_screen - This function is being removed in 3.0. Use
// Like g_spawn_async(), except the child process is spawned in such
// an environment that on calling gdk_display_open() it would be
// returned a #GdkDisplay with @screen as the default screen.
// 
// This is useful for applications which wish to launch an application
// on a specific screen.
// 
// 
// 
// either g_spawn_sync(), g_spawn_async(), or #GdkAppLaunchContext instead.
// RETURNS: %TRUE on success, %FALSE if error is set
// <screen>: a #GdkScreen
// <working_directory>: child's current working directory, or %NULL to inherit parent's
// <argv>: child's argument vector
// <envp>: child's environment, or %NULL to inherit parent's
// <flags>: flags from #GSpawnFlags
// <child_setup>: function to run in the child just before exec()
// <user_data>: user data for @child_setup
// <child_pid>: return location for child process ID, or %NULL
static int spawn_on_screen(AT0, AT1, AT2, AT3, AT4, AT5)(AT0 /*Screen*/ screen, AT1 /*char*/ working_directory, AT2 /*char**/ argv, AT3 /*char**/ envp, GLib2.SpawnFlags flags, GLib2.SpawnChildSetupFunc child_setup, AT4 /*void*/ user_data, int* child_pid, AT5 /*GLib2.Error**/ error=null) nothrow {
   return gdk_spawn_on_screen(UpCast!(Screen*)(screen), toCString!(char*)(working_directory), toCString!(char**)(argv), toCString!(char**)(envp), flags, child_setup, UpCast!(void*)(user_data), child_pid, UpCast!(GLib2.Error**)(error));
}


// Unintrospectable function: spawn_on_screen_with_pipes() / gdk_spawn_on_screen_with_pipes()
// VERSION: 2.4
// DEPRECATED (v2.24) function: spawn_on_screen_with_pipes - This function is being removed in 3.0. Use
// Like g_spawn_async_with_pipes(), except the child process is
// spawned in such an environment that on calling gdk_display_open()
// it would be returned a #GdkDisplay with @screen as the default
// screen.
// 
// This is useful for applications which wish to launch an application
// on a specific screen.
// 
// 
// 
// either g_spawn_async_with_pipes() or #GdkAppLaunchContext instead.
// RETURNS: %TRUE on success, %FALSE if an error was set
// <screen>: a #GdkScreen
// <working_directory>: child's current working directory, or %NULL to inherit parent's
// <argv>: child's argument vector
// <envp>: child's environment, or %NULL to inherit parent's
// <flags>: flags from #GSpawnFlags
// <child_setup>: function to run in the child just before exec()
// <user_data>: user data for @child_setup
// <child_pid>: return location for child process ID, or %NULL
// <standard_input>: return location for file descriptor to write to child's stdin, or %NULL
// <standard_output>: return location for file descriptor to read child's stdout, or %NULL
// <standard_error>: return location for file descriptor to read child's stderr, or %NULL
static int spawn_on_screen_with_pipes(AT0, AT1, AT2, AT3, AT4, AT5)(AT0 /*Screen*/ screen, AT1 /*char*/ working_directory, AT2 /*char**/ argv, AT3 /*char**/ envp, GLib2.SpawnFlags flags, GLib2.SpawnChildSetupFunc child_setup, AT4 /*void*/ user_data, int* child_pid, int* standard_input, int* standard_output, int* standard_error, AT5 /*GLib2.Error**/ error=null) nothrow {
   return gdk_spawn_on_screen_with_pipes(UpCast!(Screen*)(screen), toCString!(char*)(working_directory), toCString!(char**)(argv), toCString!(char**)(envp), flags, child_setup, UpCast!(void*)(user_data), child_pid, standard_input, standard_output, standard_error, UpCast!(GLib2.Error**)(error));
}


// Gets the metrics of a nul-terminated string.
// <font>: a #GdkFont.
// <string>: the nul-terminated string to measure.
// <lbearing>: the left bearing of the string.
// <rbearing>: the right bearing of the string.
// <width>: the width of the string.
// <ascent>: the ascent of the string.
// <descent>: the descent of the string.
static void string_extents(AT0, AT1)(AT0 /*Font*/ font, AT1 /*char*/ string_, int* lbearing, int* rbearing, int* width, int* ascent, int* descent) nothrow {
   gdk_string_extents(UpCast!(Font*)(font), toCString!(char*)(string_), lbearing, rbearing, width, ascent, descent);
}


// Determines the total height of a given nul-terminated
// string. This value is not generally useful, because you
// cannot determine how this total height will be drawn in
// relation to the baseline. See gdk_string_extents().
// RETURNS: the height of the string in pixels.
// <font>: a #GdkFont
// <string>: the nul-terminated string to measure.
static int string_height(AT0, AT1)(AT0 /*Font*/ font, AT1 /*char*/ string_) nothrow {
   return gdk_string_height(UpCast!(Font*)(font), toCString!(char*)(string_));
}


// Determines the distance from the origin to the rightmost
// portion of a nul-terminated string when drawn. This is not the
// correct value for determining the origin of the next
// portion when drawing text in multiple pieces.
// See gdk_string_width().
// RETURNS: the right bearing of the string in pixels.
// <font>: a #GdkFont
// <string>: the nul-terminated string to measure.
static int string_measure(AT0, AT1)(AT0 /*Font*/ font, AT1 /*char*/ string_) nothrow {
   return gdk_string_measure(UpCast!(Font*)(font), toCString!(char*)(string_));
}

static int string_to_compound_text(AT0, AT1, AT2)(AT0 /*char*/ str, AT1 /*Atom*/ encoding, int* format, AT2 /*ubyte**/ ctext, int* length) nothrow {
   return gdk_string_to_compound_text(toCString!(char*)(str), UpCast!(Atom*)(encoding), format, UpCast!(ubyte**)(ctext), length);
}


// VERSION: 2.2
// Convert a string from the encoding of the current 
// locale into a form suitable for storing in a window property.
// 
// 
// 
// Deprecated:2.24: Use gdk_x11_display_string_to_compound_text()
// RETURNS: 0 upon success, non-zero upon failure.
// <display>: the #GdkDisplay where the encoding is defined.
// <str>: a nul-terminated string.
// <encoding>: location to store the encoding atom (to be used as the type for the property).
// <format>: location to store the format of the property
// <ctext>: location to store newly allocated data for the property.
// <length>: the length of @text, in bytes
static int string_to_compound_text_for_display(AT0, AT1, AT2, AT3)(AT0 /*Display*/ display, AT1 /*char*/ str, AT2 /*Atom*/ encoding, int* format, AT3 /*ubyte**/ ctext, int* length) nothrow {
   return gdk_string_to_compound_text_for_display(UpCast!(Display*)(display), toCString!(char*)(str), UpCast!(Atom*)(encoding), format, UpCast!(ubyte**)(ctext), length);
}


// Determines the width of a nul-terminated string.
// (The distance from the origin of the string to the 
// point where the next string in a sequence of strings
// should be drawn)
// RETURNS: the width of the string in pixels.
// <font>: a #GdkFont
// <string>: the nul-terminated string to measure
static int string_width(AT0, AT1)(AT0 /*Font*/ font, AT1 /*char*/ string_) nothrow {
   return gdk_string_width(UpCast!(Font*)(font), toCString!(char*)(string_));
}

static void synthesize_window_state(AT0)(AT0 /*Window*/ window, WindowState unset_flags, WindowState set_flags) nothrow {
   gdk_synthesize_window_state(UpCast!(Window*)(window), unset_flags, set_flags);
}


// VERSION: 2.14
// This function retrieves a pixel from @window to force the windowing
// system to carry out any pending rendering commands.
// This function is intended to be used to syncronize with rendering
// pipelines, to benchmark windowing system rendering operations.
// <window>: a mapped #GdkWindow
static void test_render_sync(AT0)(AT0 /*Window*/ window) nothrow {
   gdk_test_render_sync(UpCast!(Window*)(window));
}


// VERSION: 2.14
// This function is intended to be used in GTK+ test programs.
// It will warp the mouse pointer to the given (@x,@y) corrdinates
// within @window and simulate a button press or release event.
// Because the mouse pointer needs to be warped to the target
// location, use of this function outside of test programs that
// run in their own virtual windowing system (e.g. Xvfb) is not
// recommended.
// 
// Also, gtk_test_simulate_button() is a fairly low level function,
// for most testing purposes, gtk_test_widget_click() is the right
// function to call which will generate a button press event followed
// by its accompanying button release event.
// 
// were carried out successfully.
// RETURNS: whether all actions neccessary for a button event simulation
// <window>: a #GdkWindow to simulate a button event for.
// <x>: x coordinate within @window for the button event.
// <y>: y coordinate within @window for the button event.
// <button>: Number of the pointer button for the event, usually 1, 2 or 3.
// <modifiers>: Keyboard modifiers the event is setup with.
// <button_pressrelease>: either %GDK_BUTTON_PRESS or %GDK_BUTTON_RELEASE
static int test_simulate_button(AT0)(AT0 /*Window*/ window, int x, int y, uint button, ModifierType modifiers, EventType button_pressrelease) nothrow {
   return gdk_test_simulate_button(UpCast!(Window*)(window), x, y, button, modifiers, button_pressrelease);
}


// VERSION: 2.14
// This function is intended to be used in GTK+ test programs.
// If (@x,@y) are > (-1,-1), it will warp the mouse pointer to
// the given (@x,@y) corrdinates within @window and simulate a
// key press or release event.
// 
// When the mouse pointer is warped to the target location, use
// of this function outside of test programs that run in their
// own virtual windowing system (e.g. Xvfb) is not recommended.
// If (@x,@y) are passed as (-1,-1), the mouse pointer will not
// be warped and @window origin will be used as mouse pointer
// location for the event.
// 
// Also, gtk_test_simulate_key() is a fairly low level function,
// for most testing purposes, gtk_test_widget_send_key() is the
// right function to call which will generate a key press event
// followed by its accompanying key release event.
// 
// were carried out successfully.
// RETURNS: whether all actions neccessary for a key event simulation
// <window>: a #GdkWindow to simulate a key event for.
// <x>: x coordinate within @window for the key event.
// <y>: y coordinate within @window for the key event.
// <keyval>: A GDK keyboard value.
// <modifiers>: Keyboard modifiers the event is setup with.
// <key_pressrelease>: either %GDK_KEY_PRESS or %GDK_KEY_RELEASE
static int test_simulate_key(AT0)(AT0 /*Window*/ window, int x, int y, uint keyval, ModifierType modifiers, EventType key_pressrelease) nothrow {
   return gdk_test_simulate_key(UpCast!(Window*)(window), x, y, keyval, modifiers, key_pressrelease);
}

static void text_extents(AT0, AT1)(AT0 /*Font*/ font, AT1 /*char*/ text, int text_length, int* lbearing, int* rbearing, int* width, int* ascent, int* descent) nothrow {
   gdk_text_extents(UpCast!(Font*)(font), toCString!(char*)(text), text_length, lbearing, rbearing, width, ascent, descent);
}

static void text_extents_wc(AT0, AT1)(AT0 /*Font*/ font, AT1 /*WChar*/ text, int text_length, int* lbearing, int* rbearing, int* width, int* ascent, int* descent) nothrow {
   gdk_text_extents_wc(UpCast!(Font*)(font), UpCast!(WChar*)(text), text_length, lbearing, rbearing, width, ascent, descent);
}


// Determines the total height of a given string.
// This value is not generally useful, because you cannot
// determine how this total height will be drawn in
// relation to the baseline. See gdk_text_extents().
// RETURNS: the height of the string in pixels.
// <font>: a #GdkFont
// <text>: the text to measure.
// <text_length>: the length of the text in bytes.
static int text_height(AT0, AT1)(AT0 /*Font*/ font, AT1 /*char*/ text, int text_length) nothrow {
   return gdk_text_height(UpCast!(Font*)(font), toCString!(char*)(text), text_length);
}


// Determines the distance from the origin to the rightmost
// portion of a string when drawn. This is not the
// correct value for determining the origin of the next
// portion when drawing text in multiple pieces. 
// See gdk_text_width().
// RETURNS: the right bearing of the string in pixels.
// <font>: a #GdkFont
// <text>: the text to measure.
// <text_length>: the length of the text in bytes.
static int text_measure(AT0, AT1)(AT0 /*Font*/ font, AT1 /*char*/ text, int text_length) nothrow {
   return gdk_text_measure(UpCast!(Font*)(font), toCString!(char*)(text), text_length);
}

static int text_property_to_text_list(AT0, AT1)(Atom encoding, int format, AT0 /*ubyte*/ text, int length, AT1 /*char***/ list) nothrow {
   return gdk_text_property_to_text_list(encoding, format, UpCast!(ubyte*)(text), length, toCString!(char***)(list));
}


// VERSION: 2.2
// Convert a text string from the encoding as it is stored 
// in a property into an array of strings in the encoding of
// the current locale. (The elements of the array represent the
// nul-separated elements of the original text string.)
// 
// if the conversion failed. 
// 
// 
// Deprecated:2.24: Use gdk_x11_display_text_property_to_text_list()
// RETURNS: the number of strings stored in list, or 0,
// <display>: The #GdkDisplay where the encoding is defined.
// <encoding>: an atom representing the encoding. The most common values for this are STRING, or COMPOUND_TEXT. This is value used as the type for the property.
// <format>: the format of the property.
// <text>: The text data.
// <length>: The number of items to transform.
// <list>: location to store a terminated array of strings in the encoding of the current locale. This array should be freed using gdk_free_text_list().
static int text_property_to_text_list_for_display(AT0, AT1, AT2)(AT0 /*Display*/ display, Atom encoding, int format, AT1 /*ubyte*/ text, int length, AT2 /*char***/ list) nothrow {
   return gdk_text_property_to_text_list_for_display(UpCast!(Display*)(display), encoding, format, UpCast!(ubyte*)(text), length, toCString!(char***)(list));
}


// Convert a text property in the giving encoding to
// a list of UTF-8 strings. 
// 
// list.
// RETURNS: the number of strings in the resulting
// <encoding>: an atom representing the encoding of the text
// <format>: the format of the property
// <text>: the text to convert
// <length>: the length of @text, in bytes
// <list>: location to store the list of strings or %NULL. The list should be freed with g_strfreev().
static int text_property_to_utf8_list(AT0, AT1)(Atom encoding, int format, AT0 /*ubyte*/ text, int length, AT1 /*char***/ list=null) nothrow {
   return gdk_text_property_to_utf8_list(encoding, format, UpCast!(ubyte*)(text), length, toCString!(char***)(list));
}


// VERSION: 2.2
// Converts a text property in the given encoding to
// a list of UTF-8 strings. 
// 
// list.
// RETURNS: the number of strings in the resulting
// <display>: a #GdkDisplay
// <encoding>: an atom representing the encoding of the text
// <format>: the format of the property
// <text>: the text to convert
// <length>: the length of @text, in bytes
// <list>: location to store the list of strings or %NULL. The list should be freed with g_strfreev().
static int text_property_to_utf8_list_for_display(AT0, AT1, AT2)(AT0 /*Display*/ display, Atom encoding, int format, AT1 /*ubyte*/ text, int length, AT2 /*char***/ list) nothrow {
   return gdk_text_property_to_utf8_list_for_display(UpCast!(Display*)(display), encoding, format, UpCast!(ubyte*)(text), length, toCString!(char***)(list));
}

static int text_width(AT0, AT1)(AT0 /*Font*/ font, AT1 /*char*/ text, int text_length) nothrow {
   return gdk_text_width(UpCast!(Font*)(font), toCString!(char*)(text), text_length);
}

static int text_width_wc(AT0, AT1)(AT0 /*Font*/ font, AT1 /*WChar*/ text, int text_length) nothrow {
   return gdk_text_width_wc(UpCast!(Font*)(font), UpCast!(WChar*)(text), text_length);
}


// Unintrospectable function: threads_add_idle() / gdk_threads_add_idle()
// VERSION: 2.12
// A wrapper for the common usage of gdk_threads_add_idle_full() 
// assigning the default priority, #G_PRIORITY_DEFAULT_IDLE.
// 
// See gdk_threads_add_idle_full().
// RETURNS: the ID (greater than 0) of the event source.
// <function>: function to call
// <data>: data to pass to @function
static uint threads_add_idle(AT0)(GLib2.SourceFunc function_, AT0 /*void*/ data) nothrow {
   return gdk_threads_add_idle(function_, UpCast!(void*)(data));
}


// VERSION: 2.12
// Adds a function to be called whenever there are no higher priority
// events pending.  If the function returns %FALSE it is automatically
// removed from the list of event sources and will not be called again.
// 
// This variant of g_idle_add_full() calls @function with the GDK lock
// held. It can be thought of a MT-safe version for GTK+ widgets for the 
// following use case, where you have to worry about idle_callback()
// running in thread A and accessing @self after it has been finalized
// in thread B:
// 
// |[
// static gboolean
// idle_callback (gpointer data)
// {
// /&ast; gdk_threads_enter(); would be needed for g_idle_add() &ast;/
// 
// SomeWidget *self = data;
// /&ast; do stuff with self &ast;/
// 
// self->idle_id = 0;
// 
// /&ast; gdk_threads_leave(); would be needed for g_idle_add() &ast;/
// return FALSE;
// }
// 
// static void
// some_widget_do_stuff_later (SomeWidget *self)
// {
// self->idle_id = gdk_threads_add_idle (idle_callback, self)
// /&ast; using g_idle_add() here would require thread protection in the callback &ast;/
// }
// 
// static void
// some_widget_finalize (GObject *object)
// {
// SomeWidget *self = SOME_WIDGET (object);
// if (self->idle_id)
// g_source_remove (self->idle_id);
// G_OBJECT_CLASS (parent_class)->finalize (object);
// }
// ]|
// RETURNS: the ID (greater than 0) of the event source.
// <priority>: the priority of the idle source. Typically this will be in the range btweeen #G_PRIORITY_DEFAULT_IDLE and #G_PRIORITY_HIGH_IDLE
// <function>: function to call
// <data>: data to pass to @function
// <notify>: function to call when the idle is removed, or %NULL
static uint threads_add_idle_full(AT0)(int priority, GLib2.SourceFunc function_, AT0 /*void*/ data, GLib2.DestroyNotify notify=null) nothrow {
   return gdk_threads_add_idle_full(priority, function_, UpCast!(void*)(data), notify);
}


// Unintrospectable function: threads_add_timeout() / gdk_threads_add_timeout()
// VERSION: 2.12
// A wrapper for the common usage of gdk_threads_add_timeout_full() 
// assigning the default priority, #G_PRIORITY_DEFAULT.
// 
// See gdk_threads_add_timeout_full().
// RETURNS: the ID (greater than 0) of the event source.
// <interval>: the time between calls to the function, in milliseconds (1/1000ths of a second)
// <function>: function to call
// <data>: data to pass to @function
static uint threads_add_timeout(AT0)(uint interval, GLib2.SourceFunc function_, AT0 /*void*/ data) nothrow {
   return gdk_threads_add_timeout(interval, function_, UpCast!(void*)(data));
}


// VERSION: 2.12
// Sets a function to be called at regular intervals holding the GDK lock,
// with the given priority.  The function is called repeatedly until it 
// returns %FALSE, at which point the timeout is automatically destroyed 
// and the function will not be called again.  The @notify function is
// called when the timeout is destroyed.  The first call to the
// function will be at the end of the first @interval.
// 
// Note that timeout functions may be delayed, due to the processing of other
// event sources. Thus they should not be relied on for precise timing.
// After each call to the timeout function, the time of the next
// timeout is recalculated based on the current time and the given interval
// (it does not try to 'catch up' time lost in delays).
// 
// This variant of g_timeout_add_full() can be thought of a MT-safe version 
// for GTK+ widgets for the following use case:
// 
// |[
// static gboolean timeout_callback (gpointer data)
// {
// SomeWidget *self = data;
// 
// /&ast; do stuff with self &ast;/
// 
// self->timeout_id = 0;
// 
// return FALSE;
// }
// 
// static void some_widget_do_stuff_later (SomeWidget *self)
// {
// self->timeout_id = g_timeout_add (timeout_callback, self)
// }
// 
// static void some_widget_finalize (GObject *object)
// {
// SomeWidget *self = SOME_WIDGET (object);
// 
// if (self->timeout_id)
// g_source_remove (self->timeout_id);
// 
// G_OBJECT_CLASS (parent_class)->finalize (object);
// }
// ]|
// RETURNS: the ID (greater than 0) of the event source.
// <priority>: the priority of the timeout source. Typically this will be in the range between #G_PRIORITY_DEFAULT_IDLE and #G_PRIORITY_HIGH_IDLE.
// <interval>: the time between calls to the function, in milliseconds (1/1000ths of a second)
// <function>: function to call
// <data>: data to pass to @function
// <notify>: function to call when the timeout is removed, or %NULL
static uint threads_add_timeout_full(AT0)(int priority, uint interval, GLib2.SourceFunc function_, AT0 /*void*/ data, GLib2.DestroyNotify notify=null) nothrow {
   return gdk_threads_add_timeout_full(priority, interval, function_, UpCast!(void*)(data), notify);
}


// Unintrospectable function: threads_add_timeout_seconds() / gdk_threads_add_timeout_seconds()
// VERSION: 2.14
// A wrapper for the common usage of gdk_threads_add_timeout_seconds_full() 
// assigning the default priority, #G_PRIORITY_DEFAULT.
// 
// For details, see gdk_threads_add_timeout_full().
// RETURNS: the ID (greater than 0) of the event source.
// <interval>: the time between calls to the function, in seconds
// <function>: function to call
// <data>: data to pass to @function
static uint threads_add_timeout_seconds(AT0)(uint interval, GLib2.SourceFunc function_, AT0 /*void*/ data) nothrow {
   return gdk_threads_add_timeout_seconds(interval, function_, UpCast!(void*)(data));
}


// VERSION: 2.14
// A variant of gdk_threads_add_timout_full() with second-granularity.
// See g_timeout_add_seconds_full() for a discussion of why it is
// a good idea to use this function if you don't need finer granularity.
// RETURNS: the ID (greater than 0) of the event source.
// <priority>: the priority of the timeout source. Typically this will be in the range between #G_PRIORITY_DEFAULT_IDLE and #G_PRIORITY_HIGH_IDLE.
// <interval>: the time between calls to the function, in seconds
// <function>: function to call
// <data>: data to pass to @function
// <notify>: function to call when the timeout is removed, or %NULL
static uint threads_add_timeout_seconds_full(AT0)(int priority, uint interval, GLib2.SourceFunc function_, AT0 /*void*/ data, GLib2.DestroyNotify notify=null) nothrow {
   return gdk_threads_add_timeout_seconds_full(priority, interval, function_, UpCast!(void*)(data), notify);
}

static void threads_enter()() nothrow {
   gdk_threads_enter();
}


// Initializes GDK so that it can be used from multiple threads
// in conjunction with gdk_threads_enter() and gdk_threads_leave().
// g_thread_init() must be called previous to this function.
// 
// This call must be made before any use of the main loop from
// GTK+; to be safe, call it before gtk_init().
static void threads_init()() nothrow {
   gdk_threads_init();
}

static void threads_leave()() nothrow {
   gdk_threads_leave();
}


// Unintrospectable function: threads_set_lock_functions() / gdk_threads_set_lock_functions()
// VERSION: 2.4
// Allows the application to replace the standard method that
// GDK uses to protect its data structures. Normally, GDK
// creates a single #GMutex that is locked by gdk_threads_enter(),
// and released by gdk_threads_leave(); using this function an
// application provides, instead, a function @enter_fn that is
// called by gdk_threads_enter() and a function @leave_fn that is
// called by gdk_threads_leave().
// 
// The functions must provide at least same locking functionality
// as the default implementation, but can also do extra application
// specific processing.
// 
// As an example, consider an application that has its own recursive
// lock that when held, holds the GTK+ lock as well. When GTK+ unlocks
// the GTK+ lock when entering a recursive main loop, the application
// must temporarily release its lock as well.
// 
// Most threaded GTK+ apps won't need to use this method.
// 
// This method must be called before gdk_threads_init(), and cannot
// be called multiple times.
// <enter_fn>: function called to guard GDK
// <leave_fn>: function called to release the guard
static void threads_set_lock_functions()(GObject2.Callback enter_fn, GObject2.Callback leave_fn) nothrow {
   gdk_threads_set_lock_functions(enter_fn, leave_fn);
}


// Convert from a ISO10646 character to a key symbol.
// 
// or, if there is no corresponding symbol, 
// wc | 0x01000000
// RETURNS: the corresponding GDK key symbol, if one exists.
// <wc>: a ISO10646 encoded character
static uint unicode_to_keyval()(uint wc) nothrow {
   return gdk_unicode_to_keyval(wc);
}


// Convert from UTF-8 to compound text. 
// 
// false.
// RETURNS: %TRUE if the conversion succeeded, otherwise
// <str>: a UTF-8 string
// <encoding>: location to store resulting encoding
// <format>: location to store format of the result
// <ctext>: location to store the data of the result
// <length>: location to store the length of the data stored in @ctext
static int utf8_to_compound_text(AT0, AT1, AT2)(AT0 /*char*/ str, AT1 /*Atom*/ encoding, int* format, AT2 /*ubyte**/ ctext, int* length) nothrow {
   return gdk_utf8_to_compound_text(toCString!(char*)(str), UpCast!(Atom*)(encoding), format, UpCast!(ubyte**)(ctext), length);
}


// VERSION: 2.2
// Converts from UTF-8 to compound text. 
// 
// %FALSE.
// 
// 
// Deprecated:2.24: Use gdk_x11_display_utf8_to_compound_text()
// RETURNS: %TRUE if the conversion succeeded, otherwise
// <display>: a #GdkDisplay
// <str>: a UTF-8 string
// <encoding>: location to store resulting encoding
// <format>: location to store format of the result
// <ctext>: location to store the data of the result
// <length>: location to store the length of the data stored in @ctext
static int utf8_to_compound_text_for_display(AT0, AT1, AT2, AT3)(AT0 /*Display*/ display, AT1 /*char*/ str, AT2 /*Atom*/ encoding, int* format, AT3 /*ubyte**/ ctext, int* length) nothrow {
   return gdk_utf8_to_compound_text_for_display(UpCast!(Display*)(display), toCString!(char*)(str), UpCast!(Atom*)(encoding), format, UpCast!(ubyte**)(ctext), length);
}


// Converts an UTF-8 string into the best possible representation
// as a STRING. The representation of characters not in STRING
// is not specified; it may be as pseudo-escape sequences
// \x{ABCD}, or it may be in some other form of approximation.
// 
// conversion failed. (It should not fail for
// any properly formed UTF-8 string unless system
// limits like memory or file descriptors are exceeded.)
// RETURNS: the newly-allocated string, or %NULL if the
// <str>: a UTF-8 string
static char* /*new*/ utf8_to_string_target(AT0)(AT0 /*char*/ str) nothrow {
   return gdk_utf8_to_string_target(toCString!(char*)(str));
}


// Converts a wide character string to a multi-byte string.
// (The function name comes from an acronym of 'Wide Character String TO
// Multi-Byte String').
// 
// conversion failed. The returned string should be freed with g_free() when no
// longer needed.
// RETURNS: the multi-byte string corresponding to @src, or %NULL if the
// <src>: a wide character string.
static char* /*new*/ wcstombs(AT0)(AT0 /*WChar*/ src) nothrow {
   return gdk_wcstombs(UpCast!(WChar*)(src));
}


// Obtains the window underneath the mouse pointer, returning the
// location of that window in @win_x, @win_y. Returns %NULL if the
// window under the mouse pointer is not known to GDK (if the window
// belongs to another application and a #GdkWindow hasn't been created
// for it with gdk_window_foreign_new())
// 
// NOTE: For multihead-aware widgets or applications use
// gdk_display_get_window_at_pointer() instead.
// RETURNS: window under the mouse pointer
// <win_x>: return location for origin of the window under the pointer
// <win_y>: return location for origin of the window under the pointer
static Window* window_at_pointer()(/*out*/ int* win_x=null, /*out*/ int* win_y=null) nothrow {
   return gdk_window_at_pointer(win_x, win_y);
}


// Constrains a desired width and height according to a
// set of geometry hints (such as minimum and maximum size).
// <geometry>: a #GdkGeometry structure
// <flags>: a mask indicating what portions of @geometry are set
// <width>: desired width of window
// <height>: desired height of the window
// <new_width>: location to store resulting width
// <new_height>: location to store resulting height
static void window_constrain_size(AT0)(AT0 /*Geometry*/ geometry, uint flags, int width, int height, /*out*/ int* new_width, /*out*/ int* new_height) nothrow {
   gdk_window_constrain_size(UpCast!(Geometry*)(geometry), flags, width, height, new_width, new_height);
}


// Unintrospectable function: window_foreign_new() / gdk_window_foreign_new()
// Wraps a native window for the default display in a #GdkWindow.
// This may fail if the window has been destroyed.
// 
// For example in the X backend, a native window handle is an Xlib
// <type>XID</type>.
// 
// native window or %NULL if the window has been destroyed.
// RETURNS: the newly-created #GdkWindow wrapper for the
// <anid>: a native window handle.
static Window* window_foreign_new()(NativeWindow anid) nothrow {
   return gdk_window_foreign_new(anid);
}


// Unintrospectable function: window_foreign_new_for_display() / gdk_window_foreign_new_for_display()
// VERSION: 2.2
// Wraps a native window in a #GdkWindow.
// This may fail if the window has been destroyed. If the window
// was already known to GDK, a new reference to the existing 
// #GdkWindow is returned.
// 
// For example in the X backend, a native window handle is an Xlib
// <type>XID</type>.
// 
// %NULL if the window has been destroyed. The wrapper will be
// newly created, if one doesn't exist already.
// 
// 
// Deprecated:2.24: Use gdk_x11_window_foreign_new_for_display() or
// equivalent backend-specific API instead
// RETURNS: a #GdkWindow wrapper for the native window or
// <display>: the #GdkDisplay where the window handle comes from.
// <anid>: a native window handle.
static Window* window_foreign_new_for_display(AT0)(AT0 /*Display*/ display, NativeWindow anid) nothrow {
   return gdk_window_foreign_new_for_display(UpCast!(Display*)(display), anid);
}


// Unintrospectable function: window_get_toplevels() / gdk_window_get_toplevels()
// DEPRECATED (v2.16) function: window_get_toplevels - Use gdk_screen_get_toplevel_windows() instead.
// Obtains a list of all toplevel windows known to GDK on the default
// screen (see gdk_screen_get_toplevel_windows()).
// A toplevel window is a child of the root window (see
// gdk_get_default_root_window()).
// 
// The returned list should be freed with g_list_free(), but
// its elements need not be freed.
// RETURNS: list of toplevel windows, free with g_list_free()
static GLib2.List* window_get_toplevels()() nothrow {
   return gdk_window_get_toplevels();
}


// Unintrospectable function: window_lookup() / gdk_window_lookup()
// DEPRECATED (v2.24) function: window_lookup - Use gdk_x11_window_lookup_for_display() or equivalent
// Looks up the #GdkWindow that wraps the given native window handle. 
// 
// For example in the X backend, a native window handle is an Xlib
// <type>XID</type>.
// 
// or %NULL if there is none.
// 
// backend-specific functionality instead
// RETURNS: the #GdkWindow wrapper for the native window,
// <anid>: a native window handle.
static Window* window_lookup()(NativeWindow anid) nothrow {
   return gdk_window_lookup(anid);
}


// Unintrospectable function: window_lookup_for_display() / gdk_window_lookup_for_display()
// VERSION: 2.2
// Looks up the #GdkWindow that wraps the given native window handle.
// 
// For example in the X backend, a native window handle is an Xlib
// <type>XID</type>.
// 
// or %NULL if there is none.
// 
// 
// Deprecated:2.24: Use gdk_x11_window_lookup_for_display() instead
// RETURNS: the #GdkWindow wrapper for the native window,
// <display>: the #GdkDisplay corresponding to the window handle
// <anid>: a native window handle.
static Window* window_lookup_for_display(AT0)(AT0 /*Display*/ display, NativeWindow anid) nothrow {
   return gdk_window_lookup_for_display(UpCast!(Display*)(display), anid);
}


// Calls gdk_window_process_updates() for all windows (see #GdkWindow)
// in the application.
static void window_process_all_updates()() nothrow {
   gdk_window_process_all_updates();
}


// With update debugging enabled, calls to
// gdk_window_invalidate_region() clear the invalidated region of the
// screen to a noticeable color, and GDK pauses for a short time
// before sending exposes to windows during
// gdk_window_process_updates().  The net effect is that you can see
// the invalid region for each window and watch redraws as they
// occur. This allows you to diagnose inefficiencies in your application.
// 
// In essence, because the GDK rendering model prevents all flicker,
// if you are redrawing the same region 400 times you may never
// notice, aside from noticing a speed problem. Enabling update
// debugging causes GTK to flicker slowly and noticeably, so you can
// see exactly what's being redrawn when, in what order.
// 
// The --gtk-debug=updates command line option passed to GTK+ programs
// enables this debug option at application startup time. That's
// usually more useful than calling gdk_window_set_debug_updates()
// yourself, though you might want to use this function to enable
// updates sometime after application startup time.
// <setting>: %TRUE to turn on update debugging
static void window_set_debug_updates()(int setting) nothrow {
   gdk_window_set_debug_updates(setting);
}


// C prototypes:

extern (C) {
AppLaunchContext* /*new*/ gdk_app_launch_context_new() nothrow;
void gdk_app_launch_context_set_desktop(AppLaunchContext* this_, int desktop) nothrow;
void gdk_app_launch_context_set_display(AppLaunchContext* this_, Display* display) nothrow;
void gdk_app_launch_context_set_icon(AppLaunchContext* this_, Gio2.Icon* icon=null) nothrow;
void gdk_app_launch_context_set_icon_name(AppLaunchContext* this_, char* icon_name=null) nothrow;
void gdk_app_launch_context_set_screen(AppLaunchContext* this_, Screen* screen) nothrow;
void gdk_app_launch_context_set_timestamp(AppLaunchContext* this_, uint timestamp) nothrow;
char* /*new*/ gdk_atom_name(Atom* this_) nothrow;
Atom gdk_atom_intern(char* atom_name, int only_if_exists) nothrow;
Atom gdk_atom_intern_static_string(char* atom_name) nothrow;
Bitmap* gdk_bitmap_create_from_data(Drawable* drawable, char* data, int width, int height) nothrow;
Color* /*new*/ gdk_color_copy(Color* this_) nothrow;
int gdk_color_equal(Color* this_, Color* colorb) nothrow;
void gdk_color_free(Color* this_) nothrow;
uint gdk_color_hash(Color* this_) nothrow;
char* /*new*/ gdk_color_to_string(Color* this_) nothrow;
int gdk_color_alloc(Colormap* colormap, Color* color) nothrow;
int gdk_color_black(Colormap* colormap, Color* color) nothrow;
int gdk_color_change(Colormap* colormap, Color* color) nothrow;
int gdk_color_parse(char* spec, /*out*/ Color* color) nothrow;
int gdk_color_white(Colormap* colormap, Color* color) nothrow;
Colormap* /*new*/ gdk_colormap_new(Visual* visual, int allocate) nothrow;
Colormap* gdk_colormap_get_system() nothrow;
int gdk_colormap_get_system_size() nothrow;
int gdk_colormap_alloc_color(Colormap* this_, Color* color, int writeable, int best_match) nothrow;
int gdk_colormap_alloc_colors(Colormap* this_, Color* colors, int n_colors, int writeable, int best_match, int* success) nothrow;
void gdk_colormap_change(Colormap* this_, int ncolors) nothrow;
void gdk_colormap_free_colors(Colormap* this_, Color* colors, int n_colors) nothrow;
Screen* gdk_colormap_get_screen(Colormap* this_) nothrow;
Visual* gdk_colormap_get_visual(Colormap* this_) nothrow;
void gdk_colormap_query_color(Colormap* this_, c_ulong pixel, Color* result) nothrow;
Colormap* gdk_colormap_ref(Colormap* this_) nothrow;
void gdk_colormap_unref(Colormap* this_) nothrow;
Cursor* /*new*/ gdk_cursor_new(CursorType cursor_type) nothrow;
Cursor* /*new*/ gdk_cursor_new_for_display(Display* display, CursorType cursor_type) nothrow;
Cursor* /*new*/ gdk_cursor_new_from_name(Display* display, char* name) nothrow;
Cursor* /*new*/ gdk_cursor_new_from_pixbuf(Display* display, GdkPixbuf2.Pixbuf* pixbuf, int x, int y) nothrow;
Cursor* /*new*/ gdk_cursor_new_from_pixmap(Pixmap* source, Pixmap* mask, Color* fg, Color* bg, int x, int y) nothrow;
CursorType gdk_cursor_get_cursor_type(Cursor* this_) nothrow;
Display* gdk_cursor_get_display(Cursor* this_) nothrow;
GdkPixbuf2.Pixbuf* gdk_cursor_get_image(Cursor* this_) nothrow;
Cursor* /*new*/ gdk_cursor_ref(Cursor* this_) nothrow;
void gdk_cursor_unref(Cursor* this_) nothrow;
void gdk_device_free_history(/*inout*/ TimeCoord** events, int n_events) nothrow;
Device* gdk_device_get_core_pointer() nothrow;
int gdk_device_get_axis(Device* this_, double* axes, AxisUse use, double* value) nothrow;
AxisUse gdk_device_get_axis_use(Device* this_, uint index) nothrow;
int gdk_device_get_has_cursor(Device* this_) nothrow;
int gdk_device_get_history(Device* this_, Window* window, uint start, uint stop, /*out*/ TimeCoord*** events, /*out*/ int* n_events) nothrow;
void gdk_device_get_key(Device* this_, uint index, uint* keyval, ModifierType* modifiers) nothrow;
InputMode gdk_device_get_mode(Device* this_) nothrow;
int gdk_device_get_n_axes(Device* this_) nothrow;
int gdk_device_get_n_keys(Device* this_) nothrow;
char* gdk_device_get_name(Device* this_) nothrow;
InputSource gdk_device_get_source(Device* this_) nothrow;
void gdk_device_get_state(Device* this_, Window* window, double* axes, ModifierType* mask) nothrow;
void gdk_device_set_axis_use(Device* this_, uint index_, AxisUse use) nothrow;
void gdk_device_set_key(Device* this_, uint index_, uint keyval, ModifierType modifiers) nothrow;
int gdk_device_set_mode(Device* this_, InputMode mode) nothrow;
void gdk_device_set_source(Device* this_, InputSource source) nothrow;
Display* gdk_display_get_default() nothrow;
Display* gdk_display_open(char* display_name) nothrow;
Display* gdk_display_open_default_libgtk_only() nothrow;
void gdk_display_add_client_message_filter(Display* this_, Atom message_type, FilterFunc func, void* data) nothrow;
void gdk_display_beep(Display* this_) nothrow;
void gdk_display_close(Display* this_) nothrow;
void gdk_display_flush(Display* this_) nothrow;
Device* gdk_display_get_core_pointer(Display* this_) nothrow;
uint gdk_display_get_default_cursor_size(Display* this_) nothrow;
Window* gdk_display_get_default_group(Display* this_) nothrow;
Screen* gdk_display_get_default_screen(Display* this_) nothrow;
Event* /*new*/ gdk_display_get_event(Display* this_) nothrow;
void gdk_display_get_maximal_cursor_size(Display* this_, /*out*/ uint* width, /*out*/ uint* height) nothrow;
int gdk_display_get_n_screens(Display* this_) nothrow;
char* gdk_display_get_name(Display* this_) nothrow;
void gdk_display_get_pointer(Display* this_, /*out*/ Screen** screen=null, /*out*/ int* x=null, /*out*/ int* y=null, /*out*/ ModifierType* mask=null) nothrow;
Screen* gdk_display_get_screen(Display* this_, int screen_num) nothrow;
Window* gdk_display_get_window_at_pointer(Display* this_, /*out*/ int* win_x=null, /*out*/ int* win_y=null) nothrow;
int gdk_display_is_closed(Display* this_) nothrow;
void gdk_display_keyboard_ungrab(Display* this_, uint time_) nothrow;
GLib2.List* gdk_display_list_devices(Display* this_) nothrow;
Event* /*new*/ gdk_display_peek_event(Display* this_) nothrow;
int gdk_display_pointer_is_grabbed(Display* this_) nothrow;
void gdk_display_pointer_ungrab(Display* this_, uint time_) nothrow;
void gdk_display_put_event(Display* this_, Event* event) nothrow;
int gdk_display_request_selection_notification(Display* this_, Atom selection) nothrow;
void gdk_display_set_double_click_distance(Display* this_, uint distance) nothrow;
void gdk_display_set_double_click_time(Display* this_, uint msec) nothrow;
DisplayPointerHooks* gdk_display_set_pointer_hooks(Display* this_, DisplayPointerHooks* new_hooks) nothrow;
void gdk_display_store_clipboard(Display* this_, Window* clipboard_window, uint time_, Atom* targets, int n_targets) nothrow;
int gdk_display_supports_clipboard_persistence(Display* this_) nothrow;
int gdk_display_supports_composite(Display* this_) nothrow;
int gdk_display_supports_cursor_alpha(Display* this_) nothrow;
int gdk_display_supports_cursor_color(Display* this_) nothrow;
int gdk_display_supports_input_shapes(Display* this_) nothrow;
int gdk_display_supports_selection_notification(Display* this_) nothrow;
int gdk_display_supports_shapes(Display* this_) nothrow;
void gdk_display_sync(Display* this_) nothrow;
void gdk_display_warp_pointer(Display* this_, Screen* screen, int x, int y) nothrow;
DisplayManager* gdk_display_manager_get() nothrow;
Display* gdk_display_manager_get_default_display(DisplayManager* this_) nothrow;
GLib2.SList* /*new container*/ gdk_display_manager_list_displays(DisplayManager* this_) nothrow;
void gdk_display_manager_set_default_display(DisplayManager* this_, Display* display) nothrow;
DragContext* /*new*/ gdk_drag_context_new() nothrow;
DragAction gdk_drag_context_get_actions(DragContext* this_) nothrow;
Window* gdk_drag_context_get_dest_window(DragContext* this_) nothrow;
DragProtocol gdk_drag_context_get_protocol(DragContext* this_) nothrow;
DragAction gdk_drag_context_get_selected_action(DragContext* this_) nothrow;
Window* gdk_drag_context_get_source_window(DragContext* this_) nothrow;
DragAction gdk_drag_context_get_suggested_action(DragContext* this_) nothrow;
GLib2.List* gdk_drag_context_list_targets(DragContext* this_) nothrow;
void gdk_drag_context_ref(DragContext* this_) nothrow;
void gdk_drag_context_unref(DragContext* this_) nothrow;
Image* gdk_drawable_copy_to_image(Drawable* this_, Image* image, int src_x, int src_y, int dest_x, int dest_y, int width, int height) nothrow;
Region* gdk_drawable_get_clip_region(Drawable* this_) nothrow;
Colormap* gdk_drawable_get_colormap(Drawable* this_) nothrow;
void* gdk_drawable_get_data(Drawable* this_, char* key) nothrow;
int gdk_drawable_get_depth(Drawable* this_) nothrow;
Display* gdk_drawable_get_display(Drawable* this_) nothrow;
Image* gdk_drawable_get_image(Drawable* this_, int x, int y, int width, int height) nothrow;
Screen* gdk_drawable_get_screen(Drawable* this_) nothrow;
void gdk_drawable_get_size(Drawable* this_, /*out*/ int* width=null, /*out*/ int* height=null) nothrow;
Region* gdk_drawable_get_visible_region(Drawable* this_) nothrow;
Visual* gdk_drawable_get_visual(Drawable* this_) nothrow;
Drawable* gdk_drawable_ref(Drawable* this_) nothrow;
void gdk_drawable_set_colormap(Drawable* this_, Colormap* colormap) nothrow;
void gdk_drawable_set_data(Drawable* this_, char* key, void* data, GLib2.DestroyNotify destroy_func=null) nothrow;
void gdk_drawable_unref(Drawable* this_) nothrow;
Event* /*new*/ gdk_event_new(EventType type) nothrow;
Event* /*new*/ gdk_event_copy(Event* this_) nothrow;
void gdk_event_free(Event* this_) nothrow;
int gdk_event_get_axis(Event* this_, AxisUse axis_use, /*out*/ double* value) nothrow;
int gdk_event_get_coords(Event* this_, /*out*/ double* x_win, /*out*/ double* y_win) nothrow;
int gdk_event_get_root_coords(Event* this_, /*out*/ double* x_root, /*out*/ double* y_root) nothrow;
Screen* gdk_event_get_screen(Event* this_) nothrow;
int gdk_event_get_state(Event* this_, /*out*/ ModifierType* state) nothrow;
uint gdk_event_get_time(Event* this_) nothrow;
void gdk_event_put(Event* this_) nothrow;
int gdk_event_send_client_message(Event* this_, NativeWindow winid) nothrow;
void gdk_event_send_clientmessage_toall(Event* this_) nothrow;
void gdk_event_set_screen(Event* this_, Screen* screen) nothrow;
Event* /*new*/ gdk_event_get() nothrow;
Event* /*new*/ gdk_event_get_graphics_expose(Window* window) nothrow;
void gdk_event_handler_set(EventFunc func, void* data, GLib2.DestroyNotify notify) nothrow;
Event* /*new*/ gdk_event_peek() nothrow;
void gdk_event_request_motions(EventMotion* event) nothrow;
int gdk_event_send_client_message_for_display(Display* display, Event* event, NativeWindow winid) nothrow;
int gdk_font_equal(Font* this_, Font* fontb) nothrow;
Display* gdk_font_get_display(Font* this_) nothrow;
int gdk_font_id(Font* this_) nothrow;
Font* /*new*/ gdk_font_ref(Font* this_) nothrow;
void gdk_font_unref(Font* this_) nothrow;
Font* /*new*/ gdk_font_from_description(Pango.FontDescription* font_desc) nothrow;
Font* /*new*/ gdk_font_from_description_for_display(Display* display, Pango.FontDescription* font_desc) nothrow;
Font* /*new*/ gdk_font_load(char* font_name) nothrow;
Font* /*new*/ gdk_font_load_for_display(Display* display, char* font_name) nothrow;
GC* /*new*/ gdk_gc_new(Drawable* drawable) nothrow;
GC* /*new*/ gdk_gc_new_with_values(Drawable* drawable, GCValues* values, GCValuesMask values_mask) nothrow;
void gdk_gc_copy(GC* this_, GC* src_gc) nothrow;
Colormap* gdk_gc_get_colormap(GC* this_) nothrow;
Screen* gdk_gc_get_screen(GC* this_) nothrow;
void gdk_gc_get_values(GC* this_, GCValues* values) nothrow;
void gdk_gc_offset(GC* this_, int x_offset, int y_offset) nothrow;
GC* gdk_gc_ref(GC* this_) nothrow;
void gdk_gc_set_background(GC* this_, Color* color) nothrow;
void gdk_gc_set_clip_mask(GC* this_, Bitmap* mask) nothrow;
void gdk_gc_set_clip_origin(GC* this_, int x, int y) nothrow;
void gdk_gc_set_clip_rectangle(GC* this_, Rectangle* rectangle) nothrow;
void gdk_gc_set_clip_region(GC* this_, Region* region) nothrow;
void gdk_gc_set_colormap(GC* this_, Colormap* colormap) nothrow;
void gdk_gc_set_dashes(GC* this_, int dash_offset, byte dash_list, int n) nothrow;
void gdk_gc_set_exposures(GC* this_, int exposures) nothrow;
void gdk_gc_set_fill(GC* this_, Fill fill) nothrow;
void gdk_gc_set_font(GC* this_, Font* font) nothrow;
void gdk_gc_set_foreground(GC* this_, Color* color) nothrow;
void gdk_gc_set_function(GC* this_, Function function_) nothrow;
void gdk_gc_set_line_attributes(GC* this_, int line_width, LineStyle line_style, CapStyle cap_style, JoinStyle join_style) nothrow;
void gdk_gc_set_rgb_bg_color(GC* this_, Color* color) nothrow;
void gdk_gc_set_rgb_fg_color(GC* this_, Color* color) nothrow;
void gdk_gc_set_stipple(GC* this_, Pixmap* stipple) nothrow;
void gdk_gc_set_subwindow(GC* this_, SubwindowMode mode) nothrow;
void gdk_gc_set_tile(GC* this_, Pixmap* tile) nothrow;
void gdk_gc_set_ts_origin(GC* this_, int x, int y) nothrow;
void gdk_gc_set_values(GC* this_, GCValues* values, GCValuesMask values_mask) nothrow;
void gdk_gc_unref(GC* this_) nothrow;
Image* /*new*/ gdk_image_new(ImageType type, Visual* visual, int width, int height) nothrow;
Image* gdk_image_get(Drawable* drawable, int x, int y, int width, int height) nothrow;
ushort gdk_image_get_bits_per_pixel(Image* this_) nothrow;
ByteOrder gdk_image_get_byte_order(Image* this_) nothrow;
ushort gdk_image_get_bytes_per_line(Image* this_) nothrow;
ushort gdk_image_get_bytes_per_pixel(Image* this_) nothrow;
Colormap* gdk_image_get_colormap(Image* this_) nothrow;
ushort gdk_image_get_depth(Image* this_) nothrow;
int gdk_image_get_height(Image* this_) nothrow;
ImageType gdk_image_get_image_type(Image* this_) nothrow;
uint gdk_image_get_pixel(Image* this_, int x, int y) nothrow;
void* gdk_image_get_pixels(Image* this_) nothrow;
Visual* gdk_image_get_visual(Image* this_) nothrow;
int gdk_image_get_width(Image* this_) nothrow;
void gdk_image_put_pixel(Image* this_, int x, int y, uint pixel) nothrow;
Image* gdk_image_ref(Image* this_) nothrow;
void gdk_image_set_colormap(Image* this_, Colormap* colormap) nothrow;
void gdk_image_unref(Image* this_) nothrow;
int gdk_keyboard_grab_info_libgtk_only(Display* display, Window** grab_window, int* owner_events) nothrow;
Keymap* gdk_keymap_get_default() nothrow;
Keymap* gdk_keymap_get_for_display(Display* display) nothrow;
void gdk_keymap_add_virtual_modifiers(Keymap* this_, ModifierType* state) nothrow;
int gdk_keymap_get_caps_lock_state(Keymap* this_) nothrow;
Pango.Direction gdk_keymap_get_direction(Keymap* this_) nothrow;
int gdk_keymap_get_entries_for_keycode(Keymap* this_, uint hardware_keycode, /*out*/ KeymapKey** keys, /*out*/ uint** keyvals, int* n_entries) nothrow;
int gdk_keymap_get_entries_for_keyval(Keymap* this_, uint keyval, /*out*/ KeymapKey** keys, /*out*/ int* n_keys) nothrow;
int gdk_keymap_have_bidi_layouts(Keymap* this_) nothrow;
uint gdk_keymap_lookup_key(Keymap* this_, KeymapKey* key) nothrow;
int gdk_keymap_map_virtual_modifiers(Keymap* this_, ModifierType* state) nothrow;
int gdk_keymap_translate_keyboard_state(Keymap* this_, uint hardware_keycode, ModifierType state, int group, /*out*/ uint* keyval=null, /*out*/ int* effective_group=null, /*out*/ int* level=null, /*out*/ ModifierType* consumed_modifiers=null) nothrow;
Pango.Attribute* gdk_pango_attr_emboss_color_new(Color* color) nothrow;
Pango.Attribute* gdk_pango_attr_embossed_new(int embossed) nothrow;
Pango.Attribute* gdk_pango_attr_stipple_new(Bitmap* stipple) nothrow;
PangoRenderer* /*new*/ gdk_pango_renderer_new(Screen* screen) nothrow;
Pango.Renderer* gdk_pango_renderer_get_default(Screen* screen) nothrow;
void gdk_pango_renderer_set_drawable(PangoRenderer* this_, Drawable* drawable=null) nothrow;
void gdk_pango_renderer_set_gc(PangoRenderer* this_, GC* gc=null) nothrow;
void gdk_pango_renderer_set_override_color(PangoRenderer* this_, Pango.RenderPart part, Color* color=null) nothrow;
void gdk_pango_renderer_set_stipple(PangoRenderer* this_, Pango.RenderPart part, Bitmap* stipple) nothrow;
Pixmap* /*new*/ gdk_pixmap_foreign_new(NativeWindow anid) nothrow;
Pixmap* /*new*/ gdk_pixmap_foreign_new_for_display(Display* display, NativeWindow anid) nothrow;
Pixmap* /*new*/ gdk_pixmap_foreign_new_for_screen(Screen* screen, NativeWindow anid, int width, int height, int depth) nothrow;
Pixmap* /*new*/ gdk_pixmap_new(Drawable* drawable, int width, int height, int depth) nothrow;
Pixmap* gdk_pixmap_colormap_create_from_xpm(Drawable* drawable, Colormap* colormap, Bitmap** mask, Color* transparent_color, char* filename) nothrow;
Pixmap* gdk_pixmap_colormap_create_from_xpm_d(Drawable* drawable, Colormap* colormap, Bitmap** mask, Color* transparent_color, char** data) nothrow;
Pixmap* gdk_pixmap_create_from_data(Drawable* drawable, char* data, int width, int height, int depth, Color* fg, Color* bg) nothrow;
Pixmap* gdk_pixmap_create_from_xpm(Drawable* drawable, Bitmap** mask, Color* transparent_color, char* filename) nothrow;
Pixmap* gdk_pixmap_create_from_xpm_d(Drawable* drawable, /*out*/ Bitmap** mask, Color* transparent_color, char** data) nothrow;
Pixmap* gdk_pixmap_lookup(NativeWindow anid) nothrow;
Pixmap* gdk_pixmap_lookup_for_display(Display* display, NativeWindow anid) nothrow;
void gdk_pixmap_get_size(Pixmap* this_, /*out*/ int* width=null, /*out*/ int* height=null) nothrow;
int gdk_rectangle_intersect(Rectangle* this_, Rectangle* src2, /*out*/ Rectangle* dest=null) nothrow;
void gdk_rectangle_union(Rectangle* this_, Rectangle* src2, /*out*/ Rectangle* dest) nothrow;
Region* gdk_region_copy(Region* this_) nothrow;
void gdk_region_destroy(Region* this_) nothrow;
int gdk_region_empty(Region* this_) nothrow;
int gdk_region_equal(Region* this_, Region* region2) nothrow;
void gdk_region_get_clipbox(Region* this_, Rectangle* rectangle) nothrow;
void gdk_region_get_rectangles(Region* this_, Rectangle** rectangles, int* n_rectangles) nothrow;
void gdk_region_intersect(Region* this_, Region* source2) nothrow;
void gdk_region_offset(Region* this_, int dx, int dy) nothrow;
int gdk_region_point_in(Region* this_, int x, int y) nothrow;
int gdk_region_rect_equal(Region* this_, Rectangle* rectangle) nothrow;
OverlapType gdk_region_rect_in(Region* this_, Rectangle* rectangle) nothrow;
void gdk_region_shrink(Region* this_, int dx, int dy) nothrow;
void gdk_region_spans_intersect_foreach(Region* this_, Span* spans, int n_spans, int sorted, SpanFunc function_, void* data) nothrow;
void gdk_region_subtract(Region* this_, Region* source2) nothrow;
void gdk_region_union(Region* this_, Region* source2) nothrow;
void gdk_region_union_with_rect(Region* this_, Rectangle* rect) nothrow;
void gdk_region_xor(Region* this_, Region* source2) nothrow;
Region* gdk_region_new() nothrow;
Region* gdk_region_polygon(Point* points, int n_points, FillRule fill_rule) nothrow;
Region* gdk_region_rectangle(Rectangle* rectangle) nothrow;
void gdk_rgb_cmap_free(RgbCmap* this_) nothrow;
RgbCmap* gdk_rgb_cmap_new(uint* colors, int n_colors) nothrow;
Screen* gdk_screen_get_default() nothrow;
int gdk_screen_height() nothrow;
int gdk_screen_height_mm() nothrow;
int gdk_screen_width() nothrow;
int gdk_screen_width_mm() nothrow;
void gdk_screen_broadcast_client_message(Screen* this_, Event* event) nothrow;
Window* gdk_screen_get_active_window(Screen* this_) nothrow;
Colormap* gdk_screen_get_default_colormap(Screen* this_) nothrow;
Display* gdk_screen_get_display(Screen* this_) nothrow;
cairo.FontOptions* gdk_screen_get_font_options(Screen* this_) nothrow;
int gdk_screen_get_height(Screen* this_) nothrow;
int gdk_screen_get_height_mm(Screen* this_) nothrow;
int gdk_screen_get_monitor_at_point(Screen* this_, int x, int y) nothrow;
int gdk_screen_get_monitor_at_window(Screen* this_, Window* window) nothrow;
void gdk_screen_get_monitor_geometry(Screen* this_, int monitor_num, Rectangle* dest) nothrow;
int gdk_screen_get_monitor_height_mm(Screen* this_, int monitor_num) nothrow;
char* /*new*/ gdk_screen_get_monitor_plug_name(Screen* this_, int monitor_num) nothrow;
int gdk_screen_get_monitor_width_mm(Screen* this_, int monitor_num) nothrow;
int gdk_screen_get_n_monitors(Screen* this_) nothrow;
int gdk_screen_get_number(Screen* this_) nothrow;
int gdk_screen_get_primary_monitor(Screen* this_) nothrow;
double gdk_screen_get_resolution(Screen* this_) nothrow;
Colormap* gdk_screen_get_rgb_colormap(Screen* this_) nothrow;
Visual* gdk_screen_get_rgb_visual(Screen* this_) nothrow;
Colormap* gdk_screen_get_rgba_colormap(Screen* this_) nothrow;
Visual* gdk_screen_get_rgba_visual(Screen* this_) nothrow;
Window* gdk_screen_get_root_window(Screen* this_) nothrow;
int gdk_screen_get_setting(Screen* this_, char* name, GObject2.Value* value) nothrow;
Colormap* gdk_screen_get_system_colormap(Screen* this_) nothrow;
Visual* gdk_screen_get_system_visual(Screen* this_) nothrow;
GLib2.List* /*new container*/ gdk_screen_get_toplevel_windows(Screen* this_) nothrow;
int gdk_screen_get_width(Screen* this_) nothrow;
int gdk_screen_get_width_mm(Screen* this_) nothrow;
GLib2.List* /*new*/ gdk_screen_get_window_stack(Screen* this_) nothrow;
int gdk_screen_is_composited(Screen* this_) nothrow;
GLib2.List* /*new container*/ gdk_screen_list_visuals(Screen* this_) nothrow;
char* /*new*/ gdk_screen_make_display_name(Screen* this_) nothrow;
void gdk_screen_set_default_colormap(Screen* this_, Colormap* colormap) nothrow;
void gdk_screen_set_font_options(Screen* this_, cairo.FontOptions* options=null) nothrow;
void gdk_screen_set_resolution(Screen* this_, double dpi) nothrow;
Visual* gdk_visual_get_best() nothrow;
int gdk_visual_get_best_depth() nothrow;
VisualType gdk_visual_get_best_type() nothrow;
Visual* gdk_visual_get_best_with_both(int depth, VisualType visual_type) nothrow;
Visual* gdk_visual_get_best_with_depth(int depth) nothrow;
Visual* gdk_visual_get_best_with_type(VisualType visual_type) nothrow;
Visual* gdk_visual_get_system() nothrow;
int gdk_visual_get_bits_per_rgb(Visual* this_) nothrow;
void gdk_visual_get_blue_pixel_details(Visual* this_, /*out*/ uint* mask=null, /*out*/ int* shift=null, /*out*/ int* precision=null) nothrow;
ByteOrder gdk_visual_get_byte_order(Visual* this_) nothrow;
int gdk_visual_get_colormap_size(Visual* this_) nothrow;
int gdk_visual_get_depth(Visual* this_) nothrow;
void gdk_visual_get_green_pixel_details(Visual* this_, /*out*/ uint* mask=null, /*out*/ int* shift=null, /*out*/ int* precision=null) nothrow;
void gdk_visual_get_red_pixel_details(Visual* this_, /*out*/ uint* mask=null, /*out*/ int* shift=null, /*out*/ int* precision=null) nothrow;
Screen* gdk_visual_get_screen(Visual* this_) nothrow;
VisualType gdk_visual_get_visual_type(Visual* this_) nothrow;
void gdk_window_add_filter(Window* this_, FilterFunc function_, void* data) nothrow;
void gdk_window_beep(Window* this_) nothrow;
void gdk_window_begin_move_drag(Window* this_, int button, int root_x, int root_y, uint timestamp) nothrow;
void gdk_window_begin_paint_rect(Window* this_, Rectangle* rectangle) nothrow;
void gdk_window_begin_paint_region(Window* this_, Region* region) nothrow;
void gdk_window_begin_resize_drag(Window* this_, WindowEdge edge, int button, int root_x, int root_y, uint timestamp) nothrow;
void gdk_window_clear(Window* this_) nothrow;
void gdk_window_clear_area(Window* this_, int x, int y, int width, int height) nothrow;
void gdk_window_clear_area_e(Window* this_, int x, int y, int width, int height) nothrow;
void gdk_window_configure_finished(Window* this_) nothrow;
void gdk_window_coords_from_parent(Window* this_, double parent_x, double parent_y, /*out*/ double* x=null, /*out*/ double* y=null) nothrow;
void gdk_window_coords_to_parent(Window* this_, double x, double y, /*out*/ double* parent_x=null, /*out*/ double* parent_y=null) nothrow;
cairo.Surface* /*new*/ gdk_window_create_similar_surface(Window* this_, cairo.Content content, int width, int height) nothrow;
void gdk_window_deiconify(Window* this_) nothrow;
void gdk_window_destroy(Window* this_) nothrow;
void gdk_window_destroy_notify(Window* this_) nothrow;
void gdk_window_enable_synchronized_configure(Window* this_) nothrow;
void gdk_window_end_paint(Window* this_) nothrow;
int gdk_window_ensure_native(Window* this_) nothrow;
void gdk_window_flush(Window* this_) nothrow;
void gdk_window_focus(Window* this_, uint timestamp) nothrow;
void gdk_window_freeze_toplevel_updates_libgtk_only(Window* this_) nothrow;
void gdk_window_freeze_updates(Window* this_) nothrow;
void gdk_window_fullscreen(Window* this_) nothrow;
void gdk_window_geometry_changed(Window* this_) nothrow;
int gdk_window_get_accept_focus(Window* this_) nothrow;
cairo.Pattern* gdk_window_get_background_pattern(Window* this_) nothrow;
GLib2.List* /*new container*/ gdk_window_get_children(Window* this_) nothrow;
int gdk_window_get_composited(Window* this_) nothrow;
Cursor* gdk_window_get_cursor(Window* this_) nothrow;
int gdk_window_get_decorations(Window* this_, WMDecoration* decorations) nothrow;
int gdk_window_get_deskrelative_origin(Window* this_, int* x, int* y) nothrow;
Display* gdk_window_get_display(Window* this_) nothrow;
Window* gdk_window_get_effective_parent(Window* this_) nothrow;
Window* gdk_window_get_effective_toplevel(Window* this_) nothrow;
EventMask gdk_window_get_events(Window* this_) nothrow;
int gdk_window_get_focus_on_map(Window* this_) nothrow;
void gdk_window_get_frame_extents(Window* this_, Rectangle* rect) nothrow;
void gdk_window_get_geometry(Window* this_, int* x, int* y, int* width, int* height, int* depth) nothrow;
Window* gdk_window_get_group(Window* this_) nothrow;
int gdk_window_get_height(Window* this_) nothrow;
void gdk_window_get_internal_paint_info(Window* this_, /*out*/ Drawable** real_drawable, /*out*/ int* x_offset, /*out*/ int* y_offset) nothrow;
int gdk_window_get_modal_hint(Window* this_) nothrow;
int gdk_window_get_origin(Window* this_, int* x, int* y) nothrow;
Window* gdk_window_get_parent(Window* this_) nothrow;
Window* gdk_window_get_pointer(Window* this_, /*out*/ int* x=null, /*out*/ int* y=null, /*out*/ ModifierType* mask=null) nothrow;
void gdk_window_get_position(Window* this_, /*out*/ int* x=null, /*out*/ int* y=null) nothrow;
void gdk_window_get_root_coords(Window* this_, int x, int y, /*out*/ int* root_x, /*out*/ int* root_y) nothrow;
void gdk_window_get_root_origin(Window* this_, int* x, int* y) nothrow;
Screen* gdk_window_get_screen(Window* this_) nothrow;
WindowState gdk_window_get_state(Window* this_) nothrow;
Window* gdk_window_get_toplevel(Window* this_) nothrow;
WindowTypeHint gdk_window_get_type_hint(Window* this_) nothrow;
Region* gdk_window_get_update_area(Window* this_) nothrow;
void gdk_window_get_user_data(Window* this_, /*out*/ void** data) nothrow;
Visual* gdk_window_get_visual(Window* this_) nothrow;
int gdk_window_get_width(Window* this_) nothrow;
WindowType gdk_window_get_window_type(Window* this_) nothrow;
int gdk_window_has_native(Window* this_) nothrow;
void gdk_window_hide(Window* this_) nothrow;
void gdk_window_iconify(Window* this_) nothrow;
void gdk_window_input_shape_combine_mask(Window* this_, Bitmap* mask, int x, int y) nothrow;
void gdk_window_input_shape_combine_region(Window* this_, Region* shape_region, int offset_x, int offset_y) nothrow;
void gdk_window_invalidate_maybe_recurse(Window* this_, Region* region, void** child_func, void* user_data) nothrow;
void gdk_window_invalidate_rect(Window* this_, Rectangle* rect, int invalidate_children) nothrow;
void gdk_window_invalidate_region(Window* this_, Region* region, int invalidate_children) nothrow;
int gdk_window_is_destroyed(Window* this_) nothrow;
int gdk_window_is_input_only(Window* this_) nothrow;
int gdk_window_is_shaped(Window* this_) nothrow;
int gdk_window_is_viewable(Window* this_) nothrow;
int gdk_window_is_visible(Window* this_) nothrow;
void gdk_window_lower(Window* this_) nothrow;
void gdk_window_maximize(Window* this_) nothrow;
void gdk_window_merge_child_input_shapes(Window* this_) nothrow;
void gdk_window_merge_child_shapes(Window* this_) nothrow;
void gdk_window_move(Window* this_, int x, int y) nothrow;
void gdk_window_move_region(Window* this_, Region* region, int dx, int dy) nothrow;
void gdk_window_move_resize(Window* this_, int x, int y, int width, int height) nothrow;
Window* gdk_window_new(Window* this_, WindowAttr* attributes, int attributes_mask) nothrow;
GLib2.List* gdk_window_peek_children(Window* this_) nothrow;
void gdk_window_process_updates(Window* this_, int update_children) nothrow;
void gdk_window_raise(Window* this_) nothrow;
void gdk_window_redirect_to_drawable(Window* this_, Drawable* drawable, int src_x, int src_y, int dest_x, int dest_y, int width, int height) nothrow;
void gdk_window_register_dnd(Window* this_) nothrow;
void gdk_window_remove_filter(Window* this_, FilterFunc function_, void* data) nothrow;
void gdk_window_remove_redirection(Window* this_) nothrow;
void gdk_window_reparent(Window* this_, Window* new_parent, int x, int y) nothrow;
void gdk_window_resize(Window* this_, int width, int height) nothrow;
void gdk_window_restack(Window* this_, Window* sibling, int above) nothrow;
void gdk_window_scroll(Window* this_, int dx, int dy) nothrow;
void gdk_window_set_accept_focus(Window* this_, int accept_focus) nothrow;
void gdk_window_set_back_pixmap(Window* this_, Pixmap* pixmap, int parent_relative) nothrow;
void gdk_window_set_background(Window* this_, Color* color) nothrow;
void gdk_window_set_child_input_shapes(Window* this_) nothrow;
void gdk_window_set_child_shapes(Window* this_) nothrow;
void gdk_window_set_composited(Window* this_, int composited) nothrow;
void gdk_window_set_cursor(Window* this_, Cursor* cursor=null) nothrow;
void gdk_window_set_decorations(Window* this_, WMDecoration decorations) nothrow;
void gdk_window_set_events(Window* this_, EventMask event_mask) nothrow;
void gdk_window_set_focus_on_map(Window* this_, int focus_on_map) nothrow;
void gdk_window_set_functions(Window* this_, WMFunction functions) nothrow;
void gdk_window_set_geometry_hints(Window* this_, Geometry* geometry, WindowHints geom_mask) nothrow;
void gdk_window_set_group(Window* this_, Window* leader) nothrow;
void gdk_window_set_hints(Window* this_, int x, int y, int min_width, int min_height, int max_width, int max_height, int flags) nothrow;
void gdk_window_set_icon(Window* this_, Window* icon_window, Pixmap* pixmap, Bitmap* mask) nothrow;
void gdk_window_set_icon_list(Window* this_, GLib2.List* pixbufs) nothrow;
void gdk_window_set_icon_name(Window* this_, char* name) nothrow;
void gdk_window_set_keep_above(Window* this_, int setting) nothrow;
void gdk_window_set_keep_below(Window* this_, int setting) nothrow;
void gdk_window_set_modal_hint(Window* this_, int modal) nothrow;
void gdk_window_set_opacity(Window* this_, double opacity) nothrow;
void gdk_window_set_override_redirect(Window* this_, int override_redirect) nothrow;
void gdk_window_set_role(Window* this_, char* role) nothrow;
void gdk_window_set_skip_pager_hint(Window* this_, int skips_pager) nothrow;
void gdk_window_set_skip_taskbar_hint(Window* this_, int skips_taskbar) nothrow;
void gdk_window_set_startup_id(Window* this_, char* startup_id) nothrow;
int gdk_window_set_static_gravities(Window* this_, int use_static) nothrow;
void gdk_window_set_title(Window* this_, char* title) nothrow;
void gdk_window_set_transient_for(Window* this_, Window* parent) nothrow;
void gdk_window_set_type_hint(Window* this_, WindowTypeHint hint) nothrow;
void gdk_window_set_urgency_hint(Window* this_, int urgent) nothrow;
void gdk_window_set_user_data(Window* this_, void* user_data) nothrow;
void gdk_window_shape_combine_mask(Window* this_, Bitmap* mask, int x, int y) nothrow;
void gdk_window_shape_combine_region(Window* this_, Region* shape_region, int offset_x, int offset_y) nothrow;
void gdk_window_show(Window* this_) nothrow;
void gdk_window_show_unraised(Window* this_) nothrow;
void gdk_window_stick(Window* this_) nothrow;
void gdk_window_thaw_toplevel_updates_libgtk_only(Window* this_) nothrow;
void gdk_window_thaw_updates(Window* this_) nothrow;
void gdk_window_unfullscreen(Window* this_) nothrow;
void gdk_window_unmaximize(Window* this_) nothrow;
void gdk_window_unstick(Window* this_) nothrow;
void gdk_window_withdraw(Window* this_) nothrow;
void gdk_add_client_message_filter(Atom message_type, FilterFunc func, void* data) nothrow;
void gdk_add_option_entries_libgtk_only(GLib2.OptionGroup* group) nothrow;
void gdk_beep() nothrow;
cairo.Context* /*new*/ gdk_cairo_create(Drawable* drawable) nothrow;
void gdk_cairo_rectangle(cairo.Context* cr, Rectangle* rectangle) nothrow;
void gdk_cairo_region(cairo.Context* cr, Region* region) nothrow;
void gdk_cairo_reset_clip(cairo.Context* cr, Drawable* drawable) nothrow;
void gdk_cairo_set_source_color(cairo.Context* cr, Color* color) nothrow;
void gdk_cairo_set_source_pixbuf(cairo.Context* cr, GdkPixbuf2.Pixbuf* pixbuf, double pixbuf_x, double pixbuf_y) nothrow;
void gdk_cairo_set_source_pixmap(cairo.Context* cr, Pixmap* pixmap, double pixmap_x, double pixmap_y) nothrow;
void gdk_cairo_set_source_window(cairo.Context* cr, Window* window, double x, double y) nothrow;
int gdk_char_height(Font* font, char character) nothrow;
int gdk_char_measure(Font* font, char character) nothrow;
int gdk_char_width(Font* font, char character) nothrow;
int gdk_char_width_wc(Font* font, WChar character) nothrow;
int gdk_colors_alloc(Colormap* colormap, int contiguous, c_ulong* planes, int nplanes, c_ulong* pixels, int npixels) nothrow;
void gdk_colors_free(Colormap* colormap, c_ulong* pixels, int npixels, c_ulong planes) nothrow;
void gdk_colors_store(Colormap* colormap, Color* colors, int ncolors) nothrow;
GLib2.List* gdk_devices_list() nothrow;
void gdk_drag_abort(DragContext* context, uint time_) nothrow;
DragContext* gdk_drag_begin(Window* window, GLib2.List* targets) nothrow;
void gdk_drag_drop(DragContext* context, uint time_) nothrow;
int gdk_drag_drop_succeeded(DragContext* context) nothrow;
void gdk_drag_find_window(DragContext* context, Window* drag_window, int x_root, int y_root, /*out*/ Window** dest_window, /*out*/ DragProtocol* protocol) nothrow;
void gdk_drag_find_window_for_screen(DragContext* context, Window* drag_window, Screen* screen, int x_root, int y_root, /*out*/ Window** dest_window, /*out*/ DragProtocol* protocol) nothrow;
NativeWindow gdk_drag_get_protocol(NativeWindow xid, DragProtocol* protocol) nothrow;
NativeWindow gdk_drag_get_protocol_for_display(Display* display, NativeWindow xid, DragProtocol* protocol) nothrow;
Atom gdk_drag_get_selection(DragContext* context) nothrow;
int gdk_drag_motion(DragContext* context, Window* dest_window, DragProtocol protocol, int x_root, int y_root, DragAction suggested_action, DragAction possible_actions, uint time_) nothrow;
void gdk_drag_status(DragContext* context, DragAction action, uint time_) nothrow;
void gdk_draw_arc(Drawable* drawable, GC* gc, int filled, int x, int y, int width, int height, int angle1, int angle2) nothrow;
void gdk_draw_drawable(Drawable* drawable, GC* gc, Drawable* src, int xsrc, int ysrc, int xdest, int ydest, int width, int height) nothrow;
void gdk_draw_glyphs(Drawable* drawable, GC* gc, Pango.Font* font, int x, int y, Pango.GlyphString* glyphs) nothrow;
void gdk_draw_glyphs_transformed(Drawable* drawable, GC* gc, Pango.Matrix* matrix, Pango.Font* font, int x, int y, Pango.GlyphString* glyphs) nothrow;
void gdk_draw_gray_image(Drawable* drawable, GC* gc, int x, int y, int width, int height, RgbDither dith, ubyte* buf, int rowstride) nothrow;
void gdk_draw_image(Drawable* drawable, GC* gc, Image* image, int xsrc, int ysrc, int xdest, int ydest, int width, int height) nothrow;
void gdk_draw_indexed_image(Drawable* drawable, GC* gc, int x, int y, int width, int height, RgbDither dith, ubyte* buf, int rowstride, RgbCmap* cmap) nothrow;
void gdk_draw_layout(Drawable* drawable, GC* gc, int x, int y, Pango.Layout* layout) nothrow;
void gdk_draw_layout_line(Drawable* drawable, GC* gc, int x, int y, Pango.LayoutLine* line) nothrow;
void gdk_draw_layout_line_with_colors(Drawable* drawable, GC* gc, int x, int y, Pango.LayoutLine* line, Color* foreground=null, Color* background=null) nothrow;
void gdk_draw_layout_with_colors(Drawable* drawable, GC* gc, int x, int y, Pango.Layout* layout, Color* foreground=null, Color* background=null) nothrow;
void gdk_draw_line(Drawable* drawable, GC* gc, int x1_, int y1_, int x2_, int y2_) nothrow;
void gdk_draw_lines(Drawable* drawable, GC* gc, Point* points, int n_points) nothrow;
void gdk_draw_pixbuf(Drawable* drawable, GC* gc, GdkPixbuf2.Pixbuf* pixbuf, int src_x, int src_y, int dest_x, int dest_y, int width, int height, RgbDither dither, int x_dither, int y_dither) nothrow;
void gdk_draw_point(Drawable* drawable, GC* gc, int x, int y) nothrow;
void gdk_draw_points(Drawable* drawable, GC* gc, Point* points, int n_points) nothrow;
void gdk_draw_polygon(Drawable* drawable, GC* gc, int filled, Point* points, int n_points) nothrow;
void gdk_draw_rectangle(Drawable* drawable, GC* gc, int filled, int x, int y, int width, int height) nothrow;
void gdk_draw_rgb_32_image(Drawable* drawable, GC* gc, int x, int y, int width, int height, RgbDither dith, ubyte* buf, int rowstride) nothrow;
void gdk_draw_rgb_32_image_dithalign(Drawable* drawable, GC* gc, int x, int y, int width, int height, RgbDither dith, ubyte* buf, int rowstride, int xdith, int ydith) nothrow;
void gdk_draw_rgb_image(Drawable* drawable, GC* gc, int x, int y, int width, int height, RgbDither dith, ubyte* rgb_buf, int rowstride) nothrow;
void gdk_draw_rgb_image_dithalign(Drawable* drawable, GC* gc, int x, int y, int width, int height, RgbDither dith, ubyte* rgb_buf, int rowstride, int xdith, int ydith) nothrow;
void gdk_draw_segments(Drawable* drawable, GC* gc, Segment* segs, int n_segs) nothrow;
void gdk_draw_string(Drawable* drawable, Font* font, GC* gc, int x, int y, char* string_) nothrow;
void gdk_draw_text(Drawable* drawable, Font* font, GC* gc, int x, int y, char* text, int text_length) nothrow;
void gdk_draw_text_wc(Drawable* drawable, Font* font, GC* gc, int x, int y, WChar* text, int text_length) nothrow;
void gdk_draw_trapezoids(Drawable* drawable, GC* gc, Trapezoid* trapezoids, int n_trapezoids) nothrow;
void gdk_drop_finish(DragContext* context, int success, uint time_) nothrow;
void gdk_drop_reply(DragContext* context, int ok, uint time_) nothrow;
int gdk_error_trap_pop() nothrow;
void gdk_error_trap_push() nothrow;
int gdk_events_pending() nothrow;
void gdk_exit(int error_code) nothrow;
void gdk_flush() nothrow;
Font* /*new*/ gdk_fontset_load(char* fontset_name) nothrow;
Font* /*new*/ gdk_fontset_load_for_display(Display* display, char* fontset_name) nothrow;
void gdk_free_compound_text(ubyte* ctext) nothrow;
void gdk_free_text_list(char** list) nothrow;
Window* gdk_get_default_root_window() nothrow;
char* /*new*/ gdk_get_display() nothrow;
char* gdk_get_display_arg_name() nothrow;
char* gdk_get_program_class() nothrow;
int gdk_get_show_events() nothrow;
int gdk_get_use_xshm() nothrow;
void gdk_init(/*inout*/ int* argc, /*inout*/ char*** argv) nothrow;
int gdk_init_check(/*inout*/ int* argc, /*inout*/ char*** argv) nothrow;
int gdk_input_add(int source, InputCondition condition, InputFunction function_, void* data) nothrow;
int gdk_input_add_full(int source, InputCondition condition, InputFunction function_, void* data, GLib2.DestroyNotify destroy) nothrow;
void gdk_input_remove(int tag) nothrow;
void gdk_input_set_extension_events(Window* window, int mask, ExtensionMode mode) nothrow;
GrabStatus gdk_keyboard_grab(Window* window, int owner_events, uint time_) nothrow;
void gdk_keyboard_ungrab(uint time_) nothrow;
void gdk_keyval_convert_case(uint symbol, /*out*/ uint* lower, /*out*/ uint* upper) nothrow;
uint gdk_keyval_from_name(char* keyval_name) nothrow;
int gdk_keyval_is_lower(uint keyval) nothrow;
int gdk_keyval_is_upper(uint keyval) nothrow;
char* gdk_keyval_name(uint keyval) nothrow;
uint gdk_keyval_to_lower(uint keyval) nothrow;
uint gdk_keyval_to_unicode(uint keyval) nothrow;
uint gdk_keyval_to_upper(uint keyval) nothrow;
GLib2.List* /*new container*/ gdk_list_visuals() nothrow;
int gdk_mbstowcs(WChar* dest, char* src, int dest_max) nothrow;
void gdk_notify_startup_complete() nothrow;
void gdk_notify_startup_complete_with_id(char* startup_id) nothrow;
Window* gdk_offscreen_window_get_embedder(Window* window) nothrow;
Pixmap* gdk_offscreen_window_get_pixmap(Window* window) nothrow;
void gdk_offscreen_window_set_embedder(Window* window, Window* embedder) nothrow;
Pango.Context* gdk_pango_context_get() nothrow;
Pango.Context* gdk_pango_context_get_for_screen(Screen* screen) nothrow;
void gdk_pango_context_set_colormap(Pango.Context* context, Colormap* colormap) nothrow;
Region* gdk_pango_layout_get_clip_region(Pango.Layout* layout, int x_origin, int y_origin, int* index_ranges, int n_ranges) nothrow;
Region* gdk_pango_layout_line_get_clip_region(Pango.LayoutLine* line, int x_origin, int y_origin, int* index_ranges, int n_ranges) nothrow;
void gdk_parse_args(/*inout*/ int* argc, /*inout*/ char*** argv) nothrow;
GdkPixbuf2.Pixbuf* gdk_pixbuf_get_from_drawable(GdkPixbuf2.Pixbuf* dest, Drawable* src, Colormap* cmap, int src_x, int src_y, int dest_x, int dest_y, int width, int height) nothrow;
GdkPixbuf2.Pixbuf* gdk_pixbuf_get_from_image(GdkPixbuf2.Pixbuf* dest, Image* src, Colormap* cmap, int src_x, int src_y, int dest_x, int dest_y, int width, int height) nothrow;
void gdk_pixbuf_render_pixmap_and_mask(GdkPixbuf2.Pixbuf* pixbuf, Pixmap** pixmap_return, Bitmap** mask_return, int alpha_threshold) nothrow;
void gdk_pixbuf_render_pixmap_and_mask_for_colormap(GdkPixbuf2.Pixbuf* pixbuf, Colormap* colormap, Pixmap** pixmap_return, Bitmap** mask_return, int alpha_threshold) nothrow;
void gdk_pixbuf_render_threshold_alpha(GdkPixbuf2.Pixbuf* pixbuf, Bitmap* bitmap, int src_x, int src_y, int dest_x, int dest_y, int width, int height, int alpha_threshold) nothrow;
void gdk_pixbuf_render_to_drawable(GdkPixbuf2.Pixbuf* pixbuf, Drawable* drawable, GC* gc, int src_x, int src_y, int dest_x, int dest_y, int width, int height, RgbDither dither, int x_dither, int y_dither) nothrow;
void gdk_pixbuf_render_to_drawable_alpha(GdkPixbuf2.Pixbuf* pixbuf, Drawable* drawable, int src_x, int src_y, int dest_x, int dest_y, int width, int height, GdkPixbuf2.PixbufAlphaMode alpha_mode, int alpha_threshold, RgbDither dither, int x_dither, int y_dither) nothrow;
GrabStatus gdk_pointer_grab(Window* window, int owner_events, EventMask event_mask, Window* confine_to, Cursor* cursor, uint time_) nothrow;
int gdk_pointer_grab_info_libgtk_only(Display* display, Window** grab_window, int* owner_events) nothrow;
int gdk_pointer_is_grabbed() nothrow;
void gdk_pointer_ungrab(uint time_) nothrow;
void gdk_pre_parse_libgtk_only() nothrow;
void gdk_property_change(Window* window, Atom property, Atom type, int format, PropMode mode, ubyte* data, int nelements) nothrow;
void gdk_property_delete(Window* window, Atom property) nothrow;
int gdk_property_get(Window* window, Atom property, Atom type, c_ulong offset, c_ulong length, int pdelete, Atom* actual_property_type, int* actual_format, int* actual_length, ubyte** data) nothrow;
void gdk_query_depths(/*out*/ int** depths, /*out*/ int* count) nothrow;
void gdk_query_visual_types(VisualType** visual_types, int* count) nothrow;
int gdk_rgb_colormap_ditherable(Colormap* cmap) nothrow;
int gdk_rgb_ditherable() nothrow;
void gdk_rgb_find_color(Colormap* colormap, Color* color) nothrow;
void gdk_rgb_gc_set_background(GC* gc, uint rgb) nothrow;
void gdk_rgb_gc_set_foreground(GC* gc, uint rgb) nothrow;
Colormap* gdk_rgb_get_colormap() nothrow;
Visual* gdk_rgb_get_visual() nothrow;
void gdk_rgb_init() nothrow;
void gdk_rgb_set_install(int install) nothrow;
void gdk_rgb_set_min_colors(int min_colors) nothrow;
void gdk_rgb_set_verbose(int verbose) nothrow;
c_ulong gdk_rgb_xpixel_from_rgb(uint rgb) nothrow;
void gdk_selection_convert(Window* requestor, Atom selection, Atom target, uint time_) nothrow;
Window* gdk_selection_owner_get(Atom selection) nothrow;
Window* gdk_selection_owner_get_for_display(Display* display, Atom selection) nothrow;
int gdk_selection_owner_set(Window* owner, Atom selection, uint time_, int send_event) nothrow;
int gdk_selection_owner_set_for_display(Display* display, Window* owner, Atom selection, uint time_, int send_event) nothrow;
int gdk_selection_property_get(Window* requestor, ubyte** data, Atom* prop_type, int* prop_format) nothrow;
void gdk_selection_send_notify(NativeWindow requestor, Atom selection, Atom target, Atom property, uint time_) nothrow;
void gdk_selection_send_notify_for_display(Display* display, NativeWindow requestor, Atom selection, Atom target, Atom property, uint time_) nothrow;
void gdk_set_double_click_time(uint msec) nothrow;
char* /*new*/ gdk_set_locale() nothrow;
PointerHooks* gdk_set_pointer_hooks(PointerHooks* new_hooks) nothrow;
void gdk_set_program_class(char* program_class) nothrow;
void gdk_set_show_events(int show_events) nothrow;
void gdk_set_sm_client_id(char* sm_client_id) nothrow;
void gdk_set_use_xshm(int use_xshm) nothrow;
int gdk_setting_get(char* name, GObject2.Value* value) nothrow;
int gdk_spawn_command_line_on_screen(Screen* screen, char* command_line, GLib2.Error** error) nothrow;
int gdk_spawn_on_screen(Screen* screen, char* working_directory, char** argv, char** envp, GLib2.SpawnFlags flags, GLib2.SpawnChildSetupFunc child_setup, void* user_data, int* child_pid, GLib2.Error** error) nothrow;
int gdk_spawn_on_screen_with_pipes(Screen* screen, char* working_directory, char** argv, char** envp, GLib2.SpawnFlags flags, GLib2.SpawnChildSetupFunc child_setup, void* user_data, int* child_pid, int* standard_input, int* standard_output, int* standard_error, GLib2.Error** error) nothrow;
void gdk_string_extents(Font* font, char* string_, int* lbearing, int* rbearing, int* width, int* ascent, int* descent) nothrow;
int gdk_string_height(Font* font, char* string_) nothrow;
int gdk_string_measure(Font* font, char* string_) nothrow;
int gdk_string_to_compound_text(char* str, Atom* encoding, int* format, ubyte** ctext, int* length) nothrow;
int gdk_string_to_compound_text_for_display(Display* display, char* str, Atom* encoding, int* format, ubyte** ctext, int* length) nothrow;
int gdk_string_width(Font* font, char* string_) nothrow;
void gdk_synthesize_window_state(Window* window, WindowState unset_flags, WindowState set_flags) nothrow;
void gdk_test_render_sync(Window* window) nothrow;
int gdk_test_simulate_button(Window* window, int x, int y, uint button, ModifierType modifiers, EventType button_pressrelease) nothrow;
int gdk_test_simulate_key(Window* window, int x, int y, uint keyval, ModifierType modifiers, EventType key_pressrelease) nothrow;
void gdk_text_extents(Font* font, char* text, int text_length, int* lbearing, int* rbearing, int* width, int* ascent, int* descent) nothrow;
void gdk_text_extents_wc(Font* font, WChar* text, int text_length, int* lbearing, int* rbearing, int* width, int* ascent, int* descent) nothrow;
int gdk_text_height(Font* font, char* text, int text_length) nothrow;
int gdk_text_measure(Font* font, char* text, int text_length) nothrow;
int gdk_text_property_to_text_list(Atom encoding, int format, ubyte* text, int length, char*** list) nothrow;
int gdk_text_property_to_text_list_for_display(Display* display, Atom encoding, int format, ubyte* text, int length, char*** list) nothrow;
int gdk_text_property_to_utf8_list(Atom encoding, int format, ubyte* text, int length, char*** list=null) nothrow;
int gdk_text_property_to_utf8_list_for_display(Display* display, Atom encoding, int format, ubyte* text, int length, char*** list) nothrow;
int gdk_text_width(Font* font, char* text, int text_length) nothrow;
int gdk_text_width_wc(Font* font, WChar* text, int text_length) nothrow;
uint gdk_threads_add_idle(GLib2.SourceFunc function_, void* data) nothrow;
uint gdk_threads_add_idle_full(int priority, GLib2.SourceFunc function_, void* data, GLib2.DestroyNotify notify=null) nothrow;
uint gdk_threads_add_timeout(uint interval, GLib2.SourceFunc function_, void* data) nothrow;
uint gdk_threads_add_timeout_full(int priority, uint interval, GLib2.SourceFunc function_, void* data, GLib2.DestroyNotify notify=null) nothrow;
uint gdk_threads_add_timeout_seconds(uint interval, GLib2.SourceFunc function_, void* data) nothrow;
uint gdk_threads_add_timeout_seconds_full(int priority, uint interval, GLib2.SourceFunc function_, void* data, GLib2.DestroyNotify notify=null) nothrow;
void gdk_threads_enter() nothrow;
void gdk_threads_init() nothrow;
void gdk_threads_leave() nothrow;
void gdk_threads_set_lock_functions(GObject2.Callback enter_fn, GObject2.Callback leave_fn) nothrow;
uint gdk_unicode_to_keyval(uint wc) nothrow;
int gdk_utf8_to_compound_text(char* str, Atom* encoding, int* format, ubyte** ctext, int* length) nothrow;
int gdk_utf8_to_compound_text_for_display(Display* display, char* str, Atom* encoding, int* format, ubyte** ctext, int* length) nothrow;
char* /*new*/ gdk_utf8_to_string_target(char* str) nothrow;
char* /*new*/ gdk_wcstombs(WChar* src) nothrow;
Window* gdk_window_at_pointer(/*out*/ int* win_x=null, /*out*/ int* win_y=null) nothrow;
void gdk_window_constrain_size(Geometry* geometry, uint flags, int width, int height, /*out*/ int* new_width, /*out*/ int* new_height) nothrow;
Window* gdk_window_foreign_new(NativeWindow anid) nothrow;
Window* gdk_window_foreign_new_for_display(Display* display, NativeWindow anid) nothrow;
GLib2.List* gdk_window_get_toplevels() nothrow;
Window* gdk_window_lookup(NativeWindow anid) nothrow;
Window* gdk_window_lookup_for_display(Display* display, NativeWindow anid) nothrow;
void gdk_window_process_all_updates() nothrow;
void gdk_window_set_debug_updates(int setting) nothrow;
}

// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/GdkPixbuf-2.0.gir"

module GdkPixbuf2;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gmodule2;
alias gtk2.gmodule2 GModule2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;
public import gtk2.gio2;
alias gtk2.gio2 Gio2;

// package: "gdk-pixbuf-2.0";
// C header: "gdk-pixbuf/gdk-pixbuf.h";

// c:symbol-prefixes: ["gdk"]
// c:identifier-prefixes: ["Gdk"]

// module GdkPixbuf2;


// This enumeration defines the color spaces that are supported by
// the &gdk-pixbuf; library.  Currently only RGB is supported.
enum Colorspace {
   RGB = 0
}

// This enumeration describes the different interpolation modes that
// can be used with the scaling functions. @GDK_INTERP_NEAREST is 
// the fastest scaling method, but has horrible quality when 
// scaling down. @GDK_INTERP_BILINEAR is the best choice if you 
// aren't sure what to choose, it has a good speed/quality balance.
// <note>
// Cubic filtering is missing from the list; hyperbolic
// interpolation is just as fast and results in higher quality.
// </note>
enum InterpType {
   NEAREST = 0,
   TILES = 1,
   BILINEAR = 2,
   HYPER = 3
}
enum int PIXBUF_FEATURES_H = 1;
enum int PIXBUF_MAGIC_NUMBER = 1197763408;
enum int PIXBUF_MAJOR = 2;
enum int PIXBUF_MICRO = 1;
enum int PIXBUF_MINOR = 24;
enum PIXBUF_VERSION = "2.24.1";
enum int PIXDATA_HEADER_LENGTH = 24;

// This is the main structure in the &gdk-pixbuf; library.  It is
// used to represent images.  It contains information about the
// image's pixel data, its color space, bits per sample, width and
// height, and the rowstride (the number of bytes between the start of
// one row and the start of the next).
struct Pixbuf /* : GObject.Object */ {
   mixin Gio2.Icon.__interface__;
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // Creates a new #GdkPixbuf structure and allocates a buffer for it.  The 
   // buffer has an optimal rowstride.  Note that the buffer is not cleared;
   // you will have to fill it completely yourself.
   // %NULL if not enough memory could be allocated for the image buffer.
   // RETURNS: A newly-created #GdkPixbuf with a reference count of 1, or
   // <colorspace>: Color space for image
   // <has_alpha>: Whether the image should have transparency information
   // <bits_per_sample>: Number of bits per color sample
   // <width>: Width of image in pixels, must be > 0
   // <height>: Height of image in pixels, must be > 0
   static Pixbuf* /*new*/ new_()(Colorspace colorspace, int has_alpha, int bits_per_sample, int width, int height) nothrow {
      return gdk_pixbuf_new(colorspace, has_alpha, bits_per_sample, width, height);
   }
   static auto opCall()(Colorspace colorspace, int has_alpha, int bits_per_sample, int width, int height) {
      return gdk_pixbuf_new(colorspace, has_alpha, bits_per_sample, width, height);
   }

   // Creates a new #GdkPixbuf out of in-memory image data.  Currently only RGB
   // images with 8 bits per sample are supported.
   // RETURNS: A newly-created #GdkPixbuf structure with a reference count of 1.
   // <data>: Image data in 8-bit/sample packed format
   // <colorspace>: Colorspace for the image data
   // <has_alpha>: Whether the data has an opacity channel
   // <bits_per_sample>: Number of bits per sample
   // <width>: Width of the image in pixels, must be > 0
   // <height>: Height of the image in pixels, must be > 0
   // <rowstride>: Distance in bytes between row starts
   // <destroy_fn>: Function used to free the data when the pixbuf's reference count drops to zero, or %NULL if the data should not be freed
   // <destroy_fn_data>: Closure data to pass to the destroy notification function
   static Pixbuf* /*new*/ new_from_data(AT0, AT1)(AT0 /*ubyte*/ data, Colorspace colorspace, int has_alpha, int bits_per_sample, int width, int height, int rowstride, PixbufDestroyNotify destroy_fn, AT1 /*void*/ destroy_fn_data) nothrow {
      return gdk_pixbuf_new_from_data(UpCast!(ubyte*)(data), colorspace, has_alpha, bits_per_sample, width, height, rowstride, destroy_fn, UpCast!(void*)(destroy_fn_data));
   }
   static auto opCall(AT0, AT1)(AT0 /*ubyte*/ data, Colorspace colorspace, int has_alpha, int bits_per_sample, int width, int height, int rowstride, PixbufDestroyNotify destroy_fn, AT1 /*void*/ destroy_fn_data) {
      return gdk_pixbuf_new_from_data(UpCast!(ubyte*)(data), colorspace, has_alpha, bits_per_sample, width, height, rowstride, destroy_fn, UpCast!(void*)(destroy_fn_data));
   }

   // Creates a new pixbuf by loading an image from a file.  The file format is
   // detected automatically. If %NULL is returned, then @error will be set.
   // Possible errors are in the #GDK_PIXBUF_ERROR and #G_FILE_ERROR domains.
   // there was no loader for the file's format, there was not enough memory to
   // allocate the image buffer, or the image file contained invalid data.
   // RETURNS: A newly-created pixbuf with a reference count of 1, or %NULL if
   // <filename>: Name of file to load, in the GLib file name encoding
   static Pixbuf* /*new*/ new_from_file(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_new_from_file(toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }
   static auto opCall(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) {
      return gdk_pixbuf_new_from_file(toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 2.6
   // Creates a new pixbuf by loading an image from a file.  The file format is
   // detected automatically. If %NULL is returned, then @error will be set.
   // Possible errors are in the #GDK_PIXBUF_ERROR and #G_FILE_ERROR domains.
   // The image will be scaled to fit in the requested size, optionally preserving
   // the image's aspect ratio. 
   // When preserving the aspect ratio, a @width of -1 will cause the image
   // to be scaled to the exact given height, and a @height of -1 will cause
   // the image to be scaled to the exact given width. When not preserving
   // aspect ratio, a @width or @height of -1 means to not scale the image 
   // at all in that dimension. Negative values for @width and @height are 
   // allowed since 2.8.
   // there was no loader for the file's format, there was not enough memory to
   // allocate the image buffer, or the image file contained invalid data.
   // RETURNS: A newly-created pixbuf with a reference count of 1, or %NULL
   // <filename>: Name of file to load, in the GLib file name encoding
   // <width>: The width the image should have or -1 to not constrain the width
   // <height>: The height the image should have or -1 to not constrain the height
   // <preserve_aspect_ratio>: %TRUE to preserve the image's aspect ratio
   static Pixbuf* /*new*/ new_from_file_at_scale(AT0, AT1)(AT0 /*char*/ filename, int width, int height, int preserve_aspect_ratio, AT1 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_new_from_file_at_scale(toCString!(char*)(filename), width, height, preserve_aspect_ratio, UpCast!(GLib2.Error**)(error));
   }
   static auto opCall(AT0, AT1)(AT0 /*char*/ filename, int width, int height, int preserve_aspect_ratio, AT1 /*GLib2.Error**/ error=null) {
      return gdk_pixbuf_new_from_file_at_scale(toCString!(char*)(filename), width, height, preserve_aspect_ratio, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 2.4
   // Creates a new pixbuf by loading an image from a file.  
   // The file format is detected automatically. If %NULL is returned, then 
   // #G_FILE_ERROR domains.
   // The image will be scaled to fit in the requested size, preserving
   // the image's aspect ratio. Note that the returned pixbuf may be smaller
   // than @width x @height, if the aspect ratio requires it. To load
   // and image at the requested size, regardless of aspect ratio, use
   // gdk_pixbuf_new_from_file_at_scale().
   // %NULL if any of several error conditions occurred:  the file could not 
   // be opened, there was no loader for the file's format, there was not 
   // enough memory to allocate the image buffer, or the image file contained 
   // invalid data.
   // RETURNS: A newly-created pixbuf with a reference count of 1, or
   // <filename>: Name of file to load, in the GLib file name encoding
   // <width>: The width the image should have or -1 to not constrain the width
   // <height>: The height the image should have or -1 to not constrain the height
   static Pixbuf* /*new*/ new_from_file_at_size(AT0, AT1)(AT0 /*char*/ filename, int width, int height, AT1 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_new_from_file_at_size(toCString!(char*)(filename), width, height, UpCast!(GLib2.Error**)(error));
   }
   static auto opCall(AT0, AT1)(AT0 /*char*/ filename, int width, int height, AT1 /*GLib2.Error**/ error=null) {
      return gdk_pixbuf_new_from_file_at_size(toCString!(char*)(filename), width, height, UpCast!(GLib2.Error**)(error));
   }

   // Create a #GdkPixbuf from a flat representation that is suitable for
   // storing as inline data in a program. This is useful if you want to
   // ship a program with images, but don't want to depend on any
   // external files.
   // gdk-pixbuf ships with a program called <command>gdk-pixbuf-csource</command> 
   // which allows for conversion of #GdkPixbufs into such a inline representation.
   // In almost all cases, you should pass the <option>--raw</option> flag to
   // <command>gdk-pixbuf-csource</command>. A sample invocation would be:
   // <informalexample><programlisting>
   // gdk-pixbuf-csource --raw --name=myimage_inline myimage.png
   // </programlisting></informalexample>
   // For the typical case where the inline pixbuf is read-only static data,
   // you don't need to copy the pixel data unless you intend to write to
   // it, so you can pass %FALSE for @copy_pixels.  (If you pass 
   // <option>--rle</option> to <command>gdk-pixbuf-csource</command>, a copy 
   // will be made even if @copy_pixels is %FALSE, so using this option is 
   // generally a bad idea.)
   // If you create a pixbuf from const inline data compiled into your
   // program, it's probably safe to ignore errors and disable length checks, 
   // since things will always succeed:
   // <informalexample><programlisting>
   // pixbuf = gdk_pixbuf_new_from_inline (-1, myimage_inline, FALSE, NULL);
   // </programlisting></informalexample>
   // For non-const inline data, you could get out of memory. For untrusted 
   // inline data located at runtime, you could have corrupt inline data in 
   // addition.
   // count of 1, or %NULL if an error occurred.
   // RETURNS: A newly-created #GdkPixbuf structure with a reference,
   // <data_length>: Length in bytes of the @data argument or -1 to disable length checks
   // <data>: Byte data containing a serialized #GdkPixdata structure
   // <copy_pixels>: Whether to copy the pixel data, or use direct pointers
   static Pixbuf* /*new*/ new_from_inline(AT0, AT1)(int data_length, AT0 /*ubyte*/ data, int copy_pixels, AT1 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_new_from_inline(data_length, UpCast!(ubyte*)(data), copy_pixels, UpCast!(GLib2.Error**)(error));
   }
   static auto opCall(AT0, AT1)(int data_length, AT0 /*ubyte*/ data, int copy_pixels, AT1 /*GLib2.Error**/ error=null) {
      return gdk_pixbuf_new_from_inline(data_length, UpCast!(ubyte*)(data), copy_pixels, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 2.14
   // Creates a new pixbuf by loading an image from an input stream.  
   // The file format is detected automatically. If %NULL is returned, then 
   // from another thread. If the operation was cancelled, the error 
   // %GIO_ERROR_CANCELLED will be returned. Other possible errors are in 
   // the #GDK_PIXBUF_ERROR and %G_IO_ERROR domains. 
   // The stream is not closed.
   // not supported, there was not enough memory to allocate the image buffer, 
   // the stream contained invalid data, or the operation was cancelled.
   // RETURNS: A newly-created pixbuf, or %NULL if any of several error
   // <stream>: a #GInputStream to load the pixbuf from
   // <cancellable>: optional #GCancellable object, %NULL to ignore
   static Pixbuf* /*new*/ new_from_stream(AT0, AT1, AT2)(AT0 /*Gio2.InputStream*/ stream, AT1 /*Gio2.Cancellable*/ cancellable, AT2 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_new_from_stream(UpCast!(Gio2.InputStream*)(stream), UpCast!(Gio2.Cancellable*)(cancellable), UpCast!(GLib2.Error**)(error));
   }
   static auto opCall(AT0, AT1, AT2)(AT0 /*Gio2.InputStream*/ stream, AT1 /*Gio2.Cancellable*/ cancellable, AT2 /*GLib2.Error**/ error=null) {
      return gdk_pixbuf_new_from_stream(UpCast!(Gio2.InputStream*)(stream), UpCast!(Gio2.Cancellable*)(cancellable), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 2.14
   // Creates a new pixbuf by loading an image from an input stream.  
   // The file format is detected automatically. If %NULL is returned, then 
   // from another thread. If the operation was cancelled, the error 
   // %GIO_ERROR_CANCELLED will be returned. Other possible errors are in 
   // the #GDK_PIXBUF_ERROR and %G_IO_ERROR domains. 
   // The image will be scaled to fit in the requested size, optionally 
   // preserving the image's aspect ratio. When preserving the aspect ratio, 
   // a @width of -1 will cause the image to be scaled to the exact given 
   // height, and a @height of -1 will cause the image to be scaled to the 
   // exact given width. When not preserving aspect ratio, a @width or 
   // The stream is not closed.
   // not supported, there was not enough memory to allocate the image buffer, 
   // the stream contained invalid data, or the operation was cancelled.
   // RETURNS: A newly-created pixbuf, or %NULL if any of several error
   // <stream>: a #GInputStream to load the pixbuf from
   // <width>: The width the image should have or -1 to not constrain the width
   // <height>: The height the image should have or -1 to not constrain the height
   // <preserve_aspect_ratio>: %TRUE to preserve the image's aspect ratio
   // <cancellable>: optional #GCancellable object, %NULL to ignore
   static Pixbuf* /*new*/ new_from_stream_at_scale(AT0, AT1, AT2)(AT0 /*Gio2.InputStream*/ stream, int width, int height, int preserve_aspect_ratio, AT1 /*Gio2.Cancellable*/ cancellable, AT2 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_new_from_stream_at_scale(UpCast!(Gio2.InputStream*)(stream), width, height, preserve_aspect_ratio, UpCast!(Gio2.Cancellable*)(cancellable), UpCast!(GLib2.Error**)(error));
   }
   static auto opCall(AT0, AT1, AT2)(AT0 /*Gio2.InputStream*/ stream, int width, int height, int preserve_aspect_ratio, AT1 /*Gio2.Cancellable*/ cancellable, AT2 /*GLib2.Error**/ error=null) {
      return gdk_pixbuf_new_from_stream_at_scale(UpCast!(Gio2.InputStream*)(stream), width, height, preserve_aspect_ratio, UpCast!(Gio2.Cancellable*)(cancellable), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 2.24
   // Finishes an asynchronous pixbuf creation operation started with
   // gdk_pixbuf_new_from_stream_async().
   // object with g_object_unref().
   // RETURNS: a #GdkPixbuf or %NULL on error. Free the returned
   // <async_result>: a #GAsyncResult
   static Pixbuf* /*new*/ new_from_stream_finish(AT0, AT1)(AT0 /*Gio2.AsyncResult*/ async_result, AT1 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_new_from_stream_finish(UpCast!(Gio2.AsyncResult*)(async_result), UpCast!(GLib2.Error**)(error));
   }
   static auto opCall(AT0, AT1)(AT0 /*Gio2.AsyncResult*/ async_result, AT1 /*GLib2.Error**/ error=null) {
      return gdk_pixbuf_new_from_stream_finish(UpCast!(Gio2.AsyncResult*)(async_result), UpCast!(GLib2.Error**)(error));
   }

   // Creates a new pixbuf by parsing XPM data in memory.  This data is commonly
   // the result of including an XPM file into a program's C source.
   // RETURNS: A newly-created pixbuf with a reference count of 1.
   // <data>: Pointer to inline XPM data.
   static Pixbuf* /*new*/ new_from_xpm_data(AT0)(AT0 /*char**/ data) nothrow {
      return gdk_pixbuf_new_from_xpm_data(toCString!(char**)(data));
   }
   static auto opCall(AT0)(AT0 /*char**/ data) {
      return gdk_pixbuf_new_from_xpm_data(toCString!(char**)(data));
   }

   // Unintrospectable function: from_pixdata() / gdk_pixbuf_from_pixdata()
   // Converts a #GdkPixdata to a #GdkPixbuf. If @copy_pixels is %TRUE or
   // if the pixel data is run-length-encoded, the pixel data is copied into
   // newly-allocated memory; otherwise it is reused.
   // RETURNS: a new #GdkPixbuf.
   // <pixdata>: a #GdkPixdata to convert into a #GdkPixbuf.
   // <copy_pixels>: whether to copy raw pixel data; run-length encoded pixel data is always copied.
   static Pixbuf* from_pixdata(AT0, AT1)(AT0 /*Pixdata*/ pixdata, int copy_pixels, AT1 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_from_pixdata(UpCast!(Pixdata*)(pixdata), copy_pixels, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 2.4
   // Parses an image file far enough to determine its format and size.
   // or %NULL if the image format wasn't recognized. The return value 
   // is owned by GdkPixbuf and should not be freed.
   // RETURNS: A #GdkPixbufFormat describing the image format of the file
   // <filename>: The name of the file to identify.
   // <width>: Return location for the width of the image, or %NULL
   // <height>: Return location for the height of the image, or %NULL
   static PixbufFormat* /*new*/ get_file_info(AT0)(AT0 /*char*/ filename, int* width, int* height) nothrow {
      return gdk_pixbuf_get_file_info(toCString!(char*)(filename), width, height);
   }

   // VERSION: 2.2
   // Obtains the available information about the image formats supported
   // by GdkPixbuf.
   // #GdkPixbufFormat<!-- -->s describing the supported
   // image formats.  The list should be freed when it is no longer needed,
   // but the structures themselves are owned by #GdkPixbuf and should not be
   // freed.
   // RETURNS: A list of
   static GLib2.SList* /*new container*/ get_formats()() nothrow {
      return gdk_pixbuf_get_formats();
   }
   static char* gettext(AT0)(AT0 /*char*/ msgid) nothrow {
      return gdk_pixbuf_gettext(toCString!(char*)(msgid));
   }

   // VERSION: 2.24
   // Creates a new pixbuf by asynchronously loading an image from an input stream.
   // For more details see gdk_pixbuf_new_from_stream(), which is the synchronous
   // version of this function.
   // When the operation is finished, @callback will be called in the main thread.
   // You can then call gdk_pixbuf_new_from_stream_finish() to get the result of the operation.
   // <stream>: a #GInputStream from which to load the pixbuf
   // <cancellable>: optional #GCancellable object, %NULL to ignore
   // <callback>: a #GAsyncReadyCallback to call when the the pixbuf is loaded
   // <user_data>: the data to pass to the callback function
   static void new_from_stream_async(AT0, AT1, AT2)(AT0 /*Gio2.InputStream*/ stream, AT1 /*Gio2.Cancellable*/ cancellable, Gio2.AsyncReadyCallback callback, AT2 /*void*/ user_data) nothrow {
      gdk_pixbuf_new_from_stream_async(UpCast!(Gio2.InputStream*)(stream), UpCast!(Gio2.Cancellable*)(cancellable), callback, UpCast!(void*)(user_data));
   }

   // VERSION: 2.24
   // Creates a new pixbuf by asynchronously loading an image from an input stream.
   // For more details see gdk_pixbuf_new_from_stream_at_scale(), which is the synchronous
   // version of this function.
   // When the operation is finished, @callback will be called in the main thread.
   // You can then call gdk_pixbuf_new_from_stream_finish() to get the result of the operation.
   // <stream>: a #GInputStream from which to load the pixbuf
   // <width>: the width the image should have or -1 to not constrain the width
   // <height>: the height the image should have or -1 to not constrain the height
   // <preserve_aspect_ratio>: %TRUE to preserve the image's aspect ratio
   // <cancellable>: optional #GCancellable object, %NULL to ignore
   // <callback>: a #GAsyncReadyCallback to call when the the pixbuf is loaded
   // <user_data>: the data to pass to the callback function
   static void new_from_stream_at_scale_async(AT0, AT1, AT2)(AT0 /*Gio2.InputStream*/ stream, int width, int height, int preserve_aspect_ratio, AT1 /*Gio2.Cancellable*/ cancellable, Gio2.AsyncReadyCallback callback, AT2 /*void*/ user_data) nothrow {
      gdk_pixbuf_new_from_stream_at_scale_async(UpCast!(Gio2.InputStream*)(stream), width, height, preserve_aspect_ratio, UpCast!(Gio2.Cancellable*)(cancellable), callback, UpCast!(void*)(user_data));
   }

   // VERSION: 2.24
   // Finishes an asynchronous pixbuf save operation started with
   // gdk_pixbuf_save_to_stream_async().
   // RETURNS: %TRUE if the pixbuf was saved successfully, %FALSE if an error was set.
   // <async_result>: a #GAsyncResult
   static int save_to_stream_finish(AT0, AT1)(AT0 /*Gio2.AsyncResult*/ async_result, AT1 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_save_to_stream_finish(UpCast!(Gio2.AsyncResult*)(async_result), UpCast!(GLib2.Error**)(error));
   }

   // Takes an existing pixbuf and adds an alpha channel to it.
   // If the existing pixbuf already had an alpha channel, the channel
   // values are copied from the original; otherwise, the alpha channel
   // is initialized to 255 (full opacity).
   // If @substitute_color is %TRUE, then the color specified by (@r, @g, @b) will be
   // assigned zero opacity. That is, if you pass (255, 255, 255) for the
   // substitute color, all white pixels will become fully transparent.
   // RETURNS: A newly-created pixbuf with a reference count of 1.
   // <substitute_color>: Whether to set a color to zero opacity.  If this is %FALSE, then the (@r, @g, @b) arguments will be ignored.
   // <r>: Red value to substitute.
   // <g>: Green value to substitute.
   // <b>: Blue value to substitute.
   Pixbuf* /*new*/ add_alpha()(int substitute_color, ubyte r, ubyte g, ubyte b) nothrow {
      return gdk_pixbuf_add_alpha(&this, substitute_color, r, g, b);
   }

   // VERSION: 2.12
   // Takes an existing pixbuf and checks for the presence of an
   // associated "orientation" option, which may be provided by the 
   // jpeg loader (which reads the exif orientation tag) or the 
   // tiff loader (which reads the tiff orientation tag, and
   // compensates it for the partial transforms performed by 
   // libtiff). If an orientation option/tag is present, the
   // appropriate transform will be performed so that the pixbuf
   // is oriented correctly.
   // input pixbuf (with an increased reference count).
   // RETURNS: A newly-created pixbuf, or a reference to the
   Pixbuf* /*new*/ apply_embedded_orientation()() nothrow {
      return gdk_pixbuf_apply_embedded_orientation(&this);
   }

   // Creates a transformation of the source image @src by scaling by
   // This gives an image in the coordinates of the destination pixbuf.
   // The rectangle (@dest_x, @dest_y, @dest_width, @dest_height)
   // is then composited onto the corresponding rectangle of the
   // original destination image.
   // When the destination rectangle contains parts not in the source 
   // image, the data at the edges of the source image is replicated
   // to infinity. 
   // <figure id="pixbuf-composite-diagram">
   // <title>Compositing of pixbufs</title>
   // <graphic fileref="composite.png" format="PNG"/>
   // </figure>
   // <dest>: the #GdkPixbuf into which to render the results
   // <dest_x>: the left coordinate for region to render
   // <dest_y>: the top coordinate for region to render
   // <dest_width>: the width of the region to render
   // <dest_height>: the height of the region to render
   // <offset_x>: the offset in the X direction (currently rounded to an integer)
   // <offset_y>: the offset in the Y direction (currently rounded to an integer)
   // <scale_x>: the scale factor in the X direction
   // <scale_y>: the scale factor in the Y direction
   // <interp_type>: the interpolation type for the transformation.
   // <overall_alpha>: overall alpha for source image (0..255)
   void composite(AT0)(AT0 /*Pixbuf*/ dest, int dest_x, int dest_y, int dest_width, int dest_height, double offset_x, double offset_y, double scale_x, double scale_y, InterpType interp_type, int overall_alpha) nothrow {
      gdk_pixbuf_composite(&this, UpCast!(Pixbuf*)(dest), dest_x, dest_y, dest_width, dest_height, offset_x, offset_y, scale_x, scale_y, interp_type, overall_alpha);
   }

   // Creates a transformation of the source image @src by scaling by
   // then composites the rectangle (@dest_x ,@dest_y, @dest_width,
   // colors @color1 and @color2 and renders it onto the destination
   // image.
   // See gdk_pixbuf_composite_color_simple() for a simpler variant of this
   // function suitable for many tasks.
   // <dest>: the #GdkPixbuf into which to render the results
   // <dest_x>: the left coordinate for region to render
   // <dest_y>: the top coordinate for region to render
   // <dest_width>: the width of the region to render
   // <dest_height>: the height of the region to render
   // <offset_x>: the offset in the X direction (currently rounded to an integer)
   // <offset_y>: the offset in the Y direction (currently rounded to an integer)
   // <scale_x>: the scale factor in the X direction
   // <scale_y>: the scale factor in the Y direction
   // <interp_type>: the interpolation type for the transformation.
   // <overall_alpha>: overall alpha for source image (0..255)
   // <check_x>: the X offset for the checkboard (origin of checkboard is at -@check_x, -@check_y)
   // <check_y>: the Y offset for the checkboard
   // <check_size>: the size of checks in the checkboard (must be a power of two)
   // <color1>: the color of check at upper left
   // <color2>: the color of the other check
   void composite_color(AT0)(AT0 /*Pixbuf*/ dest, int dest_x, int dest_y, int dest_width, int dest_height, double offset_x, double offset_y, double scale_x, double scale_y, InterpType interp_type, int overall_alpha, int check_x, int check_y, int check_size, uint color1, uint color2) nothrow {
      gdk_pixbuf_composite_color(&this, UpCast!(Pixbuf*)(dest), dest_x, dest_y, dest_width, dest_height, offset_x, offset_y, scale_x, scale_y, interp_type, overall_alpha, check_x, check_y, check_size, color1, color2);
   }

   // Creates a new #GdkPixbuf by scaling @src to @dest_width x
   // allocated for it.
   // RETURNS: the new #GdkPixbuf, or %NULL if not enough memory could be
   // <dest_width>: the width of destination image
   // <dest_height>: the height of destination image
   // <interp_type>: the interpolation type for the transformation.
   // <overall_alpha>: overall alpha for source image (0..255)
   // <check_size>: the size of checks in the checkboard (must be a power of two)
   // <color1>: the color of check at upper left
   // <color2>: the color of the other check
   Pixbuf* /*new*/ composite_color_simple()(int dest_width, int dest_height, InterpType interp_type, int overall_alpha, int check_size, uint color1, uint color2) nothrow {
      return gdk_pixbuf_composite_color_simple(&this, dest_width, dest_height, interp_type, overall_alpha, check_size, color1, color2);
   }

   // Creates a new #GdkPixbuf with a copy of the information in the specified
   // not enough memory could be allocated.
   // RETURNS: A newly-created pixbuf with a reference count of 1, or %NULL if
   Pixbuf* /*new*/ copy()() nothrow {
      return gdk_pixbuf_copy(&this);
   }

   // Copies a rectangular area from @src_pixbuf to @dest_pixbuf.  Conversion of
   // pixbuf formats is done automatically.
   // If the source rectangle overlaps the destination rectangle on the
   // same pixbuf, it will be overwritten during the copy operation.
   // Therefore, you can not use this function to scroll a pixbuf.
   // <src_x>: Source X coordinate within @src_pixbuf.
   // <src_y>: Source Y coordinate within @src_pixbuf.
   // <width>: Width of the area to copy.
   // <height>: Height of the area to copy.
   // <dest_pixbuf>: Destination pixbuf.
   // <dest_x>: X coordinate within @dest_pixbuf.
   // <dest_y>: Y coordinate within @dest_pixbuf.
   void copy_area(AT0)(int src_x, int src_y, int width, int height, AT0 /*Pixbuf*/ dest_pixbuf, int dest_x, int dest_y) nothrow {
      gdk_pixbuf_copy_area(&this, src_x, src_y, width, height, UpCast!(Pixbuf*)(dest_pixbuf), dest_x, dest_y);
   }

   // Clears a pixbuf to the given RGBA value, converting the RGBA value into
   // the pixbuf's pixel format. The alpha will be ignored if the pixbuf
   // doesn't have an alpha channel.
   // <pixel>: RGBA pixel to clear to (0xffffffff is opaque white, 0x00000000 transparent black)
   void fill()(uint pixel) nothrow {
      gdk_pixbuf_fill(&this, pixel);
   }

   // VERSION: 2.6
   // Flips a pixbuf horizontally or vertically and returns the
   // result in a new pixbuf.
   // allocated for it.
   // RETURNS: the new #GdkPixbuf, or %NULL if not enough memory could be
   // <horizontal>: %TRUE to flip horizontally, %FALSE to flip vertically
   Pixbuf* /*new*/ flip()(int horizontal) nothrow {
      return gdk_pixbuf_flip(&this, horizontal);
   }

   // Queries the number of bits per color sample in a pixbuf.
   // RETURNS: Number of bits per color sample.
   int get_bits_per_sample()() nothrow {
      return gdk_pixbuf_get_bits_per_sample(&this);
   }

   // Queries the color space of a pixbuf.
   // RETURNS: Color space.
   Colorspace get_colorspace()() nothrow {
      return gdk_pixbuf_get_colorspace(&this);
   }

   // Queries whether a pixbuf has an alpha channel (opacity information).
   // RETURNS: %TRUE if it has an alpha channel, %FALSE otherwise.
   int get_has_alpha()() nothrow {
      return gdk_pixbuf_get_has_alpha(&this);
   }

   // Queries the height of a pixbuf.
   // RETURNS: Height in pixels.
   int get_height()() nothrow {
      return gdk_pixbuf_get_height(&this);
   }

   // Queries the number of channels of a pixbuf.
   // RETURNS: Number of channels.
   int get_n_channels()() nothrow {
      return gdk_pixbuf_get_n_channels(&this);
   }

   // Looks up @key in the list of options that may have been attached to the
   // function using gdk_pixbuf_set_option().
   // For instance, the ANI loader provides "Title" and "Artist" options. 
   // The ICO, XBM, and XPM loaders provide "x_hot" and "y_hot" hot-spot 
   // options for cursor definitions. The PNG loader provides the tEXt ancillary
   // chunk key/value pairs as options. Since 2.12, the TIFF and JPEG loaders
   // return an "orientation" option string that corresponds to the embedded 
   // TIFF/Exif orientation tag (if present).
   // string that should not be freed or %NULL if @key was not found.
   // RETURNS: the value associated with @key. This is a nul-terminated
   // <key>: a nul-terminated string.
   char* get_option(AT0)(AT0 /*char*/ key) nothrow {
      return gdk_pixbuf_get_option(&this, toCString!(char*)(key));
   }

   // Queries a pointer to the pixel data of a pixbuf.
   // for information about how the pixel data is stored in
   // memory.
   // RETURNS: A pointer to the pixbuf's pixel data.  Please see <xref linkend="image-data"/>
   ubyte* get_pixels()() nothrow {
      return gdk_pixbuf_get_pixels(&this);
   }

   // Queries the rowstride of a pixbuf, which is the number of bytes between the start of a row
   // and the start of the next row.
   // RETURNS: Distance between row starts.
   int get_rowstride()() nothrow {
      return gdk_pixbuf_get_rowstride(&this);
   }

   // Queries the width of a pixbuf.
   // RETURNS: Width in pixels.
   int get_width()() nothrow {
      return gdk_pixbuf_get_width(&this);
   }

   // Creates a new pixbuf which represents a sub-region of
   // original pixbuf, so writing to one affects both.
   // The new pixbuf holds a reference to @src_pixbuf, so
   // is finalized.
   // RETURNS: a new pixbuf
   // <src_x>: X coord in @src_pixbuf
   // <src_y>: Y coord in @src_pixbuf
   // <width>: width of region in @src_pixbuf
   // <height>: height of region in @src_pixbuf
   Pixbuf* /*new*/ new_subpixbuf()(int src_x, int src_y, int width, int height) nothrow {
      return gdk_pixbuf_new_subpixbuf(&this, src_x, src_y, width, height);
   }

   // Unintrospectable method: ref() / gdk_pixbuf_ref()
   // DEPRECATED (v2.0) method: ref - Use g_object_ref().
   // Adds a reference to a pixbuf.
   // RETURNS: The same as the @pixbuf argument.
   Pixbuf* ref_()() nothrow {
      return gdk_pixbuf_ref(&this);
   }

   // VERSION: 2.6
   // Rotates a pixbuf by a multiple of 90 degrees, and returns the
   // result in a new pixbuf.
   // allocated for it.
   // RETURNS: the new #GdkPixbuf, or %NULL if not enough memory could be
   // <angle>: the angle to rotate by
   Pixbuf* /*new*/ rotate_simple()(PixbufRotation angle) nothrow {
      return gdk_pixbuf_rotate_simple(&this, angle);
   }

   // Modifies saturation and optionally pixelates @src, placing the result in
   // saturation is reduced (the image turns toward grayscale); if greater than
   // 1.0, saturation is increased (the image gets more vivid colors). If @pixelate
   // is %TRUE, then pixels are faded in a checkerboard pattern to create a
   // pixelated image. @src and @dest must have the same image format, size, and
   // rowstride.
   // <dest>: place to write modified version of @src
   // <saturation>: saturation factor
   // <pixelate>: whether to pixelate
   void saturate_and_pixelate(AT0)(AT0 /*Pixbuf*/ dest, float saturation, int pixelate) nothrow {
      gdk_pixbuf_saturate_and_pixelate(&this, UpCast!(Pixbuf*)(dest), saturation, pixelate);
   }

   // Unintrospectable method: save() / gdk_pixbuf_save()
   // Saves pixbuf to a file in format @type. By default, "jpeg", "png", "ico" 
   // and "bmp" are possible file formats to save in, but more formats may be
   // installed. The list of all writable formats can be determined in the 
   // following way:
   // |[
   // void add_if_writable (GdkPixbufFormat *data, GSList **list)
   // {
   // if (gdk_pixbuf_format_is_writable (data))
   // *list = g_slist_prepend (*list, data);
   // }
   // GSList *formats = gdk_pixbuf_get_formats ();
   // GSList *writable_formats = NULL;
   // g_slist_foreach (formats, add_if_writable, &writable_formats);
   // g_slist_free (formats);
   // ]|
   // If @error is set, %FALSE will be returned. Possible errors include 
   // those in the #GDK_PIXBUF_ERROR domain and those in the #G_FILE_ERROR domain.
   // The variable argument list should be %NULL-terminated; if not empty,
   // it should contain pairs of strings that modify the save
   // parameters. For example:
   // <informalexample><programlisting>
   // gdk_pixbuf_save (pixbuf, handle, "jpeg", &amp;error,
   // "quality", "100", NULL);
   // </programlisting></informalexample>
   // Currently only few parameters exist. JPEG images can be saved with a
   // "quality" parameter; its value should be in the range [0,100].
   // Text chunks can be attached to PNG images by specifying parameters of
   // the form "tEXt::key", where key is an ASCII string of length 1-79.
   // The values are UTF-8 encoded strings. The PNG compression level can
   // be specified using the "compression" parameter; it's value is in an
   // integer in the range of [0,9].
   // ICC color profiles can also be embedded into PNG and TIFF images.
   // The "icc-profile" value should be the complete ICC profile encoded
   // into base64.
   // <informalexample><programlisting>
   // gchar *contents;
   // gchar *contents_encode;
   // gsize length;
   // g_file_get_contents ("/home/hughsie/.color/icc/L225W.icm", &contents, &length, NULL);
   // contents_encode = g_base64_encode ((const guchar *) contents, length);
   // gdk_pixbuf_save (pixbuf, handle, "png", &amp;error,
   // "icc-profile", contents_encode,
   // NULL);
   // </programlisting></informalexample>
   // TIFF images recognize a "compression" option which acceps an integer value.
   // Among the codecs are 1 None, 2 Huffman, 5 LZW, 7 JPEG and 8 Deflate, see
   // the libtiff documentation and tiff.h for all supported codec values.
   // ICO images can be saved in depth 16, 24, or 32, by using the "depth"
   // parameter. When the ICO saver is given "x_hot" and "y_hot" parameters,
   // it produces a CUR instead of an ICO.
   // RETURNS: whether an error was set
   // <filename>: name of file to save.
   // <type>: name of file format.
   // <error>: return location for error, or %NULL
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias gdk_pixbuf_save save; // Variadic
   +/

   // Unintrospectable method: save_to_buffer() / gdk_pixbuf_save_to_buffer()
   // VERSION: 2.4
   // Saves pixbuf to a new buffer in format @type, which is currently "jpeg",
   // "png", "tiff", "ico" or "bmp".  This is a convenience function that uses
   // gdk_pixbuf_save_to_callback() to do the real work. Note that the buffer 
   // is not nul-terminated and may contain embedded  nuls.
   // If @error is set, %FALSE will be returned and @buffer will be set to
   // %NULL. Possible errors include those in the #GDK_PIXBUF_ERROR
   // domain.
   // See gdk_pixbuf_save() for more details.
   // RETURNS: whether an error was set
   // <buffer>: location to receive a pointer to the new buffer.
   // <buffer_size>: location to receive the size of the new buffer.
   // <type>: name of file format.
   // <error>: return location for error, or %NULL
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias gdk_pixbuf_save_to_buffer save_to_buffer; // Variadic
   +/

   // VERSION: 2.4
   // Saves pixbuf to a new buffer in format @type, which is currently "jpeg",
   // "tiff", "png", "ico" or "bmp".  See gdk_pixbuf_save_to_buffer() 
   // for more details.
   // RETURNS: whether an error was set
   // <buffer>: location to receive a pointer to the new buffer.
   // <buffer_size>: location to receive the size of the new buffer.
   // <type>: name of file format.
   // <option_keys>: name of options to set, %NULL-terminated
   // <option_values>: values for named options
   int save_to_bufferv(AT0, AT1, AT2, AT3, AT4, AT5)(AT0 /*char**/ buffer, AT1 /*size_t*/ buffer_size, AT2 /*char*/ type, AT3 /*char**/ option_keys, AT4 /*char**/ option_values, AT5 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_save_to_bufferv(&this, toCString!(char**)(buffer), UpCast!(size_t*)(buffer_size), toCString!(char*)(type), toCString!(char**)(option_keys), toCString!(char**)(option_values), UpCast!(GLib2.Error**)(error));
   }

   // Unintrospectable method: save_to_callback() / gdk_pixbuf_save_to_callback()
   // VERSION: 2.4
   // Saves pixbuf in format @type by feeding the produced data to a 
   // callback. Can be used when you want to store the image to something 
   // other than a file, such as an in-memory buffer or a socket.  
   // If @error is set, %FALSE will be returned. Possible errors
   // include those in the #GDK_PIXBUF_ERROR domain and whatever the save
   // function generates.
   // See gdk_pixbuf_save() for more details.
   // RETURNS: whether an error was set
   // <save_func>: a function that is called to save each block of data that the save routine generates.
   // <user_data>: user data to pass to the save function.
   // <type>: name of file format.
   // <error>: return location for error, or %NULL
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias gdk_pixbuf_save_to_callback save_to_callback; // Variadic
   +/

   // VERSION: 2.4
   // Saves pixbuf to a callback in format @type, which is currently "jpeg",
   // "png", "tiff", "ico" or "bmp".  If @error is set, %FALSE will be returned. See
   // gdk_pixbuf_save_to_callback () for more details.
   // RETURNS: whether an error was set
   // <save_func>: a function that is called to save each block of data that the save routine generates.
   // <user_data>: user data to pass to the save function.
   // <type>: name of file format.
   // <option_keys>: name of options to set, %NULL-terminated
   // <option_values>: values for named options
   int save_to_callbackv(AT0, AT1, AT2, AT3, AT4)(PixbufSaveFunc save_func, AT0 /*void*/ user_data, AT1 /*char*/ type, AT2 /*char**/ option_keys, AT3 /*char**/ option_values, AT4 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_save_to_callbackv(&this, save_func, UpCast!(void*)(user_data), toCString!(char*)(type), toCString!(char**)(option_keys), toCString!(char**)(option_values), UpCast!(GLib2.Error**)(error));
   }

   // Unintrospectable method: save_to_stream() / gdk_pixbuf_save_to_stream()
   // VERSION: 2.14
   // Saves @pixbuf to an output stream.
   // Supported file formats are currently "jpeg", "tiff", "png", "ico" or 
   // "bmp". See gdk_pixbuf_save_to_buffer() for more details.
   // The @cancellable can be used to abort the operation from another 
   // thread. If the operation was cancelled, the error %GIO_ERROR_CANCELLED 
   // will be returned. Other possible errors are in the #GDK_PIXBUF_ERROR 
   // and %G_IO_ERROR domains. 
   // The stream is not closed.
   // error was set.
   // RETURNS: %TRUE if the pixbuf was saved successfully, %FALSE if an
   // <stream>: a #GOutputStream to save the pixbuf to
   // <type>: name of file format
   // <cancellable>: optional #GCancellable object, %NULL to ignore
   // <error>: return location for error, or %NULL
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias gdk_pixbuf_save_to_stream save_to_stream; // Variadic
   +/

   // Unintrospectable method: save_to_stream_async() / gdk_pixbuf_save_to_stream_async()
   // VERSION: 2.24
   // Saves @pixbuf to an output stream asynchronously.
   // For more details see gdk_pixbuf_save_to_stream(), which is the synchronous
   // version of this function.
   // When the operation is finished, @callback will be called in the main thread.
   // You can then call gdk_pixbuf_save_to_stream_finish() to get the result of the operation.
   // <stream>: a #GOutputStream to which to save the pixbuf
   // <type>: name of file format
   // <cancellable>: optional #GCancellable object, %NULL to ignore
   // <callback>: a #GAsyncReadyCallback to call when the the pixbuf is loaded
   // <user_data>: the data to pass to the callback function
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias gdk_pixbuf_save_to_stream_async save_to_stream_async; // Variadic
   +/

   // Saves pixbuf to a file in @type, which is currently "jpeg", "png", "tiff", "ico" or "bmp".
   // If @error is set, %FALSE will be returned. 
   // See gdk_pixbuf_save () for more details.
   // RETURNS: whether an error was set
   // <filename>: name of file to save.
   // <type>: name of file format.
   // <option_keys>: name of options to set, %NULL-terminated
   // <option_values>: values for named options
   int savev(AT0, AT1, AT2, AT3, AT4)(AT0 /*char*/ filename, AT1 /*char*/ type, AT2 /*char**/ option_keys, AT3 /*char**/ option_values, AT4 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_savev(&this, toCString!(char*)(filename), toCString!(char*)(type), toCString!(char**)(option_keys), toCString!(char**)(option_values), UpCast!(GLib2.Error**)(error));
   }

   // Creates a transformation of the source image @src by scaling by
   // then renders the rectangle (@dest_x, @dest_y, @dest_width,
   // replacing the previous contents.
   // Try to use gdk_pixbuf_scale_simple() first, this function is
   // the industrial-strength power tool you can fall back to if
   // gdk_pixbuf_scale_simple() isn't powerful enough.
   // If the source rectangle overlaps the destination rectangle on the
   // same pixbuf, it will be overwritten during the scaling which
   // results in rendering artifacts.
   // <dest>: the #GdkPixbuf into which to render the results
   // <dest_x>: the left coordinate for region to render
   // <dest_y>: the top coordinate for region to render
   // <dest_width>: the width of the region to render
   // <dest_height>: the height of the region to render
   // <offset_x>: the offset in the X direction (currently rounded to an integer)
   // <offset_y>: the offset in the Y direction (currently rounded to an integer)
   // <scale_x>: the scale factor in the X direction
   // <scale_y>: the scale factor in the Y direction
   // <interp_type>: the interpolation type for the transformation.
   void scale(AT0)(AT0 /*Pixbuf*/ dest, int dest_x, int dest_y, int dest_width, int dest_height, double offset_x, double offset_y, double scale_x, double scale_y, InterpType interp_type) nothrow {
      gdk_pixbuf_scale(&this, UpCast!(Pixbuf*)(dest), dest_x, dest_y, dest_width, dest_height, offset_x, offset_y, scale_x, scale_y, interp_type);
   }

   // Create a new #GdkPixbuf containing a copy of @src scaled to
   // should be #GDK_INTERP_NEAREST if you want maximum speed (but when
   // scaling down #GDK_INTERP_NEAREST is usually unusably ugly).  The
   // default @interp_type should be #GDK_INTERP_BILINEAR which offers
   // reasonable quality and speed.
   // You can scale a sub-portion of @src by creating a sub-pixbuf
   // pointing into @src; see gdk_pixbuf_new_subpixbuf().
   // For more complicated scaling/compositing see gdk_pixbuf_scale()
   // and gdk_pixbuf_composite().
   // allocated for it.
   // RETURNS: the new #GdkPixbuf, or %NULL if not enough memory could be
   // <dest_width>: the width of destination image
   // <dest_height>: the height of destination image
   // <interp_type>: the interpolation type for the transformation.
   Pixbuf* /*new*/ scale_simple()(int dest_width, int dest_height, InterpType interp_type) nothrow {
      return gdk_pixbuf_scale_simple(&this, dest_width, dest_height, interp_type);
   }

   // Unintrospectable method: unref() / gdk_pixbuf_unref()
   // DEPRECATED (v2.0) method: unref - Use g_object_unref().
   // Removes a reference from a pixbuf.
   void unref()() nothrow {
      gdk_pixbuf_unref(&this);
   }
}


// These values can be passed to
// gdk_pixbuf_render_to_drawable_alpha() to control how the alpha
// channel of an image should be handled.  This function can create a
// bilevel clipping mask (black and white) and use it while painting
// the image.  In the future, when the X Window System gets an alpha
// channel extension, it will be possible to do full alpha
// compositing onto arbitrary drawables.  For now both cases fall
// back to a bilevel clipping mask.
enum PixbufAlphaMode {
   BILEVEL = 0,
   FULL = 1
}
// An opaque struct representing an animation.
struct PixbufAnimation /* : GObject.Object */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // Creates a new animation by loading it from a file.  The file format is
   // detected automatically.  If the file's format does not support multi-frame
   // images, then an animation with a single frame will be created. Possible errors
   // are in the #GDK_PIXBUF_ERROR and #G_FILE_ERROR domains.
   // there was no loader for the file's format, there was not enough memory to
   // allocate the image buffer, or the image file contained invalid data.
   // RETURNS: A newly-created animation with a reference count of 1, or %NULL
   // <filename>: Name of file to load, in the GLib file name encoding
   static PixbufAnimation* /*new*/ new_from_file(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_animation_new_from_file(toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }
   static auto opCall(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) {
      return gdk_pixbuf_animation_new_from_file(toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }

   // Queries the height of the bounding box of a pixbuf animation.
   // RETURNS: Height of the bounding box of the animation.
   int get_height()() nothrow {
      return gdk_pixbuf_animation_get_height(&this);
   }

   // Get an iterator for displaying an animation. The iterator provides
   // the frames that should be displayed at a given time.
   // It should be freed after use with g_object_unref().
   // marks the beginning of animation playback. After creating an
   // iterator, you should immediately display the pixbuf returned by
   // gdk_pixbuf_animation_iter_get_pixbuf(). Then, you should install a
   // timeout (with g_timeout_add()) or by some other mechanism ensure
   // that you'll update the image after
   // gdk_pixbuf_animation_iter_get_delay_time() milliseconds. Each time
   // the image is updated, you should reinstall the timeout with the new,
   // possibly-changed delay time.
   // As a shortcut, if @start_time is %NULL, the result of
   // g_get_current_time() will be used automatically.
   // To update the image (i.e. possibly change the result of
   // gdk_pixbuf_animation_iter_get_pixbuf() to a new frame of the animation),
   // call gdk_pixbuf_animation_iter_advance().
   // If you're using #GdkPixbufLoader, in addition to updating the image
   // after the delay time, you should also update it whenever you
   // receive the area_updated signal and
   // gdk_pixbuf_animation_iter_on_currently_loading_frame() returns
   // %TRUE. In this case, the frame currently being fed into the loader
   // has received new data, so needs to be refreshed. The delay time for
   // a frame may also be modified after an area_updated signal, for
   // example if the delay time for a frame is encoded in the data after
   // the frame itself. So your timeout should be reinstalled after any
   // area_updated signal.
   // A delay time of -1 is possible, indicating "infinite."
   // RETURNS: an iterator to move over the animation
   // <start_time>: time when the animation starts playing
   PixbufAnimationIter* /*new*/ get_iter(AT0)(AT0 /*GLib2.TimeVal*/ start_time) nothrow {
      return gdk_pixbuf_animation_get_iter(&this, UpCast!(GLib2.TimeVal*)(start_time));
   }

   // If an animation is really just a plain image (has only one frame),
   // this function returns that image. If the animation is an animation,
   // this function returns a reasonable thing to display as a static
   // unanimated image, which might be the first frame, or something more
   // sophisticated. If an animation hasn't loaded any frames yet, this
   // function will return %NULL.
   // RETURNS: unanimated image representing the animation
   Pixbuf* get_static_image()() nothrow {
      return gdk_pixbuf_animation_get_static_image(&this);
   }

   // Queries the width of the bounding box of a pixbuf animation.
   // RETURNS: Width of the bounding box of the animation.
   int get_width()() nothrow {
      return gdk_pixbuf_animation_get_width(&this);
   }

   // If you load a file with gdk_pixbuf_animation_new_from_file() and it turns
   // out to be a plain, unanimated image, then this function will return
   // %TRUE. Use gdk_pixbuf_animation_get_static_image() to retrieve
   // the image.
   // RETURNS: %TRUE if the "animation" was really just an image
   int is_static_image()() nothrow {
      return gdk_pixbuf_animation_is_static_image(&this);
   }

   // Unintrospectable method: ref() / gdk_pixbuf_animation_ref()
   // DEPRECATED (v2.0) method: ref - Use g_object_ref().
   // Adds a reference to an animation.
   // RETURNS: The same as the @animation argument.
   PixbufAnimation* ref_()() nothrow {
      return gdk_pixbuf_animation_ref(&this);
   }

   // Unintrospectable method: unref() / gdk_pixbuf_animation_unref()
   // DEPRECATED (v2.0) method: unref - Use g_object_unref().
   // Removes a reference from an animation.
   void unref()() nothrow {
      gdk_pixbuf_animation_unref(&this);
   }
}


// An opaque struct representing an iterator which points to a
// certain position in an animation.
struct PixbufAnimationIter /* : GObject.Object */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // Possibly advances an animation to a new frame. Chooses the frame based
   // on the start time passed to gdk_pixbuf_animation_get_iter().
   // must be greater than or equal to the time passed to
   // gdk_pixbuf_animation_get_iter(), and must increase or remain
   // unchanged each time gdk_pixbuf_animation_iter_get_pixbuf() is
   // called. That is, you can't go backward in time; animations only
   // play forward.
   // As a shortcut, pass %NULL for the current time and g_get_current_time()
   // will be invoked on your behalf. So you only need to explicitly pass
   // at double speed.
   // If this function returns %FALSE, there's no need to update the animation
   // display, assuming the display had been rendered prior to advancing;
   // if %TRUE, you need to call gdk_animation_iter_get_pixbuf() and update the
   // display with the new pixbuf.
   // RETURNS: %TRUE if the image may need updating
   // <current_time>: current time
   int advance(AT0)(AT0 /*GLib2.TimeVal*/ current_time) nothrow {
      return gdk_pixbuf_animation_iter_advance(&this, UpCast!(GLib2.TimeVal*)(current_time));
   }

   // Gets the number of milliseconds the current pixbuf should be displayed,
   // or -1 if the current pixbuf should be displayed forever. g_timeout_add()
   // conveniently takes a timeout in milliseconds, so you can use a timeout
   // to schedule the next update.
   // RETURNS: delay time in milliseconds (thousandths of a second)
   int get_delay_time()() nothrow {
      return gdk_pixbuf_animation_iter_get_delay_time(&this);
   }

   // Gets the current pixbuf which should be displayed; the pixbuf will
   // be the same size as the animation itself
   // (gdk_pixbuf_animation_get_width(), gdk_pixbuf_animation_get_height()). 
   // This pixbuf should be displayed for 
   // gdk_pixbuf_animation_iter_get_delay_time() milliseconds.  The caller
   // of this function does not own a reference to the returned pixbuf;
   // the returned pixbuf will become invalid when the iterator advances
   // to the next frame, which may happen anytime you call
   // gdk_pixbuf_animation_iter_advance(). Copy the pixbuf to keep it
   // (don't just add a reference), as it may get recycled as you advance
   // the iterator.
   // RETURNS: the pixbuf to be displayed
   Pixbuf* get_pixbuf()() nothrow {
      return gdk_pixbuf_animation_iter_get_pixbuf(&this);
   }

   // Used to determine how to respond to the area_updated signal on
   // #GdkPixbufLoader when loading an animation. area_updated is emitted
   // for an area of the frame currently streaming in to the loader. So if
   // you're on the currently loading frame, you need to redraw the screen for
   // the updated area.
   // RETURNS: %TRUE if the frame we're on is partially loaded, or the last frame
   int on_currently_loading_frame()() nothrow {
      return gdk_pixbuf_animation_iter_on_currently_loading_frame(&this);
   }
}


// A function of this type is responsible for freeing the pixel array
// of a pixbuf.  The gdk_pixbuf_new_from_data() function lets you
// pass in a pre-allocated pixel array so that a pixbuf can be
// created from it; in this case you will need to pass in a function
// of #GdkPixbufDestroyNotify so that the pixel data can be freed
// when the pixbuf is finalized.
// <pixels>: The pixel array of the pixbuf that is being finalized.
// <data>: User closure data.
extern (C) alias void function (ubyte* pixels, void* data) nothrow PixbufDestroyNotify;


// An error code in the #GDK_PIXBUF_ERROR domain. Many &gdk-pixbuf;
// operations can cause errors in this domain, or in the #G_FILE_ERROR
// domain.
enum PixbufError {
   CORRUPT_IMAGE = 0,
   INSUFFICIENT_MEMORY = 1,
   BAD_OPTION = 2,
   UNKNOWN_TYPE = 3,
   UNSUPPORTED_OPERATION = 4,
   FAILED = 5
}
struct PixbufFormat {

   // VERSION: 2.22
   // Creates a copy of @format
   // gdk_pixbuf_format_free() to free the resources when done
   // RETURNS: the newly allocated copy of a #GdkPixbufFormat. Use
   PixbufFormat* /*new*/ copy()() nothrow {
      return gdk_pixbuf_format_copy(&this);
   }

   // VERSION: 2.22
   // Frees the resources allocated when copying a #GdkPixbufFormat
   // using gdk_pixbuf_format_copy()
   void free()() nothrow {
      gdk_pixbuf_format_free(&this);
   }

   // VERSION: 2.2
   // Returns a description of the format.
   // RETURNS: a description of the format.
   char* /*new*/ get_description()() nothrow {
      return gdk_pixbuf_format_get_description(&this);
   }

   // VERSION: 2.2
   // Returns the filename extensions typically used for files in the 
   // given format.
   // freed with g_strfreev() when it is no longer needed.
   // RETURNS: a %NULL-terminated array of filename extensions which must be
   char** /*new*/ get_extensions()() nothrow {
      return gdk_pixbuf_format_get_extensions(&this);
   }

   // VERSION: 2.6
   // Returns information about the license of the image loader for the format. The
   // returned string should be a shorthand for a wellknown license, e.g. "LGPL",
   // "GPL", "QPL", "GPL/QPL", or "other" to indicate some other license.  This
   // string should be freed with g_free() when it's no longer needed.
   // RETURNS: a string describing the license of @format.
   char* /*new*/ get_license()() nothrow {
      return gdk_pixbuf_format_get_license(&this);
   }

   // VERSION: 2.2
   // Returns the mime types supported by the format.
   // g_strfreev() when it is no longer needed.
   // RETURNS: a %NULL-terminated array of mime types which must be freed with
   char** /*new*/ get_mime_types()() nothrow {
      return gdk_pixbuf_format_get_mime_types(&this);
   }

   // VERSION: 2.2
   // Returns the name of the format.
   // RETURNS: the name of the format.
   char* /*new*/ get_name()() nothrow {
      return gdk_pixbuf_format_get_name(&this);
   }

   // VERSION: 2.6
   // Returns whether this image format is disabled. See
   // gdk_pixbuf_format_set_disabled().
   // RETURNS: whether this image format is disabled.
   int is_disabled()() nothrow {
      return gdk_pixbuf_format_is_disabled(&this);
   }

   // VERSION: 2.6
   // Returns whether this image format is scalable. If a file is in a 
   // scalable format, it is preferable to load it at the desired size, 
   // rather than loading it at the default size and scaling the 
   // resulting pixbuf to the desired size.
   // RETURNS: whether this image format is scalable.
   int is_scalable()() nothrow {
      return gdk_pixbuf_format_is_scalable(&this);
   }

   // VERSION: 2.2
   // Returns whether pixbufs can be saved in the given format.
   // RETURNS: whether pixbufs can be saved in the given format.
   int is_writable()() nothrow {
      return gdk_pixbuf_format_is_writable(&this);
   }

   // VERSION: 2.6
   // Disables or enables an image format. If a format is disabled, 
   // gdk-pixbuf won't use the image loader for this format to load 
   // images. Applications can use this to avoid using image loaders 
   // with an inappropriate license, see gdk_pixbuf_format_get_license().
   // <disabled>: %TRUE to disable the format @format
   void set_disabled()(int disabled) nothrow {
      gdk_pixbuf_format_set_disabled(&this, disabled);
   }
}


// The <structname>GdkPixbufLoader</structname> struct contains only private
// fields.
struct PixbufLoader /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private void* priv;


   // Creates a new pixbuf loader object.
   // RETURNS: A newly-created pixbuf loader.
   static PixbufLoader* /*new*/ new_()() nothrow {
      return gdk_pixbuf_loader_new();
   }
   static auto opCall()() {
      return gdk_pixbuf_loader_new();
   }

   // VERSION: 2.4
   // Creates a new pixbuf loader object that always attempts to parse
   // image data as if it were an image of mime type @mime_type, instead of
   // identifying the type automatically. Useful if you want an error if
   // the image isn't the expected mime type, for loading image formats
   // that can't be reliably identified by looking at the data, or if
   // the user manually forces a specific mime type.
   // The list of supported mime types depends on what image loaders
   // are installed, but typically "image/png", "image/jpeg", "image/gif", 
   // "image/tiff" and "image/x-xpixmap" are among the supported mime types. 
   // To obtain the full list of supported mime types, call 
   // gdk_pixbuf_format_get_mime_types() on each of the #GdkPixbufFormat 
   // structs returned by gdk_pixbuf_get_formats().
   // RETURNS: A newly-created pixbuf loader.
   // <mime_type>: the mime type to be loaded
   static PixbufLoader* /*new*/ new_with_mime_type(AT0, AT1)(AT0 /*char*/ mime_type, AT1 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_loader_new_with_mime_type(toCString!(char*)(mime_type), UpCast!(GLib2.Error**)(error));
   }
   static auto opCall(AT0, AT1)(AT0 /*char*/ mime_type, AT1 /*GLib2.Error**/ error=null) {
      return gdk_pixbuf_loader_new_with_mime_type(toCString!(char*)(mime_type), UpCast!(GLib2.Error**)(error));
   }

   // Creates a new pixbuf loader object that always attempts to parse
   // image data as if it were an image of type @image_type, instead of
   // identifying the type automatically. Useful if you want an error if
   // the image isn't the expected type, for loading image formats
   // that can't be reliably identified by looking at the data, or if
   // the user manually forces a specific type.
   // The list of supported image formats depends on what image loaders
   // are installed, but typically "png", "jpeg", "gif", "tiff" and 
   // "xpm" are among the supported formats. To obtain the full list of
   // supported image formats, call gdk_pixbuf_format_get_name() on each 
   // of the #GdkPixbufFormat structs returned by gdk_pixbuf_get_formats().
   // RETURNS: A newly-created pixbuf loader.
   // <image_type>: name of the image format to be loaded with the image
   static PixbufLoader* /*new*/ new_with_type(AT0, AT1)(AT0 /*char*/ image_type, AT1 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_loader_new_with_type(toCString!(char*)(image_type), UpCast!(GLib2.Error**)(error));
   }
   static auto opCall(AT0, AT1)(AT0 /*char*/ image_type, AT1 /*GLib2.Error**/ error=null) {
      return gdk_pixbuf_loader_new_with_type(toCString!(char*)(image_type), UpCast!(GLib2.Error**)(error));
   }

   // Informs a pixbuf loader that no further writes with
   // gdk_pixbuf_loader_write() will occur, so that it can free its
   // internal loading structures. Also, tries to parse any data that
   // hasn't yet been parsed; if the remaining data is partial or
   // corrupt, an error will be returned.  If %FALSE is returned, @error
   // will be set to an error from the #GDK_PIXBUF_ERROR or #G_FILE_ERROR
   // domains. If you're just cancelling a load rather than expecting it
   // to be finished, passing %NULL for @error to ignore it is
   // reasonable.
   // Remember that this does not unref the loader, so if you plan not to
   // use it anymore, please g_object_unref() it.
   // RETURNS: %TRUE if all image data written so far was successfully
   int close(AT0)(AT0 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_loader_close(&this, UpCast!(GLib2.Error**)(error));
   }

   // Queries the #GdkPixbufAnimation that a pixbuf loader is currently creating.
   // In general it only makes sense to call this function after the "area-prepared"
   // signal has been emitted by the loader. If the loader doesn't have enough
   // bytes yet (hasn't emitted the "area-prepared" signal) this function will 
   // return %NULL.
   // RETURNS: The #GdkPixbufAnimation that the loader is loading, or %NULL if
   PixbufAnimation* get_animation()() nothrow {
      return gdk_pixbuf_loader_get_animation(&this);
   }

   // VERSION: 2.2
   // Obtains the available information about the format of the 
   // currently loading image file.
   // by GdkPixbuf and should not be freed.
   // RETURNS: A #GdkPixbufFormat or %NULL. The return value is owned
   PixbufFormat* /*new*/ get_format()() nothrow {
      return gdk_pixbuf_loader_get_format(&this);
   }

   // Queries the #GdkPixbuf that a pixbuf loader is currently creating.
   // In general it only makes sense to call this function after the
   // "area-prepared" signal has been emitted by the loader; this means
   // that enough data has been read to know the size of the image that
   // will be allocated.  If the loader has not received enough data via
   // gdk_pixbuf_loader_write(), then this function returns %NULL.  The
   // returned pixbuf will be the same in all future calls to the loader,
   // so simply calling g_object_ref() should be sufficient to continue
   // using it.  Additionally, if the loader is an animation, it will
   // return the "static image" of the animation
   // (see gdk_pixbuf_animation_get_static_image()).
   // enough data has been read to determine how to create the image buffer.
   // RETURNS: The #GdkPixbuf that the loader is creating, or %NULL if not
   Pixbuf* get_pixbuf()() nothrow {
      return gdk_pixbuf_loader_get_pixbuf(&this);
   }

   // VERSION: 2.2
   // Causes the image to be scaled while it is loaded. The desired
   // image size can be determined relative to the original size of
   // the image by calling gdk_pixbuf_loader_set_size() from a
   // signal handler for the ::size-prepared signal.
   // Attempts to set the desired image size  are ignored after the 
   // emission of the ::size-prepared signal.
   // <width>: The desired width of the image being loaded.
   // <height>: The desired height of the image being loaded.
   void set_size()(int width, int height) nothrow {
      gdk_pixbuf_loader_set_size(&this, width, height);
   }

   // This will cause a pixbuf loader to parse the next @count bytes of
   // an image.  It will return %TRUE if the data was loaded successfully,
   // and %FALSE if an error occurred.  In the latter case, the loader
   // will be closed, and will not accept further writes. If %FALSE is
   // returned, @error will be set to an error from the #GDK_PIXBUF_ERROR
   // or #G_FILE_ERROR domains.
   // cannot parse the buffer.
   // RETURNS: %TRUE if the write was successful, or %FALSE if the loader
   // <buf>: Pointer to image data.
   // <count>: Length of the @buf buffer in bytes.
   int write(AT0, AT1)(AT0 /*ubyte*/ buf, size_t count, AT1 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixbuf_loader_write(&this, UpCast!(ubyte*)(buf), count, UpCast!(GLib2.Error**)(error));
   }

   // This signal is emitted when the pixbuf loader has allocated the 
   // pixbuf in the desired size.  After this signal is emitted, 
   // applications can call gdk_pixbuf_loader_get_pixbuf() to fetch 
   // the partially-loaded pixbuf.
   extern (C) alias static void function (PixbufLoader* this_, void* user_data=null) nothrow signal_area_prepared;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"area-prepared", CB/*:signal_area_prepared*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_area_prepared)||_ttmm!(CB, signal_area_prepared)()) {
      return signal_connect_data!()(&this, cast(char*)"area-prepared",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // This signal is emitted when a significant area of the image being
   // loaded has been updated.  Normally it means that a complete
   // scanline has been read in, but it could be a different area as
   // well.  Applications can use this signal to know when to repaint
   // areas of an image that is being loaded.
   // <x>: X offset of upper-left corner of the updated area.
   // <y>: Y offset of upper-left corner of the updated area.
   // <width>: Width of updated area.
   // <height>: Height of updated area.
   extern (C) alias static void function (PixbufLoader* this_, int x, int y, int width, int height, void* user_data=null) nothrow signal_area_updated;
   ulong signal_connect(string name:"area-updated", CB/*:signal_area_updated*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_area_updated)||_ttmm!(CB, signal_area_updated)()) {
      return signal_connect_data!()(&this, cast(char*)"area-updated",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // This signal is emitted when gdk_pixbuf_loader_close() is called.
   // It can be used by different parts of an application to receive
   // notification when an image loader is closed by the code that
   // drives it.
   extern (C) alias static void function (PixbufLoader* this_, void* user_data=null) nothrow signal_closed;
   ulong signal_connect(string name:"closed", CB/*:signal_closed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_closed)||_ttmm!(CB, signal_closed)()) {
      return signal_connect_data!()(&this, cast(char*)"closed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // This signal is emitted when the pixbuf loader has been fed the
   // initial amount of data that is required to figure out the size
   // of the image that it will create.  Applications can call  
   // gdk_pixbuf_loader_set_size() in response to this signal to set
   // the desired size to which the image should be scaled.
   // <width>: the original width of the image
   // <height>: the original height of the image
   extern (C) alias static void function (PixbufLoader* this_, int width, int height, void* user_data=null) nothrow signal_size_prepared;
   ulong signal_connect(string name:"size-prepared", CB/*:signal_size_prepared*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_size_prepared)||_ttmm!(CB, signal_size_prepared)()) {
      return signal_connect_data!()(&this, cast(char*)"size-prepared",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct PixbufLoaderClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function (PixbufLoader* loader, int width, int height) nothrow size_prepared;
   extern (C) void function (PixbufLoader* loader) nothrow area_prepared;
   extern (C) void function (PixbufLoader* loader, int x, int y, int width, int height) nothrow area_updated;
   extern (C) void function (PixbufLoader* loader) nothrow closed;
}


// The possible rotations which can be passed to gdk_pixbuf_rotate_simple().
// To make them easier to use, their numerical values are the actual degrees.
enum PixbufRotation {
   NONE = 0,
   COUNTERCLOCKWISE = 90,
   UPSIDEDOWN = 180,
   CLOCKWISE = 270
}

// VERSION: 2.4
// Specifies the type of the function passed to
// gdk_pixbuf_save_to_callback().  It is called once for each block of
// bytes that is "written" by gdk_pixbuf_save_to_callback().  If
// successful it should return %TRUE.  If an error occurs it should set
// will fail with the same error.
// RETURNS: %TRUE if successful, %FALSE (with @error set) if failed.
// <buf>: bytes to be written.
// <count>: number of bytes in @buf.
// <error>: A location to return an error.
// <data>: user data passed to gdk_pixbuf_save_to_callback().
extern (C) alias int function (char* buf, size_t count, /*out*/ GLib2.Error** error, void* data) nothrow PixbufSaveFunc;

// An opaque struct representing a simple animation.
struct PixbufSimpleAnim /* : PixbufAnimation */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent pixbufanimation;
   PixbufAnimation method_parent;


   // VERSION: 2.8
   // Creates a new, empty animation.
   // RETURNS: a newly allocated #GdkPixbufSimpleAnim
   // <width>: the width of the animation
   // <height>: the height of the animation
   // <rate>: the speed of the animation, in frames per second
   static PixbufSimpleAnim* /*new*/ new_()(int width, int height, float rate) nothrow {
      return gdk_pixbuf_simple_anim_new(width, height, rate);
   }
   static auto opCall()(int width, int height, float rate) {
      return gdk_pixbuf_simple_anim_new(width, height, rate);
   }

   // VERSION: 2.8
   // Adds a new frame to @animation. The @pixbuf must
   // have the dimensions specified when the animation 
   // was constructed.
   // <pixbuf>: the pixbuf to add
   void add_frame(AT0)(AT0 /*Pixbuf*/ pixbuf) nothrow {
      gdk_pixbuf_simple_anim_add_frame(&this, UpCast!(Pixbuf*)(pixbuf));
   }

   // VERSION: 2.18
   // Gets whether @animation should loop indefinitely when it reaches the end.
   // RETURNS: %TRUE if the animation loops forever, %FALSE otherwise
   int get_loop()() nothrow {
      return gdk_pixbuf_simple_anim_get_loop(&this);
   }

   // VERSION: 2.18
   // Sets whether @animation should loop indefinitely when it reaches the end.
   // <loop>: whether to loop the animation
   void set_loop()(int loop) nothrow {
      gdk_pixbuf_simple_anim_set_loop(&this, loop);
   }
}

struct PixbufSimpleAnimClass {
}

struct PixbufSimpleAnimIter /* : PixbufAnimationIter */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent pixbufanimationiter;
   PixbufAnimationIter method_parent;
}


// A #GdkPixdata contains pixbuf information in a form suitable for 
// serialization and streaming.
struct Pixdata {
   uint magic;
   int length;
   uint pixdata_type, rowstride, width, height;
   ubyte* pixel_data;


   // Deserializes (reconstruct) a #GdkPixdata structure from a byte stream.
   // The byte stream consists of a straightforward writeout of the
   // #GdkPixdata fields in network byte order, plus the @pixel_data
   // bytes the structure points to.
   // The @pixdata contents are reconstructed byte by byte and are checked
   // for validity. This function may fail with %GDK_PIXBUF_CORRUPT_IMAGE
   // or %GDK_PIXBUF_ERROR_UNKNOWN_TYPE.
   // %FALSE otherwise.
   // RETURNS: Upon successful deserialization %TRUE is returned,
   // <stream_length>: length of the stream used for deserialization.
   // <stream>: stream of bytes containing a serialized #GdkPixdata structure.
   int deserialize(AT0, AT1)(uint stream_length, AT0 /*ubyte*/ stream, AT1 /*GLib2.Error**/ error=null) nothrow {
      return gdk_pixdata_deserialize(&this, stream_length, UpCast!(ubyte*)(stream), UpCast!(GLib2.Error**)(error));
   }

   // Unintrospectable method: from_pixbuf() / gdk_pixdata_from_pixbuf()
   // Converts a #GdkPixbuf to a #GdkPixdata. If @use_rle is %TRUE, the
   // pixel data is run-length encoded into newly-allocated memory and a 
   // pointer to that memory is returned. 
   // for the run-length encoded pixel data, otherwise %NULL.
   // RETURNS: If @ure_rle is %TRUE, a pointer to the newly-allocated memory
   // <pixbuf>: the data to fill @pixdata with.
   // <use_rle>: whether to use run-length encoding for the pixel data.
   void* from_pixbuf(AT0)(AT0 /*Pixbuf*/ pixbuf, int use_rle) nothrow {
      return gdk_pixdata_from_pixbuf(&this, UpCast!(Pixbuf*)(pixbuf), use_rle);
   }

   // Serializes a #GdkPixdata structure into a byte stream.
   // The byte stream consists of a straightforward writeout of the
   // #GdkPixdata fields in network byte order, plus the @pixel_data
   // bytes the structure points to.
   // #GdkPixdata structure.
   // RETURNS: A newly-allocated string containing the serialized
   // <stream_length_p>: location to store the resulting stream length in.
   ubyte* serialize(AT0)(AT0 /*uint*/ stream_length_p) nothrow {
      return gdk_pixdata_serialize(&this, UpCast!(uint*)(stream_length_p));
   }

   // Unintrospectable method: to_csource() / gdk_pixdata_to_csource()
   // Generates C source code suitable for compiling images directly 
   // into programs. 
   // gdk-pixbuf ships with a program called <command>gdk-pixbuf-csource</command> 
   // which offers a command line interface to this function.
   // of @pixdata.
   // RETURNS: a newly-allocated string containing the C source form
   // <name>: used for naming generated data structures or macros.
   // <dump_type>: a #GdkPixdataDumpType determining the kind of C source to be generated.
   GLib2.String* to_csource(AT0)(AT0 /*char*/ name, PixdataDumpType dump_type) nothrow {
      return gdk_pixdata_to_csource(&this, toCString!(char*)(name), dump_type);
   }
}


// An enumeration which is used by gdk_pixdata_to_csource() to
// determine the form of C source to be generated. The three values
// and @GDK_PIXDATA_DUMP_MACROS are mutually exclusive, as are
// elements are optional flags that can be freely added.
enum PixdataDumpType {
   PIXDATA_STREAM = 0,
   PIXDATA_STRUCT = 1,
   MACROS = 2,
   GTYPES = 0,
   CTYPES = 256,
   STATIC = 512,
   CONST = 1024,
   RLE_DECODER = 65536
}

// one for the used colorspace, one for the width of the samples and one 
// for the encoding of the pixel data.
enum PixdataType {
   COLOR_TYPE_RGB = 1,
   COLOR_TYPE_RGBA = 2,
   COLOR_TYPE_MASK = 255,
   SAMPLE_WIDTH_8 = 65536,
   SAMPLE_WIDTH_MASK = 983040,
   ENCODING_RAW = 16777216,
   ENCODING_RLE = 33554432,
   ENCODING_MASK = 251658240
}
static GLib2.Quark pixbuf_error_quark()() nothrow {
   return gdk_pixbuf_error_quark();
}


// C prototypes:

extern (C) {
Pixbuf* /*new*/ gdk_pixbuf_new(Colorspace colorspace, int has_alpha, int bits_per_sample, int width, int height) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_new_from_data(ubyte* data, Colorspace colorspace, int has_alpha, int bits_per_sample, int width, int height, int rowstride, PixbufDestroyNotify destroy_fn, void* destroy_fn_data) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_new_from_file(char* filename, GLib2.Error** error) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_new_from_file_at_scale(char* filename, int width, int height, int preserve_aspect_ratio, GLib2.Error** error) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_new_from_file_at_size(char* filename, int width, int height, GLib2.Error** error) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_new_from_inline(int data_length, ubyte* data, int copy_pixels, GLib2.Error** error) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_new_from_stream(Gio2.InputStream* stream, Gio2.Cancellable* cancellable, GLib2.Error** error) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_new_from_stream_at_scale(Gio2.InputStream* stream, int width, int height, int preserve_aspect_ratio, Gio2.Cancellable* cancellable, GLib2.Error** error) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_new_from_stream_finish(Gio2.AsyncResult* async_result, GLib2.Error** error) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_new_from_xpm_data(char** data) nothrow;
Pixbuf* gdk_pixbuf_from_pixdata(Pixdata* pixdata, int copy_pixels, GLib2.Error** error) nothrow;
PixbufFormat* /*new*/ gdk_pixbuf_get_file_info(char* filename, int* width, int* height) nothrow;
GLib2.SList* /*new container*/ gdk_pixbuf_get_formats() nothrow;
char* gdk_pixbuf_gettext(char* msgid) nothrow;
void gdk_pixbuf_new_from_stream_async(Gio2.InputStream* stream, Gio2.Cancellable* cancellable, Gio2.AsyncReadyCallback callback, void* user_data) nothrow;
void gdk_pixbuf_new_from_stream_at_scale_async(Gio2.InputStream* stream, int width, int height, int preserve_aspect_ratio, Gio2.Cancellable* cancellable, Gio2.AsyncReadyCallback callback, void* user_data) nothrow;
int gdk_pixbuf_save_to_stream_finish(Gio2.AsyncResult* async_result, GLib2.Error** error) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_add_alpha(Pixbuf* this_, int substitute_color, ubyte r, ubyte g, ubyte b) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_apply_embedded_orientation(Pixbuf* this_) nothrow;
void gdk_pixbuf_composite(Pixbuf* this_, Pixbuf* dest, int dest_x, int dest_y, int dest_width, int dest_height, double offset_x, double offset_y, double scale_x, double scale_y, InterpType interp_type, int overall_alpha) nothrow;
void gdk_pixbuf_composite_color(Pixbuf* this_, Pixbuf* dest, int dest_x, int dest_y, int dest_width, int dest_height, double offset_x, double offset_y, double scale_x, double scale_y, InterpType interp_type, int overall_alpha, int check_x, int check_y, int check_size, uint color1, uint color2) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_composite_color_simple(Pixbuf* this_, int dest_width, int dest_height, InterpType interp_type, int overall_alpha, int check_size, uint color1, uint color2) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_copy(Pixbuf* this_) nothrow;
void gdk_pixbuf_copy_area(Pixbuf* this_, int src_x, int src_y, int width, int height, Pixbuf* dest_pixbuf, int dest_x, int dest_y) nothrow;
void gdk_pixbuf_fill(Pixbuf* this_, uint pixel) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_flip(Pixbuf* this_, int horizontal) nothrow;
int gdk_pixbuf_get_bits_per_sample(Pixbuf* this_) nothrow;
Colorspace gdk_pixbuf_get_colorspace(Pixbuf* this_) nothrow;
int gdk_pixbuf_get_has_alpha(Pixbuf* this_) nothrow;
int gdk_pixbuf_get_height(Pixbuf* this_) nothrow;
int gdk_pixbuf_get_n_channels(Pixbuf* this_) nothrow;
char* gdk_pixbuf_get_option(Pixbuf* this_, char* key) nothrow;
ubyte* gdk_pixbuf_get_pixels(Pixbuf* this_) nothrow;
int gdk_pixbuf_get_rowstride(Pixbuf* this_) nothrow;
int gdk_pixbuf_get_width(Pixbuf* this_) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_new_subpixbuf(Pixbuf* this_, int src_x, int src_y, int width, int height) nothrow;
Pixbuf* gdk_pixbuf_ref(Pixbuf* this_) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_rotate_simple(Pixbuf* this_, PixbufRotation angle) nothrow;
void gdk_pixbuf_saturate_and_pixelate(Pixbuf* this_, Pixbuf* dest, float saturation, int pixelate) nothrow;
int gdk_pixbuf_save(Pixbuf* this_, char* filename, char* type, GLib2.Error** error=null, ...) nothrow;
int gdk_pixbuf_save_to_buffer(Pixbuf* this_, char** buffer, size_t* buffer_size, char* type, GLib2.Error** error=null, ...) nothrow;
int gdk_pixbuf_save_to_bufferv(Pixbuf* this_, char** buffer, size_t* buffer_size, char* type, char** option_keys, char** option_values, GLib2.Error** error) nothrow;
int gdk_pixbuf_save_to_callback(Pixbuf* this_, PixbufSaveFunc save_func, void* user_data, char* type, GLib2.Error** error=null, ...) nothrow;
int gdk_pixbuf_save_to_callbackv(Pixbuf* this_, PixbufSaveFunc save_func, void* user_data, char* type, char** option_keys, char** option_values, GLib2.Error** error) nothrow;
int gdk_pixbuf_save_to_stream(Pixbuf* this_, Gio2.OutputStream* stream, char* type, Gio2.Cancellable* cancellable=null, GLib2.Error** error=null, ...) nothrow;
void gdk_pixbuf_save_to_stream_async(Pixbuf* this_, Gio2.OutputStream* stream, char* type, Gio2.Cancellable* cancellable, Gio2.AsyncReadyCallback callback, void* user_data, ...) nothrow;
int gdk_pixbuf_savev(Pixbuf* this_, char* filename, char* type, char** option_keys, char** option_values, GLib2.Error** error) nothrow;
void gdk_pixbuf_scale(Pixbuf* this_, Pixbuf* dest, int dest_x, int dest_y, int dest_width, int dest_height, double offset_x, double offset_y, double scale_x, double scale_y, InterpType interp_type) nothrow;
Pixbuf* /*new*/ gdk_pixbuf_scale_simple(Pixbuf* this_, int dest_width, int dest_height, InterpType interp_type) nothrow;
void gdk_pixbuf_unref(Pixbuf* this_) nothrow;
PixbufAnimation* /*new*/ gdk_pixbuf_animation_new_from_file(char* filename, GLib2.Error** error) nothrow;
int gdk_pixbuf_animation_get_height(PixbufAnimation* this_) nothrow;
PixbufAnimationIter* /*new*/ gdk_pixbuf_animation_get_iter(PixbufAnimation* this_, GLib2.TimeVal* start_time) nothrow;
Pixbuf* gdk_pixbuf_animation_get_static_image(PixbufAnimation* this_) nothrow;
int gdk_pixbuf_animation_get_width(PixbufAnimation* this_) nothrow;
int gdk_pixbuf_animation_is_static_image(PixbufAnimation* this_) nothrow;
PixbufAnimation* gdk_pixbuf_animation_ref(PixbufAnimation* this_) nothrow;
void gdk_pixbuf_animation_unref(PixbufAnimation* this_) nothrow;
int gdk_pixbuf_animation_iter_advance(PixbufAnimationIter* this_, GLib2.TimeVal* current_time) nothrow;
int gdk_pixbuf_animation_iter_get_delay_time(PixbufAnimationIter* this_) nothrow;
Pixbuf* gdk_pixbuf_animation_iter_get_pixbuf(PixbufAnimationIter* this_) nothrow;
int gdk_pixbuf_animation_iter_on_currently_loading_frame(PixbufAnimationIter* this_) nothrow;
PixbufFormat* /*new*/ gdk_pixbuf_format_copy(PixbufFormat* this_) nothrow;
void gdk_pixbuf_format_free(PixbufFormat* this_) nothrow;
char* /*new*/ gdk_pixbuf_format_get_description(PixbufFormat* this_) nothrow;
char** /*new*/ gdk_pixbuf_format_get_extensions(PixbufFormat* this_) nothrow;
char* /*new*/ gdk_pixbuf_format_get_license(PixbufFormat* this_) nothrow;
char** /*new*/ gdk_pixbuf_format_get_mime_types(PixbufFormat* this_) nothrow;
char* /*new*/ gdk_pixbuf_format_get_name(PixbufFormat* this_) nothrow;
int gdk_pixbuf_format_is_disabled(PixbufFormat* this_) nothrow;
int gdk_pixbuf_format_is_scalable(PixbufFormat* this_) nothrow;
int gdk_pixbuf_format_is_writable(PixbufFormat* this_) nothrow;
void gdk_pixbuf_format_set_disabled(PixbufFormat* this_, int disabled) nothrow;
PixbufLoader* /*new*/ gdk_pixbuf_loader_new() nothrow;
PixbufLoader* /*new*/ gdk_pixbuf_loader_new_with_mime_type(char* mime_type, GLib2.Error** error) nothrow;
PixbufLoader* /*new*/ gdk_pixbuf_loader_new_with_type(char* image_type, GLib2.Error** error) nothrow;
int gdk_pixbuf_loader_close(PixbufLoader* this_, GLib2.Error** error) nothrow;
PixbufAnimation* gdk_pixbuf_loader_get_animation(PixbufLoader* this_) nothrow;
PixbufFormat* /*new*/ gdk_pixbuf_loader_get_format(PixbufLoader* this_) nothrow;
Pixbuf* gdk_pixbuf_loader_get_pixbuf(PixbufLoader* this_) nothrow;
void gdk_pixbuf_loader_set_size(PixbufLoader* this_, int width, int height) nothrow;
int gdk_pixbuf_loader_write(PixbufLoader* this_, ubyte* buf, size_t count, GLib2.Error** error) nothrow;
PixbufSimpleAnim* /*new*/ gdk_pixbuf_simple_anim_new(int width, int height, float rate) nothrow;
void gdk_pixbuf_simple_anim_add_frame(PixbufSimpleAnim* this_, Pixbuf* pixbuf) nothrow;
int gdk_pixbuf_simple_anim_get_loop(PixbufSimpleAnim* this_) nothrow;
void gdk_pixbuf_simple_anim_set_loop(PixbufSimpleAnim* this_, int loop) nothrow;
int gdk_pixdata_deserialize(Pixdata* this_, uint stream_length, ubyte* stream, GLib2.Error** error) nothrow;
void* gdk_pixdata_from_pixbuf(Pixdata* this_, Pixbuf* pixbuf, int use_rle) nothrow;
ubyte* gdk_pixdata_serialize(Pixdata* this_, uint* stream_length_p) nothrow;
GLib2.String* gdk_pixdata_to_csource(Pixdata* this_, char* name, PixdataDumpType dump_type) nothrow;
GLib2.Quark gdk_pixbuf_error_quark() nothrow;
}

// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/GL-1.0.gir"

module GL;

// c:symbol-prefixes: ["gl"]
// c:identifier-prefixes: ["GL"]

// module GL;

struct bitfield {
}

struct charARB {
}

struct clampf {
}

struct boolean_ {
}

struct enum_ {
}

struct float_ {
}

struct handleARB {
}

struct int_ {
}

struct intptr {
}

struct sizei {
}

struct sizeiptr {
}

struct uint_ {
}

struct void_ {
}

static void InitNames()() nothrow {
   glInitNames();
}


// C prototypes:

extern (C) {
void glInitNames() nothrow;
}

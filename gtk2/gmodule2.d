// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/GModule-2.0.gir"

module GModule2;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;

// package: "gmodule-2.0";
// C header: "gmodule.h";

// c:symbol-prefixes: ["g"]
// c:identifier-prefixes: ["G"]

// module GModule2;

struct Module {
   int close()() nothrow {
      return g_module_close(&this);
   }
   void make_resident()() nothrow {
      g_module_make_resident(&this);
   }
   char* name()() nothrow {
      return g_module_name(&this);
   }
   int symbol(AT0, AT1)(AT0 /*char*/ symbol_name, AT1 /*void**/ symbol) nothrow {
      return g_module_symbol(&this, toCString!(char*)(symbol_name), UpCast!(void**)(symbol));
   }
   static char* /*new*/ build_path(AT0, AT1)(AT0 /*char*/ directory, AT1 /*char*/ module_name) nothrow {
      return g_module_build_path(toCString!(char*)(directory), toCString!(char*)(module_name));
   }
   static char* error()() nothrow {
      return g_module_error();
   }
   // Unintrospectable function: open() / g_module_open()
   static Module* open(AT0)(AT0 /*char*/ file_name, ModuleFlags flags) nothrow {
      return g_module_open(toCString!(char*)(file_name), flags);
   }
   static int supported()() nothrow {
      return g_module_supported();
   }
}

extern (C) alias char* function (Module* module_) nothrow ModuleCheckInit;

enum ModuleFlags {
   LAZY = 1,
   LOCAL = 2,
   MASK = 3
}
extern (C) alias void function (Module* module_) nothrow ModuleUnload;

// MOVED TO: Module.build_path
static char* /*new*/ module_build_path(AT0, AT1)(AT0 /*char*/ directory, AT1 /*char*/ module_name) nothrow {
   return g_module_build_path(toCString!(char*)(directory), toCString!(char*)(module_name));
}

// MOVED TO: Module.error
static char* module_error()() nothrow {
   return g_module_error();
}

// MOVED TO: Module.supported
static int module_supported()() nothrow {
   return g_module_supported();
}


// C prototypes:

extern (C) {
int g_module_close(Module* this_) nothrow;
void g_module_make_resident(Module* this_) nothrow;
char* g_module_name(Module* this_) nothrow;
int g_module_symbol(Module* this_, char* symbol_name, void** symbol) nothrow;
char* /*new*/ g_module_build_path(char* directory, char* module_name) nothrow;
char* g_module_error() nothrow;
Module* g_module_open(char* file_name, ModuleFlags flags) nothrow;
int g_module_supported() nothrow;
}

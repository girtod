// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/GObject-2.0.gir"

module GObject2;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;

// package: "gobject-2.0";
// C header: "glib-object.h";

// c:symbol-prefixes: ["g"]
// c:identifier-prefixes: ["G"]

// module GObject2;

//  --- mixin/GObject2__MODULE_HEAD.d --->

// Newer GLib versions define 'Type', older ones do not.
static if (!is(Type))
   alias size_t Type;

// <--- mixin/GObject2__MODULE_HEAD.d ---


// This is the signature of marshaller functions, required to marshall
// arrays of parameter values to signal emissions into C language callback
// invocations. It is merely an alias to #GClosureMarshal since the #GClosure
// mechanism takes over responsibility of actual function invocation for the
// signal system.
alias ClosureMarshal SignalCMarshaller;

// This is the signature of va_list marshaller functions, an optional
// marshaller that can be used in some situations to avoid
// marshalling the signal argument into GValues.
alias VaClosureMarshal SignalCVaMarshaller;

// alias "Type" removed: Defined in glib


// A callback function used by the type system to finalize those portions
// of a derived types class structure that were setup from the corresponding
// GBaseInitFunc() function. Class finalization basically works the inverse
// way in which class intialization is performed.
// See GClassInitFunc() for a discussion of the class intialization process.
// <g_class>: The #GTypeClass structure to finalize.
extern (C) alias void function (void* g_class) nothrow BaseFinalizeFunc;


// A callback function used by the type system to do base initialization
// of the class structures of derived types. It is called as part of the
// initialization process of all derived classes and should reallocate
// or reset all dynamic class members copied over from the parent class.
// For example, class members (such as strings) that are not sufficiently
// handled by a plain memory copy of the parent class into the derived class
// have to be altered. See GClassInitFunc() for a discussion of the class
// intialization process.
// <g_class>: The #GTypeClass structure to initialize.
extern (C) alias void function (void* g_class) nothrow BaseInitFunc;


// #GBinding is the representation of a binding between a property on a
// #GObject instance (or source) and another property on another #GObject
// instance (or target). Whenever the source property changes, the same
// value is applied to the target property; for instance, the following
// binding:
// 
// |[
// g_object_bind_property (object1, "property-a",
// object2, "property-b",
// G_BINDING_DEFAULT);
// ]|
// 
// will cause <emphasis>object2:property-b</emphasis> to be updated every
// time g_object_set() or the specific accessor changes the value of
// <emphasis>object1:property-a</emphasis>.
// 
// It is possible to create a bidirectional binding between two properties
// of two #GObject instances, so that if either property changes, the
// other is updated as well, for instance:
// 
// |[
// g_object_bind_property (object1, "property-a",
// object2, "property-b",
// G_BINDING_BIDIRECTIONAL);
// ]|
// 
// will keep the two properties in sync.
// 
// It is also possible to set a custom transformation function (in both
// directions, in case of a bidirectional binding) to apply a custom
// transformation from the source value to the target value before
// applying it; for instance, the following binding:
// 
// |[
// g_object_bind_property_full (adjustment1, "value",
// adjustment2, "value",
// G_BINDING_BIDIRECTIONAL,
// celsius_to_fahrenheit,
// fahrenheit_to_celsius,
// NULL, NULL);
// ]|
// 
// will keep the <emphasis>value</emphasis> property of the two adjustments
// in sync; the <function>celsius_to_fahrenheit</function> function will be
// called whenever the <emphasis>adjustment1:value</emphasis> property changes
// and will transform the current value of the property before applying it
// to the <emphasis>adjustment2:value</emphasis> property; vice versa, the
// <function>fahrenheit_to_celsius</function> function will be called whenever
// the <emphasis>adjustment2:value</emphasis> property changes, and will
// transform the current value of the property before applying it to the
// <emphasis>adjustment1:value</emphasis>.
// 
// Note that #GBinding does not resolve cycles by itself; a cycle like
// 
// |[
// object1:propertyA -> object2:propertyB
// object2:propertyB -> object3:propertyC
// object3:propertyC -> object1:propertyA
// ]|
// 
// might lead to an infinite loop. The loop, in this particular case,
// can be avoided if the objects emit the #GObject::notify signal only
// if the value has effectively been changed. A binding is implemented
// using the #GObject::notify signal, so it is susceptible to all the
// various ways of blocking a signal emission, like g_signal_stop_emission()
// or g_signal_handler_block().
// 
// A binding will be severed, and the resources it allocates freed, whenever
// either one of the #GObject instances it refers to are finalized, or when
// the #GBinding instance loses its last reference.
// 
// #GBinding is available since GObject 2.26
struct Binding /* : Object */ /* Version 2.26 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   Object method_parent;


   // VERSION: 2.26
   // Retrieves the flags passed when constructing the #GBinding
   // RETURNS: the #GBindingFlags used by the #GBinding
   BindingFlags get_flags()() nothrow {
      return g_binding_get_flags(&this);
   }

   // VERSION: 2.26
   // Retrieves the #GObject instance used as the source of the binding
   // RETURNS: the source #GObject
   Object* get_source()() nothrow {
      return g_binding_get_source(&this);
   }

   // VERSION: 2.26
   // Retrieves the name of the property of #GBinding:source used as the source
   // of the binding
   // RETURNS: the name of the source property
   char* get_source_property()() nothrow {
      return g_binding_get_source_property(&this);
   }

   // VERSION: 2.26
   // Retrieves the #GObject instance used as the target of the binding
   // RETURNS: the target #GObject
   Object* get_target()() nothrow {
      return g_binding_get_target(&this);
   }

   // VERSION: 2.26
   // Retrieves the name of the property of #GBinding:target used as the target
   // of the binding
   // RETURNS: the name of the target property
   char* get_target_property()() nothrow {
      return g_binding_get_target_property(&this);
   }
}


// Flags to be passed to g_object_bind_property() or
// g_object_bind_property_full().
// 
// This enumeration can be extended at later date.
enum BindingFlags /* Version 2.26 */ {
   DEFAULT = 0,
   BIDIRECTIONAL = 1,
   SYNC_CREATE = 2,
   INVERT_BOOLEAN = 4
}

// VERSION: 2.26
// A function to be called to transform the source property of @source
// from @source_value into the target property of @target
// using @target_value.
// 
// otherwise
// RETURNS: %TRUE if the transformation was successful, and %FALSE
// <binding>: a #GBinding
// <source_value>: the value of the source property
// <target_value>: the value of the target property
// <user_data>: data passed to the transform function
extern (C) alias int function (Binding* binding, Value* source_value, Value* target_value, void* user_data) nothrow BindingTransformFunc;


// Unintrospectable callback: BoxedCopyFunc() / ()
// This function is provided by the user and should produce a copy
// of the passed in boxed structure.
// RETURNS: The newly created copy of the boxed structure.
// <boxed>: The boxed structure to be copied.
extern (C) alias void* function (void* boxed) nothrow BoxedCopyFunc;


// This function is provided by the user and should free the boxed
// structure passed.
// <boxed>: The boxed structure to be freed.
extern (C) alias void function (void* boxed) nothrow BoxedFreeFunc;

// A #GCClosure is a specialization of #GClosure for C function callbacks.
struct CClosure {
   Closure closure;
   void* callback;

   static void marshal_BOOLEAN__BOXED_BOXED(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_BOOLEAN__BOXED_BOXED(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_BOOLEAN__BOXED_BOXEDv() / g_cclosure_marshal_BOOLEAN__BOXED_BOXEDv()
   static void marshal_BOOLEAN__BOXED_BOXEDv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_BOOLEAN__BOXED_BOXEDv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>gboolean (*callback) (gpointer instance, gint arg1, gpointer user_data)</literal> where the #gint parameter
   // denotes a flags type.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: a #GValue which can store the returned #gboolean
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding instance and arg1
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_BOOLEAN__FLAGS(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_BOOLEAN__FLAGS(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_BOOLEAN__FLAGSv() / g_cclosure_marshal_BOOLEAN__FLAGSv()
   static void marshal_BOOLEAN__FLAGSv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_BOOLEAN__FLAGSv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>gchar* (*callback) (gpointer instance, GObject *arg1, gpointer arg2, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: a #GValue, which can store the returned string
   // <n_param_values>: 3
   // <param_values>: a #GValue array holding instance, arg1 and arg2
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_STRING__OBJECT_POINTER(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_STRING__OBJECT_POINTER(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_STRING__OBJECT_POINTERv() / g_cclosure_marshal_STRING__OBJECT_POINTERv()
   static void marshal_STRING__OBJECT_POINTERv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_STRING__OBJECT_POINTERv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, gboolean arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #gboolean parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__BOOLEAN(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__BOOLEAN(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__BOOLEANv() / g_cclosure_marshal_VOID__BOOLEANv()
   static void marshal_VOID__BOOLEANv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__BOOLEANv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, GBoxed *arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #GBoxed* parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__BOXED(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__BOXED(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__BOXEDv() / g_cclosure_marshal_VOID__BOXEDv()
   static void marshal_VOID__BOXEDv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__BOXEDv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, gchar arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #gchar parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__CHAR(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__CHAR(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__CHARv() / g_cclosure_marshal_VOID__CHARv()
   static void marshal_VOID__CHARv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__CHARv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, gdouble arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #gdouble parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__DOUBLE(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__DOUBLE(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__DOUBLEv() / g_cclosure_marshal_VOID__DOUBLEv()
   static void marshal_VOID__DOUBLEv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__DOUBLEv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, gint arg1, gpointer user_data)</literal> where the #gint parameter denotes an enumeration type..
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the enumeration parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__ENUM(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__ENUM(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__ENUMv() / g_cclosure_marshal_VOID__ENUMv()
   static void marshal_VOID__ENUMv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__ENUMv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, gint arg1, gpointer user_data)</literal> where the #gint parameter denotes a flags type.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the flags parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__FLAGS(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__FLAGS(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__FLAGSv() / g_cclosure_marshal_VOID__FLAGSv()
   static void marshal_VOID__FLAGSv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__FLAGSv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, gfloat arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #gfloat parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__FLOAT(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__FLOAT(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__FLOATv() / g_cclosure_marshal_VOID__FLOATv()
   static void marshal_VOID__FLOATv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__FLOATv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, gint arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #gint parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__INT(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__INT(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__INTv() / g_cclosure_marshal_VOID__INTv()
   static void marshal_VOID__INTv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__INTv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, glong arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #glong parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__LONG(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__LONG(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__LONGv() / g_cclosure_marshal_VOID__LONGv()
   static void marshal_VOID__LONGv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__LONGv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, GObject *arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #GObject* parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__OBJECT(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__OBJECT(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__OBJECTv() / g_cclosure_marshal_VOID__OBJECTv()
   static void marshal_VOID__OBJECTv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__OBJECTv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, GParamSpec *arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #GParamSpec* parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__PARAM(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__PARAM(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__PARAMv() / g_cclosure_marshal_VOID__PARAMv()
   static void marshal_VOID__PARAMv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__PARAMv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, gpointer arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #gpointer parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__POINTER(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__POINTER(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__POINTERv() / g_cclosure_marshal_VOID__POINTERv()
   static void marshal_VOID__POINTERv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__POINTERv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, const gchar *arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #gchar* parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__STRING(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__STRING(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__STRINGv() / g_cclosure_marshal_VOID__STRINGv()
   static void marshal_VOID__STRINGv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__STRINGv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, guchar arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #guchar parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__UCHAR(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__UCHAR(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__UCHARv() / g_cclosure_marshal_VOID__UCHARv()
   static void marshal_VOID__UCHARv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__UCHARv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, guint arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #guint parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__UINT(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__UINT(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, guint arg1, gpointer arg2, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 3
   // <param_values>: a #GValue array holding instance, arg1 and arg2
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__UINT_POINTER(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__UINT_POINTER(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__UINT_POINTERv() / g_cclosure_marshal_VOID__UINT_POINTERv()
   static void marshal_VOID__UINT_POINTERv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__UINT_POINTERv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }
   // Unintrospectable function: marshal_VOID__UINTv() / g_cclosure_marshal_VOID__UINTv()
   static void marshal_VOID__UINTv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__UINTv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, gulong arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #gulong parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__ULONG(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__ULONG(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__ULONGv() / g_cclosure_marshal_VOID__ULONGv()
   static void marshal_VOID__ULONGv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__ULONGv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // VERSION: 2.26
   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, GVariant *arg1, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 2
   // <param_values>: a #GValue array holding the instance and the #GVariant* parameter
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__VARIANT(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__VARIANT(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__VARIANTv() / g_cclosure_marshal_VOID__VARIANTv()
   static void marshal_VOID__VARIANTv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__VARIANTv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // A marshaller for a #GCClosure with a callback of type
   // <literal>void (*callback) (gpointer instance, gpointer user_data)</literal>.
   // <closure>: the #GClosure to which the marshaller belongs
   // <return_value>: ignored
   // <n_param_values>: 1
   // <param_values>: a #GValue array holding only the instance
   // <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
   // <marshal_data>: additional data specified when registering the marshaller
   static void marshal_VOID__VOID(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_VOID__VOID(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_VOID__VOIDv() / g_cclosure_marshal_VOID__VOIDv()
   static void marshal_VOID__VOIDv(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_VOID__VOIDv(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // VERSION: 2.30
   // A generic marshaller function implemented via <ulink
   // url="http://sourceware.org/libffi/">libffi</ulink>.
   // <closure>: A #GClosure.
   // <return_gvalue>: A #GValue to store the return value. May be %NULL if the callback of closure doesn't return a value.
   // <n_param_values>: The length of the @param_values array.
   // <param_values>: An array of #GValue<!-- -->s holding the arguments on which to invoke the callback of closure.
   // <invocation_hint>: The invocation hint given as the last argument to g_closure_invoke().
   // <marshal_data>: Additional data specified when registering the marshaller, see g_closure_set_marshal() and g_closure_set_meta_marshal()
   static void marshal_generic(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_gvalue, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
      g_cclosure_marshal_generic(UpCast!(Closure*)(closure), UpCast!(Value*)(return_gvalue), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
   }
   // Unintrospectable function: marshal_generic_va() / g_cclosure_marshal_generic_va()
   static void marshal_generic_va(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, AT2 /*void*/ instance, va_list args_list, AT3 /*void*/ marshal_data, int n_params, AT4 /*Type*/ param_types) nothrow {
      g_cclosure_marshal_generic_va(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), UpCast!(void*)(instance), args_list, UpCast!(void*)(marshal_data), n_params, UpCast!(Type*)(param_types));
   }

   // Unintrospectable function: new() / g_cclosure_new()
   // Creates a new closure which invokes @callback_func with @user_data as
   // the last parameter.
   // RETURNS: a new #GCClosure
   // <callback_func>: the function to invoke
   // <user_data>: user data to pass to @callback_func
   // <destroy_data>: destroy notify to be called when @user_data is no longer used
   static Closure* /*new*/ new_(AT0)(Callback callback_func, AT0 /*void*/ user_data, ClosureNotify destroy_data) nothrow {
      return g_cclosure_new(callback_func, UpCast!(void*)(user_data), destroy_data);
   }

   // Unintrospectable function: new_object() / g_cclosure_new_object()
   // A variant of g_cclosure_new() which uses @object as @user_data and
   // calls g_object_watch_closure() on @object and the created
   // closure. This function is useful when you have a callback closely
   // associated with a #GObject, and want the callback to no longer run
   // after the object is is freed.
   // RETURNS: a new #GCClosure
   // <callback_func>: the function to invoke
   // <object>: a #GObject pointer to pass to @callback_func
   static Closure* /*new*/ new_object(AT0)(Callback callback_func, AT0 /*Object*/ object) nothrow {
      return g_cclosure_new_object(callback_func, UpCast!(Object*)(object));
   }

   // Unintrospectable function: new_object_swap() / g_cclosure_new_object_swap()
   // A variant of g_cclosure_new_swap() which uses @object as @user_data
   // and calls g_object_watch_closure() on @object and the created
   // closure. This function is useful when you have a callback closely
   // associated with a #GObject, and want the callback to no longer run
   // after the object is is freed.
   // RETURNS: a new #GCClosure
   // <callback_func>: the function to invoke
   // <object>: a #GObject pointer to pass to @callback_func
   static Closure* /*new*/ new_object_swap(AT0)(Callback callback_func, AT0 /*Object*/ object) nothrow {
      return g_cclosure_new_object_swap(callback_func, UpCast!(Object*)(object));
   }

   // Unintrospectable function: new_swap() / g_cclosure_new_swap()
   // Creates a new closure which invokes @callback_func with @user_data as
   // the first parameter.
   // RETURNS: a new #GCClosure
   // <callback_func>: the function to invoke
   // <user_data>: user data to pass to @callback_func
   // <destroy_data>: destroy notify to be called when @user_data is no longer used
   static Closure* /*new*/ new_swap(AT0)(Callback callback_func, AT0 /*void*/ user_data, ClosureNotify destroy_data) nothrow {
      return g_cclosure_new_swap(callback_func, UpCast!(void*)(user_data), destroy_data);
   }
}


// The type used for callback functions in structure definitions and function 
// signatures. This doesn't mean that all callback functions must take no 
// parameters and return void. The required signature of a callback function 
// is determined by the context in which is used (e.g. the signal to which it 
// is connected). Use G_CALLBACK() to cast the callback function to a #GCallback.
extern (C) alias void function () nothrow Callback;


// A callback function used by the type system to finalize a class.
// This function is rarely needed, as dynamically allocated class resources
// should be handled by GBaseInitFunc() and GBaseFinalizeFunc().
// Also, specification of a GClassFinalizeFunc() in the #GTypeInfo
// structure of a static type is invalid, because classes of static types
// will never be finalized (they are artificially kept alive when their
// reference count drops to zero).
// <g_class>: The #GTypeClass structure to finalize.
// <class_data>: The @class_data member supplied via the #GTypeInfo structure.
extern (C) alias void function (void* g_class, void* class_data) nothrow ClassFinalizeFunc;


// A callback function used by the type system to initialize the class
// of a specific type. This function should initialize all static class
// members.
// The initialization process of a class involves:
// <itemizedlist>
// <listitem><para>
// 1 - Copying common members from the parent class over to the
// derived class structure.
// </para></listitem>
// <listitem><para>
// 2 -  Zero initialization of the remaining members not copied
// over from the parent class.
// </para></listitem>
// <listitem><para>
// 3 - Invocation of the GBaseInitFunc() initializers of all parent
// types and the class' type.
// </para></listitem>
// <listitem><para>
// 4 - Invocation of the class' GClassInitFunc() initializer.
// </para></listitem>
// </itemizedlist>
// Since derived classes are partially initialized through a memory copy
// of the parent class, the general rule is that GBaseInitFunc() and
// GBaseFinalizeFunc() should take care of necessary reinitialization
// and release of those class members that were introduced by the type
// that specified these GBaseInitFunc()/GBaseFinalizeFunc().
// GClassInitFunc() should only care about initializing static
// class members, while dynamic class members (such as allocated strings
// or reference counted resources) are better handled by a GBaseInitFunc()
// for this type, so proper initialization of the dynamic class members
// is performed for class initialization of derived types as well.
// An example may help to correspond the intend of the different class
// initializers:
// 
// |[
// typedef struct {
// GObjectClass parent_class;
// gint         static_integer;
// gchar       *dynamic_string;
// } TypeAClass;
// static void
// type_a_base_class_init (TypeAClass *class)
// {
// class->dynamic_string = g_strdup ("some string");
// }
// static void
// type_a_base_class_finalize (TypeAClass *class)
// {
// g_free (class->dynamic_string);
// }
// static void
// type_a_class_init (TypeAClass *class)
// {
// class->static_integer = 42;
// }
// 
// typedef struct {
// TypeAClass   parent_class;
// gfloat       static_float;
// GString     *dynamic_gstring;
// } TypeBClass;
// static void
// type_b_base_class_init (TypeBClass *class)
// {
// class->dynamic_gstring = g_string_new ("some other string");
// }
// static void
// type_b_base_class_finalize (TypeBClass *class)
// {
// g_string_free (class->dynamic_gstring);
// }
// static void
// type_b_class_init (TypeBClass *class)
// {
// class->static_float = 3.14159265358979323846;
// }
// ]|
// Initialization of TypeBClass will first cause initialization of
// TypeAClass (derived classes reference their parent classes, see
// g_type_class_ref() on this).
// Initialization of TypeAClass roughly involves zero-initializing its fields,
// then calling its GBaseInitFunc() type_a_base_class_init() to allocate
// its dynamic members (dynamic_string), and finally calling its GClassInitFunc()
// type_a_class_init() to initialize its static members (static_integer).
// The first step in the initialization process of TypeBClass is then
// a plain memory copy of the contents of TypeAClass into TypeBClass and 
// zero-initialization of the remaining fields in TypeBClass.
// The dynamic members of TypeAClass within TypeBClass now need
// reinitialization which is performed by calling type_a_base_class_init()
// with an argument of TypeBClass.
// After that, the GBaseInitFunc() of TypeBClass, type_b_base_class_init()
// is called to allocate the dynamic members of TypeBClass (dynamic_gstring),
// and finally the GClassInitFunc() of TypeBClass, type_b_class_init(),
// is called to complete the initialization process with the static members
// (static_float).
// Corresponding finalization counter parts to the GBaseInitFunc() functions
// have to be provided to release allocated resources at class finalization
// time.
// <g_class>: The #GTypeClass structure to initialize.
// <class_data>: The @class_data member supplied via the #GTypeInfo structure.
extern (C) alias void function (void* g_class, void* class_data) nothrow ClassInitFunc;


// A #GClosure represents a callback supplied by the programmer. It
// will generally comprise a function of some kind and a marshaller
// used to call it. It is the reponsibility of the marshaller to
// convert the arguments for the invocation from #GValue<!-- -->s into
// a suitable form, perform the callback on the converted arguments,
// and transform the return value back into a #GValue.
// 
// In the case of C programs, a closure usually just holds a pointer
// to a function and maybe a data argument, and the marshaller
// converts between #GValue<!-- --> and native C types. The GObject
// library provides the #GCClosure type for this purpose. Bindings for
// other languages need marshallers which convert between #GValue<!--
// -->s and suitable representations in the runtime of the language in
// order to use functions written in that languages as callbacks.
// 
// Within GObject, closures play an important role in the
// implementation of signals. When a signal is registered, the
// @c_marshaller argument to g_signal_new() specifies the default C
// marshaller for any closure which is connected to this
// signal. GObject provides a number of C marshallers for this
// purpose, see the g_cclosure_marshal_*() functions. Additional C
// marshallers can be generated with the <link
// linkend="glib-genmarshal">glib-genmarshal</link> utility.  Closures
// can be explicitly connected to signals with
// g_signal_connect_closure(), but it usually more convenient to let
// GObject create a closure automatically by using one of the
// g_signal_connect_*() functions which take a callback function/user
// data pair.
// 
// Using closures has a number of important advantages over a simple
// callback function/data pointer combination:
// <itemizedlist>
// <listitem><para>
// Closures allow the callee to get the types of the callback parameters,
// which means that language bindings don't have to write individual glue
// for each callback type.
// </para></listitem>
// <listitem><para>
// The reference counting of #GClosure makes it easy to handle reentrancy
// right; if a callback is removed while it is being invoked, the closure
// and its parameters won't be freed until the invocation finishes.
// </para></listitem>
// <listitem><para>
// g_closure_invalidate() and invalidation notifiers allow callbacks to be
// automatically removed when the objects they point to go away.
// </para></listitem>
// </itemizedlist>
struct Closure {
    static import std.bitmanip; mixin(std.bitmanip.bitfields!(
   uint, "ref_count", 15,
   uint, "meta_marshal_nouse", 1,
   uint, "n_guards", 1,
   uint, "n_fnotifiers", 2,
   uint, "n_inotifiers", 8,
   uint, "in_inotify", 1,
   uint, "floating", 1,
   uint, "derivative_flag", 1,
   uint, "in_marshal", 1,
   uint, "is_invalid", 1));
   extern (C) void function (Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow marshal;
   private void* data;
   private ClosureNotifyData* notifiers;


   // A variant of g_closure_new_simple() which stores @object in the
   // @data field of the closure and calls g_object_watch_closure() on
   // @object and the created closure. This function is mainly useful
   // when implementing new types of closures.
   // RETURNS: a newly allocated #GClosure
   // <sizeof_closure>: the size of the structure to allocate, must be at least <literal>sizeof (GClosure)</literal>
   // <object>: a #GObject pointer to store in the @data field of the newly allocated #GClosure
   static Closure* /*new*/ new_object(AT0)(uint sizeof_closure, AT0 /*Object*/ object) nothrow {
      return g_closure_new_object(sizeof_closure, UpCast!(Object*)(object));
   }
   static auto opCall(AT0)(uint sizeof_closure, AT0 /*Object*/ object) {
      return g_closure_new_object(sizeof_closure, UpCast!(Object*)(object));
   }

   // Allocates a struct of the given size and initializes the initial
   // part as a #GClosure. This function is mainly useful when
   // implementing new types of closures.
   // 
   // |[
   // typedef struct _MyClosure MyClosure;
   // struct _MyClosure
   // {
   // GClosure closure;
   // // extra data goes here
   // };
   // 
   // static void
   // my_closure_finalize (gpointer  notify_data,
   // GClosure *closure)
   // {
   // MyClosure *my_closure = (MyClosure *)closure;
   // 
   // // free extra data here
   // }
   // 
   // MyClosure *my_closure_new (gpointer data)
   // {
   // GClosure *closure;
   // MyClosure *my_closure;
   // 
   // closure = g_closure_new_simple (sizeof (MyClosure), data);
   // my_closure = (MyClosure *) closure;
   // 
   // // initialize extra data here
   // 
   // g_closure_add_finalize_notifier (closure, notify_data,
   // my_closure_finalize);
   // return my_closure;
   // }
   // ]|
   // RETURNS: a newly allocated #GClosure
   // <sizeof_closure>: the size of the structure to allocate, must be at least <literal>sizeof (GClosure)</literal>
   // <data>: data to store in the @data field of the newly allocated #GClosure
   static Closure* /*new*/ new_simple(AT0)(uint sizeof_closure, AT0 /*void*/ data) nothrow {
      return g_closure_new_simple(sizeof_closure, UpCast!(void*)(data));
   }
   static auto opCall(AT0)(uint sizeof_closure, AT0 /*void*/ data) {
      return g_closure_new_simple(sizeof_closure, UpCast!(void*)(data));
   }

   // Unintrospectable method: add_finalize_notifier() / g_closure_add_finalize_notifier()
   // Registers a finalization notifier which will be called when the
   // reference count of @closure goes down to 0. Multiple finalization
   // notifiers on a single closure are invoked in unspecified order. If
   // a single call to g_closure_unref() results in the closure being
   // both invalidated and finalized, then the invalidate notifiers will
   // be run before the finalize notifiers.
   // <notify_data>: data to pass to @notify_func
   // <notify_func>: the callback function to register
   void add_finalize_notifier(AT0)(AT0 /*void*/ notify_data, ClosureNotify notify_func) nothrow {
      g_closure_add_finalize_notifier(&this, UpCast!(void*)(notify_data), notify_func);
   }

   // Unintrospectable method: add_invalidate_notifier() / g_closure_add_invalidate_notifier()
   // Registers an invalidation notifier which will be called when the
   // @closure is invalidated with g_closure_invalidate(). Invalidation
   // notifiers are invoked before finalization notifiers, in an
   // unspecified order.
   // <notify_data>: data to pass to @notify_func
   // <notify_func>: the callback function to register
   void add_invalidate_notifier(AT0)(AT0 /*void*/ notify_data, ClosureNotify notify_func) nothrow {
      g_closure_add_invalidate_notifier(&this, UpCast!(void*)(notify_data), notify_func);
   }

   // Unintrospectable method: add_marshal_guards() / g_closure_add_marshal_guards()
   // Adds a pair of notifiers which get invoked before and after the
   // closure callback, respectively. This is typically used to protect
   // the extra arguments for the duration of the callback. See
   // g_object_watch_closure() for an example of marshal guards.
   // <pre_marshal_data>: data to pass to @pre_marshal_notify
   // <pre_marshal_notify>: a function to call before the closure callback
   // <post_marshal_data>: data to pass to @post_marshal_notify
   // <post_marshal_notify>: a function to call after the closure callback
   void add_marshal_guards(AT0, AT1)(AT0 /*void*/ pre_marshal_data, ClosureNotify pre_marshal_notify, AT1 /*void*/ post_marshal_data, ClosureNotify post_marshal_notify) nothrow {
      g_closure_add_marshal_guards(&this, UpCast!(void*)(pre_marshal_data), pre_marshal_notify, UpCast!(void*)(post_marshal_data), post_marshal_notify);
   }

   // Sets a flag on the closure to indicate that its calling
   // environment has become invalid, and thus causes any future
   // invocations of g_closure_invoke() on this @closure to be
   // ignored. Also, invalidation notifiers installed on the closure will
   // be called at this point. Note that unless you are holding a
   // reference to the closure yourself, the invalidation notifiers may
   // unref the closure and cause it to be destroyed, so if you need to
   // access the closure after calling g_closure_invalidate(), make sure
   // that you've previously called g_closure_ref().
   // 
   // Note that g_closure_invalidate() will also be called when the
   // reference count of a closure drops to zero (unless it has already
   // been invalidated before).
   void invalidate()() nothrow {
      g_closure_invalidate(&this);
   }

   // Invokes the closure, i.e. executes the callback represented by the @closure.
   // <return_value>: a #GValue to store the return value. May be %NULL if the callback of @closure doesn't return a value.
   // <n_param_values>: the length of the @param_values array
   // <param_values>: an array of #GValue<!-- -->s holding the arguments on which to invoke the callback of @closure
   // <invocation_hint>: a context-dependent invocation hint
   void invoke(AT0, AT1, AT2)(AT0 /*Value*/ return_value, uint n_param_values, AT1 /*Value*/ param_values, AT2 /*void*/ invocation_hint=null) nothrow {
      g_closure_invoke(&this, UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint));
   }

   // Increments the reference count on a closure to force it staying
   // alive while the caller holds a pointer to it.
   // RETURNS: The @closure passed in, for convenience
   Closure* ref_()() nothrow {
      return g_closure_ref(&this);
   }

   // Unintrospectable method: remove_finalize_notifier() / g_closure_remove_finalize_notifier()
   // Removes a finalization notifier.
   // 
   // Notice that notifiers are automatically removed after they are run.
   // <notify_data>: data which was passed to g_closure_add_finalize_notifier() when registering @notify_func
   // <notify_func>: the callback function to remove
   void remove_finalize_notifier(AT0)(AT0 /*void*/ notify_data, ClosureNotify notify_func) nothrow {
      g_closure_remove_finalize_notifier(&this, UpCast!(void*)(notify_data), notify_func);
   }

   // Unintrospectable method: remove_invalidate_notifier() / g_closure_remove_invalidate_notifier()
   // Removes an invalidation notifier.
   // 
   // Notice that notifiers are automatically removed after they are run.
   // <notify_data>: data which was passed to g_closure_add_invalidate_notifier() when registering @notify_func
   // <notify_func>: the callback function to remove
   void remove_invalidate_notifier(AT0)(AT0 /*void*/ notify_data, ClosureNotify notify_func) nothrow {
      g_closure_remove_invalidate_notifier(&this, UpCast!(void*)(notify_data), notify_func);
   }

   // Unintrospectable method: set_marshal() / g_closure_set_marshal()
   // Sets the marshaller of @closure. The <literal>marshal_data</literal>
   // of @marshal provides a way for a meta marshaller to provide additional
   // information to the marshaller. (See g_closure_set_meta_marshal().) For
   // GObject's C predefined marshallers (the g_cclosure_marshal_*()
   // functions), what it provides is a callback function to use instead of
   // @closure->callback.
   // <marshal>: a #GClosureMarshal function
   void set_marshal()(ClosureMarshal marshal) nothrow {
      g_closure_set_marshal(&this, marshal);
   }

   // Unintrospectable method: set_meta_marshal() / g_closure_set_meta_marshal()
   // Sets the meta marshaller of @closure.  A meta marshaller wraps
   // @closure->marshal and modifies the way it is called in some
   // fashion. The most common use of this facility is for C callbacks.
   // The same marshallers (generated by <link
   // linkend="glib-genmarshal">glib-genmarshal</link>) are used
   // everywhere, but the way that we get the callback function
   // differs. In most cases we want to use @closure->callback, but in
   // other cases we want to use some different technique to retrieve the
   // callback function.
   // 
   // For example, class closures for signals (see
   // g_signal_type_cclosure_new()) retrieve the callback function from a
   // fixed offset in the class structure.  The meta marshaller retrieves
   // the right callback and passes it to the marshaller as the
   // @marshal_data argument.
   // <marshal_data>: context-dependent data to pass to @meta_marshal
   // <meta_marshal>: a #GClosureMarshal function
   void set_meta_marshal(AT0)(AT0 /*void*/ marshal_data, ClosureMarshal meta_marshal) nothrow {
      g_closure_set_meta_marshal(&this, UpCast!(void*)(marshal_data), meta_marshal);
   }

   // Takes over the initial ownership of a closure.  Each closure is
   // initially created in a <firstterm>floating</firstterm> state, which
   // means that the initial reference count is not owned by any caller.
   // g_closure_sink() checks to see if the object is still floating, and
   // if so, unsets the floating state and decreases the reference
   // count. If the closure is not floating, g_closure_sink() does
   // nothing. The reason for the existence of the floating state is to
   // prevent cumbersome code sequences like:
   // |[
   // closure = g_cclosure_new (cb_func, cb_data);
   // g_source_set_closure (source, closure);
   // g_closure_unref (closure); // XXX GObject doesn't really need this
   // ]|
   // Because g_source_set_closure() (and similar functions) take ownership of the
   // initial reference count, if it is unowned, we instead can write:
   // |[
   // g_source_set_closure (source, g_cclosure_new (cb_func, cb_data));
   // ]|
   // 
   // Generally, this function is used together with g_closure_ref(). Ane example
   // of storing a closure for later notification looks like:
   // |[
   // static GClosure *notify_closure = NULL;
   // void
   // foo_notify_set_closure (GClosure *closure)
   // {
   // if (notify_closure)
   // g_closure_unref (notify_closure);
   // notify_closure = closure;
   // if (notify_closure)
   // {
   // g_closure_ref (notify_closure);
   // g_closure_sink (notify_closure);
   // }
   // }
   // ]|
   // 
   // Because g_closure_sink() may decrement the reference count of a closure
   // (if it hasn't been called on @closure yet) just like g_closure_unref(),
   // g_closure_ref() should be called prior to this function.
   void sink()() nothrow {
      g_closure_sink(&this);
   }

   // Decrements the reference count of a closure after it was previously
   // incremented by the same caller. If no other callers are using the
   // closure, then the closure will be destroyed and freed.
   void unref()() nothrow {
      g_closure_unref(&this);
   }
}


// The type used for marshaller functions.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: a #GValue to store the return value. May be %NULL if the callback of @closure doesn't return a value.
// <n_param_values>: the length of the @param_values array
// <param_values>: an array of #GValue<!-- -->s holding the arguments on which to invoke the callback of @closure
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller, see g_closure_set_marshal() and g_closure_set_meta_marshal()
extern (C) alias void function (Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint=null, void* marshal_data=null) nothrow ClosureMarshal;


// The type used for the various notification callbacks which can be registered
// on closures.
// <data>: data specified when registering the notification callback
// <closure>: the #GClosure on which the notification is emitted
extern (C) alias void function (void* data, Closure* closure) nothrow ClosureNotify;

struct ClosureNotifyData {
   void* data;
   ClosureNotify notify;
}


// The connection flags are used to specify the behaviour of a signal's 
// connection.
enum ConnectFlags {
   AFTER = 1,
   SWAPPED = 2
}

// The class of an enumeration type holds information about its 
// possible values.
struct EnumClass {
   TypeClass g_type_class;
   int minimum, maximum;
   uint n_values;
   EnumValue* values;
}


// A structure which contains a single enum value, its name, and its
// nickname.
struct EnumValue {
   int value;
   char* value_name, value_nick;
}


// The class of a flags type holds information about its 
// possible values.
struct FlagsClass {
   TypeClass g_type_class;
   uint mask, n_values;
   FlagsValue* values;
}


// A structure which contains a single flags value, its name, and its
// nickname.
struct FlagsValue {
   uint value;
   char* value_name, value_nick;
}


// All the fields in the <structname>GInitiallyUnowned</structname> structure 
// are private to the #GInitiallyUnowned implementation and should never be 
// accessed directly.
struct InitiallyUnowned /* : Object */ {
   TypeInstance g_type_instance;
   private uint ref_count;
   private GLib2.Data* qdata;
}

// The class structure for the <structname>GInitiallyUnowned</structname> type.
struct InitiallyUnownedClass {
   TypeClass g_type_class;
   private GLib2.SList* construct_properties;
   // Unintrospectable functionp: constructor() / ()
   extern (C) Object* function (Type type, uint n_construct_properties, ObjectConstructParam* construct_properties) nothrow constructor;
   extern (C) void function (Object* object, uint property_id, Value* value, ParamSpec* pspec) nothrow set_property;
   extern (C) void function (Object* object, uint property_id, Value* value, ParamSpec* pspec) nothrow get_property;
   extern (C) void function (Object* object) nothrow dispose;
   extern (C) void function (Object* object) nothrow finalize;
   extern (C) void function (Object* object, uint n_pspecs, ParamSpec** pspecs) nothrow dispatch_properties_changed;
   extern (C) void function (Object* object, ParamSpec* pspec) nothrow notify;
   extern (C) void function (Object* object) nothrow constructed;
   private size_t flags;
   private void*[6] pdummy;
}


// A callback function used by the type system to initialize a new
// instance of a type. This function initializes all instance members and
// allocates any resources required by it.
// Initialization of a derived instance involves calling all its parent
// types instance initializers, so the class member of the instance
// is altered during its initialization to always point to the class that
// belongs to the type the current initializer was introduced for.
// <instance>: The instance to initialize.
// <g_class>: The class of the type the instance is created for.
extern (C) alias void function (TypeInstance* instance, void* g_class) nothrow InstanceInitFunc;


// A callback function used by the type system to finalize an interface.
// This function should destroy any internal data and release any resources
// allocated by the corresponding GInterfaceInitFunc() function.
// <g_iface>: The interface structure to finalize.
// <iface_data>: The @interface_data supplied via the #GInterfaceInfo structure.
extern (C) alias void function (void* g_iface, void* iface_data) nothrow InterfaceFinalizeFunc;


// A structure that provides information to the type system which is
// used specifically for managing interface types.
struct InterfaceInfo {
   InterfaceInitFunc interface_init;
   InterfaceFinalizeFunc interface_finalize;
   void* interface_data;
}


// A callback function used by the type system to initialize a new
// interface.  This function should initialize all internal data and
// allocate any resources required by the interface.
// <g_iface>: The interface structure to initialize.
// <iface_data>: The @interface_data supplied via the #GInterfaceInfo structure.
extern (C) alias void function (void* g_iface, void* iface_data) nothrow InterfaceInitFunc;


// All the fields in the <structname>GObject</structname> structure are private 
// to the #GObject implementation and should never be accessed directly.
struct Object {
   TypeInstance g_type_instance;
   private uint ref_count;
   private GLib2.Data* qdata;


   // Unintrospectable constructor: new_valist() / g_object_new_valist()
   // Creates a new instance of a #GObject subtype and sets its properties.
   // 
   // Construction parameters (see #G_PARAM_CONSTRUCT, #G_PARAM_CONSTRUCT_ONLY)
   // which are not explicitly specified are set to their default values.
   // RETURNS: a new instance of @object_type
   // <object_type>: the type id of the #GObject subtype to instantiate
   // <first_property_name>: the name of the first property
   // <var_args>: the value of the first property, followed optionally by more name/value pairs, followed by %NULL
   static Object* /*new*/ new_valist(AT0)(Type object_type, AT0 /*char*/ first_property_name, va_list var_args) nothrow {
      return g_object_new_valist(object_type, toCString!(char*)(first_property_name), var_args);
   }
   static auto opCall(AT0)(Type object_type, AT0 /*char*/ first_property_name, va_list var_args) {
      return g_object_new_valist(object_type, toCString!(char*)(first_property_name), var_args);
   }

   // Creates a new instance of a #GObject subtype and sets its properties.
   // 
   // Construction parameters (see #G_PARAM_CONSTRUCT, #G_PARAM_CONSTRUCT_ONLY)
   // which are not explicitly specified are set to their default values.
   // 
   // @object_type
   // RETURNS: a new instance of
   // <object_type>: the type id of the #GObject subtype to instantiate
   // <n_parameters>: the length of the @parameters array
   // <parameters>: an array of #GParameter
   static Object* /*new*/ newv(AT0)(Type object_type, uint n_parameters, AT0 /*Parameter*/ parameters) nothrow {
      return g_object_newv(object_type, n_parameters, UpCast!(Parameter*)(parameters));
   }
   static auto opCall(AT0)(Type object_type, uint n_parameters, AT0 /*Parameter*/ parameters) {
      return g_object_newv(object_type, n_parameters, UpCast!(Parameter*)(parameters));
   }
   static size_t compat_control(AT0)(size_t what, AT0 /*void*/ data) nothrow {
      return g_object_compat_control(what, UpCast!(void*)(data));
   }

   // Unintrospectable function: connect() / g_object_connect()
   // A convenience function to connect multiple signals at once.
   // 
   // The signal specs expected by this function have the form
   // "modifier::signal_name", where modifier can be one of the following:
   // <variablelist>
   // <varlistentry>
   // <term>signal</term>
   // <listitem><para>
   // equivalent to <literal>g_signal_connect_data (..., NULL, 0)</literal>
   // </para></listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>object_signal</term>
   // <term>object-signal</term>
   // <listitem><para>
   // equivalent to <literal>g_signal_connect_object (..., 0)</literal>
   // </para></listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>swapped_signal</term>
   // <term>swapped-signal</term>
   // <listitem><para>
   // equivalent to <literal>g_signal_connect_data (..., NULL, G_CONNECT_SWAPPED)</literal>
   // </para></listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>swapped_object_signal</term>
   // <term>swapped-object-signal</term>
   // <listitem><para>
   // equivalent to <literal>g_signal_connect_object (..., G_CONNECT_SWAPPED)</literal>
   // </para></listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>signal_after</term>
   // <term>signal-after</term>
   // <listitem><para>
   // equivalent to <literal>g_signal_connect_data (..., NULL, G_CONNECT_AFTER)</literal>
   // </para></listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>object_signal_after</term>
   // <term>object-signal-after</term>
   // <listitem><para>
   // equivalent to <literal>g_signal_connect_object (..., G_CONNECT_AFTER)</literal>
   // </para></listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>swapped_signal_after</term>
   // <term>swapped-signal-after</term>
   // <listitem><para>
   // equivalent to <literal>g_signal_connect_data (..., NULL, G_CONNECT_SWAPPED | G_CONNECT_AFTER)</literal>
   // </para></listitem>
   // </varlistentry>
   // <varlistentry>
   // <term>swapped_object_signal_after</term>
   // <term>swapped-object-signal-after</term>
   // <listitem><para>
   // equivalent to <literal>g_signal_connect_object (..., G_CONNECT_SWAPPED | G_CONNECT_AFTER)</literal>
   // </para></listitem>
   // </varlistentry>
   // </variablelist>
   // 
   // |[
   // menu->toplevel = g_object_connect (g_object_new (GTK_TYPE_WINDOW,
   // "type", GTK_WINDOW_POPUP,
   // "child", menu,
   // NULL),
   // "signal::event", gtk_menu_window_event, menu,
   // "signal::size_request", gtk_menu_window_size_request, menu,
   // "signal::destroy", gtk_widget_destroyed, &amp;menu-&gt;toplevel,
   // NULL);
   // ]|
   // RETURNS: @object
   // <object>: a #GObject
   // <signal_spec>: the spec for the first signal
   alias g_object_connect connect; // Variadic

   // Unintrospectable function: disconnect() / g_object_disconnect()
   // A convenience function to disconnect multiple signals at once.
   // 
   // The signal specs expected by this function have the form
   // "any_signal", which means to disconnect any signal with matching
   // callback and data, or "any_signal::signal_name", which only
   // disconnects the signal named "signal_name".
   // <object>: a #GObject
   // <signal_spec>: the spec for the first signal
   alias g_object_disconnect disconnect; // Variadic

   // Unintrospectable function: get() / g_object_get()
   // Gets properties of an object.
   // 
   // In general, a copy is made of the property contents and the caller
   // is responsible for freeing the memory in the appropriate manner for
   // the type, for instance by calling g_free() or g_object_unref().
   // 
   // <example>
   // <title>Using g_object_get(<!-- -->)</title>
   // An example of using g_object_get() to get the contents
   // of three properties - one of type #G_TYPE_INT,
   // one of type #G_TYPE_STRING, and one of type #G_TYPE_OBJECT:
   // <programlisting>
   // gint intval;
   // gchar *strval;
   // GObject *objval;
   // 
   // g_object_get (my_object,
   // "int-property", &intval,
   // "str-property", &strval,
   // "obj-property", &objval,
   // NULL);
   // 
   // // Do something with intval, strval, objval
   // 
   // g_free (strval);
   // g_object_unref (objval);
   // </programlisting>
   // </example>
   // <object>: a #GObject
   // <first_property_name>: name of the first property to get
   alias g_object_get get; // Variadic

   // VERSION: 2.4
   // Find the #GParamSpec with the given name for an
   // interface. Generally, the interface vtable passed in as @g_iface
   // will be the default vtable from g_type_default_interface_ref(), or,
   // if you know the interface has already been loaded,
   // g_type_default_interface_peek().
   // 
   // 
   // interface with the name @property_name, or %NULL if no
   // such property exists.
   // RETURNS: the #GParamSpec for the property of the
   // <g_iface>: any interface vtable for the interface, or the default vtable for the interface
   // <property_name>: name of a property to lookup.
   static ParamSpec* interface_find_property(AT0, AT1)(AT0 /*void*/ g_iface, AT1 /*char*/ property_name) nothrow {
      return g_object_interface_find_property(UpCast!(void*)(g_iface), toCString!(char*)(property_name));
   }

   // VERSION: 2.4
   // Add a property to an interface; this is only useful for interfaces
   // that are added to GObject-derived types. Adding a property to an
   // interface forces all objects classes with that interface to have a
   // compatible property. The compatible property could be a newly
   // created #GParamSpec, but normally
   // g_object_class_override_property() will be used so that the object
   // class only needs to provide an implementation and inherits the
   // property description, default value, bounds, and so forth from the
   // interface property.
   // 
   // This function is meant to be called from the interface's default
   // vtable initialization function (the @class_init member of
   // #GTypeInfo.) It must not be called after after @class_init has
   // been called for any object types implementing this interface.
   // <g_iface>: any interface vtable for the interface, or the default vtable for the interface.
   // <pspec>: the #GParamSpec for the new property
   static void interface_install_property(AT0, AT1)(AT0 /*void*/ g_iface, AT1 /*ParamSpec*/ pspec) nothrow {
      g_object_interface_install_property(UpCast!(void*)(g_iface), UpCast!(ParamSpec*)(pspec));
   }

   // VERSION: 2.4
   // Lists the properties of an interface.Generally, the interface
   // vtable passed in as @g_iface will be the default vtable from
   // g_type_default_interface_ref(), or, if you know the interface has
   // already been loaded, g_type_default_interface_peek().
   // 
   // 
   // pointer to an array of pointers to #GParamSpec
   // structures. The paramspecs are owned by GLib, but the
   // array should be freed with g_free() when you are done with
   // it.
   // RETURNS: a
   // <g_iface>: any interface vtable for the interface, or the default vtable for the interface
   // <n_properties_p>: location to store number of properties returned.
   static ParamSpec** /*new container*/ interface_list_properties(AT0, AT1)(AT0 /*void*/ g_iface, /*out*/ AT1 /*uint*/ n_properties_p) nothrow {
      return g_object_interface_list_properties(UpCast!(void*)(g_iface), UpCast!(uint*)(n_properties_p));
   }

   // Unintrospectable function: new() / g_object_new()
   // Creates a new instance of a #GObject subtype and sets its properties.
   // 
   // Construction parameters (see #G_PARAM_CONSTRUCT, #G_PARAM_CONSTRUCT_ONLY)
   // which are not explicitly specified are set to their default values.
   // RETURNS: a new instance of @object_type
   // <object_type>: the type id of the #GObject subtype to instantiate
   // <first_property_name>: the name of the first property
   alias g_object_new new_; // Variadic

   // Unintrospectable function: set() / g_object_set()
   // Sets properties on an object.
   // <object>: a #GObject
   // <first_property_name>: name of the first property to set
   alias g_object_set set; // Variadic

   // Unintrospectable method: add_toggle_ref() / g_object_add_toggle_ref()
   // VERSION: 2.8
   // Increases the reference count of the object by one and sets a
   // callback to be called when all other references to the object are
   // dropped, or when this is already the last reference to the object
   // and another reference is established.
   // 
   // This functionality is intended for binding @object to a proxy
   // object managed by another memory manager. This is done with two
   // paired references: the strong reference added by
   // g_object_add_toggle_ref() and a reverse reference to the proxy
   // object which is either a strong reference or weak reference.
   // 
   // The setup is that when there are no other references to @object,
   // only a weak reference is held in the reverse direction from @object
   // to the proxy object, but when there are other references held to
   // @object, a strong reference is held. The @notify callback is called
   // when the reference from @object to the proxy object should be
   // <firstterm>toggled</firstterm> from strong to weak (@is_last_ref
   // true) or weak to strong (@is_last_ref false).
   // 
   // Since a (normal) reference must be held to the object before
   // calling g_object_add_toggle_ref(), the initial state of the reverse
   // link is always strong.
   // 
   // Multiple toggle references may be added to the same gobject,
   // however if there are multiple toggle references to an object, none
   // of them will ever be notified until all but one are removed.  For
   // this reason, you should only ever use a toggle reference if there
   // is important state in the proxy object.
   // <notify>: a function to call when this reference is the last reference to the object, or is no longer the last reference.
   // <data>: data to pass to @notify
   void add_toggle_ref(AT0)(ToggleNotify notify, AT0 /*void*/ data) nothrow {
      g_object_add_toggle_ref(&this, notify, UpCast!(void*)(data));
   }

   // Unintrospectable method: add_weak_pointer() / g_object_add_weak_pointer()
   // Adds a weak reference from weak_pointer to @object to indicate that
   // the pointer located at @weak_pointer_location is only valid during
   // the lifetime of @object. When the @object is finalized,
   // @weak_pointer will be set to %NULL.
   // 
   // Note that as with g_object_weak_ref(), the weak references created by
   // this method are not thread-safe: they cannot safely be used in one
   // thread if the object's last g_object_unref() might happen in another
   // thread. Use #GWeakRef if thread-safety is required.
   // <weak_pointer_location>: The memory address of a pointer.
   void add_weak_pointer(AT0)(/*inout*/ AT0 /*void**/ weak_pointer_location) nothrow {
      g_object_add_weak_pointer(&this, UpCast!(void**)(weak_pointer_location));
   }

   // VERSION: 2.26
   // Creates a binding between @source_property on @source and @target_property
   // on @target. Whenever the @source_property is changed the @target_property is
   // updated using the same value. For instance:
   // 
   // |[
   // g_object_bind_property (action, "active", widget, "sensitive", 0);
   // ]|
   // 
   // Will result in the "sensitive" property of the widget #GObject instance to be
   // updated with the same value of the "active" property of the action #GObject
   // instance.
   // 
   // If @flags contains %G_BINDING_BIDIRECTIONAL then the binding will be mutual:
   // if @target_property on @target changes then the @source_property on @source
   // will be updated as well.
   // 
   // The binding will automatically be removed when either the @source or the
   // @target instances are finalized. To remove the binding without affecting the
   // @source and the @target you can just call g_object_unref() on the returned
   // #GBinding instance.
   // 
   // A #GObject can have multiple bindings.
   // 
   // binding between the two #GObject instances. The binding is released
   // whenever the #GBinding reference count reaches zero.
   // RETURNS: the #GBinding instance representing the
   // <source_property>: the property on @source to bind
   // <target>: the target #GObject
   // <target_property>: the property on @target to bind
   // <flags>: flags to pass to #GBinding
   Binding* bind_property(AT0, AT1, AT2)(AT0 /*char*/ source_property, AT1 /*Object*/ target, AT2 /*char*/ target_property, BindingFlags flags) nothrow {
      return g_object_bind_property(&this, toCString!(char*)(source_property), UpCast!(Object*)(target), toCString!(char*)(target_property), flags);
   }

   // VERSION: 2.26
   // Complete version of g_object_bind_property().
   // 
   // Creates a binding between @source_property on @source and @target_property
   // on @target, allowing you to set the transformation functions to be used by
   // the binding.
   // 
   // If @flags contains %G_BINDING_BIDIRECTIONAL then the binding will be mutual:
   // if @target_property on @target changes then the @source_property on @source
   // will be updated as well. The @transform_from function is only used in case
   // of bidirectional bindings, otherwise it will be ignored
   // 
   // The binding will automatically be removed when either the @source or the
   // @target instances are finalized. To remove the binding without affecting the
   // @source and the @target you can just call g_object_unref() on the returned
   // #GBinding instance.
   // 
   // A #GObject can have multiple bindings.
   // 
   // <note>The same @user_data parameter will be used for both @transform_to
   // and @transform_from transformation functions; the @notify function will
   // be called once, when the binding is removed. If you need different data
   // for each transformation function, please use
   // g_object_bind_property_with_closures() instead.</note>
   // 
   // binding between the two #GObject instances. The binding is released
   // whenever the #GBinding reference count reaches zero.
   // RETURNS: the #GBinding instance representing the
   // <source_property>: the property on @source to bind
   // <target>: the target #GObject
   // <target_property>: the property on @target to bind
   // <flags>: flags to pass to #GBinding
   // <transform_to>: the transformation function from the @source to the @target, or %NULL to use the default
   // <transform_from>: the transformation function from the @target to the @source, or %NULL to use the default
   // <user_data>: custom data to be passed to the transformation functions, or %NULL
   // <notify>: function to be called when disposing the binding, to free the resources used by the transformation functions
   Binding* bind_property_full(AT0, AT1, AT2, AT3)(AT0 /*char*/ source_property, AT1 /*Object*/ target, AT2 /*char*/ target_property, BindingFlags flags, BindingTransformFunc transform_to, BindingTransformFunc transform_from, AT3 /*void*/ user_data, GLib2.DestroyNotify notify) nothrow {
      return g_object_bind_property_full(&this, toCString!(char*)(source_property), UpCast!(Object*)(target), toCString!(char*)(target_property), flags, transform_to, transform_from, UpCast!(void*)(user_data), notify);
   }

   // VERSION: 2.26
   // Creates a binding between @source_property on @source and @target_property
   // on @target, allowing you to set the transformation functions to be used by
   // the binding.
   // 
   // This function is the language bindings friendly version of
   // g_object_bind_property_full(), using #GClosure<!-- -->s instead of
   // function pointers.
   // 
   // 
   // binding between the two #GObject instances. The binding is released
   // whenever the #GBinding reference count reaches zero.
   // RETURNS: the #GBinding instance representing the
   // <source_property>: the property on @source to bind
   // <target>: the target #GObject
   // <target_property>: the property on @target to bind
   // <flags>: flags to pass to #GBinding
   // <transform_to>: a #GClosure wrapping the transformation function from the @source to the @target, or %NULL to use the default
   // <transform_from>: a #GClosure wrapping the transformation function from the @target to the @source, or %NULL to use the default
   Binding* bind_property_with_closures(AT0, AT1, AT2, AT3, AT4)(AT0 /*char*/ source_property, AT1 /*Object*/ target, AT2 /*char*/ target_property, BindingFlags flags, AT3 /*Closure*/ transform_to, AT4 /*Closure*/ transform_from) nothrow {
      return g_object_bind_property_with_closures(&this, toCString!(char*)(source_property), UpCast!(Object*)(target), toCString!(char*)(target_property), flags, UpCast!(Closure*)(transform_to), UpCast!(Closure*)(transform_from));
   }

   // VERSION: 2.10
   // This function is intended for #GObject implementations to re-enforce a
   // <link linkend="floating-ref">floating</link> object reference.
   // Doing this is seldom required: all
   // #GInitiallyUnowned<!-- -->s are created with a floating reference which
   // usually just needs to be sunken by calling g_object_ref_sink().
   void force_floating()() nothrow {
      g_object_force_floating(&this);
   }

   // Increases the freeze count on @object. If the freeze count is
   // non-zero, the emission of "notify" signals on @object is
   // stopped. The signals are queued until the freeze count is decreased
   // to zero.
   // 
   // This is necessary for accessors that modify multiple properties to prevent
   // premature notification while the object is still being modified.
   void freeze_notify()() nothrow {
      g_object_freeze_notify(&this);
   }

   // Gets a named field from the objects table of associations (see g_object_set_data()).
   // RETURNS: the data if found, or %NULL if no such data exists.
   // <key>: name of the key for that association
   void* get_data(AT0)(AT0 /*char*/ key) nothrow {
      return g_object_get_data(&this, toCString!(char*)(key));
   }

   // Gets a property of an object. @value must have been initialized to the
   // expected type of the property (or a type to which the expected type can be
   // transformed) using g_value_init().
   // 
   // In general, a copy is made of the property contents and the caller is
   // responsible for freeing the memory by calling g_value_unset().
   // 
   // Note that g_object_get_property() is really intended for language
   // bindings, g_object_get() is much more convenient for C programming.
   // <property_name>: the name of the property to get
   // <value>: return location for the property value
   void get_property(AT0, AT1)(AT0 /*char*/ property_name, AT1 /*Value*/ value) nothrow {
      g_object_get_property(&this, toCString!(char*)(property_name), UpCast!(Value*)(value));
   }

   // This function gets back user data pointers stored via
   // g_object_set_qdata().
   // RETURNS: The user data pointer set, or %NULL
   // <quark>: A #GQuark, naming the user data pointer
   void* get_qdata()(GLib2.Quark quark) nothrow {
      return g_object_get_qdata(&this, quark);
   }

   // Unintrospectable method: get_valist() / g_object_get_valist()
   // Gets properties of an object.
   // 
   // In general, a copy is made of the property contents and the caller
   // is responsible for freeing the memory in the appropriate manner for
   // the type, for instance by calling g_free() or g_object_unref().
   // 
   // See g_object_get().
   // <first_property_name>: name of the first property to get
   // <var_args>: return location for the first property, followed optionally by more name/return location pairs, followed by %NULL
   void get_valist(AT0)(AT0 /*char*/ first_property_name, va_list var_args) nothrow {
      g_object_get_valist(&this, toCString!(char*)(first_property_name), var_args);
   }

   // VERSION: 2.10
   // Checks whether @object has a <link linkend="floating-ref">floating</link>
   // reference.
   // RETURNS: %TRUE if @object has a floating reference
   int is_floating()() nothrow {
      return g_object_is_floating(&this);
   }

   // Emits a "notify" signal for the property @property_name on @object.
   // 
   // When possible, eg. when signaling a property change from within the class
   // that registered the property, you should use g_object_notify_by_pspec()
   // instead.
   // <property_name>: the name of a property installed on the class of @object.
   void notify(AT0)(AT0 /*char*/ property_name) nothrow {
      g_object_notify(&this, toCString!(char*)(property_name));
   }

   // VERSION: 2.26
   // Emits a "notify" signal for the property specified by @pspec on @object.
   // 
   // This function omits the property name lookup, hence it is faster than
   // g_object_notify().
   // 
   // One way to avoid using g_object_notify() from within the
   // class that registered the properties, and using g_object_notify_by_pspec()
   // instead, is to store the GParamSpec used with
   // g_object_class_install_property() inside a static array, e.g.:
   // 
   // |[
   // enum
   // {
   // PROP_0,
   // PROP_FOO,
   // PROP_LAST
   // };
   // 
   // static GParamSpec *properties[PROP_LAST];
   // 
   // static void
   // my_object_class_init (MyObjectClass *klass)
   // {
   // properties[PROP_FOO] = g_param_spec_int ("foo", "Foo", "The foo",
   // 0, 100,
   // 50,
   // G_PARAM_READWRITE);
   // g_object_class_install_property (gobject_class,
   // PROP_FOO,
   // properties[PROP_FOO]);
   // }
   // ]|
   // 
   // and then notify a change on the "foo" property with:
   // 
   // |[
   // g_object_notify_by_pspec (self, properties[PROP_FOO]);
   // ]|
   // <pspec>: the #GParamSpec of a property installed on the class of @object.
   void notify_by_pspec(AT0)(AT0 /*ParamSpec*/ pspec) nothrow {
      g_object_notify_by_pspec(&this, UpCast!(ParamSpec*)(pspec));
   }

   // Increases the reference count of @object.
   // RETURNS: the same @object
   Object* ref_()() nothrow {
      return g_object_ref(&this);
   }

   // VERSION: 2.10
   // Increase the reference count of @object, and possibly remove the
   // <link linkend="floating-ref">floating</link> reference, if @object
   // has a floating reference.
   // 
   // In other words, if the object is floating, then this call "assumes
   // ownership" of the floating reference, converting it to a normal
   // reference by clearing the floating flag while leaving the reference
   // count unchanged.  If the object is not floating, then this call
   // adds a new normal reference increasing the reference count by one.
   // RETURNS: @object
   Object* ref_sink()() nothrow {
      return g_object_ref_sink(&this);
   }

   // Unintrospectable method: remove_toggle_ref() / g_object_remove_toggle_ref()
   // VERSION: 2.8
   // Removes a reference added with g_object_add_toggle_ref(). The
   // reference count of the object is decreased by one.
   // <notify>: a function to call when this reference is the last reference to the object, or is no longer the last reference.
   // <data>: data to pass to @notify
   void remove_toggle_ref(AT0)(ToggleNotify notify, AT0 /*void*/ data) nothrow {
      g_object_remove_toggle_ref(&this, notify, UpCast!(void*)(data));
   }

   // Unintrospectable method: remove_weak_pointer() / g_object_remove_weak_pointer()
   // Removes a weak reference from @object that was previously added
   // using g_object_add_weak_pointer(). The @weak_pointer_location has
   // to match the one used with g_object_add_weak_pointer().
   // <weak_pointer_location>: The memory address of a pointer.
   void remove_weak_pointer(AT0)(/*inout*/ AT0 /*void**/ weak_pointer_location) nothrow {
      g_object_remove_weak_pointer(&this, UpCast!(void**)(weak_pointer_location));
   }

   // Releases all references to other objects. This can be used to break
   // reference cycles.
   // 
   // This functions should only be called from object system implementations.
   void run_dispose()() nothrow {
      g_object_run_dispose(&this);
   }

   // Each object carries around a table of associations from
   // strings to pointers.  This function lets you set an association.
   // 
   // If the object already had an association with that name,
   // the old association will be destroyed.
   // <key>: name of the key
   // <data>: data to associate with that key
   void set_data(AT0, AT1)(AT0 /*char*/ key, AT1 /*void*/ data) nothrow {
      g_object_set_data(&this, toCString!(char*)(key), UpCast!(void*)(data));
   }

   // Unintrospectable method: set_data_full() / g_object_set_data_full()
   // Like g_object_set_data() except it adds notification
   // for when the association is destroyed, either by setting it
   // to a different value or when the object is destroyed.
   // 
   // Note that the @destroy callback is not called if @data is %NULL.
   // <key>: name of the key
   // <data>: data to associate with that key
   // <destroy>: function to call when the association is destroyed
   void set_data_full(AT0, AT1)(AT0 /*char*/ key, AT1 /*void*/ data, GLib2.DestroyNotify destroy) nothrow {
      g_object_set_data_full(&this, toCString!(char*)(key), UpCast!(void*)(data), destroy);
   }

   // Sets a property on an object.
   // <property_name>: the name of the property to set
   // <value>: the value
   void set_property(AT0, AT1)(AT0 /*char*/ property_name, AT1 /*Value*/ value) nothrow {
      g_object_set_property(&this, toCString!(char*)(property_name), UpCast!(Value*)(value));
   }

   // Unintrospectable method: set_qdata() / g_object_set_qdata()
   // This sets an opaque, named pointer on an object.
   // The name is specified through a #GQuark (retrived e.g. via
   // g_quark_from_static_string()), and the pointer
   // can be gotten back from the @object with g_object_get_qdata()
   // until the @object is finalized.
   // Setting a previously set user data pointer, overrides (frees)
   // the old pointer set, using #NULL as pointer essentially
   // removes the data stored.
   // <quark>: A #GQuark, naming the user data pointer
   // <data>: An opaque user data pointer
   void set_qdata(AT0)(GLib2.Quark quark, AT0 /*void*/ data) nothrow {
      g_object_set_qdata(&this, quark, UpCast!(void*)(data));
   }

   // Unintrospectable method: set_qdata_full() / g_object_set_qdata_full()
   // This function works like g_object_set_qdata(), but in addition,
   // a void (*destroy) (gpointer) function may be specified which is
   // called with @data as argument when the @object is finalized, or
   // the data is being overwritten by a call to g_object_set_qdata()
   // with the same @quark.
   // <quark>: A #GQuark, naming the user data pointer
   // <data>: An opaque user data pointer
   // <destroy>: Function to invoke with @data as argument, when @data needs to be freed
   void set_qdata_full(AT0)(GLib2.Quark quark, AT0 /*void*/ data, GLib2.DestroyNotify destroy) nothrow {
      g_object_set_qdata_full(&this, quark, UpCast!(void*)(data), destroy);
   }

   // Unintrospectable method: set_valist() / g_object_set_valist()
   // Sets properties on an object.
   // <first_property_name>: name of the first property to set
   // <var_args>: value for the first property, followed optionally by more name/value pairs, followed by %NULL
   void set_valist(AT0)(AT0 /*char*/ first_property_name, va_list var_args) nothrow {
      g_object_set_valist(&this, toCString!(char*)(first_property_name), var_args);
   }

   // Remove a specified datum from the object's data associations,
   // without invoking the association's destroy handler.
   // RETURNS: the data if found, or %NULL if no such data exists.
   // <key>: name of the key
   void* /*new*/ steal_data(AT0)(AT0 /*char*/ key) nothrow {
      return g_object_steal_data(&this, toCString!(char*)(key));
   }

   // This function gets back user data pointers stored via
   // g_object_set_qdata() and removes the @data from object
   // without invoking its destroy() function (if any was
   // set).
   // Usually, calling this function is only required to update
   // user data pointers with a destroy notifier, for example:
   // |[
   // void
   // object_add_to_user_list (GObject     *object,
   // const gchar *new_string)
   // {
   // // the quark, naming the object data
   // GQuark quark_string_list = g_quark_from_static_string ("my-string-list");
   // // retrive the old string list
   // GList *list = g_object_steal_qdata (object, quark_string_list);
   // 
   // // prepend new string
   // list = g_list_prepend (list, g_strdup (new_string));
   // // this changed 'list', so we need to set it again
   // g_object_set_qdata_full (object, quark_string_list, list, free_string_list);
   // }
   // static void
   // free_string_list (gpointer data)
   // {
   // GList *node, *list = data;
   // 
   // for (node = list; node; node = node->next)
   // g_free (node->data);
   // g_list_free (list);
   // }
   // ]|
   // Using g_object_get_qdata() in the above example, instead of
   // g_object_steal_qdata() would have left the destroy function set,
   // and thus the partial string list would have been freed upon
   // g_object_set_qdata_full().
   // RETURNS: The user data pointer set, or %NULL
   // <quark>: A #GQuark, naming the user data pointer
   void* /*new*/ steal_qdata()(GLib2.Quark quark) nothrow {
      return g_object_steal_qdata(&this, quark);
   }

   // Reverts the effect of a previous call to
   // g_object_freeze_notify(). The freeze count is decreased on @object
   // and when it reaches zero, all queued "notify" signals are emitted.
   // 
   // It is an error to call this function when the freeze count is zero.
   void thaw_notify()() nothrow {
      g_object_thaw_notify(&this);
   }

   // Decreases the reference count of @object. When its reference count
   // drops to 0, the object is finalized (i.e. its memory is freed).
   void unref()() nothrow {
      g_object_unref(&this);
   }

   // This function essentially limits the life time of the @closure to
   // the life time of the object. That is, when the object is finalized,
   // the @closure is invalidated by calling g_closure_invalidate() on
   // it, in order to prevent invocations of the closure with a finalized
   // (nonexisting) object. Also, g_object_ref() and g_object_unref() are
   // added as marshal guards to the @closure, to ensure that an extra
   // reference count is held on @object during invocation of the
   // @closure.  Usually, this function will be called on closures that
   // use this @object as closure data.
   // <closure>: GClosure to watch
   void watch_closure(AT0)(AT0 /*Closure*/ closure) nothrow {
      g_object_watch_closure(&this, UpCast!(Closure*)(closure));
   }

   // Unintrospectable method: weak_ref() / g_object_weak_ref()
   // Adds a weak reference callback to an object. Weak references are
   // used for notification when an object is finalized. They are called
   // "weak references" because they allow you to safely hold a pointer
   // to an object without calling g_object_ref() (g_object_ref() adds a
   // strong reference, that is, forces the object to stay alive).
   // 
   // Note that the weak references created by this method are not
   // thread-safe: they cannot safely be used in one thread if the
   // object's last g_object_unref() might happen in another thread.
   // Use #GWeakRef if thread-safety is required.
   // <notify>: callback to invoke before the object is freed
   // <data>: extra data to pass to notify
   void weak_ref(AT0)(WeakNotify notify, AT0 /*void*/ data) nothrow {
      g_object_weak_ref(&this, notify, UpCast!(void*)(data));
   }

   // Unintrospectable method: weak_unref() / g_object_weak_unref()
   // Removes a weak reference callback to an object.
   // <notify>: callback to search for
   // <data>: data to search for
   void weak_unref(AT0)(WeakNotify notify, AT0 /*void*/ data) nothrow {
      g_object_weak_unref(&this, notify, UpCast!(void*)(data));
   }

   // The notify signal is emitted on an object when one of its
   // properties has been changed. Note that getting this signal
   // doesn't guarantee that the value of the property has actually
   // changed, it may also be emitted when the setter for the property
   // is called to reinstate the previous value.
   // 
   // This signal is typically used to obtain change notification for a
   // single property, by specifying the property name as a detail in the
   // g_signal_connect() call, like this:
   // |[
   // g_signal_connect (text_view->buffer, "notify::paste-target-list",
   // G_CALLBACK (gtk_text_view_target_list_notify),
   // text_view)
   // ]|
   // It is important to note that you must use
   // <link linkend="canonical-parameter-name">canonical</link> parameter names as
   // detail strings for the notify signal.
   // <pspec>: the #GParamSpec of the property which changed.
   extern (C) alias static void function (Object* this_, ParamSpec* pspec, void* user_data=null) nothrow signal_notify;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"notify", CB/*:signal_notify*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_notify)||_ttmm!(CB, signal_notify)()) {
      return signal_connect_data!()(&this, cast(char*)"notify",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}


// The class structure for the <structname>GObject</structname> type.
// 
// <example>
// <title>Implementing singletons using a constructor</title>
// <programlisting>
// static MySingleton *the_singleton = NULL;
// 
// static GObject*
// my_singleton_constructor (GType                  type,
// guint                  n_construct_params,
// GObjectConstructParam *construct_params)
// {
// GObject *object;
// 
// if (!the_singleton)
// {
// object = G_OBJECT_CLASS (parent_class)->constructor (type,
// n_construct_params,
// construct_params);
// the_singleton = MY_SINGLETON (object);
// }
// else
// object = g_object_ref (G_OBJECT (the_singleton));
// 
// return object;
// }
// </programlisting></example>
struct ObjectClass {
   TypeClass g_type_class;
   private GLib2.SList* construct_properties;
   // Unintrospectable functionp: constructor() / ()
   extern (C) Object* function (Type type, uint n_construct_properties, ObjectConstructParam* construct_properties) nothrow constructor;
   extern (C) void function (Object* object, uint property_id, Value* value, ParamSpec* pspec) nothrow set_property;
   extern (C) void function (Object* object, uint property_id, Value* value, ParamSpec* pspec) nothrow get_property;
   extern (C) void function (Object* object) nothrow dispose;
   extern (C) void function (Object* object) nothrow finalize;
   extern (C) void function (Object* object, uint n_pspecs, ParamSpec** pspecs) nothrow dispatch_properties_changed;
   extern (C) void function (Object* object, ParamSpec* pspec) nothrow notify;
   extern (C) void function (Object* object) nothrow constructed;
   private size_t flags;
   private void*[6] pdummy;


   // Looks up the #GParamSpec for a property of a class.
   // 
   // %NULL if the class doesn't have a property of that name
   // RETURNS: the #GParamSpec for the property, or
   // <property_name>: the name of the property to look up
   ParamSpec* find_property(AT0)(AT0 /*char*/ property_name) nothrow {
      return g_object_class_find_property(&this, toCString!(char*)(property_name));
   }

   // VERSION: 2.26
   // Installs new properties from an array of #GParamSpec<!-- -->s. This is
   // usually done in the class initializer.
   // 
   // The property id of each property is the index of each #GParamSpec in
   // the @pspecs array.
   // 
   // The property id of 0 is treated specially by #GObject and it should not
   // be used to store a #GParamSpec.
   // 
   // This function should be used if you plan to use a static array of
   // #GParamSpec<!-- -->s and g_object_notify_by_pspec(). For instance, this
   // class initialization:
   // 
   // |[
   // enum {
   // PROP_0, PROP_FOO, PROP_BAR, N_PROPERTIES
   // };
   // 
   // static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };
   // 
   // static void
   // my_object_class_init (MyObjectClass *klass)
   // {
   // GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
   // 
   // obj_properties[PROP_FOO] =
   // g_param_spec_int ("foo", "Foo", "Foo",
   // -1, G_MAXINT,
   // 0,
   // G_PARAM_READWRITE);
   // 
   // obj_properties[PROP_BAR] =
   // g_param_spec_string ("bar", "Bar", "Bar",
   // NULL,
   // G_PARAM_READWRITE);
   // 
   // gobject_class->set_property = my_object_set_property;
   // gobject_class->get_property = my_object_get_property;
   // g_object_class_install_properties (gobject_class,
   // N_PROPERTIES,
   // obj_properties);
   // }
   // ]|
   // 
   // allows calling g_object_notify_by_pspec() to notify of property changes:
   // 
   // |[
   // void
   // my_object_set_foo (MyObject *self, gint foo)
   // {
   // if (self->foo != foo)
   // {
   // self->foo = foo;
   // g_object_notify_by_pspec (G_OBJECT (self), obj_properties[PROP_FOO]);
   // }
   // }
   // ]|
   // <n_pspecs>: the length of the #GParamSpec<!-- -->s array
   // <pspecs>: the #GParamSpec<!-- -->s array defining the new properties
   void install_properties(AT0)(uint n_pspecs, AT0 /*ParamSpec**/ pspecs) nothrow {
      g_object_class_install_properties(&this, n_pspecs, UpCast!(ParamSpec**)(pspecs));
   }

   // Installs a new property. This is usually done in the class initializer.
   // 
   // Note that it is possible to redefine a property in a derived class,
   // by installing a property with the same name. This can be useful at times,
   // e.g. to change the range of allowed values or the default value.
   // <property_id>: the id for the new property
   // <pspec>: the #GParamSpec for the new property
   void install_property(AT0)(uint property_id, AT0 /*ParamSpec*/ pspec) nothrow {
      g_object_class_install_property(&this, property_id, UpCast!(ParamSpec*)(pspec));
   }

   // Get an array of #GParamSpec* for all properties of a class.
   // 
   // #GParamSpec* which should be freed after use
   // RETURNS: an array of
   // <n_properties>: return location for the length of the returned array
   ParamSpec** /*new container*/ list_properties(AT0)(/*out*/ AT0 /*uint*/ n_properties) nothrow {
      return g_object_class_list_properties(&this, UpCast!(uint*)(n_properties));
   }

   // VERSION: 2.4
   // Registers @property_id as referring to a property with the
   // name @name in a parent class or in an interface implemented
   // by @oclass. This allows this class to <firstterm>override</firstterm>
   // a property implementation in a parent class or to provide
   // the implementation of a property from an interface.
   // 
   // <note>
   // Internally, overriding is implemented by creating a property of type
   // #GParamSpecOverride; generally operations that query the properties of
   // the object class, such as g_object_class_find_property() or
   // g_object_class_list_properties() will return the overridden
   // property. However, in one case, the @construct_properties argument of
   // the @constructor virtual function, the #GParamSpecOverride is passed
   // instead, so that the @param_id field of the #GParamSpec will be
   // correct.  For virtually all uses, this makes no difference. If you
   // need to get the overridden property, you can call
   // g_param_spec_get_redirect_target().
   // </note>
   // <property_id>: the new property ID
   // <name>: the name of a property registered in a parent class or in an interface of this class.
   void override_property(AT0)(uint property_id, AT0 /*char*/ name) nothrow {
      g_object_class_override_property(&this, property_id, toCString!(char*)(name));
   }
}


// The <structname>GObjectConstructParam</structname> struct is an auxiliary 
// structure used to hand #GParamSpec/#GValue pairs to the @constructor of
// a #GObjectClass.
struct ObjectConstructParam {
   ParamSpec* pspec;
   Value* value;
}


// The type of the @finalize function of #GObjectClass.
// <object>: the #GObject being finalized
extern (C) alias void function (Object* object) nothrow ObjectFinalizeFunc;


// The type of the @get_property function of #GObjectClass.
// <object>: a #GObject
// <property_id>: the numeric id under which the property was registered with g_object_class_install_property().
// <value>: a #GValue to return the property value in
// <pspec>: the #GParamSpec describing the property
extern (C) alias void function (Object* object, uint property_id, Value* value, ParamSpec* pspec) nothrow ObjectGetPropertyFunc;


// The type of the @set_property function of #GObjectClass.
// <object>: a #GObject
// <property_id>: the numeric id under which the property was registered with g_object_class_install_property().
// <value>: the new value for the property
// <pspec>: the #GParamSpec describing the property
extern (C) alias void function (Object* object, uint property_id, Value* value, ParamSpec* pspec) nothrow ObjectSetPropertyFunc;

enum int PARAM_MASK = 255;
enum int PARAM_READWRITE = 0;
enum int PARAM_STATIC_STRINGS = 0;
enum int PARAM_USER_SHIFT = 8;

// Through the #GParamFlags flag values, certain aspects of parameters
// can be configured.
enum ParamFlags {
   READABLE = 1,
   WRITABLE = 2,
   CONSTRUCT = 4,
   CONSTRUCT_ONLY = 8,
   LAX_VALIDATION = 16,
   STATIC_NAME = 32,
   PRIVATE = 32,
   STATIC_NICK = 64,
   STATIC_BLURB = 128,
   DEPRECATED = cast(uint)2147483648
}

// #GParamSpec is an object structure that encapsulates the metadata
// required to specify parameters, such as e.g. #GObject properties.
// 
// <para id="canonical-parameter-name">
// Parameter names need to start with a letter (a-z or A-Z). Subsequent
// characters can be letters, numbers or a '-'.
// All other characters are replaced by a '-' during construction.
// The result of this replacement is called the canonical name of the
// parameter.
// </para>
struct ParamSpec {
   TypeInstance g_type_instance;
   char* name;
   ParamFlags flags;
   Type value_type, owner_type;
   private char* _nick, _blurb;
   private GLib2.Data* qdata;
   private uint ref_count, param_id;


   // Unintrospectable function: internal() / g_param_spec_internal()
   // Creates a new #GParamSpec instance.
   // 
   // A property name consists of segments consisting of ASCII letters and
   // digits, separated by either the '-' or '_' character. The first
   // character of a property name must be a letter. Names which violate these
   // rules lead to undefined behaviour.
   // 
   // When creating and looking up a #GParamSpec, either separator can be
   // used, but they cannot be mixed. Using '-' is considerably more
   // efficient and in fact required when using property names as detail
   // strings for signals.
   // 
   // Beyond the name, #GParamSpec<!-- -->s have two more descriptive
   // strings associated with them, the @nick, which should be suitable
   // for use as a label for the property in a property editor, and the
   // @blurb, which should be a somewhat longer description, suitable for
   // e.g. a tooltip. The @nick and @blurb should ideally be localized.
   // RETURNS: a newly allocated #GParamSpec instance
   // <param_type>: the #GType for the property; must be derived from #G_TYPE_PARAM
   // <name>: the canonical name of the property
   // <nick>: the nickname of the property
   // <blurb>: a short description of the property
   // <flags>: a combination of #GParamFlags
   static void* internal(AT0, AT1, AT2)(Type param_type, AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, ParamFlags flags) nothrow {
      return g_param_spec_internal(param_type, toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), flags);
   }

   // Get the short description of a #GParamSpec.
   // RETURNS: the short description of @pspec.
   char* get_blurb()() nothrow {
      return g_param_spec_get_blurb(&this);
   }

   // Get the name of a #GParamSpec.
   // 
   // The name is always an "interned" string (as per g_intern_string()).
   // This allows for pointer-value comparisons.
   // RETURNS: the name of @pspec.
   char* get_name()() nothrow {
      return g_param_spec_get_name(&this);
   }

   // Get the nickname of a #GParamSpec.
   // RETURNS: the nickname of @pspec.
   char* get_nick()() nothrow {
      return g_param_spec_get_nick(&this);
   }

   // Gets back user data pointers stored via g_param_spec_set_qdata().
   // RETURNS: the user data pointer set, or %NULL
   // <quark>: a #GQuark, naming the user data pointer
   void* get_qdata()(GLib2.Quark quark) nothrow {
      return g_param_spec_get_qdata(&this, quark);
   }

   // VERSION: 2.4
   // If the paramspec redirects operations to another paramspec,
   // returns that paramspec. Redirect is used typically for
   // providing a new implementation of a property in a derived
   // type while preserving all the properties from the parent
   // type. Redirection is established by creating a property
   // of type #GParamSpecOverride. See g_object_class_override_property()
   // for an example of the use of this capability.
   // 
   // 
   // paramspec should be redirected, or %NULL if none.
   // RETURNS: paramspec to which requests on this
   ParamSpec* get_redirect_target()() nothrow {
      return g_param_spec_get_redirect_target(&this);
   }

   // Unintrospectable method: ref() / g_param_spec_ref()
   // Increments the reference count of @pspec.
   // RETURNS: the #GParamSpec that was passed into this function
   ParamSpec* ref_()() nothrow {
      return g_param_spec_ref(&this);
   }

   // Unintrospectable method: ref_sink() / g_param_spec_ref_sink()
   // VERSION: 2.10
   // Convenience function to ref and sink a #GParamSpec.
   // RETURNS: the #GParamSpec that was passed into this function
   ParamSpec* ref_sink()() nothrow {
      return g_param_spec_ref_sink(&this);
   }

   // Sets an opaque, named pointer on a #GParamSpec. The name is
   // specified through a #GQuark (retrieved e.g. via
   // g_quark_from_static_string()), and the pointer can be gotten back
   // from the @pspec with g_param_spec_get_qdata().  Setting a
   // previously set user data pointer, overrides (frees) the old pointer
   // set, using %NULL as pointer essentially removes the data stored.
   // <quark>: a #GQuark, naming the user data pointer
   // <data>: an opaque user data pointer
   void set_qdata(AT0)(GLib2.Quark quark, AT0 /*void*/ data) nothrow {
      g_param_spec_set_qdata(&this, quark, UpCast!(void*)(data));
   }

   // Unintrospectable method: set_qdata_full() / g_param_spec_set_qdata_full()
   // This function works like g_param_spec_set_qdata(), but in addition,
   // a <literal>void (*destroy) (gpointer)</literal> function may be
   // specified which is called with @data as argument when the @pspec is
   // finalized, or the data is being overwritten by a call to
   // g_param_spec_set_qdata() with the same @quark.
   // <quark>: a #GQuark, naming the user data pointer
   // <data>: an opaque user data pointer
   // <destroy>: function to invoke with @data as argument, when @data needs to be freed
   void set_qdata_full(AT0)(GLib2.Quark quark, AT0 /*void*/ data, GLib2.DestroyNotify destroy) nothrow {
      g_param_spec_set_qdata_full(&this, quark, UpCast!(void*)(data), destroy);
   }

   // The initial reference count of a newly created #GParamSpec is 1,
   // even though no one has explicitly called g_param_spec_ref() on it
   // yet. So the initial reference count is flagged as "floating", until
   // someone calls <literal>g_param_spec_ref (pspec); g_param_spec_sink
   // (pspec);</literal> in sequence on it, taking over the initial
   // reference count (thus ending up with a @pspec that has a reference
   // count of 1 still, but is not flagged "floating" anymore).
   void sink()() nothrow {
      g_param_spec_sink(&this);
   }

   // Gets back user data pointers stored via g_param_spec_set_qdata()
   // and removes the @data from @pspec without invoking its destroy()
   // function (if any was set).  Usually, calling this function is only
   // required to update user data pointers with a destroy notifier.
   // RETURNS: the user data pointer set, or %NULL
   // <quark>: a #GQuark, naming the user data pointer
   void* steal_qdata()(GLib2.Quark quark) nothrow {
      return g_param_spec_steal_qdata(&this, quark);
   }

   // Unintrospectable method: unref() / g_param_spec_unref()
   // Decrements the reference count of a @pspec.
   void unref()() nothrow {
      g_param_spec_unref(&this);
   }
}

// A #GParamSpec derived structure that contains the meta data for boolean properties.
struct ParamSpecBoolean /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   int default_value;
}

// A #GParamSpec derived structure that contains the meta data for boxed properties.
struct ParamSpecBoxed /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
}

// A #GParamSpec derived structure that contains the meta data for character properties.
struct ParamSpecChar /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   byte minimum, maximum, default_value;
}


// The class structure for the <structname>GParamSpec</structname> type.
// Normally, <structname>GParamSpec</structname> classes are filled by
// g_param_type_register_static().
struct ParamSpecClass {
   TypeClass g_type_class;
   Type value_type;
   extern (C) void function (ParamSpec* pspec) nothrow finalize;
   extern (C) void function (ParamSpec* pspec, Value* value) nothrow value_set_default;
   extern (C) int function (ParamSpec* pspec, Value* value) nothrow value_validate;
   extern (C) int function (ParamSpec* pspec, Value* value1, Value* value2) nothrow values_cmp;
   private void*[4] dummy;
}

// A #GParamSpec derived structure that contains the meta data for double properties.
struct ParamSpecDouble /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   double minimum, maximum, default_value, epsilon;
}


// A #GParamSpec derived structure that contains the meta data for enum 
// properties.
struct ParamSpecEnum /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   EnumClass* enum_class;
   int default_value;
}


// A #GParamSpec derived structure that contains the meta data for flags
// properties.
struct ParamSpecFlags /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   FlagsClass* flags_class;
   uint default_value;
}

// A #GParamSpec derived structure that contains the meta data for float properties.
struct ParamSpecFloat /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   float minimum, maximum, default_value, epsilon;
}

// A #GParamSpec derived structure that contains the meta data for #GType properties.
struct ParamSpecGType /* : ParamSpec */ /* Version 2.10 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   Type is_a_type;
}

// A #GParamSpec derived structure that contains the meta data for integer properties.
struct ParamSpecInt /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   int minimum, maximum, default_value;
}

// A #GParamSpec derived structure that contains the meta data for 64bit integer properties.
struct ParamSpecInt64 /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   long minimum, maximum, default_value;
}

// A #GParamSpec derived structure that contains the meta data for long integer properties.
struct ParamSpecLong /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   c_long minimum, maximum, default_value;
}

// A #GParamSpec derived structure that contains the meta data for object properties.
struct ParamSpecObject /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
}


// This is a type of #GParamSpec type that simply redirects operations to
// another paramspec.  All operations other than getting or
// setting the value are redirected, including accessing the nick and
// blurb, validating a value, and so forth. See
// g_param_spec_get_redirect_target() for retrieving the overidden
// property. #GParamSpecOverride is used in implementing
// g_object_class_override_property(), and will not be directly useful
// unless you are implementing a new base type similar to GObject.
struct ParamSpecOverride /* : ParamSpec */ /* Version 2.4 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   private ParamSpec* overridden;
}


// A #GParamSpec derived structure that contains the meta data for %G_TYPE_PARAM
// properties.
struct ParamSpecParam /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
}

// A #GParamSpec derived structure that contains the meta data for pointer properties.
struct ParamSpecPointer /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
}


// A #GParamSpecPool maintains a collection of #GParamSpec<!-- -->s which can be
// quickly accessed by owner and name. The implementation of the #GObject property
// system uses such a pool to store the #GParamSpecs of the properties all object
// types.
struct ParamSpecPool {

   // Inserts a #GParamSpec in the pool.
   // <pspec>: the #GParamSpec to insert
   // <owner_type>: a #GType identifying the owner of @pspec
   void insert(AT0)(AT0 /*ParamSpec*/ pspec, Type owner_type) nothrow {
      g_param_spec_pool_insert(&this, UpCast!(ParamSpec*)(pspec), owner_type);
   }

   // Gets an array of all #GParamSpec<!-- -->s owned by @owner_type in
   // the pool.
   // 
   // allocated array containing pointers to all #GParamSpecs
   // owned by @owner_type in the pool
   // RETURNS: a newly
   // <owner_type>: the owner to look for
   // <n_pspecs_p>: return location for the length of the returned array
   ParamSpec** /*new container*/ list(AT0)(Type owner_type, /*out*/ AT0 /*uint*/ n_pspecs_p) nothrow {
      return g_param_spec_pool_list(&this, owner_type, UpCast!(uint*)(n_pspecs_p));
   }

   // Gets an #GList of all #GParamSpec<!-- -->s owned by @owner_type in
   // the pool.
   // 
   // #GList of all #GParamSpec<!-- -->s owned by @owner_type in
   // the pool#GParamSpec<!-- -->s.
   // RETURNS: a
   // <owner_type>: the owner to look for
   GLib2.List* /*new container*/ list_owned()(Type owner_type) nothrow {
      return g_param_spec_pool_list_owned(&this, owner_type);
   }

   // Looks up a #GParamSpec in the pool.
   // 
   // matching #GParamSpec was found.
   // RETURNS: The found #GParamSpec, or %NULL if no
   // <param_name>: the name to look for
   // <owner_type>: the owner to look for
   // <walk_ancestors>: If %TRUE, also try to find a #GParamSpec with @param_name owned by an ancestor of @owner_type.
   ParamSpec* lookup(AT0)(AT0 /*char*/ param_name, Type owner_type, int walk_ancestors) nothrow {
      return g_param_spec_pool_lookup(&this, toCString!(char*)(param_name), owner_type, walk_ancestors);
   }

   // Removes a #GParamSpec from the pool.
   // <pspec>: the #GParamSpec to remove
   void remove(AT0)(AT0 /*ParamSpec*/ pspec) nothrow {
      g_param_spec_pool_remove(&this, UpCast!(ParamSpec*)(pspec));
   }

   // Creates a new #GParamSpecPool.
   // 
   // If @type_prefixing is %TRUE, lookups in the newly created pool will
   // allow to specify the owner as a colon-separated prefix of the
   // property name, like "GtkContainer:border-width". This feature is
   // deprecated, so you should always set @type_prefixing to %FALSE.
   // RETURNS: a newly allocated #GParamSpecPool.
   // <type_prefixing>: Whether the pool will support type-prefixed property names.
   static ParamSpecPool* new_()(int type_prefixing) nothrow {
      return g_param_spec_pool_new(type_prefixing);
   }
}


// A #GParamSpec derived structure that contains the meta data for string
// properties.
struct ParamSpecString /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   char* default_value, cset_first, cset_nth;
   char substitutor;
    static import std.bitmanip; mixin(std.bitmanip.bitfields!(
   uint, "null_fold_if_empty", 1,
   uint, "ensure_non_null", 1,
    uint, "__dummy32A", 30));
}


// This structure is used to provide the type system with the information
// required to initialize and destruct (finalize) a parameter's class and
// instances thereof.
// The initialized structure is passed to the g_param_type_register_static() 
// The type system will perform a deep copy of this structure, so its memory 
// does not need to be persistent across invocation of 
// g_param_type_register_static().
struct ParamSpecTypeInfo {
   ushort instance_size, n_preallocs;
   extern (C) void function (ParamSpec* pspec) nothrow instance_init;
   Type value_type;
   extern (C) void function (ParamSpec* pspec) nothrow finalize;
   extern (C) void function (ParamSpec* pspec, Value* value) nothrow value_set_default;
   extern (C) int function (ParamSpec* pspec, Value* value) nothrow value_validate;
   extern (C) int function (ParamSpec* pspec, Value* value1, Value* value2) nothrow values_cmp;
}

// A #GParamSpec derived structure that contains the meta data for unsigned character properties.
struct ParamSpecUChar /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   ubyte minimum, maximum, default_value;
}

// A #GParamSpec derived structure that contains the meta data for unsigned integer properties.
struct ParamSpecUInt /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   uint minimum, maximum, default_value;
}

// A #GParamSpec derived structure that contains the meta data for unsigned 64bit integer properties.
struct ParamSpecUInt64 /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   ulong minimum, maximum, default_value;
}

// A #GParamSpec derived structure that contains the meta data for unsigned long integer properties.
struct ParamSpecULong /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   c_ulong minimum, maximum, default_value;
}

// A #GParamSpec derived structure that contains the meta data for unichar (unsigned integer) properties.
struct ParamSpecUnichar /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   dchar default_value;
}

// A #GParamSpec derived structure that contains the meta data for #GValueArray properties.
struct ParamSpecValueArray /* : ParamSpec */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   ParamSpec* element_spec;
   uint fixed_n_elements;
}

// A #GParamSpec derived structure that contains the meta data for #GVariant properties.
struct ParamSpecVariant /* : ParamSpec */ /* Version 2.26 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance paramspec;
   ParamSpec parent_instance;
   GLib2.VariantType* type;
   GLib2.Variant* default_value;
   private void*[4] padding;
}


// The <structname>GParameter</structname> struct is an auxiliary structure used
// to hand parameter name/value pairs to g_object_newv().
struct Parameter {
   char* name;
   Value value;
}

enum int SIGNAL_FLAGS_MASK = 511;
enum int SIGNAL_MATCH_MASK = 63;

// The signal accumulator is a special callback function that can be used
// to collect return values of the various callbacks that are called
// during a signal emission. The signal accumulator is specified at signal
// creation time, if it is left %NULL, no accumulation of callback return
// values is performed. The return value of signal emissions is then the
// value returned by the last callback.
// 
// should be aborted. Returning %FALSE means to abort the
// current emission and %TRUE is returned for continuation.
// RETURNS: The accumulator function returns whether the signal emission
// <ihint>: Signal invocation hint, see #GSignalInvocationHint.
// <return_accu>: Accumulator to collect callback return values in, this is the return value of the current signal emission.
// <handler_return>: A #GValue holding the return value of the signal handler.
// <data>: Callback data that was specified when creating the signal.
extern (C) alias int function (SignalInvocationHint* ihint, Value* return_accu, Value* handler_return, void* data) nothrow SignalAccumulator;


// A simple function pointer to get invoked when the signal is emitted. This 
// allows you to tie a hook to the signal type, so that it will trap all 
// emissions of that signal, from any object.
// 
// You may not attach these to signals created with the #G_SIGNAL_NO_HOOKS flag.
// 
// hook is disconnected (and destroyed).
// RETURNS: whether it wants to stay connected. If it returns %FALSE, the signal
// <ihint>: Signal invocation hint, see #GSignalInvocationHint.
// <n_param_values>: the number of parameters to the function, including the instance on which the signal was emitted.
// <param_values>: the instance on which the signal was emitted, followed by the parameters of the emission.
// <data>: user data associated with the hook.
extern (C) alias int function (SignalInvocationHint* ihint, uint n_param_values, Value* param_values, void* data) nothrow SignalEmissionHook;


// The signal flags are used to specify a signal's behaviour, the overall
// signal description outlines how especially the RUN flags control the
// stages of a signal emission.
enum SignalFlags {
   RUN_FIRST = 1,
   RUN_LAST = 2,
   RUN_CLEANUP = 4,
   NO_RECURSE = 8,
   DETAILED = 16,
   ACTION = 32,
   NO_HOOKS = 64,
   MUST_COLLECT = 128,
   DEPRECATED = 256
}

// The #GSignalInvocationHint structure is used to pass on additional information
// to callbacks during a signal emission.
struct SignalInvocationHint {
   uint signal_id;
   GLib2.Quark detail;
   SignalFlags run_type;
}


// The match types specify what g_signal_handlers_block_matched(),
// g_signal_handlers_unblock_matched() and g_signal_handlers_disconnect_matched()
// match signals by.
enum SignalMatchType {
   ID = 1,
   DETAIL = 2,
   CLOSURE = 4,
   FUNC = 8,
   DATA = 16,
   UNBLOCKED = 32
}

// A structure holding in-depth information for a specific signal. It is
// filled in by the g_signal_query() function.
struct SignalQuery {
   uint signal_id;
   char* signal_name;
   Type itype;
   SignalFlags signal_flags;
   Type return_type;
   uint n_params;
   Type* param_types;
}

enum int TYPE_FUNDAMENTAL_MAX = 255;
enum int TYPE_FUNDAMENTAL_SHIFT = 2;
enum int TYPE_RESERVED_BSE_FIRST = 32;
enum int TYPE_RESERVED_BSE_LAST = 48;
enum int TYPE_RESERVED_GLIB_FIRST = 22;
enum int TYPE_RESERVED_GLIB_LAST = 31;
enum int TYPE_RESERVED_USER_FIRST = 49;

// A callback function used for notification when the state
// of a toggle reference changes. See g_object_add_toggle_ref().
// <data>: Callback data passed to g_object_add_toggle_ref()
// <object>: The object on which g_object_add_toggle_ref() was called.
// <is_last_ref>: %TRUE if the toggle reference is now the last reference to the object. %FALSE if the toggle reference was the last reference and there are now other references.
extern (C) alias void function (void* data, Object* object, int is_last_ref) nothrow ToggleNotify;

// A union holding one collected value.
union TypeCValue {
   int v_int;
   c_long v_long;
   long v_int64;
   double v_double;
   void* v_pointer;
}

// An opaque structure used as the base of all classes.
struct TypeClass {
   private Type g_type;

   // Unintrospectable method: get_private() / g_type_class_get_private()
   void* get_private()(Type private_type) nothrow {
      return g_type_class_get_private(&this, private_type);
   }

   // This is a convenience function often needed in class initializers.
   // It returns the class structure of the immediate parent type of the
   // class passed in.  Since derived classes hold a reference count on
   // their parent classes as long as they are instantiated, the returned
   // class will always exist. This function is essentially equivalent
   // to:
   // 
   // <programlisting>
   // g_type_class_peek (g_type_parent (G_TYPE_FROM_CLASS (g_class)));
   // </programlisting>
   // 
   // of @g_class.
   // RETURNS: The parent class
   TypeClass* peek_parent()() nothrow {
      return g_type_class_peek_parent(&this);
   }

   // Decrements the reference count of the class structure being passed in.
   // Once the last reference count of a class has been released, classes
   // may be finalized by the type system, so further dereferencing of a
   // class pointer after g_type_class_unref() are invalid.
   void unref()() nothrow {
      g_type_class_unref(&this);
   }

   // Unintrospectable method: unref_uncached() / g_type_class_unref_uncached()
   // A variant of g_type_class_unref() for use in #GTypeClassCacheFunc
   // implementations. It unreferences a class without consulting the chain
   // of #GTypeClassCacheFunc<!-- -->s, avoiding the recursion which would occur
   // otherwise.
   void unref_uncached()() nothrow {
      g_type_class_unref_uncached(&this);
   }

   // VERSION: 2.4
   // Registers a private structure for an instantiatable type.
   // 
   // When an object is allocated, the private structures for
   // the type and all of its parent types are allocated
   // sequentially in the same memory block as the public
   // structures.
   // 
   // Note that the accumulated size of the private structures of
   // a type and all its parent types cannot excced 64 KiB.
   // 
   // This function should be called in the type's class_init() function.
   // The private structure can be retrieved using the
   // G_TYPE_INSTANCE_GET_PRIVATE() macro.
   // 
   // The following example shows attaching a private structure
   // <structname>MyObjectPrivate</structname> to an object
   // <structname>MyObject</structname> defined in the standard GObject
   // fashion.
   // type's class_init() function.
   // Note the use of a structure member "priv" to avoid the overhead
   // of repeatedly calling MY_OBJECT_GET_PRIVATE().
   // 
   // |[
   // typedef struct _MyObject        MyObject;
   // typedef struct _MyObjectPrivate MyObjectPrivate;
   // 
   // struct _MyObject {
   // GObject parent;
   // 
   // MyObjectPrivate *priv;
   // };
   // 
   // struct _MyObjectPrivate {
   // int some_field;
   // };
   // 
   // static void
   // my_object_class_init (MyObjectClass *klass)
   // {
   // g_type_class_add_private (klass, sizeof (MyObjectPrivate));
   // }
   // 
   // static void
   // my_object_init (MyObject *my_object)
   // {
   // my_object->priv = G_TYPE_INSTANCE_GET_PRIVATE (my_object,
   // MY_TYPE_OBJECT,
   // MyObjectPrivate);
   // }
   // 
   // static int
   // my_object_get_some_field (MyObject *my_object)
   // {
   // MyObjectPrivate *priv;
   // 
   // g_return_val_if_fail (MY_IS_OBJECT (my_object), 0);
   // 
   // priv = my_object->priv;
   // 
   // return priv->some_field;
   // }
   // ]|
   // <g_class>: class structure for an instantiatable type
   // <private_size>: size of private structure.
   static void add_private(AT0)(AT0 /*void*/ g_class, size_t private_size) nothrow {
      g_type_class_add_private(UpCast!(void*)(g_class), private_size);
   }

   // This function is essentially the same as g_type_class_ref(), except that
   // the classes reference count isn't incremented. As a consequence, this function
   // may return %NULL if the class of the type passed in does not currently
   // exist (hasn't been referenced before).
   // 
   // structure for the given type ID or %NULL if the class does not
   // currently exist.
   // RETURNS: The #GTypeClass
   // <type>: Type ID of a classed type.
   static TypeClass* peek()(Type type) nothrow {
      return g_type_class_peek(type);
   }

   // VERSION: 2.4
   // A more efficient version of g_type_class_peek() which works only for
   // static types.
   // 
   // structure for the given type ID or %NULL if the class does not
   // currently exist or is dynamically loaded.
   // RETURNS: The #GTypeClass
   // <type>: Type ID of a classed type.
   static TypeClass* peek_static()(Type type) nothrow {
      return g_type_class_peek_static(type);
   }

   // Increments the reference count of the class structure belonging to
   // @type. This function will demand-create the class if it doesn't
   // exist already.
   // 
   // structure for the given type ID.
   // RETURNS: The #GTypeClass
   // <type>: Type ID of a classed type.
   static TypeClass* ref_()(Type type) nothrow {
      return g_type_class_ref(type);
   }
}


// A callback function which is called when the reference count of a class 
// drops to zero. It may use g_type_class_ref() to prevent the class from
// being freed. You should not call g_type_class_unref() from a 
// #GTypeClassCacheFunc function to prevent infinite recursion, use 
// g_type_class_unref_uncached() instead.
// 
// The functions have to check the class id passed in to figure 
// whether they actually want to cache the class of this type, since all
// classes are routed through the same #GTypeClassCacheFunc chain.
// 
// called, %FALSE to continue.
// RETURNS: %TRUE to stop further #GTypeClassCacheFunc<!-- -->s from being
// <cache_data>: data that was given to the g_type_add_class_cache_func() call
// <g_class>: The #GTypeClass structure which is unreferenced
extern (C) alias int function (void* cache_data, TypeClass* g_class) nothrow TypeClassCacheFunc;


// The <type>GTypeDebugFlags</type> enumeration values can be passed to
// g_type_init_with_debug_flags() to trigger debugging messages during runtime.
// Note that the messages can also be triggered by setting the
// <envar>GOBJECT_DEBUG</envar> environment variable to a ':'-separated list of 
// "objects" and "signals".
enum TypeDebugFlags {
   NONE = 0,
   OBJECTS = 1,
   SIGNALS = 2,
   MASK = 3
}
// Bit masks used to check or determine characteristics of a type.
enum TypeFlags {
   ABSTRACT = 16,
   VALUE_ABSTRACT = 32
}

// Bit masks used to check or determine specific characteristics of a
// fundamental type.
enum TypeFundamentalFlags {
   CLASSED = 1,
   INSTANTIATABLE = 2,
   DERIVABLE = 4,
   DEEP_DERIVABLE = 8
}

// A structure that provides information to the type system which is
// used specifically for managing fundamental types.
struct TypeFundamentalInfo {
   TypeFundamentalFlags type_flags;
}


// This structure is used to provide the type system with the information
// required to initialize and destruct (finalize) a type's class and
// its instances.
// The initialized structure is passed to the g_type_register_static() function
// (or is copied into the provided #GTypeInfo structure in the
// g_type_plugin_complete_type_info()). The type system will perform a deep
// copy of this structure, so its memory does not need to be persistent
// across invocation of g_type_register_static().
struct GTypeInfo {
   ushort class_size;
   BaseInitFunc base_init;
   BaseFinalizeFunc base_finalize;
   ClassInitFunc class_init;
   ClassFinalizeFunc class_finalize;
   const(void)* class_data;
   ushort instance_size, n_preallocs;
   InstanceInitFunc instance_init;
   TypeValueTable* value_table;
}

// An opaque structure used as the base of all type instances.
struct TypeInstance {
   private TypeClass* g_class;

   // Unintrospectable method: get_private() / g_type_instance_get_private()
   void* get_private()(Type private_type) nothrow {
      return g_type_instance_get_private(&this, private_type);
   }
}

// An opaque structure used as the base of all interface types.
struct TypeInterface {
   private Type g_type, g_instance_type;


   // Returns the corresponding #GTypeInterface structure of the parent type
   // of the instance type to which @g_iface belongs. This is useful when
   // deriving the implementation of an interface from the parent type and
   // then possibly overriding some methods.
   // 
   // corresponding #GTypeInterface structure of the parent type of the
   // instance type to which @g_iface belongs, or %NULL if the parent
   // type doesn't conform to the interface.
   // RETURNS: The
   TypeInterface* peek_parent()() nothrow {
      return g_type_interface_peek_parent(&this);
   }

   // Adds @prerequisite_type to the list of prerequisites of @interface_type.
   // This means that any type implementing @interface_type must also implement
   // @prerequisite_type. Prerequisites can be thought of as an alternative to
   // interface derivation (which GType doesn't support). An interface can have
   // at most one instantiatable prerequisite type.
   // <interface_type>: #GType value of an interface type.
   // <prerequisite_type>: #GType value of an interface or instantiatable type.
   static void add_prerequisite()(Type interface_type, Type prerequisite_type) nothrow {
      g_type_interface_add_prerequisite(interface_type, prerequisite_type);
   }

   // Returns the #GTypePlugin structure for the dynamic interface
   // @interface_type which has been added to @instance_type, or %NULL if
   // @interface_type has not been added to @instance_type or does not
   // have a #GTypePlugin structure. See g_type_add_interface_dynamic().
   // 
   // interface @interface_type of @instance_type.
   // RETURNS: the #GTypePlugin for the dynamic
   // <instance_type>: the #GType value of an instantiatable type.
   // <interface_type>: the #GType value of an interface type.
   static TypePlugin* get_plugin()(Type instance_type, Type interface_type) nothrow {
      return g_type_interface_get_plugin(instance_type, interface_type);
   }

   // Returns the #GTypeInterface structure of an interface to which the
   // passed in class conforms.
   // 
   // structure of iface_type if implemented by @instance_class, %NULL
   // otherwise
   // RETURNS: The GTypeInterface
   // <instance_class>: A #GTypeClass structure.
   // <iface_type>: An interface ID which this class conforms to.
   static TypeInterface* peek(AT0)(AT0 /*TypeClass*/ instance_class, Type iface_type) nothrow {
      return g_type_interface_peek(UpCast!(TypeClass*)(instance_class), iface_type);
   }

   // VERSION: 2.2
   // Returns the prerequisites of an interfaces type.
   // 
   // 
   // newly-allocated zero-terminated array of #GType containing
   // the prerequisites of @interface_type
   // RETURNS: a
   // <interface_type>: an interface type
   // <n_prerequisites>: location to return the number of prerequisites, or %NULL
   static Type* /*new*/ prerequisites(AT0)(Type interface_type, /*out*/ AT0 /*uint*/ n_prerequisites=null) nothrow {
      return g_type_interface_prerequisites(interface_type, UpCast!(uint*)(n_prerequisites));
   }
}


// VERSION: 2.4
// A callback called after an interface vtable is initialized.
// See g_type_add_interface_check().
// <check_data>: data passed to g_type_add_interface_check().
// <g_iface>: the interface that has been initialized
extern (C) alias void function (void* check_data, void* g_iface) nothrow TypeInterfaceCheckFunc;


// #GTypeModule provides a simple implementation of the #GTypePlugin
// interface. The model of #GTypeModule is a dynamically loaded module
// which implements some number of types and interface
// implementations. When the module is loaded, it registers its types
// and interfaces using g_type_module_register_type() and
// g_type_module_add_interface().  As long as any instances of these
// types and interface implementations are in use, the module is kept
// loaded. When the types and interfaces are gone, the module may be
// unloaded. If the types and interfaces become used again, the module
// will be reloaded. Note that the last unref cannot happen in module
// code, since that would lead to the caller's code being unloaded before
// g_object_unref() returns to it.
// 
// Keeping track of whether the module should be loaded or not is done by
// using a use count - it starts at zero, and whenever it is greater than
// zero, the module is loaded. The use count is maintained internally by
// the type system, but also can be explicitly controlled by
// g_type_module_use() and g_type_module_unuse(). Typically, when loading
// a module for the first type, g_type_module_use() will be used to load
// it so that it can initialize its types. At some later point, when the
// module no longer needs to be loaded except for the type
// implementations it contains, g_type_module_unuse() is called.
// 
// #GTypeModule does not actually provide any implementation of module
// loading and unloading. To create a particular module type you must
// derive from #GTypeModule and implement the load and unload functions
// in #GTypeModuleClass.
struct TypeModule /* : Object */ {
   mixin TypePlugin.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   Object parent_instance;
   uint use_count;
   GLib2.SList* type_infos, interface_infos;
   char* name;


   // Registers an additional interface for a type, whose interface lives
   // in the given type plugin. If the interface was already registered
   // for the type in this plugin, nothing will be done.
   // 
   // As long as any instances of the type exist, the type plugin will
   // not be unloaded.
   // <instance_type>: type to which to add the interface.
   // <interface_type>: interface type to add
   // <interface_info>: type information structure
   void add_interface(AT0)(Type instance_type, Type interface_type, AT0 /*InterfaceInfo*/ interface_info) nothrow {
      g_type_module_add_interface(&this, instance_type, interface_type, UpCast!(InterfaceInfo*)(interface_info));
   }

   // VERSION: 2.6
   // Looks up or registers an enumeration that is implemented with a particular
   // type plugin. If a type with name @type_name was previously registered,
   // the #GType identifier for the type is returned, otherwise the type
   // is newly registered, and the resulting #GType identifier returned.
   // 
   // As long as any instances of the type exist, the type plugin will
   // not be unloaded.
   // RETURNS: the new or existing type ID
   // <name>: name for the type
   // <const_static_values>: an array of #GEnumValue structs for the possible enumeration values. The array is terminated by a struct with all members being 0.
   Type register_enum(AT0, AT1)(AT0 /*char*/ name, AT1 /*EnumValue*/ const_static_values) nothrow {
      return g_type_module_register_enum(&this, toCString!(char*)(name), UpCast!(EnumValue*)(const_static_values));
   }

   // VERSION: 2.6
   // Looks up or registers a flags type that is implemented with a particular
   // type plugin. If a type with name @type_name was previously registered,
   // the #GType identifier for the type is returned, otherwise the type
   // is newly registered, and the resulting #GType identifier returned.
   // 
   // As long as any instances of the type exist, the type plugin will
   // not be unloaded.
   // RETURNS: the new or existing type ID
   // <name>: name for the type
   // <const_static_values>: an array of #GFlagsValue structs for the possible flags values. The array is terminated by a struct with all members being 0.
   Type register_flags(AT0, AT1)(AT0 /*char*/ name, AT1 /*FlagsValue*/ const_static_values) nothrow {
      return g_type_module_register_flags(&this, toCString!(char*)(name), UpCast!(FlagsValue*)(const_static_values));
   }

   // Looks up or registers a type that is implemented with a particular
   // type plugin. If a type with name @type_name was previously registered,
   // the #GType identifier for the type is returned, otherwise the type
   // is newly registered, and the resulting #GType identifier returned.
   // 
   // When reregistering a type (typically because a module is unloaded
   // then reloaded, and reinitialized), @module and @parent_type must
   // be the same as they were previously.
   // 
   // As long as any instances of the type exist, the type plugin will
   // not be unloaded.
   // RETURNS: the new or existing type ID
   // <parent_type>: the type for the parent class
   // <type_name>: name for the type
   // <type_info>: type information structure
   // <flags>: flags field providing details about the type
   Type register_type(AT0, AT1)(Type parent_type, AT0 /*char*/ type_name, AT1 /*GTypeInfo*/ type_info, TypeFlags flags) nothrow {
      return g_type_module_register_type(&this, parent_type, toCString!(char*)(type_name), UpCast!(GTypeInfo*)(type_info), flags);
   }

   // Sets the name for a #GTypeModule
   // <name>: a human-readable name to use in error messages.
   void set_name(AT0)(AT0 /*char*/ name) nothrow {
      g_type_module_set_name(&this, toCString!(char*)(name));
   }

   // Decreases the use count of a #GTypeModule by one. If the
   // result is zero, the module will be unloaded. (However, the
   // #GTypeModule will not be freed, and types associated with the
   // #GTypeModule are not unregistered. Once a #GTypeModule is
   // initialized, it must exist forever.)
   void unuse()() nothrow {
      g_type_module_unuse(&this);
   }

   // Increases the use count of a #GTypeModule by one. If the
   // use count was zero before, the plugin will be loaded.
   // If loading the plugin fails, the use count is reset to
   // its prior value.
   // 
   // loading the plugin failed.
   // RETURNS: %FALSE if the plugin needed to be loaded and
   int use()() nothrow {
      return g_type_module_use(&this);
   }
}


// In order to implement dynamic loading of types based on #GTypeModule, 
// the @load and @unload functions in #GTypeModuleClass must be implemented.
struct TypeModuleClass {
   ObjectClass parent_class;
   extern (C) int function (TypeModule* module_) nothrow load;
   extern (C) void function (TypeModule* module_) nothrow unload;
   extern (C) void function () nothrow reserved1;
   extern (C) void function () nothrow reserved2;
   extern (C) void function () nothrow reserved3;
   extern (C) void function () nothrow reserved4;
}


// The GObject type system supports dynamic loading of types. The
// #GTypePlugin interface is used to handle the lifecycle of
// dynamically loaded types.  It goes as follows:
// 
// <orderedlist>
// <listitem><para>
// The type is initially introduced (usually upon loading the module
// the first time, or by your main application that knows what modules
// introduces what types), like this:
// |[
// new_type_id = g_type_register_dynamic (parent_type_id,
// "TypeName",
// new_type_plugin,
// type_flags);
// ]|
// where <literal>new_type_plugin</literal> is an implementation of the
// #GTypePlugin interface.
// </para></listitem>
// <listitem><para>
// The type's implementation is referenced, e.g. through
// g_type_class_ref() or through g_type_create_instance() (this is
// being called by g_object_new()) or through one of the above done on
// a type derived from <literal>new_type_id</literal>.
// </para></listitem>
// <listitem><para>
// This causes the type system to load the type's implementation by calling
// g_type_plugin_use() and g_type_plugin_complete_type_info() on
// <literal>new_type_plugin</literal>.
// </para></listitem>
// <listitem><para>
// At some point the type's implementation isn't required anymore, e.g. after
// g_type_class_unref() or g_type_free_instance() (called when the reference
// count of an instance drops to zero).
// </para></listitem>
// <listitem><para>
// This causes the type system to throw away the information retrieved from
// g_type_plugin_complete_type_info() and then it calls
// g_type_plugin_unuse() on <literal>new_type_plugin</literal>.
// </para></listitem>
// <listitem><para>
// Things may repeat from the second step.
// </para></listitem>
// </orderedlist>
// 
// So basically, you need to implement a #GTypePlugin type that
// carries a use_count, once use_count goes from zero to one, you need
// to load the implementation to successfully handle the upcoming
// g_type_plugin_complete_type_info() call. Later, maybe after
// succeeding use/unuse calls, once use_count drops to zero, you can
// unload the implementation again. The type system makes sure to call
// g_type_plugin_use() and g_type_plugin_complete_type_info() again
// when the type is needed again.
// 
// #GTypeModule is an implementation of #GTypePlugin that already
// implements most of this except for the actual module loading and
// unloading. It even handles multiple registered types per module.
struct TypePlugin /* Interface */ {
   mixin template __interface__() {
      // Calls the @complete_interface_info function from the
      // #GTypePluginClass of @plugin. There should be no need to use this
      // function outside of the GObject type system itself.
      // <instance_type>: the #GType of an instantiable type to which the interface is added
      // <interface_type>: the #GType of the interface whose info is completed
      // <info>: the #GInterfaceInfo to fill in
      void complete_interface_info(AT0)(Type instance_type, Type interface_type, AT0 /*InterfaceInfo*/ info) nothrow {
         g_type_plugin_complete_interface_info(cast(TypePlugin*)&this, instance_type, interface_type, UpCast!(InterfaceInfo*)(info));
      }

      // Calls the @complete_type_info function from the #GTypePluginClass of @plugin.
      // There should be no need to use this function outside of the GObject
      // type system itself.
      // <g_type>: the #GType whose info is completed
      // <info>: the #GTypeInfo struct to fill in
      // <value_table>: the #GTypeValueTable to fill in
      void complete_type_info(AT0, AT1)(Type g_type, AT0 /*GTypeInfo*/ info, AT1 /*TypeValueTable*/ value_table) nothrow {
         g_type_plugin_complete_type_info(cast(TypePlugin*)&this, g_type, UpCast!(GTypeInfo*)(info), UpCast!(TypeValueTable*)(value_table));
      }

      // Calls the @unuse_plugin function from the #GTypePluginClass of
      // @plugin.  There should be no need to use this function outside of
      // the GObject type system itself.
      void unuse()() nothrow {
         g_type_plugin_unuse(cast(TypePlugin*)&this);
      }

      // Calls the @use_plugin function from the #GTypePluginClass of
      // @plugin.  There should be no need to use this function outside of
      // the GObject type system itself.
      void use()() nothrow {
         g_type_plugin_use(cast(TypePlugin*)&this);
      }
   }
   mixin __interface__;
}


// The #GTypePlugin interface is used by the type system in order to handle
// the lifecycle of dynamically loaded types.
struct TypePluginClass {
   private TypeInterface base_iface;
   TypePluginUse use_plugin;
   TypePluginUnuse unuse_plugin;
   TypePluginCompleteTypeInfo complete_type_info;
   TypePluginCompleteInterfaceInfo complete_interface_info;
}


// The type of the @complete_interface_info function of #GTypePluginClass.
// <plugin>: the #GTypePlugin
// <instance_type>: the #GType of an instantiable type to which the interface is added
// <interface_type>: the #GType of the interface whose info is completed
// <info>: the #GInterfaceInfo to fill in
extern (C) alias void function (TypePlugin* plugin, Type instance_type, Type interface_type, InterfaceInfo* info) nothrow TypePluginCompleteInterfaceInfo;


// The type of the @complete_type_info function of #GTypePluginClass.
// <plugin>: the #GTypePlugin
// <g_type>: the #GType whose info is completed
// <info>: the #GTypeInfo struct to fill in
// <value_table>: the #GTypeValueTable to fill in
extern (C) alias void function (TypePlugin* plugin, Type g_type, GTypeInfo* info, TypeValueTable* value_table) nothrow TypePluginCompleteTypeInfo;


// The type of the @unuse_plugin function of #GTypePluginClass.
// <plugin>: the #GTypePlugin whose use count should be decreased
extern (C) alias void function (TypePlugin* plugin) nothrow TypePluginUnuse;


// The type of the @use_plugin function of #GTypePluginClass, which gets called
// to increase the use count of @plugin.
// <plugin>: the #GTypePlugin whose use count should be increased
extern (C) alias void function (TypePlugin* plugin) nothrow TypePluginUse;


// A structure holding information for a specific type. It is
// filled in by the g_type_query() function.
struct TypeQuery {
   Type type;
   char* type_name;
   uint class_size, instance_size;
}


// The #GTypeValueTable provides the functions required by the #GValue implementation,
// to serve as a container for values of a type.
struct TypeValueTable {
   extern (C) void function (Value* value) nothrow value_init;
   extern (C) void function (Value* value) nothrow value_free;
   extern (C) void function (Value* src_value, Value* dest_value) nothrow value_copy;
   // Unintrospectable functionp: value_peek_pointer() / ()
   extern (C) void* function (Value* value) nothrow value_peek_pointer;
   char* collect_format;
   extern (C) char* /*new*/ function (Value* value, uint n_collect_values, TypeCValue* collect_values, uint collect_flags) nothrow collect_value;
   char* lcopy_format;
   extern (C) char* /*new*/ function (Value* value, uint n_collect_values, TypeCValue* collect_values, uint collect_flags) nothrow lcopy_value;


   // Unintrospectable function: peek() / g_type_value_table_peek()
   // Returns the location of the #GTypeValueTable associated with @type.
   // <emphasis>Note that this function should only be used from source code
   // that implements or has internal knowledge of the implementation of
   // @type.</emphasis>
   // 
   // %NULL if there is no #GTypeValueTable associated with @type.
   // RETURNS: Location of the #GTypeValueTable associated with @type or
   // <type>: A #GType value.
   static TypeValueTable* peek()(Type type) nothrow {
      return g_type_value_table_peek(type);
   }
}

enum int VALUE_COLLECT_FORMAT_MAX_LENGTH = 8;
enum int VALUE_NOCOPY_CONTENTS = 134217728;
// Unintrospectable callback: VaClosureMarshal() / ()
extern (C) alias void function (Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow VaClosureMarshal;


// An opaque structure used to hold different types of values.
// The data within the structure has protected scope: it is accessible only
// to functions within a #GTypeValueTable structure, or implementations of
// the g_value_*() API. That is, code portions which implement new fundamental
// types.
// #GValue users cannot make any assumptions about how data is stored
// within the 2 element @data union, and the @g_type member should
// only be accessed through the G_VALUE_TYPE() macro.
struct Value {
   private Type g_type;
   _Value__data__union[2] data;


   // Copies the value of @src_value into @dest_value.
   // <dest_value>: An initialized #GValue structure of the same type as @src_value.
   void copy(AT0)(AT0 /*Value*/ dest_value) nothrow {
      g_value_copy(&this, UpCast!(Value*)(dest_value));
   }

   // Unintrospectable method: dup_boxed() / g_value_dup_boxed()
   // Get the contents of a %G_TYPE_BOXED derived #GValue.  Upon getting,
   // the boxed value is duplicated and needs to be later freed with
   // g_boxed_free(), e.g. like: g_boxed_free (G_VALUE_TYPE (@value),
   // return_value);
   // RETURNS: boxed contents of @value
   void* dup_boxed()() nothrow {
      return g_value_dup_boxed(&this);
   }

   // Get the contents of a %G_TYPE_OBJECT derived #GValue, increasing
   // its reference count. If the contents of the #GValue are %NULL, then
   // %NULL will be returned.
   // 
   // should be unreferenced when no longer needed.
   // RETURNS: object content of @value,
   Object* /*new*/ dup_object()() nothrow {
      return g_value_dup_object(&this);
   }

   // Unintrospectable method: dup_param() / g_value_dup_param()
   // Get the contents of a %G_TYPE_PARAM #GValue, increasing its
   // reference count.
   // 
   // no longer needed.
   // RETURNS: #GParamSpec content of @value, should be unreferenced when
   ParamSpec* dup_param()() nothrow {
      return g_value_dup_param(&this);
   }

   // Get a copy the contents of a %G_TYPE_STRING #GValue.
   // RETURNS: a newly allocated copy of the string content of @value
   char* /*new*/ dup_string()() nothrow {
      return g_value_dup_string(&this);
   }

   // VERSION: 2.26
   // Get the contents of a variant #GValue, increasing its refcount.
   // 
   // g_variant_unref() when no longer needed
   // RETURNS: variant contents of @value, should be unrefed using
   GLib2.Variant* /*new*/ dup_variant()() nothrow {
      return g_value_dup_variant(&this);
   }

   // Determines if @value will fit inside the size of a pointer value.
   // This is an internal function introduced mainly for C marshallers.
   // RETURNS: %TRUE if @value will fit inside a pointer value.
   int fits_pointer()() nothrow {
      return g_value_fits_pointer(&this);
   }

   // Get the contents of a %G_TYPE_BOOLEAN #GValue.
   // RETURNS: boolean contents of @value
   int get_boolean()() nothrow {
      return g_value_get_boolean(&this);
   }

   // Get the contents of a %G_TYPE_BOXED derived #GValue.
   // RETURNS: boxed contents of @value
   void* get_boxed()() nothrow {
      return g_value_get_boxed(&this);
   }

   // DEPRECATED (v2.32) method: get_char - This function's return type is broken, see g_value_get_schar()
   // Do not use this function; it is broken on platforms where the %char
   // type is unsigned, such as ARM and PowerPC.  See g_value_get_schar().
   // 
   // Get the contents of a %G_TYPE_CHAR #GValue.
   // RETURNS: character contents of @value
   char get_char()() nothrow {
      return g_value_get_char(&this);
   }

   // Get the contents of a %G_TYPE_DOUBLE #GValue.
   // RETURNS: double contents of @value
   double get_double()() nothrow {
      return g_value_get_double(&this);
   }

   // Get the contents of a %G_TYPE_ENUM #GValue.
   // RETURNS: enum contents of @value
   int get_enum()() nothrow {
      return g_value_get_enum(&this);
   }

   // Get the contents of a %G_TYPE_FLAGS #GValue.
   // RETURNS: flags contents of @value
   uint get_flags()() nothrow {
      return g_value_get_flags(&this);
   }

   // Get the contents of a %G_TYPE_FLOAT #GValue.
   // RETURNS: float contents of @value
   float get_float()() nothrow {
      return g_value_get_float(&this);
   }

   // VERSION: 2.12
   // Get the contents of a %G_TYPE_GTYPE #GValue.
   // RETURNS: the #GType stored in @value
   Type get_gtype()() nothrow {
      return g_value_get_gtype(&this);
   }

   // Get the contents of a %G_TYPE_INT #GValue.
   // RETURNS: integer contents of @value
   int get_int()() nothrow {
      return g_value_get_int(&this);
   }

   // Get the contents of a %G_TYPE_INT64 #GValue.
   // RETURNS: 64bit integer contents of @value
   long get_int64()() nothrow {
      return g_value_get_int64(&this);
   }

   // Get the contents of a %G_TYPE_LONG #GValue.
   // RETURNS: long integer contents of @value
   c_long get_long()() nothrow {
      return g_value_get_long(&this);
   }

   // Get the contents of a %G_TYPE_OBJECT derived #GValue.
   // RETURNS: object contents of @value
   Object* get_object()() nothrow {
      return g_value_get_object(&this);
   }

   // Get the contents of a %G_TYPE_PARAM #GValue.
   // RETURNS: #GParamSpec content of @value
   ParamSpec* get_param()() nothrow {
      return g_value_get_param(&this);
   }

   // Get the contents of a pointer #GValue.
   // RETURNS: pointer contents of @value
   void* get_pointer()() nothrow {
      return g_value_get_pointer(&this);
   }

   // VERSION: 2.32
   // Get the contents of a %G_TYPE_CHAR #GValue.
   // RETURNS: signed 8 bit integer contents of @value
   byte get_schar()() nothrow {
      return g_value_get_schar(&this);
   }

   // Get the contents of a %G_TYPE_STRING #GValue.
   // RETURNS: string content of @value
   char* get_string()() nothrow {
      return g_value_get_string(&this);
   }

   // Get the contents of a %G_TYPE_UCHAR #GValue.
   // RETURNS: unsigned character contents of @value
   ubyte get_uchar()() nothrow {
      return g_value_get_uchar(&this);
   }

   // Get the contents of a %G_TYPE_UINT #GValue.
   // RETURNS: unsigned integer contents of @value
   uint get_uint()() nothrow {
      return g_value_get_uint(&this);
   }

   // Get the contents of a %G_TYPE_UINT64 #GValue.
   // RETURNS: unsigned 64bit integer contents of @value
   ulong get_uint64()() nothrow {
      return g_value_get_uint64(&this);
   }

   // Get the contents of a %G_TYPE_ULONG #GValue.
   // RETURNS: unsigned long integer contents of @value
   c_ulong get_ulong()() nothrow {
      return g_value_get_ulong(&this);
   }

   // VERSION: 2.26
   // Get the contents of a variant #GValue.
   // RETURNS: variant contents of @value
   GLib2.Variant* /*new*/ get_variant()() nothrow {
      return g_value_get_variant(&this);
   }

   // Initializes @value with the default value of @type.
   // RETURNS: the #GValue structure that has been passed in
   // <g_type>: Type the #GValue should hold values of.
   Value* init()(Type g_type) nothrow {
      return g_value_init(&this, g_type);
   }

   // function asserts that g_value_fits_pointer() returned %TRUE for the
   // passed in value.  This is an internal function introduced mainly
   // for C marshallers.
   // RETURNS: the value contents as pointer. This
   void* peek_pointer()() nothrow {
      return g_value_peek_pointer(&this);
   }

   // Clears the current value in @value and resets it to the default value
   // (as if the value had just been initialized).
   // RETURNS: the #GValue structure that has been passed in
   Value* /*new*/ reset()() nothrow {
      return g_value_reset(&this);
   }

   // Set the contents of a %G_TYPE_BOOLEAN #GValue to @v_boolean.
   // <v_boolean>: boolean value to be set
   void set_boolean()(int v_boolean) nothrow {
      g_value_set_boolean(&this, v_boolean);
   }

   // Set the contents of a %G_TYPE_BOXED derived #GValue to @v_boxed.
   // <v_boxed>: boxed value to be set
   void set_boxed(AT0)(AT0 /*const(void)*/ v_boxed=null) nothrow {
      g_value_set_boxed(&this, UpCast!(const(void)*)(v_boxed));
   }

   // DEPRECATED (v2.4) method: set_boxed_take_ownership - Use g_value_take_boxed() instead.
   // This is an internal function introduced mainly for C marshallers.
   // <v_boxed>: duplicated unowned boxed value to be set
   void set_boxed_take_ownership(AT0)(AT0 /*const(void)*/ v_boxed=null) nothrow {
      g_value_set_boxed_take_ownership(&this, UpCast!(const(void)*)(v_boxed));
   }

   // DEPRECATED (v2.32) method: set_char - This function's input type is broken, see g_value_set_schar()
   // Set the contents of a %G_TYPE_CHAR #GValue to @v_char.
   // <v_char>: character value to be set
   void set_char()(char v_char) nothrow {
      g_value_set_char(&this, v_char);
   }

   // Set the contents of a %G_TYPE_DOUBLE #GValue to @v_double.
   // <v_double>: double value to be set
   void set_double()(double v_double) nothrow {
      g_value_set_double(&this, v_double);
   }

   // Set the contents of a %G_TYPE_ENUM #GValue to @v_enum.
   // <v_enum>: enum value to be set
   void set_enum()(int v_enum) nothrow {
      g_value_set_enum(&this, v_enum);
   }

   // Set the contents of a %G_TYPE_FLAGS #GValue to @v_flags.
   // <v_flags>: flags value to be set
   void set_flags()(uint v_flags) nothrow {
      g_value_set_flags(&this, v_flags);
   }

   // Set the contents of a %G_TYPE_FLOAT #GValue to @v_float.
   // <v_float>: float value to be set
   void set_float()(float v_float) nothrow {
      g_value_set_float(&this, v_float);
   }

   // VERSION: 2.12
   // Set the contents of a %G_TYPE_GTYPE #GValue to @v_gtype.
   // <v_gtype>: #GType to be set
   void set_gtype()(Type v_gtype) nothrow {
      g_value_set_gtype(&this, v_gtype);
   }

   // Sets @value from an instantiatable type via the
   // value_table's collect_value() function.
   // <instance>: the instance
   void set_instance(AT0)(AT0 /*void*/ instance=null) nothrow {
      g_value_set_instance(&this, UpCast!(void*)(instance));
   }

   // Set the contents of a %G_TYPE_INT #GValue to @v_int.
   // <v_int>: integer value to be set
   void set_int()(int v_int) nothrow {
      g_value_set_int(&this, v_int);
   }

   // Set the contents of a %G_TYPE_INT64 #GValue to @v_int64.
   // <v_int64>: 64bit integer value to be set
   void set_int64()(long v_int64) nothrow {
      g_value_set_int64(&this, v_int64);
   }

   // Set the contents of a %G_TYPE_LONG #GValue to @v_long.
   // <v_long>: long integer value to be set
   void set_long()(c_long v_long) nothrow {
      g_value_set_long(&this, v_long);
   }

   // Set the contents of a %G_TYPE_OBJECT derived #GValue to @v_object.
   // 
   // g_value_set_object() increases the reference count of @v_object
   // (the #GValue holds a reference to @v_object).  If you do not wish
   // to increase the reference count of the object (i.e. you wish to
   // pass your current reference to the #GValue because you no longer
   // need it), use g_value_take_object() instead.
   // 
   // It is important that your #GValue holds a reference to @v_object (either its
   // own, or one it has taken) to ensure that the object won't be destroyed while
   // the #GValue still exists).
   // <v_object>: object value to be set
   void set_object(AT0)(AT0 /*Object*/ v_object=null) nothrow {
      g_value_set_object(&this, UpCast!(Object*)(v_object));
   }

   // Unintrospectable method: set_object_take_ownership() / g_value_set_object_take_ownership()
   // DEPRECATED (v2.4) method: set_object_take_ownership - Use g_value_take_object() instead.
   // This is an internal function introduced mainly for C marshallers.
   // <v_object>: object value to be set
   void set_object_take_ownership(AT0)(AT0 /*void*/ v_object=null) nothrow {
      g_value_set_object_take_ownership(&this, UpCast!(void*)(v_object));
   }

   // Set the contents of a %G_TYPE_PARAM #GValue to @param.
   // <param>: the #GParamSpec to be set
   void set_param(AT0)(AT0 /*ParamSpec*/ param=null) nothrow {
      g_value_set_param(&this, UpCast!(ParamSpec*)(param));
   }

   // Unintrospectable method: set_param_take_ownership() / g_value_set_param_take_ownership()
   // DEPRECATED (v2.4) method: set_param_take_ownership - Use g_value_take_param() instead.
   // This is an internal function introduced mainly for C marshallers.
   // <param>: the #GParamSpec to be set
   void set_param_take_ownership(AT0)(AT0 /*ParamSpec*/ param=null) nothrow {
      g_value_set_param_take_ownership(&this, UpCast!(ParamSpec*)(param));
   }

   // Set the contents of a pointer #GValue to @v_pointer.
   // <v_pointer>: pointer value to be set
   void set_pointer(AT0)(AT0 /*void*/ v_pointer) nothrow {
      g_value_set_pointer(&this, UpCast!(void*)(v_pointer));
   }

   // VERSION: 2.32
   // Set the contents of a %G_TYPE_CHAR #GValue to @v_char.
   // <v_char>: signed 8 bit integer to be set
   void set_schar()(byte v_char) nothrow {
      g_value_set_schar(&this, v_char);
   }

   // Set the contents of a %G_TYPE_BOXED derived #GValue to @v_boxed.
   // The boxed value is assumed to be static, and is thus not duplicated
   // when setting the #GValue.
   // <v_boxed>: static boxed value to be set
   void set_static_boxed(AT0)(AT0 /*const(void)*/ v_boxed=null) nothrow {
      g_value_set_static_boxed(&this, UpCast!(const(void)*)(v_boxed));
   }

   // Set the contents of a %G_TYPE_STRING #GValue to @v_string.
   // The string is assumed to be static, and is thus not duplicated
   // when setting the #GValue.
   // <v_string>: static string to be set
   void set_static_string(AT0)(AT0 /*char*/ v_string=null) nothrow {
      g_value_set_static_string(&this, toCString!(char*)(v_string));
   }

   // Set the contents of a %G_TYPE_STRING #GValue to @v_string.
   // <v_string>: caller-owned string to be duplicated for the #GValue
   void set_string(AT0)(AT0 /*char*/ v_string=null) nothrow {
      g_value_set_string(&this, toCString!(char*)(v_string));
   }

   // DEPRECATED (v2.4) method: set_string_take_ownership - Use g_value_take_string() instead.
   // This is an internal function introduced mainly for C marshallers.
   // <v_string>: duplicated unowned string to be set
   void set_string_take_ownership(AT0)(AT0 /*char*/ v_string=null) nothrow {
      g_value_set_string_take_ownership(&this, toCString!(char*)(v_string));
   }

   // Set the contents of a %G_TYPE_UCHAR #GValue to @v_uchar.
   // <v_uchar>: unsigned character value to be set
   void set_uchar()(ubyte v_uchar) nothrow {
      g_value_set_uchar(&this, v_uchar);
   }

   // Set the contents of a %G_TYPE_UINT #GValue to @v_uint.
   // <v_uint>: unsigned integer value to be set
   void set_uint()(uint v_uint) nothrow {
      g_value_set_uint(&this, v_uint);
   }

   // Set the contents of a %G_TYPE_UINT64 #GValue to @v_uint64.
   // <v_uint64>: unsigned 64bit integer value to be set
   void set_uint64()(ulong v_uint64) nothrow {
      g_value_set_uint64(&this, v_uint64);
   }

   // Set the contents of a %G_TYPE_ULONG #GValue to @v_ulong.
   // <v_ulong>: unsigned long integer value to be set
   void set_ulong()(c_ulong v_ulong) nothrow {
      g_value_set_ulong(&this, v_ulong);
   }

   // VERSION: 2.26
   // Set the contents of a variant #GValue to @variant.
   // If the variant is floating, it is consumed.
   // <variant>: a #GVariant, or %NULL
   void set_variant(AT0)(AT0 /*GLib2.Variant*/ variant=null) nothrow {
      g_value_set_variant(&this, UpCast!(GLib2.Variant*)(variant));
   }

   // VERSION: 2.4
   // Sets the contents of a %G_TYPE_BOXED derived #GValue to @v_boxed
   // and takes over the ownership of the callers reference to @v_boxed;
   // the caller doesn't have to unref it any more.
   // <v_boxed>: duplicated unowned boxed value to be set
   void take_boxed(AT0)(AT0 /*const(void)*/ v_boxed=null) nothrow {
      g_value_take_boxed(&this, UpCast!(const(void)*)(v_boxed));
   }

   // Unintrospectable method: take_object() / g_value_take_object()
   // VERSION: 2.4
   // Sets the contents of a %G_TYPE_OBJECT derived #GValue to @v_object
   // and takes over the ownership of the callers reference to @v_object;
   // the caller doesn't have to unref it any more (i.e. the reference
   // count of the object is not increased).
   // 
   // If you want the #GValue to hold its own reference to @v_object, use
   // g_value_set_object() instead.
   // <v_object>: object value to be set
   void take_object(AT0)(AT0 /*void*/ v_object=null) nothrow {
      g_value_take_object(&this, UpCast!(void*)(v_object));
   }

   // Unintrospectable method: take_param() / g_value_take_param()
   // VERSION: 2.4
   // Sets the contents of a %G_TYPE_PARAM #GValue to @param and takes
   // over the ownership of the callers reference to @param; the caller
   // doesn't have to unref it any more.
   // <param>: the #GParamSpec to be set
   void take_param(AT0)(AT0 /*ParamSpec*/ param=null) nothrow {
      g_value_take_param(&this, UpCast!(ParamSpec*)(param));
   }

   // VERSION: 2.4
   // Sets the contents of a %G_TYPE_STRING #GValue to @v_string.
   // <v_string>: string to take ownership of
   void take_string(AT0)(AT0 /*char*/ v_string=null) nothrow {
      g_value_take_string(&this, toCString!(char*)(v_string));
   }

   // VERSION: 2.26
   // Set the contents of a variant #GValue to @variant, and takes over
   // the ownership of the caller's reference to @variant;
   // the caller doesn't have to unref it any more (i.e. the reference
   // count of the variant is not increased).
   // 
   // If @variant was floating then its floating reference is converted to
   // a hard reference.
   // 
   // If you want the #GValue to hold its own reference to @variant, use
   // g_value_set_variant() instead.
   // 
   // This is an internal function introduced mainly for C marshallers.
   // <variant>: a #GVariant, or %NULL
   void take_variant(AT0)(AT0 /*GLib2.Variant*/ variant=null) nothrow {
      g_value_take_variant(&this, UpCast!(GLib2.Variant*)(variant));
   }

   // Tries to cast the contents of @src_value into a type appropriate
   // to store in @dest_value, e.g. to transform a %G_TYPE_INT value
   // into a %G_TYPE_FLOAT value. Performing transformations between
   // value types might incur precision lossage. Especially
   // transformations into strings might reveal seemingly arbitrary
   // results and shouldn't be relied upon for production code (such
   // as rcfile value or object property serialization).
   // 
   // Upon failing transformations, @dest_value is left untouched.
   // RETURNS: Whether a transformation rule was found and could be applied.
   // <dest_value>: Target value.
   int transform(AT0)(AT0 /*Value*/ dest_value) nothrow {
      return g_value_transform(&this, UpCast!(Value*)(dest_value));
   }

   // Clears the current value in @value and "unsets" the type,
   // this releases all resources associated with this GValue.
   // An unset value is the same as an uninitialized (zero-filled)
   // #GValue structure.
   void unset()() nothrow {
      g_value_unset(&this);
   }

   // Unintrospectable function: register_transform_func() / g_value_register_transform_func()
   // Registers a value transformation function for use in g_value_transform().
   // A previously registered transformation function for @src_type and @dest_type
   // will be replaced.
   // <src_type>: Source type.
   // <dest_type>: Target type.
   // <transform_func>: a function which transforms values of type @src_type into value of type @dest_type
   static void register_transform_func()(Type src_type, Type dest_type, ValueTransform transform_func) nothrow {
      g_value_register_transform_func(src_type, dest_type, transform_func);
   }

   // Returns whether a #GValue of type @src_type can be copied into
   // a #GValue of type @dest_type.
   // RETURNS: %TRUE if g_value_copy() is possible with @src_type and @dest_type.
   // <src_type>: source type to be copied.
   // <dest_type>: destination type for copying.
   static int type_compatible()(Type src_type, Type dest_type) nothrow {
      return g_value_type_compatible(src_type, dest_type);
   }

   // Check whether g_value_transform() is able to transform values
   // of type @src_type into values of type @dest_type.
   // RETURNS: %TRUE if the transformation is possible, %FALSE otherwise.
   // <src_type>: Source type.
   // <dest_type>: Target type.
   static int type_transformable()(Type src_type, Type dest_type) nothrow {
      return g_value_type_transformable(src_type, dest_type);
   }
}

// A #GValueArray contains an array of #GValue elements.
struct ValueArray {
   uint n_values;
   Value* values;
   private uint n_prealloced;


   // DEPRECATED (v2.32) constructor: new - Use #GArray and g_array_sized_new() instead.
   // Allocate and initialize a new #GValueArray, optionally preserve space
   // for @n_prealloced elements. New arrays always contain 0 elements,
   // regardless of the value of @n_prealloced.
   // RETURNS: a newly allocated #GValueArray with 0 values
   // <n_prealloced>: number of values to preallocate space for
   static ValueArray* /*new*/ new_()(uint n_prealloced) nothrow {
      return g_value_array_new(n_prealloced);
   }
   static auto opCall()(uint n_prealloced) {
      return g_value_array_new(n_prealloced);
   }

   // DEPRECATED (v2.32) method: append - Use #GArray and g_array_append_val() instead.
   // Insert a copy of @value as last element of @value_array. If @value is
   // %NULL, an uninitialized value is appended.
   // RETURNS: the #GValueArray passed in as @value_array
   // <value>: #GValue to copy into #GValueArray, or %NULL
   ValueArray* append(AT0)(AT0 /*Value*/ value=null) nothrow {
      return g_value_array_append(&this, UpCast!(Value*)(value));
   }

   // DEPRECATED (v2.32) method: copy - Use #GArray and g_array_ref() instead.
   // Construct an exact copy of a #GValueArray by duplicating all its
   // contents.
   // RETURNS: Newly allocated copy of #GValueArray
   ValueArray* /*new*/ copy()() nothrow {
      return g_value_array_copy(&this);
   }

   // DEPRECATED (v2.32) method: free - Use #GArray and g_array_unref() instead.
   // Free a #GValueArray including its contents.
   void free()() nothrow {
      g_value_array_free(&this);
   }

   // DEPRECATED (v2.32) method: get_nth - Use g_array_index() instead.
   // Return a pointer to the value at @index_ containd in @value_array.
   // RETURNS: pointer to a value at @index_ in @value_array
   // <index_>: index of the value of interest
   Value* get_nth()(uint index_) nothrow {
      return g_value_array_get_nth(&this, index_);
   }

   // DEPRECATED (v2.32) method: insert - Use #GArray and g_array_insert_val() instead.
   // Insert a copy of @value at specified position into @value_array. If @value
   // is %NULL, an uninitialized value is inserted.
   // RETURNS: the #GValueArray passed in as @value_array
   // <index_>: insertion position, must be &lt;= value_array-&gt;n_values
   // <value>: #GValue to copy into #GValueArray, or %NULL
   ValueArray* insert(AT0)(uint index_, AT0 /*Value*/ value=null) nothrow {
      return g_value_array_insert(&this, index_, UpCast!(Value*)(value));
   }

   // DEPRECATED (v2.32) method: prepend - Use #GArray and g_array_prepend_val() instead.
   // Insert a copy of @value as first element of @value_array. If @value is
   // %NULL, an uninitialized value is prepended.
   // RETURNS: the #GValueArray passed in as @value_array
   // <value>: #GValue to copy into #GValueArray, or %NULL
   ValueArray* prepend(AT0)(AT0 /*Value*/ value=null) nothrow {
      return g_value_array_prepend(&this, UpCast!(Value*)(value));
   }

   // DEPRECATED (v2.32) method: remove - Use #GArray and g_array_remove_index() instead.
   // Remove the value at position @index_ from @value_array.
   // RETURNS: the #GValueArray passed in as @value_array
   // <index_>: position of value to remove, which must be less than <code>value_array-><link linkend="GValueArray.n-values">n_values</link></code>
   ValueArray* remove()(uint index_) nothrow {
      return g_value_array_remove(&this, index_);
   }

   // DEPRECATED (v2.32) method: sort - Use #GArray and g_array_sort().
   // Sort @value_array using @compare_func to compare the elements according to
   // the semantics of #GCompareFunc.
   // 
   // The current implementation uses Quick-Sort as sorting algorithm.
   // RETURNS: the #GValueArray passed in as @value_array
   // <compare_func>: function to compare elements
   ValueArray* sort()(GLib2.CompareFunc compare_func) nothrow {
      return g_value_array_sort(&this, compare_func);
   }

   // DEPRECATED (v2.32) method: sort_with_data - Use #GArray and g_array_sort_with_data().
   // Sort @value_array using @compare_func to compare the elements according
   // to the semantics of #GCompareDataFunc.
   // 
   // The current implementation uses Quick-Sort as sorting algorithm.
   // RETURNS: the #GValueArray passed in as @value_array
   // <compare_func>: function to compare elements
   // <user_data>: extra data argument provided for @compare_func
   ValueArray* sort_with_data(AT0)(GLib2.CompareDataFunc compare_func, AT0 /*void*/ user_data) nothrow {
      return g_value_array_sort_with_data(&this, compare_func, UpCast!(void*)(user_data));
   }
}


// The type of value transformation functions which can be registered with
// g_value_register_transform_func().
// <src_value>: Source value.
// <dest_value>: Target value.
extern (C) alias void function (Value* src_value, Value* dest_value) nothrow ValueTransform;


// A #GWeakNotify function can be added to an object as a callback that gets
// triggered when the object is finalized. Since the object is already being
// finalized when the #GWeakNotify is called, there's not much you could do 
// with the object, apart from e.g. using its address as hash-index or the like.
// <data>: data that was provided when the weak reference was established
// <where_the_object_was>: the object being finalized
extern (C) alias void function (void* data, Object* where_the_object_was) nothrow WeakNotify;


// A structure containing a weak reference to a #GObject.  It can either
// be empty (i.e. point to %NULL), or point to an object for as long as
// at least one "strong" reference to that object exists. Before the
// object's #GObjectClass.dispose method is called, every #GWeakRef
// associated with becomes empty (i.e. points to %NULL).
// 
// Like #GValue, #GWeakRef can be statically allocated, stack- or
// heap-allocated, or embedded in larger structures.
// 
// Unlike g_object_weak_ref() and g_object_add_weak_pointer(), this weak
// reference is thread-safe: converting a weak pointer to a reference is
// atomic with respect to invalidation of weak pointers to destroyed
// objects.
// 
// If the object's #GObjectClass.dispose method results in additional
// references to the object being held, any #GWeakRef<!-- -->s taken
// before it was disposed will continue to point to %NULL.  If
// #GWeakRef<!-- -->s are taken after the object is disposed and
// re-referenced, they will continue to point to it until its refcount
// goes back to zero, at which point they too will be invalidated.
struct WeakRef {


   union priv {
      void* p;
   }


   // Unintrospectable method: clear() / g_weak_ref_clear()
   // VERSION: 2.32
   // Frees resources associated with a non-statically-allocated #GWeakRef.
   // After this call, the #GWeakRef is left in an undefined state.
   // 
   // You should only call this on a #GWeakRef that previously had
   // g_weak_ref_init() called on it.
   void clear()() nothrow {
      g_weak_ref_clear(&this);
   }

   // Unintrospectable method: get() / g_weak_ref_get()
   // VERSION: 2.32
   // If @weak_ref is not empty, atomically acquire a strong
   // reference to the object it points to, and return that reference.
   // 
   // This function is needed because of the potential race between taking
   // the pointer value and g_object_ref() on it, if the object was losing
   // its last reference at the same time in a different thread.
   // 
   // The caller should release the resulting reference in the usual way,
   // by using g_object_unref().
   // 
   // by @weak_ref, or %NULL if it was empty
   // RETURNS: the object pointed to
   Object* /*new*/ get()() nothrow {
      return g_weak_ref_get(&this);
   }

   // Unintrospectable method: init() / g_weak_ref_init()
   // VERSION: 2.32
   // Initialise a non-statically-allocated #GWeakRef.
   // 
   // This function also calls g_weak_ref_set() with @object on the
   // freshly-initialised weak reference.
   // 
   // This function should always be matched with a call to
   // g_weak_ref_clear().  It is not necessary to use this function for a
   // #GWeakRef in static storage because it will already be
   // properly initialised.  Just use g_weak_ref_set() directly.
   // <object>: a #GObject or %NULL
   void init(AT0)(AT0 /*void*/ object=null) nothrow {
      g_weak_ref_init(&this, UpCast!(void*)(object));
   }

   // Unintrospectable method: set() / g_weak_ref_set()
   // VERSION: 2.32
   // Change the object to which @weak_ref points, or set it to
   // %NULL.
   // 
   // You must own a strong reference on @object while calling this
   // function.
   // <object>: a #GObject or %NULL
   void set(AT0)(AT0 /*void*/ object=null) nothrow {
      g_weak_ref_set(&this, UpCast!(void*)(object));
   }
}

union _Value__data__union {
   int v_int;
   uint v_uint;
   c_long v_long;
   c_ulong v_ulong;
   long v_int64;
   ulong v_uint64;
   float v_float;
   double v_double;
   void* v_pointer;
}


// Unintrospectable function: boxed_copy() / g_boxed_copy()
// Provide a copy of a boxed structure @src_boxed which is of type @boxed_type.
// RETURNS: The newly created copy of the boxed structure.
// <boxed_type>: The type of @src_boxed.
// <src_boxed>: The boxed structure to be copied.
static void* boxed_copy(AT0)(Type boxed_type, AT0 /*const(void)*/ src_boxed) nothrow {
   return g_boxed_copy(boxed_type, UpCast!(const(void)*)(src_boxed));
}


// Free the boxed structure @boxed which is of type @boxed_type.
// <boxed_type>: The type of @boxed.
// <boxed>: The boxed structure to be freed.
static void boxed_free(AT0)(Type boxed_type, AT0 /*void*/ boxed) nothrow {
   g_boxed_free(boxed_type, UpCast!(void*)(boxed));
}


// Unintrospectable function: boxed_type_register_static() / g_boxed_type_register_static()
// This function creates a new %G_TYPE_BOXED derived type id for a new
// boxed type with name @name. Boxed type handling functions have to be
// provided to copy and free opaque boxed structures of this type.
// RETURNS: New %G_TYPE_BOXED derived type id for @name.
// <name>: Name of the new boxed type.
// <boxed_copy>: Boxed structure copy function.
// <boxed_free>: Boxed structure free function.
static Type boxed_type_register_static(AT0)(AT0 /*char*/ name, BoxedCopyFunc boxed_copy, BoxedFreeFunc boxed_free) nothrow {
   return g_boxed_type_register_static(toCString!(char*)(name), boxed_copy, boxed_free);
}

// MOVED TO: CClosure.marshal_BOOLEAN__BOXED_BOXED
static void cclosure_marshal_BOOLEAN__BOXED_BOXED(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_BOOLEAN__BOXED_BOXED(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_BOOLEAN__FLAGS
// A marshaller for a #GCClosure with a callback of type
// <literal>gboolean (*callback) (gpointer instance, gint arg1, gpointer user_data)</literal> where the #gint parameter
// denotes a flags type.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: a #GValue which can store the returned #gboolean
// <n_param_values>: 2
// <param_values>: a #GValue array holding instance and arg1
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_BOOLEAN__FLAGS(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_BOOLEAN__FLAGS(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_STRING__OBJECT_POINTER
// A marshaller for a #GCClosure with a callback of type
// <literal>gchar* (*callback) (gpointer instance, GObject *arg1, gpointer arg2, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: a #GValue, which can store the returned string
// <n_param_values>: 3
// <param_values>: a #GValue array holding instance, arg1 and arg2
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_STRING__OBJECT_POINTER(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_STRING__OBJECT_POINTER(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__BOOLEAN
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, gboolean arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #gboolean parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__BOOLEAN(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__BOOLEAN(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__BOXED
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, GBoxed *arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #GBoxed* parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__BOXED(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__BOXED(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__CHAR
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, gchar arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #gchar parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__CHAR(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__CHAR(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__DOUBLE
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, gdouble arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #gdouble parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__DOUBLE(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__DOUBLE(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__ENUM
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, gint arg1, gpointer user_data)</literal> where the #gint parameter denotes an enumeration type..
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the enumeration parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__ENUM(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__ENUM(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__FLAGS
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, gint arg1, gpointer user_data)</literal> where the #gint parameter denotes a flags type.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the flags parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__FLAGS(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__FLAGS(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__FLOAT
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, gfloat arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #gfloat parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__FLOAT(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__FLOAT(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__INT
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, gint arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #gint parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__INT(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__INT(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__LONG
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, glong arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #glong parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__LONG(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__LONG(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__OBJECT
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, GObject *arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #GObject* parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__OBJECT(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__OBJECT(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__PARAM
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, GParamSpec *arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #GParamSpec* parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__PARAM(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__PARAM(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__POINTER
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, gpointer arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #gpointer parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__POINTER(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__POINTER(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__STRING
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, const gchar *arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #gchar* parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__STRING(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__STRING(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__UCHAR
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, guchar arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #guchar parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__UCHAR(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__UCHAR(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__UINT
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, guint arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #guint parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__UINT(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__UINT(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__UINT_POINTER
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, guint arg1, gpointer arg2, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 3
// <param_values>: a #GValue array holding instance, arg1 and arg2
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__UINT_POINTER(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__UINT_POINTER(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__ULONG
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, gulong arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #gulong parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__ULONG(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__ULONG(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// VERSION: 2.26
// MOVED TO: CClosure.marshal_VOID__VARIANT
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, GVariant *arg1, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 2
// <param_values>: a #GValue array holding the instance and the #GVariant* parameter
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__VARIANT(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__VARIANT(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// MOVED TO: CClosure.marshal_VOID__VOID
// A marshaller for a #GCClosure with a callback of type
// <literal>void (*callback) (gpointer instance, gpointer user_data)</literal>.
// <closure>: the #GClosure to which the marshaller belongs
// <return_value>: ignored
// <n_param_values>: 1
// <param_values>: a #GValue array holding only the instance
// <invocation_hint>: the invocation hint given as the last argument to g_closure_invoke()
// <marshal_data>: additional data specified when registering the marshaller
static void cclosure_marshal_VOID__VOID(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_value, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_VOID__VOID(UpCast!(Closure*)(closure), UpCast!(Value*)(return_value), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// VERSION: 2.30
// MOVED TO: CClosure.marshal_generic
// A generic marshaller function implemented via <ulink
// url="http://sourceware.org/libffi/">libffi</ulink>.
// <closure>: A #GClosure.
// <return_gvalue>: A #GValue to store the return value. May be %NULL if the callback of closure doesn't return a value.
// <n_param_values>: The length of the @param_values array.
// <param_values>: An array of #GValue<!-- -->s holding the arguments on which to invoke the callback of closure.
// <invocation_hint>: The invocation hint given as the last argument to g_closure_invoke().
// <marshal_data>: Additional data specified when registering the marshaller, see g_closure_set_marshal() and g_closure_set_meta_marshal()
static void cclosure_marshal_generic(AT0, AT1, AT2, AT3, AT4)(AT0 /*Closure*/ closure, AT1 /*Value*/ return_gvalue, uint n_param_values, AT2 /*Value*/ param_values, AT3 /*void*/ invocation_hint, AT4 /*void*/ marshal_data) nothrow {
   g_cclosure_marshal_generic(UpCast!(Closure*)(closure), UpCast!(Value*)(return_gvalue), n_param_values, UpCast!(Value*)(param_values), UpCast!(void*)(invocation_hint), UpCast!(void*)(marshal_data));
}


// Unintrospectable function: cclosure_new() / g_cclosure_new()
// MOVED TO: CClosure.new
// Creates a new closure which invokes @callback_func with @user_data as
// the last parameter.
// RETURNS: a new #GCClosure
// <callback_func>: the function to invoke
// <user_data>: user data to pass to @callback_func
// <destroy_data>: destroy notify to be called when @user_data is no longer used
static Closure* /*new*/ cclosure_new(AT0)(Callback callback_func, AT0 /*void*/ user_data, ClosureNotify destroy_data) nothrow {
   return g_cclosure_new(callback_func, UpCast!(void*)(user_data), destroy_data);
}


// Unintrospectable function: cclosure_new_object() / g_cclosure_new_object()
// MOVED TO: CClosure.new_object
// A variant of g_cclosure_new() which uses @object as @user_data and
// calls g_object_watch_closure() on @object and the created
// closure. This function is useful when you have a callback closely
// associated with a #GObject, and want the callback to no longer run
// after the object is is freed.
// RETURNS: a new #GCClosure
// <callback_func>: the function to invoke
// <object>: a #GObject pointer to pass to @callback_func
static Closure* /*new*/ cclosure_new_object(AT0)(Callback callback_func, AT0 /*Object*/ object) nothrow {
   return g_cclosure_new_object(callback_func, UpCast!(Object*)(object));
}


// Unintrospectable function: cclosure_new_object_swap() / g_cclosure_new_object_swap()
// MOVED TO: CClosure.new_object_swap
// A variant of g_cclosure_new_swap() which uses @object as @user_data
// and calls g_object_watch_closure() on @object and the created
// closure. This function is useful when you have a callback closely
// associated with a #GObject, and want the callback to no longer run
// after the object is is freed.
// RETURNS: a new #GCClosure
// <callback_func>: the function to invoke
// <object>: a #GObject pointer to pass to @callback_func
static Closure* /*new*/ cclosure_new_object_swap(AT0)(Callback callback_func, AT0 /*Object*/ object) nothrow {
   return g_cclosure_new_object_swap(callback_func, UpCast!(Object*)(object));
}


// Unintrospectable function: cclosure_new_swap() / g_cclosure_new_swap()
// MOVED TO: CClosure.new_swap
// Creates a new closure which invokes @callback_func with @user_data as
// the first parameter.
// RETURNS: a new #GCClosure
// <callback_func>: the function to invoke
// <user_data>: user data to pass to @callback_func
// <destroy_data>: destroy notify to be called when @user_data is no longer used
static Closure* /*new*/ cclosure_new_swap(AT0)(Callback callback_func, AT0 /*void*/ user_data, ClosureNotify destroy_data) nothrow {
   return g_cclosure_new_swap(callback_func, UpCast!(void*)(user_data), destroy_data);
}


// Unintrospectable function: clear_object() / g_clear_object()
// VERSION: 2.28
// Clears a reference to a #GObject.
// 
// @object_ptr must not be %NULL.
// 
// If the reference is %NULL then this function does nothing.
// Otherwise, the reference count of the object is decreased and the
// pointer is set to %NULL.
// 
// This function is threadsafe and modifies the pointer atomically,
// using memory barriers where needed.
// 
// A macro is also included that allows this function to be used without
// pointer casts.
// <object_ptr>: a pointer to a #GObject reference
static void clear_object(AT0)(AT0 /*Object**/ object_ptr) nothrow {
   g_clear_object(UpCast!(Object**)(object_ptr));
}


// This function is meant to be called from the <literal>complete_type_info</literal>
// function of a #GTypePlugin implementation, as in the following
// example:
// 
// |[
// static void
// my_enum_complete_type_info (GTypePlugin     *plugin,
// GType            g_type,
// GTypeInfo       *info,
// GTypeValueTable *value_table)
// {
// static const GEnumValue values[] = {
// { MY_ENUM_FOO, "MY_ENUM_FOO", "foo" },
// { MY_ENUM_BAR, "MY_ENUM_BAR", "bar" },
// { 0, NULL, NULL }
// };
// 
// g_enum_complete_type_info (type, info, values);
// }
// ]|
// <g_enum_type>: the type identifier of the type being completed
// <info>: the #GTypeInfo struct to be filled in
// <const_values>: An array of #GEnumValue structs for the possible enumeration values. The array is terminated by a struct with all members being 0.
static void enum_complete_type_info(AT0, AT1)(Type g_enum_type, AT0 /*GTypeInfo*/ info, AT1 /*EnumValue*/ const_values) nothrow {
   g_enum_complete_type_info(g_enum_type, UpCast!(GTypeInfo*)(info), UpCast!(EnumValue*)(const_values));
}


// Unintrospectable function: enum_get_value() / g_enum_get_value()
// Returns the #GEnumValue for a value.
// 
// member of the enumeration
// RETURNS: the #GEnumValue for @value, or %NULL if @value is not a
// <enum_class>: a #GEnumClass
// <value>: the value to look up
static EnumValue* enum_get_value(AT0)(AT0 /*EnumClass*/ enum_class, int value) nothrow {
   return g_enum_get_value(UpCast!(EnumClass*)(enum_class), value);
}


// Unintrospectable function: enum_get_value_by_name() / g_enum_get_value_by_name()
// Looks up a #GEnumValue by name.
// 
// enumeration doesn't have a member with that name
// RETURNS: the #GEnumValue with name @name, or %NULL if the
// <enum_class>: a #GEnumClass
// <name>: the name to look up
static EnumValue* enum_get_value_by_name(AT0, AT1)(AT0 /*EnumClass*/ enum_class, AT1 /*char*/ name) nothrow {
   return g_enum_get_value_by_name(UpCast!(EnumClass*)(enum_class), toCString!(char*)(name));
}


// Unintrospectable function: enum_get_value_by_nick() / g_enum_get_value_by_nick()
// Looks up a #GEnumValue by nickname.
// 
// enumeration doesn't have a member with that nickname
// RETURNS: the #GEnumValue with nickname @nick, or %NULL if the
// <enum_class>: a #GEnumClass
// <nick>: the nickname to look up
static EnumValue* enum_get_value_by_nick(AT0, AT1)(AT0 /*EnumClass*/ enum_class, AT1 /*char*/ nick) nothrow {
   return g_enum_get_value_by_nick(UpCast!(EnumClass*)(enum_class), toCString!(char*)(nick));
}


// Registers a new static enumeration type with the name @name.
// 
// It is normally more convenient to let <link
// linkend="glib-mkenums">glib-mkenums</link> generate a
// my_enum_get_type() function from a usual C enumeration definition
// than to write one yourself using g_enum_register_static().
// RETURNS: The new type identifier.
// <name>: A nul-terminated string used as the name of the new type.
// <const_static_values>: An array of #GEnumValue structs for the possible enumeration values. The array is terminated by a struct with all members being 0. GObject keeps a reference to the data, so it cannot be stack-allocated.
static Type enum_register_static(AT0, AT1)(AT0 /*char*/ name, AT1 /*EnumValue*/ const_static_values) nothrow {
   return g_enum_register_static(toCString!(char*)(name), UpCast!(EnumValue*)(const_static_values));
}


// This function is meant to be called from the complete_type_info()
// function of a #GTypePlugin implementation, see the example for
// g_enum_complete_type_info() above.
// <g_flags_type>: the type identifier of the type being completed
// <info>: the #GTypeInfo struct to be filled in
// <const_values>: An array of #GFlagsValue structs for the possible enumeration values. The array is terminated by a struct with all members being 0.
static void flags_complete_type_info(AT0, AT1)(Type g_flags_type, AT0 /*GTypeInfo*/ info, AT1 /*FlagsValue*/ const_values) nothrow {
   g_flags_complete_type_info(g_flags_type, UpCast!(GTypeInfo*)(info), UpCast!(FlagsValue*)(const_values));
}


// Unintrospectable function: flags_get_first_value() / g_flags_get_first_value()
// Returns the first #GFlagsValue which is set in @value.
// 
// none is set
// RETURNS: the first #GFlagsValue which is set in @value, or %NULL if
// <flags_class>: a #GFlagsClass
// <value>: the value
static FlagsValue* flags_get_first_value(AT0)(AT0 /*FlagsClass*/ flags_class, uint value) nothrow {
   return g_flags_get_first_value(UpCast!(FlagsClass*)(flags_class), value);
}


// Unintrospectable function: flags_get_value_by_name() / g_flags_get_value_by_name()
// Looks up a #GFlagsValue by name.
// 
// flag with that name
// RETURNS: the #GFlagsValue with name @name, or %NULL if there is no
// <flags_class>: a #GFlagsClass
// <name>: the name to look up
static FlagsValue* flags_get_value_by_name(AT0, AT1)(AT0 /*FlagsClass*/ flags_class, AT1 /*char*/ name) nothrow {
   return g_flags_get_value_by_name(UpCast!(FlagsClass*)(flags_class), toCString!(char*)(name));
}


// Unintrospectable function: flags_get_value_by_nick() / g_flags_get_value_by_nick()
// Looks up a #GFlagsValue by nickname.
// 
// no flag with that nickname
// RETURNS: the #GFlagsValue with nickname @nick, or %NULL if there is
// <flags_class>: a #GFlagsClass
// <nick>: the nickname to look up
static FlagsValue* flags_get_value_by_nick(AT0, AT1)(AT0 /*FlagsClass*/ flags_class, AT1 /*char*/ nick) nothrow {
   return g_flags_get_value_by_nick(UpCast!(FlagsClass*)(flags_class), toCString!(char*)(nick));
}


// Registers a new static flags type with the name @name.
// 
// It is normally more convenient to let <link
// linkend="glib-mkenums">glib-mkenums</link> generate a
// my_flags_get_type() function from a usual C enumeration definition
// than to write one yourself using g_flags_register_static().
// RETURNS: The new type identifier.
// <name>: A nul-terminated string used as the name of the new type.
// <const_static_values>: An array of #GFlagsValue structs for the possible flags values. The array is terminated by a struct with all members being 0. GObject keeps a reference to the data, so it cannot be stack-allocated.
static Type flags_register_static(AT0, AT1)(AT0 /*char*/ name, AT1 /*FlagsValue*/ const_static_values) nothrow {
   return g_flags_register_static(toCString!(char*)(name), UpCast!(FlagsValue*)(const_static_values));
}

static Type gtype_get_type()() nothrow {
   return g_gtype_get_type();
}


// Unintrospectable function: param_spec_boolean() / g_param_spec_boolean()
// Creates a new #GParamSpecBoolean instance specifying a %G_TYPE_BOOLEAN
// property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_boolean(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, int default_value, ParamFlags flags) nothrow {
   return g_param_spec_boolean(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), default_value, flags);
}


// Unintrospectable function: param_spec_boxed() / g_param_spec_boxed()
// Creates a new #GParamSpecBoxed instance specifying a %G_TYPE_BOXED
// derived property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <boxed_type>: %G_TYPE_BOXED derived type of this property
// <flags>: flags for the property specified
static ParamSpec* param_spec_boxed(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, Type boxed_type, ParamFlags flags) nothrow {
   return g_param_spec_boxed(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), boxed_type, flags);
}


// Unintrospectable function: param_spec_char() / g_param_spec_char()
// Creates a new #GParamSpecChar instance specifying a %G_TYPE_CHAR property.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <minimum>: minimum value for the property specified
// <maximum>: maximum value for the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_char(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, byte minimum, byte maximum, byte default_value, ParamFlags flags) nothrow {
   return g_param_spec_char(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), minimum, maximum, default_value, flags);
}


// Unintrospectable function: param_spec_double() / g_param_spec_double()
// Creates a new #GParamSpecDouble instance specifying a %G_TYPE_DOUBLE
// property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <minimum>: minimum value for the property specified
// <maximum>: maximum value for the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_double(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, double minimum, double maximum, double default_value, ParamFlags flags) nothrow {
   return g_param_spec_double(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), minimum, maximum, default_value, flags);
}


// Unintrospectable function: param_spec_enum() / g_param_spec_enum()
// Creates a new #GParamSpecEnum instance specifying a %G_TYPE_ENUM
// property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <enum_type>: a #GType derived from %G_TYPE_ENUM
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_enum(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, Type enum_type, int default_value, ParamFlags flags) nothrow {
   return g_param_spec_enum(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), enum_type, default_value, flags);
}


// Unintrospectable function: param_spec_flags() / g_param_spec_flags()
// Creates a new #GParamSpecFlags instance specifying a %G_TYPE_FLAGS
// property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <flags_type>: a #GType derived from %G_TYPE_FLAGS
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_flags(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, Type flags_type, uint default_value, ParamFlags flags) nothrow {
   return g_param_spec_flags(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), flags_type, default_value, flags);
}


// Unintrospectable function: param_spec_float() / g_param_spec_float()
// Creates a new #GParamSpecFloat instance specifying a %G_TYPE_FLOAT property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <minimum>: minimum value for the property specified
// <maximum>: maximum value for the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_float(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, float minimum, float maximum, float default_value, ParamFlags flags) nothrow {
   return g_param_spec_float(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), minimum, maximum, default_value, flags);
}


// Unintrospectable function: param_spec_gtype() / g_param_spec_gtype()
// VERSION: 2.10
// Creates a new #GParamSpecGType instance specifying a
// %G_TYPE_GTYPE property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <is_a_type>: a #GType whose subtypes are allowed as values of the property (use %G_TYPE_NONE for any type)
// <flags>: flags for the property specified
static ParamSpec* param_spec_gtype(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, Type is_a_type, ParamFlags flags) nothrow {
   return g_param_spec_gtype(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), is_a_type, flags);
}


// Unintrospectable function: param_spec_int() / g_param_spec_int()
// Creates a new #GParamSpecInt instance specifying a %G_TYPE_INT property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <minimum>: minimum value for the property specified
// <maximum>: maximum value for the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_int(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, int minimum, int maximum, int default_value, ParamFlags flags) nothrow {
   return g_param_spec_int(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), minimum, maximum, default_value, flags);
}


// Unintrospectable function: param_spec_int64() / g_param_spec_int64()
// Creates a new #GParamSpecInt64 instance specifying a %G_TYPE_INT64 property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <minimum>: minimum value for the property specified
// <maximum>: maximum value for the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_int64(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, long minimum, long maximum, long default_value, ParamFlags flags) nothrow {
   return g_param_spec_int64(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), minimum, maximum, default_value, flags);
}


// Unintrospectable function: param_spec_long() / g_param_spec_long()
// Creates a new #GParamSpecLong instance specifying a %G_TYPE_LONG property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <minimum>: minimum value for the property specified
// <maximum>: maximum value for the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_long(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, c_long minimum, c_long maximum, c_long default_value, ParamFlags flags) nothrow {
   return g_param_spec_long(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), minimum, maximum, default_value, flags);
}


// Unintrospectable function: param_spec_object() / g_param_spec_object()
// Creates a new #GParamSpecBoxed instance specifying a %G_TYPE_OBJECT
// derived property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <object_type>: %G_TYPE_OBJECT derived type of this property
// <flags>: flags for the property specified
static ParamSpec* param_spec_object(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, Type object_type, ParamFlags flags) nothrow {
   return g_param_spec_object(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), object_type, flags);
}


// Unintrospectable function: param_spec_override() / g_param_spec_override()
// VERSION: 2.4
// Creates a new property of type #GParamSpecOverride. This is used
// to direct operations to another paramspec, and will not be directly
// useful unless you are implementing a new base type similar to GObject.
// RETURNS: the newly created #GParamSpec
// <name>: the name of the property.
// <overridden>: The property that is being overridden
static ParamSpec* param_spec_override(AT0, AT1)(AT0 /*char*/ name, AT1 /*ParamSpec*/ overridden) nothrow {
   return g_param_spec_override(toCString!(char*)(name), UpCast!(ParamSpec*)(overridden));
}


// Unintrospectable function: param_spec_param() / g_param_spec_param()
// Creates a new #GParamSpecParam instance specifying a %G_TYPE_PARAM
// property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <param_type>: a #GType derived from %G_TYPE_PARAM
// <flags>: flags for the property specified
static ParamSpec* param_spec_param(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, Type param_type, ParamFlags flags) nothrow {
   return g_param_spec_param(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), param_type, flags);
}


// Unintrospectable function: param_spec_pointer() / g_param_spec_pointer()
// Creates a new #GParamSpecPointer instance specifying a pointer property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_pointer(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, ParamFlags flags) nothrow {
   return g_param_spec_pointer(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), flags);
}


// MOVED TO: ParamSpecPool.new
// Creates a new #GParamSpecPool.
// 
// If @type_prefixing is %TRUE, lookups in the newly created pool will
// allow to specify the owner as a colon-separated prefix of the
// property name, like "GtkContainer:border-width". This feature is
// deprecated, so you should always set @type_prefixing to %FALSE.
// RETURNS: a newly allocated #GParamSpecPool.
// <type_prefixing>: Whether the pool will support type-prefixed property names.
static ParamSpecPool* param_spec_pool_new()(int type_prefixing) nothrow {
   return g_param_spec_pool_new(type_prefixing);
}


// Unintrospectable function: param_spec_string() / g_param_spec_string()
// Creates a new #GParamSpecString instance.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_string(AT0, AT1, AT2, AT3)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, AT3 /*char*/ default_value, ParamFlags flags) nothrow {
   return g_param_spec_string(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), toCString!(char*)(default_value), flags);
}


// Unintrospectable function: param_spec_uchar() / g_param_spec_uchar()
// Creates a new #GParamSpecUChar instance specifying a %G_TYPE_UCHAR property.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <minimum>: minimum value for the property specified
// <maximum>: maximum value for the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_uchar(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, ubyte minimum, ubyte maximum, ubyte default_value, ParamFlags flags) nothrow {
   return g_param_spec_uchar(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), minimum, maximum, default_value, flags);
}


// Unintrospectable function: param_spec_uint() / g_param_spec_uint()
// Creates a new #GParamSpecUInt instance specifying a %G_TYPE_UINT property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <minimum>: minimum value for the property specified
// <maximum>: maximum value for the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_uint(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, uint minimum, uint maximum, uint default_value, ParamFlags flags) nothrow {
   return g_param_spec_uint(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), minimum, maximum, default_value, flags);
}


// Unintrospectable function: param_spec_uint64() / g_param_spec_uint64()
// Creates a new #GParamSpecUInt64 instance specifying a %G_TYPE_UINT64
// property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <minimum>: minimum value for the property specified
// <maximum>: maximum value for the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_uint64(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, ulong minimum, ulong maximum, ulong default_value, ParamFlags flags) nothrow {
   return g_param_spec_uint64(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), minimum, maximum, default_value, flags);
}


// Unintrospectable function: param_spec_ulong() / g_param_spec_ulong()
// Creates a new #GParamSpecULong instance specifying a %G_TYPE_ULONG
// property.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <minimum>: minimum value for the property specified
// <maximum>: maximum value for the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_ulong(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, c_ulong minimum, c_ulong maximum, c_ulong default_value, ParamFlags flags) nothrow {
   return g_param_spec_ulong(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), minimum, maximum, default_value, flags);
}


// Unintrospectable function: param_spec_unichar() / g_param_spec_unichar()
// Creates a new #GParamSpecUnichar instance specifying a %G_TYPE_UINT
// property. #GValue structures for this property can be accessed with
// g_value_set_uint() and g_value_get_uint().
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <default_value>: default value for the property specified
// <flags>: flags for the property specified
static ParamSpec* param_spec_unichar(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, dchar default_value, ParamFlags flags) nothrow {
   return g_param_spec_unichar(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), default_value, flags);
}


// Unintrospectable function: param_spec_value_array() / g_param_spec_value_array()
// Creates a new #GParamSpecValueArray instance specifying a
// %G_TYPE_VALUE_ARRAY property. %G_TYPE_VALUE_ARRAY is a
// %G_TYPE_BOXED type, as such, #GValue structures for this property
// can be accessed with g_value_set_boxed() and g_value_get_boxed().
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: a newly created parameter specification
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <element_spec>: a #GParamSpec describing the elements contained in arrays of this property, may be %NULL
// <flags>: flags for the property specified
static ParamSpec* param_spec_value_array(AT0, AT1, AT2, AT3)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, AT3 /*ParamSpec*/ element_spec, ParamFlags flags) nothrow {
   return g_param_spec_value_array(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), UpCast!(ParamSpec*)(element_spec), flags);
}


// Unintrospectable function: param_spec_variant() / g_param_spec_variant()
// VERSION: 2.26
// Creates a new #GParamSpecVariant instance specifying a #GVariant
// property.
// 
// If @default_value is floating, it is consumed.
// 
// See g_param_spec_internal() for details on property names.
// RETURNS: the newly created #GParamSpec
// <name>: canonical name of the property specified
// <nick>: nick name for the property specified
// <blurb>: description of the property specified
// <type>: a #GVariantType
// <default_value>: a #GVariant of type @type to use as the default value, or %NULL
// <flags>: flags for the property specified
static ParamSpec* param_spec_variant(AT0, AT1, AT2, AT3, AT4)(AT0 /*char*/ name, AT1 /*char*/ nick, AT2 /*char*/ blurb, AT3 /*GLib2.VariantType*/ type, AT4 /*GLib2.Variant*/ default_value, ParamFlags flags) nothrow {
   return g_param_spec_variant(toCString!(char*)(name), toCString!(char*)(nick), toCString!(char*)(blurb), UpCast!(GLib2.VariantType*)(type), UpCast!(GLib2.Variant*)(default_value), flags);
}


// Registers @name as the name of a new static type derived from
// #G_TYPE_PARAM. The type system uses the information contained in
// the #GParamSpecTypeInfo structure pointed to by @info to manage the
// #GParamSpec type and its instances.
// RETURNS: The new type identifier.
// <name>: 0-terminated string used as the name of the new #GParamSpec type.
// <pspec_info>: The #GParamSpecTypeInfo for this #GParamSpec type.
static Type param_type_register_static(AT0, AT1)(AT0 /*char*/ name, AT1 /*ParamSpecTypeInfo*/ pspec_info) nothrow {
   return g_param_type_register_static(toCString!(char*)(name), UpCast!(ParamSpecTypeInfo*)(pspec_info));
}


// Transforms @src_value into @dest_value if possible, and then
// validates @dest_value, in order for it to conform to @pspec.  If
// @strict_validation is %TRUE this function will only succeed if the
// transformed @dest_value complied to @pspec without modifications.
// 
// See also g_value_type_transformable(), g_value_transform() and
// g_param_value_validate().
// 
// %FALSE otherwise and @dest_value is left untouched.
// RETURNS: %TRUE if transformation and validation were successful,
// <pspec>: a valid #GParamSpec
// <src_value>: souce #GValue
// <dest_value>: destination #GValue of correct type for @pspec
// <strict_validation>: %TRUE requires @dest_value to conform to @pspec without modifications
static int param_value_convert(AT0, AT1, AT2)(AT0 /*ParamSpec*/ pspec, AT1 /*Value*/ src_value, AT2 /*Value*/ dest_value, int strict_validation) nothrow {
   return g_param_value_convert(UpCast!(ParamSpec*)(pspec), UpCast!(Value*)(src_value), UpCast!(Value*)(dest_value), strict_validation);
}


// Checks whether @value contains the default value as specified in @pspec.
// RETURNS: whether @value contains the canonical default for this @pspec
// <pspec>: a valid #GParamSpec
// <value>: a #GValue of correct type for @pspec
static int param_value_defaults(AT0, AT1)(AT0 /*ParamSpec*/ pspec, AT1 /*Value*/ value) nothrow {
   return g_param_value_defaults(UpCast!(ParamSpec*)(pspec), UpCast!(Value*)(value));
}


// Sets @value to its default value as specified in @pspec.
// <pspec>: a valid #GParamSpec
// <value>: a #GValue of correct type for @pspec
static void param_value_set_default(AT0, AT1)(AT0 /*ParamSpec*/ pspec, AT1 /*Value*/ value) nothrow {
   g_param_value_set_default(UpCast!(ParamSpec*)(pspec), UpCast!(Value*)(value));
}


// Ensures that the contents of @value comply with the specifications
// set out by @pspec. For example, a #GParamSpecInt might require
// that integers stored in @value may not be smaller than -42 and not be
// greater than +42. If @value contains an integer outside of this range,
// it is modified accordingly, so the resulting value will fit into the
// range -42 .. +42.
// RETURNS: whether modifying @value was necessary to ensure validity
// <pspec>: a valid #GParamSpec
// <value>: a #GValue of correct type for @pspec
static int param_value_validate(AT0, AT1)(AT0 /*ParamSpec*/ pspec, AT1 /*Value*/ value) nothrow {
   return g_param_value_validate(UpCast!(ParamSpec*)(pspec), UpCast!(Value*)(value));
}


// Compares @value1 with @value2 according to @pspec, and return -1, 0 or +1,
// if @value1 is found to be less than, equal to or greater than @value2,
// respectively.
// RETURNS: -1, 0 or +1, for a less than, equal to or greater than result
// <pspec>: a valid #GParamSpec
// <value1>: a #GValue of correct type for @pspec
// <value2>: a #GValue of correct type for @pspec
static int param_values_cmp(AT0, AT1, AT2)(AT0 /*ParamSpec*/ pspec, AT1 /*Value*/ value1, AT2 /*Value*/ value2) nothrow {
   return g_param_values_cmp(UpCast!(ParamSpec*)(pspec), UpCast!(Value*)(value1), UpCast!(Value*)(value2));
}


// Creates a new %G_TYPE_POINTER derived type id for a new
// pointer type with name @name.
// RETURNS: a new %G_TYPE_POINTER derived type id for @name.
// <name>: the name of the new pointer type.
static Type pointer_type_register_static(AT0)(AT0 /*char*/ name) nothrow {
   return g_pointer_type_register_static(toCString!(char*)(name));
}


// VERSION: 2.28
// A predefined #GSignalAccumulator for signals intended to be used as a
// hook for application code to provide a particular value.  Usually
// only one such value is desired and multiple handlers for the same
// signal don't make much sense (except for the case of the default
// handler defined in the class structure, in which case you will
// usually want the signal connection to override the class handler).
// 
// This accumulator will use the return value from the first signal
// handler that is run as the return value for the signal and not run
// any further handlers (ie: the first handler "wins").
// RETURNS: standard #GSignalAccumulator result
// <ihint>: standard #GSignalAccumulator parameter
// <return_accu>: standard #GSignalAccumulator parameter
// <handler_return>: standard #GSignalAccumulator parameter
// <dummy>: standard #GSignalAccumulator parameter
static int signal_accumulator_first_wins(AT0, AT1, AT2, AT3)(AT0 /*SignalInvocationHint*/ ihint, AT1 /*Value*/ return_accu, AT2 /*Value*/ handler_return, AT3 /*void*/ dummy) nothrow {
   return g_signal_accumulator_first_wins(UpCast!(SignalInvocationHint*)(ihint), UpCast!(Value*)(return_accu), UpCast!(Value*)(handler_return), UpCast!(void*)(dummy));
}


// VERSION: 2.4
// A predefined #GSignalAccumulator for signals that return a
// boolean values. The behavior that this accumulator gives is
// that a return of %TRUE stops the signal emission: no further
// callbacks will be invoked, while a return of %FALSE allows
// the emission to continue. The idea here is that a %TRUE return
// indicates that the callback <emphasis>handled</emphasis> the signal,
// and no further handling is needed.
// RETURNS: standard #GSignalAccumulator result
// <ihint>: standard #GSignalAccumulator parameter
// <return_accu>: standard #GSignalAccumulator parameter
// <handler_return>: standard #GSignalAccumulator parameter
// <dummy>: standard #GSignalAccumulator parameter
static int signal_accumulator_true_handled(AT0, AT1, AT2, AT3)(AT0 /*SignalInvocationHint*/ ihint, AT1 /*Value*/ return_accu, AT2 /*Value*/ handler_return, AT3 /*void*/ dummy) nothrow {
   return g_signal_accumulator_true_handled(UpCast!(SignalInvocationHint*)(ihint), UpCast!(Value*)(return_accu), UpCast!(Value*)(handler_return), UpCast!(void*)(dummy));
}


// Adds an emission hook for a signal, which will get called for any emission
// of that signal, independent of the instance. This is possible only
// for signals which don't have #G_SIGNAL_NO_HOOKS flag set.
// RETURNS: the hook id, for later use with g_signal_remove_emission_hook().
// <signal_id>: the signal identifier, as returned by g_signal_lookup().
// <detail>: the detail on which to call the hook.
// <hook_func>: a #GSignalEmissionHook function.
// <hook_data>: user data for @hook_func.
// <data_destroy>: a #GDestroyNotify for @hook_data.
static c_ulong signal_add_emission_hook(AT0)(uint signal_id, GLib2.Quark detail, SignalEmissionHook hook_func, AT0 /*void*/ hook_data, GLib2.DestroyNotify data_destroy) nothrow {
   return g_signal_add_emission_hook(signal_id, detail, hook_func, UpCast!(void*)(hook_data), data_destroy);
}


// Calls the original class closure of a signal. This function should only
// be called from an overridden class closure; see
// g_signal_override_class_closure() and
// g_signal_override_class_handler().
// <instance_and_params>: (array) the argument list of the signal emission. The first element in the array is a #GValue for the instance the signal is being emitted on. The rest are any arguments to be passed to the signal.
// <return_value>: Location for the return value.
static void signal_chain_from_overridden(AT0, AT1)(AT0 /*Value*/ instance_and_params, AT1 /*Value*/ return_value) nothrow {
   g_signal_chain_from_overridden(UpCast!(Value*)(instance_and_params), UpCast!(Value*)(return_value));
}


// Unintrospectable function: signal_chain_from_overridden_handler() / g_signal_chain_from_overridden_handler()
// VERSION: 2.18
// Calls the original class closure of a signal. This function should
// only be called from an overridden class closure; see
// g_signal_override_class_closure() and
// g_signal_override_class_handler().
// <instance>: the instance the signal is being emitted on.
alias g_signal_chain_from_overridden_handler signal_chain_from_overridden_handler; // Variadic


// Connects a closure to a signal for a particular object.
// RETURNS: the handler id
// <instance>: the instance to connect to.
// <detailed_signal>: a string of the form "signal-name::detail".
// <closure>: the closure to connect.
// <after>: whether the handler should be called before or after the default handler of the signal.
static c_ulong signal_connect_closure(AT0, AT1, AT2)(AT0 /*void*/ instance, AT1 /*char*/ detailed_signal, AT2 /*Closure*/ closure, int after) nothrow {
   return g_signal_connect_closure(UpCast!(void*)(instance), toCString!(char*)(detailed_signal), UpCast!(Closure*)(closure), after);
}


// Connects a closure to a signal for a particular object.
// RETURNS: the handler id
// <instance>: the instance to connect to.
// <signal_id>: the id of the signal.
// <detail>: the detail.
// <closure>: the closure to connect.
// <after>: whether the handler should be called before or after the default handler of the signal.
static c_ulong signal_connect_closure_by_id(AT0, AT1)(AT0 /*void*/ instance, uint signal_id, GLib2.Quark detail, AT1 /*Closure*/ closure, int after) nothrow {
   return g_signal_connect_closure_by_id(UpCast!(void*)(instance), signal_id, detail, UpCast!(Closure*)(closure), after);
}


// Unintrospectable function: signal_connect_data() / g_signal_connect_data()
// Connects a #GCallback function to a signal for a particular object. Similar
// to g_signal_connect(), but allows to provide a #GClosureNotify for the data
// which will be called when the signal handler is disconnected and no longer
// used. Specify @connect_flags if you need <literal>..._after()</literal> or
// <literal>..._swapped()</literal> variants of this function.
// RETURNS: the handler id
// <instance>: the instance to connect to.
// <detailed_signal>: a string of the form "signal-name::detail".
// <c_handler>: the #GCallback to connect.
// <data>: data to pass to @c_handler calls.
// <destroy_data>: a #GClosureNotify for @data.
// <connect_flags>: a combination of #GConnectFlags.
static c_ulong signal_connect_data(AT0, AT1, AT2)(AT0 /*void*/ instance, AT1 /*char*/ detailed_signal, Callback c_handler, AT2 /*void*/ data, ClosureNotify destroy_data, ConnectFlags connect_flags) nothrow {
   return g_signal_connect_data(UpCast!(void*)(instance), toCString!(char*)(detailed_signal), c_handler, UpCast!(void*)(data), destroy_data, connect_flags);
}


// Unintrospectable function: signal_connect_object() / g_signal_connect_object()
// This is similar to g_signal_connect_data(), but uses a closure which
// ensures that the @gobject stays alive during the call to @c_handler
// by temporarily adding a reference count to @gobject.
// 
// Note that there is a bug in GObject that makes this function
// much less useful than it might seem otherwise. Once @gobject is
// disposed, the callback will no longer be called, but, the signal
// handler is <emphasis>not</emphasis> currently disconnected. If the
// @instance is itself being freed at the same time than this doesn't
// matter, since the signal will automatically be removed, but
// if @instance persists, then the signal handler will leak. You
// should not remove the signal yourself because in a future versions of
// GObject, the handler <emphasis>will</emphasis> automatically
// be disconnected.
// 
// It's possible to work around this problem in a way that will
// continue to work with future versions of GObject by checking
// that the signal handler is still connected before disconnected it:
// <informalexample><programlisting>
// if (g_signal_handler_is_connected (instance, id))
// g_signal_handler_disconnect (instance, id);
// </programlisting></informalexample>
// RETURNS: the handler id.
// <instance>: the instance to connect to.
// <detailed_signal>: a string of the form "signal-name::detail".
// <c_handler>: the #GCallback to connect.
// <gobject>: the object to pass as data to @c_handler.
// <connect_flags>: a combination of #GConnectFlags.
static c_ulong signal_connect_object(AT0, AT1, AT2)(AT0 /*void*/ instance, AT1 /*char*/ detailed_signal, Callback c_handler, AT2 /*void*/ gobject, ConnectFlags connect_flags) nothrow {
   return g_signal_connect_object(UpCast!(void*)(instance), toCString!(char*)(detailed_signal), c_handler, UpCast!(void*)(gobject), connect_flags);
}


// Unintrospectable function: signal_emit() / g_signal_emit()
// Emits a signal.
// 
// Note that g_signal_emit() resets the return value to the default
// if no handlers are connected, in contrast to g_signal_emitv().
// <instance>: the instance the signal is being emitted on.
// <signal_id>: the signal id
// <detail>: the detail
alias g_signal_emit signal_emit; // Variadic


// Unintrospectable function: signal_emit_by_name() / g_signal_emit_by_name()
// Emits a signal.
// 
// Note that g_signal_emit_by_name() resets the return value to the default
// if no handlers are connected, in contrast to g_signal_emitv().
// <instance>: the instance the signal is being emitted on.
// <detailed_signal>: a string of the form "signal-name::detail".
alias g_signal_emit_by_name signal_emit_by_name; // Variadic


// Unintrospectable function: signal_emit_valist() / g_signal_emit_valist()
// Emits a signal.
// 
// Note that g_signal_emit_valist() resets the return value to the default
// if no handlers are connected, in contrast to g_signal_emitv().
// <instance>: the instance the signal is being emitted on.
// <signal_id>: the signal id
// <detail>: the detail
// <var_args>: a list of parameters to be passed to the signal, followed by a location for the return value. If the return type of the signal is #G_TYPE_NONE, the return value location can be omitted.
static void signal_emit_valist(AT0)(AT0 /*void*/ instance, uint signal_id, GLib2.Quark detail, va_list var_args) nothrow {
   g_signal_emit_valist(UpCast!(void*)(instance), signal_id, detail, var_args);
}


// Emits a signal.
// 
// Note that g_signal_emitv() doesn't change @return_value if no handlers are
// connected, in contrast to g_signal_emit() and g_signal_emit_valist().
// <instance_and_params>: argument list for the signal emission. The first element in the array is a #GValue for the instance the signal is being emitted on. The rest are any arguments to be passed to the signal.
// <signal_id>: the signal id
// <detail>: the detail
// <return_value>: Location to store the return value of the signal emission.
static void signal_emitv(AT0, AT1)(AT0 /*Value*/ instance_and_params, uint signal_id, GLib2.Quark detail, AT1 /*Value*/ return_value) nothrow {
   g_signal_emitv(UpCast!(Value*)(instance_and_params), signal_id, detail, UpCast!(Value*)(return_value));
}


// Returns the invocation hint of the innermost signal emission of instance.
// RETURNS: the invocation hint of the innermost signal  emission.
// <instance>: the instance to query
static SignalInvocationHint* signal_get_invocation_hint(AT0)(AT0 /*void*/ instance) nothrow {
   return g_signal_get_invocation_hint(UpCast!(void*)(instance));
}


// Blocks a handler of an instance so it will not be called during any
// signal emissions unless it is unblocked again. Thus "blocking" a
// signal handler means to temporarily deactive it, a signal handler
// has to be unblocked exactly the same amount of times it has been
// blocked before to become active again.
// 
// The @handler_id has to be a valid signal handler id, connected to a
// signal of @instance.
// <instance>: The instance to block the signal handler of.
// <handler_id>: Handler id of the handler to be blocked.
static void signal_handler_block(AT0)(AT0 /*void*/ instance, c_ulong handler_id) nothrow {
   g_signal_handler_block(UpCast!(void*)(instance), handler_id);
}


// Disconnects a handler from an instance so it will not be called during
// any future or currently ongoing emissions of the signal it has been
// connected to. The @handler_id becomes invalid and may be reused.
// 
// The @handler_id has to be a valid signal handler id, connected to a
// signal of @instance.
// <instance>: The instance to remove the signal handler from.
// <handler_id>: Handler id of the handler to be disconnected.
static void signal_handler_disconnect(AT0)(AT0 /*void*/ instance, c_ulong handler_id) nothrow {
   g_signal_handler_disconnect(UpCast!(void*)(instance), handler_id);
}


// Finds the first signal handler that matches certain selection criteria.
// The criteria mask is passed as an OR-ed combination of #GSignalMatchType
// flags, and the criteria values are passed as arguments.
// The match @mask has to be non-0 for successful matches.
// If no handler was found, 0 is returned.
// RETURNS: A valid non-0 signal handler id for a successful match.
// <instance>: The instance owning the signal handler to be found.
// <mask>: Mask indicating which of @signal_id, @detail, @closure, @func and/or @data the handler has to match.
// <signal_id>: Signal the handler has to be connected to.
// <detail>: Signal detail the handler has to be connected to.
// <closure>: The closure the handler will invoke.
// <func>: The C closure callback of the handler (useless for non-C closures).
// <data>: The closure data of the handler's closure.
static c_ulong signal_handler_find(AT0, AT1, AT2, AT3)(AT0 /*void*/ instance, SignalMatchType mask, uint signal_id, GLib2.Quark detail, AT1 /*Closure*/ closure, AT2 /*void*/ func, AT3 /*void*/ data) nothrow {
   return g_signal_handler_find(UpCast!(void*)(instance), mask, signal_id, detail, UpCast!(Closure*)(closure), UpCast!(void*)(func), UpCast!(void*)(data));
}


// Returns whether @handler_id is the id of a handler connected to @instance.
// RETURNS: whether @handler_id identifies a handler connected to @instance.
// <instance>: The instance where a signal handler is sought.
// <handler_id>: the handler id.
static int signal_handler_is_connected(AT0)(AT0 /*void*/ instance, c_ulong handler_id) nothrow {
   return g_signal_handler_is_connected(UpCast!(void*)(instance), handler_id);
}


// Undoes the effect of a previous g_signal_handler_block() call.  A
// blocked handler is skipped during signal emissions and will not be
// invoked, unblocking it (for exactly the amount of times it has been
// blocked before) reverts its "blocked" state, so the handler will be
// recognized by the signal system and is called upon future or
// currently ongoing signal emissions (since the order in which
// handlers are called during signal emissions is deterministic,
// whether the unblocked handler in question is called as part of a
// currently ongoing emission depends on how far that emission has
// proceeded yet).
// 
// The @handler_id has to be a valid id of a signal handler that is
// connected to a signal of @instance and is currently blocked.
// <instance>: The instance to unblock the signal handler of.
// <handler_id>: Handler id of the handler to be unblocked.
static void signal_handler_unblock(AT0)(AT0 /*void*/ instance, c_ulong handler_id) nothrow {
   g_signal_handler_unblock(UpCast!(void*)(instance), handler_id);
}


// Blocks all handlers on an instance that match a certain selection criteria.
// The criteria mask is passed as an OR-ed combination of #GSignalMatchType
// flags, and the criteria values are passed as arguments.
// Passing at least one of the %G_SIGNAL_MATCH_CLOSURE, %G_SIGNAL_MATCH_FUNC
// or %G_SIGNAL_MATCH_DATA match flags is required for successful matches.
// If no handlers were found, 0 is returned, the number of blocked handlers
// otherwise.
// RETURNS: The number of handlers that matched.
// <instance>: The instance to block handlers from.
// <mask>: Mask indicating which of @signal_id, @detail, @closure, @func and/or @data the handlers have to match.
// <signal_id>: Signal the handlers have to be connected to.
// <detail>: Signal detail the handlers have to be connected to.
// <closure>: The closure the handlers will invoke.
// <func>: The C closure callback of the handlers (useless for non-C closures).
// <data>: The closure data of the handlers' closures.
static uint signal_handlers_block_matched(AT0, AT1, AT2, AT3)(AT0 /*void*/ instance, SignalMatchType mask, uint signal_id, GLib2.Quark detail, AT1 /*Closure*/ closure, AT2 /*void*/ func, AT3 /*void*/ data) nothrow {
   return g_signal_handlers_block_matched(UpCast!(void*)(instance), mask, signal_id, detail, UpCast!(Closure*)(closure), UpCast!(void*)(func), UpCast!(void*)(data));
}

static void signal_handlers_destroy(AT0)(AT0 /*void*/ instance) nothrow {
   g_signal_handlers_destroy(UpCast!(void*)(instance));
}


// Disconnects all handlers on an instance that match a certain
// selection criteria. The criteria mask is passed as an OR-ed
// combination of #GSignalMatchType flags, and the criteria values are
// passed as arguments.  Passing at least one of the
// %G_SIGNAL_MATCH_CLOSURE, %G_SIGNAL_MATCH_FUNC or
// %G_SIGNAL_MATCH_DATA match flags is required for successful
// matches.  If no handlers were found, 0 is returned, the number of
// disconnected handlers otherwise.
// RETURNS: The number of handlers that matched.
// <instance>: The instance to remove handlers from.
// <mask>: Mask indicating which of @signal_id, @detail, @closure, @func and/or @data the handlers have to match.
// <signal_id>: Signal the handlers have to be connected to.
// <detail>: Signal detail the handlers have to be connected to.
// <closure>: The closure the handlers will invoke.
// <func>: The C closure callback of the handlers (useless for non-C closures).
// <data>: The closure data of the handlers' closures.
static uint signal_handlers_disconnect_matched(AT0, AT1, AT2, AT3)(AT0 /*void*/ instance, SignalMatchType mask, uint signal_id, GLib2.Quark detail, AT1 /*Closure*/ closure, AT2 /*void*/ func, AT3 /*void*/ data) nothrow {
   return g_signal_handlers_disconnect_matched(UpCast!(void*)(instance), mask, signal_id, detail, UpCast!(Closure*)(closure), UpCast!(void*)(func), UpCast!(void*)(data));
}


// Unblocks all handlers on an instance that match a certain selection
// criteria. The criteria mask is passed as an OR-ed combination of
// #GSignalMatchType flags, and the criteria values are passed as arguments.
// Passing at least one of the %G_SIGNAL_MATCH_CLOSURE, %G_SIGNAL_MATCH_FUNC
// or %G_SIGNAL_MATCH_DATA match flags is required for successful matches.
// If no handlers were found, 0 is returned, the number of unblocked handlers
// otherwise. The match criteria should not apply to any handlers that are
// not currently blocked.
// RETURNS: The number of handlers that matched.
// <instance>: The instance to unblock handlers from.
// <mask>: Mask indicating which of @signal_id, @detail, @closure, @func and/or @data the handlers have to match.
// <signal_id>: Signal the handlers have to be connected to.
// <detail>: Signal detail the handlers have to be connected to.
// <closure>: The closure the handlers will invoke.
// <func>: The C closure callback of the handlers (useless for non-C closures).
// <data>: The closure data of the handlers' closures.
static uint signal_handlers_unblock_matched(AT0, AT1, AT2, AT3)(AT0 /*void*/ instance, SignalMatchType mask, uint signal_id, GLib2.Quark detail, AT1 /*Closure*/ closure, AT2 /*void*/ func, AT3 /*void*/ data) nothrow {
   return g_signal_handlers_unblock_matched(UpCast!(void*)(instance), mask, signal_id, detail, UpCast!(Closure*)(closure), UpCast!(void*)(func), UpCast!(void*)(data));
}


// Returns whether there are any handlers connected to @instance for the
// given signal id and detail.
// 
// One example of when you might use this is when the arguments to the
// signal are difficult to compute. A class implementor may opt to not
// emit the signal if no one is attached anyway, thus saving the cost
// of building the arguments.
// 
// otherwise.
// RETURNS: %TRUE if a handler is connected to the signal, %FALSE
// <instance>: the object whose signal handlers are sought.
// <signal_id>: the signal id.
// <detail>: the detail.
// <may_be_blocked>: whether blocked handlers should count as match.
static int signal_has_handler_pending(AT0)(AT0 /*void*/ instance, uint signal_id, GLib2.Quark detail, int may_be_blocked) nothrow {
   return g_signal_has_handler_pending(UpCast!(void*)(instance), signal_id, detail, may_be_blocked);
}


// Lists the signals by id that a certain instance or interface type
// created. Further information about the signals can be acquired through
// g_signal_query().
// RETURNS: Newly allocated array of signal IDs.
// <itype>: Instance or interface type.
// <n_ids>: Location to store the number of signal ids for @itype.
static uint* signal_list_ids(AT0)(Type itype, /*out*/ AT0 /*uint*/ n_ids) nothrow {
   return g_signal_list_ids(itype, UpCast!(uint*)(n_ids));
}


// Given the name of the signal and the type of object it connects to, gets
// the signal's identifying integer. Emitting the signal by number is
// somewhat faster than using the name each time.
// 
// Also tries the ancestors of the given type.
// 
// See g_signal_new() for details on allowed signal names.
// RETURNS: the signal's identifying number, or 0 if no signal was found.
// <name>: the signal's name.
// <itype>: the type that the signal operates on.
static uint signal_lookup(AT0)(AT0 /*char*/ name, Type itype) nothrow {
   return g_signal_lookup(toCString!(char*)(name), itype);
}


// Given the signal's identifier, finds its name.
// 
// Two different signals may have the same name, if they have differing types.
// RETURNS: the signal name, or %NULL if the signal number was invalid.
// <signal_id>: the signal's identifying number.
static char* signal_name()(uint signal_id) nothrow {
   return g_signal_name(signal_id);
}


// Unintrospectable function: signal_new() / g_signal_new()
// Creates a new signal. (This is usually done in the class initializer.)
// 
// A signal name consists of segments consisting of ASCII letters and
// digits, separated by either the '-' or '_' character. The first
// character of a signal name must be a letter. Names which violate these
// rules lead to undefined behaviour of the GSignal system.
// 
// When registering a signal and looking up a signal, either separator can
// be used, but they cannot be mixed.
// 
// If 0 is used for @class_offset subclasses cannot override the class handler
// in their <code>class_init</code> method by doing
// <code>super_class->signal_handler = my_signal_handler</code>. Instead they
// will have to use g_signal_override_class_handler().
// 
// If c_marshaller is %NULL, g_cclosure_marshal_generic() will be used as
// the marshaller for this signal.
// RETURNS: the signal id
// <signal_name>: the name for the signal
// <itype>: the type this signal pertains to. It will also pertain to types which are derived from this type.
// <signal_flags>: a combination of #GSignalFlags specifying detail of when the default handler is to be invoked. You should at least specify %G_SIGNAL_RUN_FIRST or %G_SIGNAL_RUN_LAST.
// <class_offset>: The offset of the function pointer in the class structure for this type. Used to invoke a class method generically. Pass 0 to not associate a class method slot with this signal.
// <accumulator>: the accumulator for this signal; may be %NULL.
// <accu_data>: user data for the @accumulator.
// <c_marshaller>: the function to translate arrays of parameter values to signal emissions into C language callback invocations or %NULL.
// <return_type>: the type of return value, or #G_TYPE_NONE for a signal without a return value.
// <n_params>: the number of parameter types to follow.
alias g_signal_new signal_new; // Variadic


// Unintrospectable function: signal_new_class_handler() / g_signal_new_class_handler()
// VERSION: 2.18
// Creates a new signal. (This is usually done in the class initializer.)
// 
// This is a variant of g_signal_new() that takes a C callback instead
// off a class offset for the signal's class handler. This function
// doesn't need a function pointer exposed in the class structure of
// an object definition, instead the function pointer is passed
// directly and can be overriden by derived classes with
// g_signal_override_class_closure() or
// g_signal_override_class_handler()and chained to with
// g_signal_chain_from_overridden() or
// g_signal_chain_from_overridden_handler().
// 
// See g_signal_new() for information about signal names.
// 
// If c_marshaller is %NULL @g_cclosure_marshal_generic will be used as
// the marshaller for this signal.
// RETURNS: the signal id
// <signal_name>: the name for the signal
// <itype>: the type this signal pertains to. It will also pertain to types which are derived from this type.
// <signal_flags>: a combination of #GSignalFlags specifying detail of when the default handler is to be invoked. You should at least specify %G_SIGNAL_RUN_FIRST or %G_SIGNAL_RUN_LAST.
// <class_handler>: a #GCallback which acts as class implementation of this signal. Used to invoke a class method generically. Pass %NULL to not associate a class method with this signal.
// <accumulator>: the accumulator for this signal; may be %NULL.
// <accu_data>: user data for the @accumulator.
// <c_marshaller>: the function to translate arrays of parameter values to signal emissions into C language callback invocations or %NULL.
// <return_type>: the type of return value, or #G_TYPE_NONE for a signal without a return value.
// <n_params>: the number of parameter types to follow.
alias g_signal_new_class_handler signal_new_class_handler; // Variadic


// Unintrospectable function: signal_new_valist() / g_signal_new_valist()
// Creates a new signal. (This is usually done in the class initializer.)
// 
// See g_signal_new() for details on allowed signal names.
// 
// If c_marshaller is %NULL, g_cclosure_marshal_generic() will be used as
// the marshaller for this signal.
// RETURNS: the signal id
// <signal_name>: the name for the signal
// <itype>: the type this signal pertains to. It will also pertain to types which are derived from this type.
// <signal_flags>: a combination of #GSignalFlags specifying detail of when the default handler is to be invoked. You should at least specify %G_SIGNAL_RUN_FIRST or %G_SIGNAL_RUN_LAST.
// <class_closure>: The closure to invoke on signal emission; may be %NULL.
// <accumulator>: the accumulator for this signal; may be %NULL.
// <accu_data>: user data for the @accumulator.
// <c_marshaller>: the function to translate arrays of parameter values to signal emissions into C language callback invocations or %NULL.
// <return_type>: the type of return value, or #G_TYPE_NONE for a signal without a return value.
// <n_params>: the number of parameter types in @args.
// <args>: va_list of #GType, one for each parameter.
static uint signal_new_valist(AT0, AT1, AT2)(AT0 /*char*/ signal_name, Type itype, SignalFlags signal_flags, AT1 /*Closure*/ class_closure, SignalAccumulator accumulator, AT2 /*void*/ accu_data, SignalCMarshaller c_marshaller, Type return_type, uint n_params, va_list args) nothrow {
   return g_signal_new_valist(toCString!(char*)(signal_name), itype, signal_flags, UpCast!(Closure*)(class_closure), accumulator, UpCast!(void*)(accu_data), c_marshaller, return_type, n_params, args);
}


// Unintrospectable function: signal_newv() / g_signal_newv()
// Creates a new signal. (This is usually done in the class initializer.)
// 
// See g_signal_new() for details on allowed signal names.
// 
// If c_marshaller is %NULL @g_cclosure_marshal_generic will be used as
// the marshaller for this signal.
// RETURNS: the signal id
// <signal_name>: the name for the signal
// <itype>: the type this signal pertains to. It will also pertain to types which are derived from this type
// <signal_flags>: a combination of #GSignalFlags specifying detail of when the default handler is to be invoked. You should at least specify %G_SIGNAL_RUN_FIRST or %G_SIGNAL_RUN_LAST
// <class_closure>: The closure to invoke on signal emission; may be %NULL
// <accumulator>: the accumulator for this signal; may be %NULL
// <accu_data>: user data for the @accumulator
// <c_marshaller>: the function to translate arrays of parameter values to signal emissions into C language callback invocations or %NULL
// <return_type>: the type of return value, or #G_TYPE_NONE for a signal without a return value
// <n_params>: the length of @param_types
// <param_types>: an array of types, one for each parameter
static uint signal_newv(AT0, AT1, AT2, AT3)(AT0 /*char*/ signal_name, Type itype, SignalFlags signal_flags, AT1 /*Closure*/ class_closure, SignalAccumulator accumulator, AT2 /*void*/ accu_data, SignalCMarshaller c_marshaller, Type return_type, uint n_params, AT3 /*Type*/ param_types) nothrow {
   return g_signal_newv(toCString!(char*)(signal_name), itype, signal_flags, UpCast!(Closure*)(class_closure), accumulator, UpCast!(void*)(accu_data), c_marshaller, return_type, n_params, UpCast!(Type*)(param_types));
}


// Overrides the class closure (i.e. the default handler) for the given signal
// for emissions on instances of @instance_type. @instance_type must be derived
// from the type to which the signal belongs.
// 
// See g_signal_chain_from_overridden() and
// g_signal_chain_from_overridden_handler() for how to chain up to the
// parent class closure from inside the overridden one.
// <signal_id>: the signal id
// <instance_type>: the instance type on which to override the class closure for the signal.
// <class_closure>: the closure.
static void signal_override_class_closure(AT0)(uint signal_id, Type instance_type, AT0 /*Closure*/ class_closure) nothrow {
   g_signal_override_class_closure(signal_id, instance_type, UpCast!(Closure*)(class_closure));
}


// Unintrospectable function: signal_override_class_handler() / g_signal_override_class_handler()
// VERSION: 2.18
// Overrides the class closure (i.e. the default handler) for the
// given signal for emissions on instances of @instance_type with
// callabck @class_handler. @instance_type must be derived from the
// type to which the signal belongs.
// 
// See g_signal_chain_from_overridden() and
// g_signal_chain_from_overridden_handler() for how to chain up to the
// parent class closure from inside the overridden one.
// <signal_name>: the name for the signal
// <instance_type>: the instance type on which to override the class handler for the signal.
// <class_handler>: the handler.
static void signal_override_class_handler(AT0)(AT0 /*char*/ signal_name, Type instance_type, Callback class_handler) nothrow {
   g_signal_override_class_handler(toCString!(char*)(signal_name), instance_type, class_handler);
}


// Internal function to parse a signal name into its @signal_id
// and @detail quark.
// RETURNS: Whether the signal name could successfully be parsed and @signal_id_p and @detail_p contain valid return values.
// <detailed_signal>: a string of the form "signal-name::detail".
// <itype>: The interface/instance type that introduced "signal-name".
// <signal_id_p>: Location to store the signal id.
// <detail_p>: Location to store the detail quark.
// <force_detail_quark>: %TRUE forces creation of a #GQuark for the detail.
static int signal_parse_name(AT0, AT1, AT2)(AT0 /*char*/ detailed_signal, Type itype, /*out*/ AT1 /*uint*/ signal_id_p, /*out*/ AT2 /*GLib2.Quark*/ detail_p, int force_detail_quark) nothrow {
   return g_signal_parse_name(toCString!(char*)(detailed_signal), itype, UpCast!(uint*)(signal_id_p), UpCast!(GLib2.Quark*)(detail_p), force_detail_quark);
}


// Queries the signal system for in-depth information about a
// specific signal. This function will fill in a user-provided
// structure to hold signal-specific information. If an invalid
// signal id is passed in, the @signal_id member of the #GSignalQuery
// is 0. All members filled into the #GSignalQuery structure should
// be considered constant and have to be left untouched.
// <signal_id>: The signal id of the signal to query information for.
// <query>: A user provided structure that is filled in with constant values upon success.
static void signal_query(AT0)(uint signal_id, /*out*/ AT0 /*SignalQuery*/ query) nothrow {
   g_signal_query(signal_id, UpCast!(SignalQuery*)(query));
}


// Deletes an emission hook.
// <signal_id>: the id of the signal
// <hook_id>: the id of the emission hook, as returned by g_signal_add_emission_hook()
static void signal_remove_emission_hook()(uint signal_id, c_ulong hook_id) nothrow {
   g_signal_remove_emission_hook(signal_id, hook_id);
}

static void signal_set_va_marshaller()(uint signal_id, Type instance_type, SignalCVaMarshaller va_marshaller) nothrow {
   g_signal_set_va_marshaller(signal_id, instance_type, va_marshaller);
}


// Stops a signal's current emission.
// 
// This will prevent the default method from running, if the signal was
// %G_SIGNAL_RUN_LAST and you connected normally (i.e. without the "after"
// flag).
// 
// Prints a warning if used on a signal which isn't being emitted.
// <instance>: the object whose signal handlers you wish to stop.
// <signal_id>: the signal identifier, as returned by g_signal_lookup().
// <detail>: the detail which the signal was emitted with.
static void signal_stop_emission(AT0)(AT0 /*void*/ instance, uint signal_id, GLib2.Quark detail) nothrow {
   g_signal_stop_emission(UpCast!(void*)(instance), signal_id, detail);
}


// Stops a signal's current emission.
// 
// This is just like g_signal_stop_emission() except it will look up the
// signal id for you.
// <instance>: the object whose signal handlers you wish to stop.
// <detailed_signal>: a string of the form "signal-name::detail".
static void signal_stop_emission_by_name(AT0, AT1)(AT0 /*void*/ instance, AT1 /*char*/ detailed_signal) nothrow {
   g_signal_stop_emission_by_name(UpCast!(void*)(instance), toCString!(char*)(detailed_signal));
}


// Creates a new closure which invokes the function found at the offset
// @struct_offset in the class structure of the interface or classed type
// identified by @itype.
// RETURNS: a new #GCClosure
// <itype>: the #GType identifier of an interface or classed type
// <struct_offset>: the offset of the member function of @itype's class structure which is to be invoked by the new closure
static Closure* /*new*/ signal_type_cclosure_new()(Type itype, uint struct_offset) nothrow {
   return g_signal_type_cclosure_new(itype, struct_offset);
}


// Set the callback for a source as a #GClosure.
// 
// If the source is not one of the standard GLib types, the @closure_callback
// and @closure_marshal fields of the #GSourceFuncs structure must have been
// filled in with pointers to appropriate functions.
// <source>: the source
// <closure>: a #GClosure
static void source_set_closure(AT0, AT1)(AT0 /*GLib2.Source*/ source, AT1 /*Closure*/ closure) nothrow {
   g_source_set_closure(UpCast!(GLib2.Source*)(source), UpCast!(Closure*)(closure));
}


// Sets a dummy callback for @source. The callback will do nothing, and
// if the source expects a #gboolean return value, it will return %TRUE.
// (If the source expects any other type of return value, it will return
// a 0/%NULL value; whatever g_value_init() initializes a #GValue to for
// that type.)
// 
// If the source is not one of the standard GLib types, the
// @closure_callback and @closure_marshal fields of the #GSourceFuncs
// structure must have been filled in with pointers to appropriate
// functions.
// <source>: the source
static void source_set_dummy_callback(AT0)(AT0 /*GLib2.Source*/ source) nothrow {
   g_source_set_dummy_callback(UpCast!(GLib2.Source*)(source));
}


// Return a newly allocated string, which describes the contents of a
// #GValue.  The main purpose of this function is to describe #GValue
// contents for debugging output, the way in which the contents are
// described may change between different GLib versions.
// RETURNS: Newly allocated string.
// <value>: #GValue which contents are to be described.
static char* /*new*/ strdup_value_contents(AT0)(AT0 /*Value*/ value) nothrow {
   return g_strdup_value_contents(UpCast!(Value*)(value));
}


// Unintrospectable function: type_add_class_cache_func() / g_type_add_class_cache_func()
// Adds a #GTypeClassCacheFunc to be called before the reference count of a
// class goes from one to zero. This can be used to prevent premature class
// destruction. All installed #GTypeClassCacheFunc functions will be chained
// until one of them returns %TRUE. The functions have to check the class id
// passed in to figure whether they actually want to cache the class of this
// type, since all classes are routed through the same #GTypeClassCacheFunc
// chain.
// <cache_data>: data to be passed to @cache_func
// <cache_func>: a #GTypeClassCacheFunc
static void type_add_class_cache_func(AT0)(AT0 /*void*/ cache_data, TypeClassCacheFunc cache_func) nothrow {
   g_type_add_class_cache_func(UpCast!(void*)(cache_data), cache_func);
}


// VERSION: 2.24
// Registers a private class structure for a classed type;
// when the class is allocated, the private structures for
// the class and all of its parent types are allocated
// sequentially in the same memory block as the public
// structures. This function should be called in the
// type's get_type() function after the type is registered.
// The private structure can be retrieved using the
// G_TYPE_CLASS_GET_PRIVATE() macro.
// <class_type>: GType of an classed type.
// <private_size>: size of private structure.
static void type_add_class_private()(Type class_type, size_t private_size) nothrow {
   g_type_add_class_private(class_type, private_size);
}


// Unintrospectable function: type_add_interface_check() / g_type_add_interface_check()
// VERSION: 2.4
// Adds a function to be called after an interface vtable is
// initialized for any class (i.e. after the @interface_init member of
// #GInterfaceInfo has been called).
// 
// This function is useful when you want to check an invariant that
// depends on the interfaces of a class. For instance, the
// implementation of #GObject uses this facility to check that an
// object implements all of the properties that are defined on its
// interfaces.
// <check_data>: data to pass to @check_func
// <check_func>: function to be called after each interface is initialized.
static void type_add_interface_check(AT0)(AT0 /*void*/ check_data, TypeInterfaceCheckFunc check_func) nothrow {
   g_type_add_interface_check(UpCast!(void*)(check_data), check_func);
}


// Adds the dynamic @interface_type to @instantiable_type. The information
// contained in the #GTypePlugin structure pointed to by @plugin
// is used to manage the relationship.
// <instance_type>: the #GType value of an instantiable type.
// <interface_type>: the #GType value of an interface type.
// <plugin>: the #GTypePlugin structure to retrieve the #GInterfaceInfo from.
static void type_add_interface_dynamic(AT0)(Type instance_type, Type interface_type, AT0 /*TypePlugin*/ plugin) nothrow {
   g_type_add_interface_dynamic(instance_type, interface_type, UpCast!(TypePlugin*)(plugin));
}


// Adds the static @interface_type to @instantiable_type.  The
// information contained in the #GInterfaceInfo structure pointed to by
// @info is used to manage the relationship.
// <instance_type>: #GType value of an instantiable type.
// <interface_type>: #GType value of an interface type.
// <info>: The #GInterfaceInfo structure for this (@instance_type, @interface_type) combination.
static void type_add_interface_static(AT0)(Type instance_type, Type interface_type, AT0 /*InterfaceInfo*/ info) nothrow {
   g_type_add_interface_static(instance_type, interface_type, UpCast!(InterfaceInfo*)(info));
}

// Unintrospectable function: type_check_class_cast() / g_type_check_class_cast()
static TypeClass* type_check_class_cast(AT0)(AT0 /*TypeClass*/ g_class, Type is_a_type) nothrow {
   return g_type_check_class_cast(UpCast!(TypeClass*)(g_class), is_a_type);
}

static int type_check_class_is_a(AT0)(AT0 /*TypeClass*/ g_class, Type is_a_type) nothrow {
   return g_type_check_class_is_a(UpCast!(TypeClass*)(g_class), is_a_type);
}


// Private helper function to aid implementation of the G_TYPE_CHECK_INSTANCE()
// macro.
// <instance>: A valid #GTypeInstance structure.
static int type_check_instance(AT0)(AT0 /*TypeInstance*/ instance) nothrow {
   return g_type_check_instance(UpCast!(TypeInstance*)(instance));
}

// Unintrospectable function: type_check_instance_cast() / g_type_check_instance_cast()
static TypeInstance* type_check_instance_cast(AT0)(AT0 /*TypeInstance*/ instance, Type iface_type) nothrow {
   return g_type_check_instance_cast(UpCast!(TypeInstance*)(instance), iface_type);
}

static int type_check_instance_is_a(AT0)(AT0 /*TypeInstance*/ instance, Type iface_type) nothrow {
   return g_type_check_instance_is_a(UpCast!(TypeInstance*)(instance), iface_type);
}

static int type_check_is_value_type()(Type type) nothrow {
   return g_type_check_is_value_type(type);
}

static int type_check_value(AT0)(AT0 /*Value*/ value) nothrow {
   return g_type_check_value(UpCast!(Value*)(value));
}

static int type_check_value_holds(AT0)(AT0 /*Value*/ value, Type type) nothrow {
   return g_type_check_value_holds(UpCast!(Value*)(value), type);
}


// Return a newly allocated and 0-terminated array of type IDs, listing the
// child types of @type. The return value has to be g_free()ed after use.
// 
// and 0-terminated array of child types.
// RETURNS: Newly allocated
// <type>: The parent type.
// <n_children>: Optional #guint pointer to contain the number of child types.
static Type* /*new*/ type_children(AT0)(Type type, /*out*/ AT0 /*uint*/ n_children=null) nothrow {
   return g_type_children(type, UpCast!(uint*)(n_children));
}


// VERSION: 2.4
// MOVED TO: TypeClass.add_private
// Registers a private structure for an instantiatable type.
// 
// When an object is allocated, the private structures for
// the type and all of its parent types are allocated
// sequentially in the same memory block as the public
// structures.
// 
// Note that the accumulated size of the private structures of
// a type and all its parent types cannot excced 64 KiB.
// 
// This function should be called in the type's class_init() function.
// The private structure can be retrieved using the
// G_TYPE_INSTANCE_GET_PRIVATE() macro.
// 
// The following example shows attaching a private structure
// <structname>MyObjectPrivate</structname> to an object
// <structname>MyObject</structname> defined in the standard GObject
// fashion.
// type's class_init() function.
// Note the use of a structure member "priv" to avoid the overhead
// of repeatedly calling MY_OBJECT_GET_PRIVATE().
// 
// |[
// typedef struct _MyObject        MyObject;
// typedef struct _MyObjectPrivate MyObjectPrivate;
// 
// struct _MyObject {
// GObject parent;
// 
// MyObjectPrivate *priv;
// };
// 
// struct _MyObjectPrivate {
// int some_field;
// };
// 
// static void
// my_object_class_init (MyObjectClass *klass)
// {
// g_type_class_add_private (klass, sizeof (MyObjectPrivate));
// }
// 
// static void
// my_object_init (MyObject *my_object)
// {
// my_object->priv = G_TYPE_INSTANCE_GET_PRIVATE (my_object,
// MY_TYPE_OBJECT,
// MyObjectPrivate);
// }
// 
// static int
// my_object_get_some_field (MyObject *my_object)
// {
// MyObjectPrivate *priv;
// 
// g_return_val_if_fail (MY_IS_OBJECT (my_object), 0);
// 
// priv = my_object->priv;
// 
// return priv->some_field;
// }
// ]|
// <g_class>: class structure for an instantiatable type
// <private_size>: size of private structure.
static void type_class_add_private(AT0)(AT0 /*void*/ g_class, size_t private_size) nothrow {
   g_type_class_add_private(UpCast!(void*)(g_class), private_size);
}


// MOVED TO: TypeClass.peek
// This function is essentially the same as g_type_class_ref(), except that
// the classes reference count isn't incremented. As a consequence, this function
// may return %NULL if the class of the type passed in does not currently
// exist (hasn't been referenced before).
// 
// structure for the given type ID or %NULL if the class does not
// currently exist.
// RETURNS: The #GTypeClass
// <type>: Type ID of a classed type.
static TypeClass* type_class_peek()(Type type) nothrow {
   return g_type_class_peek(type);
}


// VERSION: 2.4
// MOVED TO: TypeClass.peek_static
// A more efficient version of g_type_class_peek() which works only for
// static types.
// 
// structure for the given type ID or %NULL if the class does not
// currently exist or is dynamically loaded.
// RETURNS: The #GTypeClass
// <type>: Type ID of a classed type.
static TypeClass* type_class_peek_static()(Type type) nothrow {
   return g_type_class_peek_static(type);
}


// MOVED TO: TypeClass.ref
// Increments the reference count of the class structure belonging to
// @type. This function will demand-create the class if it doesn't
// exist already.
// 
// structure for the given type ID.
// RETURNS: The #GTypeClass
// <type>: Type ID of a classed type.
static TypeClass* type_class_ref()(Type type) nothrow {
   return g_type_class_ref(type);
}


// Unintrospectable function: type_create_instance() / g_type_create_instance()
// Creates and initializes an instance of @type if @type is valid and
// can be instantiated. The type system only performs basic allocation
// and structure setups for instances: actual instance creation should
// happen through functions supplied by the type's fundamental type
// implementation.  So use of g_type_create_instance() is reserved for
// implementators of fundamental types only. E.g. instances of the
// #GObject hierarchy should be created via g_object_new() and
// <emphasis>never</emphasis> directly through
// g_type_create_instance() which doesn't handle things like singleton
// objects or object construction.  Note: Do <emphasis>not</emphasis>
// use this function, unless you're implementing a fundamental
// type. Also language bindings should <emphasis>not</emphasis> use
// this function but g_object_new() instead.
// 
// treatment by the fundamental type implementation.
// RETURNS: An allocated and initialized instance, subject to further
// <type>: An instantiatable type to create an instance for.
static TypeInstance* type_create_instance()(Type type) nothrow {
   return g_type_create_instance(type);
}


// VERSION: 2.4
// If the interface type @g_type is currently in use, returns its
// default interface vtable.
// 
// 
// vtable for the interface, or %NULL if the type is not currently in
// use.
// RETURNS: the default
// <g_type>: an interface type
static TypeInterface* type_default_interface_peek()(Type g_type) nothrow {
   return g_type_default_interface_peek(g_type);
}


// VERSION: 2.4
// Increments the reference count for the interface type @g_type,
// and returns the default interface vtable for the type.
// 
// If the type is not currently in use, then the default vtable
// for the type will be created and initalized by calling
// the base interface init and default vtable init functions for
// the type (the @<structfield>base_init</structfield>
// and <structfield>class_init</structfield> members of #GTypeInfo).
// Calling g_type_default_interface_ref() is useful when you
// want to make sure that signals and properties for an interface
// have been installed.
// 
// 
// vtable for the interface; call g_type_default_interface_unref()
// when you are done using the interface.
// RETURNS: the default
// <g_type>: an interface type
static TypeInterface* type_default_interface_ref()(Type g_type) nothrow {
   return g_type_default_interface_ref(g_type);
}


// VERSION: 2.4
// Decrements the reference count for the type corresponding to the
// interface default vtable @g_iface. If the type is dynamic, then
// when no one is using the interface and all references have
// been released, the finalize function for the interface's default
// vtable (the <structfield>class_finalize</structfield> member of
// #GTypeInfo) will be called.
// <g_iface>: the default vtable structure for a interface, as returned by g_type_default_interface_ref()
static void type_default_interface_unref(AT0)(AT0 /*TypeInterface*/ g_iface) nothrow {
   g_type_default_interface_unref(UpCast!(TypeInterface*)(g_iface));
}


// Returns the length of the ancestry of the passed in type. This
// includes the type itself, so that e.g. a fundamental type has depth 1.
// RETURNS: The depth of @type.
// <type>: A #GType value.
static uint type_depth()(Type type) nothrow {
   return g_type_depth(type);
}


// Frees an instance of a type, returning it to the instance pool for
// the type, if there is one.
// 
// Like g_type_create_instance(), this function is reserved for
// implementors of fundamental types.
// <instance>: an instance of a type.
static void type_free_instance(AT0)(AT0 /*TypeInstance*/ instance) nothrow {
   g_type_free_instance(UpCast!(TypeInstance*)(instance));
}


// Lookup the type ID from a given type name, returning 0 if no type
// has been registered under this name (this is the preferred method
// to find out by name whether a specific type has been registered
// yet).
// RETURNS: Corresponding type ID or 0.
// <name>: Type name to lookup.
static Type type_from_name(AT0)(AT0 /*char*/ name) nothrow {
   return g_type_from_name(toCString!(char*)(name));
}


// Internal function, used to extract the fundamental type ID portion.
// use G_TYPE_FUNDAMENTAL() instead.
// RETURNS: fundamental type ID
// <type_id>: valid type ID
static Type type_fundamental()(Type type_id) nothrow {
   return g_type_fundamental(type_id);
}


// Returns the next free fundamental type id which can be used to
// register a new fundamental type with g_type_register_fundamental().
// The returned type ID represents the highest currently registered
// fundamental type identifier.
// 
// or 0 if the type system ran out of fundamental type IDs.
// RETURNS: The nextmost fundamental type ID to be registered,
static Type type_fundamental_next()() nothrow {
   return g_type_fundamental_next();
}


// Returns the #GTypePlugin structure for @type or
// %NULL if @type does not have a #GTypePlugin structure.
// 
// dynamic type, %NULL otherwise.
// RETURNS: The corresponding plugin if @type is a
// <type>: The #GType to retrieve the plugin for.
static TypePlugin* type_get_plugin()(Type type) nothrow {
   return g_type_get_plugin(type);
}


// Obtains data which has previously been attached to @type
// with g_type_set_qdata().
// 
// Note that this does not take subtyping into account; data
// attached to one type with g_type_set_qdata() cannot
// be retrieved from a subtype using g_type_get_qdata().
// RETURNS: the data, or %NULL if no data was found
// <type>: a #GType
// <quark>: a #GQuark id to identify the data
static void* type_get_qdata()(Type type, GLib2.Quark quark) nothrow {
   return g_type_get_qdata(type, quark);
}


// Prior to any use of the type system, g_type_init() has to be called
// to initialize the type system and assorted other code portions
// (such as the various fundamental type implementations or the signal
// system).
// 
// This function is idempotent.
// 
// Since version 2.24 this also initializes the thread system
static void type_init()() nothrow {
   g_type_init();
}


// Similar to g_type_init(), but additionally sets debug flags.
// 
// This function is idempotent.
// <debug_flags>: Bitwise combination of #GTypeDebugFlags values for debugging purposes.
static void type_init_with_debug_flags()(TypeDebugFlags debug_flags) nothrow {
   g_type_init_with_debug_flags(debug_flags);
}


// MOVED TO: TypeInterface.add_prerequisite
// Adds @prerequisite_type to the list of prerequisites of @interface_type.
// This means that any type implementing @interface_type must also implement
// @prerequisite_type. Prerequisites can be thought of as an alternative to
// interface derivation (which GType doesn't support). An interface can have
// at most one instantiatable prerequisite type.
// <interface_type>: #GType value of an interface type.
// <prerequisite_type>: #GType value of an interface or instantiatable type.
static void type_interface_add_prerequisite()(Type interface_type, Type prerequisite_type) nothrow {
   g_type_interface_add_prerequisite(interface_type, prerequisite_type);
}


// MOVED TO: TypeInterface.get_plugin
// Returns the #GTypePlugin structure for the dynamic interface
// @interface_type which has been added to @instance_type, or %NULL if
// @interface_type has not been added to @instance_type or does not
// have a #GTypePlugin structure. See g_type_add_interface_dynamic().
// 
// interface @interface_type of @instance_type.
// RETURNS: the #GTypePlugin for the dynamic
// <instance_type>: the #GType value of an instantiatable type.
// <interface_type>: the #GType value of an interface type.
static TypePlugin* type_interface_get_plugin()(Type instance_type, Type interface_type) nothrow {
   return g_type_interface_get_plugin(instance_type, interface_type);
}


// MOVED TO: TypeInterface.peek
// Returns the #GTypeInterface structure of an interface to which the
// passed in class conforms.
// 
// structure of iface_type if implemented by @instance_class, %NULL
// otherwise
// RETURNS: The GTypeInterface
// <instance_class>: A #GTypeClass structure.
// <iface_type>: An interface ID which this class conforms to.
static TypeInterface* type_interface_peek(AT0)(AT0 /*TypeClass*/ instance_class, Type iface_type) nothrow {
   return g_type_interface_peek(UpCast!(TypeClass*)(instance_class), iface_type);
}


// VERSION: 2.2
// MOVED TO: TypeInterface.prerequisites
// Returns the prerequisites of an interfaces type.
// 
// 
// newly-allocated zero-terminated array of #GType containing
// the prerequisites of @interface_type
// RETURNS: a
// <interface_type>: an interface type
// <n_prerequisites>: location to return the number of prerequisites, or %NULL
static Type* /*new*/ type_interface_prerequisites(AT0)(Type interface_type, /*out*/ AT0 /*uint*/ n_prerequisites=null) nothrow {
   return g_type_interface_prerequisites(interface_type, UpCast!(uint*)(n_prerequisites));
}


// Return a newly allocated and 0-terminated array of type IDs, listing the
// interface types that @type conforms to. The return value has to be
// g_free()ed after use.
// 
// allocated and 0-terminated array of interface types.
// RETURNS: Newly
// <type>: The type to list interface types for.
// <n_interfaces>: Optional #guint pointer to contain the number of interface types.
static Type* /*new*/ type_interfaces(AT0)(Type type, /*out*/ AT0 /*uint*/ n_interfaces=null) nothrow {
   return g_type_interfaces(type, UpCast!(uint*)(n_interfaces));
}


// If @is_a_type is a derivable type, check whether @type is a
// descendant of @is_a_type.  If @is_a_type is an interface, check
// whether @type conforms to it.
// RETURNS: %TRUE if @type is_a @is_a_type holds true.
// <type>: Type to check anchestry for.
// <is_a_type>: Possible anchestor of @type or interface @type could conform to.
static int type_is_a()(Type type, Type is_a_type) nothrow {
   return g_type_is_a(type, is_a_type);
}


// Get the unique name that is assigned to a type ID.  Note that this
// function (like all other GType API) cannot cope with invalid type
// IDs. %G_TYPE_INVALID may be passed to this function, as may be any
// other validly registered type ID, but randomized type IDs should
// not be passed in and will most likely lead to a crash.
// RETURNS: Static type name or %NULL.
// <type>: Type to return name for.
static char* type_name()(Type type) nothrow {
   return g_type_name(type);
}

static char* type_name_from_class(AT0)(AT0 /*TypeClass*/ g_class) nothrow {
   return g_type_name_from_class(UpCast!(TypeClass*)(g_class));
}

static char* type_name_from_instance(AT0)(AT0 /*TypeInstance*/ instance) nothrow {
   return g_type_name_from_instance(UpCast!(TypeInstance*)(instance));
}


// Given a @leaf_type and a @root_type which is contained in its
// anchestry, return the type that @root_type is the immediate parent
// of.  In other words, this function determines the type that is
// derived directly from @root_type which is also a base class of
// @leaf_type.  Given a root type and a leaf type, this function can
// be used to determine the types and order in which the leaf type is
// descended from the root type.
// RETURNS: Immediate child of @root_type and anchestor of @leaf_type.
// <leaf_type>: Descendant of @root_type and the type to be returned.
// <root_type>: Immediate parent of the returned type.
static Type type_next_base()(Type leaf_type, Type root_type) nothrow {
   return g_type_next_base(leaf_type, root_type);
}


// Return the direct parent type of the passed in type.  If the passed
// in type has no parent, i.e. is a fundamental type, 0 is returned.
// RETURNS: The parent type.
// <type>: The derived type.
static Type type_parent()(Type type) nothrow {
   return g_type_parent(type);
}


// Get the corresponding quark of the type IDs name.
// RETURNS: The type names quark or 0.
// <type>: Type to return quark of type name for.
static GLib2.Quark type_qname()(Type type) nothrow {
   return g_type_qname(type);
}


// Queries the type system for information about a specific type.
// This function will fill in a user-provided structure to hold
// type-specific information. If an invalid #GType is passed in, the
// @type member of the #GTypeQuery is 0. All members filled into the
// #GTypeQuery structure should be considered constant and have to be
// left untouched.
// <type>: the #GType value of a static, classed type.
// <query>: A user provided structure that is filled in with constant values upon success.
static void type_query(AT0)(Type type, /*out*/ AT0 /*TypeQuery*/ query) nothrow {
   g_type_query(type, UpCast!(TypeQuery*)(query));
}


// Registers @type_name as the name of a new dynamic type derived from
// @parent_type.  The type system uses the information contained in the
// #GTypePlugin structure pointed to by @plugin to manage the type and its
// instances (if not abstract).  The value of @flags determines the nature
// (e.g. abstract or not) of the type.
// RETURNS: The new type identifier or #G_TYPE_INVALID if registration failed.
// <parent_type>: Type from which this type will be derived.
// <type_name>: 0-terminated string used as the name of the new type.
// <plugin>: The #GTypePlugin structure to retrieve the #GTypeInfo from.
// <flags>: Bitwise combination of #GTypeFlags values.
static Type type_register_dynamic(AT0, AT1)(Type parent_type, AT0 /*char*/ type_name, AT1 /*TypePlugin*/ plugin, TypeFlags flags) nothrow {
   return g_type_register_dynamic(parent_type, toCString!(char*)(type_name), UpCast!(TypePlugin*)(plugin), flags);
}


// Registers @type_id as the predefined identifier and @type_name as the
// name of a fundamental type. If @type_id is already registered, or a type
// named @type_name is already registered, the behaviour is undefined. The type
// system uses the information contained in the #GTypeInfo structure pointed to
// by @info and the #GTypeFundamentalInfo structure pointed to by @finfo to
// manage the type and its instances. The value of @flags determines additional
// characteristics of the fundamental type.
// RETURNS: The predefined type identifier.
// <type_id>: A predefined type identifier.
// <type_name>: 0-terminated string used as the name of the new type.
// <info>: The #GTypeInfo structure for this type.
// <finfo>: The #GTypeFundamentalInfo structure for this type.
// <flags>: Bitwise combination of #GTypeFlags values.
static Type type_register_fundamental(AT0, AT1, AT2)(Type type_id, AT0 /*char*/ type_name, AT1 /*GTypeInfo*/ info, AT2 /*TypeFundamentalInfo*/ finfo, TypeFlags flags) nothrow {
   return g_type_register_fundamental(type_id, toCString!(char*)(type_name), UpCast!(GTypeInfo*)(info), UpCast!(TypeFundamentalInfo*)(finfo), flags);
}


// Registers @type_name as the name of a new static type derived from
// @parent_type.  The type system uses the information contained in the
// #GTypeInfo structure pointed to by @info to manage the type and its
// instances (if not abstract).  The value of @flags determines the nature
// (e.g. abstract or not) of the type.
// RETURNS: The new type identifier.
// <parent_type>: Type from which this type will be derived.
// <type_name>: 0-terminated string used as the name of the new type.
// <info>: The #GTypeInfo structure for this type.
// <flags>: Bitwise combination of #GTypeFlags values.
static Type type_register_static(AT0, AT1)(Type parent_type, AT0 /*char*/ type_name, AT1 /*GTypeInfo*/ info, TypeFlags flags) nothrow {
   return g_type_register_static(parent_type, toCString!(char*)(type_name), UpCast!(GTypeInfo*)(info), flags);
}


// Unintrospectable function: type_register_static_simple() / g_type_register_static_simple()
// VERSION: 2.12
// Registers @type_name as the name of a new static type derived from
// @parent_type.  The value of @flags determines the nature (e.g.
// abstract or not) of the type. It works by filling a #GTypeInfo
// struct and calling g_type_register_static().
// RETURNS: The new type identifier.
// <parent_type>: Type from which this type will be derived.
// <type_name>: 0-terminated string used as the name of the new type.
// <class_size>: Size of the class structure (see #GTypeInfo)
// <class_init>: Location of the class initialization function (see #GTypeInfo)
// <instance_size>: Size of the instance structure (see #GTypeInfo)
// <instance_init>: Location of the instance initialization function (see #GTypeInfo)
// <flags>: Bitwise combination of #GTypeFlags values.
static Type type_register_static_simple(AT0)(Type parent_type, AT0 /*char*/ type_name, uint class_size, ClassInitFunc class_init, uint instance_size, InstanceInitFunc instance_init, TypeFlags flags) nothrow {
   return g_type_register_static_simple(parent_type, toCString!(char*)(type_name), class_size, class_init, instance_size, instance_init, flags);
}


// Unintrospectable function: type_remove_class_cache_func() / g_type_remove_class_cache_func()
// Removes a previously installed #GTypeClassCacheFunc. The cache
// maintained by @cache_func has to be empty when calling
// g_type_remove_class_cache_func() to avoid leaks.
// <cache_data>: data that was given when adding @cache_func
// <cache_func>: a #GTypeClassCacheFunc
static void type_remove_class_cache_func(AT0)(AT0 /*void*/ cache_data, TypeClassCacheFunc cache_func) nothrow {
   g_type_remove_class_cache_func(UpCast!(void*)(cache_data), cache_func);
}


// Unintrospectable function: type_remove_interface_check() / g_type_remove_interface_check()
// VERSION: 2.4
// Removes an interface check function added with
// g_type_add_interface_check().
// <check_data>: callback data passed to g_type_add_interface_check()
// <check_func>: callback function passed to g_type_add_interface_check()
static void type_remove_interface_check(AT0)(AT0 /*void*/ check_data, TypeInterfaceCheckFunc check_func) nothrow {
   g_type_remove_interface_check(UpCast!(void*)(check_data), check_func);
}


// Attaches arbitrary data to a type.
// <type>: a #GType
// <quark>: a #GQuark id to identify the data
// <data>: the data
static void type_set_qdata(AT0)(Type type, GLib2.Quark quark, AT0 /*void*/ data) nothrow {
   g_type_set_qdata(type, quark, UpCast!(void*)(data));
}

static int type_test_flags()(Type type, uint flags) nothrow {
   return g_type_test_flags(type, flags);
}


// Unintrospectable function: type_value_table_peek() / g_type_value_table_peek()
// MOVED TO: TypeValueTable.peek
// Returns the location of the #GTypeValueTable associated with @type.
// <emphasis>Note that this function should only be used from source code
// that implements or has internal knowledge of the implementation of
// @type.</emphasis>
// 
// %NULL if there is no #GTypeValueTable associated with @type.
// RETURNS: Location of the #GTypeValueTable associated with @type or
// <type>: A #GType value.
static TypeValueTable* type_value_table_peek()(Type type) nothrow {
   return g_type_value_table_peek(type);
}


// Unintrospectable function: value_register_transform_func() / g_value_register_transform_func()
// MOVED TO: Value.register_transform_func
// Registers a value transformation function for use in g_value_transform().
// A previously registered transformation function for @src_type and @dest_type
// will be replaced.
// <src_type>: Source type.
// <dest_type>: Target type.
// <transform_func>: a function which transforms values of type @src_type into value of type @dest_type
static void value_register_transform_func()(Type src_type, Type dest_type, ValueTransform transform_func) nothrow {
   g_value_register_transform_func(src_type, dest_type, transform_func);
}


// MOVED TO: Value.type_compatible
// Returns whether a #GValue of type @src_type can be copied into
// a #GValue of type @dest_type.
// RETURNS: %TRUE if g_value_copy() is possible with @src_type and @dest_type.
// <src_type>: source type to be copied.
// <dest_type>: destination type for copying.
static int value_type_compatible()(Type src_type, Type dest_type) nothrow {
   return g_value_type_compatible(src_type, dest_type);
}


// MOVED TO: Value.type_transformable
// Check whether g_value_transform() is able to transform values
// of type @src_type into values of type @dest_type.
// RETURNS: %TRUE if the transformation is possible, %FALSE otherwise.
// <src_type>: Source type.
// <dest_type>: Target type.
static int value_type_transformable()(Type src_type, Type dest_type) nothrow {
   return g_value_type_transformable(src_type, dest_type);
}


// C prototypes:

extern (C) {
BindingFlags g_binding_get_flags(Binding* this_) nothrow;
Object* g_binding_get_source(Binding* this_) nothrow;
char* g_binding_get_source_property(Binding* this_) nothrow;
Object* g_binding_get_target(Binding* this_) nothrow;
char* g_binding_get_target_property(Binding* this_) nothrow;
void g_cclosure_marshal_BOOLEAN__BOXED_BOXED(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_BOOLEAN__BOXED_BOXEDv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_BOOLEAN__FLAGS(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_BOOLEAN__FLAGSv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_STRING__OBJECT_POINTER(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_STRING__OBJECT_POINTERv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__BOOLEAN(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__BOOLEANv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__BOXED(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__BOXEDv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__CHAR(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__CHARv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__DOUBLE(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__DOUBLEv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__ENUM(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__ENUMv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__FLAGS(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__FLAGSv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__FLOAT(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__FLOATv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__INT(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__INTv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__LONG(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__LONGv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__OBJECT(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__OBJECTv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__PARAM(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__PARAMv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__POINTER(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__POINTERv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__STRING(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__STRINGv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__UCHAR(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__UCHARv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__UINT(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__UINT_POINTER(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__UINT_POINTERv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__UINTv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__ULONG(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__ULONGv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__VARIANT(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__VARIANTv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_VOID__VOID(Closure* closure, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_VOID__VOIDv(Closure* closure, Value* return_value, void* instance, va_list args, void* marshal_data, int n_params, Type* param_types) nothrow;
void g_cclosure_marshal_generic(Closure* closure, Value* return_gvalue, uint n_param_values, Value* param_values, void* invocation_hint, void* marshal_data) nothrow;
void g_cclosure_marshal_generic_va(Closure* closure, Value* return_value, void* instance, va_list args_list, void* marshal_data, int n_params, Type* param_types) nothrow;
Closure* /*new*/ g_cclosure_new(Callback callback_func, void* user_data, ClosureNotify destroy_data) nothrow;
Closure* /*new*/ g_cclosure_new_object(Callback callback_func, Object* object) nothrow;
Closure* /*new*/ g_cclosure_new_object_swap(Callback callback_func, Object* object) nothrow;
Closure* /*new*/ g_cclosure_new_swap(Callback callback_func, void* user_data, ClosureNotify destroy_data) nothrow;
Closure* /*new*/ g_closure_new_object(uint sizeof_closure, Object* object) nothrow;
Closure* /*new*/ g_closure_new_simple(uint sizeof_closure, void* data) nothrow;
void g_closure_add_finalize_notifier(Closure* this_, void* notify_data, ClosureNotify notify_func) nothrow;
void g_closure_add_invalidate_notifier(Closure* this_, void* notify_data, ClosureNotify notify_func) nothrow;
void g_closure_add_marshal_guards(Closure* this_, void* pre_marshal_data, ClosureNotify pre_marshal_notify, void* post_marshal_data, ClosureNotify post_marshal_notify) nothrow;
void g_closure_invalidate(Closure* this_) nothrow;
void g_closure_invoke(Closure* this_, Value* return_value, uint n_param_values, Value* param_values, void* invocation_hint=null) nothrow;
Closure* g_closure_ref(Closure* this_) nothrow;
void g_closure_remove_finalize_notifier(Closure* this_, void* notify_data, ClosureNotify notify_func) nothrow;
void g_closure_remove_invalidate_notifier(Closure* this_, void* notify_data, ClosureNotify notify_func) nothrow;
void g_closure_set_marshal(Closure* this_, ClosureMarshal marshal) nothrow;
void g_closure_set_meta_marshal(Closure* this_, void* marshal_data, ClosureMarshal meta_marshal) nothrow;
void g_closure_sink(Closure* this_) nothrow;
void g_closure_unref(Closure* this_) nothrow;
Object* /*new*/ g_object_new_valist(Type object_type, char* first_property_name, va_list var_args) nothrow;
Object* /*new*/ g_object_newv(Type object_type, uint n_parameters, Parameter* parameters) nothrow;
size_t g_object_compat_control(size_t what, void* data) nothrow;
void* g_object_connect(void* object, char* signal_spec, ...) nothrow;
void g_object_disconnect(void* object, char* signal_spec, ...) nothrow;
void g_object_get(void* object, char* first_property_name, ...) nothrow;
ParamSpec* g_object_interface_find_property(void* g_iface, char* property_name) nothrow;
void g_object_interface_install_property(void* g_iface, ParamSpec* pspec) nothrow;
ParamSpec** /*new container*/ g_object_interface_list_properties(void* g_iface, /*out*/ uint* n_properties_p) nothrow;
void* /*new*/ g_object_new(Type object_type, char* first_property_name, ...) nothrow;
void g_object_set(void* object, char* first_property_name, ...) nothrow;
void g_object_add_toggle_ref(Object* this_, ToggleNotify notify, void* data) nothrow;
void g_object_add_weak_pointer(Object* this_, /*inout*/ void** weak_pointer_location) nothrow;
Binding* g_object_bind_property(Object* this_, char* source_property, Object* target, char* target_property, BindingFlags flags) nothrow;
Binding* g_object_bind_property_full(Object* this_, char* source_property, Object* target, char* target_property, BindingFlags flags, BindingTransformFunc transform_to, BindingTransformFunc transform_from, void* user_data, GLib2.DestroyNotify notify) nothrow;
Binding* g_object_bind_property_with_closures(Object* this_, char* source_property, Object* target, char* target_property, BindingFlags flags, Closure* transform_to, Closure* transform_from) nothrow;
void g_object_force_floating(Object* this_) nothrow;
void g_object_freeze_notify(Object* this_) nothrow;
void* g_object_get_data(Object* this_, char* key) nothrow;
void g_object_get_property(Object* this_, char* property_name, Value* value) nothrow;
void* g_object_get_qdata(Object* this_, GLib2.Quark quark) nothrow;
void g_object_get_valist(Object* this_, char* first_property_name, va_list var_args) nothrow;
int g_object_is_floating(Object* this_) nothrow;
void g_object_notify(Object* this_, char* property_name) nothrow;
void g_object_notify_by_pspec(Object* this_, ParamSpec* pspec) nothrow;
Object* g_object_ref(Object* this_) nothrow;
Object* g_object_ref_sink(Object* this_) nothrow;
void g_object_remove_toggle_ref(Object* this_, ToggleNotify notify, void* data) nothrow;
void g_object_remove_weak_pointer(Object* this_, /*inout*/ void** weak_pointer_location) nothrow;
void g_object_run_dispose(Object* this_) nothrow;
void g_object_set_data(Object* this_, char* key, void* data) nothrow;
void g_object_set_data_full(Object* this_, char* key, void* data, GLib2.DestroyNotify destroy) nothrow;
void g_object_set_property(Object* this_, char* property_name, Value* value) nothrow;
void g_object_set_qdata(Object* this_, GLib2.Quark quark, void* data) nothrow;
void g_object_set_qdata_full(Object* this_, GLib2.Quark quark, void* data, GLib2.DestroyNotify destroy) nothrow;
void g_object_set_valist(Object* this_, char* first_property_name, va_list var_args) nothrow;
void* /*new*/ g_object_steal_data(Object* this_, char* key) nothrow;
void* /*new*/ g_object_steal_qdata(Object* this_, GLib2.Quark quark) nothrow;
void g_object_thaw_notify(Object* this_) nothrow;
void g_object_unref(Object* this_) nothrow;
void g_object_watch_closure(Object* this_, Closure* closure) nothrow;
void g_object_weak_ref(Object* this_, WeakNotify notify, void* data) nothrow;
void g_object_weak_unref(Object* this_, WeakNotify notify, void* data) nothrow;
ParamSpec* g_object_class_find_property(ObjectClass* this_, char* property_name) nothrow;
void g_object_class_install_properties(ObjectClass* this_, uint n_pspecs, ParamSpec** pspecs) nothrow;
void g_object_class_install_property(ObjectClass* this_, uint property_id, ParamSpec* pspec) nothrow;
ParamSpec** /*new container*/ g_object_class_list_properties(ObjectClass* this_, /*out*/ uint* n_properties) nothrow;
void g_object_class_override_property(ObjectClass* this_, uint property_id, char* name) nothrow;
void* g_param_spec_internal(Type param_type, char* name, char* nick, char* blurb, ParamFlags flags) nothrow;
char* g_param_spec_get_blurb(ParamSpec* this_) nothrow;
char* g_param_spec_get_name(ParamSpec* this_) nothrow;
char* g_param_spec_get_nick(ParamSpec* this_) nothrow;
void* g_param_spec_get_qdata(ParamSpec* this_, GLib2.Quark quark) nothrow;
ParamSpec* g_param_spec_get_redirect_target(ParamSpec* this_) nothrow;
ParamSpec* g_param_spec_ref(ParamSpec* this_) nothrow;
ParamSpec* g_param_spec_ref_sink(ParamSpec* this_) nothrow;
void g_param_spec_set_qdata(ParamSpec* this_, GLib2.Quark quark, void* data) nothrow;
void g_param_spec_set_qdata_full(ParamSpec* this_, GLib2.Quark quark, void* data, GLib2.DestroyNotify destroy) nothrow;
void g_param_spec_sink(ParamSpec* this_) nothrow;
void* g_param_spec_steal_qdata(ParamSpec* this_, GLib2.Quark quark) nothrow;
void g_param_spec_unref(ParamSpec* this_) nothrow;
void g_param_spec_pool_insert(ParamSpecPool* this_, ParamSpec* pspec, Type owner_type) nothrow;
ParamSpec** /*new container*/ g_param_spec_pool_list(ParamSpecPool* this_, Type owner_type, /*out*/ uint* n_pspecs_p) nothrow;
GLib2.List* /*new container*/ g_param_spec_pool_list_owned(ParamSpecPool* this_, Type owner_type) nothrow;
ParamSpec* g_param_spec_pool_lookup(ParamSpecPool* this_, char* param_name, Type owner_type, int walk_ancestors) nothrow;
void g_param_spec_pool_remove(ParamSpecPool* this_, ParamSpec* pspec) nothrow;
ParamSpecPool* g_param_spec_pool_new(int type_prefixing) nothrow;
void* g_type_class_get_private(TypeClass* this_, Type private_type) nothrow;
TypeClass* g_type_class_peek_parent(TypeClass* this_) nothrow;
void g_type_class_unref(TypeClass* this_) nothrow;
void g_type_class_unref_uncached(TypeClass* this_) nothrow;
void g_type_class_add_private(void* g_class, size_t private_size) nothrow;
TypeClass* g_type_class_peek(Type type) nothrow;
TypeClass* g_type_class_peek_static(Type type) nothrow;
TypeClass* g_type_class_ref(Type type) nothrow;
void* g_type_instance_get_private(TypeInstance* this_, Type private_type) nothrow;
TypeInterface* g_type_interface_peek_parent(TypeInterface* this_) nothrow;
void g_type_interface_add_prerequisite(Type interface_type, Type prerequisite_type) nothrow;
TypePlugin* g_type_interface_get_plugin(Type instance_type, Type interface_type) nothrow;
TypeInterface* g_type_interface_peek(TypeClass* instance_class, Type iface_type) nothrow;
Type* /*new*/ g_type_interface_prerequisites(Type interface_type, /*out*/ uint* n_prerequisites=null) nothrow;
void g_type_module_add_interface(TypeModule* this_, Type instance_type, Type interface_type, InterfaceInfo* interface_info) nothrow;
Type g_type_module_register_enum(TypeModule* this_, char* name, EnumValue* const_static_values) nothrow;
Type g_type_module_register_flags(TypeModule* this_, char* name, FlagsValue* const_static_values) nothrow;
Type g_type_module_register_type(TypeModule* this_, Type parent_type, char* type_name, GTypeInfo* type_info, TypeFlags flags) nothrow;
void g_type_module_set_name(TypeModule* this_, char* name) nothrow;
void g_type_module_unuse(TypeModule* this_) nothrow;
int g_type_module_use(TypeModule* this_) nothrow;
void g_type_plugin_complete_interface_info(TypePlugin* this_, Type instance_type, Type interface_type, InterfaceInfo* info) nothrow;
void g_type_plugin_complete_type_info(TypePlugin* this_, Type g_type, GTypeInfo* info, TypeValueTable* value_table) nothrow;
void g_type_plugin_unuse(TypePlugin* this_) nothrow;
void g_type_plugin_use(TypePlugin* this_) nothrow;
TypeValueTable* g_type_value_table_peek(Type type) nothrow;
void g_value_copy(Value* this_, Value* dest_value) nothrow;
void* g_value_dup_boxed(Value* this_) nothrow;
Object* /*new*/ g_value_dup_object(Value* this_) nothrow;
ParamSpec* g_value_dup_param(Value* this_) nothrow;
char* /*new*/ g_value_dup_string(Value* this_) nothrow;
GLib2.Variant* /*new*/ g_value_dup_variant(Value* this_) nothrow;
int g_value_fits_pointer(Value* this_) nothrow;
int g_value_get_boolean(Value* this_) nothrow;
void* g_value_get_boxed(Value* this_) nothrow;
char g_value_get_char(Value* this_) nothrow;
double g_value_get_double(Value* this_) nothrow;
int g_value_get_enum(Value* this_) nothrow;
uint g_value_get_flags(Value* this_) nothrow;
float g_value_get_float(Value* this_) nothrow;
Type g_value_get_gtype(Value* this_) nothrow;
int g_value_get_int(Value* this_) nothrow;
long g_value_get_int64(Value* this_) nothrow;
c_long g_value_get_long(Value* this_) nothrow;
Object* g_value_get_object(Value* this_) nothrow;
ParamSpec* g_value_get_param(Value* this_) nothrow;
void* g_value_get_pointer(Value* this_) nothrow;
byte g_value_get_schar(Value* this_) nothrow;
char* g_value_get_string(Value* this_) nothrow;
ubyte g_value_get_uchar(Value* this_) nothrow;
uint g_value_get_uint(Value* this_) nothrow;
ulong g_value_get_uint64(Value* this_) nothrow;
c_ulong g_value_get_ulong(Value* this_) nothrow;
GLib2.Variant* /*new*/ g_value_get_variant(Value* this_) nothrow;
Value* g_value_init(Value* this_, Type g_type) nothrow;
void* g_value_peek_pointer(Value* this_) nothrow;
Value* /*new*/ g_value_reset(Value* this_) nothrow;
void g_value_set_boolean(Value* this_, int v_boolean) nothrow;
void g_value_set_boxed(Value* this_, const(void)* v_boxed=null) nothrow;
void g_value_set_boxed_take_ownership(Value* this_, const(void)* v_boxed=null) nothrow;
void g_value_set_char(Value* this_, char v_char) nothrow;
void g_value_set_double(Value* this_, double v_double) nothrow;
void g_value_set_enum(Value* this_, int v_enum) nothrow;
void g_value_set_flags(Value* this_, uint v_flags) nothrow;
void g_value_set_float(Value* this_, float v_float) nothrow;
void g_value_set_gtype(Value* this_, Type v_gtype) nothrow;
void g_value_set_instance(Value* this_, void* instance=null) nothrow;
void g_value_set_int(Value* this_, int v_int) nothrow;
void g_value_set_int64(Value* this_, long v_int64) nothrow;
void g_value_set_long(Value* this_, c_long v_long) nothrow;
void g_value_set_object(Value* this_, Object* v_object=null) nothrow;
void g_value_set_object_take_ownership(Value* this_, void* v_object=null) nothrow;
void g_value_set_param(Value* this_, ParamSpec* param=null) nothrow;
void g_value_set_param_take_ownership(Value* this_, ParamSpec* param=null) nothrow;
void g_value_set_pointer(Value* this_, void* v_pointer) nothrow;
void g_value_set_schar(Value* this_, byte v_char) nothrow;
void g_value_set_static_boxed(Value* this_, const(void)* v_boxed=null) nothrow;
void g_value_set_static_string(Value* this_, char* v_string=null) nothrow;
void g_value_set_string(Value* this_, char* v_string=null) nothrow;
void g_value_set_string_take_ownership(Value* this_, char* v_string=null) nothrow;
void g_value_set_uchar(Value* this_, ubyte v_uchar) nothrow;
void g_value_set_uint(Value* this_, uint v_uint) nothrow;
void g_value_set_uint64(Value* this_, ulong v_uint64) nothrow;
void g_value_set_ulong(Value* this_, c_ulong v_ulong) nothrow;
void g_value_set_variant(Value* this_, GLib2.Variant* variant=null) nothrow;
void g_value_take_boxed(Value* this_, const(void)* v_boxed=null) nothrow;
void g_value_take_object(Value* this_, void* v_object=null) nothrow;
void g_value_take_param(Value* this_, ParamSpec* param=null) nothrow;
void g_value_take_string(Value* this_, char* v_string=null) nothrow;
void g_value_take_variant(Value* this_, GLib2.Variant* variant=null) nothrow;
int g_value_transform(Value* this_, Value* dest_value) nothrow;
void g_value_unset(Value* this_) nothrow;
void g_value_register_transform_func(Type src_type, Type dest_type, ValueTransform transform_func) nothrow;
int g_value_type_compatible(Type src_type, Type dest_type) nothrow;
int g_value_type_transformable(Type src_type, Type dest_type) nothrow;
ValueArray* /*new*/ g_value_array_new(uint n_prealloced) nothrow;
ValueArray* g_value_array_append(ValueArray* this_, Value* value=null) nothrow;
ValueArray* /*new*/ g_value_array_copy(ValueArray* this_) nothrow;
void g_value_array_free(ValueArray* this_) nothrow;
Value* g_value_array_get_nth(ValueArray* this_, uint index_) nothrow;
ValueArray* g_value_array_insert(ValueArray* this_, uint index_, Value* value=null) nothrow;
ValueArray* g_value_array_prepend(ValueArray* this_, Value* value=null) nothrow;
ValueArray* g_value_array_remove(ValueArray* this_, uint index_) nothrow;
ValueArray* g_value_array_sort(ValueArray* this_, GLib2.CompareFunc compare_func) nothrow;
ValueArray* g_value_array_sort_with_data(ValueArray* this_, GLib2.CompareDataFunc compare_func, void* user_data) nothrow;
void g_weak_ref_clear(WeakRef* this_) nothrow;
Object* /*new*/ g_weak_ref_get(WeakRef* this_) nothrow;
void g_weak_ref_init(WeakRef* this_, void* object=null) nothrow;
void g_weak_ref_set(WeakRef* this_, void* object=null) nothrow;
void* g_boxed_copy(Type boxed_type, const(void)* src_boxed) nothrow;
void g_boxed_free(Type boxed_type, void* boxed) nothrow;
Type g_boxed_type_register_static(char* name, BoxedCopyFunc boxed_copy, BoxedFreeFunc boxed_free) nothrow;
void g_clear_object(Object** object_ptr) nothrow;
void g_enum_complete_type_info(Type g_enum_type, GTypeInfo* info, EnumValue* const_values) nothrow;
EnumValue* g_enum_get_value(EnumClass* enum_class, int value) nothrow;
EnumValue* g_enum_get_value_by_name(EnumClass* enum_class, char* name) nothrow;
EnumValue* g_enum_get_value_by_nick(EnumClass* enum_class, char* nick) nothrow;
Type g_enum_register_static(char* name, EnumValue* const_static_values) nothrow;
void g_flags_complete_type_info(Type g_flags_type, GTypeInfo* info, FlagsValue* const_values) nothrow;
FlagsValue* g_flags_get_first_value(FlagsClass* flags_class, uint value) nothrow;
FlagsValue* g_flags_get_value_by_name(FlagsClass* flags_class, char* name) nothrow;
FlagsValue* g_flags_get_value_by_nick(FlagsClass* flags_class, char* nick) nothrow;
Type g_flags_register_static(char* name, FlagsValue* const_static_values) nothrow;
Type g_gtype_get_type() nothrow;
ParamSpec* g_param_spec_boolean(char* name, char* nick, char* blurb, int default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_boxed(char* name, char* nick, char* blurb, Type boxed_type, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_char(char* name, char* nick, char* blurb, byte minimum, byte maximum, byte default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_double(char* name, char* nick, char* blurb, double minimum, double maximum, double default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_enum(char* name, char* nick, char* blurb, Type enum_type, int default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_flags(char* name, char* nick, char* blurb, Type flags_type, uint default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_float(char* name, char* nick, char* blurb, float minimum, float maximum, float default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_gtype(char* name, char* nick, char* blurb, Type is_a_type, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_int(char* name, char* nick, char* blurb, int minimum, int maximum, int default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_int64(char* name, char* nick, char* blurb, long minimum, long maximum, long default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_long(char* name, char* nick, char* blurb, c_long minimum, c_long maximum, c_long default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_object(char* name, char* nick, char* blurb, Type object_type, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_override(char* name, ParamSpec* overridden) nothrow;
ParamSpec* g_param_spec_param(char* name, char* nick, char* blurb, Type param_type, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_pointer(char* name, char* nick, char* blurb, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_string(char* name, char* nick, char* blurb, char* default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_uchar(char* name, char* nick, char* blurb, ubyte minimum, ubyte maximum, ubyte default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_uint(char* name, char* nick, char* blurb, uint minimum, uint maximum, uint default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_uint64(char* name, char* nick, char* blurb, ulong minimum, ulong maximum, ulong default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_ulong(char* name, char* nick, char* blurb, c_ulong minimum, c_ulong maximum, c_ulong default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_unichar(char* name, char* nick, char* blurb, dchar default_value, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_value_array(char* name, char* nick, char* blurb, ParamSpec* element_spec, ParamFlags flags) nothrow;
ParamSpec* g_param_spec_variant(char* name, char* nick, char* blurb, GLib2.VariantType* type, GLib2.Variant* default_value, ParamFlags flags) nothrow;
Type g_param_type_register_static(char* name, ParamSpecTypeInfo* pspec_info) nothrow;
int g_param_value_convert(ParamSpec* pspec, Value* src_value, Value* dest_value, int strict_validation) nothrow;
int g_param_value_defaults(ParamSpec* pspec, Value* value) nothrow;
void g_param_value_set_default(ParamSpec* pspec, Value* value) nothrow;
int g_param_value_validate(ParamSpec* pspec, Value* value) nothrow;
int g_param_values_cmp(ParamSpec* pspec, Value* value1, Value* value2) nothrow;
Type g_pointer_type_register_static(char* name) nothrow;
int g_signal_accumulator_first_wins(SignalInvocationHint* ihint, Value* return_accu, Value* handler_return, void* dummy) nothrow;
int g_signal_accumulator_true_handled(SignalInvocationHint* ihint, Value* return_accu, Value* handler_return, void* dummy) nothrow;
c_ulong g_signal_add_emission_hook(uint signal_id, GLib2.Quark detail, SignalEmissionHook hook_func, void* hook_data, GLib2.DestroyNotify data_destroy) nothrow;
void g_signal_chain_from_overridden(Value* instance_and_params, Value* return_value) nothrow;
void g_signal_chain_from_overridden_handler(void* instance, ...) nothrow;
c_ulong g_signal_connect_closure(void* instance, char* detailed_signal, Closure* closure, int after) nothrow;
c_ulong g_signal_connect_closure_by_id(void* instance, uint signal_id, GLib2.Quark detail, Closure* closure, int after) nothrow;
c_ulong g_signal_connect_data(void* instance, char* detailed_signal, Callback c_handler, void* data, ClosureNotify destroy_data, ConnectFlags connect_flags) nothrow;
c_ulong g_signal_connect_object(void* instance, char* detailed_signal, Callback c_handler, void* gobject, ConnectFlags connect_flags) nothrow;
void g_signal_emit(void* instance, uint signal_id, GLib2.Quark detail, ...) nothrow;
void g_signal_emit_by_name(void* instance, char* detailed_signal, ...) nothrow;
void g_signal_emit_valist(void* instance, uint signal_id, GLib2.Quark detail, va_list var_args) nothrow;
void g_signal_emitv(Value* instance_and_params, uint signal_id, GLib2.Quark detail, Value* return_value) nothrow;
SignalInvocationHint* g_signal_get_invocation_hint(void* instance) nothrow;
void g_signal_handler_block(void* instance, c_ulong handler_id) nothrow;
void g_signal_handler_disconnect(void* instance, c_ulong handler_id) nothrow;
c_ulong g_signal_handler_find(void* instance, SignalMatchType mask, uint signal_id, GLib2.Quark detail, Closure* closure, void* func, void* data) nothrow;
int g_signal_handler_is_connected(void* instance, c_ulong handler_id) nothrow;
void g_signal_handler_unblock(void* instance, c_ulong handler_id) nothrow;
uint g_signal_handlers_block_matched(void* instance, SignalMatchType mask, uint signal_id, GLib2.Quark detail, Closure* closure, void* func, void* data) nothrow;
void g_signal_handlers_destroy(void* instance) nothrow;
uint g_signal_handlers_disconnect_matched(void* instance, SignalMatchType mask, uint signal_id, GLib2.Quark detail, Closure* closure, void* func, void* data) nothrow;
uint g_signal_handlers_unblock_matched(void* instance, SignalMatchType mask, uint signal_id, GLib2.Quark detail, Closure* closure, void* func, void* data) nothrow;
int g_signal_has_handler_pending(void* instance, uint signal_id, GLib2.Quark detail, int may_be_blocked) nothrow;
uint* g_signal_list_ids(Type itype, /*out*/ uint* n_ids) nothrow;
uint g_signal_lookup(char* name, Type itype) nothrow;
char* g_signal_name(uint signal_id) nothrow;
uint g_signal_new(char* signal_name, Type itype, SignalFlags signal_flags, uint class_offset, SignalAccumulator accumulator, void* accu_data, SignalCMarshaller c_marshaller, Type return_type, uint n_params, ...) nothrow;
uint g_signal_new_class_handler(char* signal_name, Type itype, SignalFlags signal_flags, Callback class_handler, SignalAccumulator accumulator, void* accu_data, SignalCMarshaller c_marshaller, Type return_type, uint n_params, ...) nothrow;
uint g_signal_new_valist(char* signal_name, Type itype, SignalFlags signal_flags, Closure* class_closure, SignalAccumulator accumulator, void* accu_data, SignalCMarshaller c_marshaller, Type return_type, uint n_params, va_list args) nothrow;
uint g_signal_newv(char* signal_name, Type itype, SignalFlags signal_flags, Closure* class_closure, SignalAccumulator accumulator, void* accu_data, SignalCMarshaller c_marshaller, Type return_type, uint n_params, Type* param_types) nothrow;
void g_signal_override_class_closure(uint signal_id, Type instance_type, Closure* class_closure) nothrow;
void g_signal_override_class_handler(char* signal_name, Type instance_type, Callback class_handler) nothrow;
int g_signal_parse_name(char* detailed_signal, Type itype, /*out*/ uint* signal_id_p, /*out*/ GLib2.Quark* detail_p, int force_detail_quark) nothrow;
void g_signal_query(uint signal_id, /*out*/ SignalQuery* query) nothrow;
void g_signal_remove_emission_hook(uint signal_id, c_ulong hook_id) nothrow;
void g_signal_set_va_marshaller(uint signal_id, Type instance_type, SignalCVaMarshaller va_marshaller) nothrow;
void g_signal_stop_emission(void* instance, uint signal_id, GLib2.Quark detail) nothrow;
void g_signal_stop_emission_by_name(void* instance, char* detailed_signal) nothrow;
Closure* /*new*/ g_signal_type_cclosure_new(Type itype, uint struct_offset) nothrow;
void g_source_set_closure(GLib2.Source* source, Closure* closure) nothrow;
void g_source_set_dummy_callback(GLib2.Source* source) nothrow;
char* /*new*/ g_strdup_value_contents(Value* value) nothrow;
void g_type_add_class_cache_func(void* cache_data, TypeClassCacheFunc cache_func) nothrow;
void g_type_add_class_private(Type class_type, size_t private_size) nothrow;
void g_type_add_interface_check(void* check_data, TypeInterfaceCheckFunc check_func) nothrow;
void g_type_add_interface_dynamic(Type instance_type, Type interface_type, TypePlugin* plugin) nothrow;
void g_type_add_interface_static(Type instance_type, Type interface_type, InterfaceInfo* info) nothrow;
TypeClass* g_type_check_class_cast(TypeClass* g_class, Type is_a_type) nothrow;
int g_type_check_class_is_a(TypeClass* g_class, Type is_a_type) nothrow;
int g_type_check_instance(TypeInstance* instance) nothrow;
TypeInstance* g_type_check_instance_cast(TypeInstance* instance, Type iface_type) nothrow;
int g_type_check_instance_is_a(TypeInstance* instance, Type iface_type) nothrow;
int g_type_check_is_value_type(Type type) nothrow;
int g_type_check_value(Value* value) nothrow;
int g_type_check_value_holds(Value* value, Type type) nothrow;
Type* /*new*/ g_type_children(Type type, /*out*/ uint* n_children=null) nothrow;
TypeInstance* g_type_create_instance(Type type) nothrow;
TypeInterface* g_type_default_interface_peek(Type g_type) nothrow;
TypeInterface* g_type_default_interface_ref(Type g_type) nothrow;
void g_type_default_interface_unref(TypeInterface* g_iface) nothrow;
uint g_type_depth(Type type) nothrow;
void g_type_free_instance(TypeInstance* instance) nothrow;
Type g_type_from_name(char* name) nothrow;
Type g_type_fundamental(Type type_id) nothrow;
Type g_type_fundamental_next() nothrow;
TypePlugin* g_type_get_plugin(Type type) nothrow;
void* g_type_get_qdata(Type type, GLib2.Quark quark) nothrow;
void g_type_init() nothrow;
void g_type_init_with_debug_flags(TypeDebugFlags debug_flags) nothrow;
Type* /*new*/ g_type_interfaces(Type type, /*out*/ uint* n_interfaces=null) nothrow;
int g_type_is_a(Type type, Type is_a_type) nothrow;
char* g_type_name(Type type) nothrow;
char* g_type_name_from_class(TypeClass* g_class) nothrow;
char* g_type_name_from_instance(TypeInstance* instance) nothrow;
Type g_type_next_base(Type leaf_type, Type root_type) nothrow;
Type g_type_parent(Type type) nothrow;
GLib2.Quark g_type_qname(Type type) nothrow;
void g_type_query(Type type, /*out*/ TypeQuery* query) nothrow;
Type g_type_register_dynamic(Type parent_type, char* type_name, TypePlugin* plugin, TypeFlags flags) nothrow;
Type g_type_register_fundamental(Type type_id, char* type_name, GTypeInfo* info, TypeFundamentalInfo* finfo, TypeFlags flags) nothrow;
Type g_type_register_static(Type parent_type, char* type_name, GTypeInfo* info, TypeFlags flags) nothrow;
Type g_type_register_static_simple(Type parent_type, char* type_name, uint class_size, ClassInitFunc class_init, uint instance_size, InstanceInitFunc instance_init, TypeFlags flags) nothrow;
void g_type_remove_class_cache_func(void* cache_data, TypeClassCacheFunc cache_func) nothrow;
void g_type_remove_interface_check(void* check_data, TypeInterfaceCheckFunc check_func) nothrow;
void g_type_set_qdata(Type type, GLib2.Quark quark, void* data) nothrow;
int g_type_test_flags(Type type, uint flags) nothrow;
}

// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/Json-1.0.gir"

module Json;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;
public import gtk2.gio2;
alias gtk2.gio2 Gio2;

// package: "json-glib-1.0";
// C header: "json-glib/json-glib.h";

// c:symbol-prefixes: ["json"]
// c:identifier-prefixes: ["Json"]

// module Json;


// A JSON array type. The contents of the #JsonArray structure are private
// and should only be accessed by the provided API
struct Array {

   // Creates a new #JsonArray.
   // RETURNS: the newly created #JsonArray
   static Array* /*new*/ new_()() nothrow {
      return json_array_new();
   }
   static auto opCall()() {
      return json_array_new();
   }

   // Creates a new #JsonArray with @n_elements slots already allocated.
   // RETURNS: the newly created #JsonArray
   // <n_elements>: number of slots to pre-allocate
   static Array* /*new*/ sized_new()(uint n_elements) nothrow {
      return json_array_sized_new(n_elements);
   }
   static auto opCall()(uint n_elements) {
      return json_array_sized_new(n_elements);
   }

   // VERSION: 0.8
   // Conveniently adds an array into @array. The @array takes ownership
   // of the newly added #JsonArray
   // 
   // See also: json_array_add_element(), json_node_take_array()
   // <value>: a #JsonArray
   void add_array_element(AT0)(AT0 /*Array*/ value) nothrow {
      json_array_add_array_element(&this, UpCast!(Array*)(value));
   }

   // VERSION: 0.8
   // Conveniently adds a boolean @value into @array
   // 
   // See also: json_array_add_element(), json_node_set_boolean()
   // <value>: a boolean value
   void add_boolean_element()(int value) nothrow {
      json_array_add_boolean_element(&this, value);
   }

   // VERSION: 0.8
   // Conveniently adds a floating point @value into @array
   // 
   // See also: json_array_add_element(), json_node_set_double()
   // <value>: a floating point value
   void add_double_element()(double value) nothrow {
      json_array_add_double_element(&this, value);
   }

   // Appends @node inside @array. The array will take ownership of the
   // #JsonNode.
   // <node>: a #JsonNode
   void add_element(AT0)(AT0 /*Node*/ node) nothrow {
      json_array_add_element(&this, UpCast!(Node*)(node));
   }

   // VERSION: 0.8
   // Conveniently adds an integer @value into @array
   // 
   // See also: json_array_add_element(), json_node_set_int()
   // <value>: an integer value
   void add_int_element()(long value) nothrow {
      json_array_add_int_element(&this, value);
   }

   // VERSION: 0.8
   // Conveniently adds a null element into @array
   // 
   // See also: json_array_add_element(), %JSON_NODE_NULL
   void add_null_element()() nothrow {
      json_array_add_null_element(&this);
   }

   // VERSION: 0.8
   // Conveniently adds an object into @array. The @array takes ownership
   // of the newly added #JsonObject
   // 
   // See also: json_array_add_element(), json_node_take_object()
   // <value>: a #JsonObject
   void add_object_element(AT0)(AT0 /*Object*/ value) nothrow {
      json_array_add_object_element(&this, UpCast!(Object*)(value));
   }

   // VERSION: 0.8
   // Conveniently adds a string @value into @array
   // 
   // See also: json_array_add_element(), json_node_set_string()
   // <value>: a string value
   void add_string_element(AT0)(AT0 /*char*/ value) nothrow {
      json_array_add_string_element(&this, toCString!(char*)(value));
   }

   // VERSION: 0.6
   // Retrieves a copy of the #JsonNode containing the value of the
   // element at @index_ inside a #JsonArray
   // 
   // index. Use json_node_free() when done.
   // RETURNS: a copy of the #JsonNode at the requested
   // <index_>: the index of the element to retrieve
   Node* /*new*/ dup_element()(uint index_) nothrow {
      return json_array_dup_element(&this, index_);
   }

   // VERSION: 0.8
   // Iterates over all elements of @array and calls @func on
   // each one of them.
   // 
   // It is safe to change the value of a #JsonNode of the @array
   // from within the iterator @func, but it is not safe to add or
   // remove elements from the @array.
   // <func>: the function to be called on each element
   // <data>: data to be passed to the function
   void foreach_element(AT0)(ArrayForeach func, AT0 /*void*/ data) nothrow {
      json_array_foreach_element(&this, func, UpCast!(void*)(data));
   }

   // VERSION: 0.8
   // Conveniently retrieves the array from the element at @index_
   // inside @array
   // 
   // See also: json_array_get_element(), json_node_get_array()
   // RETURNS: the array
   // <index_>: the index of the element to retrieve
   Array* get_array_element()(uint index_) nothrow {
      return json_array_get_array_element(&this, index_);
   }

   // VERSION: 0.8
   // Conveniently retrieves the boolean value of the element at @index_
   // inside @array
   // 
   // See also: json_array_get_element(), json_node_get_boolean()
   // RETURNS: the integer value
   // <index_>: the index of the element to retrieve
   int get_boolean_element()(uint index_) nothrow {
      return json_array_get_boolean_element(&this, index_);
   }

   // VERSION: 0.8
   // Conveniently retrieves the floating point value of the element at
   // @index_ inside @array
   // 
   // See also: json_array_get_element(), json_node_get_double()
   // RETURNS: the floating point value
   // <index_>: the index of the element to retrieve
   double get_double_element()(uint index_) nothrow {
      return json_array_get_double_element(&this, index_);
   }

   // Retrieves the #JsonNode containing the value of the element at @index_
   // inside a #JsonArray.
   // RETURNS: a pointer to the #JsonNode at the requested index
   // <index_>: the index of the element to retrieve
   Node* get_element()(uint index_) nothrow {
      return json_array_get_element(&this, index_);
   }

   // Gets the elements of a #JsonArray as a list of #JsonNode<!-- -->s.
   // 
   // containing the elements of the array. The contents of the list are
   // owned by the array and should never be modified or freed. Use
   // g_list_free() on the returned list when done using it
   // RETURNS: a #GList
   GLib2.List* /*new container*/ get_elements()() nothrow {
      return json_array_get_elements(&this);
   }

   // VERSION: 0.8
   // Conveniently retrieves the integer value of the element at @index_
   // inside @array
   // 
   // See also: json_array_get_element(), json_node_get_int()
   // RETURNS: the integer value
   // <index_>: the index of the element to retrieve
   long get_int_element()(uint index_) nothrow {
      return json_array_get_int_element(&this, index_);
   }

   // Retrieves the length of a #JsonArray
   // RETURNS: the length of the array
   uint get_length()() nothrow {
      return json_array_get_length(&this);
   }

   // VERSION: 0.8
   // Conveniently retrieves whether the element at @index_ is set to null
   // 
   // See also: json_array_get_element(), JSON_NODE_TYPE(), %JSON_NODE_NULL
   // RETURNS: %TRUE if the element is null
   // <index_>: the index of the element to retrieve
   int get_null_element()(uint index_) nothrow {
      return json_array_get_null_element(&this, index_);
   }

   // VERSION: 0.8
   // Conveniently retrieves the object from the element at @index_
   // inside @array
   // 
   // See also: json_array_get_element(), json_node_get_object()
   // RETURNS: the object
   // <index_>: the index of the element to retrieve
   Object* get_object_element()(uint index_) nothrow {
      return json_array_get_object_element(&this, index_);
   }

   // VERSION: 0.8
   // Conveniently retrieves the string value of the element at @index_
   // inside @array
   // 
   // See also: json_array_get_element(), json_node_get_string()
   // 
   // the #JsonArray and should not be modified or freed
   // RETURNS: the string value; the returned string is owned by
   // <index_>: the index of the element to retrieve
   char* get_string_element()(uint index_) nothrow {
      return json_array_get_string_element(&this, index_);
   }

   // Increase by one the reference count of a #JsonArray.
   // 
   // increased by one.
   // RETURNS: the passed #JsonArray, with the reference count
   Array* /*new*/ ref_()() nothrow {
      return json_array_ref(&this);
   }

   // Removes the #JsonNode inside @array at @index_ freeing its allocated
   // resources.
   // <index_>: the position of the element to be removed
   void remove_element()(uint index_) nothrow {
      json_array_remove_element(&this, index_);
   }

   // Decreases by one the reference count of a #JsonArray. If the
   // reference count reaches zero, the array is destroyed and all
   // its allocated resources are freed.
   void unref()() nothrow {
      json_array_unref(&this);
   }
}


// VERSION: 0.8
// The function to be passed to json_array_foreach_element(). You
// should not add or remove elements to and from @array within
// this function. It is safe to change the value of @element_node.
// <array>: the iterated #JsonArray
// <index_>: the index of the element
// <element_node>: a #JsonNode containing the value at @index_
// <user_data>: data passed to the function
extern (C) alias void function (Array* array, uint index_, Node* element_node, void* user_data) nothrow ArrayForeach;


// Unintrospectable callback: BoxedDeserializeFunc() / ()
// VERSION: 0.10
// Deserializes the contents of the passed #JsonNode into a #GBoxed
// RETURNS: the newly created boxed type
// <node>: a #JsonNode
extern (C) alias void* function (Node* node) nothrow BoxedDeserializeFunc;


// VERSION: 0.10
// Serializes the passed #GBoxed and stores it inside a #JsonNode
// RETURNS: the newly created #JsonNode
// <boxed>: a #GBoxed
extern (C) alias Node* /*new*/ function (const(void)* boxed) nothrow BoxedSerializeFunc;


// The <structname>JsonBuilder</structname> structure contains only
// private data and shouls be accessed using the provided API
struct Builder /* : GObject.Object */ /* Version 0.12 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private BuilderPrivate* priv;


   // Creates a new #JsonBuilder. You can use this object to generate a
   // JSON tree and obtain the root #JsonNode<!-- -->s.
   // RETURNS: the newly created #JsonBuilder instance
   static Builder* /*new*/ new_()() nothrow {
      return json_builder_new();
   }
   static auto opCall()() {
      return json_builder_new();
   }

   // If called after json_builder_set_member_name(), sets @value as member of the
   // most recent opened object, otherwise @value is added as element of the most
   // recent opened array.
   // 
   // See also: json_builder_add_value()
   // RETURNS: the #JsonBuilder, or %NULL if the call was inconsistent
   // <value>: the value of the member or element
   Builder* add_boolean_value()(int value) nothrow {
      return json_builder_add_boolean_value(&this, value);
   }

   // If called after json_builder_set_member_name(), sets @value as member of the
   // most recent opened object, otherwise @value is added as element of the most
   // recent opened array.
   // 
   // See also: json_builder_add_value()
   // RETURNS: the #JsonBuilder, or %NULL if the call was inconsistent
   // <value>: the value of the member or element
   Builder* add_double_value()(double value) nothrow {
      return json_builder_add_double_value(&this, value);
   }

   // If called after json_builder_set_member_name(), sets @value as member of the
   // most recent opened object, otherwise @value is added as element of the most
   // recent opened array.
   // 
   // See also: json_builder_add_value()
   // RETURNS: the #JsonBuilder, or %NULL if the call was inconsistent
   // <value>: the value of the member or element
   Builder* add_int_value()(long value) nothrow {
      return json_builder_add_int_value(&this, value);
   }

   // If called after json_builder_set_member_name(), sets null as member of the
   // most recent opened object, otherwise null is added as element of the most
   // recent opened array.
   // 
   // See also: json_builder_add_value()
   // RETURNS: the #JsonBuilder, or %NULL if the call was inconsistent
   Builder* add_null_value()() nothrow {
      return json_builder_add_null_value(&this);
   }

   // If called after json_builder_set_member_name(), sets @value as member of the
   // most recent opened object, otherwise @value is added as element of the most
   // recent opened array.
   // 
   // See also: json_builder_add_value()
   // RETURNS: the #JsonBuilder, or %NULL if the call was inconsistent
   // <value>: the value of the member or element
   Builder* add_string_value(AT0)(AT0 /*char*/ value) nothrow {
      return json_builder_add_string_value(&this, toCString!(char*)(value));
   }

   // If called after json_builder_set_member_name(), sets @node as member of the
   // most recent opened object, otherwise @node is added as element of the most
   // recent opened array.
   // RETURNS: the #JsonBuilder, or %NULL if the call was inconsistent
   // <node>: the value of the member or element
   Builder* add_value(AT0)(AT0 /*Node*/ node) nothrow {
      return json_builder_add_value(&this, UpCast!(Node*)(node));
   }

   // Opens a subarray inside the given @builder. When done adding members to
   // the subarray, json_builder_end_array() must be called.
   // 
   // Can be called for first or only if the call is associated to an object member
   // or an array element.
   // RETURNS: the #JsonBuilder, or %NULL if the call was inconsistent
   Builder* begin_array()() nothrow {
      return json_builder_begin_array(&this);
   }

   // Opens a subobject inside the given @builder. When done adding members to
   // the subobject, json_builder_end_object() must be called.
   // 
   // Can be called for first or only if the call is associated to an object member
   // or an array element.
   // RETURNS: the #JsonBuilder, or %NULL if the call was inconsistent
   Builder* begin_object()() nothrow {
      return json_builder_begin_object(&this);
   }

   // Closes the subarray inside the given @builder that was opened by the most
   // recent call to json_builder_begin_array().
   // 
   // Cannot be called after json_builder_set_member_name().
   // RETURNS: the #JsonBuilder, or %NULL if the call was inconsistent
   Builder* end_array()() nothrow {
      return json_builder_end_array(&this);
   }

   // Closes the subobject inside the given @builder that was opened by the most
   // recent call to json_builder_begin_object().
   // 
   // Cannot be called after json_builder_set_member_name().
   // RETURNS: the #JsonBuilder, or %NULL if the call was inconsistent
   Builder* end_object()() nothrow {
      return json_builder_end_object(&this);
   }

   // Returns the root of the current constructed tree, if the build is complete
   // (ie: all opened objects, object members and arrays are being closed).
   // 
   // Free the returned value with json_node_free().
   // RETURNS: the #JsonNode, or %NULL if the build is not complete.
   Node* /*new*/ get_root()() nothrow {
      return json_builder_get_root(&this);
   }
   // Resets the state of the @builder back to its initial state.
   void reset()() nothrow {
      json_builder_reset(&this);
   }

   // Set the name of the next member in an object. The next call must add a value,
   // open an object or an array.
   // 
   // Can be called only if the call is associated to an object.
   // RETURNS: the #JsonBuilder, or %NULL if the call was inconsistent
   // <member_name>: the name of the member
   Builder* set_member_name(AT0)(AT0 /*char*/ member_name) nothrow {
      return json_builder_set_member_name(&this, toCString!(char*)(member_name));
   }
}


// The <structname>JsonBuilder</structname> structure contains only
// private data
struct BuilderClass /* Version 0.12 */ {
   private GObject2.ObjectClass parent_class;
   extern (C) void function () nothrow _json_reserved1;
   extern (C) void function () nothrow _json_reserved2;
}

struct BuilderPrivate {
}


// JSON data streams generator. The contents of the #JsonGenerator structure
// are private and should only be accessed via the provided API.
struct Generator /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private GeneratorPrivate* priv;


   // Creates a new #JsonGenerator. You can use this object to generate a
   // JSON data stream starting from a data object model composed by
   // #JsonNode<!-- -->s.
   // RETURNS: the newly created #JsonGenerator instance
   static Generator* /*new*/ new_()() nothrow {
      return json_generator_new();
   }
   static auto opCall()() {
      return json_generator_new();
   }

   // VERSION: 0.14
   // Retrieves the value set using json_generator_set_indent().
   // RETURNS: the number of repetitions per indentation level
   uint get_indent()() nothrow {
      return json_generator_get_indent(&this);
   }

   // VERSION: 0.14
   // Retrieves the value set using json_generator_set_indent_char().
   // RETURNS: the character to be used when indenting
   dchar get_indent_char()() nothrow {
      return json_generator_get_indent_char(&this);
   }

   // VERSION: 0.14
   // Retrieves the value set using json_generator_set_pretty().
   // 
   // %FALSE otherwise
   // RETURNS: %TRUE if the generated JSON should be pretty-printed, and
   int get_pretty()() nothrow {
      return json_generator_get_pretty(&this);
   }

   // VERSION: 0.14
   // Retrieves a pointer to the root #JsonNode set using
   // json_generator_set_root().
   // 
   // is owned by the #JsonGenerator and it should not be freed
   // RETURNS: a #JsonNode, or %NULL. The returned node
   Node* get_root()() nothrow {
      return json_generator_get_root(&this);
   }

   // VERSION: 0.14
   // Sets the number of repetitions for each indentation level.
   // <indent_level>: the number of repetitions of the indentation character that should be applied when pretty printing
   void set_indent()(uint indent_level) nothrow {
      json_generator_set_indent(&this, indent_level);
   }

   // VERSION: 0.14
   // Sets the character to be used when indenting
   // <indent_char>: a Unicode character to be used when indenting
   void set_indent_char()(dchar indent_char) nothrow {
      json_generator_set_indent_char(&this, indent_char);
   }

   // VERSION: 0.14
   // Sets whether the generated JSON should be pretty printed, using the
   // indentation character specified in the #JsonGenerator:indent-char
   // property and the spacing specified in #JsonGenerator:indent property.
   // <is_pretty>: whether the generated string should be pretty printed
   void set_pretty()(int is_pretty) nothrow {
      json_generator_set_pretty(&this, is_pretty);
   }

   // Sets @node as the root of the JSON data stream to be serialized by
   // the #JsonGenerator.
   // 
   // <note>The node is copied by the generator object, so it can be safely
   // freed after calling this function.</note>
   // <node>: a #JsonNode
   void set_root(AT0)(AT0 /*Node*/ node) nothrow {
      json_generator_set_root(&this, UpCast!(Node*)(node));
   }

   // Generates a JSON data stream from @generator and returns it as a
   // buffer.
   // 
   // Use g_free() to free the allocated resources.
   // RETURNS: a newly allocated buffer holding a JSON data stream.
   // <length>: return location for the length of the returned buffer, or %NULL
   char* /*new*/ to_data(AT0)(/*out*/ AT0 /*size_t*/ length) nothrow {
      return json_generator_to_data(&this, UpCast!(size_t*)(length));
   }

   // Creates a JSON data stream and puts it inside @filename, overwriting the
   // current file contents. This operation is atomic.
   // RETURNS: %TRUE if saving was successful.
   // <filename>: path to the target file
   int to_file(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) nothrow {
      return json_generator_to_file(&this, toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 0.12
   // Outputs JSON data and streams it (synchronously) to @stream.
   // 
   // on failure. In case of error, the #GError will be filled accordingly
   // RETURNS: %TRUE if the write operation was successful, and %FALSE
   // <stream>: a #GOutputStream
   // <cancellable>: a #GCancellable, or %NULL
   int to_stream(AT0, AT1, AT2)(AT0 /*Gio2.OutputStream*/ stream, AT1 /*Gio2.Cancellable*/ cancellable, AT2 /*GLib2.Error**/ error=null) nothrow {
      return json_generator_to_stream(&this, UpCast!(Gio2.OutputStream*)(stream), UpCast!(Gio2.Cancellable*)(cancellable), UpCast!(GLib2.Error**)(error));
   }
}

// #JsonGenerator class
struct GeneratorClass {
   private GObject2.ObjectClass parent_class;
   extern (C) void function () nothrow _json_reserved1;
   extern (C) void function () nothrow _json_reserved2;
   extern (C) void function () nothrow _json_reserved3;
   extern (C) void function () nothrow _json_reserved4;
}

struct GeneratorPrivate {
}


// A generic container of JSON data types. The contents of the #JsonNode
// structure are private and should only be accessed via the provided
// functions and never directly.
struct Node {

   // Creates a new #JsonNode of @type.
   // RETURNS: the newly created #JsonNode
   // <type>: a #JsonNodeType
   static Node* /*new*/ new_()(NodeType type) nothrow {
      return json_node_new(type);
   }
   static auto opCall()(NodeType type) {
      return json_node_new(type);
   }

   // Copies @node. If the node contains complex data types then the reference
   // count of the objects is increased.
   // RETURNS: the copied #JsonNode
   Node* /*new*/ copy()() nothrow {
      return json_node_copy(&this);
   }

   // Retrieves the #JsonArray stored inside a #JsonNode and returns it
   // with its reference count increased by one.
   // 
   // count increased.
   // RETURNS: the #JsonArray with its reference
   Array* /*new*/ dup_array()() nothrow {
      return json_node_dup_array(&this);
   }

   // Retrieves the #JsonObject inside @node. The reference count of
   // the returned object is increased.
   // RETURNS: the #JsonObject
   Object* /*new*/ dup_object()() nothrow {
      return json_node_dup_object(&this);
   }

   // Gets a copy of the string value stored inside a #JsonNode
   // 
   // of the #JsonNode contents. Use g_free() to free the allocated resources
   // RETURNS: a newly allocated string containing a copy
   char* /*new*/ dup_string()() nothrow {
      return json_node_dup_string(&this);
   }
   // Frees the resources allocated by @node.
   void free()() nothrow {
      json_node_free(&this);
   }

   // Retrieves the #JsonArray stored inside a #JsonNode
   // RETURNS: the #JsonArray
   Array* get_array()() nothrow {
      return json_node_get_array(&this);
   }

   // Gets the boolean value stored inside a #JsonNode
   // RETURNS: a boolean value.
   int get_boolean()() nothrow {
      return json_node_get_boolean(&this);
   }

   // Gets the double value stored inside a #JsonNode
   // RETURNS: a double value.
   double get_double()() nothrow {
      return json_node_get_double(&this);
   }

   // Gets the integer value stored inside a #JsonNode
   // RETURNS: an integer value.
   long get_int()() nothrow {
      return json_node_get_int(&this);
   }

   // VERSION: 0.8
   // Retrieves the #JsonNodeType of @node
   // RETURNS: the type of the node
   NodeType get_node_type()() nothrow {
      return json_node_get_node_type(&this);
   }

   // Retrieves the #JsonObject stored inside a #JsonNode
   // RETURNS: the #JsonObject
   Object* get_object()() nothrow {
      return json_node_get_object(&this);
   }

   // Retrieves the parent #JsonNode of @node.
   // 
   // the root node
   // RETURNS: the parent node, or %NULL if @node is
   Node* get_parent()() nothrow {
      return json_node_get_parent(&this);
   }

   // Gets the string value stored inside a #JsonNode
   // RETURNS: a string value.
   char* get_string()() nothrow {
      return json_node_get_string(&this);
   }

   // Retrieves a value from a #JsonNode and copies into @value. When done
   // using it, call g_value_unset() on the #GValue.
   // <value>: return location for an uninitialized value
   void get_value(AT0)(/*out*/ AT0 /*GObject2.Value*/ value) nothrow {
      json_node_get_value(&this, UpCast!(GObject2.Value*)(value));
   }

   // VERSION: 0.4
   // Returns the #GType of the payload of the node.
   // RETURNS: a #GType for the payload.
   Type get_value_type()() nothrow {
      return json_node_get_value_type(&this);
   }

   // VERSION: 0.8
   // Checks whether @node is a %JSON_NODE_NULL
   // 
   // <note>A null node is not the same as a %NULL #JsonNode</note>
   // RETURNS: %TRUE if the node is null
   int is_null()() nothrow {
      return json_node_is_null(&this);
   }

   // Sets @array inside @node and increases the #JsonArray reference count
   // <array>: a #JsonArray
   void set_array(AT0)(AT0 /*Array*/ array) nothrow {
      json_node_set_array(&this, UpCast!(Array*)(array));
   }

   // Sets @value as the boolean content of the @node, replacing any existing
   // content.
   // <value>: a boolean value
   void set_boolean()(int value) nothrow {
      json_node_set_boolean(&this, value);
   }

   // Sets @value as the double content of the @node, replacing any existing
   // content.
   // <value>: a double value
   void set_double()(double value) nothrow {
      json_node_set_double(&this, value);
   }

   // Sets @value as the integer content of the @node, replacing any existing
   // content.
   // <value>: an integer value
   void set_int()(long value) nothrow {
      json_node_set_int(&this, value);
   }

   // Sets @objects inside @node. The reference count of @object is increased.
   // <object>: a #JsonObject
   void set_object(AT0)(AT0 /*Object*/ object) nothrow {
      json_node_set_object(&this, UpCast!(Object*)(object));
   }

   // VERSION: 0.8
   // Sets the parent #JsonNode of @node
   // <parent>: the parent #JsonNode of @node
   void set_parent(AT0)(AT0 /*Node*/ parent) nothrow {
      json_node_set_parent(&this, UpCast!(Node*)(parent));
   }

   // Sets @value as the string content of the @node, replacing any existing
   // content.
   // <value>: a string value
   void set_string(AT0)(AT0 /*char*/ value) nothrow {
      json_node_set_string(&this, toCString!(char*)(value));
   }

   // Sets @value inside @node. The passed #GValue is copied into the #JsonNode
   // <value>: the #GValue to set
   void set_value(AT0)(AT0 /*GObject2.Value*/ value) nothrow {
      json_node_set_value(&this, UpCast!(GObject2.Value*)(value));
   }

   // Sets @array into @node without increasing the #JsonArray reference count.
   // <array>: a #JsonArray
   void take_array(AT0)(AT0 /*Array*/ array) nothrow {
      json_node_take_array(&this, UpCast!(Array*)(array));
   }

   // Sets @object inside @node. The reference count of @object is not increased.
   // <object>: a #JsonObject
   void take_object(AT0)(AT0 /*Object*/ object) nothrow {
      json_node_take_object(&this, UpCast!(Object*)(object));
   }

   // Retrieves the user readable name of the data type contained by @node.
   // 
   // is owned by the node and should never be modified or freed
   // RETURNS: a string containing the name of the type. The returned string
   char* type_name()() nothrow {
      return json_node_type_name(&this);
   }
}

// Indicates the content of a #JsonNode.
enum NodeType {
   OBJECT = 0,
   ARRAY = 1,
   VALUE = 2,
   NULL = 3
}

// A JSON object type. The contents of the #JsonObject structure are private
// and should only be accessed by the provided API
struct Object {

   // Creates a new #JsonObject, an JSON object type representation.
   // RETURNS: the newly created #JsonObject
   static Object* /*new*/ new_()() nothrow {
      return json_object_new();
   }
   static auto opCall()() {
      return json_object_new();
   }

   // DEPRECATED (v0.8) method: add_member - Use json_object_set_member() instead
   // Adds a member named @member_name and containing @node into a #JsonObject.
   // The object will take ownership of the #JsonNode.
   // 
   // This function will return if the @object already contains a member
   // @member_name.
   // <member_name>: the name of the member
   // <node>: the value of the member
   void add_member(AT0, AT1)(AT0 /*char*/ member_name, AT1 /*Node*/ node) nothrow {
      json_object_add_member(&this, toCString!(char*)(member_name), UpCast!(Node*)(node));
   }

   // VERSION: 0.6
   // Retrieves a copy of the #JsonNode containing the value of @member_name
   // inside a #JsonObject
   // 
   // object member or %NULL. Use json_node_free() when done.
   // RETURNS: a copy of the node for the requested
   // <member_name>: the name of the JSON object member to access
   Node* /*new*/ dup_member(AT0)(AT0 /*char*/ member_name) nothrow {
      return json_object_dup_member(&this, toCString!(char*)(member_name));
   }

   // VERSION: 0.8
   // Iterates over all members of @object and calls @func on
   // each one of them.
   // 
   // It is safe to change the value of a #JsonNode of the @object
   // from within the iterator @func, but it is not safe to add or
   // remove members from the @object.
   // <func>: the function to be called on each member
   // <data>: data to be passed to the function
   void foreach_member(AT0)(ObjectForeach func, AT0 /*void*/ data) nothrow {
      json_object_foreach_member(&this, func, UpCast!(void*)(data));
   }

   // VERSION: 0.8
   // Convenience function that retrieves the array
   // stored in @member_name of @object
   // 
   // See also: json_object_get_member()
   // RETURNS: the array inside the object's member
   // <member_name>: the name of the member
   Array* get_array_member(AT0)(AT0 /*char*/ member_name) nothrow {
      return json_object_get_array_member(&this, toCString!(char*)(member_name));
   }

   // VERSION: 0.8
   // Convenience function that retrieves the boolean value
   // stored in @member_name of @object
   // 
   // See also: json_object_get_member()
   // RETURNS: the boolean value of the object's member
   // <member_name>: the name of the member
   int get_boolean_member(AT0)(AT0 /*char*/ member_name) nothrow {
      return json_object_get_boolean_member(&this, toCString!(char*)(member_name));
   }

   // VERSION: 0.8
   // Convenience function that retrieves the floating point value
   // stored in @member_name of @object
   // 
   // See also: json_object_get_member()
   // RETURNS: the floating point value of the object's member
   // <member_name>: the name of the member
   double get_double_member(AT0)(AT0 /*char*/ member_name) nothrow {
      return json_object_get_double_member(&this, toCString!(char*)(member_name));
   }

   // VERSION: 0.8
   // Convenience function that retrieves the integer value
   // stored in @member_name of @object
   // 
   // See also: json_object_get_member()
   // RETURNS: the integer value of the object's member
   // <member_name>: the name of the member
   long get_int_member(AT0)(AT0 /*char*/ member_name) nothrow {
      return json_object_get_int_member(&this, toCString!(char*)(member_name));
   }

   // Retrieves the #JsonNode containing the value of @member_name inside
   // a #JsonObject.
   // 
   // member, or %NULL
   // RETURNS: a pointer to the node for the requested object
   // <member_name>: the name of the JSON object member to access
   Node* get_member(AT0)(AT0 /*char*/ member_name) nothrow {
      return json_object_get_member(&this, toCString!(char*)(member_name));
   }

   // Retrieves all the names of the members of a #JsonObject. You can
   // obtain the value for each member using json_object_get_member().
   // 
   // of member names. The content of the list is owned by the #JsonObject
   // and should never be modified or freed. When you have finished using
   // the returned list, use g_list_free() to free the resources it has
   // allocated.
   // RETURNS: a #GList
   GLib2.List* /*new container*/ get_members()() nothrow {
      return json_object_get_members(&this);
   }

   // VERSION: 0.8
   // Convenience function that checks whether the value
   // stored in @member_name of @object is null
   // 
   // See also: json_object_get_member()
   // RETURNS: %TRUE if the value is null
   // <member_name>: the name of the member
   int get_null_member(AT0)(AT0 /*char*/ member_name) nothrow {
      return json_object_get_null_member(&this, toCString!(char*)(member_name));
   }

   // VERSION: 0.8
   // Convenience function that retrieves the object
   // stored in @member_name of @object
   // 
   // See also: json_object_get_member()
   // RETURNS: the object inside the object's member
   // <member_name>: the name of the member
   Object* get_object_member(AT0)(AT0 /*char*/ member_name) nothrow {
      return json_object_get_object_member(&this, toCString!(char*)(member_name));
   }

   // Retrieves the number of members of a #JsonObject.
   // RETURNS: the number of members
   uint get_size()() nothrow {
      return json_object_get_size(&this);
   }

   // VERSION: 0.8
   // Convenience function that retrieves the string value
   // stored in @member_name of @object
   // 
   // See also: json_object_get_member()
   // RETURNS: the string value of the object's member
   // <member_name>: the name of the member
   char* get_string_member(AT0)(AT0 /*char*/ member_name) nothrow {
      return json_object_get_string_member(&this, toCString!(char*)(member_name));
   }

   // Retrieves all the values of the members of a #JsonObject.
   // 
   // #JsonNode<!-- -->s. The content of the list is owned by the #JsonObject
   // and should never be modified or freed. When you have finished using the
   // returned list, use g_list_free() to free the resources it has allocated.
   // RETURNS: a #GList of
   GLib2.List* /*new container*/ get_values()() nothrow {
      return json_object_get_values(&this);
   }

   // Checks whether @object has a member named @member_name.
   // RETURNS: %TRUE if the JSON object has the requested member
   // <member_name>: the name of a JSON object member
   int has_member(AT0)(AT0 /*char*/ member_name) nothrow {
      return json_object_has_member(&this, toCString!(char*)(member_name));
   }

   // Increase by one the reference count of a #JsonObject.
   // 
   // increased by one.
   // RETURNS: the passed #JsonObject, with the reference count
   Object* /*new*/ ref_()() nothrow {
      return json_object_ref(&this);
   }

   // Removes @member_name from @object, freeing its allocated resources.
   // <member_name>: the name of the member to remove
   void remove_member(AT0)(AT0 /*char*/ member_name) nothrow {
      json_object_remove_member(&this, toCString!(char*)(member_name));
   }

   // VERSION: 0.8
   // Convenience function for setting an array @value of
   // @member_name inside @object.
   // 
   // The @object will take ownership of the passed #JsonArray
   // 
   // See also: json_object_set_member()
   // <member_name>: the name of the member
   // <value>: the value of the member
   void set_array_member(AT0, AT1)(AT0 /*char*/ member_name, AT1 /*Array*/ value) nothrow {
      json_object_set_array_member(&this, toCString!(char*)(member_name), UpCast!(Array*)(value));
   }

   // VERSION: 0.8
   // Convenience function for setting a boolean @value of
   // @member_name inside @object.
   // 
   // See also: json_object_set_member()
   // <member_name>: the name of the member
   // <value>: the value of the member
   void set_boolean_member(AT0)(AT0 /*char*/ member_name, int value) nothrow {
      json_object_set_boolean_member(&this, toCString!(char*)(member_name), value);
   }

   // VERSION: 0.8
   // Convenience function for setting a floating point @value
   // of @member_name inside @object.
   // 
   // See also: json_object_set_member()
   // <member_name>: the name of the member
   // <value>: the value of the member
   void set_double_member(AT0)(AT0 /*char*/ member_name, double value) nothrow {
      json_object_set_double_member(&this, toCString!(char*)(member_name), value);
   }

   // VERSION: 0.8
   // Convenience function for setting an integer @value of
   // @member_name inside @object.
   // 
   // See also: json_object_set_member()
   // <member_name>: the name of the member
   // <value>: the value of the member
   void set_int_member(AT0)(AT0 /*char*/ member_name, long value) nothrow {
      json_object_set_int_member(&this, toCString!(char*)(member_name), value);
   }

   // VERSION: 0.8
   // Sets @node as the value of @member_name inside @object.
   // 
   // If @object already contains a member called @member_name then
   // the member's current value is overwritten. Otherwise, a new
   // member is added to @object.
   // <member_name>: the name of the member
   // <node>: the value of the member
   void set_member(AT0, AT1)(AT0 /*char*/ member_name, AT1 /*Node*/ node) nothrow {
      json_object_set_member(&this, toCString!(char*)(member_name), UpCast!(Node*)(node));
   }

   // VERSION: 0.8
   // Convenience function for setting a null @value of
   // @member_name inside @object.
   // 
   // See also: json_object_set_member()
   // <member_name>: the name of the member
   void set_null_member(AT0)(AT0 /*char*/ member_name) nothrow {
      json_object_set_null_member(&this, toCString!(char*)(member_name));
   }

   // VERSION: 0.8
   // Convenience function for setting an object @value of
   // @member_name inside @object.
   // 
   // The @object will take ownership of the passed #JsonObject
   // 
   // See also: json_object_set_member()
   // <member_name>: the name of the member
   // <value>: the value of the member
   void set_object_member(AT0, AT1)(AT0 /*char*/ member_name, AT1 /*Object*/ value) nothrow {
      json_object_set_object_member(&this, toCString!(char*)(member_name), UpCast!(Object*)(value));
   }

   // VERSION: 0.8
   // Convenience function for setting a string @value of
   // @member_name inside @object.
   // 
   // See also: json_object_set_member()
   // <member_name>: the name of the member
   // <value>: the value of the member
   void set_string_member(AT0, AT1)(AT0 /*char*/ member_name, AT1 /*char*/ value) nothrow {
      json_object_set_string_member(&this, toCString!(char*)(member_name), toCString!(char*)(value));
   }

   // Decreases by one the reference count of a #JsonObject. If the
   // reference count reaches zero, the object is destroyed and all
   // its allocated resources are freed.
   void unref()() nothrow {
      json_object_unref(&this);
   }
}


// VERSION: 0.8
// The function to be passed to json_object_foreach_member(). You
// should not add or remove members to and from @object within
// this function. It is safe to change the value of @member_node.
// <object>: the iterated #JsonObject
// <member_name>: the name of the member
// <member_node>: a #JsonNode containing the @member_name value
// <user_data>: data passed to the function
extern (C) alias void function (Object* object, char* member_name, Node* member_node, void* user_data) nothrow ObjectForeach;


// JSON data streams parser. The contents of the #JsonParser structure are
// private and should only be accessed via the provided API.
struct Parser /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private ParserPrivate* priv;


   // Creates a new #JsonParser instance. You can use the #JsonParser to
   // load a JSON stream from either a file or a buffer and then walk the
   // hierarchy using the data types API.
   // 
   // to release all the memory it allocates.
   // RETURNS: the newly created #JsonParser. Use g_object_unref()
   static Parser* /*new*/ new_()() nothrow {
      return json_parser_new();
   }
   static auto opCall()() {
      return json_parser_new();
   }
   static GLib2.Quark error_quark()() nothrow {
      return json_parser_error_quark();
   }

   // Retrieves the line currently parsed, starting from 1.
   // 
   // This function has defined behaviour only while parsing; calling this
   // function from outside the signal handlers emitted by #JsonParser will
   // yield 0.
   // RETURNS: the currently parsed line, or 0.
   uint get_current_line()() nothrow {
      return json_parser_get_current_line(&this);
   }

   // Retrieves the current position inside the current line, starting
   // from 0.
   // 
   // This function has defined behaviour only while parsing; calling this
   // function from outside the signal handlers emitted by #JsonParser will
   // yield 0.
   // RETURNS: the position in the current line, or 0.
   uint get_current_pos()() nothrow {
      return json_parser_get_current_pos(&this);
   }

   // Retrieves the top level node from the parsed JSON stream.
   // 
   // node is owned by the #JsonParser and should never be modified
   // or freed.
   // RETURNS: the root #JsonNode . The returned
   Node* get_root()() nothrow {
      return json_parser_get_root(&this);
   }

   // VERSION: 0.4
   // A JSON data stream might sometimes contain an assignment, like:
   // 
   // |[
   // var _json_data = { "member_name" : [ ...
   // ]|
   // 
   // even though it would technically constitute a violation of the RFC.
   // 
   // #JsonParser will ignore the left hand identifier and parse the right
   // hand value of the assignment. #JsonParser will record, though, the
   // existence of the assignment in the data stream and the variable name
   // used.
   // 
   // @variable_name is not %NULL it will be set to the name of the variable
   // used in the assignment. The string is owned by #JsonParser and should
   // never be modified or freed.
   // RETURNS: %TRUE if there was an assignment, %FALSE otherwise. If
   // <variable_name>: Return location for the variable name, or %NULL
   int has_assignment(AT0)(/*out*/ AT0 /*char**/ variable_name=null) nothrow {
      return json_parser_has_assignment(&this, toCString!(char**)(variable_name));
   }

   // Loads a JSON stream from a buffer and parses it. You can call this function
   // multiple times with the same #JsonParser object, but the contents of the
   // parser will be destroyed each time.
   // 
   // of error, @error is set accordingly and %FALSE is returned
   // RETURNS: %TRUE if the buffer was succesfully parser. In case
   // <data>: the buffer to parse
   // <length>: the length of the buffer, or -1
   int load_from_data(AT0, AT1)(AT0 /*char*/ data, ssize_t length, AT1 /*GLib2.Error**/ error=null) nothrow {
      return json_parser_load_from_data(&this, toCString!(char*)(data), length, UpCast!(GLib2.Error**)(error));
   }

   // Loads a JSON stream from the content of @filename and parses it. See
   // json_parser_load_from_data().
   // 
   // In case of error, @error is set accordingly and %FALSE is returned
   // RETURNS: %TRUE if the file was successfully loaded and parsed.
   // <filename>: the path for the file to parse
   int load_from_file(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) nothrow {
      return json_parser_load_from_file(&this, toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 0.12
   // Loads the contents of an input stream and parses them.
   // 
   // If @cancellable is not %NULL, then the operation can be cancelled by
   // triggering the @cancellable object from another thread. If the
   // operation was cancelled, the error %G_IO_ERROR_CANCELLED will be set
   // on the passed @error.
   // 
   // parsed, and %FALSE otherwise
   // RETURNS: %TRUE if the data stream was successfully read and
   // <stream>: an open #GInputStream
   // <cancellable>: a #GCancellable, or %NULL
   int load_from_stream(AT0, AT1, AT2)(AT0 /*Gio2.InputStream*/ stream, AT1 /*Gio2.Cancellable*/ cancellable, AT2 /*GLib2.Error**/ error=null) nothrow {
      return json_parser_load_from_stream(&this, UpCast!(Gio2.InputStream*)(stream), UpCast!(Gio2.Cancellable*)(cancellable), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 0.12
   // Asynchronously reads the contents of @stream.
   // 
   // For more details, see json_parser_load_from_stream() which is the
   // synchronous version of this call.
   // 
   // When the operation is finished, @callback will be called. You should
   // then call json_parser_load_from_stream_finish() to get the result
   // of the operation.
   // <stream>: a #GInputStream
   // <cancellable>: a #GCancellable, or %NULL
   // <callback>: a #GAsyncReadyCallback to call when the request is satisfied
   // <user_data>: the data to pass to @callback
   void load_from_stream_async(AT0, AT1, AT2)(AT0 /*Gio2.InputStream*/ stream, AT1 /*Gio2.Cancellable*/ cancellable, Gio2.AsyncReadyCallback callback, AT2 /*void*/ user_data) nothrow {
      json_parser_load_from_stream_async(&this, UpCast!(Gio2.InputStream*)(stream), UpCast!(Gio2.Cancellable*)(cancellable), callback, UpCast!(void*)(user_data));
   }

   // VERSION: 0.12
   // Finishes an asynchronous stream loading started with
   // json_parser_load_from_stream_async().
   // 
   // and parsed, and %FALSE otherwise. In case of error, the #GError will be
   // filled accordingly.
   // RETURNS: %TRUE if the content of the stream was successfully retrieves
   // <result>: a #GAsyncResult
   int load_from_stream_finish(AT0, AT1)(AT0 /*Gio2.AsyncResult*/ result, AT1 /*GLib2.Error**/ error=null) nothrow {
      return json_parser_load_from_stream_finish(&this, UpCast!(Gio2.AsyncResult*)(result), UpCast!(GLib2.Error**)(error));
   }

   // The ::array-element signal is emitted each time the #JsonParser
   // has successfully parsed a single element of a #JsonArray. The
   // array and element index are passed to the signal handlers.
   // <array>: a #JsonArray
   // <index_>: the index of the newly parsed element
   extern (C) alias static void function (Parser* this_, Array* array, int index_, void* user_data=null) nothrow signal_array_element;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"array-element", CB/*:signal_array_element*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_array_element)||_ttmm!(CB, signal_array_element)()) {
      return signal_connect_data!()(&this, cast(char*)"array-element",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // The ::array-end signal is emitted each time the #JsonParser
   // has successfully parsed an entire #JsonArray
   // <array>: the parsed #JsonArray
   extern (C) alias static void function (Parser* this_, Array* array, void* user_data=null) nothrow signal_array_end;
   ulong signal_connect(string name:"array-end", CB/*:signal_array_end*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_array_end)||_ttmm!(CB, signal_array_end)()) {
      return signal_connect_data!()(&this, cast(char*)"array-end",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // The ::array-start signal is emitted each time the #JsonParser
   // starts parsing a #JsonArray
   extern (C) alias static void function (Parser* this_, void* user_data=null) nothrow signal_array_start;
   ulong signal_connect(string name:"array-start", CB/*:signal_array_start*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_array_start)||_ttmm!(CB, signal_array_start)()) {
      return signal_connect_data!()(&this, cast(char*)"array-start",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // The ::error signal is emitted each time a #JsonParser encounters
   // an error in a JSON stream.
   // <error>: a pointer to the #GError
   extern (C) alias static void function (Parser* this_, void* error, void* user_data=null) nothrow signal_error;
   ulong signal_connect(string name:"error", CB/*:signal_error*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_error)||_ttmm!(CB, signal_error)()) {
      return signal_connect_data!()(&this, cast(char*)"error",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // The ::object-end signal is emitted each time the #JsonParser
   // has successfully parsed an entire #JsonObject.
   // <object>: the parsed #JsonObject
   extern (C) alias static void function (Parser* this_, Object* object, void* user_data=null) nothrow signal_object_end;
   ulong signal_connect(string name:"object-end", CB/*:signal_object_end*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_object_end)||_ttmm!(CB, signal_object_end)()) {
      return signal_connect_data!()(&this, cast(char*)"object-end",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // The ::object-member signal is emitted each time the #JsonParser
   // has successfully parsed a single member of a #JsonObject. The
   // object and member are passed to the signal handlers.
   // <object>: a #JsonObject
   // <member_name>: the name of the newly parsed member
   extern (C) alias static void function (Parser* this_, Object* object, char* member_name, void* user_data=null) nothrow signal_object_member;
   ulong signal_connect(string name:"object-member", CB/*:signal_object_member*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_object_member)||_ttmm!(CB, signal_object_member)()) {
      return signal_connect_data!()(&this, cast(char*)"object-member",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // The ::object-start signal is emitted each time the #JsonParser
   // starts parsing a #JsonObject.
   extern (C) alias static void function (Parser* this_, void* user_data=null) nothrow signal_object_start;
   ulong signal_connect(string name:"object-start", CB/*:signal_object_start*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_object_start)||_ttmm!(CB, signal_object_start)()) {
      return signal_connect_data!()(&this, cast(char*)"object-start",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // The ::parse-end signal is emitted when the parser successfully
   // finished parsing a JSON data stream
   extern (C) alias static void function (Parser* this_, void* user_data=null) nothrow signal_parse_end;
   ulong signal_connect(string name:"parse-end", CB/*:signal_parse_end*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_parse_end)||_ttmm!(CB, signal_parse_end)()) {
      return signal_connect_data!()(&this, cast(char*)"parse-end",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // The ::parse-start signal is emitted when the parser began parsing
   // a JSON data stream.
   extern (C) alias static void function (Parser* this_, void* user_data=null) nothrow signal_parse_start;
   ulong signal_connect(string name:"parse-start", CB/*:signal_parse_start*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_parse_start)||_ttmm!(CB, signal_parse_start)()) {
      return signal_connect_data!()(&this, cast(char*)"parse-start",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// #JsonParser class.
struct ParserClass {
   private GObject2.ObjectClass parent_class;
   extern (C) void function (Parser* parser) nothrow parse_start;
   extern (C) void function (Parser* parser) nothrow object_start;
   extern (C) void function (Parser* parser, Object* object, char* member_name) nothrow object_member;
   extern (C) void function (Parser* parser, Object* object) nothrow object_end;
   extern (C) void function (Parser* parser) nothrow array_start;
   extern (C) void function (Parser* parser, Array* array, int index_) nothrow array_element;
   extern (C) void function (Parser* parser, Array* array) nothrow array_end;
   extern (C) void function (Parser* parser) nothrow parse_end;
   extern (C) void function (Parser* parser, GLib2.Error* error) nothrow error;
   extern (C) void function () nothrow _json_reserved1;
   extern (C) void function () nothrow _json_reserved2;
   extern (C) void function () nothrow _json_reserved3;
   extern (C) void function () nothrow _json_reserved4;
   extern (C) void function () nothrow _json_reserved5;
   extern (C) void function () nothrow _json_reserved6;
   extern (C) void function () nothrow _json_reserved7;
   extern (C) void function () nothrow _json_reserved8;
}


// Error enumeration for #JsonParser
// 
// This enumeration can be extended at later date
enum ParserError {
   PARSE = 0,
   TRAILING_COMMA = 1,
   MISSING_COMMA = 2,
   MISSING_COLON = 3,
   INVALID_BAREWORD = 4,
   UNKNOWN = 5
}
struct ParserPrivate {
}


// The <structname>JsonPath</structname> structure is an opaque object
// whose members cannot be directly accessed except through the provided
// API.
struct Path /* : GObject.Object */ /* Version 0.14 */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // VERSION: 0.14
   // Creates a new #JsonPath instance.
   // 
   // Once created, the #JsonPath object should be used with json_path_compile()
   // and json_path_match().
   // 
   // g_object_unref() to free the allocated resources when done
   // RETURNS: the newly created #JsonPath instance. Use
   static Path* /*new*/ new_()() nothrow {
      return json_path_new();
   }
   static auto opCall()() {
      return json_path_new();
   }
   static GLib2.Quark error_quark()() nothrow {
      return json_path_error_quark();
   }

   // VERSION: 0.14
   // Queries a JSON tree using a JSONPath expression.
   // 
   // This function is a simple wrapper around json_path_new(),
   // json_path_compile() and json_path_match(). It implicitly
   // creates a #JsonPath instance, compiles @expression and
   // matches it against the JSON tree pointed by @root.
   // 
   // %JSON_NODE_ARRAY containing an array of matching #JsonNode<!-- -->s.
   // Use json_node_free() when done
   // RETURNS: a newly-created #JsonNode of type
   // <expression>: a JSONPath expression
   // <root>: the root of a JSON tree
   static Node* /*new*/ query(AT0, AT1, AT2)(AT0 /*char*/ expression, AT1 /*Node*/ root, AT2 /*GLib2.Error**/ error=null) nothrow {
      return json_path_query(toCString!(char*)(expression), UpCast!(Node*)(root), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 0.14
   // Validates and decomposes @expression.
   // 
   // A JSONPath expression must be compiled before calling json_path_match().
   // 
   // the %JSON_PATH_ERROR domain and a code from the #JsonPathError
   // enumeration, and %FALSE will be returned
   // RETURNS: %TRUE on success; on error, @error will be set with
   // <expression>: a JSONPath expression
   int compile(AT0, AT1)(AT0 /*char*/ expression, AT1 /*GLib2.Error**/ error=null) nothrow {
      return json_path_compile(&this, toCString!(char*)(expression), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 0.14
   // Matches the JSON tree pointed by @root using the expression compiled
   // into the #JsonPath.
   // 
   // The matching #JsonNode<!-- -->s will be copied into a #JsonArray and
   // returned wrapped in a #JsonNode.
   // 
   // %JSON_NODE_ARRAY containing an array of matching #JsonNode<!-- -->s.
   // Use json_node_free() when done
   // RETURNS: a newly-created #JsonNode of type
   // <root>: a #JsonNode
   Node* /*new*/ match(AT0)(AT0 /*Node*/ root) nothrow {
      return json_path_match(&this, UpCast!(Node*)(root));
   }
}


// The <structname>JsonPathClass</structname> structure is an opaque
// object class whose members cannot be directly accessed.
struct PathClass /* Version 0.14 */ {
}

// Error code enumeration for the %JSON_PATH_ERROR domain.
enum PathError /* Version 0.14 */ {
   PATH_ERROR_INVALID_QUERY = 0
}

// The <structname>JsonReader</structname> structure contains only
// private data and should only be accessed using the provided API
struct Reader /* : GObject.Object */ /* Version 0.12 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private ReaderPrivate* priv;


   // VERSION: 0.12
   // Creates a new #JsonReader. You can use this object to read the contents of
   // the JSON tree starting from @node
   // 
   // release the allocated resources when done
   // RETURNS: the newly created #JsonReader. Use g_object_unref() to
   // <node>: a #JsonNode, or %NULL
   static Reader* /*new*/ new_(AT0)(AT0 /*Node*/ node=null) nothrow {
      return json_reader_new(UpCast!(Node*)(node));
   }
   static auto opCall(AT0)(AT0 /*Node*/ node=null) {
      return json_reader_new(UpCast!(Node*)(node));
   }
   static GLib2.Quark error_quark()() nothrow {
      return json_reader_error_quark();
   }

   // VERSION: 0.12
   // Counts the elements of the current position, if @reader is
   // positioned on an array
   // 
   // the #JsonReader is set in an error state
   // RETURNS: the number of elements, or -1. In case of failure
   int count_elements()() nothrow {
      return json_reader_count_elements(&this);
   }

   // VERSION: 0.12
   // Counts the members of the current position, if @reader is
   // positioned on an object
   // 
   // the #JsonReader is set in an error state
   // RETURNS: the number of members, or -1. In case of failure
   int count_members()() nothrow {
      return json_reader_count_members(&this);
   }

   // VERSION: 0.12
   // Moves the cursor back to the previous node after being positioned
   // inside an array
   // 
   // This function resets the error state of @reader, if any was set
   void end_element()() nothrow {
      json_reader_end_element(&this);
   }

   // VERSION: 0.12
   // Moves the cursor back to the previous node after being positioned
   // inside an object
   // 
   // This function resets the error state of @reader, if any was set
   void end_member()() nothrow {
      json_reader_end_member(&this);
   }

   // VERSION: 0.12
   // Retrieves the boolean value of the current position of @reader
   // RETURNS: the boolean value
   int get_boolean_value()() nothrow {
      return json_reader_get_boolean_value(&this);
   }

   // VERSION: 0.12
   // Retrieves the floating point value of the current position of @reader
   // RETURNS: the floating point value
   double get_double_value()() nothrow {
      return json_reader_get_double_value(&this);
   }

   // VERSION: 0.12
   // Retrieves the #GError currently set on @reader, if the #JsonReader
   // is in error state
   // RETURNS: the pointer to the error, or %NULL
   GLib2.Error* get_error()() nothrow {
      return json_reader_get_error(&this);
   }

   // VERSION: 0.12
   // Retrieves the integer value of the current position of @reader
   // RETURNS: the integer value
   long get_int_value()() nothrow {
      return json_reader_get_int_value(&this);
   }

   // VERSION: 0.14
   // Retrieves the name of the current member.
   // RETURNS: the name of the member, or %NULL
   char* get_member_name()() nothrow {
      return json_reader_get_member_name(&this);
   }

   // VERSION: 0.12
   // Checks whether the value of the current position of @reader is 'null'
   // RETURNS: %TRUE if 'null' is set, and %FALSE otherwise
   int get_null_value()() nothrow {
      return json_reader_get_null_value(&this);
   }

   // VERSION: 0.12
   // Retrieves the string value of the current position of @reader
   // RETURNS: the string value
   char* get_string_value()() nothrow {
      return json_reader_get_string_value(&this);
   }

   // VERSION: 0.12
   // Retrieves the #JsonNode of the current position of @reader
   // 
   // is owned by the #JsonReader and it should not be modified or freed
   // directly
   // RETURNS: a #JsonNode, or %NULL. The returned node
   Node* get_value()() nothrow {
      return json_reader_get_value(&this);
   }

   // VERSION: 0.12
   // Checks whether the @reader is currently on an array
   // 
   // otherwise
   // RETURNS: %TRUE if the #JsonReader is on an array, and %FALSE
   int is_array()() nothrow {
      return json_reader_is_array(&this);
   }

   // VERSION: 0.12
   // Checks whether the @reader is currently on an object
   // 
   // otherwise
   // RETURNS: %TRUE if the #JsonReader is on an object, and %FALSE
   int is_object()() nothrow {
      return json_reader_is_object(&this);
   }

   // VERSION: 0.12
   // Checks whether the @reader is currently on a value
   // 
   // otherwise
   // RETURNS: %TRUE if the #JsonReader is on a value, and %FALSE
   int is_value()() nothrow {
      return json_reader_is_value(&this);
   }

   // VERSION: 0.14
   // Retrieves a list of member names from the current position, if @reader
   // is positioned on an object.
   // 
   // array of strings holding the members name. Use g_strfreev() when
   // done.
   // RETURNS: a newly allocated, %NULL-terminated
   char** /*new*/ list_members()() nothrow {
      return json_reader_list_members(&this);
   }

   // VERSION: 0.12
   // Advances the cursor of @reader to the element @index_ of the array
   // or the object at the current position.
   // 
   // You can use the json_reader_get_value* family of functions to retrieve
   // the value of the element; for instance:
   // 
   // |[
   // json_reader_read_element (reader, 0);
   // int_value = json_reader_get_int_value (reader);
   // ]|
   // 
   // After reading the value, json_reader_end_element() should be called to
   // reposition the cursor inside the #JsonReader, e.g.:
   // 
   // |[
   // json_reader_read_element (reader, 1);
   // str_value = json_reader_get_string_value (reader);
   // json_reader_end_element (reader);
   // 
   // json_reader_read_element (reader, 2);
   // str_value = json_reader_get_string_value (reader);
   // json_reader_end_element (reader);
   // ]|
   // 
   // If @reader is not currently on an array or an object, or if the @index_ is
   // bigger than the size of the array or the object, the #JsonReader will be
   // put in an error state until json_reader_end_element() is called.
   // RETURNS: %TRUE on success, and %FALSE otherwise
   // <index_>: the index of the element
   int read_element()(uint index_) nothrow {
      return json_reader_read_element(&this, index_);
   }

   // VERSION: 0.12
   // Advances the cursor of @reader to the @member_name of the object at the
   // current position.
   // 
   // You can use the json_reader_get_value* family of functions to retrieve
   // the value of the member; for instance:
   // 
   // |[
   // json_reader_read_member (reader, "width");
   // width = json_reader_get_int_value (reader);
   // ]|
   // 
   // After reading the value, json_reader_end_member() should be called to
   // reposition the cursor inside the #JsonReader, e.g.:
   // 
   // |[
   // json_reader_read_member (reader, "author");
   // author = json_reader_get_string_value (reader);
   // json_reader_end_element (reader);
   // 
   // json_reader_read_element (reader, "title");
   // title = json_reader_get_string_value (reader);
   // json_reader_end_element (reader);
   // ]|
   // 
   // If @reader is not currently on an object, or if the @member_name is not
   // defined in the object, the #JsonReader will be put in an error state until
   // json_reader_end_member() is called.
   // RETURNS: %TRUE on success, and %FALSE otherwise
   // <member_name>: the name of the member to read
   int read_member(AT0)(AT0 /*char*/ member_name) nothrow {
      return json_reader_read_member(&this, toCString!(char*)(member_name));
   }

   // VERSION: 0.12
   // Sets the root #JsonNode to be read by @reader. The @reader will take
   // a copy of @root
   // 
   // If another #JsonNode is currently set as root, it will be replaced.
   // <root>: a #JsonNode
   void set_root(AT0)(AT0 /*Node*/ root=null) nothrow {
      json_reader_set_root(&this, UpCast!(Node*)(root));
   }
}


// The <structname>JsonReaderClass</structname> structure contains only
// private data
struct ReaderClass /* Version 0.12 */ {
   private GObject2.ObjectClass parent_class;
   extern (C) void function () nothrow _json_padding0;
   extern (C) void function () nothrow _json_padding1;
   extern (C) void function () nothrow _json_padding2;
   extern (C) void function () nothrow _json_padding3;
   extern (C) void function () nothrow _json_padding4;
}

// Error codes enumeration for #JsonReader errors
enum ReaderError /* Version 0.12 */ {
   NO_ARRAY = 0,
   INVALID_INDEX = 1,
   NO_OBJECT = 2,
   INVALID_MEMBER = 3,
   INVALID_NODE = 4,
   NO_VALUE = 5,
   INVALID_TYPE = 6
}
struct ReaderPrivate {
}

struct Serializable /* Interface */ {
   mixin template __interface__() {
      // VERSION: 0.10
      // Calls the default implementation of the #JsonSerializable
      // deserialize_property() virtual function
      // 
      // This function can be used inside a custom implementation
      // of the deserialize_property() virtual function in lieu of:
      // 
      // |[
      // JsonSerializable *iface;
      // gboolean res;
      // 
      // iface = g_type_default_interface_peek (JSON_TYPE_SERIALIZABLE);
      // res = iface->deserialize_property (serializable, property_name,
      // value,
      // pspec,
      // property_node);
      // ]|
      // RETURNS: %TRUE if the property was successfully deserialized.
      // <property_name>: the name of the property
      // <value>: a pointer to an uninitialized #GValue
      // <pspec>: a #GParamSpec
      // <property_node>: a #JsonNode containing the serialized property
      int default_deserialize_property(AT0, AT1, AT2, AT3)(AT0 /*char*/ property_name, AT1 /*GObject2.Value*/ value, AT2 /*GObject2.ParamSpec*/ pspec, AT3 /*Node*/ property_node) nothrow {
         return json_serializable_default_deserialize_property(cast(Serializable*)&this, toCString!(char*)(property_name), UpCast!(GObject2.Value*)(value), UpCast!(GObject2.ParamSpec*)(pspec), UpCast!(Node*)(property_node));
      }

      // VERSION: 0.10
      // Calls the default implementation of the #JsonSerializable
      // serialize_property() virtual function
      // 
      // This function can be used inside a custom implementation
      // of the serialize_property() virtual function in lieu of:
      // 
      // |[
      // JsonSerializable *iface;
      // JsonNode *node;
      // 
      // iface = g_type_default_interface_peek (JSON_TYPE_SERIALIZABLE);
      // node = iface->serialize_property (serializable, property_name,
      // value,
      // pspec);
      // ]|
      // 
      // property
      // RETURNS: a #JsonNode containing the serialized
      // <property_name>: the name of the property
      // <value>: the value of the property
      // <pspec>: a #GParamSpec
      Node* /*new*/ default_serialize_property(AT0, AT1, AT2)(AT0 /*char*/ property_name, AT1 /*GObject2.Value*/ value, AT2 /*GObject2.ParamSpec*/ pspec) nothrow {
         return json_serializable_default_serialize_property(cast(Serializable*)&this, toCString!(char*)(property_name), UpCast!(GObject2.Value*)(value), UpCast!(GObject2.ParamSpec*)(pspec));
      }

      // Asks a #JsonSerializable implementation to deserialize the
      // property contained inside @property_node into @value.
      // RETURNS: %TRUE if the property was successfully deserialized.
      // <property_name>: the name of the property
      // <value>: a pointer to an uninitialized #GValue
      // <pspec>: a #GParamSpec
      // <property_node>: a #JsonNode containing the serialized property
      int deserialize_property(AT0, AT1, AT2, AT3)(AT0 /*char*/ property_name, /*out*/ AT1 /*GObject2.Value*/ value, AT2 /*GObject2.ParamSpec*/ pspec, AT3 /*Node*/ property_node) nothrow {
         return json_serializable_deserialize_property(cast(Serializable*)&this, toCString!(char*)(property_name), UpCast!(GObject2.Value*)(value), UpCast!(GObject2.ParamSpec*)(pspec), UpCast!(Node*)(property_node));
      }

      // VERSION: 0.14
      // FIXME
      // 
      // or %NULL if no property was found
      // RETURNS: the #GParamSpec for the property
      // <name>: the name of the property
      GObject2.ParamSpec* find_property(AT0)(AT0 /*char*/ name) nothrow {
         return json_serializable_find_property(cast(Serializable*)&this, toCString!(char*)(name));
      }
      void get_property(AT0, AT1)(AT0 /*GObject2.ParamSpec*/ pspec, AT1 /*GObject2.Value*/ value) nothrow {
         json_serializable_get_property(cast(Serializable*)&this, UpCast!(GObject2.ParamSpec*)(pspec), UpCast!(GObject2.Value*)(value));
      }

      // VERSION: 0.14
      // FIXME
      // 
      // of #GParamSpec. Use g_free() to free the array when done.
      // RETURNS: an array
      // <n_pspecs>: return location for the length of the array of #GParamSpec returned by the function
      GObject2.ParamSpec** /*new container*/ list_properties(AT0)(/*out*/ AT0 /*uint*/ n_pspecs) nothrow {
         return json_serializable_list_properties(cast(Serializable*)&this, UpCast!(uint*)(n_pspecs));
      }

      // Asks a #JsonSerializable implementation to serialize a #GObject
      // property into a #JsonNode object.
      // RETURNS: a #JsonNode containing the serialized property
      // <property_name>: the name of the property
      // <value>: the value of the property
      // <pspec>: a #GParamSpec
      Node* /*new*/ serialize_property(AT0, AT1, AT2)(AT0 /*char*/ property_name, AT1 /*GObject2.Value*/ value, AT2 /*GObject2.ParamSpec*/ pspec) nothrow {
         return json_serializable_serialize_property(cast(Serializable*)&this, toCString!(char*)(property_name), UpCast!(GObject2.Value*)(value), UpCast!(GObject2.ParamSpec*)(pspec));
      }
      void set_property(AT0, AT1)(AT0 /*GObject2.ParamSpec*/ pspec, AT1 /*GObject2.Value*/ value) nothrow {
         json_serializable_set_property(cast(Serializable*)&this, UpCast!(GObject2.ParamSpec*)(pspec), UpCast!(GObject2.Value*)(value));
      }
   }
   mixin __interface__;
}


// Interface that allows serializing and deserializing #GObject<!-- -->s
// with properties storing complex data types. The json_serialize_gobject()
// function will check if the passed #GObject implements this interface,
// so it can also be used to override the default property serialization
// sequence.
struct SerializableIface {
   private GObject2.TypeInterface g_iface;

   // RETURNS: a #JsonNode containing the serialized property
   // <property_name>: the name of the property
   // <value>: the value of the property
   // <pspec>: a #GParamSpec
   extern (C) Node* /*new*/ function (Serializable* serializable, char* property_name, GObject2.Value* value, GObject2.ParamSpec* pspec) nothrow serialize_property;

   // RETURNS: %TRUE if the property was successfully deserialized.
   // <property_name>: the name of the property
   // <value>: a pointer to an uninitialized #GValue
   // <pspec>: a #GParamSpec
   // <property_node>: a #JsonNode containing the serialized property
   extern (C) int function (Serializable* serializable, char* property_name, /*out*/ GObject2.Value* value, GObject2.ParamSpec* pspec, Node* property_node) nothrow deserialize_property;

   // RETURNS: the #GParamSpec for the property
   // <name>: the name of the property
   extern (C) GObject2.ParamSpec* function (Serializable* serializable, char* name) nothrow find_property;
   // Unintrospectable functionp: list_properties() / ()
   extern (C) GObject2.ParamSpec** function (Serializable* serializable, uint* n_pspecs) nothrow list_properties;
   extern (C) void function (Serializable* serializable, GObject2.ParamSpec* pspec, GObject2.Value* value) nothrow set_property;
   extern (C) void function (Serializable* serializable, GObject2.ParamSpec* pspec, GObject2.Value* value) nothrow get_property;
}


// VERSION: 0.10
// Checks whether it is possible to deserialize a #GBoxed of
// type @gboxed_type from a #JsonNode of type @node_type
// RETURNS: %TRUE if the type can be deserialized, %FALSE otherwise
// <gboxed_type>: a boxed type
// <node_type>: a #JsonNode type
static int boxed_can_deserialize()(Type gboxed_type, NodeType node_type) nothrow {
   return json_boxed_can_deserialize(gboxed_type, node_type);
}


// VERSION: 0.10
// Checks whether it is possible to serialize a #GBoxed of
// type @gboxed_type into a #JsonNode. The type of the
// #JsonNode is placed inside @node_type if the function
// returns %TRUE and it's undefined otherwise.
// 
// and %FALSE otherwise.
// RETURNS: %TRUE if the type can be serialized,
// <gboxed_type>: a boxed type
// <node_type>: the #JsonNode type to which the boxed type can be serialized into
static int boxed_can_serialize(AT0)(Type gboxed_type, /*out*/ AT0 /*NodeType*/ node_type) nothrow {
   return json_boxed_can_serialize(gboxed_type, UpCast!(NodeType*)(node_type));
}


// VERSION: 0.10
// Deserializes @node into a #GBoxed of @gboxed_type
// 
// g_boxed_free() to release the resources allocated by this
// function
// RETURNS: the newly allocated #GBoxed. Use
// <gboxed_type>: a boxed type
// <node>: a #JsonNode
static void* /*new*/ boxed_deserialize(AT0)(Type gboxed_type, AT0 /*Node*/ node) nothrow {
   return json_boxed_deserialize(gboxed_type, UpCast!(Node*)(node));
}


// Unintrospectable function: boxed_register_deserialize_func() / json_boxed_register_deserialize_func()
// VERSION: 0.10
// Registers a deserialization function for a #GBoxed of type @gboxed_type
// from a #JsonNode of type @node_type
// <gboxed_type>: a boxed type
// <node_type>: a node type
// <deserialize_func>: deserialization function for @boxed_type from a #JsonNode of type @node_type
static void boxed_register_deserialize_func()(Type gboxed_type, NodeType node_type, BoxedDeserializeFunc deserialize_func) nothrow {
   json_boxed_register_deserialize_func(gboxed_type, node_type, deserialize_func);
}


// Unintrospectable function: boxed_register_serialize_func() / json_boxed_register_serialize_func()
// VERSION: 0.10
// Registers a serialization function for a #GBoxed of type @gboxed_type
// to a #JsonNode of type @node_type
// <gboxed_type>: a boxed type
// <node_type>: a node type
// <serialize_func>: serialization function for @boxed_type into a #JsonNode of type @node_type
static void boxed_register_serialize_func()(Type gboxed_type, NodeType node_type, BoxedSerializeFunc serialize_func) nothrow {
   json_boxed_register_serialize_func(gboxed_type, node_type, serialize_func);
}


// VERSION: 0.10
// Serializes @boxed, a pointer to a #GBoxed of type @gboxed_type,
// into a #JsonNode
// 
// boxed type, or %NULL if serialization either failed or was not possible
// RETURNS: a #JsonNode with the serialization of the
// <gboxed_type>: a boxed type
// <boxed>: a pointer to a #GBoxed of type @gboxed_type
static Node* /*new*/ boxed_serialize(AT0)(Type gboxed_type, AT0 /*const(void)*/ boxed) nothrow {
   return json_boxed_serialize(gboxed_type, UpCast!(const(void)*)(boxed));
}


// VERSION: 0.4
// DEPRECATED (v0.10) function: construct_gobject - Use json_gobject_from_data() instead
// Deserializes a JSON data stream and creates the corresponding
// #GObject class. If @gtype implements the #JsonSerializableIface
// interface, it will be asked to deserialize all the JSON members
// into the respective properties; otherwise, the default implementation
// will be used to translate the compatible JSON native types.
// 
// Note: the JSON data stream must be an object declaration.
// RETURNS: a #GObject or %NULL
// <gtype>: the #GType of object to construct
// <data>: a JSON data stream
// <length>: length of the data stream
static GObject2.Object* /*new*/ construct_gobject(AT0, AT1)(Type gtype, AT0 /*char*/ data, size_t length, AT1 /*GLib2.Error**/ error=null) nothrow {
   return json_construct_gobject(gtype, toCString!(char*)(data), length, UpCast!(GLib2.Error**)(error));
}


// VERSION: 0.10
// Creates a new #GObject of type @gtype, and constructs it
// using the members of the passed #JsonObject
// 
// instance. Use g_object_unref() to free the resources
// allocated by this function
// RETURNS: The newly created #GObject
// <gtype>: the type of the #GObject to create
// <node>: a #JsonNode of type %JSON_NODE_OBJECT describing the instance of type @gtype
static GObject2.Object* /*new*/ gobject_deserialize(AT0)(Type gtype, AT0 /*Node*/ node) nothrow {
   return json_gobject_deserialize(gtype, UpCast!(Node*)(node));
}


// VERSION: 0.10
// Deserializes a JSON data stream and creates the corresponding
// #GObject class. If @gtype implements the #JsonSerializableIface
// interface, it will be asked to deserialize all the JSON members
// into the respective properties; otherwise, the default implementation
// will be used to translate the compatible JSON native types.
// 
// Note: the JSON data stream must be an object declaration.
// RETURNS: a #GObject or %NULL
// <gtype>: the #GType of object to construct
// <data>: a JSON data stream
// <length>: length of the data stream, or -1 if it is NUL-terminated
static GObject2.Object* /*new*/ gobject_from_data(AT0, AT1)(Type gtype, AT0 /*char*/ data, ssize_t length, AT1 /*GLib2.Error**/ error=null) nothrow {
   return json_gobject_from_data(gtype, toCString!(char*)(data), length, UpCast!(GLib2.Error**)(error));
}


// VERSION: 0.10
// Creates a #JsonNode representing the passed #GObject
// instance. Each member of the returned JSON object will
// map to a property of the #GObject
// 
// of type %JSON_NODE_OBJECT. Use json_node_free() to free
// the resources allocated by this function
// RETURNS: the newly created #JsonNode
// <gobject>: a #GObject
static Node* /*new*/ gobject_serialize(AT0)(AT0 /*GObject2.Object*/ gobject) nothrow {
   return json_gobject_serialize(UpCast!(GObject2.Object*)(gobject));
}


// VERSION: 0.10
// Serializes a #GObject into a JSON data stream, iterating recursively
// over each property.
// 
// If @gobject implements the #JsonSerializableIface interface, it will
// be asked to serialize all its properties; otherwise, the default
// implementation will be use to translate the compatible types into
// JSON native types.
// RETURNS: a JSON data stream representing the passed #GObject
// <gobject>: a #GObject
// <length>: return value for the length of the buffer, or %NULL
static char* /*new*/ gobject_to_data(AT0, AT1)(AT0 /*GObject2.Object*/ gobject, /*out*/ AT1 /*size_t*/ length) nothrow {
   return json_gobject_to_data(UpCast!(GObject2.Object*)(gobject), UpCast!(size_t*)(length));
}


// VERSION: 0.14
// Converts a JSON data structure to a GVariant value using @signature to
// resolve ambiguous data types. If no error occurs, the resulting #GVariant
// is guaranteed to conform to @signature.
// 
// If @signature is not %NULL but does not represent a valid GVariant type
// string, %NULL is returned and error is set to %G_IO_ERROR_INVALID_ARGUMENT.
// If a @signature is provided but the JSON structure cannot be mapped to it,
// %NULL is returned and error is set to %G_IO_ERROR_INVALID_DATA.
// If @signature is %NULL, the conversion is done based strictly on the types
// in the JSON nodes.
// 
// @signature, or %NULL on error
// RETURNS: A newly created #GVariant compliant with
// <json_node>: A #JsonNode to convert
// <signature>: A valid #GVariant type string, or %NULL
static GLib2.Variant* /*new*/ gvariant_deserialize(AT0, AT1, AT2)(AT0 /*Node*/ json_node, AT1 /*char*/ signature, AT2 /*GLib2.Error**/ error=null) nothrow {
   return json_gvariant_deserialize(UpCast!(Node*)(json_node), toCString!(char*)(signature), UpCast!(GLib2.Error**)(error));
}


// VERSION: 0.14
// Converts a JSON string to a #GVariant value. This method works exactly
// like json_gvariant_deserialize(), but takes a JSON encoded string instead.
// The string is first converted to a #JsonNode using #JsonParser, and then
// json_gvariant_deserialize() is called.
// 
// @signature, or %NULL on error
// RETURNS: A newly created #GVariant compliant with
// <json>: A JSON data string
// <length>: The length of @json, or -1 if %NULL-terminated
// <signature>: A valid #GVariant type string, or %NULL
static GLib2.Variant* /*new*/ gvariant_deserialize_data(AT0, AT1, AT2)(AT0 /*char*/ json, ssize_t length, AT1 /*char*/ signature, AT2 /*GLib2.Error**/ error=null) nothrow {
   return json_gvariant_deserialize_data(toCString!(char*)(json), length, toCString!(char*)(signature), UpCast!(GLib2.Error**)(error));
}


// VERSION: 0.14
// Converts @variant to a JSON tree.
// 
// JSON data structure obtained from @variant
// RETURNS: A #JsonNode representing the root of the
// <variant>: A #GVariant to convert
static Node* /*new*/ gvariant_serialize(AT0)(AT0 /*GLib2.Variant*/ variant) nothrow {
   return json_gvariant_serialize(UpCast!(GLib2.Variant*)(variant));
}


// VERSION: 0.14
// Converts @variant to its JSON encoded string representation. This method
// is actually a helper function. It uses json_gvariant_serialize() to obtain the
// JSON tree, and then #JsonGenerator to stringify it.
// 
// @variant
// RETURNS: The JSON encoded string corresponding to
// <variant>: A #GVariant to convert
// <length>: Return location for the length of the returned string, or %NULL
static char* /*new*/ gvariant_serialize_data(AT0, AT1)(AT0 /*GLib2.Variant*/ variant, /*out*/ AT1 /*size_t*/ length=null) nothrow {
   return json_gvariant_serialize_data(UpCast!(GLib2.Variant*)(variant), UpCast!(size_t*)(length));
}


// DEPRECATED (v0.10) function: serialize_gobject - Use json_gobject_to_data() instead
// Serializes a #GObject into a JSON data stream. If @gobject implements
// the #JsonSerializableIface interface, it will be asked to serizalize all
// its properties; otherwise, the default implementation will be use to
// translate the compatible types into JSON native types.
// RETURNS: a JSON data stream representing the passed #GObject
// <gobject>: a #GObject
// <length>: return value for the length of the buffer, or %NULL
static char* /*new*/ serialize_gobject(AT0, AT1)(AT0 /*GObject2.Object*/ gobject, /*out*/ AT1 /*size_t*/ length) nothrow {
   return json_serialize_gobject(UpCast!(GObject2.Object*)(gobject), UpCast!(size_t*)(length));
}


// C prototypes:

extern (C) {
Array* /*new*/ json_array_new() nothrow;
Array* /*new*/ json_array_sized_new(uint n_elements) nothrow;
void json_array_add_array_element(Array* this_, Array* value) nothrow;
void json_array_add_boolean_element(Array* this_, int value) nothrow;
void json_array_add_double_element(Array* this_, double value) nothrow;
void json_array_add_element(Array* this_, Node* node) nothrow;
void json_array_add_int_element(Array* this_, long value) nothrow;
void json_array_add_null_element(Array* this_) nothrow;
void json_array_add_object_element(Array* this_, Object* value) nothrow;
void json_array_add_string_element(Array* this_, char* value) nothrow;
Node* /*new*/ json_array_dup_element(Array* this_, uint index_) nothrow;
void json_array_foreach_element(Array* this_, ArrayForeach func, void* data) nothrow;
Array* json_array_get_array_element(Array* this_, uint index_) nothrow;
int json_array_get_boolean_element(Array* this_, uint index_) nothrow;
double json_array_get_double_element(Array* this_, uint index_) nothrow;
Node* json_array_get_element(Array* this_, uint index_) nothrow;
GLib2.List* /*new container*/ json_array_get_elements(Array* this_) nothrow;
long json_array_get_int_element(Array* this_, uint index_) nothrow;
uint json_array_get_length(Array* this_) nothrow;
int json_array_get_null_element(Array* this_, uint index_) nothrow;
Object* json_array_get_object_element(Array* this_, uint index_) nothrow;
char* json_array_get_string_element(Array* this_, uint index_) nothrow;
Array* /*new*/ json_array_ref(Array* this_) nothrow;
void json_array_remove_element(Array* this_, uint index_) nothrow;
void json_array_unref(Array* this_) nothrow;
Builder* /*new*/ json_builder_new() nothrow;
Builder* json_builder_add_boolean_value(Builder* this_, int value) nothrow;
Builder* json_builder_add_double_value(Builder* this_, double value) nothrow;
Builder* json_builder_add_int_value(Builder* this_, long value) nothrow;
Builder* json_builder_add_null_value(Builder* this_) nothrow;
Builder* json_builder_add_string_value(Builder* this_, char* value) nothrow;
Builder* json_builder_add_value(Builder* this_, Node* node) nothrow;
Builder* json_builder_begin_array(Builder* this_) nothrow;
Builder* json_builder_begin_object(Builder* this_) nothrow;
Builder* json_builder_end_array(Builder* this_) nothrow;
Builder* json_builder_end_object(Builder* this_) nothrow;
Node* /*new*/ json_builder_get_root(Builder* this_) nothrow;
void json_builder_reset(Builder* this_) nothrow;
Builder* json_builder_set_member_name(Builder* this_, char* member_name) nothrow;
Generator* /*new*/ json_generator_new() nothrow;
uint json_generator_get_indent(Generator* this_) nothrow;
dchar json_generator_get_indent_char(Generator* this_) nothrow;
int json_generator_get_pretty(Generator* this_) nothrow;
Node* json_generator_get_root(Generator* this_) nothrow;
void json_generator_set_indent(Generator* this_, uint indent_level) nothrow;
void json_generator_set_indent_char(Generator* this_, dchar indent_char) nothrow;
void json_generator_set_pretty(Generator* this_, int is_pretty) nothrow;
void json_generator_set_root(Generator* this_, Node* node) nothrow;
char* /*new*/ json_generator_to_data(Generator* this_, /*out*/ size_t* length) nothrow;
int json_generator_to_file(Generator* this_, char* filename, GLib2.Error** error) nothrow;
int json_generator_to_stream(Generator* this_, Gio2.OutputStream* stream, Gio2.Cancellable* cancellable, GLib2.Error** error) nothrow;
Node* /*new*/ json_node_new(NodeType type) nothrow;
Node* /*new*/ json_node_copy(Node* this_) nothrow;
Array* /*new*/ json_node_dup_array(Node* this_) nothrow;
Object* /*new*/ json_node_dup_object(Node* this_) nothrow;
char* /*new*/ json_node_dup_string(Node* this_) nothrow;
void json_node_free(Node* this_) nothrow;
Array* json_node_get_array(Node* this_) nothrow;
int json_node_get_boolean(Node* this_) nothrow;
double json_node_get_double(Node* this_) nothrow;
long json_node_get_int(Node* this_) nothrow;
NodeType json_node_get_node_type(Node* this_) nothrow;
Object* json_node_get_object(Node* this_) nothrow;
Node* json_node_get_parent(Node* this_) nothrow;
char* json_node_get_string(Node* this_) nothrow;
void json_node_get_value(Node* this_, /*out*/ GObject2.Value* value) nothrow;
Type json_node_get_value_type(Node* this_) nothrow;
int json_node_is_null(Node* this_) nothrow;
void json_node_set_array(Node* this_, Array* array) nothrow;
void json_node_set_boolean(Node* this_, int value) nothrow;
void json_node_set_double(Node* this_, double value) nothrow;
void json_node_set_int(Node* this_, long value) nothrow;
void json_node_set_object(Node* this_, Object* object) nothrow;
void json_node_set_parent(Node* this_, Node* parent) nothrow;
void json_node_set_string(Node* this_, char* value) nothrow;
void json_node_set_value(Node* this_, GObject2.Value* value) nothrow;
void json_node_take_array(Node* this_, Array* array) nothrow;
void json_node_take_object(Node* this_, Object* object) nothrow;
char* json_node_type_name(Node* this_) nothrow;
Object* /*new*/ json_object_new() nothrow;
void json_object_add_member(Object* this_, char* member_name, Node* node) nothrow;
Node* /*new*/ json_object_dup_member(Object* this_, char* member_name) nothrow;
void json_object_foreach_member(Object* this_, ObjectForeach func, void* data) nothrow;
Array* json_object_get_array_member(Object* this_, char* member_name) nothrow;
int json_object_get_boolean_member(Object* this_, char* member_name) nothrow;
double json_object_get_double_member(Object* this_, char* member_name) nothrow;
long json_object_get_int_member(Object* this_, char* member_name) nothrow;
Node* json_object_get_member(Object* this_, char* member_name) nothrow;
GLib2.List* /*new container*/ json_object_get_members(Object* this_) nothrow;
int json_object_get_null_member(Object* this_, char* member_name) nothrow;
Object* json_object_get_object_member(Object* this_, char* member_name) nothrow;
uint json_object_get_size(Object* this_) nothrow;
char* json_object_get_string_member(Object* this_, char* member_name) nothrow;
GLib2.List* /*new container*/ json_object_get_values(Object* this_) nothrow;
int json_object_has_member(Object* this_, char* member_name) nothrow;
Object* /*new*/ json_object_ref(Object* this_) nothrow;
void json_object_remove_member(Object* this_, char* member_name) nothrow;
void json_object_set_array_member(Object* this_, char* member_name, Array* value) nothrow;
void json_object_set_boolean_member(Object* this_, char* member_name, int value) nothrow;
void json_object_set_double_member(Object* this_, char* member_name, double value) nothrow;
void json_object_set_int_member(Object* this_, char* member_name, long value) nothrow;
void json_object_set_member(Object* this_, char* member_name, Node* node) nothrow;
void json_object_set_null_member(Object* this_, char* member_name) nothrow;
void json_object_set_object_member(Object* this_, char* member_name, Object* value) nothrow;
void json_object_set_string_member(Object* this_, char* member_name, char* value) nothrow;
void json_object_unref(Object* this_) nothrow;
Parser* /*new*/ json_parser_new() nothrow;
GLib2.Quark json_parser_error_quark() nothrow;
uint json_parser_get_current_line(Parser* this_) nothrow;
uint json_parser_get_current_pos(Parser* this_) nothrow;
Node* json_parser_get_root(Parser* this_) nothrow;
int json_parser_has_assignment(Parser* this_, /*out*/ char** variable_name=null) nothrow;
int json_parser_load_from_data(Parser* this_, char* data, ssize_t length, GLib2.Error** error) nothrow;
int json_parser_load_from_file(Parser* this_, char* filename, GLib2.Error** error) nothrow;
int json_parser_load_from_stream(Parser* this_, Gio2.InputStream* stream, Gio2.Cancellable* cancellable, GLib2.Error** error) nothrow;
void json_parser_load_from_stream_async(Parser* this_, Gio2.InputStream* stream, Gio2.Cancellable* cancellable, Gio2.AsyncReadyCallback callback, void* user_data) nothrow;
int json_parser_load_from_stream_finish(Parser* this_, Gio2.AsyncResult* result, GLib2.Error** error) nothrow;
Path* /*new*/ json_path_new() nothrow;
GLib2.Quark json_path_error_quark() nothrow;
Node* /*new*/ json_path_query(char* expression, Node* root, GLib2.Error** error) nothrow;
int json_path_compile(Path* this_, char* expression, GLib2.Error** error) nothrow;
Node* /*new*/ json_path_match(Path* this_, Node* root) nothrow;
Reader* /*new*/ json_reader_new(Node* node=null) nothrow;
GLib2.Quark json_reader_error_quark() nothrow;
int json_reader_count_elements(Reader* this_) nothrow;
int json_reader_count_members(Reader* this_) nothrow;
void json_reader_end_element(Reader* this_) nothrow;
void json_reader_end_member(Reader* this_) nothrow;
int json_reader_get_boolean_value(Reader* this_) nothrow;
double json_reader_get_double_value(Reader* this_) nothrow;
GLib2.Error* json_reader_get_error(Reader* this_) nothrow;
long json_reader_get_int_value(Reader* this_) nothrow;
char* json_reader_get_member_name(Reader* this_) nothrow;
int json_reader_get_null_value(Reader* this_) nothrow;
char* json_reader_get_string_value(Reader* this_) nothrow;
Node* json_reader_get_value(Reader* this_) nothrow;
int json_reader_is_array(Reader* this_) nothrow;
int json_reader_is_object(Reader* this_) nothrow;
int json_reader_is_value(Reader* this_) nothrow;
char** /*new*/ json_reader_list_members(Reader* this_) nothrow;
int json_reader_read_element(Reader* this_, uint index_) nothrow;
int json_reader_read_member(Reader* this_, char* member_name) nothrow;
void json_reader_set_root(Reader* this_, Node* root=null) nothrow;
int json_serializable_default_deserialize_property(Serializable* this_, char* property_name, GObject2.Value* value, GObject2.ParamSpec* pspec, Node* property_node) nothrow;
Node* /*new*/ json_serializable_default_serialize_property(Serializable* this_, char* property_name, GObject2.Value* value, GObject2.ParamSpec* pspec) nothrow;
int json_serializable_deserialize_property(Serializable* this_, char* property_name, /*out*/ GObject2.Value* value, GObject2.ParamSpec* pspec, Node* property_node) nothrow;
GObject2.ParamSpec* json_serializable_find_property(Serializable* this_, char* name) nothrow;
void json_serializable_get_property(Serializable* this_, GObject2.ParamSpec* pspec, GObject2.Value* value) nothrow;
GObject2.ParamSpec** /*new container*/ json_serializable_list_properties(Serializable* this_, /*out*/ uint* n_pspecs) nothrow;
Node* /*new*/ json_serializable_serialize_property(Serializable* this_, char* property_name, GObject2.Value* value, GObject2.ParamSpec* pspec) nothrow;
void json_serializable_set_property(Serializable* this_, GObject2.ParamSpec* pspec, GObject2.Value* value) nothrow;
int json_boxed_can_deserialize(Type gboxed_type, NodeType node_type) nothrow;
int json_boxed_can_serialize(Type gboxed_type, /*out*/ NodeType* node_type) nothrow;
void* /*new*/ json_boxed_deserialize(Type gboxed_type, Node* node) nothrow;
void json_boxed_register_deserialize_func(Type gboxed_type, NodeType node_type, BoxedDeserializeFunc deserialize_func) nothrow;
void json_boxed_register_serialize_func(Type gboxed_type, NodeType node_type, BoxedSerializeFunc serialize_func) nothrow;
Node* /*new*/ json_boxed_serialize(Type gboxed_type, const(void)* boxed) nothrow;
GObject2.Object* /*new*/ json_construct_gobject(Type gtype, char* data, size_t length, GLib2.Error** error) nothrow;
GObject2.Object* /*new*/ json_gobject_deserialize(Type gtype, Node* node) nothrow;
GObject2.Object* /*new*/ json_gobject_from_data(Type gtype, char* data, ssize_t length, GLib2.Error** error) nothrow;
Node* /*new*/ json_gobject_serialize(GObject2.Object* gobject) nothrow;
char* /*new*/ json_gobject_to_data(GObject2.Object* gobject, /*out*/ size_t* length) nothrow;
GLib2.Variant* /*new*/ json_gvariant_deserialize(Node* json_node, char* signature, GLib2.Error** error) nothrow;
GLib2.Variant* /*new*/ json_gvariant_deserialize_data(char* json, ssize_t length, char* signature, GLib2.Error** error) nothrow;
Node* /*new*/ json_gvariant_serialize(GLib2.Variant* variant) nothrow;
char* /*new*/ json_gvariant_serialize_data(GLib2.Variant* variant, /*out*/ size_t* length=null) nothrow;
char* /*new*/ json_serialize_gobject(GObject2.Object* gobject, /*out*/ size_t* length) nothrow;
}

// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/Mx-2.0.gir"

module Mx2;
public import gtk2.atk;
alias gtk2.atk Atk;
public import gtk2.clutter;
alias gtk2.clutter Clutter;
public import gtk2.cogl;
alias gtk2.cogl Cogl;
public import gtk2.coglpango;
alias gtk2.coglpango CoglPango;
public import gtk2.gl;
alias gtk2.gl GL;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;
public import gtk2.gio2;
alias gtk2.gio2 Gio2;
public import gtk2.json;
alias gtk2.json Json;
public import gtk2.pango;
alias gtk2.pango Pango;
public import gtk2.pangocairo;
alias gtk2.pangocairo PangoCairo;
public import gtk2.pangoft2;
alias gtk2.pangoft2 PangoFT2;
public import gtk2.cairo;
alias gtk2.cairo cairo;
public import gtk2.fontconfig2;
alias gtk2.fontconfig2 fontconfig2;
public import gtk2.freetype2;
alias gtk2.freetype2 freetype2;

// package: "mx-2.0";
// C header: "mx/mx.h";

// c:symbol-prefixes: ["mx"]
// c:identifier-prefixes: ["Mx"]

// module Mx2;

//  --- mixin/Mx2__MODULE_HEAD.d --->

// Missing type:

alias void* MxSettingsProperty;

// <--- mixin/Mx2__MODULE_HEAD.d ---


// The contents of this structure are private and should only be accessed
// through the public API.
struct Action /* : GObject.InitiallyUnowned */ {
   mixin Gio2.Action.__interface__;
   alias parent this;
   alias parent super_;
   alias parent initiallyunowned;
   GObject2.InitiallyUnowned parent;
   private ActionPrivate* priv;


   // Creates a new, blank, #MxAction
   // RETURNS: a newly allocated #MxAction
   static Action* /*new*/ new_()() nothrow {
      return mx_action_new();
   }
   static auto opCall()() {
      return mx_action_new();
   }

   // Creates a new #MxAction with the name and callback set
   // RETURNS: a newly allocated #MxAction
   // <name>: name of the action
   // <display_name>: name of the action to display to the user
   // <activated_cb>: callback to connect to the activated signal
   // <user_data>: user data to be passed to the callback
   static Action* /*new*/ new_full(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ display_name, ActionCallbackFunc activated_cb, AT2 /*void*/ user_data) nothrow {
      return mx_action_new_full(toCString!(char*)(name), toCString!(char*)(display_name), activated_cb, UpCast!(void*)(user_data));
   }
   static auto opCall(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*char*/ display_name, ActionCallbackFunc activated_cb, AT2 /*void*/ user_data) {
      return mx_action_new_full(toCString!(char*)(name), toCString!(char*)(display_name), activated_cb, UpCast!(void*)(user_data));
   }

   // VERSION: 1.4
   // Creates a new stateful action.
   // 
   // @state is the initial state of the action.  All future state values
   // must have the same #GVariantType as the initial state.
   // RETURNS: a new #MxAction
   // <name>: the name of the action
   // <parameter_type>: the type of the parameter to the activate function
   // <state>: the initial state of the action
   static Action* new_stateful(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*GLib2.VariantType*/ parameter_type, AT2 /*GLib2.Variant*/ state) nothrow {
      return mx_action_new_stateful(toCString!(char*)(name), UpCast!(GLib2.VariantType*)(parameter_type), UpCast!(GLib2.Variant*)(state));
   }
   static auto opCall(AT0, AT1, AT2)(AT0 /*char*/ name, AT1 /*GLib2.VariantType*/ parameter_type, AT2 /*GLib2.Variant*/ state) {
      return mx_action_new_stateful(toCString!(char*)(name), UpCast!(GLib2.VariantType*)(parameter_type), UpCast!(GLib2.Variant*)(state));
   }

   // VERSION: 1.4
   // Creates a new action with a parameter.
   // 
   // The created action is stateless.  See mx_action_new_stateful().
   // RETURNS: a new #MxAction
   // <name>: the name of the action
   // <parameter_type>: the type of parameter to the activate function
   static Action* new_with_parameter(AT0, AT1)(AT0 /*char*/ name, AT1 /*GLib2.VariantType*/ parameter_type=null) nothrow {
      return mx_action_new_with_parameter(toCString!(char*)(name), UpCast!(GLib2.VariantType*)(parameter_type));
   }
   static auto opCall(AT0, AT1)(AT0 /*char*/ name, AT1 /*GLib2.VariantType*/ parameter_type=null) {
      return mx_action_new_with_parameter(toCString!(char*)(name), UpCast!(GLib2.VariantType*)(parameter_type));
   }

   // Get the value of the active property
   // RETURNS: #TRUE if the action is active
   int get_active()() nothrow {
      return mx_action_get_active(&this);
   }

   // Get the display name of the action
   // RETURNS: display-name of the action, owned by MxAction
   char* get_display_name()() nothrow {
      return mx_action_get_display_name(&this);
   }

   // Get the icon of the action
   // RETURNS: icon of the action, owned by MxAction
   char* get_icon()() nothrow {
      return mx_action_get_icon(&this);
   }

   // Get the name of the action
   // RETURNS: name of the action, owned by MxAction
   char* get_name()() nothrow {
      return mx_action_get_name(&this);
   }

   // Set the value of the active property
   // <active>: the value to set
   void set_active()(int active) nothrow {
      mx_action_set_active(&this, active);
   }

   // Set the name of the action to display to the user
   // <name>: new display name to set
   void set_display_name(AT0)(AT0 /*char*/ name) nothrow {
      mx_action_set_display_name(&this, toCString!(char*)(name));
   }

   // The icon to be used in a visual representation of an action.
   // <name>: new icon to set
   void set_icon(AT0)(AT0 /*char*/ name) nothrow {
      mx_action_set_icon(&this, toCString!(char*)(name));
   }

   // Set the name of the action
   // <name>: new name to set
   void set_name(AT0)(AT0 /*char*/ name) nothrow {
      mx_action_set_name(&this, toCString!(char*)(name));
   }

   // VERSION: 1.4
   // Indicates that the action was just activated.
   // 
   // @parameter will always be of the expected type.  In the event that
   // an incorrect type was given, no signal will be emitted.
   // <parameter>: the parameter to the activation
   extern (C) alias static void function (Action* this_, GLib2.Variant* parameter=null, void* user_data=null) nothrow signal_activate;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"activate", CB/*:signal_activate*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_activate)||_ttmm!(CB, signal_activate)()) {
      return signal_connect_data!()(&this, cast(char*)"activate",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // DEPRECATED (v1.4) glib:signal: activated - Use MxAction::activate instead.
   // Emitted when the MxAction is activated.
   extern (C) alias static void function (Action* this_, void* user_data=null) nothrow signal_activated;
   ulong signal_connect(string name:"activated", CB/*:signal_activated*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_activated)||_ttmm!(CB, signal_activated)()) {
      return signal_connect_data!()(&this, cast(char*)"activated",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}


// Callback function called when action is activated.
// <action>: An #MxAction
// <user_data>: user data
extern (C) alias void function (Action* action, void* user_data) nothrow ActionCallbackFunc;

struct ActionClass {
   GObject2.InitiallyUnownedClass parent_class;
   extern (C) void function (Action* action) nothrow activated;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct ActionPrivate {
}

struct ActorManager /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   ActorManagerPrivate* priv;


   // VERSION: 1.2
   // Creates a new #MxActorManager, associated with the given stage.
   // 
   // <note><para>
   // A reference will not be taken on the stage, and when the stage is destroyed,
   // the actor manager will lose a reference. The actor manager can be kept
   // alive by taking a reference, but will no longer divide up events.
   // </para></note>
   // RETURNS: An #MxActorManager, tied to the given #ClutterStage
   // <stage>: A #ClutterStage
   static ActorManager* new_(AT0)(AT0 /*Clutter.Stage*/ stage) nothrow {
      return mx_actor_manager_new(UpCast!(Clutter.Stage*)(stage));
   }
   static auto opCall(AT0)(AT0 /*Clutter.Stage*/ stage) {
      return mx_actor_manager_new(UpCast!(Clutter.Stage*)(stage));
   }

   // VERSION: 1.2
   // Get the MxActorManager associated with a stage, or creates one if this is the
   // first call to the function with the given #ClutterStage.
   // 
   // This is a convenience function that allows for easy association of one
   // #MxActorManager to a #ClutterStage.
   // RETURNS: An #MxActorManager
   // <stage>: A #ClutterStage
   static ActorManager* get_for_stage(AT0)(AT0 /*Clutter.Stage*/ stage) nothrow {
      return mx_actor_manager_get_for_stage(UpCast!(Clutter.Stage*)(stage));
   }

   // VERSION: 1.2
   // Adds @actor to @container. The actor may not be parented immediately,
   // or at all, if the operation is cancelled.
   // 
   // On successful completion, the #MxActorManager::actor_added signal will
   // be fired.
   // RETURNS: The ID for this operation.
   // <container>: A #ClutterContainer
   // <actor>: A #ClutterActor
   c_ulong add_actor(AT0, AT1)(AT0 /*Clutter.Container*/ container, AT1 /*Clutter.Actor*/ actor) nothrow {
      return mx_actor_manager_add_actor(&this, UpCast!(Clutter.Container*)(container), UpCast!(Clutter.Actor*)(actor));
   }

   // VERSION: 1.2
   // Cancels the given operation, if it exists. The
   // #MxActorManager::operation_cancelled signal is fired whenever an operation
   // is cancelled.
   // <id>: An operation ID
   void cancel_operation()(c_ulong id) nothrow {
      mx_actor_manager_cancel_operation(&this, id);
   }

   // VERSION: 1.2
   // Cancels all operations associated with the given actor.
   // <actor>: A #ClutterActor
   void cancel_operations(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
      mx_actor_manager_cancel_operations(&this, UpCast!(Clutter.Actor*)(actor));
   }

   // Unintrospectable method: create_actor() / mx_actor_manager_create_actor()
   // VERSION: 1.2
   // Creates a #ClutterActor. The actor may not be created immediately,
   // or at all, if the operation is cancelled.
   // 
   // On successful completion, the #MxActorManager::actor_created signal will
   // be fired.
   // RETURNS: The ID for this operation.
   // <create_func>: A #ClutterActor creation function
   // <userdata>: data to be passed to the function, or %NULL
   // <destroy_func>: callback to invoke before the operation is removed
   c_ulong create_actor(AT0)(ActorManagerCreateFunc create_func, AT0 /*void*/ userdata, GLib2.DestroyNotify destroy_func) nothrow {
      return mx_actor_manager_create_actor(&this, create_func, UpCast!(void*)(userdata), destroy_func);
   }

   // VERSION: 1.2
   // Retrieves the amount of operations left in the queue.
   // RETURNS: Number of operations left to perform
   uint get_n_operations()() nothrow {
      return mx_actor_manager_get_n_operations(&this);
   }

   // VERSION: 1.2
   // Gets the #ClutterStage the actor manager is associated with.
   // RETURNS: The #ClutterStage the actor is associated with.
   Clutter.Stage* get_stage()() nothrow {
      return mx_actor_manager_get_stage(&this);
   }

   // VERSION: 1.2
   // Retrieves the current time slice being used for operations.
   // RETURNS: The time-slice being used, in milliseconds
   uint get_time_slice()() nothrow {
      return mx_actor_manager_get_time_slice(&this);
   }

   // VERSION: 1.2
   // Removes @actor from @container.
   // 
   // On successful completion, the #MxActorManager::actor_removed signal will
   // be fired.
   // 
   // <note><para>
   // The actor may not be removed immediately, and thus you may want to set
   // the actor's opacity to 0 before calling this function.
   // </para></note>
   // RETURNS: The ID for this operation.
   // <container>: A #ClutterContainer
   // <actor>: A #ClutterActor
   c_ulong remove_actor(AT0, AT1)(AT0 /*Clutter.Container*/ container, AT1 /*Clutter.Actor*/ actor) nothrow {
      return mx_actor_manager_remove_actor(&this, UpCast!(Clutter.Container*)(container), UpCast!(Clutter.Actor*)(actor));
   }

   // VERSION: 1.2
   // Removes the container. This is a utility function that works by first
   // removing all the children of the container, then the children itself. This
   // effectively spreads the load of removing a large container. All prior
   // operations associated with this container will be cancelled.
   // 
   // <note><para>
   // The container may not be removed immediately, and thus you may want to set
   // the container's opacity to 0 before calling this function.
   // </para></note>
   // <container>: A #ClutterContainer
   void remove_container(AT0)(AT0 /*Clutter.Container*/ container) nothrow {
      mx_actor_manager_remove_container(&this, UpCast!(Clutter.Container*)(container));
   }

   // VERSION: 1.2
   // Sets the amount of time the actor manager will spend performing operations,
   // before yielding to allow any necessary redrawing to occur.
   // 
   // Lower times will lead to smoother performance, but will increase the amount
   // of time it takes for operations to complete.
   // <msecs>: A time, in milliseconds
   void set_time_slice()(uint msecs) nothrow {
      mx_actor_manager_set_time_slice(&this, msecs);
   }

   // VERSION: 1.2
   // Emitted when an actor add operation has completed.
   // <id>: The operation ID
   // <container>: The #ClutterContainer the actor was added to
   // <actor>: The added #ClutterActor
   extern (C) alias static void function (ActorManager* this_, c_ulong id, Clutter.Actor* container, Clutter.Actor* actor, void* user_data=null) nothrow signal_actor_added;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"actor-added", CB/*:signal_actor_added*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_actor_added)||_ttmm!(CB, signal_actor_added)()) {
      return signal_connect_data!()(&this, cast(char*)"actor-added",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.2
   // Emitted when an actor creation operation has completed.
   // <id>: The operation ID
   // <actor>: The created #ClutterActor
   extern (C) alias static void function (ActorManager* this_, c_ulong id, Clutter.Actor* actor, void* user_data=null) nothrow signal_actor_created;
   ulong signal_connect(string name:"actor-created", CB/*:signal_actor_created*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_actor_created)||_ttmm!(CB, signal_actor_created)()) {
      return signal_connect_data!()(&this, cast(char*)"actor-created",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.2
   // Emitted when all queued operations involving @actor have completed.
   // <actor>: The #ClutterActor to which the signal pertains
   extern (C) alias static void function (ActorManager* this_, Clutter.Actor* actor, void* user_data=null) nothrow signal_actor_finished;
   ulong signal_connect(string name:"actor-finished", CB/*:signal_actor_finished*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_actor_finished)||_ttmm!(CB, signal_actor_finished)()) {
      return signal_connect_data!()(&this, cast(char*)"actor-finished",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.2
   // Emitted when an actor remove operation has completed.
   // <id>: The operation ID
   // <container>: The #ClutterContainer the actor was removed from
   // <actor>: The removed #ClutterActor
   extern (C) alias static void function (ActorManager* this_, c_ulong id, Clutter.Actor* container, Clutter.Actor* actor, void* user_data=null) nothrow signal_actor_removed;
   ulong signal_connect(string name:"actor-removed", CB/*:signal_actor_removed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_actor_removed)||_ttmm!(CB, signal_actor_removed)()) {
      return signal_connect_data!()(&this, cast(char*)"actor-removed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.2
   // Emitted when an operation has been cancelled.
   // <id>: The operation id
   extern (C) alias static void function (ActorManager* this_, c_ulong id, void* user_data=null) nothrow signal_operation_cancelled;
   ulong signal_connect(string name:"operation-cancelled", CB/*:signal_operation_cancelled*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_operation_cancelled)||_ttmm!(CB, signal_operation_cancelled)()) {
      return signal_connect_data!()(&this, cast(char*)"operation-cancelled",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.2
   // Emitted when an operation has completed successfully.
   // <id>: The operation id
   extern (C) alias static void function (ActorManager* this_, c_ulong id, void* user_data=null) nothrow signal_operation_completed;
   ulong signal_connect(string name:"operation-completed", CB/*:signal_operation_completed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_operation_completed)||_ttmm!(CB, signal_operation_completed)()) {
      return signal_connect_data!()(&this, cast(char*)"operation-completed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.2
   // Emitted when an operation has failed.
   // <id>: The operation id
   // <error>: A #GError describing the reason of the failure
   extern (C) alias static void function (ActorManager* this_, c_ulong id, GLib2.Error* error, void* user_data=null) nothrow signal_operation_failed;
   ulong signal_connect(string name:"operation-failed", CB/*:signal_operation_failed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_operation_failed)||_ttmm!(CB, signal_operation_failed)()) {
      return signal_connect_data!()(&this, cast(char*)"operation-failed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct ActorManagerClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function (ActorManager* manager, c_ulong id, Clutter.Actor* actor) nothrow actor_created;
   extern (C) void function (ActorManager* manager, c_ulong id, Clutter.Container* container, Clutter.Actor* actor) nothrow actor_added;
   extern (C) void function (ActorManager* manager, c_ulong id, Clutter.Container* container, Clutter.Actor* actor) nothrow actor_removed;
   extern (C) void function (ActorManager* manager, Clutter.Actor* actor) nothrow actor_finished;
   extern (C) void function (ActorManager* manager, c_ulong id) nothrow operation_completed;
   extern (C) void function (ActorManager* manager, c_ulong id) nothrow operation_cancelled;
   extern (C) void function (ActorManager* manager, c_ulong id, GLib2.Error* error) nothrow operation_failed;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

// Unintrospectable callback: ActorManagerCreateFunc() / ()
extern (C) alias Clutter.Actor* function (ActorManager* manager, void* userdata) nothrow ActorManagerCreateFunc;

enum ActorManagerError {
   CONTAINER_DESTROYED = 0,
   ACTOR_DESTROYED = 1,
   CREATION_FAILED = 2,
   UNKNOWN_OPERATION = 3
}
struct ActorManagerPrivate {
}


// Class for handling an interval between to values. The contents of
// the #MxAdjustment are private and should be accessed using the
// public API.
struct Adjustment /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private AdjustmentPrivate* priv;


   // Create a new MxAdjustment
   // RETURNS: a newly allocated MxAdjustment
   static Adjustment* /*new*/ new_()() nothrow {
      return mx_adjustment_new();
   }
   static auto opCall()() {
      return mx_adjustment_new();
   }

   // Create a new MxAdjustment with the properties set to the values specified.
   // RETURNS: a newly allocated MxAdjustment
   // <value>: A #gdouble
   // <lower>: A #gdouble
   // <upper>: A #gdouble
   // <step_increment>: A #gdouble
   // <page_increment>: A #gdouble
   // <page_size>: A #gdouble
   static Adjustment* /*new*/ new_with_values()(double value, double lower, double upper, double step_increment, double page_increment, double page_size) nothrow {
      return mx_adjustment_new_with_values(value, lower, upper, step_increment, page_increment, page_size);
   }
   static auto opCall()(double value, double lower, double upper, double step_increment, double page_increment, double page_size) {
      return mx_adjustment_new_with_values(value, lower, upper, step_increment, page_increment, page_size);
   }

   // VERSION: 1.2
   // Get the value of the #MxAdjustment:clamp-value property.
   // RETURNS: the current value of the "clamp-value" property.
   int get_clamp_value()() nothrow {
      return mx_adjustment_get_clamp_value(&this);
   }

   // Get the value of the #MxAdjustment:elastic property.
   // RETURNS: the current value of the "elastic" property.
   int get_elastic()() nothrow {
      return mx_adjustment_get_elastic(&this);
   }

   // Get the value of the #MxAdjustment:lower property.
   // RETURNS: the current value of the "lower" property.
   double get_lower()() nothrow {
      return mx_adjustment_get_lower(&this);
   }

   // Get the value of the MxAdjustment:page-increment property.
   // RETURNS: the current value of the "page-increment" property.
   double get_page_increment()() nothrow {
      return mx_adjustment_get_page_increment(&this);
   }

   // Get the value of the #MxAdjustment:page-size property.
   // RETURNS: the current value of the "page-size" property.
   double get_page_size()() nothrow {
      return mx_adjustment_get_page_size(&this);
   }

   // Get the value of the MxAdjustment:step-increment property.
   // RETURNS: the current value of the "step-increment" property.
   double get_step_increment()() nothrow {
      return mx_adjustment_get_step_increment(&this);
   }

   // Get the value of the #MxAdjustment:upper property.
   // RETURNS: the current value of the "upper" property.
   double get_upper()() nothrow {
      return mx_adjustment_get_upper(&this);
   }

   // Get the current value of the #MxAdjustment:value property
   // RETURNS: the current value of the "value" property
   double get_value()() nothrow {
      return mx_adjustment_get_value(&this);
   }

   // Get the various properties of MxAdjustment.
   // <value>: A #gdouble
   // <lower>: A #gdouble
   // <upper>: A #gdouble
   // <step_increment>: A #gdouble
   // <page_increment>: A #gdouble
   // <page_size>: A #gdouble
   void get_values(AT0, AT1, AT2, AT3, AT4, AT5)(AT0 /*double*/ value, AT1 /*double*/ lower, AT2 /*double*/ upper, AT3 /*double*/ step_increment, AT4 /*double*/ page_increment, AT5 /*double*/ page_size) nothrow {
      mx_adjustment_get_values(&this, UpCast!(double*)(value), UpCast!(double*)(lower), UpCast!(double*)(upper), UpCast!(double*)(step_increment), UpCast!(double*)(page_increment), UpCast!(double*)(page_size));
   }

   // Interpolate #MxAdjustment:value to the new value specified by @value, using
   // the mode and duration given.
   // <value>: A #gdouble
   // <duration>: duration in milliseconds
   // <mode>: A #ClutterAnimationMode
   void interpolate()(double value, uint duration, c_ulong mode) nothrow {
      mx_adjustment_interpolate(&this, value, duration, mode);
   }

   // Interpolate the value of #MxAdjustment:value to a new value calculated from
   // @offset.
   // <offset>: A #gdouble
   // <duration>: duration in milliseconds
   // <mode>: A #ClutterAnimationMode
   void interpolate_relative()(double offset, uint duration, c_ulong mode) nothrow {
      mx_adjustment_interpolate_relative(&this, offset, duration, mode);
   }

   // VERSION: 1.2
   // Set the value of the #MxAdjustment:clamp-value property.
   // <clamp>: a #gboolean
   void set_clamp_value()(int clamp) nothrow {
      mx_adjustment_set_clamp_value(&this, clamp);
   }

   // Set the value of the #MxAdjustment:elastic property.
   // <elastic>: A #gboolean
   void set_elastic()(int elastic) nothrow {
      mx_adjustment_set_elastic(&this, elastic);
   }

   // Set the value of the #MxAdjustment:lower property.
   // <lower>: A #gdouble
   void set_lower()(double lower) nothrow {
      mx_adjustment_set_lower(&this, lower);
   }

   // Set the value of the #MxAdjustment:page-increment property.
   // <increment>: A #gdouble
   void set_page_increment()(double increment) nothrow {
      mx_adjustment_set_page_increment(&this, increment);
   }

   // Set the #MxAdjustment:page-size property.
   // <page_size>: A #gdouble
   void set_page_size()(double page_size) nothrow {
      mx_adjustment_set_page_size(&this, page_size);
   }

   // Set the value of the #MxAdjustment:step-increment property.
   // <increment>: A #gdouble
   void set_step_increment()(double increment) nothrow {
      mx_adjustment_set_step_increment(&this, increment);
   }

   // Set the value of the #MxAdjustment:upper property.
   // <upper>: A #gdouble
   void set_upper()(double upper) nothrow {
      mx_adjustment_set_upper(&this, upper);
   }

   // Set the value of the #MxAdjustment:value property.
   // <value>: A #gdouble
   void set_value()(double value) nothrow {
      mx_adjustment_set_value(&this, value);
   }

   // Set the various properties of MxAdjustment.
   // <value>: A #gdouble
   // <lower>: A #gdouble
   // <upper>: A #gdouble
   // <step_increment>: A #gdouble
   // <page_increment>: A #gdouble
   // <page_size>: A #gdouble
   void set_values()(double value, double lower, double upper, double step_increment, double page_increment, double page_size) nothrow {
      mx_adjustment_set_values(&this, value, lower, upper, step_increment, page_increment, page_size);
   }
   // Emitted when any of the adjustment values have changed
   extern (C) alias static void function (Adjustment* this_, void* user_data=null) nothrow signal_changed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"changed", CB/*:signal_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_changed)||_ttmm!(CB, signal_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
   // Emitted when the animation started by mx_adjustment_interpolate completes
   extern (C) alias static void function (Adjustment* this_, void* user_data=null) nothrow signal_interpolation_completed;
   ulong signal_connect(string name:"interpolation-completed", CB/*:signal_interpolation_completed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_interpolation_completed)||_ttmm!(CB, signal_interpolation_completed)()) {
      return signal_connect_data!()(&this, cast(char*)"interpolation-completed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// Base class for #MxAdjustment.
struct AdjustmentClass {
   private GObject2.ObjectClass parent_class;
   extern (C) void function (Adjustment* adjustment) nothrow changed;
   extern (C) void function (Adjustment* adjustment) nothrow interpolation_completed;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
}

struct AdjustmentPrivate {
}

// Set the alignment of the item
enum Align {
   START = 0,
   MIDDLE = 1,
   END = 2
}

// The contents of this structure are private and should only be accessed
// through the public API.
struct Application /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   ApplicationPrivate* priv;


   // Intialises everything needed to operate Clutter and use #MxApplication.
   // See clutter_init().
   // RETURNS: the #MxApplication singleton.
   // <argc>: The number of arguments in argv.
   // <argv>: A pointer to an array of arguments
   // <name>: Unique application name.
   // <flags>: Application flags.
   static Application* /*new*/ new_(AT0, AT1)(/*inout*/ int* argc, /*inout*/ AT0 /*char***/ argv, AT1 /*char*/ name, ApplicationFlags flags) nothrow {
      return mx_application_new(argc, toCString!(char***)(argv), toCString!(char*)(name), flags);
   }
   static auto opCall(AT0, AT1)(/*inout*/ int* argc, /*inout*/ AT0 /*char***/ argv, AT1 /*char*/ name, ApplicationFlags flags) {
      return mx_application_new(argc, toCString!(char***)(argv), toCString!(char*)(name), flags);
   }

   // Add an action to the application.
   // <action>: an #MxAction
   void add_action(AT0)(AT0 /*Action*/ action) nothrow {
      mx_application_add_action(&this, UpCast!(Action*)(action));
   }

   // Adds a window to the list of windows associated with @application. If this
   // is the first window, it will be treated as the primary window and used for
   // startup notification.
   // 
   // This function does not take a reference on @window.
   // <window>: The #MxWindow to add to the application
   void add_window(AT0)(AT0 /*Window*/ window) nothrow {
      mx_application_add_window(&this, UpCast!(Window*)(window));
   }

   // Creates a window and associates it with the application.
   // RETURNS: An #MxWindow.
   Window* create_window()() nothrow {
      return mx_application_create_window(&this);
   }

   // Retrieves all actions registered on @application.
   // 
   // of #MxAction<!-- -->s. Use g_list_free() on the returned list
   // when done.
   // RETURNS: a list
   GLib2.List* /*new container*/ get_actions()() nothrow {
      return mx_application_get_actions(&this);
   }

   // Get the application flags that where set on @application when created.
   // RETURNS: the application flags
   ApplicationFlags get_flags()() nothrow {
      return mx_application_get_flags(&this);
   }

   // Retrieves all windows added to @application.
   // 
   // of #MxWindow<!-- -->s. The returned list is owned by
   // @application and must not be altered.
   // RETURNS: a list
   GLib2.List* get_windows()() nothrow {
      return mx_application_get_windows(&this);
   }

   // Run the named action for the application.
   // <name>: name of the action to invoke
   void invoke_action(AT0)(AT0 /*char*/ name) nothrow {
      mx_application_invoke_action(&this, toCString!(char*)(name));
   }

   // VERSION: 1.4
   // Run the named action for the application, passing @variant as the parameter
   // for the action.
   // <name>: name of the action to invoke
   // <variant>: parameter for the action
   void invoke_action_with_parameter(AT0, AT1)(AT0 /*char*/ name, AT1 /*GLib2.Variant*/ variant) nothrow {
      mx_application_invoke_action_with_parameter(&this, toCString!(char*)(name), UpCast!(GLib2.Variant*)(variant));
   }

   // Query whether #MxApplication is running. This will also return #TRUE if the
   // given #MxApplication is single instance and there is an instance already
   // running.
   // RETURNS: #TRUE if the application is running
   int is_running()() nothrow {
      return mx_application_is_running(&this);
   }

   // Stop the application from running and quit the main loop. This will cause
   // the call to mx_application_run() to complete.
   void quit()() nothrow {
      mx_application_quit(&this);
   }

   // Remove the action with the specified name from the application.
   // <name>: name of the action to remove
   void remove_action(AT0)(AT0 /*char*/ name) nothrow {
      mx_application_remove_action(&this, toCString!(char*)(name));
   }

   // Remove the specified window from the application. This will cause the window
   // to be unreferenced and destroyed unless another reference is held on it.
   // <window>: an #MxWindow
   void remove_window(AT0)(AT0 /*Window*/ window) nothrow {
      mx_application_remove_window(&this, UpCast!(Window*)(window));
   }

   // Run the main loop of the application and start processing events. This
   // function will not return until the application is quit. If the application
   // is single instance and an existing instance is already running, this will
   // cause the existing instance to be raised and the function will complete
   // immediately.
   void run()() nothrow {
      mx_application_run(&this);
   }
   // Emitted when an action has been added or removed from the MxApplication.
   extern (C) alias static void function (Application* this_, void* user_data=null) nothrow signal_actions_changed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"actions-changed", CB/*:signal_actions_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_actions_changed)||_ttmm!(CB, signal_actions_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"actions-changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   //  --- mixin/Mx2_Application.d --->

   static Application* /*new*/ new_d(AT0)(ref string argv[], AT0 /*char*/ name, ApplicationFlags flags) {
      // mx_application_new() wants 'argc' as 'int*'...
      c_int argc = cast(c_int)argv.length;
      auto c_argv = argvToC(argv);
      auto app = mx_application_new(&argc, &c_argv, toCString!(char*)(name), flags);
      argv = argvFromC(argc, c_argv);
      return app;
   }
   static auto opCall(AT0)(ref string argv[], AT0 name, ApplicationFlags flags) {
      return new_d(argv, name, flags);
   }
   
   // <--- mixin/Mx2_Application.d ---
}

struct ApplicationClass {
   GObject2.ObjectClass parent_class;
   // RETURNS: An #MxWindow.
   extern (C) Window* function (Application* application) nothrow create_window;
   extern (C) void function (Application* application) nothrow raise;
   extern (C) void function (Application* app) nothrow actions_changed;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

enum ApplicationFlags {
   SINGLE_INSTANCE = 1,
   KEEP_ALIVE = 4
}
struct ApplicationPrivate {
}

// The #MxBin struct contains only private data
struct Bin /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance widget;
   Widget parent_instance;
   private BinPrivate* priv;


   // Allocates the child of an #MxBin using the width and height from @box.
   // This function should usually only be called by subclasses of #MxBin.
   // 
   // This function can be used to allocate the child of an #MxBin if no special
   // allocation requirements are needed. It is similar to
   // #mx_allocate_align_fill, except that it reads the alignment, padding and
   // fill values from the #MxBin, and will call #clutter_actor_allocate on the
   // child.
   // <box>: The allocation box of the parent actor.
   // <flags>: #ClutterAllocationFlags, usually provided by the. clutter_actor_allocate function.
   void allocate_child(AT0)(AT0 /*Clutter.ActorBox*/ box, Clutter.AllocationFlags flags) nothrow {
      mx_bin_allocate_child(&this, UpCast!(Clutter.ActorBox*)(box), flags);
   }

   // Retrieves the horizontal and vertical alignment of the child
   // inside a #MxBin, as set by mx_bin_set_alignment().
   // <x_align>: return location for the horizontal alignment, or %NULL
   // <y_align>: return location for the vertical alignment, or %NULL
   void get_alignment(AT0, AT1)(AT0 /*Align*/ x_align, AT1 /*Align*/ y_align) nothrow {
      mx_bin_get_alignment(&this, UpCast!(Align*)(x_align), UpCast!(Align*)(y_align));
   }

   // Retrieves a pointer to the child of @bin.
   // RETURNS: a #ClutterActor, or %NULL
   Clutter.Actor* get_child()() nothrow {
      return mx_bin_get_child(&this);
   }

   // Retrieves the horizontal and vertical fill settings
   // <x_fill>: return location for the horizontal fill, or %NULL
   // <y_fill>: return location for the vertical fill, or %NULL
   void get_fill()(/*out*/ int* x_fill, /*out*/ int* y_fill) nothrow {
      mx_bin_get_fill(&this, x_fill, y_fill);
   }

   // Sets the horizontal and vertical alignment of the child
   // inside a #MxBin.
   // <x_align>: horizontal alignment
   // <y_align>: vertical alignment
   void set_alignment()(Align x_align, Align y_align) nothrow {
      mx_bin_set_alignment(&this, x_align, y_align);
   }

   // Sets @child as the child of @bin.
   // 
   // If @bin already has a child, the previous child is removed.
   // <child>: a #ClutterActor, or %NULL
   void set_child(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      mx_bin_set_child(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Sets whether the child of @bin should fill out the horizontal
   // and/or vertical allocation of the parent
   // <x_fill>: %TRUE if the child should fill horizontally the @bin
   // <y_fill>: %TRUE if the child should fill vertically the @bin
   void set_fill()(int x_fill, int y_fill) nothrow {
      mx_bin_set_fill(&this, x_fill, y_fill);
   }
}

// The #MxBinClass struct contains only private data
struct BinClass {
   private WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct BinPrivate {
}

struct BorderImage {
   char* uri;
   int top, right, bottom, left;

   int equal(AT0)(AT0 /*BorderImage*/ b2) nothrow {
      return mx_border_image_equal(&this, UpCast!(BorderImage*)(b2));
   }
   static void set_from_string(AT0, AT1, AT2)(AT0 /*GObject2.Value*/ value, AT1 /*char*/ str, AT2 /*char*/ filename) nothrow {
      mx_border_image_set_from_string(UpCast!(GObject2.Value*)(value), toCString!(char*)(str), toCString!(char*)(filename));
   }
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct BoxLayout /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Scrollable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   private BoxLayoutPrivate* priv;


   // Create a new #MxBoxLayout.
   // RETURNS: a newly allocated #MxBoxLayout
   static BoxLayout* new_()() nothrow {
      return mx_box_layout_new();
   }
   static auto opCall()() {
      return mx_box_layout_new();
   }
   static BoxLayout* new_with_orientation()(Orientation orientation) nothrow {
      return mx_box_layout_new_with_orientation(orientation);
   }
   static auto opCall()(Orientation orientation) {
      return mx_box_layout_new_with_orientation(orientation);
   }

   // Inserts @actor at @position in @box.
   // <actor>: the #ClutterActor actor to add to the box layout
   // <position>: the position where to insert the actor
   void add_actor(AT0)(AT0 /*Clutter.Actor*/ actor, int position) nothrow {
      mx_box_layout_add_actor(&this, UpCast!(Clutter.Actor*)(actor), position);
   }

   // Unintrospectable method: add_actor_with_properties() / mx_box_layout_add_actor_with_properties()
   // Inserts @actor at @position in the layout @box. You can set some layout
   // properties on the child at the same time.
   // 
   // If @position is negative, or is larger than the number of actors in the
   // layout, the new actor is added on to the end of the list.
   // <actor>: the #ClutterActor actor to add to the box layout
   // <position>: the position where to insert the actor
   // <first_property>: name of the first property to set
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias mx_box_layout_add_actor_with_properties add_actor_with_properties; // Variadic
   +/

   // Get the value of the #MxBoxLayoutChild:expand property
   // RETURNS: the current value of the "expand" property
   // <child>: A #ClutterActor
   int child_get_expand(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_box_layout_child_get_expand(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the value of the #MxBoxLayoutChild:x-align property
   // RETURNS: the current value of the "x-align" property
   // <child>: A #ClutterActor
   Align child_get_x_align(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_box_layout_child_get_x_align(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the value of the #MxBoxLayoutChild:x-fill property.
   // RETURNS: the current value of the "x-fill" property.
   // <child>: A #ClutterActor
   int child_get_x_fill(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_box_layout_child_get_x_fill(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the value of the #MxBoxLayoutChild:y-align property.
   // RETURNS: the current value of the "y-align" property.
   // <child>: A #ClutterActor
   Align child_get_y_align(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_box_layout_child_get_y_align(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the value of the #MxBoxLayoutChild:y-fill property
   // RETURNS: the current value of the "y-fill" property
   // <child>: A #ClutterActor
   int child_get_y_fill(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_box_layout_child_get_y_fill(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Set the value of the #MxBoxLayoutChild:expand property.
   // <child>: A #ClutterActor
   // <expand>: A #gboolean
   void child_set_expand(AT0)(AT0 /*Clutter.Actor*/ child, int expand) nothrow {
      mx_box_layout_child_set_expand(&this, UpCast!(Clutter.Actor*)(child), expand);
   }

   // Set the value of the #MxBoxLayoutChild:x-align property.
   // <child>: A #ClutterActor
   // <x_align>: An #MxAlign
   void child_set_x_align(AT0)(AT0 /*Clutter.Actor*/ child, Align x_align) nothrow {
      mx_box_layout_child_set_x_align(&this, UpCast!(Clutter.Actor*)(child), x_align);
   }

   // Set the value of the #MxBoxLayoutChild:x-fill property.
   // <child>: A #ClutterActor
   // <x_fill>: A #gboolean
   void child_set_x_fill(AT0)(AT0 /*Clutter.Actor*/ child, int x_fill) nothrow {
      mx_box_layout_child_set_x_fill(&this, UpCast!(Clutter.Actor*)(child), x_fill);
   }

   // Set the value of the #MxBoxLayoutChild:y-align property.
   // <child>: A #ClutterActor
   // <y_align>: An #MxAlign
   void child_set_y_align(AT0)(AT0 /*Clutter.Actor*/ child, Align y_align) nothrow {
      mx_box_layout_child_set_y_align(&this, UpCast!(Clutter.Actor*)(child), y_align);
   }

   // Set the value of the #MxBoxLayoutChild:y-fill property.
   // <child>: A #ClutterActor
   // <y_fill>: A #gboolean
   void child_set_y_fill(AT0)(AT0 /*Clutter.Actor*/ child, int y_fill) nothrow {
      mx_box_layout_child_set_y_fill(&this, UpCast!(Clutter.Actor*)(child), y_fill);
   }

   // Get the value of the #MxBoxLayout:enable-animations property.
   // RETURNS: #TRUE if animations enabled
   int get_enable_animations()() nothrow {
      return mx_box_layout_get_enable_animations(&this);
   }

   // Get the value of the #MxBoxLayout:orientation property.
   // RETURNS: the orientation of the layout
   Orientation get_orientation()() nothrow {
      return mx_box_layout_get_orientation(&this);
   }

   // VERSION: 1.2
   // Get the value of the #MxBoxLayout:scroll-to-focused property.
   // RETURNS: #TRUE if automatically scrolling to the focused actor is enabled
   int get_scroll_to_focused()() nothrow {
      return mx_box_layout_get_scroll_to_focused(&this);
   }

   // Get the spacing between children in pixels
   // RETURNS: the spacing value
   uint get_spacing()() nothrow {
      return mx_box_layout_get_spacing(&this);
   }

   // Enable animations when certain properties change.
   // <enable_animations>: #TRUE to enable animations
   void set_enable_animations()(int enable_animations) nothrow {
      mx_box_layout_set_enable_animations(&this, enable_animations);
   }

   // Set the orientation of the box layout.
   // <orientation>: orientation value for the layout
   void set_orientation()(Orientation orientation) nothrow {
      mx_box_layout_set_orientation(&this, orientation);
   }

   // VERSION: 1.2
   // Enables or disables automatic scrolling to the focused actor.
   // <scroll_to_focused>: #TRUE to enable automatically scrolling to the focused actor
   void set_scroll_to_focused()(int scroll_to_focused) nothrow {
      mx_box_layout_set_scroll_to_focused(&this, scroll_to_focused);
   }

   // Set the amount of spacing between children in pixels
   // <spacing>: the spacing value
   void set_spacing()(uint spacing) nothrow {
      mx_box_layout_set_spacing(&this, spacing);
   }
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct BoxLayoutChild /* : Clutter.ChildMeta */ {
   alias parent this;
   alias parent super_;
   alias parent childmeta;
   Clutter.ChildMeta parent;
   private int expand;
    static import std.bitmanip; mixin(std.bitmanip.bitfields!(
   uint, "x_fill", 1,
   uint, "y_fill", 1,
    uint, "__dummy32A", 30));
   private Align x_align, y_align;
}

struct BoxLayoutChildClass {
   Clutter.ChildMetaClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct BoxLayoutChildPrivate {
}

struct BoxLayoutClass {
   WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct BoxLayoutPrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct Button /* : Bin */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance bin;
   Bin parent_instance;
   private ButtonPrivate* priv;


   // Create a new button
   // RETURNS: a new #MxButton
   static Button* new_()() nothrow {
      return mx_button_new();
   }
   static auto opCall()() {
      return mx_button_new();
   }

   // Create a new #MxButton with the specified label
   // RETURNS: a new #MxButton
   // <text>: text to set the label to
   static Button* new_with_label(AT0)(AT0 /*char*/ text) nothrow {
      return mx_button_new_with_label(toCString!(char*)(text));
   }
   static auto opCall(AT0)(AT0 /*char*/ text) {
      return mx_button_new_with_label(toCString!(char*)(text));
   }

   // VERSION: 1.2
   // Retrieves the #MxAction associated with @button.
   // RETURNS: A #MxAction
   Action* get_action()() nothrow {
      return mx_button_get_action(&this);
   }

   // VERSION: 1.2
   // Get the icon-name being used on the button.
   // 
   // no icon has been set
   // RETURNS: the icon-name. This must not be freed by the application. %NULL if
   char* get_icon_name()() nothrow {
      return mx_button_get_icon_name(&this);
   }

   // VERSION: 1.2
   // Retrieves the icon's relative position to the text.
   // RETURNS: A #MxPosition
   Position get_icon_position()() nothrow {
      return mx_button_get_icon_position(&this);
   }

   // VERSION: 1.2
   // Retrieves the icon-size being used for the displayed icon inside the button.
   // RETURNS: The icon-size being used for the button icon, in pixels
   uint get_icon_size()() nothrow {
      return mx_button_get_icon_size(&this);
   }

   // VERSION: 1.2
   // Retrieves the visibility of the icon associated with the button's action.
   // RETURNS: %TRUE if the icon is visible, %FALSE otherwise
   int get_icon_visible()() nothrow {
      return mx_button_get_icon_visible(&this);
   }

   // Get the toggle mode status of the button.
   // RETURNS: #TRUE if toggle mode is set, otherwise #FALSE
   int get_is_toggle()() nothrow {
      return mx_button_get_is_toggle(&this);
   }

   // Get the text displayed on the button
   // RETURNS: the text for the button. This must not be freed by the application
   char* get_label()() nothrow {
      return mx_button_get_label(&this);
   }

   // VERSION: 1.2
   // Retrieves the visibility of the text associated with the button's action.
   // RETURNS: %TRUE if the text is visible, %FALSE otherwise
   int get_label_visible()() nothrow {
      return mx_button_get_label_visible(&this);
   }

   // Get the state of the button that is in toggle mode.
   // RETURNS: #TRUE if the button is toggled, or #FALSE if not
   int get_toggled()() nothrow {
      return mx_button_get_toggled(&this);
   }

   // VERSION: 1.2
   // Sets @action as the action for @button. @Button will take its label and
   // icon from @action.
   // <action>: A #MxAction
   void set_action(AT0)(AT0 /*Action*/ action) nothrow {
      mx_button_set_action(&this, UpCast!(Action*)(action));
   }

   // VERSION: 1.2
   // Sets the icon-name used to display an icon on the button. Setting %NULL
   // will remove the icon name, or resort to the icon-name set in the current
   // style. Setting an icon name overrides any icon set in the style.
   // <icon_name>: icon-name to use on the button
   void set_icon_name(AT0)(AT0 /*char*/ icon_name=null) nothrow {
      mx_button_set_icon_name(&this, toCString!(char*)(icon_name));
   }

   // VERSION: 1.2
   // Sets the icon position, relative to the text on the button.
   // <position>: A #MxPosition
   void set_icon_position()(Position position) nothrow {
      mx_button_set_icon_position(&this, position);
   }

   // VERSION: 1.2
   // Sets the icon-size to use for the icon displayed inside the button. This will
   // override the icon-size set in the style. Setting a value of %0 resets to the
   // size from the style.
   void set_icon_size()(uint icon_size) nothrow {
      mx_button_set_icon_size(&this, icon_size);
   }

   // VERSION: 1.2
   // Sets the visibility of the icon associated with the button's action.
   // <visible>: %TRUE if the icon should be visible
   void set_icon_visible()(int visible) nothrow {
      mx_button_set_icon_visible(&this, visible);
   }

   // Enables or disables toggle mode for the button. In toggle mode, the active
   // state will be "toggled" when the user clicks the button.
   // <toggle>: #TRUE or #FALSE
   void set_is_toggle()(int toggle) nothrow {
      mx_button_set_is_toggle(&this, toggle);
   }

   // Sets the text displayed on the button
   // <text>: text to set the label to
   void set_label(AT0)(AT0 /*char*/ text) nothrow {
      mx_button_set_label(&this, toCString!(char*)(text));
   }

   // VERSION: 1.2
   // Sets the visibility of the text associated with the button's action.
   // <visible>: %TRUE if the text should be visible
   void set_label_visible()(int visible) nothrow {
      mx_button_set_label_visible(&this, visible);
   }

   // Sets the toggled state of the button. This is only really useful if the
   // button has #toggle-mode mode set to #TRUE.
   // <toggled>: #TRUE or #FALSE
   void set_toggled()(int toggled) nothrow {
      mx_button_set_toggled(&this, toggled);
   }

   // Emitted when the user activates the button, either with a mouse press and
   // release or with the keyboard.
   extern (C) alias static void function (Button* this_, void* user_data=null) nothrow signal_clicked;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"clicked", CB/*:signal_clicked*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_clicked)||_ttmm!(CB, signal_clicked)()) {
      return signal_connect_data!()(&this, cast(char*)"clicked",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct ButtonClass {
   BinClass parent_class;
   extern (C) void function (Button* button) nothrow clicked;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct ButtonGroup /* : GObject.InitiallyUnowned */ {
   alias parent this;
   alias parent super_;
   alias parent initiallyunowned;
   GObject2.InitiallyUnowned parent;
   ButtonGroupPrivate* priv;


   // Create a new #MxButtonGroup.
   // RETURNS: a newly allocated #MxButtonGroup.
   static ButtonGroup* new_()() nothrow {
      return mx_button_group_new();
   }
   static auto opCall()() {
      return mx_button_group_new();
   }

   // Add @button to the #MxButtonGroup.
   // <button>: A #MxButton
   void add(AT0)(AT0 /*Button*/ button) nothrow {
      mx_button_group_add(&this, UpCast!(Button*)(button));
   }

   // Calls @callback for each button in the group.
   // <callback>: A #ClutterCallback
   // <userdata>: A #gpointer
   void foreach_(AT0)(Clutter.Callback callback, AT0 /*void*/ userdata) nothrow {
      mx_button_group_foreach(&this, callback, UpCast!(void*)(userdata));
   }

   // Get the current active button
   // RETURNS: the currently active button
   Button* get_active_button()() nothrow {
      return mx_button_group_get_active_button(&this);
   }

   // Get the value of the #MxButtonGroup:allow-no-active property.
   // RETURNS: the value of the "allow-no-active" property.
   int get_allow_no_active()() nothrow {
      return mx_button_group_get_allow_no_active(&this);
   }

   // Get a list of the buttons in the button group.
   // 
   // owned by the #MxButtonGroup and should not be modified by the
   // application.
   // RETURNS: a list of buttons. The list is
   GLib2.SList* get_buttons()() nothrow {
      return mx_button_group_get_buttons(&this);
   }

   // Remove @button from the #MxButtonGroup
   // <button>: A #MxButton
   void remove(AT0)(AT0 /*Button*/ button) nothrow {
      mx_button_group_remove(&this, UpCast!(Button*)(button));
   }

   // Set the current active button in the group. The previous active button will
   // have #MxButton:toggled set to #FALSE.
   // <button>: A #MxButton
   void set_active_button(AT0)(AT0 /*Button*/ button=null) nothrow {
      mx_button_group_set_active_button(&this, UpCast!(Button*)(button));
   }

   // Set the value of the #MxButtonGroup:allow-no-active property.
   // <allow_no_active>: A #gboolean
   void set_allow_no_active()(int allow_no_active) nothrow {
      mx_button_group_set_allow_no_active(&this, allow_no_active);
   }
}

struct ButtonGroupClass {
   GObject2.InitiallyUnownedClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct ButtonGroupPrivate {
}

struct ButtonPrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct Clipboard /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   private ClipboardPrivate* priv;


   // Get the global #MxClipboard object that represents the clipboard.
   // 
   // unrefferenced or freed.
   // RETURNS: a #MxClipboard owned by Mx and must not be
   static Clipboard* get_default()() nothrow {
      return mx_clipboard_get_default();
   }

   // Request the data from the clipboard in text form. @callback is executed
   // when the data is retreived.
   // <callback>: function to be called when the text is retreived
   // <user_data>: data to be passed to the callback
   void get_text(AT0)(ClipboardCallbackFunc callback, AT0 /*void*/ user_data) nothrow {
      mx_clipboard_get_text(&this, callback, UpCast!(void*)(user_data));
   }

   // Sets text as the current contents of the clipboard.
   // <text>: text to copy to the clipboard
   void set_text(AT0)(AT0 /*char*/ text) nothrow {
      mx_clipboard_set_text(&this, toCString!(char*)(text));
   }
}


// Callback function called when text is retrieved from the clipboard.
// <clipboard>: A #MxClipboard
// <text>: text from the clipboard
// <user_data>: user data
extern (C) alias void function (Clipboard* clipboard, char* text, void* user_data) nothrow ClipboardCallbackFunc;

struct ClipboardClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct ClipboardPrivate {
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct ComboBox /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   private ComboBoxPrivate* priv;


   // Create a new MxComboBox
   // RETURNS: a newly allocated MxComboBox
   static ComboBox* new_()() nothrow {
      return mx_combo_box_new();
   }
   static auto opCall()() {
      return mx_combo_box_new();
   }

   // Append an item to the combo box list
   // <text>: name of the item
   void append_text(AT0)(AT0 /*char*/ text) nothrow {
      mx_combo_box_append_text(&this, toCString!(char*)(text));
   }

   // Get the name of the icon displayed in the combo box
   // 
   // the combo box, or %NULL if there is no active icon.
   // RETURNS: the text string of the name of the displayed icon, owned by
   char* get_active_icon_name()() nothrow {
      return mx_combo_box_get_active_icon_name(&this);
   }

   // Get the text displayed in the combo box
   // RETURNS: the text string, owned by the combo box
   char* get_active_text()() nothrow {
      return mx_combo_box_get_active_text(&this);
   }

   // Get the index of the last item selected
   // RETURNS: gint
   int get_index()() nothrow {
      return mx_combo_box_get_index(&this);
   }

   // Insert an item into the combo box list.
   // <position>: zero indexed position to insert the item at
   // <text>: name of the item
   void insert_text(AT0)(int position, AT0 /*char*/ text) nothrow {
      mx_combo_box_insert_text(&this, position, toCString!(char*)(text));
   }

   // Insert an item with text and an icon into the combo box list.
   // <position>: zero indexed position to insert the item at
   // <text>: name of the item
   // <icon>: name of an icon from the icon theme
   void insert_text_with_icon(AT0, AT1)(int position, AT0 /*char*/ text, AT1 /*char*/ icon) nothrow {
      mx_combo_box_insert_text_with_icon(&this, position, toCString!(char*)(text), toCString!(char*)(icon));
   }

   // Prepend an item to the combo box list
   // <text>: name of the item
   void prepend_text(AT0)(AT0 /*char*/ text) nothrow {
      mx_combo_box_prepend_text(&this, toCString!(char*)(text));
   }

   // VERSION: 1.4
   // Remove all the items of @box
   void remove_all()() nothrow {
      mx_combo_box_remove_all(&this);
   }

   // Remove the item at @position
   // <position>: position of the item to remove
   void remove_text()(int position) nothrow {
      mx_combo_box_remove_text(&this, position);
   }

   // Set the icon displayed in the combo box.
   // <icon_name>: Icon name to use for displayed icon
   void set_active_icon_name(AT0)(AT0 /*char*/ icon_name=null) nothrow {
      mx_combo_box_set_active_icon_name(&this, toCString!(char*)(icon_name));
   }

   // Set the text displayed in the combo box
   // <text>: text to display
   void set_active_text(AT0)(AT0 /*char*/ text) nothrow {
      mx_combo_box_set_active_text(&this, toCString!(char*)(text));
   }

   // Set the current combo box text from the item at @index in the list.
   // <index>: the index of the list item to set
   void set_index()(int index) nothrow {
      mx_combo_box_set_index(&this, index);
   }
}

struct ComboBoxClass {
   WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct ComboBoxPrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct DeformBowTie /* : DeformTexture */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent deformtexture;
   DeformTexture parent;
   DeformBowTiePrivate* priv;

   static DeformBowTie* new_()() nothrow {
      return mx_deform_bow_tie_new();
   }
   static auto opCall()() {
      return mx_deform_bow_tie_new();
   }
   int get_flip_back()() nothrow {
      return mx_deform_bow_tie_get_flip_back(&this);
   }
   double get_period()() nothrow {
      return mx_deform_bow_tie_get_period(&this);
   }
   void set_flip_back()(int flip_back) nothrow {
      mx_deform_bow_tie_set_flip_back(&this, flip_back);
   }
   void set_period()(double period) nothrow {
      mx_deform_bow_tie_set_period(&this, period);
   }
}

struct DeformBowTieClass {
   DeformTextureClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct DeformBowTiePrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct DeformPageTurn /* : DeformTexture */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent deformtexture;
   DeformTexture parent;
   DeformPageTurnPrivate* priv;

   static DeformPageTurn* new_()() nothrow {
      return mx_deform_page_turn_new();
   }
   static auto opCall()() {
      return mx_deform_page_turn_new();
   }
   double get_angle()() nothrow {
      return mx_deform_page_turn_get_angle(&this);
   }
   double get_period()() nothrow {
      return mx_deform_page_turn_get_period(&this);
   }
   double get_radius()() nothrow {
      return mx_deform_page_turn_get_radius(&this);
   }
   void set_angle()(double angle) nothrow {
      mx_deform_page_turn_set_angle(&this, angle);
   }
   void set_period()(double period) nothrow {
      mx_deform_page_turn_set_period(&this, period);
   }
   void set_radius()(double radius) nothrow {
      mx_deform_page_turn_set_radius(&this, radius);
   }
}

struct DeformPageTurnClass {
   DeformTextureClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct DeformPageTurnPrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct DeformTexture /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   DeformTexturePrivate* priv;


   // Retrieve the mesh resolution of the texture.
   // See mx_deform_texture_set_resolution().
   // <tiles_x>: The horizontal resolution
   // <tiles_y>: The vertical resolution
   void get_resolution()(/*out*/ int* tiles_x=null, /*out*/ int* tiles_y=null) nothrow {
      mx_deform_texture_get_resolution(&this, tiles_x, tiles_y);
   }

   // Retrieves the textures used by @texture.
   // <front>: The front-facing texture
   // <back>: The back-facing texture
   void get_textures(AT0, AT1)(/*out*/ AT0 /*Clutter.Texture**/ front=null, /*out*/ AT1 /*Clutter.Texture**/ back=null) nothrow {
      mx_deform_texture_get_textures(&this, UpCast!(Clutter.Texture**)(front), UpCast!(Clutter.Texture**)(back));
   }
   // Make @texture re-calculate its vertices and redraw itself.
   void invalidate()() nothrow {
      mx_deform_texture_invalidate(&this);
   }

   // Sets the amount of sub-divisions used on each axis when generating
   // the mesh, where a value of 1 for each axis will produce a single quad.
   // <tiles_x>: The horizontal resolution
   // <tiles_y>: The vertical resolution
   void set_resolution()(int tiles_x, int tiles_y) nothrow {
      mx_deform_texture_set_resolution(&this, tiles_x, tiles_y);
   }

   // Set textures to use as the sources of a deformation effect. Textures
   // must not be parented.
   // <front>: #ClutterTexture to use for the front-face.
   // <back>: #ClutterTexture to use for the back-face.
   void set_textures(AT0, AT1)(AT0 /*Clutter.Texture*/ front=null, AT1 /*Clutter.Texture*/ back=null) nothrow {
      mx_deform_texture_set_textures(&this, UpCast!(Clutter.Texture*)(front), UpCast!(Clutter.Texture*)(back));
   }
}

struct DeformTextureClass {
   WidgetClass parent_class;
   extern (C) void function (DeformTexture* texture, Cogl.TextureVertex* vertex, float width, float height) nothrow deform;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct DeformTexturePrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct DeformWaves /* : DeformTexture */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent deformtexture;
   DeformTexture parent;
   DeformWavesPrivate* priv;

   static DeformWaves* new_()() nothrow {
      return mx_deform_waves_new();
   }
   static auto opCall()() {
      return mx_deform_waves_new();
   }
   double get_amplitude()() nothrow {
      return mx_deform_waves_get_amplitude(&this);
   }
   double get_angle()() nothrow {
      return mx_deform_waves_get_angle(&this);
   }
   double get_period()() nothrow {
      return mx_deform_waves_get_period(&this);
   }
   double get_radius()() nothrow {
      return mx_deform_waves_get_radius(&this);
   }
   void set_amplitude()(double amplitude) nothrow {
      mx_deform_waves_set_amplitude(&this, amplitude);
   }
   void set_angle()(double angle) nothrow {
      mx_deform_waves_set_angle(&this, angle);
   }
   void set_period()(double period) nothrow {
      mx_deform_waves_set_period(&this, period);
   }
   void set_radius()(double radius) nothrow {
      mx_deform_waves_set_radius(&this, radius);
   }
}

struct DeformWavesClass {
   DeformTextureClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct DeformWavesPrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct Dialog /* : Bin */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent bin;
   Bin parent;
   private DialogPrivate* priv;


   // VERSION: 1.2
   // Creates a new #MxDialog.
   // RETURNS: A newly allocated #MxDialog
   static Dialog* new_()() nothrow {
      return mx_dialog_new();
   }
   static auto opCall()() {
      return mx_dialog_new();
   }

   // VERSION: 1.2
   // Adds an #MxButton that represents @action to the button area of @dialog
   // <action>: A #MxAction
   void add_action(AT0)(AT0 /*Action*/ action) nothrow {
      mx_dialog_add_action(&this, UpCast!(Action*)(action));
   }

   // VERSION: 1.2
   // Retrieves a list of actions added to @dialog.
   // 
   // #GList of #MxAction objects. The actions in the list are owned by the
   // dialog.
   // RETURNS: A newly allocated
   GLib2.List* /*new container*/ get_actions()() nothrow {
      return mx_dialog_get_actions(&this);
   }

   // VERSION: 1.2
   // Removes the button associated with @action from the button area of @dialog
   // <action>: A #MxAction
   void remove_action(AT0)(AT0 /*Action*/ action) nothrow {
      mx_dialog_remove_action(&this, UpCast!(Action*)(action));
   }

   // VERSION: 1.2
   // Sets the parent of the #MxDialog. This is the actor over which the
   // modal frame will appear when clutter_actor_show() is called.
   // <actor>: A #ClutterActor
   void set_transient_parent(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
      mx_dialog_set_transient_parent(&this, UpCast!(Clutter.Actor*)(actor));
   }
}

struct DialogClass {
   BinClass parent_class;
}

struct DialogPrivate {
}

enum DragAxis {
   NONE = 0,
   X = 1,
   Y = 2
}
// This is an opaque structure whose members cannot be directly accessed.
struct Draggable /* Interface */ {
   mixin template __interface__() {      void disable()() nothrow {
         mx_draggable_disable(cast(Draggable*)&this);
      }
      void enable()() nothrow {
         mx_draggable_enable(cast(Draggable*)&this);
      }
      DragAxis get_axis()() nothrow {
         return mx_draggable_get_axis(cast(Draggable*)&this);
      }

      // FIXME
      // RETURNS: a #ClutterActor, or %NULL
      Clutter.Actor* get_drag_actor()() nothrow {
         return mx_draggable_get_drag_actor(cast(Draggable*)&this);
      }
      uint get_drag_threshold()() nothrow {
         return mx_draggable_get_drag_threshold(cast(Draggable*)&this);
      }
      int is_enabled()() nothrow {
         return mx_draggable_is_enabled(cast(Draggable*)&this);
      }
      void set_axis()(DragAxis axis) nothrow {
         mx_draggable_set_axis(cast(Draggable*)&this, axis);
      }
      void set_drag_actor(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
         mx_draggable_set_drag_actor(cast(Draggable*)&this, UpCast!(Clutter.Actor*)(actor));
      }
      void set_drag_threshold()(uint threshold) nothrow {
         mx_draggable_set_drag_threshold(cast(Draggable*)&this, threshold);
      }
      extern (C) alias static void function (Draggable* this_, float object, float p0, int p1, Clutter.ModifierType* p2, void* user_data=null) nothrow signal_drag_begin;

      ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
         return super_.signal_connect!name(cb, data, cf);
      }

      ulong signal_connect(string name:"drag-begin", CB/*:signal_drag_begin*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_drag_begin)||_ttmm!(CB, signal_drag_begin)()) {
         return signal_connect_data!()(&this, cast(char*)"drag-begin",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Draggable* this_, float object, float p0, void* user_data=null) nothrow signal_drag_end;
      ulong signal_connect(string name:"drag-end", CB/*:signal_drag_end*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_drag_end)||_ttmm!(CB, signal_drag_end)()) {
         return signal_connect_data!()(&this, cast(char*)"drag-end",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Draggable* this_, float object, float p0, void* user_data=null) nothrow signal_drag_motion;
      ulong signal_connect(string name:"drag-motion", CB/*:signal_drag_motion*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_drag_motion)||_ttmm!(CB, signal_drag_motion)()) {
         return signal_connect_data!()(&this, cast(char*)"drag-motion",
         cast(GObject2.Callback)cb, data, null, cf);
      }
   }
   mixin __interface__;
}

// Interface for draggable #ClutterActor<!-- -->s.
struct DraggableIface {
   private GObject2.TypeInterface g_iface;
   extern (C) void function (Draggable* draggable) nothrow enable;
   extern (C) void function (Draggable* draggable) nothrow disable;
   extern (C) void function (Draggable* draggable, float event_x, float event_y, int event_button, Clutter.ModifierType modifiers) nothrow drag_begin;
   extern (C) void function (Draggable* draggable, float delta_x, float delta_y) nothrow drag_motion;
   extern (C) void function (Draggable* draggable, float event_x, float event_y) nothrow drag_end;
}

// This is an opaque structure whose members cannot be directly accessed.
struct Droppable /* Interface */ {
   mixin template __interface__() {      int accept_drop(AT0)(AT0 /*Draggable*/ draggable) nothrow {
         return mx_droppable_accept_drop(cast(Droppable*)&this, UpCast!(Draggable*)(draggable));
      }
      void disable()() nothrow {
         mx_droppable_disable(cast(Droppable*)&this);
      }
      void enable()() nothrow {
         mx_droppable_enable(cast(Droppable*)&this);
      }
      int is_enabled()() nothrow {
         return mx_droppable_is_enabled(cast(Droppable*)&this);
      }
      extern (C) alias static void function (Droppable* this_, Clutter.Actor* object, float p0, float p1, int p2, Clutter.ModifierType* p3, void* user_data=null) nothrow signal_drop;

      ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
         return super_.signal_connect!name(cb, data, cf);
      }

      ulong signal_connect(string name:"drop", CB/*:signal_drop*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_drop)||_ttmm!(CB, signal_drop)()) {
         return signal_connect_data!()(&this, cast(char*)"drop",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Droppable* this_, Clutter.Actor* object, void* user_data=null) nothrow signal_over_in;
      ulong signal_connect(string name:"over-in", CB/*:signal_over_in*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_over_in)||_ttmm!(CB, signal_over_in)()) {
         return signal_connect_data!()(&this, cast(char*)"over-in",
         cast(GObject2.Callback)cb, data, null, cf);
      }
      extern (C) alias static void function (Droppable* this_, Clutter.Actor* object, void* user_data=null) nothrow signal_over_out;
      ulong signal_connect(string name:"over-out", CB/*:signal_over_out*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_over_out)||_ttmm!(CB, signal_over_out)()) {
         return signal_connect_data!()(&this, cast(char*)"over-out",
         cast(GObject2.Callback)cb, data, null, cf);
      }
   }
   mixin __interface__;
}

struct DroppableIface {
   private GObject2.TypeInterface g_iface;
   extern (C) void function (Droppable* droppable) nothrow enable;
   extern (C) void function (Droppable* droppable) nothrow disable;
   extern (C) int function (Droppable* droppable, Draggable* draggable) nothrow accept_drop;
   extern (C) void function (Droppable* droppable, Draggable* draggable) nothrow over_in;
   extern (C) void function (Droppable* droppable, Draggable* draggable) nothrow over_out;
   extern (C) void function (Droppable* droppable, Draggable* draggable, float event_x, float event_y, int button, Clutter.ModifierType modifiers) nothrow drop;
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct Entry /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance widget;
   Widget parent_instance;
   private EntryPrivate* priv;


   // Create a new #MxEntry
   // RETURNS: a new #MxEntry
   static Entry* new_()() nothrow {
      return mx_entry_new();
   }
   static auto opCall()() {
      return mx_entry_new();
   }

   // Create a new #MxEntry with the specified entry
   // RETURNS: a new #MxEntry
   // <text>: text to set the entry to
   static Entry* new_with_text(AT0)(AT0 /*char*/ text) nothrow {
      return mx_entry_new_with_text(toCString!(char*)(text));
   }
   static auto opCall(AT0)(AT0 /*char*/ text) {
      return mx_entry_new_with_text(toCString!(char*)(text));
   }

   // Retrieve the internal #ClutterText so that extra parameters can be set
   // 
   // owned by the #MxEntry and should not be unref'ed by the application.
   // RETURNS: the #ClutterText used by #MxEntry. The entry is
   Clutter.Actor* get_clutter_text()() nothrow {
      return mx_entry_get_clutter_text(&this);
   }

   // Gets the text that is displayed when the entry is empty and unfocused
   // 
   // #MxEntry and should not be freed or modified.
   // RETURNS: the current value of the hint property. This string is owned by the
   char* get_hint_text()() nothrow {
      return mx_entry_get_hint_text(&this);
   }

   // Get the suffix appended to the filename to use for the highlighted version
   // of the icon.
   // 
   // #MxEntry and should not be freed or modified.
   // RETURNS: the highlight filename suffix. This string is owned by the
   char* get_icon_highlight_suffix()() nothrow {
      return mx_entry_get_icon_highlight_suffix(&this);
   }

   // Gets the character to display instead of the text.
   // RETURNS: a character, or 0 if input should not be hidden.
   dchar get_password_char()() nothrow {
      return mx_entry_get_password_char(&this);
   }

   // Get the text displayed on the entry
   // RETURNS: the text for the entry. This must not be freed by the application
   char* get_text()() nothrow {
      return mx_entry_get_text(&this);
   }

   // Sets the text to display when the entry is empty and unfocused. When the
   // entry is displaying the hint, it has a pseudo class of "indeterminate".
   // A value of NULL unsets the hint.
   // <text>: text to set as the entry hint
   void set_hint_text(AT0)(AT0 /*char*/ text) nothrow {
      mx_entry_set_hint_text(&this, toCString!(char*)(text));
   }

   // Sets the suffix appended to the filename to use for the highlighted version
   // of the icon. e.g. if you have set your primay icon to "primary-icon.png"
   // and the suffix to "-highlight" #MxEntry will look for
   // "primary-icon-highlight.png"
   // <suffix>: the suffix to append to the filename for the highlight version
   void set_icon_highlight_suffix(AT0)(AT0 /*char*/ suffix) nothrow {
      mx_entry_set_icon_highlight_suffix(&this, toCString!(char*)(suffix));
   }

   // Sets the character to display instead of the text. Use 0 to display
   // the actual text.
   // <password_char>: text to set as the entry hint
   void set_password_char()(dchar password_char) nothrow {
      mx_entry_set_password_char(&this, password_char);
   }

   // Set the primary icon of the entry to the given filename
   // <filename>: filename of an icon
   void set_primary_icon_from_file(AT0)(AT0 /*char*/ filename) nothrow {
      mx_entry_set_primary_icon_from_file(&this, toCString!(char*)(filename));
   }
   void set_primary_icon_tooltip_text(AT0)(AT0 /*char*/ text) nothrow {
      mx_entry_set_primary_icon_tooltip_text(&this, toCString!(char*)(text));
   }

   // Set the primary icon of the entry to the given filename
   // <filename>: filename of an icon
   void set_secondary_icon_from_file(AT0)(AT0 /*char*/ filename) nothrow {
      mx_entry_set_secondary_icon_from_file(&this, toCString!(char*)(filename));
   }
   void set_secondary_icon_tooltip_text(AT0)(AT0 /*char*/ text) nothrow {
      mx_entry_set_secondary_icon_tooltip_text(&this, toCString!(char*)(text));
   }

   // Sets the text displayed on the entry
   // <text>: text to set the entry to
   void set_text(AT0)(AT0 /*char*/ text) nothrow {
      mx_entry_set_text(&this, toCString!(char*)(text));
   }
   // Emitted when the primary icon is clicked
   extern (C) alias static void function (Entry* this_, void* user_data=null) nothrow signal_primary_icon_clicked;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"primary-icon-clicked", CB/*:signal_primary_icon_clicked*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_primary_icon_clicked)||_ttmm!(CB, signal_primary_icon_clicked)()) {
      return signal_connect_data!()(&this, cast(char*)"primary-icon-clicked",
      cast(GObject2.Callback)cb, data, null, cf);
   }
   // Emitted when the secondary icon is clicked
   extern (C) alias static void function (Entry* this_, void* user_data=null) nothrow signal_secondary_icon_clicked;
   ulong signal_connect(string name:"secondary-icon-clicked", CB/*:signal_secondary_icon_clicked*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_secondary_icon_clicked)||_ttmm!(CB, signal_secondary_icon_clicked)()) {
      return signal_connect_data!()(&this, cast(char*)"secondary-icon-clicked",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct EntryClass {
   WidgetClass parent_class;
   extern (C) void function (Entry* entry) nothrow primary_icon_clicked;
   extern (C) void function (Entry* entry) nothrow secondary_icon_clicked;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct EntryPrivate {
}


// The contents of the this structure are private and should only be accessed
// through the public API.
struct Expander /* : Bin */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent bin;
   Bin parent;
   private ExpanderPrivate* priv;


   // Creates a new #MxExpander
   // RETURNS: the newly allocated #MxExpander
   static Expander* new_()() nothrow {
      return mx_expander_new();
   }
   static auto opCall()() {
      return mx_expander_new();
   }

   // Get the current state of the expander (the value of #MxExpander:expanded)
   // RETURNS: #TRUE if the expander is open, #FALSE if it is closed
   int get_expanded()() nothrow {
      return mx_expander_get_expanded(&this);
   }

   // Set the state (the #MxExpander:expanded property) of the expander.
   // This will cause the expander to open or close.
   // <expanded>: the state of the expander to set
   void set_expanded()(int expanded) nothrow {
      mx_expander_set_expanded(&this, expanded);
   }

   // Sets the text displayed as the title of the expander
   // <label>: string to set as the expander label
   void set_label(AT0)(AT0 /*char*/ label) nothrow {
      mx_expander_set_label(&this, toCString!(char*)(label));
   }

   // Emitted after the expand animation finishes. Check the "expanded" property
   // of the #MxExpander to determine if the expander is expanded or not.
   extern (C) alias static void function (Expander* this_, void* user_data=null) nothrow signal_expand_complete;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"expand-complete", CB/*:signal_expand_complete*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_expand_complete)||_ttmm!(CB, signal_expand_complete)()) {
      return signal_connect_data!()(&this, cast(char*)"expand-complete",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct ExpanderClass {
   BinClass parent_class;
   extern (C) void function (Expander* expander) nothrow expand_complete;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct ExpanderPrivate {
}

struct FadeEffect /* : Clutter.OffscreenEffect */ {
   alias parent this;
   alias parent super_;
   alias parent offscreeneffect;
   Clutter.OffscreenEffect parent;
   FadeEffectPrivate* priv;


   // VERSION: 1.2
   // Creates a new #MxFadeEffect to be used with clutter_actor_add_effect().
   // RETURNS: the newly created #MxFadeEffect, or %NULL
   static FadeEffect* new_()() nothrow {
      return mx_fade_effect_new();
   }
   static auto opCall()() {
      return mx_fade_effect_new();
   }

   // VERSION: 1.2
   // Retrieves the border values for @effect.
   // <top>: The upper border, in pixels
   // <right>: The right border, in pixels
   // <bottom>: The lower border, in pixels
   // <left>: The left border, in pixels
   void get_border(AT0, AT1, AT2, AT3)(/*out*/ AT0 /*uint*/ top, /*out*/ AT1 /*uint*/ right, /*out*/ AT2 /*uint*/ bottom, /*out*/ AT3 /*uint*/ left) nothrow {
      mx_fade_effect_get_border(&this, UpCast!(uint*)(top), UpCast!(uint*)(right), UpCast!(uint*)(bottom), UpCast!(uint*)(left));
   }

   // VERSION: 1.2
   // Retrieves the bounding box of the effect.
   // <x>: The x value of the effect bounds, in pixels
   // <y>: The y value of the effect bounds, in pixels
   // <width>: The width of the effect bounds, in pixels, or %0
   // <height>: The height of the effect bounds, in pixels, or %0
   void get_bounds(AT0, AT1)(/*out*/ int* x, /*out*/ int* y, /*out*/ AT0 /*uint*/ width, /*out*/ AT1 /*uint*/ height) nothrow {
      mx_fade_effect_get_bounds(&this, x, y, UpCast!(uint*)(width), UpCast!(uint*)(height));
   }

   // VERSION: 1.2
   // Retrieves the color used for the fade effect.
   // <color>: A #ClutterColor to store the color in
   void get_color(AT0)(/*out*/ AT0 /*Clutter.Color*/ color) nothrow {
      mx_fade_effect_get_color(&this, UpCast!(Clutter.Color*)(color));
   }

   // VERSION: 1.2
   // Sets the border to be used for the fading effect. This is the number of
   // pixels on each side of the effect that should be used to fade.
   // <top>: The upper border, in pixels
   // <right>: The right border, in pixels
   // <bottom>: The lower border, in pixels
   // <left>: The left border, in pixels
   void set_border()(uint top, uint right, uint bottom, uint left) nothrow {
      mx_fade_effect_set_border(&this, top, right, bottom, left);
   }

   // VERSION: 1.2
   // Sets the bounding box of the effect. The effect will essentially treat
   // this box as a clipping rectangle. Setting width or height to %0 will
   // use the width or height of the #ClutterActor the effect is attached to.
   // 
   // <note><para>
   // The effect border will apply to the bounds, and not to the un-altered
   // rectangle, so an effect with an %x of %5 and a %left-border of %5 will
   // have a gap of 5 blank pixels to the left, with a fade length of 5 pixels.
   // </para></note>
   // <x>: The x value of the effect bounds, in pixels
   // <y>: The y value of the effect bounds, in pixels
   // <width>: The width of the effect bounds, in pixels, or %0
   // <height>: The height of the effect bounds, in pixels, or %0
   void set_bounds()(int x, int y, uint width, uint height) nothrow {
      mx_fade_effect_set_bounds(&this, x, y, width, height);
   }

   // VERSION: 1.2
   // Sets the color of the fade effect. The effect will fade out towards
   // the set border to this color.
   // <color>: A #ClutterColor
   void set_color(AT0)(AT0 /*Clutter.Color*/ color) nothrow {
      mx_fade_effect_set_color(&this, UpCast!(Clutter.Color*)(color));
   }
}

struct FadeEffectClass {
   Clutter.OffscreenEffectClass parent_class;
}

struct FadeEffectPrivate {
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct FloatingWidget /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   FloatingWidgetPrivate* priv;
}

struct FloatingWidgetClass {
   WidgetClass parent_class;
   extern (C) void function (Clutter.Actor* actor, Clutter.Color* color) nothrow floating_pick;
   extern (C) void function (Clutter.Actor* actor) nothrow floating_paint;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct FloatingWidgetPrivate {
}

enum FocusDirection {
   OUT = 0,
   UP = 1,
   DOWN = 2,
   LEFT = 3,
   RIGHT = 4,
   NEXT = 5,
   PREVIOUS = 6
}
enum FocusHint {
   FIRST = 0,
   LAST = 1,
   PRIOR = 2,
   FROM_ABOVE = 3,
   FROM_BELOW = 4,
   FROM_LEFT = 5,
   FROM_RIGHT = 6
}

// The contents of this structure are private and should only be accessed
// through the public API.
struct FocusManager /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   FocusManagerPrivate* priv;


   // Get the MxFocusManager associated with a stage, or create one if none exist
   // for the specified stage.
   // RETURNS: An #MxFocusManager
   // <stage>: A #ClutterStage
   static FocusManager* get_for_stage(AT0)(AT0 /*Clutter.Stage*/ stage) nothrow {
      return mx_focus_manager_get_for_stage(UpCast!(Clutter.Stage*)(stage));
   }

   // Get the currently focused #MxFocusable
   // RETURNS: MxFocusable
   Focusable* get_focused()() nothrow {
      return mx_focus_manager_get_focused(&this);
   }

   // Get the stage the MxFocusManager is associated with
   // RETURNS: A #ClutterStage
   Clutter.Stage* get_stage()() nothrow {
      return mx_focus_manager_get_stage(&this);
   }

   // Moves the current focus in the given direction.
   // <direction>: The direction to move focus in
   void move_focus()(FocusDirection direction) nothrow {
      mx_focus_manager_move_focus(&this, direction);
   }

   // Sets the currently focused actor, with an #MxFocusHint of
   // %MX_FOCUS_HINT_PRIOR.
   // 
   // Note: the final focused object may not be the same as @focusable if
   // @focusable does not accept focus directly.
   // <focusable>: the object to set focus on
   void push_focus(AT0)(AT0 /*Focusable*/ focusable) nothrow {
      mx_focus_manager_push_focus(&this, UpCast!(Focusable*)(focusable));
   }

   // VERSION: 1.2
   // Similar to #mx_focus_manager_push_focus, but allows the hint to be specified.
   // 
   // Note: the final focused object may not be the same as @focusable if
   // @focusable does not accept focus directly.
   // <focusable>: the object to set focus on
   // <hint>: an #MxFocusHint
   void push_focus_with_hint(AT0)(AT0 /*Focusable*/ focusable, FocusHint hint) nothrow {
      mx_focus_manager_push_focus_with_hint(&this, UpCast!(Focusable*)(focusable), hint);
   }
}

struct FocusManagerClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct FocusManagerPrivate {
}

// This is an opaque structure whose members cannot be directly accessed.
struct Focusable /* Interface */ {
   mixin template __interface__() {
      // Accept the focus
      // RETURNS: the focusable
      // <hint>: A #MxFocusHint
      Focusable* accept_focus()(FocusHint hint) nothrow {
         return mx_focusable_accept_focus(cast(Focusable*)&this, hint);
      }

      // Move the focus
      // RETURNS: the newly focused focusable
      // <direction>: A #MxFocusDirection
      // <from>: focusable to move the focus from
      Focusable* move_focus(AT0)(FocusDirection direction, AT0 /*Focusable*/ from) nothrow {
         return mx_focusable_move_focus(cast(Focusable*)&this, direction, UpCast!(Focusable*)(from));
      }
   }
   mixin __interface__;
}

struct FocusableIface {
   GObject2.ObjectClass parent_class;

   // RETURNS: the focusable
   // <hint>: A #MxFocusHint
   extern (C) Focusable* function (Focusable* focusable, FocusHint hint) nothrow accept_focus;

   // RETURNS: the newly focused focusable
   // <direction>: A #MxFocusDirection
   // <from>: focusable to move the focus from
   extern (C) Focusable* function (Focusable* focusable, FocusDirection direction, Focusable* from) nothrow move_focus;
}

// Support values of font weight
enum FontWeight {
   NORMAL = 0,
   BOLD = 1,
   BOLDER = 2,
   LIGHTER = 3
}

// The contents of this structure are private and should only be accessed
// through the public API.
struct Frame /* : Bin */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent bin;
   Bin parent;
   FramePrivate* priv;


   // Create a new #MxFrame
   // RETURNS: a newly allocated #MxFrame
   static Frame* new_()() nothrow {
      return mx_frame_new();
   }
   static auto opCall()() {
      return mx_frame_new();
   }
}

struct FrameClass {
   BinClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct FramePrivate {
}


// The contents of the this structure are private and should only be accessed
// through the public API.
struct Grid /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Scrollable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   private GridPrivate* priv;

   static Grid* new_()() nothrow {
      return mx_grid_new();
   }
   static auto opCall()() {
      return mx_grid_new();
   }
   Align get_child_x_align()() nothrow {
      return mx_grid_get_child_x_align(&this);
   }
   Align get_child_y_align()() nothrow {
      return mx_grid_get_child_y_align(&this);
   }
   float get_column_spacing()() nothrow {
      return mx_grid_get_column_spacing(&this);
   }
   int get_homogenous_columns()() nothrow {
      return mx_grid_get_homogenous_columns(&this);
   }
   int get_homogenous_rows()() nothrow {
      return mx_grid_get_homogenous_rows(&this);
   }
   int get_line_alignment()() nothrow {
      return mx_grid_get_line_alignment(&this);
   }
   int get_max_stride()() nothrow {
      return mx_grid_get_max_stride(&this);
   }
   Orientation get_orientation()() nothrow {
      return mx_grid_get_orientation(&this);
   }
   float get_row_spacing()() nothrow {
      return mx_grid_get_row_spacing(&this);
   }
   void set_child_x_align()(Align value) nothrow {
      mx_grid_set_child_x_align(&this, value);
   }
   void set_child_y_align()(Align value) nothrow {
      mx_grid_set_child_y_align(&this, value);
   }
   void set_column_spacing()(float value) nothrow {
      mx_grid_set_column_spacing(&this, value);
   }
   void set_homogenous_columns()(int value) nothrow {
      mx_grid_set_homogenous_columns(&this, value);
   }
   void set_homogenous_rows()(int value) nothrow {
      mx_grid_set_homogenous_rows(&this, value);
   }
   void set_line_alignment()(Align value) nothrow {
      mx_grid_set_line_alignment(&this, value);
   }
   void set_max_stride()(int value) nothrow {
      mx_grid_set_max_stride(&this, value);
   }
   void set_orientation()(Orientation orientation) nothrow {
      mx_grid_set_orientation(&this, orientation);
   }
   void set_row_spacing()(float value) nothrow {
      mx_grid_set_row_spacing(&this, value);
   }
}

struct GridClass {
   WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct GridPrivate {
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct Icon /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   private IconPrivate* priv;


   // Create a newly allocated #MxIcon
   // RETURNS: A newly allocated #MxIcon
   static Icon* new_()() nothrow {
      return mx_icon_new();
   }
   static auto opCall()() {
      return mx_icon_new();
   }
   char* get_icon_name()() nothrow {
      return mx_icon_get_icon_name(&this);
   }
   int get_icon_size()() nothrow {
      return mx_icon_get_icon_size(&this);
   }
   void set_icon_name(AT0)(AT0 /*char*/ icon_name) nothrow {
      mx_icon_set_icon_name(&this, toCString!(char*)(icon_name));
   }
   void set_icon_size()(int size) nothrow {
      mx_icon_set_icon_size(&this, size);
   }
}

struct IconClass {
   WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct IconPrivate {
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct IconTheme /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   IconThemePrivate* priv;


   // Create a new #MxIconTheme. In most cicumstances, it is more useful to use
   // #mx_icon_theme_get_default to load the default icon theme.
   // RETURNS: a newly allocated #MxIconTheme.
   static IconTheme* /*new*/ new_()() nothrow {
      return mx_icon_theme_new();
   }
   static auto opCall()() {
      return mx_icon_theme_new();
   }

   // Return the default #MxIconTheme object used by the toolkit.
   // RETURNS: an #MxIconTheme.
   static IconTheme* get_default()() nothrow {
      return mx_icon_theme_get_default();
   }

   // Gets the directories the #MxIconTheme will search in to find icons.
   // RETURNS: the search paths
   GLib2.List* get_search_paths()() nothrow {
      return mx_icon_theme_get_search_paths(&this);
   }

   // Get the value of the #MxIconTheme:theme-name property.
   // RETURNS: the current value of the "theme-name" property.
   char* get_theme_name()() nothrow {
      return mx_icon_theme_get_theme_name(&this);
   }
   int has_icon(AT0)(AT0 /*char*/ icon_name) nothrow {
      return mx_icon_theme_has_icon(&this, toCString!(char*)(icon_name));
   }

   // If the icon is available, returns a #CoglHandle of the icon.
   // RETURNS: a #CoglHandle of the icon, or %NULL.
   // <icon_name>: The name of the icon
   // <size>: The desired size of the icon
   Cogl.Handle lookup(AT0)(AT0 /*char*/ icon_name, int size) nothrow {
      return mx_icon_theme_lookup(&this, toCString!(char*)(icon_name), size);
   }

   // If the icon is available, returns a #ClutterTexture of the icon.
   // RETURNS: a #ClutterTexture of the icon, or %NULL.
   // <icon_name>: The name of the icon
   // <size>: The desired size of the icon
   Clutter.Texture* lookup_texture(AT0)(AT0 /*char*/ icon_name, int size) nothrow {
      return mx_icon_theme_lookup_texture(&this, toCString!(char*)(icon_name), size);
   }

   // Sets the directories the #MxIconTheme will search in to find icons.
   // By default, it will look in the default system and local icon
   // directories.
   // <paths>: a list of search paths
   void set_search_paths(AT0)(AT0 /*GLib2.List*/ paths) nothrow {
      mx_icon_theme_set_search_paths(&this, UpCast!(GLib2.List*)(paths));
   }

   // Set the value of the #MxIconTheme:theme-name property. This will cause the
   // icon theme to be loaded if it differs from the existing theme name. If the
   // theme could not be loaded, it will fall back to using the default icon theme
   // (hicolor).
   // 
   // This will override the system's theme setting. To revert to the system
   // icon theme, this function can be called with a %NULL @theme_name argument.
   // <theme_name>: the name of an icon theme to load, or %NULL
   void set_theme_name(AT0)(AT0 /*char*/ theme_name) nothrow {
      mx_icon_theme_set_theme_name(&this, toCString!(char*)(theme_name));
   }
}

struct IconThemeClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct IconThemePrivate {
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct Image /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   private ImagePrivate* priv;


   // VERSION: 1.2
   // Creates a new #MxImage object.
   // RETURNS: A newly created #MxImage object
   static Image* new_()() nothrow {
      return mx_image_new();
   }
   static auto opCall()() {
      return mx_image_new();
   }

   // VERSION: 1.2
   // Sets the value of #MxImage:scale-mode to @scale_mode and animates the
   // scale factor of the image between the previous value and the new value.
   // <mode>: a #ClutterAnimationMode
   // <duration>: duration of the animation in milliseconds
   // <scale_mode>: The #MxImageScaleMode to set
   void animate_scale_mode()(c_ulong mode, uint duration, ImageScaleMode scale_mode) nothrow {
      mx_image_animate_scale_mode(&this, mode, duration, scale_mode);
   }

   // VERSION: 1.2
   // Clear the current image and set a blank, transparent image.
   // RETURNS: static void
   void clear()() nothrow {
      mx_image_clear(&this);
   }

   // VERSION: 1.2
   // Determines whether image up-scaling is allowed.
   // RETURNS: %TRUE if upscaling is allowed, %FALSE otherwise
   int get_allow_upscale()() nothrow {
      return mx_image_get_allow_upscale(&this);
   }

   // VERSION: 1.2
   // Get the value of the MxImage:image-rotation property.
   // RETURNS: The value of the image-rotation property.
   float get_image_rotation()() nothrow {
      return mx_image_get_image_rotation(&this);
   }

   // VERSION: 1.2
   // Determines whether asynchronous image loading is in use.
   // RETURNS: %TRUE if images are set to load asynchronously, %FALSE otherwise
   int get_load_async()() nothrow {
      return mx_image_get_load_async(&this);
   }

   // VERSION: 1.2
   // Retrieves the height scaling threshold.
   // RETURNS: The height scaling threshold, in pixels
   uint get_scale_height_threshold()() nothrow {
      return mx_image_get_scale_height_threshold(&this);
   }

   // VERSION: 1.2
   // Get the current scale mode of @MxImage.
   // RETURNS: The current MxImageScaleMode
   ImageScaleMode get_scale_mode()() nothrow {
      return mx_image_get_scale_mode(&this);
   }

   // VERSION: 1.2
   // Retrieves the width scaling threshold.
   // RETURNS: The width scaling threshold, in pixels
   uint get_scale_width_threshold()() nothrow {
      return mx_image_get_scale_width_threshold(&this);
   }

   // VERSION: 1.2
   // Get the value of the MxImage:transition-duration property.
   // RETURNS: The value of the transition-duration property.
   uint get_transition_duration()() nothrow {
      return mx_image_get_transition_duration(&this);
   }

   // VERSION: 1.2
   // Sets whether up-scaling of images is allowed. If set to %TRUE and a size
   // larger than the image is requested, the image will be up-scaled in
   // software.
   // 
   // The advantage of this is that software up-scaling is potentially higher
   // quality, but it comes at the expense of video memory.
   // <allow>: %TRUE to allow upscaling, %FALSE otherwise
   void set_allow_upscale()(int allow) nothrow {
      mx_image_set_allow_upscale(&this, allow);
   }

   // VERSION: 1.2
   // Set the image data from unencoded image data, stored in memory. In case of
   // failure, #FALSE is returned and @error is set. It is expected that @buffer
   // will remain accessible for the duration of the load. Once it is finished
   // with, @buffer_free_func will be called.
   // RETURNS: #TRUE if the image was successfully updated
   // <buffer>: A buffer pointing to encoded image data
   // <buffer_size>: The size of @buffer, in bytes
   // <buffer_free_func>: A function to free @buffer, or %NULL
   int set_from_buffer(AT0, AT1)(AT0 /*ubyte*/ buffer, size_t buffer_size, GLib2.DestroyNotify buffer_free_func, AT1 /*GLib2.Error**/ error=null) nothrow {
      return mx_image_set_from_buffer(&this, UpCast!(ubyte*)(buffer), buffer_size, buffer_free_func, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.2
   // Set the image data from unencoded image data, stored in memory, and scales
   // it while loading. In case of failure, #FALSE is returned and @error is set.
   // It is expected that @buffer will remain accessible for the duration of the
   // load. Once it is finished with, @buffer_free_func will be called. The aspect
   // ratio will always be maintained.
   // RETURNS: #TRUE if the image was successfully updated
   // <buffer>: A buffer pointing to encoded image data
   // <buffer_size>: The size of @buffer, in bytes
   // <buffer_free_func>: A function to free @buffer, or %NULL
   // <width>: Width to scale the image to, or -1
   // <height>: Height to scale the image to, or -1
   int set_from_buffer_at_size(AT0, AT1)(AT0 /*ubyte*/ buffer, size_t buffer_size, GLib2.DestroyNotify buffer_free_func, int width, int height, AT1 /*GLib2.Error**/ error=null) nothrow {
      return mx_image_set_from_buffer_at_size(&this, UpCast!(ubyte*)(buffer), buffer_size, buffer_free_func, width, height, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.2
   // Sets the contents of the image from the given Cogl texture.
   // RETURNS: %TRUE on success, %FALSE on failure
   // <texture>: A #CoglHandle to a texture
   int set_from_cogl_texture()(Cogl.Handle texture) nothrow {
      return mx_image_set_from_cogl_texture(&this, texture);
   }

   // VERSION: 1.2
   // Set the image data from a buffer. In case of failure, #FALSE is returned
   // and @error is set.
   // RETURNS: #TRUE if the image was successfully updated
   // <data>: Image data
   // <pixel_format>: The #CoglPixelFormat of the buffer
   // <width>: Width in pixels of image data.
   // <height>: Height in pixels of image data
   // <rowstride>: Distance in bytes between row starts.
   int set_from_data(AT0, AT1)(AT0 /*ubyte*/ data, Cogl.PixelFormat pixel_format, int width, int height, int rowstride, AT1 /*GLib2.Error**/ error=null) nothrow {
      return mx_image_set_from_data(&this, UpCast!(ubyte*)(data), pixel_format, width, height, rowstride, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.2
   // Set the image data from an image file. In case of failure, #FALSE is returned
   // and @error is set.
   // RETURNS: #TRUE if the image was successfully updated
   // <filename>: Filename to read the file from
   int set_from_file(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) nothrow {
      return mx_image_set_from_file(&this, toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.2
   // Set the image data from an image file, and scale the image during loading.
   // In case of failure, #FALSE is returned and @error is set. The aspect ratio
   // will always be maintained.
   // RETURNS: #TRUE if the image was successfully updated
   // <filename>: Filename to read the file from
   // <width>: Width to scale the image to, or -1
   // <height>: Height to scale the image to, or -1
   int set_from_file_at_size(AT0, AT1)(AT0 /*char*/ filename, int width, int height, AT1 /*GLib2.Error**/ error=null) nothrow {
      return mx_image_set_from_file_at_size(&this, toCString!(char*)(filename), width, height, UpCast!(GLib2.Error**)(error));
   }

   // VERSION: 1.2
   // Set the MxImage:image-rotation property.
   // <rotation>: Rotation angle in degrees
   void set_image_rotation()(float rotation) nothrow {
      mx_image_set_image_rotation(&this, rotation);
   }

   // VERSION: 1.2
   // Sets whether to load images asynchronously. Asynchronous image loading
   // requires thread support (see g_thread_init()).
   // 
   // When using asynchronous image loading, all image-loading functions will
   // return immediately as successful. The #MxImage::image-loaded and
   // #MxImage::image-load-error signals are used to signal success or failure
   // of asynchronous image loading.
   // <load_async>: %TRUE to load images asynchronously
   void set_load_async()(int load_async) nothrow {
      mx_image_set_load_async(&this, load_async);
   }

   // VERSION: 1.2
   // Sets the threshold used to determine whether to scale the height of the
   // image. If a specific height is requested, the image height is allowed to
   // differ by this amount before scaling is employed.
   // 
   // This can be useful to avoid excessive CPU usage when the image differs
   // only slightly to the desired size.
   // <pixels>: Number of pixels
   void set_scale_height_threshold()(uint pixels) nothrow {
      mx_image_set_scale_height_threshold(&this, pixels);
   }

   // VERSION: 1.2
   // Set the scale mode on @MxImage
   // <mode>: The #MxImageScaleMode to set
   void set_scale_mode()(ImageScaleMode mode) nothrow {
      mx_image_set_scale_mode(&this, mode);
   }

   // VERSION: 1.2
   // Sets the threshold used to determine whether to scale the width of the
   // image. If a specific width is requested, the image width is allowed to
   // differ by this amount before scaling is employed.
   // 
   // This can be useful to avoid excessive CPU usage when the image differs
   // only slightly to the desired size.
   // <pixels>: Number of pixels
   void set_scale_width_threshold()(uint pixels) nothrow {
      mx_image_set_scale_width_threshold(&this, pixels);
   }

   // VERSION: 1.2
   // Set the MxImage:transition-duration property.
   // <duration>: Transition duration in milliseconds
   void set_transition_duration()(uint duration) nothrow {
      mx_image_set_transition_duration(&this, duration);
   }

   // VERSION: 1.2
   // Emitted when an asynchronous image load has encountered an error
   // and cannot load the requested image.
   extern (C) alias static void function (Image* this_, GLib2.Error* since, void* user_data=null) nothrow signal_image_load_error;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"image-load-error", CB/*:signal_image_load_error*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_image_load_error)||_ttmm!(CB, signal_image_load_error)()) {
      return signal_connect_data!()(&this, cast(char*)"image-load-error",
      cast(GObject2.Callback)cb, data, null, cf);
   }

   // VERSION: 1.2
   // Emitted when an asynchronous image load has completed successfully
   extern (C) alias static void function (Image* this_, void* user_data=null) nothrow signal_image_loaded;
   ulong signal_connect(string name:"image-loaded", CB/*:signal_image_loaded*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_image_loaded)||_ttmm!(CB, signal_image_loaded)()) {
      return signal_connect_data!()(&this, cast(char*)"image-loaded",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct ImageClass {
   private WidgetClass parent_class;
   extern (C) void function (Image* image) nothrow image_loaded;
   extern (C) void function (Image* image, GLib2.Error* error) nothrow image_load_error;
}

enum ImageError {
   BAD_FORMAT = 0,
   NO_ASYNC = 1,
   INTERNAL = 2,
   INVALID_PARAMETER = 3
}
struct ImagePrivate {
}

// Defines the scaling mode of an image.
enum ImageScaleMode {
   NONE = 0,
   FIT = 1,
   CROP = 2
}
// This is an opaque structure whose members cannot be directly accessed.
struct ItemFactory /* Interface */ {
   mixin template __interface__() {
      // Create an item
      // RETURNS: the new item
      Clutter.Actor* /*new*/ create()() nothrow {
         return mx_item_factory_create(cast(ItemFactory*)&this);
      }
   }
   mixin __interface__;
}

// Interface for creating custom items
struct ItemFactoryIface {
   private GObject2.TypeInterface g_iface;
   // RETURNS: the new item
   extern (C) Clutter.Actor* /*new*/ function (ItemFactory* factory) nothrow create;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}


// The contents of the this structure are private and should only be accessed
// through the public API.
struct ItemView /* : Grid */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Scrollable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent grid;
   Grid parent;
   private ItemViewPrivate* priv;


   // Create a new #MxItemView
   // RETURNS: a newly allocated #MxItemView
   static ItemView* new_()() nothrow {
      return mx_item_view_new();
   }
   static auto opCall()() {
      return mx_item_view_new();
   }

   // Adds an attribute mapping between the current model and the objects from the
   // cell renderer.
   // <attribute>: Name of the attribute
   // <column>: Column number
   void add_attribute(AT0)(AT0 /*char*/ attribute, int column) nothrow {
      mx_item_view_add_attribute(&this, toCString!(char*)(attribute), column);
   }

   // Freeze the view. This means that the view will not act on changes to the
   // model until it is thawed. Call #mx_item_view_thaw to thaw the view
   void freeze()() nothrow {
      mx_item_view_freeze(&this);
   }

   // Gets the #MxItemFactory used for creating new items.
   // RETURNS: A #MxItemFactory.
   ItemFactory* get_factory()() nothrow {
      return mx_item_view_get_factory(&this);
   }

   // Get the item type currently being used to create items
   // RETURNS: a #GType
   Type get_item_type()() nothrow {
      return mx_item_view_get_item_type(&this);
   }

   // Get the model currently used by the #MxItemView
   // RETURNS: the current #ClutterModel
   Clutter.Model* get_model()() nothrow {
      return mx_item_view_get_model(&this);
   }

   // Sets @factory to be the factory used for creating new items
   // <factory>: A #MxItemFactory
   void set_factory(AT0)(AT0 /*ItemFactory*/ factory=null) nothrow {
      mx_item_view_set_factory(&this, UpCast!(ItemFactory*)(factory));
   }

   // Set the item type used to create items representing each row in the
   // model
   // <item_type>: A #GType
   void set_item_type()(Type item_type) nothrow {
      mx_item_view_set_item_type(&this, item_type);
   }

   // Set the model used by the #MxItemView
   // <model>: A #ClutterModel
   void set_model(AT0)(AT0 /*Clutter.Model*/ model) nothrow {
      mx_item_view_set_model(&this, UpCast!(Clutter.Model*)(model));
   }

   // Thaw the view. This means that the view will now act on changes to the
   // model.
   void thaw()() nothrow {
      mx_item_view_thaw(&this);
   }
}

struct ItemViewClass {
   GridClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct ItemViewPrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct KineticScrollView /* : Bin */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Scrollable.__interface__;
   mixin Stylable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance bin;
   Bin parent_instance;
   private KineticScrollViewPrivate* priv;


   // VERSION: 1.2
   // Creates a new #MxKineticScrollView.
   // RETURNS: a newly allocated #MxKineticScrollView
   static KineticScrollView* new_()() nothrow {
      return mx_kinetic_scroll_view_new();
   }
   static auto opCall()() {
      return mx_kinetic_scroll_view_new();
   }

   // VERSION: 1.4
   // Retrieves the initial acceleration factor of the kinetic scroll-view.
   // RETURNS: The initial acceleration factor of the kinetic scroll-view
   double get_acceleration_factor()() nothrow {
      return mx_kinetic_scroll_view_get_acceleration_factor(&this);
   }

   // VERSION: 1.4
   // Retrieves the duration of the adjustment clamp animation.
   // RETURNS: Clamp duration
   uint get_clamp_duration()() nothrow {
      return mx_kinetic_scroll_view_get_clamp_duration(&this);
   }

   // VERSION: 1.4
   // Retrieves the animation mode to use for the adjustment clamp animation.
   // RETURNS: Clamp mode
   c_ulong get_clamp_mode()() nothrow {
      return mx_kinetic_scroll_view_get_clamp_mode(&this);
   }

   // VERSION: 1.4
   // Retrieves whether to clamp to step increments based on the center of the page.
   // RETURNS: Clamp to center
   int get_clamp_to_center()() nothrow {
      return mx_kinetic_scroll_view_get_clamp_to_center(&this);
   }

   // VERSION: 1.2
   // Retrieves the deceleration rate of the kinetic scroll-view.
   // RETURNS: The deceleration rate of the kinetic scroll-view
   double get_deceleration()() nothrow {
      return mx_kinetic_scroll_view_get_deceleration(&this);
   }

   // VERSION: 1.2
   // Gets the #MxKineticScrollView:mouse-button property
   // 
   // kinetic scroll-view
   // RETURNS: The mouse button number used to initiate drag events on the
   uint get_mouse_button()() nothrow {
      return mx_kinetic_scroll_view_get_mouse_button(&this);
   }

   // VERSION: 1.2
   // Retrieves the deceleration rate multiplier used when the scroll-view is
   // scrolling beyond its boundaries.
   double get_overshoot()() nothrow {
      return mx_kinetic_scroll_view_get_overshoot(&this);
   }

   // Retrieves the scrolling policy of the kinetic scroll-view.
   // RETURNS: A #MxScrollPolicy
   ScrollPolicy get_scroll_policy()() nothrow {
      return mx_kinetic_scroll_view_get_scroll_policy(&this);
   }

   // VERSION: 1.2
   // Gets the #MxKineticScrollView:use-captured property.
   // RETURNS: %TRUE if captured-events should be used to initiate scrolling
   int get_use_captured()() nothrow {
      return mx_kinetic_scroll_view_get_use_captured(&this);
   }

   // VERSION: 1.4
   // Factor applied to the initial momentum.
   // <acceleration_factor>: The acceleration factor
   void set_acceleration_factor()(double acceleration_factor) nothrow {
      mx_kinetic_scroll_view_set_acceleration_factor(&this, acceleration_factor);
   }

   // VERSION: 1.4
   // Duration of the adjustment clamp animation.
   // <clamp_duration>: Clamp duration
   void set_clamp_duration()(uint clamp_duration) nothrow {
      mx_kinetic_scroll_view_set_clamp_duration(&this, clamp_duration);
   }

   // VERSION: 1.4
   // Animation mode to use for the adjustment clamp animation.
   // <clamp_mode>: Clamp mode
   void set_clamp_mode()(c_ulong clamp_mode) nothrow {
      mx_kinetic_scroll_view_set_clamp_mode(&this, clamp_mode);
   }

   // VERSION: 1.4
   // Set whether to clamp to step increments based on the center of the page.
   // <clamp_to_center>: Clamp to center
   void set_clamp_to_center()(int clamp_to_center) nothrow {
      mx_kinetic_scroll_view_set_clamp_to_center(&this, clamp_to_center);
   }

   // VERSION: 1.2
   // Sets the deceleration rate when a drag is finished on the kinetic
   // scroll-view. This is the value that the momentum is divided by
   // every 60th of a second.
   // <rate>: The deceleration rate
   void set_deceleration()(double rate) nothrow {
      mx_kinetic_scroll_view_set_deceleration(&this, rate);
   }

   // VERSION: 1.2
   // Sets the mouse button number used to initiate drag events on the kinetic
   // scroll-view.
   // <button>: A mouse button number
   void set_mouse_button()(uint button) nothrow {
      mx_kinetic_scroll_view_set_mouse_button(&this, button);
   }

   // VERSION: 1.2
   // Sets the rate at which the view will decelerate when scrolling beyond its
   // boundaries. The deceleration rate will be multiplied by this value every
   // 60th of a second when the view is scrolling outside of the range set by its
   // adjustments.
   // 
   // See mx_kinetic_scroll_view_set_deceleration()
   // <overshoot>: The rate at which the view will decelerate when scrolling beyond its boundaries.
   void set_overshoot()(double overshoot) nothrow {
      mx_kinetic_scroll_view_set_overshoot(&this, overshoot);
   }

   // Sets the scrolling policy for the kinetic scroll-view. This controls the
   // possible axes of movement, and can affect the minimum size of the widget.
   // <policy>: A #MxScrollPolicy
   void set_scroll_policy()(ScrollPolicy policy) nothrow {
      mx_kinetic_scroll_view_set_scroll_policy(&this, policy);
   }

   // VERSION: 1.2
   // Sets whether to use captured events to initiate drag events. This can be
   // used to block events that would initiate scrolling from reaching the child
   // actor.
   // <use_captured>: %TRUE to use captured events
   void set_use_captured()(int use_captured) nothrow {
      mx_kinetic_scroll_view_set_use_captured(&this, use_captured);
   }

   // VERSION: 1.2
   // Stops any current movement due to kinetic scrolling.
   void stop()() nothrow {
      mx_kinetic_scroll_view_stop(&this);
   }
}

struct KineticScrollViewClass {
   BinClass parent_class;
}

struct KineticScrollViewPrivate {
}

enum KineticScrollViewState {
   IDLE = 0,
   PANNING = 1,
   SCROLLING = 2,
   CLAMPING = 3
}

// The contents of this structure is private and should only be accessed using
// the provided API.
struct Label /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Stylable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance widget;
   Widget parent_instance;
   private LabelPrivate* priv;


   // Create a new #MxLabel
   // RETURNS: a new #MxLabel
   static Label* new_()() nothrow {
      return mx_label_new();
   }
   static auto opCall()() {
      return mx_label_new();
   }

   // Create a new #MxLabel with the specified label
   // RETURNS: a new #MxLabel
   // <text>: text to set the label to
   static Label* new_with_text(AT0)(AT0 /*char*/ text) nothrow {
      return mx_label_new_with_text(toCString!(char*)(text));
   }
   static auto opCall(AT0)(AT0 /*char*/ text) {
      return mx_label_new_with_text(toCString!(char*)(text));
   }

   // Retrieve the internal #ClutterText so that extra parameters can be set
   // 
   // is owned by the #MxLabel and should not be unref'ed by the application.
   // RETURNS: the #ClutterText used by #MxLabel. The label
   Clutter.Actor* get_clutter_text()() nothrow {
      return mx_label_get_clutter_text(&this);
   }

   // VERSION: 1.2
   // Determines whether the label has been set to fade out when there isn't
   // enough space allocated to display the entire label.
   // RETURNS: %TRUE if the label is set to fade out, %FALSE otherwise
   int get_fade_out()() nothrow {
      return mx_label_get_fade_out(&this);
   }

   // VERSION: 1.2
   // Get the value of the #MxLabel:line-wrap property.
   // RETURNS: %TRUE if the "line-wrap" property is set.
   int get_line_wrap()() nothrow {
      return mx_label_get_line_wrap(&this);
   }

   // VERSION: 1.4
   // Returns the current value of the #MxLabel:show-tooltip property.
   // RETURNS: %TRUE if the #MxLabel:show-tooltip property is enabled
   int get_show_tooltip()() nothrow {
      return mx_label_get_show_tooltip(&this);
   }

   // Get the text displayed on the label
   // RETURNS: the text for the label. This must not be freed by the application
   char* get_text()() nothrow {
      return mx_label_get_text(&this);
   }

   // VERSION: 1.2
   // Determines whether the text of the label is being treated as Pango markup.
   // 
   // %FALSE otherwise.
   // RETURNS: %TRUE if the text of the label is treated as Pango markup,
   int get_use_markup()() nothrow {
      return mx_label_get_use_markup(&this);
   }
   Align get_x_align()() nothrow {
      return mx_label_get_x_align(&this);
   }
   Align get_y_align()() nothrow {
      return mx_label_get_y_align(&this);
   }

   // VERSION: 1.2
   // Set whether to fade out the end of the label, instead of ellipsizing.
   // Enabling this mode will also set the #ClutterText:single-line-mode and
   // #ClutterText:ellipsize properties.
   // <fade>: %TRUE to fade out, %FALSE otherwise
   void set_fade_out()(int fade) nothrow {
      mx_label_set_fade_out(&this, fade);
   }

   // VERSION: 1.2
   // Set the value of the #MxLabel:line-wrap property.
   // <line_wrap>: new value of the line-wrap property.
   void set_line_wrap()(int line_wrap) nothrow {
      mx_label_set_line_wrap(&this, line_wrap);
   }

   // VERSION: 1.4
   // Set the value of the #MxLabel:show-tooltip property
   // <show_tooltip>: %TRUE if the tooltip should be shown
   void set_show_tooltip()(int show_tooltip) nothrow {
      mx_label_set_show_tooltip(&this, show_tooltip);
   }

   // Sets the text displayed on the label
   // <text>: text to set the label to
   void set_text(AT0)(AT0 /*char*/ text) nothrow {
      mx_label_set_text(&this, toCString!(char*)(text));
   }

   // Sets whether the text of the label should be treated as Pango markup.
   // <use_markup>: %TRUE to use Pango markup, %FALSE otherwise
   void set_use_markup()(int use_markup) nothrow {
      mx_label_set_use_markup(&this, use_markup);
   }
   void set_x_align()(Align align_) nothrow {
      mx_label_set_x_align(&this, align_);
   }
   void set_y_align()(Align align_) nothrow {
      mx_label_set_y_align(&this, align_);
   }
}

struct LabelClass {
   WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct LabelPrivate {
}


// The contents of the this structure are private and should only be accessed
// through the public API.
struct ListView /* : BoxLayout */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Scrollable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent boxlayout;
   BoxLayout parent;
   private ListViewPrivate* priv;


   // Create a new #MxListView
   // RETURNS: a newly allocated #MxListView
   static ListView* new_()() nothrow {
      return mx_list_view_new();
   }
   static auto opCall()() {
      return mx_list_view_new();
   }

   // Adds an attribute mapping between the current model and the objects from the
   // cell renderer.
   // <attribute>: Name of the attribute
   // <column>: Column number
   void add_attribute(AT0)(AT0 /*char*/ attribute, int column) nothrow {
      mx_list_view_add_attribute(&this, toCString!(char*)(attribute), column);
   }

   // Freeze the view. This means that the view will not act on changes to the
   // model until it is thawed. Call #mx_list_view_thaw to thaw the view.
   void freeze()() nothrow {
      mx_list_view_freeze(&this);
   }

   // Gets the #MxItemFactory used for creating new list items.
   // RETURNS: A #MxItemFactory.
   ItemFactory* get_factory()() nothrow {
      return mx_list_view_get_factory(&this);
   }

   // Get the item type currently being used to create items
   // RETURNS: a #GType
   Type get_item_type()() nothrow {
      return mx_list_view_get_item_type(&this);
   }

   // Get the model currently used by the #MxListView
   // RETURNS: the current #ClutterModel
   Clutter.Model* get_model()() nothrow {
      return mx_list_view_get_model(&this);
   }

   // Sets @factory to be the factory used for creating new list items
   // <factory>: A #MxItemFactory
   void set_factory(AT0)(AT0 /*ItemFactory*/ factory=null) nothrow {
      mx_list_view_set_factory(&this, UpCast!(ItemFactory*)(factory));
   }

   // Set the item type used to create items representing each row in the
   // model
   // <item_type>: A #GType
   void set_item_type()(Type item_type) nothrow {
      mx_list_view_set_item_type(&this, item_type);
   }

   // Set the model used by the #MxListView
   // <model>: A #ClutterModel
   void set_model(AT0)(AT0 /*Clutter.Model*/ model) nothrow {
      mx_list_view_set_model(&this, UpCast!(Clutter.Model*)(model));
   }

   // Thaw the view. This means that the view will now act on changes to the
   // model.
   void thaw()() nothrow {
      mx_list_view_thaw(&this);
   }
}

struct ListViewClass {
   BoxLayoutClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct ListViewPrivate {
}

enum LongPressAction {
   QUERY = 0,
   ACTION = 1,
   CANCEL = 2
}
enum int MAJOR_VERSION = 1;
enum int MICRO_VERSION = 2;
enum int MINOR_VERSION = 99;

// The contents of this structure are private and should only be accessed
// through the public API.
struct Menu /* : FloatingWidget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent floatingwidget;
   FloatingWidget parent;
   private MenuPrivate* priv;


   // Create a new #MxMenu
   // RETURNS: a newly allocated #MxMenu
   static Menu* new_()() nothrow {
      return mx_menu_new();
   }
   static auto opCall()() {
      return mx_menu_new();
   }

   // Append @action to @menu.
   // <action>: A #MxAction
   void add_action(AT0)(AT0 /*Action*/ action) nothrow {
      mx_menu_add_action(&this, UpCast!(Action*)(action));
   }

   // Remove @action from @menu.
   // <action>: A #MxAction
   void remove_action(AT0)(AT0 /*Action*/ action) nothrow {
      mx_menu_remove_action(&this, UpCast!(Action*)(action));
   }
   // Remove all the actions from @menu.
   void remove_all()() nothrow {
      mx_menu_remove_all(&this);
   }

   // Moves the menu to the specified position and shows it.
   // <x>: X position
   // <y>: Y position
   void show_with_position()(float x, float y) nothrow {
      mx_menu_show_with_position(&this, x, y);
   }
   extern (C) alias static void function (Menu* this_, Action* object, void* user_data=null) nothrow signal_action_activated;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"action-activated", CB/*:signal_action_activated*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_action_activated)||_ttmm!(CB, signal_action_activated)()) {
      return signal_connect_data!()(&this, cast(char*)"action-activated",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct MenuClass {
   FloatingWidgetClass parent_class;
   extern (C) void function (Menu* menu, Action* action) nothrow action_activated;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct MenuPrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct Notebook /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   NotebookPrivate* priv;

   static Notebook* new_()() nothrow {
      return mx_notebook_new();
   }
   static auto opCall()() {
      return mx_notebook_new();
   }

   // Get the current page
   // RETURNS: the current page
   Clutter.Actor* get_current_page()() nothrow {
      return mx_notebook_get_current_page(&this);
   }
   int get_enable_gestures()() nothrow {
      return mx_notebook_get_enable_gestures(&this);
   }
   // Change the current page to next one.
   void next_page()() nothrow {
      mx_notebook_next_page(&this);
   }
   // Change the current page to previous one.
   void previous_page()() nothrow {
      mx_notebook_previous_page(&this);
   }
   void set_current_page(AT0)(AT0 /*Clutter.Actor*/ page) nothrow {
      mx_notebook_set_current_page(&this, UpCast!(Clutter.Actor*)(page));
   }
   void set_enable_gestures()(int enabled) nothrow {
      mx_notebook_set_enable_gestures(&this, enabled);
   }
}

struct NotebookClass {
   WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct NotebookPrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct Offscreen /* : Clutter.Texture */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent texture;
   Clutter.Texture parent;
   OffscreenPrivate* priv;


   // Creates a new #MxOffscreen.
   // RETURNS: a newly allocated #MxOffscreen
   static Offscreen* new_()() nothrow {
      return mx_offscreen_new();
   }
   static auto opCall()() {
      return mx_offscreen_new();
   }

   // VERSION: 1.2
   // Gets the value of the #MxOffscreen:accumulation-enabled property.
   // RETURNS: #TRUE if the accumulation buffer is enabled
   int get_accumulation_enabled()() nothrow {
      return mx_offscreen_get_accumulation_enabled(&this);
   }

   // VERSION: 1.2
   // Gets the #MxOffscreen:accumulation-material property.
   // 
   // for the accumulation buffer
   // RETURNS: The #CoglHandle for the material used
   Cogl.Handle get_accumulation_material()() nothrow {
      return mx_offscreen_get_accumulation_material(&this);
   }

   // Gets the value of the #MxOffscreen:auto-update property.
   // RETURNS: #TRUE if automatic updating of the offscreen surface is enabled
   int get_auto_update()() nothrow {
      return mx_offscreen_get_auto_update(&this);
   }

   // VERSION: 1.2
   // Gets the value of the #MxOffscreen:buffer property.
   // RETURNS: the #CoglHandle for the offscreen buffer object
   Cogl.Handle get_buffer()() nothrow {
      return mx_offscreen_get_buffer(&this);
   }

   // Gets the value of the #MxOffscreen:child property.
   // RETURNS: The child of the offscreen widget
   Clutter.Actor* get_child()() nothrow {
      return mx_offscreen_get_child(&this);
   }

   // Gets the value of the #MxOffscreen:pick-child property.
   // RETURNS: #TRUE if picking of the child is enabled.
   int get_pick_child()() nothrow {
      return mx_offscreen_get_pick_child(&this);
   }

   // VERSION: 1.2
   // Gets the value of the #MxOffscreen:redirect-enabled property.
   // RETURNS: #TRUE if offscreen redirection is enabled
   int get_redirect_enabled()() nothrow {
      return mx_offscreen_get_redirect_enabled(&this);
   }

   // VERSION: 1.2
   // Sets whether the accumulation buffer is enabled. When enabled, an extra
   // offscreen buffer is allocated, and the contents of the offscreen texture
   // are blended with this accumulation buffer. By default, the blend function
   // is set to blend the contents of the offscreen texture with the accumulation
   // buffer at the opacity specified in the alpha component of the blend
   // constant. This opacity is 50% by default.
   // <enable>: #TRUE to enable an accumulation buffer
   void set_accumulation_enabled()(int enable) nothrow {
      mx_offscreen_set_accumulation_enabled(&this, enable);
   }

   // Enable automatic updating of the offscreen surface when the child is
   // updated.
   // <auto_update>: #TRUE if the offscreen surface should be automatically updated
   void set_auto_update()(int auto_update) nothrow {
      mx_offscreen_set_auto_update(&this, auto_update);
   }

   // Redirects the painting of @actor to the offscreen surface owned by
   // @offscreen. In the event that @actor is unparented, it will be parented
   // to @offscreen. Note that when you redirect the painting of @actor, it
   // will no longer be painted in its original position in the scenegraph.
   // <actor>: A #ClutterActor
   void set_child(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
      mx_offscreen_set_child(&this, UpCast!(Clutter.Actor*)(actor));
   }

   // Enable picking of the child actor.
   // <pick>: #TRUE to enable picking of the child actor
   void set_pick_child()(int pick) nothrow {
      mx_offscreen_set_pick_child(&this, pick);
   }

   // VERSION: 1.2
   // Sets the value of the #MxOffscreen:redirect-enabled property. When
   // redirection is enabled, the painting of the child of @offscreen will be
   // redirected to the texture of @offscreen.
   // <enabled>: #TRUE if redirection to the offscreen surface should be enabled
   void set_redirect_enabled()(int enabled) nothrow {
      mx_offscreen_set_redirect_enabled(&this, enabled);
   }

   // Updates the offscreen surface. This causes the child of @offscreen to be
   // drawn into the texture of @offscreen.
   void update()() nothrow {
      mx_offscreen_update(&this);
   }
}

struct OffscreenClass {
   Clutter.TextureClass parent_class;
   extern (C) void function (Offscreen* self) nothrow paint_child;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct OffscreenPrivate {
}

// Defines the orientation of various layout widgets.
enum Orientation {
   HORIZONTAL = 0,
   VERTICAL = 1
}
enum int PARAM_TRANSLATEABLE = 256;
// The padding from the internal border of the parent container.
struct Padding {
   float top, right, bottom, left;
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct PathBar /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   PathBarPrivate* priv;

   static PathBar* new_()() nothrow {
      return mx_path_bar_new();
   }
   static auto opCall()() {
      return mx_path_bar_new();
   }
   // Remove all the current buttons
   void clear()() nothrow {
      mx_path_bar_clear(&this);
   }

   // Get the value of the #MxPathBar:clear-on-change property
   // RETURNS: the value of the "clear-on-change" property
   int get_clear_on_change()() nothrow {
      return mx_path_bar_get_clear_on_change(&this);
   }

   // Get the value of the #MxPathBar:editable property.
   // RETURNS: the current value of the "editable" property.
   int get_editable()() nothrow {
      return mx_path_bar_get_editable(&this);
   }

   // Get the MxEntry used as the editable area in the MxPathBar.
   // RETURNS: MxEntry *
   Entry* get_entry()() nothrow {
      return mx_path_bar_get_entry(&this);
   }
   char* get_label()(int level) nothrow {
      return mx_path_bar_get_label(&this, level);
   }
   int get_level()() nothrow {
      return mx_path_bar_get_level(&this);
   }
   char* get_text()() nothrow {
      return mx_path_bar_get_text(&this);
   }
   int pop()() nothrow {
      return mx_path_bar_pop(&this);
   }
   int push(AT0)(AT0 /*char*/ name) nothrow {
      return mx_path_bar_push(&this, toCString!(char*)(name));
   }

   // Set theh value of the #MxPathBar:clear-on-change property
   // <clear_on_change>: the new value of the property
   void set_clear_on_change()(int clear_on_change) nothrow {
      mx_path_bar_set_clear_on_change(&this, clear_on_change);
   }

   // Set the value of the #MxPathBar:editable property.
   // <editable>: #TRUE if the path bar should be editable
   void set_editable()(int editable) nothrow {
      mx_path_bar_set_editable(&this, editable);
   }

   // Set the text on the button specified by @level
   // <level>: A #gint
   // <label>: A #gchar
   void set_label(AT0)(int level, AT0 /*char*/ label) nothrow {
      mx_path_bar_set_label(&this, level, toCString!(char*)(label));
   }

   // Set the text in the editable area of the #MxPathBar
   // <text>: string to set the editable text to.
   void set_text(AT0)(AT0 /*char*/ text) nothrow {
      mx_path_bar_set_text(&this, toCString!(char*)(text));
   }
}

struct PathBarClass {
   WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct PathBarPrivate {
}

// Defines the position of an interface element.
enum Position /* Version 1.2 */ {
   TOP = 0,
   RIGHT = 1,
   BOTTOM = 2,
   LEFT = 3
}

// The contents of this structure are private and should only be
// accessed through the public API.
struct ProgressBar /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   private ProgressBarPrivate* priv;


   // Create a new progress bar
   // RETURNS: a new #MxProgressBar
   static ProgressBar* new_()() nothrow {
      return mx_progress_bar_new();
   }
   static auto opCall()() {
      return mx_progress_bar_new();
   }

   // Get the progress of the progress bar
   // RETURNS: A value between 0.0 and 1.0
   double get_progress()() nothrow {
      return mx_progress_bar_get_progress(&this);
   }

   // Set the progress of the progress bar
   // <progress>: A value between 0.0 and 1.0
   void set_progress()(double progress) nothrow {
      mx_progress_bar_set_progress(&this, progress);
   }
}

struct ProgressBarClass {
   WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct ProgressBarPrivate {
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct ScrollBar /* : Bin */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance bin;
   Bin parent_instance;
   private ScrollBarPrivate* priv;


   // Create a new #MxScrollBar
   // RETURNS: a new #MxScrollBar
   static ScrollBar* new_()() nothrow {
      return mx_scroll_bar_new();
   }
   static auto opCall()() {
      return mx_scroll_bar_new();
   }

   // Create a new #MxScrollBar with the given adjustment set
   // RETURNS: a new #MxScrollBar
   // <adjustment>: an #MxAdjustment
   static ScrollBar* new_with_adjustment(AT0)(AT0 /*Adjustment*/ adjustment) nothrow {
      return mx_scroll_bar_new_with_adjustment(UpCast!(Adjustment*)(adjustment));
   }
   static auto opCall(AT0)(AT0 /*Adjustment*/ adjustment) {
      return mx_scroll_bar_new_with_adjustment(UpCast!(Adjustment*)(adjustment));
   }

   // Gets the adjustment object that stores the current position
   // of the scrollbar.
   // RETURNS: the adjustment
   Adjustment* get_adjustment()() nothrow {
      return mx_scroll_bar_get_adjustment(&this);
   }
   Orientation get_orientation()() nothrow {
      return mx_scroll_bar_get_orientation(&this);
   }
   void set_adjustment(AT0)(AT0 /*Adjustment*/ adjustment) nothrow {
      mx_scroll_bar_set_adjustment(&this, UpCast!(Adjustment*)(adjustment));
   }
   void set_orientation()(Orientation orientation) nothrow {
      mx_scroll_bar_set_orientation(&this, orientation);
   }
   extern (C) alias static void function (ScrollBar* this_, void* user_data=null) nothrow signal_scroll_start;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"scroll-start", CB/*:signal_scroll_start*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_scroll_start)||_ttmm!(CB, signal_scroll_start)()) {
      return signal_connect_data!()(&this, cast(char*)"scroll-start",
      cast(GObject2.Callback)cb, data, null, cf);
   }
   extern (C) alias static void function (ScrollBar* this_, void* user_data=null) nothrow signal_scroll_stop;
   ulong signal_connect(string name:"scroll-stop", CB/*:signal_scroll_stop*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_scroll_stop)||_ttmm!(CB, signal_scroll_stop)()) {
      return signal_connect_data!()(&this, cast(char*)"scroll-stop",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct ScrollBarClass {
   BinClass parent_class;
   extern (C) void function (ScrollBar* bar) nothrow scroll_start;
   extern (C) void function (ScrollBar* bar) nothrow scroll_stop;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct ScrollBarPrivate {
}

// Defines the scrolling policy of scrollable widgets.
enum ScrollPolicy {
   NONE = 0,
   HORIZONTAL = 1,
   VERTICAL = 2,
   BOTH = 3
}

// The contents of this structure are private and should only be accessed
// through the public API.
struct ScrollView /* : Bin */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance bin;
   Bin parent_instance;
   private ScrollViewPrivate* priv;

   static ScrollView* new_()() nothrow {
      return mx_scroll_view_new();
   }
   static auto opCall()() {
      return mx_scroll_view_new();
   }

   // Ensures that a given region is visible in the ScrollView, with the top-left
   // taking precedence.
   // <geometry>: The region to make visible
   void ensure_visible(AT0)(AT0 /*Clutter.Geometry*/ geometry) nothrow {
      mx_scroll_view_ensure_visible(&this, UpCast!(Clutter.Geometry*)(geometry));
   }
   int get_enable_gestures()() nothrow {
      return mx_scroll_view_get_enable_gestures(&this);
   }
   int get_enable_mouse_scrolling()() nothrow {
      return mx_scroll_view_get_enable_mouse_scrolling(&this);
   }
   ScrollPolicy get_scroll_policy()() nothrow {
      return mx_scroll_view_get_scroll_policy(&this);
   }
   void set_enable_gestures()(int enabled) nothrow {
      mx_scroll_view_set_enable_gestures(&this, enabled);
   }
   void set_enable_mouse_scrolling()(int enabled) nothrow {
      mx_scroll_view_set_enable_mouse_scrolling(&this, enabled);
   }
   void set_scroll_policy()(ScrollPolicy policy) nothrow {
      mx_scroll_view_set_scroll_policy(&this, policy);
   }
}

struct ScrollViewClass {
   BinClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct ScrollViewPrivate {
}

// This is an opaque structure whose members cannot be directly accessed.
struct Scrollable /* Interface */ {
   mixin template __interface__() {
      // Gets the adjustment objects that store the offsets of the scrollable widget
      // into its possible scrolling area.
      // <hadjustment>: location to store the horizontal adjustment, or %NULL
      // <vadjustment>: location to store the vertical adjustment, or %NULL
      void get_adjustments(AT0, AT1)(/*out*/ AT0 /*Adjustment**/ hadjustment=null, /*out*/ AT1 /*Adjustment**/ vadjustment=null) nothrow {
         mx_scrollable_get_adjustments(cast(Scrollable*)&this, UpCast!(Adjustment**)(hadjustment), UpCast!(Adjustment**)(vadjustment));
      }
      void set_adjustments(AT0, AT1)(AT0 /*Adjustment*/ hadjustment, AT1 /*Adjustment*/ vadjustment) nothrow {
         mx_scrollable_set_adjustments(cast(Scrollable*)&this, UpCast!(Adjustment*)(hadjustment), UpCast!(Adjustment*)(vadjustment));
      }
   }
   mixin __interface__;
}

struct ScrollableIface {
   private GObject2.TypeInterface parent;
   extern (C) void function (Scrollable* scrollable, Adjustment* hadjustment, Adjustment* vadjustment) nothrow set_adjustments;

   // <hadjustment>: location to store the horizontal adjustment, or %NULL
   // <vadjustment>: location to store the vertical adjustment, or %NULL
   extern (C) void function (Scrollable* scrollable, /*out*/ Adjustment** hadjustment=null, /*out*/ Adjustment** vadjustment=null) nothrow get_adjustments;
}

struct Settings /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   SettingsPrivate* priv;


   // Get the global MxSettings object.
   // RETURNS: an #MxSettings object
   static Settings* get_default()() nothrow {
      return mx_settings_get_default();
   }
}

struct SettingsClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct SettingsPrivate {
}

struct SettingsProvider {
}

struct SettingsProviderIface {
   private GObject2.TypeInterface parent_iface;
   // Unintrospectable functionp: setting_changed() / ()
   extern (C) void function (SettingsProvider* provider, MxSettingsProperty id) nothrow setting_changed;
   // Unintrospectable functionp: get_setting() / ()
   extern (C) int function (SettingsProvider* provider, MxSettingsProperty id, void* value) nothrow get_setting;
   // Unintrospectable functionp: set_setting() / ()
   extern (C) int function (SettingsProvider* provider, MxSettingsProperty id, void* value) nothrow set_setting;
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct Slider /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   private SliderPrivate* priv;


   // Create a new slider
   // RETURNS: a new #MxSlider
   static Slider* new_()() nothrow {
      return mx_slider_new();
   }
   static auto opCall()() {
      return mx_slider_new();
   }

   // VERSION: 1.2
   // Get the value of the #MxSlider:buffer-value property.
   // RETURNS: The current value of the "buffer-value" property.
   double get_buffer_value()() nothrow {
      return mx_slider_get_buffer_value(&this);
   }

   // Retrieve the current value of the media bar
   // RETURNS: gdouble
   double get_value()() nothrow {
      return mx_slider_get_value(&this);
   }

   // VERSION: 1.2
   // Set the value of the #MxSlider:buffer-value property.
   // <value>: the new buffer value of the slider
   void set_buffer_value()(double value) nothrow {
      mx_slider_set_buffer_value(&this, value);
   }

   // Set the value of the slider
   // <value>: A value between 0.0 and 1.0
   void set_value()(double value) nothrow {
      mx_slider_set_value(&this, value);
   }
   extern (C) alias static void function (Slider* this_, void* user_data=null) nothrow signal_slide_start;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"slide-start", CB/*:signal_slide_start*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_slide_start)||_ttmm!(CB, signal_slide_start)()) {
      return signal_connect_data!()(&this, cast(char*)"slide-start",
      cast(GObject2.Callback)cb, data, null, cf);
   }
   extern (C) alias static void function (Slider* this_, void* user_data=null) nothrow signal_slide_stop;
   ulong signal_connect(string name:"slide-stop", CB/*:signal_slide_stop*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_slide_stop)||_ttmm!(CB, signal_slide_stop)()) {
      return signal_connect_data!()(&this, cast(char*)"slide-stop",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct SliderClass {
   WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct SliderPrivate {
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct Spinner /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   private SpinnerPrivate* priv;


   // VERSION: 1.2
   // Create a new #MxSpinner widget.
   // RETURNS: a newly allocated #MxSpinner
   static Spinner* new_()() nothrow {
      return mx_spinner_new();
   }
   static auto opCall()() {
      return mx_spinner_new();
   }

   // VERSION: 1.2
   // Determines whether the spinner is animating.
   // RETURNS: %TRUE if the spinner is animating, %FALSE otherwise
   int get_animating()() nothrow {
      return mx_spinner_get_animating(&this);
   }

   // VERSION: 1.2
   // Sets whether the spinner is animating. A spinner can be stopped if
   // the task it represents has finished, or to save energy.
   // <animating>: %TRUE to enable animation, %FALSE to disable
   void set_animating()(int animating) nothrow {
      mx_spinner_set_animating(&this, animating);
   }

   // VERSION: 1.2
   // Emitted after the animation has displayed the final frame.
   extern (C) alias static void function (Spinner* this_, void* user_data=null) nothrow signal_looped;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"looped", CB/*:signal_looped*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_looped)||_ttmm!(CB, signal_looped)()) {
      return signal_connect_data!()(&this, cast(char*)"looped",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct SpinnerClass {
   WidgetClass parent_class;
   extern (C) void function (Spinner* spinner) nothrow looped;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
}

struct SpinnerPrivate {
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct Stack /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   private StackPrivate* priv;


   // VERSION: 1.2
   // Create a new #MxStack.
   // RETURNS: a newly allocated #MxStack
   static Stack* new_()() nothrow {
      return mx_stack_new();
   }
   static auto opCall()() {
      return mx_stack_new();
   }

   // VERSION: 1.4
   // Get the value of the #MxStackChild:fit property.
   // RETURNS: the current value of the #MxStackChild:crop property
   // <child>: A #ClutterActor
   int child_get_crop(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_stack_child_get_crop(&this, UpCast!(Clutter.Actor*)(child));
   }

   // VERSION: 1.2
   // Get the value of the #MxStackChild:fit property.
   // RETURNS: the current value of the #MxStackChild:fit property
   // <child>: A #ClutterActor
   int child_get_fit(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_stack_child_get_fit(&this, UpCast!(Clutter.Actor*)(child));
   }

   // VERSION: 1.2
   // Get the value of the #MxStackChild:x-align property
   // RETURNS: the current value of the "x-align" property
   // <child>: A #ClutterActor
   Align child_get_x_align(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_stack_child_get_x_align(&this, UpCast!(Clutter.Actor*)(child));
   }

   // VERSION: 1.2
   // Get the value of the #MxStackChild:x-fill property.
   // RETURNS: the current value of the "x-fill" property.
   // <child>: A #ClutterActor
   int child_get_x_fill(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_stack_child_get_x_fill(&this, UpCast!(Clutter.Actor*)(child));
   }

   // VERSION: 1.2
   // Get the value of the #MxStackChild:y-align property.
   // RETURNS: the current value of the "y-align" property.
   // <child>: A #ClutterActor
   Align child_get_y_align(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_stack_child_get_y_align(&this, UpCast!(Clutter.Actor*)(child));
   }

   // VERSION: 1.2
   // Get the value of the #MxStackChild:y-fill property
   // RETURNS: the current value of the "y-fill" property
   // <child>: A #ClutterActor
   int child_get_y_fill(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_stack_child_get_y_fill(&this, UpCast!(Clutter.Actor*)(child));
   }

   // VERSION: 1.4
   // Set the value of the #MxStackChild:crop property.
   // <child>: A #ClutterActor
   // <crop>: A #gboolean
   void child_set_crop(AT0)(AT0 /*Clutter.Actor*/ child, int crop) nothrow {
      mx_stack_child_set_crop(&this, UpCast!(Clutter.Actor*)(child), crop);
   }

   // VERSION: 1.2
   // Set the value of the #MxStackChild:fit property.
   // <child>: A #ClutterActor
   // <fit>: A #gboolean
   void child_set_fit(AT0)(AT0 /*Clutter.Actor*/ child, int fit) nothrow {
      mx_stack_child_set_fit(&this, UpCast!(Clutter.Actor*)(child), fit);
   }

   // VERSION: 1.2
   // Set the value of the #MxStackChild:x-align property.
   // <child>: A #ClutterActor
   // <x_align>: An #MxAlign
   void child_set_x_align(AT0)(AT0 /*Clutter.Actor*/ child, Align x_align) nothrow {
      mx_stack_child_set_x_align(&this, UpCast!(Clutter.Actor*)(child), x_align);
   }

   // VERSION: 1.2
   // Set the value of the #MxStackChild:x-fill property.
   // <child>: A #ClutterActor
   // <x_fill>: A #gboolean
   void child_set_x_fill(AT0)(AT0 /*Clutter.Actor*/ child, int x_fill) nothrow {
      mx_stack_child_set_x_fill(&this, UpCast!(Clutter.Actor*)(child), x_fill);
   }

   // VERSION: 1.2
   // Set the value of the #MxStackChild:y-align property.
   // <child>: A #ClutterActor
   // <y_align>: An #MxAlign
   void child_set_y_align(AT0)(AT0 /*Clutter.Actor*/ child, Align y_align) nothrow {
      mx_stack_child_set_y_align(&this, UpCast!(Clutter.Actor*)(child), y_align);
   }

   // VERSION: 1.2
   // Set the value of the #MxStackChild:y-fill property.
   // <child>: A #ClutterActor
   // <y_fill>: A #gboolean
   void child_set_y_fill(AT0)(AT0 /*Clutter.Actor*/ child, int y_fill) nothrow {
      mx_stack_child_set_y_fill(&this, UpCast!(Clutter.Actor*)(child), y_fill);
   }
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct StackChild /* : Clutter.ChildMeta */ {
   alias parent this;
   alias parent super_;
   alias parent childmeta;
   Clutter.ChildMeta parent;
    static import std.bitmanip; mixin(std.bitmanip.bitfields!(
   uint, "x_fill", 1,
   uint, "y_fill", 1,
   uint, "fit", 1,
   uint, "crop", 1,
    uint, "__dummy32A", 28));
   private Align x_align, y_align;
}

struct StackChildClass {
   Clutter.ChildMetaClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct StackChildPrivate {
}

struct StackClass {
   WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct StackPrivate {
}

// This is an opaque structure whose members cannot be directly accessed.
struct Stylable /* Interface */ {
   mixin template __interface__() {      void apply_clutter_text_attributes(AT0)(AT0 /*Clutter.Text*/ text) nothrow {
         mx_stylable_apply_clutter_text_attributes(cast(Stylable*)&this, UpCast!(Clutter.Text*)(text));
      }
      void connect_change_notifiers()() nothrow {
         mx_stylable_connect_change_notifiers(cast(Stylable*)&this);
      }

      // Finds the #GParamSpec installed by @stylable for the property
      // with @property_name.
      // 
      // or %NULL if no property with that name was found
      // RETURNS: a #GParamSpec for the given property,
      // <property_name>: the name of the property to find
      GObject2.ParamSpec* find_property(AT0)(AT0 /*char*/ property_name) nothrow {
         return mx_stylable_find_property(cast(Stylable*)&this, toCString!(char*)(property_name));
      }

      // Unintrospectable method: get() / mx_stylable_get()
      // Gets the style properties for @stylable.
      // 
      // In general, a copy is made of the property contents and the called
      // is responsible for freeing the memory in the appropriate manner for
      // the property type.
      // 
      // <example>
      // <title>Using mx_stylable_get(<!-- -->)</title>
      // <para>An example of using mx_stylable_get() to get the contents of
      // two style properties - one of type #G_TYPE_INT and one of type
      // #CLUTTER_TYPE_COLOR:</para>
      // <programlisting>
      // gint x_spacing;
      // ClutterColor *bg_color;
      // 
      // mx_stylable_get (stylable,
      // "x-spacing", &amp;x_spacing,
      // "bg-color", &amp;bg_color,
      // NULL);
      // 
      // /<!-- -->* do something with x_spacing and bg_color *<!-- -->/
      // 
      // clutter_color_free (bg_color);
      // </programlisting>
      // </example>
      // <first_property_name>: name of the first property to get
      /+ Not available -- variadic methods unsupported - use the C function directly.
         alias mx_stylable_get get; // Variadic
      +/

      // Query @stylable for the default value of property @property_name and
      // fill @value_out with the result.
      // 
      // been returned.
      // RETURNS: %TRUE if property @property_name exists and the default value has
      // <property_name>: name of the property to query
      // <value_out>: return location for the default value
      int get_default_value(AT0, AT1)(AT0 /*char*/ property_name, /*out*/ AT1 /*GObject2.Value*/ value_out) nothrow {
         return mx_stylable_get_default_value(cast(Stylable*)&this, toCString!(char*)(property_name), UpCast!(GObject2.Value*)(value_out));
      }

      // Retrieves the value of @property_name for @stylable, and puts it
      // into @value.
      // <property_name>: the name of the property
      // <value>: return location for an empty #GValue
      void get_property(AT0, AT1)(AT0 /*char*/ property_name, /*out*/ AT1 /*GObject2.Value*/ value) nothrow {
         mx_stylable_get_property(cast(Stylable*)&this, toCString!(char*)(property_name), UpCast!(GObject2.Value*)(value));
      }

      // Retrieves the #MxStyle used by @stylable. This function does not
      // alter the reference count of the returned object.
      // RETURNS: a #MxStyle
      Style* get_style()() nothrow {
         return mx_stylable_get_style(cast(Stylable*)&this);
      }

      // Get the current style class name
      // 
      // should not be modified or freed.
      // RETURNS: the class name string. The string is owned by the #MxWidget and
      char* get_style_class()() nothrow {
         return mx_stylable_get_style_class(cast(Stylable*)&this);
      }

      // Get the current style pseudo class. This can contain multiple pseudo class
      // names, separated by ':'.
      // 
      // should not be modified or freed.
      // RETURNS: the pseudo class string. The string is owned by the #MxWidget and
      char* get_style_pseudo_class()() nothrow {
         return mx_stylable_get_style_pseudo_class(cast(Stylable*)&this);
      }

      // Retrieves all the #GParamSpec<!-- -->s installed by @stylable.
      // 
      // of #GParamSpec<!-- -->s. Free it with  g_free() when done.
      // RETURNS: an array
      // <n_props>: return location for the number of properties, or %NULL
      GObject2.ParamSpec** /*new container*/ list_properties(AT0)(/*out*/ AT0 /*uint*/ n_props) nothrow {
         return mx_stylable_list_properties(cast(Stylable*)&this, UpCast!(uint*)(n_props));
      }

      // Sets @style as the new #MxStyle to be used by @stylable.
      // 
      // The #MxStylable will take ownership of the passed #MxStyle.
      // 
      // After the #MxStyle has been set, the MxStylable::style-set signal
      // will be emitted.
      // <style>: a #MxStyle
      void set_style(AT0)(AT0 /*Style*/ style) nothrow {
         mx_stylable_set_style(cast(Stylable*)&this, UpCast!(Style*)(style));
      }

      // Set the style class name
      // <style_class>: a new style class string
      void set_style_class(AT0)(AT0 /*char*/ style_class) nothrow {
         mx_stylable_set_style_class(cast(Stylable*)&this, toCString!(char*)(style_class));
      }

      // Set the style pseudo class. The string can contain multiple pseudo class
      // names, separated by ':'.
      // <pseudo_class>: a new pseudo class string
      void set_style_pseudo_class(AT0)(AT0 /*char*/ pseudo_class) nothrow {
         mx_stylable_set_style_pseudo_class(cast(Stylable*)&this, toCString!(char*)(pseudo_class));
      }

      // Emit the "style-changed" signal on @stylable to notify it that one or more
      // of the style properties has changed.
      // 
      // If @stylable is a #ClutterContainer then the "style-changed" notification is
      // propagated to it's children, since their style may depend on one or more
      // properties of the parent.
      // <flags>: flags that control the style changing
      void style_changed()(StyleChangedFlags flags) nothrow {
         mx_stylable_style_changed(cast(Stylable*)&this, flags);
      }

      // VERSION: 1.2
      // Add a pseudo-class name to the list of pseudo classes, contained in the
      // #MxStylable:style-pseudo-class property.
      // <new_class>: A pseudo-class name to add
      void style_pseudo_class_add(AT0)(AT0 /*char*/ new_class) nothrow {
         mx_stylable_style_pseudo_class_add(cast(Stylable*)&this, toCString!(char*)(new_class));
      }

      // VERSION: 1.2
      // Check if the given pseudo-class name is contained in the list of
      // set pseudo classes on this #MxStylable object.
      // RETURNS: %TRUE if the given pseudo-class is set, %FALSE otherwise
      // <pseudo_class>: A pseudo-class name
      int style_pseudo_class_contains(AT0)(AT0 /*char*/ pseudo_class) nothrow {
         return mx_stylable_style_pseudo_class_contains(cast(Stylable*)&this, toCString!(char*)(pseudo_class));
      }

      // VERSION: 1.2
      // Remove the specified pseudo class name from the list of pseudo classes
      // contained in the #MxStylable:style-pseudo-class property.
      // <remove_class>: A pseudo class name to remove
      void style_pseudo_class_remove(AT0)(AT0 /*char*/ remove_class) nothrow {
         mx_stylable_style_pseudo_class_remove(cast(Stylable*)&this, toCString!(char*)(remove_class));
      }

      // The ::style-changed signal is emitted each time one of the style
      // properties have changed.
      // <flags>: the #MxStyleChangedFlags associated with the signal
      extern (C) alias static void function (Stylable* this_, StyleChangedFlags* flags, void* user_data=null) nothrow signal_style_changed;

      ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
         return super_.signal_connect!name(cb, data, cf);
      }

      ulong signal_connect(string name:"style-changed", CB/*:signal_style_changed*/)
            (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
            if (is(typeof(cb)==signal_style_changed)||_ttmm!(CB, signal_style_changed)()) {
         return signal_connect_data!()(&this, cast(char*)"style-changed",
         cast(GObject2.Callback)cb, data, null, cf);
      }
   }
   mixin __interface__;
}

struct StylableIface {
   private GObject2.TypeInterface g_iface;
   // RETURNS: a #MxStyle
   extern (C) Style* function (Stylable* stylable) nothrow get_style;
   // <style>: a #MxStyle
   extern (C) void function (Stylable* stylable, Style* style) nothrow set_style;
   // RETURNS: the class name string. The string is owned by the #MxWidget and
   extern (C) char* function (Stylable* stylable) nothrow get_style_class;
   // <style_class>: a new style class string
   extern (C) void function (Stylable* stylable, char* style_class) nothrow set_style_class;
   // RETURNS: the pseudo class string. The string is owned by the #MxWidget and
   extern (C) char* function (Stylable* stylable) nothrow get_style_pseudo_class;
   // <pseudo_class>: a new pseudo class string
   extern (C) void function (Stylable* stylable, char* pseudo_class) nothrow set_style_pseudo_class;
   // <flags>: flags that control the style changing
   extern (C) void function (Stylable* stylable, StyleChangedFlags flags) nothrow style_changed;


   // Installs a property for @owner_type using @pspec as the property
   // description.
   // 
   // This function should be used inside the #MxStylableIface initialization
   // function of a class, for instance:
   // 
   // <informalexample><programlisting>
   // G_DEFINE_TYPE_WITH_CODE (FooActor, foo_actor, CLUTTER_TYPE_ACTOR,
   // G_IMPLEMENT_INTERFACE (MX_TYPE_STYLABLE,
   // mx_stylable_init));
   // ...
   // static void
   // mx_stylable_init (MxStylableIface *iface)
   // {
   // static gboolean is_initialized = FALSE;
   // 
   // if (!is_initialized)
   // {
   // ...
   // mx_stylable_iface_install_property (stylable,
   // FOO_TYPE_ACTOR,
   // g_param_spec_int ("x-spacing",
   // "X Spacing",
   // "Horizontal spacing",
   // -1, G_MAXINT,
   // 2,
   // G_PARAM_READWRITE));
   // ...
   // }
   // }
   // </programlisting></informalexample>
   // <owner_type>: #GType of the style property owner
   // <pspec>: a #GParamSpec
   void install_property(AT0)(Type owner_type, AT0 /*GObject2.ParamSpec*/ pspec) nothrow {
      mx_stylable_iface_install_property(&this, owner_type, UpCast!(GObject2.ParamSpec*)(pspec));
   }
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct Style /* : GObject.Object */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private StylePrivate* priv;


   // Creates a new #MxStyle object. This must be freed using #g_object_unref
   // when no longer required.
   // RETURNS: a newly allocated #MxStyle
   static Style* /*new*/ new_()() nothrow {
      return mx_style_new();
   }
   static auto opCall()() {
      return mx_style_new();
   }

   // Return the default MxStyle object. This includes the current theme (if
   // any).
   // 
   // unref'd by applications
   // RETURNS: a #MxStyle object. This must not be freed or
   static Style* get_default()() nothrow {
      return mx_style_get_default();
   }

   // Unintrospectable method: get() / mx_style_get()
   // Gets the style properties for @stylable from @style.
   // 
   // In general, a copy is made of the property contents and the caller
   // is responsible for freeing the memory in the appropriate manner for
   // the property type.
   // <stylable>: a #MxStylable
   // <first_property_name>: name of the first property to get
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias mx_style_get get; // Variadic
   +/

   // Requests the property described in @pspec for the specified stylable
   // <stylable>: a stylable to retreive the data for
   // <pspec>: a #GParamSpec describing the property required
   // <value>: a #GValue to place the return value in
   void get_property(AT0, AT1, AT2)(AT0 /*Stylable*/ stylable, AT1 /*GObject2.ParamSpec*/ pspec, /*out*/ AT2 /*GObject2.Value*/ value) nothrow {
      mx_style_get_property(&this, UpCast!(Stylable*)(stylable), UpCast!(GObject2.ParamSpec*)(pspec), UpCast!(GObject2.Value*)(value));
   }

   // Unintrospectable method: get_valist() / mx_style_get_valist()
   // Gets the style properties for @stylable from @style.
   // 
   // Please refer to mx_style_get() for further information.
   // <stylable>: a #MxStylable
   // <first_property_name>: name of the first property to get
   // <va_args>: return location for the first property, followed optionally by more name/return location pairs, followed by %NULL
   void get_valist(AT0, AT1)(AT0 /*Stylable*/ stylable, AT1 /*char*/ first_property_name, va_list va_args) nothrow {
      mx_style_get_valist(&this, UpCast!(Stylable*)(stylable), toCString!(char*)(first_property_name), va_args);
   }

   // Load style information from the specified file.
   // 
   // FALSE on error.
   // RETURNS: TRUE if the style information was loaded successfully. Returns
   // <filename>: filename of the style sheet to load
   int load_from_file(AT0, AT1)(AT0 /*char*/ filename, AT1 /*GLib2.Error**/ error=null) nothrow {
      return mx_style_load_from_file(&this, toCString!(char*)(filename), UpCast!(GLib2.Error**)(error));
   }

   // Indicates that the style data has changed in some way. For example, a new
   // stylesheet may have been loaded.
   extern (C) alias static void function (Style* this_, void* user_data=null) nothrow signal_changed;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"changed", CB/*:signal_changed*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_changed)||_ttmm!(CB, signal_changed)()) {
      return signal_connect_data!()(&this, cast(char*)"changed",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

enum StyleChangedFlags {
   NONE = 0,
   FORCE = 1,
   INVALIDATE_CACHE = 2
}
struct StyleClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function (Style* style) nothrow changed;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

enum StyleError {
   INVALID_FILE = 0
}
struct StylePrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct Table /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance widget;
   Widget parent_instance;
   private TablePrivate* priv;


   // Create a new #MxTable
   // RETURNS: a new #MxTable
   static Table* new_()() nothrow {
      return mx_table_new();
   }
   static auto opCall()() {
      return mx_table_new();
   }

   // Add an actor at the specified row and column
   // 
   // Note, column and rows numbers start from zero
   // <actor>: the child to insert
   // <row>: the row to place the child into
   // <column>: the column to place the child into
   void add_actor(AT0)(AT0 /*Clutter.Actor*/ actor, int row, int column) nothrow {
      mx_table_add_actor(&this, UpCast!(Clutter.Actor*)(actor), row, column);
   }

   // Unintrospectable method: add_actor_with_properties() / mx_table_add_actor_with_properties()
   // Add an actor into at the specified row and column, with additional child
   // properties to set.
   // <actor>: the child #ClutterActor
   // <row>: the row to place the child into
   // <column>: the column to place the child into
   // <first_property_name>: name of the first property to set
   /+ Not available -- variadic methods unsupported - use the C function directly.
      alias mx_table_add_actor_with_properties add_actor_with_properties; // Variadic
   +/

   // Get the column of the child.
   // RETURNS: the column of the child
   // <child>: a #ClutterActor
   int child_get_column(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_table_child_get_column(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the column span of the child. Defaults to 1.
   // RETURNS: the column span of the child
   // <child>: a #ClutterActor
   int child_get_column_span(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_table_child_get_column_span(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the row of the child.
   // RETURNS: the row of the child
   // <child>: a #ClutterActor
   int child_get_row(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_table_child_get_row(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the row span of the child. Defaults to 1.
   // RETURNS: the row span of the child
   // <child>: A #ClutterActor
   int child_get_row_span(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_table_child_get_row_span(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the x-align value of the child
   // RETURNS: An #MxAlign value
   // <child>: A #ClutterActor
   Align child_get_x_align(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_table_child_get_x_align(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the x-expand property of the child
   // RETURNS: #TRUE if the child is set to x-expand
   // <child>: A #ClutterActor
   int child_get_x_expand(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_table_child_get_x_expand(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the x-fill state of the child
   // RETURNS: #TRUE if the child is set to x-fill
   // <child>: A #ClutterActor
   int child_get_x_fill(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_table_child_get_x_fill(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the y-align value of the child
   // RETURNS: An #MxAlign value
   // <child>: A #ClutterActor
   Align child_get_y_align(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_table_child_get_y_align(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the y-expand property of the child.
   // RETURNS: #TRUE if the child is set to y-expand
   // <child>: A #ClutterActor
   int child_get_y_expand(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_table_child_get_y_expand(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Get the y-fill state of the child
   // RETURNS: #TRUE if the child is set to y-fill
   // <child>: A #ClutterActor
   int child_get_y_fill(AT0)(AT0 /*Clutter.Actor*/ child) nothrow {
      return mx_table_child_get_y_fill(&this, UpCast!(Clutter.Actor*)(child));
   }

   // Set the column of the child
   // <child>: a #ClutterActor
   // <col>: the column of the child
   void child_set_column(AT0)(AT0 /*Clutter.Actor*/ child, int col) nothrow {
      mx_table_child_set_column(&this, UpCast!(Clutter.Actor*)(child), col);
   }

   // Set the column span of the child.
   // <child>: An #ClutterActor
   // <span>: The number of columns to span
   void child_set_column_span(AT0)(AT0 /*Clutter.Actor*/ child, int span) nothrow {
      mx_table_child_set_column_span(&this, UpCast!(Clutter.Actor*)(child), span);
   }

   // Set the row of the child
   // <child>: a #ClutterActor
   // <row>: the row of the child
   void child_set_row(AT0)(AT0 /*Clutter.Actor*/ child, int row) nothrow {
      mx_table_child_set_row(&this, UpCast!(Clutter.Actor*)(child), row);
   }

   // Set the row span of the child.
   // <child>: A #ClutterActor
   // <span>: the number of rows to span
   void child_set_row_span(AT0)(AT0 /*Clutter.Actor*/ child, int span) nothrow {
      mx_table_child_set_row_span(&this, UpCast!(Clutter.Actor*)(child), span);
   }

   // Set the alignment of the child within its cell. This will only have an effect
   // if the the x-fill property is FALSE.
   // <child>: A #ClutterActor
   // <align>: A #MxAlign value
   void child_set_x_align(AT0)(AT0 /*Clutter.Actor*/ child, Align align_) nothrow {
      mx_table_child_set_x_align(&this, UpCast!(Clutter.Actor*)(child), align_);
   }

   // Set x-expand on the child. This causes the column which the child
   // resides in to be allocated any extra space if the allocation of the table is
   // larger than the preferred size.
   // <child>: A #ClutterActor
   // <expand>: the new value of the x expand child property
   void child_set_x_expand(AT0)(AT0 /*Clutter.Actor*/ child, int expand) nothrow {
      mx_table_child_set_x_expand(&this, UpCast!(Clutter.Actor*)(child), expand);
   }

   // Set the fill state of the child on the x-axis. This will cause the child to
   // be allocated the maximum available space.
   // <child>: A #ClutterActor
   // <fill>: the fill state
   void child_set_x_fill(AT0)(AT0 /*Clutter.Actor*/ child, int fill) nothrow {
      mx_table_child_set_x_fill(&this, UpCast!(Clutter.Actor*)(child), fill);
   }

   // Set the value of the y-align property. This will only have an effect if
   // y-fill value is set to FALSE.
   // <child>: A #ClutterActor
   // <align>: A #MxAlign value
   void child_set_y_align(AT0)(AT0 /*Clutter.Actor*/ child, Align align_) nothrow {
      mx_table_child_set_y_align(&this, UpCast!(Clutter.Actor*)(child), align_);
   }

   // Set y-expand on the child. This causes the row which the child
   // resides in to be allocated any extra space if the allocation of the table is
   // larger than the preferred size.
   // <child>: A #ClutterActor
   // <expand>: the new value of the y-expand child property
   void child_set_y_expand(AT0)(AT0 /*Clutter.Actor*/ child, int expand) nothrow {
      mx_table_child_set_y_expand(&this, UpCast!(Clutter.Actor*)(child), expand);
   }

   // Set the fill state of the child on the y-axis. This will cause the child to
   // be allocated the maximum available space.
   // <child>: A #ClutterActor
   // <fill>: the fill state
   void child_set_y_fill(AT0)(AT0 /*Clutter.Actor*/ child, int fill) nothrow {
      mx_table_child_set_y_fill(&this, UpCast!(Clutter.Actor*)(child), fill);
   }

   // Retrieve the current number of columns in @table
   // RETURNS: the number of columns
   int get_column_count()() nothrow {
      return mx_table_get_column_count(&this);
   }

   // Gets the amount of spacing between columns.
   // RETURNS: the spacing between columns in device units
   int get_column_spacing()() nothrow {
      return mx_table_get_column_spacing(&this);
   }

   // Retrieve the current number rows in the @table
   // RETURNS: the number of rows
   int get_row_count()() nothrow {
      return mx_table_get_row_count(&this);
   }

   // Gets the amount of spacing between rows.
   // RETURNS: the spacing between rows in device units
   int get_row_spacing()() nothrow {
      return mx_table_get_row_spacing(&this);
   }

   // Sets the amount of spacing between columns.
   // <spacing>: spacing in pixels
   void set_column_spacing()(int spacing) nothrow {
      mx_table_set_column_spacing(&this, spacing);
   }

   // Sets the amount of spacing between rows.
   // <spacing>: spacing in pixels
   void set_row_spacing()(int spacing) nothrow {
      mx_table_set_row_spacing(&this, spacing);
   }
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct TableChild /* : Clutter.ChildMeta */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent childmeta;
   Clutter.ChildMeta method_parent;
}

struct TableChildClass {
   Clutter.ChildMetaClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct TableClass {
   WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct TablePrivate {
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct TextureCache /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;


   // Returns the default texture cache. This is owned by Mx and should not be
   // unreferenced or freed.
   // RETURNS: a MxTextureCache
   static TextureCache* get_default()() nothrow {
      return mx_texture_cache_get_default();
   }

   // VERSION: 1.2
   // Checks whether the given URI/path is contained within the texture
   // cache.
   // RETURNS: %TRUE if the image exists, %FALSE otherwise
   // <uri>: A URI or path to an image file
   int contains(AT0)(AT0 /*char*/ uri) nothrow {
      return mx_texture_cache_contains(&this, toCString!(char*)(uri));
   }

   // VERSION: 1.2
   // Checks whether there are any textures associated with the given URI by
   // the given identifier.
   // RETURNS: %TRUE if the data exists, %FALSE otherwise
   // <uri>: A URI or path to an image file
   // <ident>: A unique identifier
   int contains_meta(AT0, AT1)(AT0 /*char*/ uri, AT1 /*void*/ ident) nothrow {
      return mx_texture_cache_contains_meta(&this, toCString!(char*)(uri), UpCast!(void*)(ident));
   }

   // This is a wrapper around mx_texture_cache_get_texture() which returns
   // a ClutterActor.
   // RETURNS: a newly created ClutterTexture
   // <uri>: A URI or path to a image file
   Clutter.Actor* get_actor(AT0)(AT0 /*char*/ uri) nothrow {
      return mx_texture_cache_get_actor(&this, toCString!(char*)(uri));
   }

   // Create a #CoglHandle representing a texture of the specified image. Adds
   // the image to the cache if the image had not been previously loaded.
   // Subsequent calls with the same image URI/path will return the #CoglHandle of
   // the previously loaded image with an increased reference count.
   // RETURNS: a #CoglHandle to the cached texture
   // <uri>: A URI or path to an image file
   Cogl.Handle get_cogl_texture(AT0)(AT0 /*char*/ uri) nothrow {
      return mx_texture_cache_get_cogl_texture(&this, toCString!(char*)(uri));
   }

   // VERSION: 1.2
   // Retrieves the #CoglHandle of the previously added image associated
   // with the given unique identifier.
   // 
   // See mx_texture_cache_insert_meta()
   // 
   // reference. %NULL if no image was found.
   // RETURNS: A #CoglHandle to a texture, with an added
   // <uri>: A URI or path to an image file
   // <ident>: A unique identifier
   Cogl.Handle /*new*/ get_meta_cogl_texture(AT0, AT1)(AT0 /*char*/ uri, AT1 /*void*/ ident) nothrow {
      return mx_texture_cache_get_meta_cogl_texture(&this, toCString!(char*)(uri), UpCast!(void*)(ident));
   }

   // VERSION: 1.2
   // Create a new ClutterTexture using the previously added image associated
   // with the given unique identifier.
   // 
   // See mx_texture_cache_insert_meta()
   // 
   // %NULL if no image was found
   // RETURNS: A newly allocated #ClutterTexture, or
   // <uri>: A URI or path to an image file
   // <ident>: A unique identifier
   Clutter.Texture* /*new*/ get_meta_texture(AT0, AT1)(AT0 /*char*/ uri, AT1 /*void*/ ident) nothrow {
      return mx_texture_cache_get_meta_texture(&this, toCString!(char*)(uri), UpCast!(void*)(ident));
   }

   // Returns the number of items in the texture cache
   // RETURNS: the current size of the cache
   int get_size()() nothrow {
      return mx_texture_cache_get_size(&this);
   }

   // Create a new ClutterTexture with the specified image. Adds the image to the
   // cache if the image had not been previously loaded. Subsequent calls with
   // the same image URI/path will return a new ClutterTexture with the previously
   // loaded image.
   // RETURNS: a newly created ClutterTexture
   // <uri>: A URI or path to a image file
   Clutter.Texture* get_texture(AT0)(AT0 /*char*/ uri) nothrow {
      return mx_texture_cache_get_texture(&this, toCString!(char*)(uri));
   }

   // VERSION: 1.2
   // Inserts a texture into the texture cache. This can be useful if you
   // want to cache a texture from a custom or unhandled URI type, or you
   // want to override a particular texture.
   // 
   // If the image is already in the cache, this texture will replace it. A
   // reference will be taken on the given texture.
   // <uri>: A URI or local file path
   // <texture>: A #CoglHandle to a texture
   void insert(AT0, AT1)(AT0 /*char*/ uri, AT1 /*Cogl.Handle*/ texture) nothrow {
      mx_texture_cache_insert(&this, toCString!(char*)(uri), UpCast!(Cogl.Handle*)(texture));
   }

   // VERSION: 1.2
   // Inserts a texture that's associated with a URI into the cache.
   // If the metadata already exists for this URI, it will be replaced.
   // 
   // This is useful if you have a widely used modification of an image,
   // for example, an image with a border composited around it.
   // <uri>: A URI or local file path
   // <ident>: A unique identifier
   // <texture>: A #CoglHandle to a texture
   // <destroy_func>: An optional destruction function for @ident
   void insert_meta(AT0, AT1, AT2)(AT0 /*char*/ uri, AT1 /*void*/ ident, AT2 /*Cogl.Handle*/ texture, GLib2.DestroyNotify destroy_func) nothrow {
      mx_texture_cache_insert_meta(&this, toCString!(char*)(uri), UpCast!(void*)(ident), UpCast!(Cogl.Handle*)(texture), destroy_func);
   }
   void load_cache(AT0)(AT0 /*char*/ filename) nothrow {
      mx_texture_cache_load_cache(&this, toCString!(char*)(filename));
   }
}

struct TextureCacheClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function (TextureCache* self, char* uri, Clutter.Texture* texture) nothrow loaded;
   extern (C) void function (TextureCache* self, GLib2.Error* error) nothrow error_loading;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct TextureFrame /* : Clutter.Actor */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance actor;
   Clutter.Actor parent_instance;
   private TextureFramePrivate* priv;


   // A #MxTextureFrame is a specialized texture that efficiently clones
   // an area of the given @texture while keeping preserving portions of the
   // same texture.
   // 
   // A #MxTextureFrame can be used to make a rectangular texture fit a
   // given size without stretching its borders.
   // RETURNS: the newly created #MxTextureFrame
   // <texture>: a #ClutterTexture or %NULL
   // <top>: top margin preserving its content
   // <right>: right margin preserving its content
   // <bottom>: bottom margin preserving its content
   // <left>: left margin preserving its content
   static TextureFrame* new_(AT0)(AT0 /*Clutter.Texture*/ texture, float top, float right, float bottom, float left) nothrow {
      return mx_texture_frame_new(UpCast!(Clutter.Texture*)(texture), top, right, bottom, left);
   }
   static auto opCall(AT0)(AT0 /*Clutter.Texture*/ texture, float top, float right, float bottom, float left) {
      return mx_texture_frame_new(UpCast!(Clutter.Texture*)(texture), top, right, bottom, left);
   }

   // Retrieve the current slice lines from the specified frame.
   // <top>: width of the top slice
   // <right>: width of the right slice
   // <bottom>: width of the bottom slice
   // <left>: width of the left slice
   void get_border_values(AT0, AT1, AT2, AT3)(AT0 /*float*/ top, AT1 /*float*/ right, AT2 /*float*/ bottom, AT3 /*float*/ left) nothrow {
      mx_texture_frame_get_border_values(&this, UpCast!(float*)(top), UpCast!(float*)(right), UpCast!(float*)(bottom), UpCast!(float*)(left));
   }

   // Return the texture used by the #MxTextureFrame
   // RETURNS: a #ClutterTexture owned by the #MxTextureFrame
   Clutter.Texture* get_parent_texture()() nothrow {
      return mx_texture_frame_get_parent_texture(&this);
   }

   // Set the slice lines of the specified frame. The slices are calculated as
   // widths from the edge of the frame.
   // <top>: width of the top slice
   // <right>: width of the right slice
   // <bottom>: width of the bottom slice
   // <left>: width of the left slice
   void set_border_values()(float top, float right, float bottom, float left) nothrow {
      mx_texture_frame_set_border_values(&this, top, right, bottom, left);
   }

   // Set the #ClutterTexture used by this #MxTextureFrame
   // <texture>: A #ClutterTexture
   void set_parent_texture(AT0)(AT0 /*Clutter.Texture*/ texture) nothrow {
      mx_texture_frame_set_parent_texture(&this, UpCast!(Clutter.Texture*)(texture));
   }
}

struct TextureFrameClass {
   Clutter.ActorClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct TextureFramePrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct Toggle /* : Widget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent widget;
   Widget parent;
   TogglePrivate* priv;

   static Toggle* new_()() nothrow {
      return mx_toggle_new();
   }
   static auto opCall()() {
      return mx_toggle_new();
   }
   int get_active()() nothrow {
      return mx_toggle_get_active(&this);
   }
   void set_active()(int active) nothrow {
      mx_toggle_set_active(&this, active);
   }
}

struct ToggleClass {
   WidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct TogglePrivate {
}


// The contents of this structure are private and should only be accessed
// through the public API.
struct Toolbar /* : Bin */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent bin;
   Bin parent;
   ToolbarPrivate* priv;


   // Create a new #MxToolbar. This is not normally necessary if using #MxWindow,
   // where #mx_window_get_toolbar should be used to retrieve the toolbar instead.
   // RETURNS: A newly allocated #MxToolbar
   static Toolbar* new_()() nothrow {
      return mx_toolbar_new();
   }
   static auto opCall()() {
      return mx_toolbar_new();
   }

   // Get the value of the #MxToolbar:has-close-button property.
   // RETURNS: the current value of the "hast-close-button" property.
   int get_has_close_button()() nothrow {
      return mx_toolbar_get_has_close_button(&this);
   }

   // Set the #MxToolbar:has-close-button property
   // <has_close_button>: #TRUE if a close button should be displayed
   void set_has_close_button()(int has_close_button) nothrow {
      mx_toolbar_set_has_close_button(&this, has_close_button);
   }

   // Emitted when the close button of the toolbar is clicked.
   // 
   // Normally, the parent stage will be closed when the close button is
   // clicked. Return #TRUE from this handler to prevent the stage from being
   // destroyed.
   // RETURNS: #TRUE to prevent the parent stage being destroyed.
   extern (C) alias static c_int function (Toolbar* this_, void* user_data=null) nothrow signal_close_button_clicked;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"close-button-clicked", CB/*:signal_close_button_clicked*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_close_button_clicked)||_ttmm!(CB, signal_close_button_clicked)()) {
      return signal_connect_data!()(&this, cast(char*)"close-button-clicked",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct ToolbarClass {
   BinClass parent_class;
   extern (C) int function (Toolbar* toolbar) nothrow close_button_clicked;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct ToolbarPrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct Tooltip /* : FloatingWidget */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Stylable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance floatingwidget;
   FloatingWidget parent_instance;
   private TooltipPrivate* priv;


   // VERSION: 1.2
   // Browse mode is entered whenever a tooltip is displayed and it is
   // left after a short delay when a tooltip is hidden. This is used to
   // make tooltips display quicker when a previous tooltip is already
   // displayed.
   // 
   // otherwise.
   // RETURNS: %TRUE if the app is in tooltip browse mode or %FALSE
   static int is_in_browse_mode()() nothrow {
      return mx_tooltip_is_in_browse_mode();
   }

   // Get the text displayed on the tooltip
   // RETURNS: the text for the tooltip. This must not be freed by the application
   char* get_text()() nothrow {
      return mx_tooltip_get_text(&this);
   }

   // Retrieve the area on the stage that the tooltip currently applies to
   // 
   // by the application.
   // RETURNS: the #ClutterGeometry, owned by the tooltip which must not be freed
   Clutter.Geometry* get_tip_area()() nothrow {
      return mx_tooltip_get_tip_area(&this);
   }
   // Hide the tooltip
   void hide()() nothrow {
      mx_tooltip_hide(&this);
   }

   // Sets the text displayed on the tooltip
   // <text>: text to set the label to
   void set_text(AT0)(AT0 /*char*/ text) nothrow {
      mx_tooltip_set_text(&this, toCString!(char*)(text));
   }

   // Set the area on the stage that the tooltip applies to.
   // <area>: A #ClutterGeometry
   void set_tip_area(AT0)(AT0 /*Clutter.Geometry*/ area) nothrow {
      mx_tooltip_set_tip_area(&this, UpCast!(Clutter.Geometry*)(area));
   }

   // Utility function to set the geometry of the tooltip area
   // from an existing actor.
   // See also mx_tooltip_set_tip_area
   // <actor>: A #ClutterActor
   void set_tip_area_from_actor(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
      mx_tooltip_set_tip_area_from_actor(&this, UpCast!(Clutter.Actor*)(actor));
   }
   // Show the tooltip relative to the associated widget.
   void show()() nothrow {
      mx_tooltip_show(&this);
   }
}

// Defines the animation when tooltips are shown and hidden.
enum TooltipAnimation /* Version 1.2 */ {
   BOUNCE = 0,
   FADE = 1
}
struct TooltipClass {
   FloatingWidgetClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct TooltipPrivate {
}

enum int VERSION_HEX = 0;
enum VERSION_S = "1.99.2";

// The contents of this structure are private and should only be accessed
// through the public API.
struct Viewport /* : Bin */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Focusable.__interface__;
   mixin Scrollable.__interface__;
   mixin Stylable.__interface__;
   alias parent this;
   alias parent super_;
   alias parent bin;
   Bin parent;
   private ViewportPrivate* priv;

   static Viewport* new_()() nothrow {
      return mx_viewport_new();
   }
   static auto opCall()() {
      return mx_viewport_new();
   }
   void get_origin(AT0, AT1, AT2)(AT0 /*float*/ x, AT1 /*float*/ y, AT2 /*float*/ z) nothrow {
      mx_viewport_get_origin(&this, UpCast!(float*)(x), UpCast!(float*)(y), UpCast!(float*)(z));
   }
   int get_sync_adjustments()() nothrow {
      return mx_viewport_get_sync_adjustments(&this);
   }
   void set_origin()(float x, float y, float z) nothrow {
      mx_viewport_set_origin(&this, x, y, z);
   }
   void set_sync_adjustments()(int sync) nothrow {
      mx_viewport_set_sync_adjustments(&this, sync);
   }
}

struct ViewportClass {
   BinClass parent_class;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct ViewportPrivate {
}


// Base class for stylable actors. The contents of the #MxWidget
// structure are private and should only be accessed through the
// public API.
struct Widget /* : Clutter.Actor */ {
   mixin Atk.ImplementorIface.__interface__;
   mixin Clutter.Animatable.__interface__;
   mixin Clutter.Container.__interface__;
   mixin Clutter.Scriptable.__interface__;
   mixin Stylable.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance actor;
   Clutter.Actor parent_instance;
   private WidgetPrivate* priv;


   // VERSION: 1.2
   // Used to implement how a new style instance should be applied in the widget.
   // For instance, setting style instance on stylable internal children.
   // <style>: A #MxStyle
   void apply_style(AT0)(AT0 /*Style*/ style) nothrow {
      mx_widget_apply_style(&this, UpCast!(Style*)(style));
   }

   // Copies @allocation into @area and accounts for the padding values. This
   // gives the area that is available in which to allocate children with respect
   // to padding.
   // <allocation>: A #ClutterActorBox
   // <area>: A #ClutterActorBox
   void get_available_area(AT0, AT1)(AT0 /*Clutter.ActorBox*/ allocation, AT1 /*Clutter.ActorBox*/ area) nothrow {
      mx_widget_get_available_area(&this, UpCast!(Clutter.ActorBox*)(allocation), UpCast!(Clutter.ActorBox*)(area));
   }

   // Get the texture used as the background image. This is set using the
   // "background-image" CSS property. This function should normally only be used
   // by subclasses.
   // RETURNS: a #ClutterActor
   Clutter.Actor* get_background_image()() nothrow {
      return mx_widget_get_background_image(&this);
   }

   // Get the texture used as the border image. This is set using the
   // "border-image" CSS property. This function should normally only be used
   // by subclasses.
   // RETURNS: #ClutterActor
   Clutter.Actor* get_border_image()() nothrow {
      return mx_widget_get_border_image(&this);
   }
   // Get the value of the "disabled" property.
   int get_disabled()() nothrow {
      return mx_widget_get_disabled(&this);
   }

   // Get the object in the #MxWidget:menu property.
   // RETURNS: The current object in the "menu" property.
   Menu* get_menu()() nothrow {
      return mx_widget_get_menu(&this);
   }

   // Gets the padding of the widget, set using the "padding" CSS property. This
   // function should normally only be used by subclasses.
   // <padding>: A pointer to an #MxPadding to fill
   void get_padding(AT0)(AT0 /*Padding*/ padding) nothrow {
      mx_widget_get_padding(&this, UpCast!(Padding*)(padding));
   }

   // Get the value of the "tooltip-delay" property.
   // RETURNS: the current delay value in milliseconds
   uint get_tooltip_delay()() nothrow {
      return mx_widget_get_tooltip_delay(&this);
   }

   // Get the current tooltip string
   // RETURNS: The current tooltip string, owned by the #MxWidget
   char* get_tooltip_text()() nothrow {
      return mx_widget_get_tooltip_text(&this);
   }
   // Hide the tooltip for @widget
   void hide_tooltip()() nothrow {
      mx_widget_hide_tooltip(&this);
   }

   // Cancel a long-press timeout if one is running and emit the signal to notify
   // that the long-press has been cancelled.
   void long_press_cancel()() nothrow {
      mx_widget_long_press_cancel(&this);
   }

   // Emit the long-press query signal and start a long-press timeout if required.
   // <event>: the event used to determine whether to run a long-press
   void long_press_query(AT0)(AT0 /*Clutter.ButtonEvent*/ event) nothrow {
      mx_widget_long_press_query(&this, UpCast!(Clutter.ButtonEvent*)(event));
   }

   // Invokes #MxWidget::paint_background() using the default background
   // image and/or color from the @widget style
   // 
   // This function should be used by subclasses of #MxWidget that override
   // the paint() virtual function and cannot chain up
   void paint_background()() nothrow {
      mx_widget_paint_background(&this);
   }

   // Set the disabled property. Disabled widgets have a "disabled" pseudo-class
   // until disabled is set to #FALSE.
   // <disabled>: value to set
   void set_disabled()(int disabled) nothrow {
      mx_widget_set_disabled(&this, disabled);
   }

   // Set the value of the #MxWidget:menu property.
   // <menu>: A #MxMenu
   void set_menu(AT0)(AT0 /*Menu*/ menu) nothrow {
      mx_widget_set_menu(&this, UpCast!(Menu*)(menu));
   }

   // Set the value, in milliseconds, of the "tooltip-delay" property.
   // This is initially set to MX_WIDGET_TOOLTIP_TIMEOUT.
   void set_tooltip_delay()(uint delay) nothrow {
      mx_widget_set_tooltip_delay(&this, delay);
   }

   // Set the tooltip text of the widget. Note that setting tooltip text will cause
   // the widget to be set reactive. If you no longer need tooltips and you do not
   // need the widget to be reactive, you must set ClutterActor::reactive to
   // %FALSE.
   // <text>: text to set as the tooltip
   void set_tooltip_text(AT0)(AT0 /*char*/ text) nothrow {
      mx_widget_set_tooltip_text(&this, toCString!(char*)(text));
   }
   // Show the tooltip for @widget
   void show_tooltip()() nothrow {
      mx_widget_show_tooltip(&this);
   }
   // Emitted when the user holds a mouse button down for a longer period.
   extern (C) alias static c_int function (Widget* this_, float object, float p0, LongPressAction* p1, void* user_data=null) nothrow signal_long_press;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"long-press", CB/*:signal_long_press*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_long_press)||_ttmm!(CB, signal_long_press)()) {
      return signal_connect_data!()(&this, cast(char*)"long-press",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

// Base class for stylable actors.
struct WidgetClass {
   private Clutter.ActorClass parent_class;
   extern (C) void function (Widget* self, Clutter.Actor* background, Clutter.Color* color) nothrow paint_background;
   extern (C) int function (Widget* widget, LongPressAction action, float x, float y) nothrow long_press;
   // <style>: A #MxStyle
   extern (C) void function (Widget* widget, Style* style) nothrow apply_style;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
   extern (C) void function () nothrow _padding_5;
   extern (C) void function () nothrow _padding_6;
   extern (C) void function () nothrow _padding_7;
   extern (C) void function () nothrow _padding_8;
}

struct WidgetPrivate {
}


// The contents of this structure is private and should only be accessed using
// the provided API.
struct Window /* : GObject.Object */ {
   alias parent this;
   alias parent super_;
   alias parent object;
   GObject2.Object parent;
   WindowPrivate* priv;


   // Creates a new #MxWindow.
   // RETURNS: A #MxWindow
   static Window* /*new*/ new_()() nothrow {
      return mx_window_new();
   }
   static auto opCall()() {
      return mx_window_new();
   }

   // Creates a new #MxWindow, using @stage as the backing #ClutterStage. This
   // function is meant for use primarily for embedding a #MxWindow into
   // a foreign stage when using a Clutter toolkit integration library.
   // RETURNS: A #MxWindow
   // <stage>: A #ClutterStage
   static Window* /*new*/ new_with_clutter_stage(AT0)(AT0 /*Clutter.Stage*/ stage) nothrow {
      return mx_window_new_with_clutter_stage(UpCast!(Clutter.Stage*)(stage));
   }
   static auto opCall(AT0)(AT0 /*Clutter.Stage*/ stage) {
      return mx_window_new_with_clutter_stage(UpCast!(Clutter.Stage*)(stage));
   }

   // Gets the #MxWindow parent of the #ClutterStage, if it exists.
   // RETURNS: A #MxWindow, or %NULL
   // <stage>: A #ClutterStage
   static Window* get_for_stage(AT0)(AT0 /*Clutter.Stage*/ stage) nothrow {
      return mx_window_get_for_stage(UpCast!(Clutter.Stage*)(stage));
   }

   // Get the primary child of the window. See mx_window_set_child().
   // RETURNS: A #ClutterActor, or %NULL
   Clutter.Actor* get_child()() nothrow {
      return mx_window_get_child(&this);
   }

   // Gets the #ClutterStage managed by the window.
   // RETURNS: A #ClutterStage
   Clutter.Stage* get_clutter_stage()() nothrow {
      return mx_window_get_clutter_stage(&this);
   }

   // VERSION: 1.2
   // Determines if the window has been set to be in fullscreen mode.
   // 
   // otherwise %FALSE
   // RETURNS: %TRUE if the window has been set to be in fullscreen mode,
   int get_fullscreen()() nothrow {
      return mx_window_get_fullscreen(&this);
   }

   // Determines whether the window has a toolbar or not.
   // See mx_window_set_has_toolbar().
   // RETURNS: %TRUE if the window has a toolbar, otherwise %FALSE
   int get_has_toolbar()() nothrow {
      return mx_window_get_has_toolbar(&this);
   }

   // Gets the currently set window icon name. This will be %NULL if there is none
   // set, or the icon was set with mx_window_set_icon_from_cogl_texture().
   // RETURNS: The window icon name, or %NULL
   char* get_icon_name()() nothrow {
      return mx_window_get_icon_name(&this);
   }

   // Determines if the window is in small-screen mode.
   // See mx_window_set_small_screen().
   // RETURNS: %TRUE if the window is in small-screen mode, otherwise %FALSE
   int get_small_screen()() nothrow {
      return mx_window_get_small_screen(&this);
   }

   // VERSION: 1.2
   // Retrieves the title used for the window.
   // RETURNS: The title used for the window
   char* get_title()() nothrow {
      return mx_window_get_title(&this);
   }

   // Retrieves the toolbar associated with the window.
   // RETURNS: A #MxToolbar
   Toolbar* get_toolbar()() nothrow {
      return mx_window_get_toolbar(&this);
   }

   // Retrieves the absolute position of the window on the screen.
   // <x>: A pointer for the x-coordinate
   // <y>: A pointer for the y-coordinate
   void get_window_position()(/*out*/ int* x, /*out*/ int* y) nothrow {
      mx_window_get_window_position(&this, x, y);
   }

   // VERSION: 1.2
   // Retrieve the rotation of the window.
   // RETURNS: An #MxWindowRotation
   WindowRotation get_window_rotation()() nothrow {
      return mx_window_get_window_rotation(&this);
   }

   // VERSION: 1.2
   // Retrieves the size of the display area of the window, taking into
   // account any window border. This includes the area occupied by the
   // window's toolbar, if it's enabled.
   // <width>: A #gint pointer for the window's width
   // <height>: A #gint pointer for the window's height
   void get_window_size()(/*out*/ int* width, /*out*/ int* height) nothrow {
      mx_window_get_window_size(&this, width, height);
   }

   // VERSION: 1.2
   // Hide the window
   void hide()() nothrow {
      mx_window_hide(&this);
   }

   // VERSION: 1.2
   // Present the window. The actual behaviour is specific to the window system.
   void present()() nothrow {
      mx_window_present(&this);
   }

   // Adds @actor to the window and sets it as the primary child. When the
   // stage managed in the window changes size, the child will be resized
   // to match it.
   // <actor>: A #ClutterActor
   void set_child(AT0)(AT0 /*Clutter.Actor*/ actor) nothrow {
      mx_window_set_child(&this, UpCast!(Clutter.Actor*)(actor));
   }

   // VERSION: 1.2
   // Set the window to be in fullscreen mode or windowed mode.
   // 
   // <note><para>
   // Setting fullscreen mode doesn't necessarily mean the window is actually
   // fullscreen. Setting this property is only a request to the underlying
   // window system.
   // </para></note>
   // <fullscreen>: %TRUE to request fullscreen mode, %FALSE to disable
   void set_fullscreen()(int fullscreen) nothrow {
      mx_window_set_fullscreen(&this, fullscreen);
   }

   // Sets whether the window has a toolbar or not. If the window has a toolbar,
   // client-side window decorations will be enabled.
   // <toolbar>: %TRUE if the toolbar should be displayed
   void set_has_toolbar()(int toolbar) nothrow {
      mx_window_set_has_toolbar(&this, toolbar);
   }

   // Sets the window icon from a texture. This will take precedence over
   // any currently set icon-name.
   // <texture>: A #CoglHandle for a texture
   void set_icon_from_cogl_texture()(Cogl.Handle texture) nothrow {
      mx_window_set_icon_from_cogl_texture(&this, texture);
   }

   // Set an icon-name to use for the window icon. The icon will be looked up
   // from the default theme.
   // <icon_name>: An icon name, or %NULL
   void set_icon_name(AT0)(AT0 /*char*/ icon_name=null) nothrow {
      mx_window_set_icon_name(&this, toCString!(char*)(icon_name));
   }

   // Enables or disables small-screen mode. This mode is meant primarily
   // for platforms with limited screen-space, such as netbooks. When enabled,
   // the window will take up all available room and will disable moving and
   // resizing.
   // <small_screen>: %TRUE if small-screen mode should be enabled
   void set_small_screen()(int small_screen) nothrow {
      mx_window_set_small_screen(&this, small_screen);
   }

   // VERSION: 1.2
   // Sets the title used for the window, the results of which are
   // window-system specific.
   // <title>: A string to use for the window title name
   void set_title(AT0)(AT0 /*char*/ title) nothrow {
      mx_window_set_title(&this, toCString!(char*)(title));
   }

   // VERSION: 1.2
   // Sets the toolbar associated with the window.
   void set_toolbar(AT0)(AT0 /*Toolbar*/ toolbar) nothrow {
      mx_window_set_toolbar(&this, UpCast!(Toolbar*)(toolbar));
   }

   // Sets the absolute position of the window on the screen.
   // <x>: An x-coordinate
   // <y>: A y-coordinate
   void set_window_position()(int x, int y) nothrow {
      mx_window_set_window_position(&this, x, y);
   }

   // VERSION: 1.2
   // Set the rotation of the window.
   // <rotation>: The #MxWindowRotation
   void set_window_rotation()(WindowRotation rotation) nothrow {
      mx_window_set_window_rotation(&this, rotation);
   }

   // VERSION: 1.2
   // Sets the size of the window, taking into account any window border. This
   // corresponds to the window's available area for its child, minus the area
   // occupied by the window's toolbar, if it's enabled.
   // 
   // <note><para>
   // Setting the window size may involve a request to the underlying windowing
   // system, and may not immediately be reflected.
   // </para></note>
   // <width>: A width, in pixels
   // <height>: A height, in pixels
   void set_window_size()(int width, int height) nothrow {
      mx_window_set_window_size(&this, width, height);
   }

   // VERSION: 1.2
   // Show the window
   void show()() nothrow {
      mx_window_show(&this);
   }
   // Emitted when the stage managed by the window is destroyed.
   extern (C) alias static void function (Window* this_, void* user_data=null) nothrow signal_destroy;

   ulong signal_connect(string name, CB)(CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0) {
      return super_.signal_connect!name(cb, data, cf);
   }

   ulong signal_connect(string name:"destroy", CB/*:signal_destroy*/)
         (CB cb, void* data=null, ConnectFlags cf=cast(ConnectFlags)0)
         if (is(typeof(cb)==signal_destroy)||_ttmm!(CB, signal_destroy)()) {
      return signal_connect_data!()(&this, cast(char*)"destroy",
      cast(GObject2.Callback)cb, data, null, cf);
   }
}

struct WindowClass {
   GObject2.ObjectClass parent_class;
   extern (C) void function (Window* window) nothrow destroy;
   extern (C) void function () nothrow _padding_0;
   extern (C) void function () nothrow _padding_1;
   extern (C) void function () nothrow _padding_2;
   extern (C) void function () nothrow _padding_3;
   extern (C) void function () nothrow _padding_4;
}

struct WindowPrivate {
}

// Defines the clock-wise rotation angle of a window.
enum WindowRotation /* Version 1.2 */ {
   _0 = 0,
   _90 = 1,
   _180 = 2,
   _270 = 3
}
static void actor_box_clamp_to_pixels(AT0)(AT0 /*Clutter.ActorBox*/ box) nothrow {
   mx_actor_box_clamp_to_pixels(UpCast!(Clutter.ActorBox*)(box));
}

static void allocate_align_fill(AT0, AT1)(AT0 /*Clutter.Actor*/ child, AT1 /*Clutter.ActorBox*/ childbox, Align x_alignment, Align y_alignment, int x_fill, int y_fill) nothrow {
   mx_allocate_align_fill(UpCast!(Clutter.Actor*)(child), UpCast!(Clutter.ActorBox*)(childbox), x_alignment, y_alignment, x_fill, y_fill);
}

// MOVED TO: BorderImage.set_from_string
static void border_image_set_from_string(AT0, AT1, AT2)(AT0 /*GObject2.Value*/ value, AT1 /*char*/ str, AT2 /*char*/ filename) nothrow {
   mx_border_image_set_from_string(UpCast!(GObject2.Value*)(value), toCString!(char*)(str), toCString!(char*)(filename));
}


// VERSION: 1.2
// MOVED TO: FocusHint.from_direction
// Transforms a focus direction to a focus hint. This is a convenience
// function for actors that implement the #MxFocusable interface, to
// pass the correct #MxFocusHint to their children when calling
// mx_focusable_accept_focus().
// 
// %MX_FOCUS_DIRECTION_UP maps to %MX_FOCUS_HINT_FROM_BELOW,
// %MX_FOCUS_DIRECTION_DOWN maps to %MX_FOCUS_HINT_FROM_ABOVE,
// %MX_FOCUS_DIRECTION_LEFT maps to %MX_FOCUS_HINT_FROM_RIGHT,
// %MX_FOCUS_DIRECTION_RIGHT maps to %MX_FOCUS_HINT_FROM_LEFT,
// %MX_FOCUS_DIRECTION_NEXT maps to %MX_FOCUS_HINT_FIRST,
// %MX_FOCUS_DIRECTION_PREVIOUS maps to %MX_FOCUS_HINT_LAST and
// anything else maps to %MX_FOCUS_HINT_PRIOR.
// RETURNS: A #MxFocusHint
// <direction>: A #MxFocusDirection
static FocusHint focus_hint_from_direction()(FocusDirection direction) nothrow {
   return mx_focus_hint_from_direction(direction);
}

// MOVED TO: FontWeight.set_from_string
static void font_weight_set_from_string(AT0, AT1)(AT0 /*GObject2.Value*/ value, AT1 /*char*/ str) nothrow {
   mx_font_weight_set_from_string(UpCast!(GObject2.Value*)(value), toCString!(char*)(str));
}

// MOVED TO: ImageError.quark
static GLib2.Quark image_error_quark()() nothrow {
   return mx_image_error_quark();
}


// Initializes internationalization support for Mx. If MxApplication is
// used, this is called automatically. Otherwise it has to be called
// together with clutter_init() before using Mx.
static void set_locale()() nothrow {
   mx_set_locale();
}


// Generates a string describing the time given in @time_ using
// colloquial language suitable for display to the user. Examples of
// what might be returned are "A few minutes ago" or "Yesterday".
// RETURNS: a string. Free with g_free().
// <time_>: a time value
static char* /*new*/ utils_format_time(AT0)(AT0 /*GLib2.TimeVal*/ time_) nothrow {
   return mx_utils_format_time(UpCast!(GLib2.TimeVal*)(time_));
}


// C prototypes:

extern (C) {
Action* /*new*/ mx_action_new() nothrow;
Action* /*new*/ mx_action_new_full(char* name, char* display_name, ActionCallbackFunc activated_cb, void* user_data) nothrow;
Action* mx_action_new_stateful(char* name, GLib2.VariantType* parameter_type, GLib2.Variant* state) nothrow;
Action* mx_action_new_with_parameter(char* name, GLib2.VariantType* parameter_type=null) nothrow;
int mx_action_get_active(Action* this_) nothrow;
char* mx_action_get_display_name(Action* this_) nothrow;
char* mx_action_get_icon(Action* this_) nothrow;
char* mx_action_get_name(Action* this_) nothrow;
void mx_action_set_active(Action* this_, int active) nothrow;
void mx_action_set_display_name(Action* this_, char* name) nothrow;
void mx_action_set_icon(Action* this_, char* name) nothrow;
void mx_action_set_name(Action* this_, char* name) nothrow;
ActorManager* mx_actor_manager_new(Clutter.Stage* stage) nothrow;
ActorManager* mx_actor_manager_get_for_stage(Clutter.Stage* stage) nothrow;
c_ulong mx_actor_manager_add_actor(ActorManager* this_, Clutter.Container* container, Clutter.Actor* actor) nothrow;
void mx_actor_manager_cancel_operation(ActorManager* this_, c_ulong id) nothrow;
void mx_actor_manager_cancel_operations(ActorManager* this_, Clutter.Actor* actor) nothrow;
c_ulong mx_actor_manager_create_actor(ActorManager* this_, ActorManagerCreateFunc create_func, void* userdata, GLib2.DestroyNotify destroy_func) nothrow;
uint mx_actor_manager_get_n_operations(ActorManager* this_) nothrow;
Clutter.Stage* mx_actor_manager_get_stage(ActorManager* this_) nothrow;
uint mx_actor_manager_get_time_slice(ActorManager* this_) nothrow;
c_ulong mx_actor_manager_remove_actor(ActorManager* this_, Clutter.Container* container, Clutter.Actor* actor) nothrow;
void mx_actor_manager_remove_container(ActorManager* this_, Clutter.Container* container) nothrow;
void mx_actor_manager_set_time_slice(ActorManager* this_, uint msecs) nothrow;
Adjustment* /*new*/ mx_adjustment_new() nothrow;
Adjustment* /*new*/ mx_adjustment_new_with_values(double value, double lower, double upper, double step_increment, double page_increment, double page_size) nothrow;
int mx_adjustment_get_clamp_value(Adjustment* this_) nothrow;
int mx_adjustment_get_elastic(Adjustment* this_) nothrow;
double mx_adjustment_get_lower(Adjustment* this_) nothrow;
double mx_adjustment_get_page_increment(Adjustment* this_) nothrow;
double mx_adjustment_get_page_size(Adjustment* this_) nothrow;
double mx_adjustment_get_step_increment(Adjustment* this_) nothrow;
double mx_adjustment_get_upper(Adjustment* this_) nothrow;
double mx_adjustment_get_value(Adjustment* this_) nothrow;
void mx_adjustment_get_values(Adjustment* this_, double* value, double* lower, double* upper, double* step_increment, double* page_increment, double* page_size) nothrow;
void mx_adjustment_interpolate(Adjustment* this_, double value, uint duration, c_ulong mode) nothrow;
void mx_adjustment_interpolate_relative(Adjustment* this_, double offset, uint duration, c_ulong mode) nothrow;
void mx_adjustment_set_clamp_value(Adjustment* this_, int clamp) nothrow;
void mx_adjustment_set_elastic(Adjustment* this_, int elastic) nothrow;
void mx_adjustment_set_lower(Adjustment* this_, double lower) nothrow;
void mx_adjustment_set_page_increment(Adjustment* this_, double increment) nothrow;
void mx_adjustment_set_page_size(Adjustment* this_, double page_size) nothrow;
void mx_adjustment_set_step_increment(Adjustment* this_, double increment) nothrow;
void mx_adjustment_set_upper(Adjustment* this_, double upper) nothrow;
void mx_adjustment_set_value(Adjustment* this_, double value) nothrow;
void mx_adjustment_set_values(Adjustment* this_, double value, double lower, double upper, double step_increment, double page_increment, double page_size) nothrow;
Application* /*new*/ mx_application_new(/*inout*/ int* argc, /*inout*/ char*** argv, char* name, ApplicationFlags flags) nothrow;
void mx_application_add_action(Application* this_, Action* action) nothrow;
void mx_application_add_window(Application* this_, Window* window) nothrow;
Window* mx_application_create_window(Application* this_) nothrow;
GLib2.List* /*new container*/ mx_application_get_actions(Application* this_) nothrow;
ApplicationFlags mx_application_get_flags(Application* this_) nothrow;
GLib2.List* mx_application_get_windows(Application* this_) nothrow;
void mx_application_invoke_action(Application* this_, char* name) nothrow;
void mx_application_invoke_action_with_parameter(Application* this_, char* name, GLib2.Variant* variant) nothrow;
int mx_application_is_running(Application* this_) nothrow;
void mx_application_quit(Application* this_) nothrow;
void mx_application_remove_action(Application* this_, char* name) nothrow;
void mx_application_remove_window(Application* this_, Window* window) nothrow;
void mx_application_run(Application* this_) nothrow;
void mx_bin_allocate_child(Bin* this_, Clutter.ActorBox* box, Clutter.AllocationFlags flags) nothrow;
void mx_bin_get_alignment(Bin* this_, Align* x_align, Align* y_align) nothrow;
Clutter.Actor* mx_bin_get_child(Bin* this_) nothrow;
void mx_bin_get_fill(Bin* this_, /*out*/ int* x_fill, /*out*/ int* y_fill) nothrow;
void mx_bin_set_alignment(Bin* this_, Align x_align, Align y_align) nothrow;
void mx_bin_set_child(Bin* this_, Clutter.Actor* child) nothrow;
void mx_bin_set_fill(Bin* this_, int x_fill, int y_fill) nothrow;
int mx_border_image_equal(BorderImage* this_, BorderImage* b2) nothrow;
void mx_border_image_set_from_string(GObject2.Value* value, char* str, char* filename) nothrow;
BoxLayout* mx_box_layout_new() nothrow;
BoxLayout* mx_box_layout_new_with_orientation(Orientation orientation) nothrow;
void mx_box_layout_add_actor(BoxLayout* this_, Clutter.Actor* actor, int position) nothrow;
void mx_box_layout_add_actor_with_properties(BoxLayout* this_, Clutter.Actor* actor, int position, char* first_property, ...) nothrow;
int mx_box_layout_child_get_expand(BoxLayout* this_, Clutter.Actor* child) nothrow;
Align mx_box_layout_child_get_x_align(BoxLayout* this_, Clutter.Actor* child) nothrow;
int mx_box_layout_child_get_x_fill(BoxLayout* this_, Clutter.Actor* child) nothrow;
Align mx_box_layout_child_get_y_align(BoxLayout* this_, Clutter.Actor* child) nothrow;
int mx_box_layout_child_get_y_fill(BoxLayout* this_, Clutter.Actor* child) nothrow;
void mx_box_layout_child_set_expand(BoxLayout* this_, Clutter.Actor* child, int expand) nothrow;
void mx_box_layout_child_set_x_align(BoxLayout* this_, Clutter.Actor* child, Align x_align) nothrow;
void mx_box_layout_child_set_x_fill(BoxLayout* this_, Clutter.Actor* child, int x_fill) nothrow;
void mx_box_layout_child_set_y_align(BoxLayout* this_, Clutter.Actor* child, Align y_align) nothrow;
void mx_box_layout_child_set_y_fill(BoxLayout* this_, Clutter.Actor* child, int y_fill) nothrow;
int mx_box_layout_get_enable_animations(BoxLayout* this_) nothrow;
Orientation mx_box_layout_get_orientation(BoxLayout* this_) nothrow;
int mx_box_layout_get_scroll_to_focused(BoxLayout* this_) nothrow;
uint mx_box_layout_get_spacing(BoxLayout* this_) nothrow;
void mx_box_layout_set_enable_animations(BoxLayout* this_, int enable_animations) nothrow;
void mx_box_layout_set_orientation(BoxLayout* this_, Orientation orientation) nothrow;
void mx_box_layout_set_scroll_to_focused(BoxLayout* this_, int scroll_to_focused) nothrow;
void mx_box_layout_set_spacing(BoxLayout* this_, uint spacing) nothrow;
Button* mx_button_new() nothrow;
Button* mx_button_new_with_label(char* text) nothrow;
Action* mx_button_get_action(Button* this_) nothrow;
char* mx_button_get_icon_name(Button* this_) nothrow;
Position mx_button_get_icon_position(Button* this_) nothrow;
uint mx_button_get_icon_size(Button* this_) nothrow;
int mx_button_get_icon_visible(Button* this_) nothrow;
int mx_button_get_is_toggle(Button* this_) nothrow;
char* mx_button_get_label(Button* this_) nothrow;
int mx_button_get_label_visible(Button* this_) nothrow;
int mx_button_get_toggled(Button* this_) nothrow;
void mx_button_set_action(Button* this_, Action* action) nothrow;
void mx_button_set_icon_name(Button* this_, char* icon_name=null) nothrow;
void mx_button_set_icon_position(Button* this_, Position position) nothrow;
void mx_button_set_icon_size(Button* this_, uint icon_size) nothrow;
void mx_button_set_icon_visible(Button* this_, int visible) nothrow;
void mx_button_set_is_toggle(Button* this_, int toggle) nothrow;
void mx_button_set_label(Button* this_, char* text) nothrow;
void mx_button_set_label_visible(Button* this_, int visible) nothrow;
void mx_button_set_toggled(Button* this_, int toggled) nothrow;
ButtonGroup* mx_button_group_new() nothrow;
void mx_button_group_add(ButtonGroup* this_, Button* button) nothrow;
void mx_button_group_foreach(ButtonGroup* this_, Clutter.Callback callback, void* userdata) nothrow;
Button* mx_button_group_get_active_button(ButtonGroup* this_) nothrow;
int mx_button_group_get_allow_no_active(ButtonGroup* this_) nothrow;
GLib2.SList* mx_button_group_get_buttons(ButtonGroup* this_) nothrow;
void mx_button_group_remove(ButtonGroup* this_, Button* button) nothrow;
void mx_button_group_set_active_button(ButtonGroup* this_, Button* button=null) nothrow;
void mx_button_group_set_allow_no_active(ButtonGroup* this_, int allow_no_active) nothrow;
Clipboard* mx_clipboard_get_default() nothrow;
void mx_clipboard_get_text(Clipboard* this_, ClipboardCallbackFunc callback, void* user_data) nothrow;
void mx_clipboard_set_text(Clipboard* this_, char* text) nothrow;
ComboBox* mx_combo_box_new() nothrow;
void mx_combo_box_append_text(ComboBox* this_, char* text) nothrow;
char* mx_combo_box_get_active_icon_name(ComboBox* this_) nothrow;
char* mx_combo_box_get_active_text(ComboBox* this_) nothrow;
int mx_combo_box_get_index(ComboBox* this_) nothrow;
void mx_combo_box_insert_text(ComboBox* this_, int position, char* text) nothrow;
void mx_combo_box_insert_text_with_icon(ComboBox* this_, int position, char* text, char* icon) nothrow;
void mx_combo_box_prepend_text(ComboBox* this_, char* text) nothrow;
void mx_combo_box_remove_all(ComboBox* this_) nothrow;
void mx_combo_box_remove_text(ComboBox* this_, int position) nothrow;
void mx_combo_box_set_active_icon_name(ComboBox* this_, char* icon_name=null) nothrow;
void mx_combo_box_set_active_text(ComboBox* this_, char* text) nothrow;
void mx_combo_box_set_index(ComboBox* this_, int index) nothrow;
DeformBowTie* mx_deform_bow_tie_new() nothrow;
int mx_deform_bow_tie_get_flip_back(DeformBowTie* this_) nothrow;
double mx_deform_bow_tie_get_period(DeformBowTie* this_) nothrow;
void mx_deform_bow_tie_set_flip_back(DeformBowTie* this_, int flip_back) nothrow;
void mx_deform_bow_tie_set_period(DeformBowTie* this_, double period) nothrow;
DeformPageTurn* mx_deform_page_turn_new() nothrow;
double mx_deform_page_turn_get_angle(DeformPageTurn* this_) nothrow;
double mx_deform_page_turn_get_period(DeformPageTurn* this_) nothrow;
double mx_deform_page_turn_get_radius(DeformPageTurn* this_) nothrow;
void mx_deform_page_turn_set_angle(DeformPageTurn* this_, double angle) nothrow;
void mx_deform_page_turn_set_period(DeformPageTurn* this_, double period) nothrow;
void mx_deform_page_turn_set_radius(DeformPageTurn* this_, double radius) nothrow;
void mx_deform_texture_get_resolution(DeformTexture* this_, /*out*/ int* tiles_x=null, /*out*/ int* tiles_y=null) nothrow;
void mx_deform_texture_get_textures(DeformTexture* this_, /*out*/ Clutter.Texture** front=null, /*out*/ Clutter.Texture** back=null) nothrow;
void mx_deform_texture_invalidate(DeformTexture* this_) nothrow;
void mx_deform_texture_set_resolution(DeformTexture* this_, int tiles_x, int tiles_y) nothrow;
void mx_deform_texture_set_textures(DeformTexture* this_, Clutter.Texture* front=null, Clutter.Texture* back=null) nothrow;
DeformWaves* mx_deform_waves_new() nothrow;
double mx_deform_waves_get_amplitude(DeformWaves* this_) nothrow;
double mx_deform_waves_get_angle(DeformWaves* this_) nothrow;
double mx_deform_waves_get_period(DeformWaves* this_) nothrow;
double mx_deform_waves_get_radius(DeformWaves* this_) nothrow;
void mx_deform_waves_set_amplitude(DeformWaves* this_, double amplitude) nothrow;
void mx_deform_waves_set_angle(DeformWaves* this_, double angle) nothrow;
void mx_deform_waves_set_period(DeformWaves* this_, double period) nothrow;
void mx_deform_waves_set_radius(DeformWaves* this_, double radius) nothrow;
Dialog* mx_dialog_new() nothrow;
void mx_dialog_add_action(Dialog* this_, Action* action) nothrow;
GLib2.List* /*new container*/ mx_dialog_get_actions(Dialog* this_) nothrow;
void mx_dialog_remove_action(Dialog* this_, Action* action) nothrow;
void mx_dialog_set_transient_parent(Dialog* this_, Clutter.Actor* actor) nothrow;
void mx_draggable_disable(Draggable* this_) nothrow;
void mx_draggable_enable(Draggable* this_) nothrow;
DragAxis mx_draggable_get_axis(Draggable* this_) nothrow;
Clutter.Actor* mx_draggable_get_drag_actor(Draggable* this_) nothrow;
uint mx_draggable_get_drag_threshold(Draggable* this_) nothrow;
int mx_draggable_is_enabled(Draggable* this_) nothrow;
void mx_draggable_set_axis(Draggable* this_, DragAxis axis) nothrow;
void mx_draggable_set_drag_actor(Draggable* this_, Clutter.Actor* actor) nothrow;
void mx_draggable_set_drag_threshold(Draggable* this_, uint threshold) nothrow;
int mx_droppable_accept_drop(Droppable* this_, Draggable* draggable) nothrow;
void mx_droppable_disable(Droppable* this_) nothrow;
void mx_droppable_enable(Droppable* this_) nothrow;
int mx_droppable_is_enabled(Droppable* this_) nothrow;
Entry* mx_entry_new() nothrow;
Entry* mx_entry_new_with_text(char* text) nothrow;
Clutter.Actor* mx_entry_get_clutter_text(Entry* this_) nothrow;
char* mx_entry_get_hint_text(Entry* this_) nothrow;
char* mx_entry_get_icon_highlight_suffix(Entry* this_) nothrow;
dchar mx_entry_get_password_char(Entry* this_) nothrow;
char* mx_entry_get_text(Entry* this_) nothrow;
void mx_entry_set_hint_text(Entry* this_, char* text) nothrow;
void mx_entry_set_icon_highlight_suffix(Entry* this_, char* suffix) nothrow;
void mx_entry_set_password_char(Entry* this_, dchar password_char) nothrow;
void mx_entry_set_primary_icon_from_file(Entry* this_, char* filename) nothrow;
void mx_entry_set_primary_icon_tooltip_text(Entry* this_, char* text) nothrow;
void mx_entry_set_secondary_icon_from_file(Entry* this_, char* filename) nothrow;
void mx_entry_set_secondary_icon_tooltip_text(Entry* this_, char* text) nothrow;
void mx_entry_set_text(Entry* this_, char* text) nothrow;
Expander* mx_expander_new() nothrow;
int mx_expander_get_expanded(Expander* this_) nothrow;
void mx_expander_set_expanded(Expander* this_, int expanded) nothrow;
void mx_expander_set_label(Expander* this_, char* label) nothrow;
FadeEffect* mx_fade_effect_new() nothrow;
void mx_fade_effect_get_border(FadeEffect* this_, /*out*/ uint* top, /*out*/ uint* right, /*out*/ uint* bottom, /*out*/ uint* left) nothrow;
void mx_fade_effect_get_bounds(FadeEffect* this_, /*out*/ int* x, /*out*/ int* y, /*out*/ uint* width, /*out*/ uint* height) nothrow;
void mx_fade_effect_get_color(FadeEffect* this_, /*out*/ Clutter.Color* color) nothrow;
void mx_fade_effect_set_border(FadeEffect* this_, uint top, uint right, uint bottom, uint left) nothrow;
void mx_fade_effect_set_bounds(FadeEffect* this_, int x, int y, uint width, uint height) nothrow;
void mx_fade_effect_set_color(FadeEffect* this_, Clutter.Color* color) nothrow;
FocusManager* mx_focus_manager_get_for_stage(Clutter.Stage* stage) nothrow;
Focusable* mx_focus_manager_get_focused(FocusManager* this_) nothrow;
Clutter.Stage* mx_focus_manager_get_stage(FocusManager* this_) nothrow;
void mx_focus_manager_move_focus(FocusManager* this_, FocusDirection direction) nothrow;
void mx_focus_manager_push_focus(FocusManager* this_, Focusable* focusable) nothrow;
void mx_focus_manager_push_focus_with_hint(FocusManager* this_, Focusable* focusable, FocusHint hint) nothrow;
Focusable* mx_focusable_accept_focus(Focusable* this_, FocusHint hint) nothrow;
Focusable* mx_focusable_move_focus(Focusable* this_, FocusDirection direction, Focusable* from) nothrow;
Frame* mx_frame_new() nothrow;
Grid* mx_grid_new() nothrow;
Align mx_grid_get_child_x_align(Grid* this_) nothrow;
Align mx_grid_get_child_y_align(Grid* this_) nothrow;
float mx_grid_get_column_spacing(Grid* this_) nothrow;
int mx_grid_get_homogenous_columns(Grid* this_) nothrow;
int mx_grid_get_homogenous_rows(Grid* this_) nothrow;
int mx_grid_get_line_alignment(Grid* this_) nothrow;
int mx_grid_get_max_stride(Grid* this_) nothrow;
Orientation mx_grid_get_orientation(Grid* this_) nothrow;
float mx_grid_get_row_spacing(Grid* this_) nothrow;
void mx_grid_set_child_x_align(Grid* this_, Align value) nothrow;
void mx_grid_set_child_y_align(Grid* this_, Align value) nothrow;
void mx_grid_set_column_spacing(Grid* this_, float value) nothrow;
void mx_grid_set_homogenous_columns(Grid* this_, int value) nothrow;
void mx_grid_set_homogenous_rows(Grid* this_, int value) nothrow;
void mx_grid_set_line_alignment(Grid* this_, Align value) nothrow;
void mx_grid_set_max_stride(Grid* this_, int value) nothrow;
void mx_grid_set_orientation(Grid* this_, Orientation orientation) nothrow;
void mx_grid_set_row_spacing(Grid* this_, float value) nothrow;
Icon* mx_icon_new() nothrow;
char* mx_icon_get_icon_name(Icon* this_) nothrow;
int mx_icon_get_icon_size(Icon* this_) nothrow;
void mx_icon_set_icon_name(Icon* this_, char* icon_name) nothrow;
void mx_icon_set_icon_size(Icon* this_, int size) nothrow;
IconTheme* /*new*/ mx_icon_theme_new() nothrow;
IconTheme* mx_icon_theme_get_default() nothrow;
GLib2.List* mx_icon_theme_get_search_paths(IconTheme* this_) nothrow;
char* mx_icon_theme_get_theme_name(IconTheme* this_) nothrow;
int mx_icon_theme_has_icon(IconTheme* this_, char* icon_name) nothrow;
Cogl.Handle mx_icon_theme_lookup(IconTheme* this_, char* icon_name, int size) nothrow;
Clutter.Texture* mx_icon_theme_lookup_texture(IconTheme* this_, char* icon_name, int size) nothrow;
void mx_icon_theme_set_search_paths(IconTheme* this_, GLib2.List* paths) nothrow;
void mx_icon_theme_set_theme_name(IconTheme* this_, char* theme_name) nothrow;
Image* mx_image_new() nothrow;
void mx_image_animate_scale_mode(Image* this_, c_ulong mode, uint duration, ImageScaleMode scale_mode) nothrow;
void mx_image_clear(Image* this_) nothrow;
int mx_image_get_allow_upscale(Image* this_) nothrow;
float mx_image_get_image_rotation(Image* this_) nothrow;
int mx_image_get_load_async(Image* this_) nothrow;
uint mx_image_get_scale_height_threshold(Image* this_) nothrow;
ImageScaleMode mx_image_get_scale_mode(Image* this_) nothrow;
uint mx_image_get_scale_width_threshold(Image* this_) nothrow;
uint mx_image_get_transition_duration(Image* this_) nothrow;
void mx_image_set_allow_upscale(Image* this_, int allow) nothrow;
int mx_image_set_from_buffer(Image* this_, ubyte* buffer, size_t buffer_size, GLib2.DestroyNotify buffer_free_func, GLib2.Error** error) nothrow;
int mx_image_set_from_buffer_at_size(Image* this_, ubyte* buffer, size_t buffer_size, GLib2.DestroyNotify buffer_free_func, int width, int height, GLib2.Error** error) nothrow;
int mx_image_set_from_cogl_texture(Image* this_, Cogl.Handle texture) nothrow;
int mx_image_set_from_data(Image* this_, ubyte* data, Cogl.PixelFormat pixel_format, int width, int height, int rowstride, GLib2.Error** error) nothrow;
int mx_image_set_from_file(Image* this_, char* filename, GLib2.Error** error) nothrow;
int mx_image_set_from_file_at_size(Image* this_, char* filename, int width, int height, GLib2.Error** error) nothrow;
void mx_image_set_image_rotation(Image* this_, float rotation) nothrow;
void mx_image_set_load_async(Image* this_, int load_async) nothrow;
void mx_image_set_scale_height_threshold(Image* this_, uint pixels) nothrow;
void mx_image_set_scale_mode(Image* this_, ImageScaleMode mode) nothrow;
void mx_image_set_scale_width_threshold(Image* this_, uint pixels) nothrow;
void mx_image_set_transition_duration(Image* this_, uint duration) nothrow;
Clutter.Actor* /*new*/ mx_item_factory_create(ItemFactory* this_) nothrow;
ItemView* mx_item_view_new() nothrow;
void mx_item_view_add_attribute(ItemView* this_, char* attribute, int column) nothrow;
void mx_item_view_freeze(ItemView* this_) nothrow;
ItemFactory* mx_item_view_get_factory(ItemView* this_) nothrow;
Type mx_item_view_get_item_type(ItemView* this_) nothrow;
Clutter.Model* mx_item_view_get_model(ItemView* this_) nothrow;
void mx_item_view_set_factory(ItemView* this_, ItemFactory* factory=null) nothrow;
void mx_item_view_set_item_type(ItemView* this_, Type item_type) nothrow;
void mx_item_view_set_model(ItemView* this_, Clutter.Model* model) nothrow;
void mx_item_view_thaw(ItemView* this_) nothrow;
KineticScrollView* mx_kinetic_scroll_view_new() nothrow;
double mx_kinetic_scroll_view_get_acceleration_factor(KineticScrollView* this_) nothrow;
uint mx_kinetic_scroll_view_get_clamp_duration(KineticScrollView* this_) nothrow;
c_ulong mx_kinetic_scroll_view_get_clamp_mode(KineticScrollView* this_) nothrow;
int mx_kinetic_scroll_view_get_clamp_to_center(KineticScrollView* this_) nothrow;
double mx_kinetic_scroll_view_get_deceleration(KineticScrollView* this_) nothrow;
uint mx_kinetic_scroll_view_get_mouse_button(KineticScrollView* this_) nothrow;
double mx_kinetic_scroll_view_get_overshoot(KineticScrollView* this_) nothrow;
ScrollPolicy mx_kinetic_scroll_view_get_scroll_policy(KineticScrollView* this_) nothrow;
int mx_kinetic_scroll_view_get_use_captured(KineticScrollView* this_) nothrow;
void mx_kinetic_scroll_view_set_acceleration_factor(KineticScrollView* this_, double acceleration_factor) nothrow;
void mx_kinetic_scroll_view_set_clamp_duration(KineticScrollView* this_, uint clamp_duration) nothrow;
void mx_kinetic_scroll_view_set_clamp_mode(KineticScrollView* this_, c_ulong clamp_mode) nothrow;
void mx_kinetic_scroll_view_set_clamp_to_center(KineticScrollView* this_, int clamp_to_center) nothrow;
void mx_kinetic_scroll_view_set_deceleration(KineticScrollView* this_, double rate) nothrow;
void mx_kinetic_scroll_view_set_mouse_button(KineticScrollView* this_, uint button) nothrow;
void mx_kinetic_scroll_view_set_overshoot(KineticScrollView* this_, double overshoot) nothrow;
void mx_kinetic_scroll_view_set_scroll_policy(KineticScrollView* this_, ScrollPolicy policy) nothrow;
void mx_kinetic_scroll_view_set_use_captured(KineticScrollView* this_, int use_captured) nothrow;
void mx_kinetic_scroll_view_stop(KineticScrollView* this_) nothrow;
Label* mx_label_new() nothrow;
Label* mx_label_new_with_text(char* text) nothrow;
Clutter.Actor* mx_label_get_clutter_text(Label* this_) nothrow;
int mx_label_get_fade_out(Label* this_) nothrow;
int mx_label_get_line_wrap(Label* this_) nothrow;
int mx_label_get_show_tooltip(Label* this_) nothrow;
char* mx_label_get_text(Label* this_) nothrow;
int mx_label_get_use_markup(Label* this_) nothrow;
Align mx_label_get_x_align(Label* this_) nothrow;
Align mx_label_get_y_align(Label* this_) nothrow;
void mx_label_set_fade_out(Label* this_, int fade) nothrow;
void mx_label_set_line_wrap(Label* this_, int line_wrap) nothrow;
void mx_label_set_show_tooltip(Label* this_, int show_tooltip) nothrow;
void mx_label_set_text(Label* this_, char* text) nothrow;
void mx_label_set_use_markup(Label* this_, int use_markup) nothrow;
void mx_label_set_x_align(Label* this_, Align align_) nothrow;
void mx_label_set_y_align(Label* this_, Align align_) nothrow;
ListView* mx_list_view_new() nothrow;
void mx_list_view_add_attribute(ListView* this_, char* attribute, int column) nothrow;
void mx_list_view_freeze(ListView* this_) nothrow;
ItemFactory* mx_list_view_get_factory(ListView* this_) nothrow;
Type mx_list_view_get_item_type(ListView* this_) nothrow;
Clutter.Model* mx_list_view_get_model(ListView* this_) nothrow;
void mx_list_view_set_factory(ListView* this_, ItemFactory* factory=null) nothrow;
void mx_list_view_set_item_type(ListView* this_, Type item_type) nothrow;
void mx_list_view_set_model(ListView* this_, Clutter.Model* model) nothrow;
void mx_list_view_thaw(ListView* this_) nothrow;
Menu* mx_menu_new() nothrow;
void mx_menu_add_action(Menu* this_, Action* action) nothrow;
void mx_menu_remove_action(Menu* this_, Action* action) nothrow;
void mx_menu_remove_all(Menu* this_) nothrow;
void mx_menu_show_with_position(Menu* this_, float x, float y) nothrow;
Notebook* mx_notebook_new() nothrow;
Clutter.Actor* mx_notebook_get_current_page(Notebook* this_) nothrow;
int mx_notebook_get_enable_gestures(Notebook* this_) nothrow;
void mx_notebook_next_page(Notebook* this_) nothrow;
void mx_notebook_previous_page(Notebook* this_) nothrow;
void mx_notebook_set_current_page(Notebook* this_, Clutter.Actor* page) nothrow;
void mx_notebook_set_enable_gestures(Notebook* this_, int enabled) nothrow;
Offscreen* mx_offscreen_new() nothrow;
int mx_offscreen_get_accumulation_enabled(Offscreen* this_) nothrow;
Cogl.Handle mx_offscreen_get_accumulation_material(Offscreen* this_) nothrow;
int mx_offscreen_get_auto_update(Offscreen* this_) nothrow;
Cogl.Handle mx_offscreen_get_buffer(Offscreen* this_) nothrow;
Clutter.Actor* mx_offscreen_get_child(Offscreen* this_) nothrow;
int mx_offscreen_get_pick_child(Offscreen* this_) nothrow;
int mx_offscreen_get_redirect_enabled(Offscreen* this_) nothrow;
void mx_offscreen_set_accumulation_enabled(Offscreen* this_, int enable) nothrow;
void mx_offscreen_set_auto_update(Offscreen* this_, int auto_update) nothrow;
void mx_offscreen_set_child(Offscreen* this_, Clutter.Actor* actor) nothrow;
void mx_offscreen_set_pick_child(Offscreen* this_, int pick) nothrow;
void mx_offscreen_set_redirect_enabled(Offscreen* this_, int enabled) nothrow;
void mx_offscreen_update(Offscreen* this_) nothrow;
PathBar* mx_path_bar_new() nothrow;
void mx_path_bar_clear(PathBar* this_) nothrow;
int mx_path_bar_get_clear_on_change(PathBar* this_) nothrow;
int mx_path_bar_get_editable(PathBar* this_) nothrow;
Entry* mx_path_bar_get_entry(PathBar* this_) nothrow;
char* mx_path_bar_get_label(PathBar* this_, int level) nothrow;
int mx_path_bar_get_level(PathBar* this_) nothrow;
char* mx_path_bar_get_text(PathBar* this_) nothrow;
int mx_path_bar_pop(PathBar* this_) nothrow;
int mx_path_bar_push(PathBar* this_, char* name) nothrow;
void mx_path_bar_set_clear_on_change(PathBar* this_, int clear_on_change) nothrow;
void mx_path_bar_set_editable(PathBar* this_, int editable) nothrow;
void mx_path_bar_set_label(PathBar* this_, int level, char* label) nothrow;
void mx_path_bar_set_text(PathBar* this_, char* text) nothrow;
ProgressBar* mx_progress_bar_new() nothrow;
double mx_progress_bar_get_progress(ProgressBar* this_) nothrow;
void mx_progress_bar_set_progress(ProgressBar* this_, double progress) nothrow;
ScrollBar* mx_scroll_bar_new() nothrow;
ScrollBar* mx_scroll_bar_new_with_adjustment(Adjustment* adjustment) nothrow;
Adjustment* mx_scroll_bar_get_adjustment(ScrollBar* this_) nothrow;
Orientation mx_scroll_bar_get_orientation(ScrollBar* this_) nothrow;
void mx_scroll_bar_set_adjustment(ScrollBar* this_, Adjustment* adjustment) nothrow;
void mx_scroll_bar_set_orientation(ScrollBar* this_, Orientation orientation) nothrow;
ScrollView* mx_scroll_view_new() nothrow;
void mx_scroll_view_ensure_visible(ScrollView* this_, Clutter.Geometry* geometry) nothrow;
int mx_scroll_view_get_enable_gestures(ScrollView* this_) nothrow;
int mx_scroll_view_get_enable_mouse_scrolling(ScrollView* this_) nothrow;
ScrollPolicy mx_scroll_view_get_scroll_policy(ScrollView* this_) nothrow;
void mx_scroll_view_set_enable_gestures(ScrollView* this_, int enabled) nothrow;
void mx_scroll_view_set_enable_mouse_scrolling(ScrollView* this_, int enabled) nothrow;
void mx_scroll_view_set_scroll_policy(ScrollView* this_, ScrollPolicy policy) nothrow;
void mx_scrollable_get_adjustments(Scrollable* this_, /*out*/ Adjustment** hadjustment=null, /*out*/ Adjustment** vadjustment=null) nothrow;
void mx_scrollable_set_adjustments(Scrollable* this_, Adjustment* hadjustment, Adjustment* vadjustment) nothrow;
Settings* mx_settings_get_default() nothrow;
Slider* mx_slider_new() nothrow;
double mx_slider_get_buffer_value(Slider* this_) nothrow;
double mx_slider_get_value(Slider* this_) nothrow;
void mx_slider_set_buffer_value(Slider* this_, double value) nothrow;
void mx_slider_set_value(Slider* this_, double value) nothrow;
Spinner* mx_spinner_new() nothrow;
int mx_spinner_get_animating(Spinner* this_) nothrow;
void mx_spinner_set_animating(Spinner* this_, int animating) nothrow;
Stack* mx_stack_new() nothrow;
int mx_stack_child_get_crop(Stack* this_, Clutter.Actor* child) nothrow;
int mx_stack_child_get_fit(Stack* this_, Clutter.Actor* child) nothrow;
Align mx_stack_child_get_x_align(Stack* this_, Clutter.Actor* child) nothrow;
int mx_stack_child_get_x_fill(Stack* this_, Clutter.Actor* child) nothrow;
Align mx_stack_child_get_y_align(Stack* this_, Clutter.Actor* child) nothrow;
int mx_stack_child_get_y_fill(Stack* this_, Clutter.Actor* child) nothrow;
void mx_stack_child_set_crop(Stack* this_, Clutter.Actor* child, int crop) nothrow;
void mx_stack_child_set_fit(Stack* this_, Clutter.Actor* child, int fit) nothrow;
void mx_stack_child_set_x_align(Stack* this_, Clutter.Actor* child, Align x_align) nothrow;
void mx_stack_child_set_x_fill(Stack* this_, Clutter.Actor* child, int x_fill) nothrow;
void mx_stack_child_set_y_align(Stack* this_, Clutter.Actor* child, Align y_align) nothrow;
void mx_stack_child_set_y_fill(Stack* this_, Clutter.Actor* child, int y_fill) nothrow;
void mx_stylable_apply_clutter_text_attributes(Stylable* this_, Clutter.Text* text) nothrow;
void mx_stylable_connect_change_notifiers(Stylable* this_) nothrow;
GObject2.ParamSpec* mx_stylable_find_property(Stylable* this_, char* property_name) nothrow;
void mx_stylable_get(Stylable* this_, char* first_property_name, ...) nothrow;
int mx_stylable_get_default_value(Stylable* this_, char* property_name, /*out*/ GObject2.Value* value_out) nothrow;
void mx_stylable_get_property(Stylable* this_, char* property_name, /*out*/ GObject2.Value* value) nothrow;
Style* mx_stylable_get_style(Stylable* this_) nothrow;
char* mx_stylable_get_style_class(Stylable* this_) nothrow;
char* mx_stylable_get_style_pseudo_class(Stylable* this_) nothrow;
GObject2.ParamSpec** /*new container*/ mx_stylable_list_properties(Stylable* this_, /*out*/ uint* n_props) nothrow;
void mx_stylable_set_style(Stylable* this_, Style* style) nothrow;
void mx_stylable_set_style_class(Stylable* this_, char* style_class) nothrow;
void mx_stylable_set_style_pseudo_class(Stylable* this_, char* pseudo_class) nothrow;
void mx_stylable_style_changed(Stylable* this_, StyleChangedFlags flags) nothrow;
void mx_stylable_style_pseudo_class_add(Stylable* this_, char* new_class) nothrow;
int mx_stylable_style_pseudo_class_contains(Stylable* this_, char* pseudo_class) nothrow;
void mx_stylable_style_pseudo_class_remove(Stylable* this_, char* remove_class) nothrow;
void mx_stylable_iface_install_property(StylableIface* this_, Type owner_type, GObject2.ParamSpec* pspec) nothrow;
Style* /*new*/ mx_style_new() nothrow;
Style* mx_style_get_default() nothrow;
void mx_style_get(Style* this_, Stylable* stylable, char* first_property_name, ...) nothrow;
void mx_style_get_property(Style* this_, Stylable* stylable, GObject2.ParamSpec* pspec, /*out*/ GObject2.Value* value) nothrow;
void mx_style_get_valist(Style* this_, Stylable* stylable, char* first_property_name, va_list va_args) nothrow;
int mx_style_load_from_file(Style* this_, char* filename, GLib2.Error** error) nothrow;
Table* mx_table_new() nothrow;
void mx_table_add_actor(Table* this_, Clutter.Actor* actor, int row, int column) nothrow;
void mx_table_add_actor_with_properties(Table* this_, Clutter.Actor* actor, int row, int column, char* first_property_name, ...) nothrow;
int mx_table_child_get_column(Table* this_, Clutter.Actor* child) nothrow;
int mx_table_child_get_column_span(Table* this_, Clutter.Actor* child) nothrow;
int mx_table_child_get_row(Table* this_, Clutter.Actor* child) nothrow;
int mx_table_child_get_row_span(Table* this_, Clutter.Actor* child) nothrow;
Align mx_table_child_get_x_align(Table* this_, Clutter.Actor* child) nothrow;
int mx_table_child_get_x_expand(Table* this_, Clutter.Actor* child) nothrow;
int mx_table_child_get_x_fill(Table* this_, Clutter.Actor* child) nothrow;
Align mx_table_child_get_y_align(Table* this_, Clutter.Actor* child) nothrow;
int mx_table_child_get_y_expand(Table* this_, Clutter.Actor* child) nothrow;
int mx_table_child_get_y_fill(Table* this_, Clutter.Actor* child) nothrow;
void mx_table_child_set_column(Table* this_, Clutter.Actor* child, int col) nothrow;
void mx_table_child_set_column_span(Table* this_, Clutter.Actor* child, int span) nothrow;
void mx_table_child_set_row(Table* this_, Clutter.Actor* child, int row) nothrow;
void mx_table_child_set_row_span(Table* this_, Clutter.Actor* child, int span) nothrow;
void mx_table_child_set_x_align(Table* this_, Clutter.Actor* child, Align align_) nothrow;
void mx_table_child_set_x_expand(Table* this_, Clutter.Actor* child, int expand) nothrow;
void mx_table_child_set_x_fill(Table* this_, Clutter.Actor* child, int fill) nothrow;
void mx_table_child_set_y_align(Table* this_, Clutter.Actor* child, Align align_) nothrow;
void mx_table_child_set_y_expand(Table* this_, Clutter.Actor* child, int expand) nothrow;
void mx_table_child_set_y_fill(Table* this_, Clutter.Actor* child, int fill) nothrow;
int mx_table_get_column_count(Table* this_) nothrow;
int mx_table_get_column_spacing(Table* this_) nothrow;
int mx_table_get_row_count(Table* this_) nothrow;
int mx_table_get_row_spacing(Table* this_) nothrow;
void mx_table_set_column_spacing(Table* this_, int spacing) nothrow;
void mx_table_set_row_spacing(Table* this_, int spacing) nothrow;
TextureCache* mx_texture_cache_get_default() nothrow;
int mx_texture_cache_contains(TextureCache* this_, char* uri) nothrow;
int mx_texture_cache_contains_meta(TextureCache* this_, char* uri, void* ident) nothrow;
Clutter.Actor* mx_texture_cache_get_actor(TextureCache* this_, char* uri) nothrow;
Cogl.Handle mx_texture_cache_get_cogl_texture(TextureCache* this_, char* uri) nothrow;
Cogl.Handle /*new*/ mx_texture_cache_get_meta_cogl_texture(TextureCache* this_, char* uri, void* ident) nothrow;
Clutter.Texture* /*new*/ mx_texture_cache_get_meta_texture(TextureCache* this_, char* uri, void* ident) nothrow;
int mx_texture_cache_get_size(TextureCache* this_) nothrow;
Clutter.Texture* mx_texture_cache_get_texture(TextureCache* this_, char* uri) nothrow;
void mx_texture_cache_insert(TextureCache* this_, char* uri, Cogl.Handle* texture) nothrow;
void mx_texture_cache_insert_meta(TextureCache* this_, char* uri, void* ident, Cogl.Handle* texture, GLib2.DestroyNotify destroy_func) nothrow;
void mx_texture_cache_load_cache(TextureCache* this_, char* filename) nothrow;
TextureFrame* mx_texture_frame_new(Clutter.Texture* texture, float top, float right, float bottom, float left) nothrow;
void mx_texture_frame_get_border_values(TextureFrame* this_, float* top, float* right, float* bottom, float* left) nothrow;
Clutter.Texture* mx_texture_frame_get_parent_texture(TextureFrame* this_) nothrow;
void mx_texture_frame_set_border_values(TextureFrame* this_, float top, float right, float bottom, float left) nothrow;
void mx_texture_frame_set_parent_texture(TextureFrame* this_, Clutter.Texture* texture) nothrow;
Toggle* mx_toggle_new() nothrow;
int mx_toggle_get_active(Toggle* this_) nothrow;
void mx_toggle_set_active(Toggle* this_, int active) nothrow;
Toolbar* mx_toolbar_new() nothrow;
int mx_toolbar_get_has_close_button(Toolbar* this_) nothrow;
void mx_toolbar_set_has_close_button(Toolbar* this_, int has_close_button) nothrow;
int mx_tooltip_is_in_browse_mode() nothrow;
char* mx_tooltip_get_text(Tooltip* this_) nothrow;
Clutter.Geometry* mx_tooltip_get_tip_area(Tooltip* this_) nothrow;
void mx_tooltip_hide(Tooltip* this_) nothrow;
void mx_tooltip_set_text(Tooltip* this_, char* text) nothrow;
void mx_tooltip_set_tip_area(Tooltip* this_, Clutter.Geometry* area) nothrow;
void mx_tooltip_set_tip_area_from_actor(Tooltip* this_, Clutter.Actor* actor) nothrow;
void mx_tooltip_show(Tooltip* this_) nothrow;
Viewport* mx_viewport_new() nothrow;
void mx_viewport_get_origin(Viewport* this_, float* x, float* y, float* z) nothrow;
int mx_viewport_get_sync_adjustments(Viewport* this_) nothrow;
void mx_viewport_set_origin(Viewport* this_, float x, float y, float z) nothrow;
void mx_viewport_set_sync_adjustments(Viewport* this_, int sync) nothrow;
void mx_widget_apply_style(Widget* this_, Style* style) nothrow;
void mx_widget_get_available_area(Widget* this_, Clutter.ActorBox* allocation, Clutter.ActorBox* area) nothrow;
Clutter.Actor* mx_widget_get_background_image(Widget* this_) nothrow;
Clutter.Actor* mx_widget_get_border_image(Widget* this_) nothrow;
int mx_widget_get_disabled(Widget* this_) nothrow;
Menu* mx_widget_get_menu(Widget* this_) nothrow;
void mx_widget_get_padding(Widget* this_, Padding* padding) nothrow;
uint mx_widget_get_tooltip_delay(Widget* this_) nothrow;
char* mx_widget_get_tooltip_text(Widget* this_) nothrow;
void mx_widget_hide_tooltip(Widget* this_) nothrow;
void mx_widget_long_press_cancel(Widget* this_) nothrow;
void mx_widget_long_press_query(Widget* this_, Clutter.ButtonEvent* event) nothrow;
void mx_widget_paint_background(Widget* this_) nothrow;
void mx_widget_set_disabled(Widget* this_, int disabled) nothrow;
void mx_widget_set_menu(Widget* this_, Menu* menu) nothrow;
void mx_widget_set_tooltip_delay(Widget* this_, uint delay) nothrow;
void mx_widget_set_tooltip_text(Widget* this_, char* text) nothrow;
void mx_widget_show_tooltip(Widget* this_) nothrow;
Window* /*new*/ mx_window_new() nothrow;
Window* /*new*/ mx_window_new_with_clutter_stage(Clutter.Stage* stage) nothrow;
Window* mx_window_get_for_stage(Clutter.Stage* stage) nothrow;
Clutter.Actor* mx_window_get_child(Window* this_) nothrow;
Clutter.Stage* mx_window_get_clutter_stage(Window* this_) nothrow;
int mx_window_get_fullscreen(Window* this_) nothrow;
int mx_window_get_has_toolbar(Window* this_) nothrow;
char* mx_window_get_icon_name(Window* this_) nothrow;
int mx_window_get_small_screen(Window* this_) nothrow;
char* mx_window_get_title(Window* this_) nothrow;
Toolbar* mx_window_get_toolbar(Window* this_) nothrow;
void mx_window_get_window_position(Window* this_, /*out*/ int* x, /*out*/ int* y) nothrow;
WindowRotation mx_window_get_window_rotation(Window* this_) nothrow;
void mx_window_get_window_size(Window* this_, /*out*/ int* width, /*out*/ int* height) nothrow;
void mx_window_hide(Window* this_) nothrow;
void mx_window_present(Window* this_) nothrow;
void mx_window_set_child(Window* this_, Clutter.Actor* actor) nothrow;
void mx_window_set_fullscreen(Window* this_, int fullscreen) nothrow;
void mx_window_set_has_toolbar(Window* this_, int toolbar) nothrow;
void mx_window_set_icon_from_cogl_texture(Window* this_, Cogl.Handle texture) nothrow;
void mx_window_set_icon_name(Window* this_, char* icon_name=null) nothrow;
void mx_window_set_small_screen(Window* this_, int small_screen) nothrow;
void mx_window_set_title(Window* this_, char* title) nothrow;
void mx_window_set_toolbar(Window* this_, Toolbar* toolbar) nothrow;
void mx_window_set_window_position(Window* this_, int x, int y) nothrow;
void mx_window_set_window_rotation(Window* this_, WindowRotation rotation) nothrow;
void mx_window_set_window_size(Window* this_, int width, int height) nothrow;
void mx_window_show(Window* this_) nothrow;
void mx_actor_box_clamp_to_pixels(Clutter.ActorBox* box) nothrow;
void mx_allocate_align_fill(Clutter.Actor* child, Clutter.ActorBox* childbox, Align x_alignment, Align y_alignment, int x_fill, int y_fill) nothrow;
FocusHint mx_focus_hint_from_direction(FocusDirection direction) nothrow;
void mx_font_weight_set_from_string(GObject2.Value* value, char* str) nothrow;
GLib2.Quark mx_image_error_quark() nothrow;
void mx_set_locale() nothrow;
char* /*new*/ mx_utils_format_time(GLib2.TimeVal* time_) nothrow;
}

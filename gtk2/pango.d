// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/Pango-1.0.gir"

module Pango;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;
public import gtk2.cairo;
alias gtk2.cairo cairo;

// package: "cairo";

// package: "freetype2";

// package: "gobject-2.0";

// c:symbol-prefixes: ["pango"]
// c:identifier-prefixes: ["Pango"]

// module Pango;

//  --- mixin/Pango__MODULE_HEAD.d --->

// Missing "macros":

int PIXELS(T)(T d)       { return (cast(int)d+512)>>10; }
int PIXELS_FLOOR(T)(T d) { return (cast(int)d)>>10; }
int PIXELS_CEIL(T)(T d)  { return (cast(int)d+1023)>>10; }

// <--- mixin/Pango__MODULE_HEAD.d ---

alias uint Glyph;
alias int GlyphUnit;
alias GlyphItem LayoutRun;
enum int ANALYSIS_FLAG_CENTERED_BASELINE = 1;
enum int ATTR_INDEX_FROM_TEXT_BEGINNING = 0;
enum Alignment {
   LEFT = 0,
   CENTER = 1,
   RIGHT = 2
}
struct Analysis {
   EngineShape* shape_engine;
   EngineLang* lang_engine;
   Font* font;
   ubyte level, gravity, flags, script;
   Language* language;
   GLib2.SList* extra_attrs;
}

struct AttrClass {
   AttrType type;
   // Unintrospectable functionp: copy() / ()
   extern (C) Attribute* function (Attribute* attr) nothrow copy;
   extern (C) void function (Attribute* attr) nothrow destroy;
   extern (C) int function (Attribute* attr1, Attribute* attr2) nothrow equal;
}

struct AttrColor {
   Attribute attr;
   Color color;
}

// Unintrospectable callback: AttrDataCopyFunc() / ()
extern (C) alias void* function (const(void)* data) nothrow AttrDataCopyFunc;

extern (C) alias int function (Attribute* attribute, void* data) nothrow AttrFilterFunc;

struct AttrFloat {
   Attribute attr;
   double value;
}

struct AttrFontDesc {
   Attribute attr;
   FontDescription* desc;


   // Unintrospectable function: new() / pango_attr_font_desc_new()
   // Create a new font description attribute. This attribute
   // allows setting family, style, weight, variant, stretch,
   // and size simultaneously.
   // 
   // freed with pango_attribute_destroy().
   // RETURNS: the newly allocated #PangoAttribute, which should be
   // <desc>: the font description
   static Attribute* new_(AT0)(AT0 /*FontDescription*/ desc) nothrow {
      return pango_attr_font_desc_new(UpCast!(FontDescription*)(desc));
   }
}

struct AttrInt {
   Attribute attr;
   int value;
}

struct AttrIterator {

   // Unintrospectable method: copy() / pango_attr_iterator_copy()
   // Copy a #PangoAttrIterator
   // 
   // be freed with pango_attr_iterator_destroy().
   // RETURNS: the newly allocated #PangoAttrIterator, which should
   AttrIterator* copy()() nothrow {
      return pango_attr_iterator_copy(&this);
   }
   // Destroy a #PangoAttrIterator and free all associated memory.
   void destroy()() nothrow {
      pango_attr_iterator_destroy(&this);
   }

   // Unintrospectable method: get() / pango_attr_iterator_get()
   // Find the current attribute of a particular type at the iterator
   // location. When multiple attributes of the same type overlap,
   // the attribute whose range starts closest to the current location
   // is used.
   // 
   // if no attribute of that type applies to the current
   // location.
   // RETURNS: the current attribute of the given type, or %NULL
   // <type>: the type of attribute to find.
   Attribute* get()(AttrType type) nothrow {
      return pango_attr_iterator_get(&this, type);
   }

   // VERSION: 1.2
   // Gets a list of all attributes at the current position of the
   // iterator.
   // 
   // all attributes for the current range.
   // To free this value, call pango_attribute_destroy() on
   // each value and g_slist_free() on the list.
   // RETURNS: a list of
   GLib2.SList* /*new*/ get_attrs()() nothrow {
      return pango_attr_iterator_get_attrs(&this);
   }

   // Unintrospectable method: get_font() / pango_attr_iterator_get_font()
   // Get the font and other attributes at the current iterator position.
   // <desc>: a #PangoFontDescription to fill in with the current values. The family name in this structure will be set using pango_font_description_set_family_static() using values from an attribute in the #PangoAttrList associated with the iterator, so if you plan to keep it around, you must call: <literal>pango_font_description_set_family (desc, pango_font_description_get_family (desc))</literal>.
   // <language>: if non-%NULL, location to store language tag for item, or %NULL if none is found.
   // <extra_attrs>: if non-%NULL, location in which to store a list of non-font attributes at the the current position; only the highest priority value of each attribute will be added to this list. In order to free this value, you must call pango_attribute_destroy() on each member.
   void get_font(AT0, AT1, AT2)(AT0 /*FontDescription*/ desc, AT1 /*Language**/ language, AT2 /*GLib2.SList**/ extra_attrs) nothrow {
      pango_attr_iterator_get_font(&this, UpCast!(FontDescription*)(desc), UpCast!(Language**)(language), UpCast!(GLib2.SList**)(extra_attrs));
   }

   // Advance the iterator until the next change of style.
   // RETURNS: %FALSE if the iterator is at the end of the list, otherwise %TRUE
   int next()() nothrow {
      return pango_attr_iterator_next(&this);
   }

   // Get the range of the current segment. Note that the
   // stored return values are signed, not unsigned like
   // the values in #PangoAttribute. To deal with this API
   // oversight, stored return values that wouldn't fit into
   // a signed integer are clamped to %G_MAXINT.
   // <start>: location to store the start of the range
   // <end>: location to store the end of the range
   void range()(int* start, int* end) nothrow {
      pango_attr_iterator_range(&this, start, end);
   }
}

struct AttrLanguage {
   Attribute attr;
   Language* value;


   // Unintrospectable function: new() / pango_attr_language_new()
   // Create a new language tag attribute.
   // 
   // freed with pango_attribute_destroy().
   // RETURNS: the newly allocated #PangoAttribute, which should be
   // <language>: language tag
   static Attribute* new_(AT0)(AT0 /*Language*/ language) nothrow {
      return pango_attr_language_new(UpCast!(Language*)(language));
   }
}

struct AttrList {

   // Create a new empty attribute list with a reference count of one.
   // 
   // be freed with pango_attr_list_unref().
   // RETURNS: the newly allocated #PangoAttrList, which should
   static AttrList* /*new*/ new_()() nothrow {
      return pango_attr_list_new();
   }
   static auto opCall()() {
      return pango_attr_list_new();
   }

   // Insert the given attribute into the #PangoAttrList. It will
   // replace any attributes of the same type on that segment
   // and be merged with any adjoining attributes that are identical.
   // 
   // This function is slower than pango_attr_list_insert() for
   // creating a attribute list in order (potentially much slower
   // for large lists). However, pango_attr_list_insert() is not
   // suitable for continually changing a set of attributes
   // since it never removes or combines existing attributes.
   // <attr>: the attribute to insert. Ownership of this value is assumed by the list.
   void change(AT0)(AT0 /*Attribute*/ attr) nothrow {
      pango_attr_list_change(&this, UpCast!(Attribute*)(attr));
   }

   // Copy @list and return an identical new list.
   // 
   // reference count of one, which should
   // be freed with pango_attr_list_unref().
   // Returns %NULL if @list was %NULL.
   // RETURNS: the newly allocated #PangoAttrList, with a
   AttrList* /*new*/ copy()() nothrow {
      return pango_attr_list_copy(&this);
   }

   // Unintrospectable method: filter() / pango_attr_list_filter()
   // VERSION: 1.2
   // Given a #PangoAttrList and callback function, removes any elements
   // of @list for which @func returns %TRUE and inserts them into
   // a new list.
   // 
   // no attributes of the given types were found.
   // RETURNS: the new #PangoAttrList or %NULL if
   // <func>: callback function; returns %TRUE if an attribute should be filtered out.
   // <data>: Data to be passed to @func
   AttrList* /*new*/ filter(AT0)(AttrFilterFunc func, AT0 /*void*/ data) nothrow {
      return pango_attr_list_filter(&this, func, UpCast!(void*)(data));
   }

   // Unintrospectable method: get_iterator() / pango_attr_list_get_iterator()
   // Create a iterator initialized to the beginning of the list.
   // @list must not be modified until this iterator is freed.
   // 
   // be freed with pango_attr_iterator_destroy().
   // RETURNS: the newly allocated #PangoAttrIterator, which should
   AttrIterator* get_iterator()() nothrow {
      return pango_attr_list_get_iterator(&this);
   }

   // Insert the given attribute into the #PangoAttrList. It will
   // be inserted after all other attributes with a matching
   // @start_index.
   // <attr>: the attribute to insert. Ownership of this value is assumed by the list.
   void insert(AT0)(AT0 /*Attribute*/ attr) nothrow {
      pango_attr_list_insert(&this, UpCast!(Attribute*)(attr));
   }

   // Insert the given attribute into the #PangoAttrList. It will
   // be inserted before all other attributes with a matching
   // @start_index.
   // <attr>: the attribute to insert. Ownership of this value is assumed by the list.
   void insert_before(AT0)(AT0 /*Attribute*/ attr) nothrow {
      pango_attr_list_insert_before(&this, UpCast!(Attribute*)(attr));
   }

   // VERSION: 1.10
   // Increase the reference count of the given attribute list by one.
   // RETURNS: The attribute list passed in
   AttrList* /*new*/ ref_()() nothrow {
      return pango_attr_list_ref(&this);
   }

   // This function opens up a hole in @list, fills it in with attributes from
   // the left, and then merges @other on top of the hole.
   // 
   // This operation is equivalent to stretching every attribute
   // that applies at position @pos in @list by an amount @len,
   // and then calling pango_attr_list_change() with a copy
   // of each attribute in @other in sequence (offset in position by @pos).
   // 
   // This operation proves useful for, for instance, inserting
   // a pre-edit string in the middle of an edit buffer.
   // <other>: another #PangoAttrList
   // <pos>: the position in @list at which to insert @other
   // <len>: the length of the spliced segment. (Note that this must be specified since the attributes in @other may only be present at some subsection of this range)
   void splice(AT0)(AT0 /*AttrList*/ other, int pos, int len) nothrow {
      pango_attr_list_splice(&this, UpCast!(AttrList*)(other), pos, len);
   }

   // Decrease the reference count of the given attribute list by one.
   // If the result is zero, free the attribute list and the attributes
   // it contains.
   void unref()() nothrow {
      pango_attr_list_unref(&this);
   }
}

struct AttrShape {
   Attribute attr;
   Rectangle ink_rect, logical_rect;
   void* data;
   AttrDataCopyFunc copy_func;
   GLib2.DestroyNotify destroy_func;


   // Unintrospectable function: new() / pango_attr_shape_new()
   // Create a new shape attribute. A shape is used to impose a
   // particular ink and logical rectangle on the result of shaping a
   // particular glyph. This might be used, for instance, for
   // embedding a picture or a widget inside a #PangoLayout.
   // 
   // freed with pango_attribute_destroy().
   // RETURNS: the newly allocated #PangoAttribute, which should be
   // <ink_rect>: ink rectangle to assign to each character
   // <logical_rect>: logical rectangle to assign to each character
   static Attribute* new_(AT0, AT1)(AT0 /*Rectangle*/ ink_rect, AT1 /*Rectangle*/ logical_rect) nothrow {
      return pango_attr_shape_new(UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect));
   }

   // Unintrospectable function: new_with_data() / pango_attr_shape_new_with_data()
   // VERSION: 1.8
   // Like pango_attr_shape_new(), but a user data pointer is also
   // provided; this pointer can be accessed when later
   // rendering the glyph.
   // 
   // freed with pango_attribute_destroy().
   // RETURNS: the newly allocated #PangoAttribute, which should be
   // <ink_rect>: ink rectangle to assign to each character
   // <logical_rect>: logical rectangle to assign to each character
   // <data>: user data pointer
   // <copy_func>: function to copy @data when the attribute is copied. If %NULL, @data is simply copied as a pointer.
   // <destroy_func>: function to free @data when the attribute is freed, or %NULL
   static Attribute* new_with_data(AT0, AT1, AT2)(AT0 /*Rectangle*/ ink_rect, AT1 /*Rectangle*/ logical_rect, AT2 /*void*/ data, AttrDataCopyFunc copy_func, GLib2.DestroyNotify destroy_func) nothrow {
      return pango_attr_shape_new_with_data(UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect), UpCast!(void*)(data), copy_func, destroy_func);
   }
}

struct AttrSize {
   Attribute attr;
   int size;
    static import std.bitmanip; mixin(std.bitmanip.bitfields!(
   uint, "absolute", 1,
    uint, "__dummy32A", 31));


   // Unintrospectable function: new() / pango_attr_size_new()
   // Create a new font-size attribute in fractional points.
   // 
   // freed with pango_attribute_destroy().
   // RETURNS: the newly allocated #PangoAttribute, which should be
   // <size>: the font size, in %PANGO_SCALE<!-- -->ths of a point.
   static Attribute* new_()(int size) nothrow {
      return pango_attr_size_new(size);
   }

   // Unintrospectable function: new_absolute() / pango_attr_size_new_absolute()
   // VERSION: 1.8
   // Create a new font-size attribute in device units.
   // 
   // freed with pango_attribute_destroy().
   // RETURNS: the newly allocated #PangoAttribute, which should be
   // <size>: the font size, in %PANGO_SCALE<!-- -->ths of a device unit.
   static Attribute* new_absolute()(int size) nothrow {
      return pango_attr_size_new_absolute(size);
   }
}

struct AttrString {
   Attribute attr;
   char* value;
}

enum AttrType {
   INVALID = 0,
   LANGUAGE = 1,
   FAMILY = 2,
   STYLE = 3,
   WEIGHT = 4,
   VARIANT = 5,
   STRETCH = 6,
   SIZE = 7,
   FONT_DESC = 8,
   FOREGROUND = 9,
   BACKGROUND = 10,
   UNDERLINE = 11,
   STRIKETHROUGH = 12,
   RISE = 13,
   SHAPE = 14,
   SCALE = 15,
   FALLBACK = 16,
   LETTER_SPACING = 17,
   UNDERLINE_COLOR = 18,
   STRIKETHROUGH_COLOR = 19,
   ABSOLUTE_SIZE = 20,
   GRAVITY = 21,
   GRAVITY_HINT = 22
}
struct Attribute {
   AttrClass* klass;
   uint start_index, end_index;


   // Unintrospectable method: copy() / pango_attribute_copy()
   // Make a copy of an attribute.
   // 
   // freed with pango_attribute_destroy().
   // RETURNS: the newly allocated #PangoAttribute, which should be
   Attribute* copy()() nothrow {
      return pango_attribute_copy(&this);
   }
   // Destroy a #PangoAttribute and free all associated memory.
   void destroy()() nothrow {
      pango_attribute_destroy(&this);
   }

   // Compare two attributes for equality. This compares only the
   // actual value of the two attributes and not the ranges that the
   // attributes apply to.
   // RETURNS: %TRUE if the two attributes have the same value.
   // <attr2>: another #PangoAttribute
   int equal(AT0)(AT0 /*Attribute*/ attr2) nothrow {
      return pango_attribute_equal(&this, UpCast!(Attribute*)(attr2));
   }

   // VERSION: 1.20
   // Initializes @attr's klass to @klass,
   // it's start_index to %PANGO_ATTR_INDEX_FROM_TEXT_BEGINNING
   // and end_index to %PANGO_ATTR_INDEX_TO_TEXT_END
   // such that the attribute applies
   // to the entire text by default.
   // <klass>: a #PangoAttributeClass
   void init(AT0)(AT0 /*AttrClass*/ klass) nothrow {
      pango_attribute_init(&this, UpCast!(AttrClass*)(klass));
   }
}


// The #PangoBidiType type represents the bidirectional character
// type of a Unicode character as specified by the
// <ulink url="http://www.unicode.org/reports/tr9/">Unicode bidirectional algorithm</ulink>.
enum BidiType /* Version 1.22 */ {
   L = 0,
   LRE = 1,
   LRO = 2,
   R = 3,
   AL = 4,
   RLE = 5,
   RLO = 6,
   PDF = 7,
   EN = 8,
   ES = 9,
   ET = 10,
   AN = 11,
   CS = 12,
   NSM = 13,
   BN = 14,
   B = 15,
   S = 16,
   WS = 17,
   ON = 18
}
struct Color {
   ushort red, green, blue;


   // Creates a copy of @src, which should be freed with
   // pango_color_free(). Primarily used by language bindings,
   // not that useful otherwise (since colors can just be copied
   // by assignment in C).
   // 
   // be freed with pango_color_free(), or %NULL
   // if @src was %NULL.
   // RETURNS: the newly allocated #PangoColor, which should
   Color* /*new*/ copy()() nothrow {
      return pango_color_copy(&this);
   }
   // Frees a color allocated by pango_color_copy().
   void free()() nothrow {
      pango_color_free(&this);
   }

   // Fill in the fields of a color from a string specification. The
   // string can either one of a large set of standard names. (Taken
   // from the X11 <filename>rgb.txt</filename> file), or it can be a hex value in the
   // form '&num;rgb' '&num;rrggbb' '&num;rrrgggbbb' or '&num;rrrrggggbbbb' where
   // 'r', 'g' and 'b' are hex digits of the red, green, and blue
   // components of the color, respectively. (White in the four
   // forms is '&num;fff' '&num;ffffff' '&num;fffffffff' and '&num;ffffffffffff')
   // 
   // otherwise false.
   // RETURNS: %TRUE if parsing of the specifier succeeded,
   // <spec>: a string specifying the new color
   int parse(AT0)(AT0 /*char*/ spec) nothrow {
      return pango_color_parse(&this, toCString!(char*)(spec));
   }

   // VERSION: 1.16
   // Returns a textual specification of @color in the hexadecimal form
   // <literal>&num;rrrrggggbbbb</literal>, where <literal>r</literal>,
   // <literal>g</literal> and <literal>b</literal> are hex digits representing
   // the red, green, and blue components respectively.
   // RETURNS: a newly-allocated text string that must be freed with g_free().
   char* /*new*/ to_string()() nothrow {
      return pango_color_to_string(&this);
   }
}

struct Context /* : GObject.Object */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // Creates a new #PangoContext initialized to default values.
   // 
   // This function is not particularly useful as it should always
   // be followed by a pango_context_set_font_map() call, and the
   // function pango_font_map_create_context() does these two steps
   // together and hence users are recommended to use that.
   // 
   // If you are using Pango as part of a higher-level system,
   // that system may have it's own way of create a #PangoContext.
   // For instance, the GTK+ toolkit has, among others,
   // gdk_pango_context_get_for_screen(), and
   // gtk_widget_get_pango_context().  Use those instead.
   // 
   // be freed with g_object_unref().
   // RETURNS: the newly allocated #PangoContext, which should
   static Context* /*new*/ new_()() nothrow {
      return pango_context_new();
   }
   static auto opCall()() {
      return pango_context_new();
   }

   // Retrieves the base direction for the context. See
   // pango_context_set_base_dir().
   // RETURNS: the base direction for the context.
   Direction get_base_dir()() nothrow {
      return pango_context_get_base_dir(&this);
   }

   // VERSION: 1.16
   // Retrieves the base gravity for the context. See
   // pango_context_set_base_gravity().
   // RETURNS: the base gravity for the context.
   Gravity get_base_gravity()() nothrow {
      return pango_context_get_base_gravity(&this);
   }

   // Retrieve the default font description for the context.
   // 
   // This value must not be modified or freed.
   // RETURNS: a pointer to the context's default font description.
   FontDescription* /*new*/ get_font_description()() nothrow {
      return pango_context_get_font_description(&this);
   }

   // Unintrospectable method: get_font_map() / pango_context_get_font_map()
   // VERSION: 1.6
   // Gets the #PangoFontmap used to look up fonts for this context.
   // 
   // is owned by Pango and should not be unreferenced.
   // RETURNS: the font map for the #PangoContext. This value
   FontMap* get_font_map()() nothrow {
      return pango_context_get_font_map(&this);
   }

   // VERSION: 1.16
   // Retrieves the gravity for the context. This is similar to
   // pango_context_get_base_gravity(), except for when the base gravity
   // is %PANGO_GRAVITY_AUTO for which pango_gravity_get_for_matrix() is used
   // to return the gravity from the current context matrix.
   // RETURNS: the resolved gravity for the context.
   Gravity get_gravity()() nothrow {
      return pango_context_get_gravity(&this);
   }

   // VERSION: 1.16
   // Retrieves the gravity hint for the context. See
   // pango_context_set_gravity_hint() for details.
   // RETURNS: the gravity hint for the context.
   GravityHint get_gravity_hint()() nothrow {
      return pango_context_get_gravity_hint(&this);
   }

   // Retrieves the global language tag for the context.
   // RETURNS: the global language tag.
   Language* /*new*/ get_language()() nothrow {
      return pango_context_get_language(&this);
   }

   // VERSION: 1.6
   // Gets the transformation matrix that will be applied when
   // rendering with this context. See pango_context_set_matrix().
   // 
   // (which is the same as the identity matrix). The returned
   // matrix is owned by Pango and must not be modified or
   // freed.
   // RETURNS: the matrix, or %NULL if no matrix has been set
   Matrix* get_matrix()() nothrow {
      return pango_context_get_matrix(&this);
   }

   // Get overall metric information for a particular font
   // description.  Since the metrics may be substantially different for
   // different scripts, a language tag can be provided to indicate that
   // the metrics should be retrieved that correspond to the script(s)
   // used by that language.
   // 
   // The #PangoFontDescription is interpreted in the same way as
   // by pango_itemize(), and the family name may be a comma separated
   // list of figures. If characters from multiple of these families
   // would be used to render the string, then the returned fonts would
   // be a composite of the metrics for the fonts loaded for the
   // individual families.
   // 
   // when finished using the object.
   // RETURNS: a #PangoFontMetrics object. The caller must call pango_font_metrics_unref()
   // <desc>: a #PangoFontDescription structure.  %NULL means that the font description from the context will be used.
   // <language>: language tag used to determine which script to get the metrics for. %NULL means that the language tag from the context will be used. If no language tag is set on the context, metrics for the default language (as determined by pango_language_get_default()) will be returned.
   FontMetrics* /*new*/ get_metrics(AT0, AT1)(AT0 /*FontDescription*/ desc, AT1 /*Language*/ language) nothrow {
      return pango_context_get_metrics(&this, UpCast!(FontDescription*)(desc), UpCast!(Language*)(language));
   }

   // List all families for a context.
   // <families>: location to store a pointer to an array of #PangoFontFamily *. This array should be freed with g_free().
   // <n_families>: location to store the number of elements in @descs
   void list_families(AT0)(AT0 /*FontFamily***/ families, int* n_families) nothrow {
      pango_context_list_families(&this, UpCast!(FontFamily***)(families), n_families);
   }

   // Unintrospectable method: load_font() / pango_context_load_font()
   // Loads the font in one of the fontmaps in the context
   // that is the closest match for @desc.
   // RETURNS: the font loaded, or %NULL if no font matched.
   // <desc>: a #PangoFontDescription describing the font to load
   Font* load_font(AT0)(AT0 /*FontDescription*/ desc) nothrow {
      return pango_context_load_font(&this, UpCast!(FontDescription*)(desc));
   }

   // Unintrospectable method: load_fontset() / pango_context_load_fontset()
   // Load a set of fonts in the context that can be used to render
   // a font matching @desc.
   // RETURNS: the fontset, or %NULL if no font matched.
   // <desc>: a #PangoFontDescription describing the fonts to load
   // <language>: a #PangoLanguage the fonts will be used for
   Fontset* load_fontset(AT0, AT1)(AT0 /*FontDescription*/ desc, AT1 /*Language*/ language) nothrow {
      return pango_context_load_fontset(&this, UpCast!(FontDescription*)(desc), UpCast!(Language*)(language));
   }

   // Sets the base direction for the context.
   // 
   // The base direction is used in applying the Unicode bidirectional
   // algorithm; if the @direction is %PANGO_DIRECTION_LTR or
   // %PANGO_DIRECTION_RTL, then the value will be used as the paragraph
   // direction in the Unicode bidirectional algorithm.  A value of
   // %PANGO_DIRECTION_WEAK_LTR or %PANGO_DIRECTION_WEAK_RTL is used only
   // for paragraphs that do not contain any strong characters themselves.
   // <direction>: the new base direction
   void set_base_dir()(Direction direction) nothrow {
      pango_context_set_base_dir(&this, direction);
   }

   // VERSION: 1.16
   // Sets the base gravity for the context.
   // 
   // The base gravity is used in laying vertical text out.
   // <gravity>: the new base gravity
   void set_base_gravity()(Gravity gravity) nothrow {
      pango_context_set_base_gravity(&this, gravity);
   }

   // Set the default font description for the context
   // <desc>: the new pango font description
   void set_font_description(AT0)(AT0 /*FontDescription*/ desc) nothrow {
      pango_context_set_font_description(&this, UpCast!(FontDescription*)(desc));
   }

   // Sets the font map to be searched when fonts are looked-up in this context.
   // This is only for internal use by Pango backends, a #PangoContext obtained
   // via one of the recommended methods should already have a suitable font map.
   // <font_map>: the #PangoFontMap to set.
   void set_font_map(AT0)(AT0 /*FontMap*/ font_map) nothrow {
      pango_context_set_font_map(&this, UpCast!(FontMap*)(font_map));
   }

   // VERSION: 1.16
   // Sets the gravity hint for the context.
   // 
   // The gravity hint is used in laying vertical text out, and is only relevant
   // if gravity of the context as returned by pango_context_get_gravity()
   // is set %PANGO_GRAVITY_EAST or %PANGO_GRAVITY_WEST.
   // <hint>: the new gravity hint
   void set_gravity_hint()(GravityHint hint) nothrow {
      pango_context_set_gravity_hint(&this, hint);
   }

   // Sets the global language tag for the context.  The default language
   // for the locale of the running process can be found using
   // pango_language_get_default().
   // <language>: the new language tag.
   void set_language(AT0)(AT0 /*Language*/ language) nothrow {
      pango_context_set_language(&this, UpCast!(Language*)(language));
   }

   // VERSION: 1.6
   // Sets the transformation matrix that will be applied when rendering
   // with this context. Note that reported metrics are in the user space
   // coordinates before the application of the matrix, not device-space
   // coordinates after the application of the matrix. So, they don't scale
   // with the matrix, though they may change slightly for different
   // matrices, depending on how the text is fit to the pixel grid.
   // <matrix>: a #PangoMatrix, or %NULL to unset any existing matrix. (No matrix set is the same as setting the identity matrix.)
   void set_matrix(AT0)(AT0 /*Matrix*/ matrix) nothrow {
      pango_context_set_matrix(&this, UpCast!(Matrix*)(matrix));
   }
}

struct ContextClass {
}

struct Coverage {

   // Unintrospectable method: copy() / pango_coverage_copy()
   // Copy an existing #PangoCoverage. (This function may now be unnecessary
   // since we refcount the structure. File a bug if you use it.)
   // 
   // with a reference count of one, which
   // should be freed with pango_coverage_unref().
   // RETURNS: the newly allocated #PangoCoverage,
   Coverage* copy()() nothrow {
      return pango_coverage_copy(&this);
   }

   // Determine whether a particular index is covered by @coverage
   // RETURNS: the coverage level of @coverage for character @index_.
   // <index_>: the index to check
   CoverageLevel get()(int index_) nothrow {
      return pango_coverage_get(&this, index_);
   }

   // Set the coverage for each index in @coverage to be the max (better)
   // value of the current coverage for the index and the coverage for
   // the corresponding index in @other.
   // <other>: another #PangoCoverage
   void max(AT0)(AT0 /*Coverage*/ other) nothrow {
      pango_coverage_max(&this, UpCast!(Coverage*)(other));
   }

   // Unintrospectable method: ref() / pango_coverage_ref()
   // Increase the reference count on the #PangoCoverage by one
   // RETURNS: @coverage
   Coverage* ref_()() nothrow {
      return pango_coverage_ref(&this);
   }

   // Modify a particular index within @coverage
   // <index_>: the index to modify
   // <level>: the new level for @index_
   void set()(int index_, CoverageLevel level) nothrow {
      pango_coverage_set(&this, index_, level);
   }

   // Convert a #PangoCoverage structure into a flat binary format
   // <bytes>: location to store result (must be freed with g_free())
   // <n_bytes>: location to store size of result
   void to_bytes(AT0)(AT0 /*ubyte**/ bytes, int* n_bytes) nothrow {
      pango_coverage_to_bytes(&this, UpCast!(ubyte**)(bytes), n_bytes);
   }

   // Decrease the reference count on the #PangoCoverage by one.
   // If the result is zero, free the coverage and all associated memory.
   void unref()() nothrow {
      pango_coverage_unref(&this);
   }

   // Unintrospectable function: from_bytes() / pango_coverage_from_bytes()
   // Convert data generated from pango_converage_to_bytes() back
   // to a #PangoCoverage
   // 
   // the data was invalid.
   // RETURNS: a newly allocated #PangoCoverage, or %NULL if
   // <bytes>: binary data representing a #PangoCoverage
   // <n_bytes>: the size of @bytes in bytes
   static Coverage* from_bytes(AT0)(AT0 /*ubyte*/ bytes, int n_bytes) nothrow {
      return pango_coverage_from_bytes(UpCast!(ubyte*)(bytes), n_bytes);
   }

   // Unintrospectable function: new() / pango_coverage_new()
   // Create a new #PangoCoverage
   // 
   // initialized to %PANGO_COVERAGE_NONE
   // with a reference count of one, which
   // should be freed with pango_coverage_unref().
   // RETURNS: the newly allocated #PangoCoverage,
   static Coverage* new_()() nothrow {
      return pango_coverage_new();
   }
}

enum CoverageLevel {
   NONE = 0,
   FALLBACK = 1,
   APPROXIMATE = 2,
   EXACT = 3
}

// The #PangoDirection type represents a direction in the
// Unicode bidirectional algorithm; not every value in this
// enumeration makes sense for every usage of #PangoDirection;
// for example, the return value of pango_unichar_direction()
// and pango_find_base_dir() cannot be %PANGO_DIRECTION_WEAK_LTR
// or %PANGO_DIRECTION_WEAK_RTL, since every character is either
// neutral or has a strong direction; on the other hand
// %PANGO_DIRECTION_NEUTRAL doesn't make sense to pass
// to pango_itemize_with_base_dir().
// 
// The %PANGO_DIRECTION_TTB_LTR, %PANGO_DIRECTION_TTB_RTL
// values come from an earlier interpretation of this
// enumeration as the writing direction of a block of
// text and are no longer used; See #PangoGravity for how
// vertical text is handled in Pango.
enum Direction {
   LTR = 0,
   RTL = 1,
   TTB_LTR = 2,
   TTB_RTL = 3,
   WEAK_LTR = 4,
   WEAK_RTL = 5,
   NEUTRAL = 6
}
enum ENGINE_TYPE_LANG = "PangoEngineLang";
enum ENGINE_TYPE_SHAPE = "PangoEngineShape";

// The #PangoEllipsizeMode type describes what sort of (if any)
// ellipsization should be applied to a line of text. In
// the ellipsization process characters are removed from the
// text in order to make it fit to a given width and replaced
// with an ellipsis.
enum EllipsizeMode {
   NONE = 0,
   START = 1,
   MIDDLE = 2,
   END = 3
}
struct EngineLang {
}

struct EngineShape {
}

struct Font /* : GObject.Object */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // Frees an array of font descriptions.
   // <descs>: a pointer to an array of #PangoFontDescription, may be %NULL
   // <n_descs>: number of font descriptions in @descs
   static void descriptions_free(AT0)(AT0 /*FontDescription**/ descs, int n_descs) nothrow {
      pango_font_descriptions_free(UpCast!(FontDescription**)(descs), n_descs);
   }

   // Returns a description of the font, with font size set in points.
   // Use pango_font_describe_with_absolute_size() if you want the font
   // size in device units.
   // RETURNS: a newly-allocated #PangoFontDescription object.
   FontDescription* /*new*/ describe()() nothrow {
      return pango_font_describe(&this);
   }

   // VERSION: 1.14
   // Returns a description of the font, with absolute font size set
   // (in device units). Use pango_font_describe() if you want the font
   // size in points.
   // RETURNS: a newly-allocated #PangoFontDescription object.
   FontDescription* /*new*/ describe_with_absolute_size()() nothrow {
      return pango_font_describe_with_absolute_size(&this);
   }

   // Unintrospectable method: find_shaper() / pango_font_find_shaper()
   // Finds the best matching shaper for a font for a particular
   // language tag and character point.
   // RETURNS: the best matching shaper.
   // <language>: the language tag
   // <ch>: a Unicode character.
   EngineShape* find_shaper(AT0)(AT0 /*Language*/ language, uint ch) nothrow {
      return pango_font_find_shaper(&this, UpCast!(Language*)(language), ch);
   }

   // Unintrospectable method: get_coverage() / pango_font_get_coverage()
   // Computes the coverage map for a given font and language tag.
   // RETURNS: a newly-allocated #PangoCoverage object.
   // <language>: the language tag
   Coverage* get_coverage(AT0)(AT0 /*Language*/ language) nothrow {
      return pango_font_get_coverage(&this, UpCast!(Language*)(language));
   }

   // Unintrospectable method: get_font_map() / pango_font_get_font_map()
   // VERSION: 1.10
   // Gets the font map for which the font was created.
   // 
   // Note that the font maintains a <firstterm>weak</firstterm> reference
   // to the font map, so if all references to font map are dropped, the font
   // map will be finalized even if there are fonts created with the font
   // map that are still alive.  In that case this function will return %NULL.
   // It is the responsibility of the user to ensure that the font map is kept
   // alive.  In most uses this is not an issue as a #PangoContext holds
   // a reference to the font map.
   // RETURNS: the #PangoFontMap for the font, or %NULL if @font is %NULL.
   FontMap* get_font_map()() nothrow {
      return pango_font_get_font_map(&this);
   }

   // Gets the logical and ink extents of a glyph within a font. The
   // coordinate system for each rectangle has its origin at the
   // base line and horizontal origin of the character with increasing
   // coordinates extending to the right and down. The macros PANGO_ASCENT(),
   // PANGO_DESCENT(), PANGO_LBEARING(), and PANGO_RBEARING() can be used to convert
   // from the extents rectangle to more traditional font metrics. The units
   // of the rectangles are in 1/PANGO_SCALE of a device unit.
   // 
   // If @font is %NULL, this function gracefully sets some sane values in the
   // output variables and returns.
   // <glyph>: the glyph index
   // <ink_rect>: rectangle used to store the extents of the glyph as drawn or %NULL to indicate that the result is not needed.
   // <logical_rect>: rectangle used to store the logical extents of the glyph or %NULL to indicate that the result is not needed.
   void get_glyph_extents(AT0, AT1)(Glyph glyph, AT0 /*Rectangle*/ ink_rect, AT1 /*Rectangle*/ logical_rect) nothrow {
      pango_font_get_glyph_extents(&this, glyph, UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect));
   }

   // Gets overall metric information for a font. Since the metrics may be
   // substantially different for different scripts, a language tag can
   // be provided to indicate that the metrics should be retrieved that
   // correspond to the script(s) used by that language.
   // 
   // If @font is %NULL, this function gracefully sets some sane values in the
   // output variables and returns.
   // 
   // when finished using the object.
   // RETURNS: a #PangoFontMetrics object. The caller must call pango_font_metrics_unref()
   // <language>: language tag used to determine which script to get the metrics for, or %NULL to indicate to get the metrics for the entire font.
   FontMetrics* /*new*/ get_metrics(AT0)(AT0 /*Language*/ language) nothrow {
      return pango_font_get_metrics(&this, UpCast!(Language*)(language));
   }
}

struct FontDescription {

   // Creates a new font description structure with all fields unset.
   // 
   // should be freed using pango_font_description_free().
   // RETURNS: the newly allocated #PangoFontDescription, which
   static FontDescription* /*new*/ new_()() nothrow {
      return pango_font_description_new();
   }
   static auto opCall()() {
      return pango_font_description_new();
   }

   // Determines if the style attributes of @new_match are a closer match
   // for @desc than those of @old_match are, or if @old_match is %NULL,
   // determines if @new_match is a match at all.
   // Approximate matching is done for
   // weight and style; other style attributes must match exactly.
   // Style attributes are all attributes other than family and size-related
   // attributes.  Approximate matching for style considers PANGO_STYLE_OBLIQUE
   // and PANGO_STYLE_ITALIC as matches, but not as good a match as when the
   // styles are equal.
   // 
   // Note that @old_match must match @desc.
   // RETURNS: %TRUE if @new_match is a better match
   // <old_match>: a #PangoFontDescription, or %NULL
   // <new_match>: a #PangoFontDescription
   int better_match(AT0, AT1)(AT0 /*FontDescription*/ old_match, AT1 /*FontDescription*/ new_match) nothrow {
      return pango_font_description_better_match(&this, UpCast!(FontDescription*)(old_match), UpCast!(FontDescription*)(new_match));
   }

   // Make a copy of a #PangoFontDescription.
   // 
   // be freed with pango_font_description_free(), or %NULL
   // if @desc was %NULL.
   // RETURNS: the newly allocated #PangoFontDescription, which should
   FontDescription* /*new*/ copy()() nothrow {
      return pango_font_description_copy(&this);
   }

   // Like pango_font_description_copy(), but only a shallow copy is made
   // of the family name and other allocated fields. The result can only
   // be used until @desc is modified or freed. This is meant to be used
   // when the copy is only needed temporarily.
   // 
   // be freed with pango_font_description_free(), or %NULL
   // if @desc was %NULL.
   // RETURNS: the newly allocated #PangoFontDescription, which should
   FontDescription* /*new*/ copy_static()() nothrow {
      return pango_font_description_copy_static(&this);
   }

   // Compares two font descriptions for equality. Two font descriptions
   // are considered equal if the fonts they describe are provably identical.
   // This means that their masks do not have to match, as long as other fields
   // are all the same. (Two font descriptions may result in identical fonts
   // being loaded, but still compare %FALSE.)
   // 
   // %FALSE otherwise.
   // RETURNS: %TRUE if the two font descriptions are identical,
   // <desc2>: another #PangoFontDescription
   int equal(AT0)(AT0 /*FontDescription*/ desc2) nothrow {
      return pango_font_description_equal(&this, UpCast!(FontDescription*)(desc2));
   }
   // Frees a font description.
   void free()() nothrow {
      pango_font_description_free(&this);
   }

   // Gets the family name field of a font description. See
   // pango_font_description_set_family().
   // 
   // %NULL if not previously set.  This has the same life-time
   // as the font description itself and should not be freed.
   // RETURNS: the family name field for the font description, or
   char* get_family()() nothrow {
      return pango_font_description_get_family(&this);
   }

   // VERSION: 1.16
   // Gets the gravity field of a font description. See
   // pango_font_description_set_gravity().
   // 
   // pango_font_description_get_set_fields() to find out if
   // the field was explicitly set or not.
   // RETURNS: the gravity field for the font description. Use
   Gravity get_gravity()() nothrow {
      return pango_font_description_get_gravity(&this);
   }

   // Determines which fields in a font description have been set.
   // 
   // fields in @desc that have been set.
   // RETURNS: a bitmask with bits set corresponding to the
   FontMask get_set_fields()() nothrow {
      return pango_font_description_get_set_fields(&this);
   }

   // Gets the size field of a font description.
   // See pango_font_description_set_size().
   // 
   // You must call pango_font_description_get_size_is_absolute()
   // to find out which is the case. Returns 0 if the size field has not
   // previously been set or it has been set to 0 explicitly.
   // Use pango_font_description_get_set_fields() to
   // find out if the field was explicitly set or not.
   // RETURNS: the size field for the font description in points or device units.
   int get_size()() nothrow {
      return pango_font_description_get_size(&this);
   }

   // VERSION: 1.8
   // Determines whether the size of the font is in points (not absolute) or device units (absolute).
   // See pango_font_description_set_size() and pango_font_description_set_absolute_size().
   // 
   // points or device units.  Use pango_font_description_get_set_fields() to
   // find out if the size field of the font description was explicitly set or not.
   // RETURNS: whether the size for the font description is in
   int get_size_is_absolute()() nothrow {
      return pango_font_description_get_size_is_absolute(&this);
   }

   // Gets the stretch field of a font description.
   // See pango_font_description_set_stretch().
   // 
   // pango_font_description_get_set_fields() to find out if
   // the field was explicitly set or not.
   // RETURNS: the stretch field for the font description. Use
   Stretch get_stretch()() nothrow {
      return pango_font_description_get_stretch(&this);
   }

   // Gets the style field of a #PangoFontDescription. See
   // pango_font_description_set_style().
   // 
   // Use pango_font_description_get_set_fields() to find out if
   // the field was explicitly set or not.
   // RETURNS: the style field for the font description.
   Style get_style()() nothrow {
      return pango_font_description_get_style(&this);
   }

   // Gets the variant field of a #PangoFontDescription. See
   // pango_font_description_set_variant().
   // 
   // pango_font_description_get_set_fields() to find out if
   // the field was explicitly set or not.
   // RETURNS: the variant field for the font description. Use
   Variant get_variant()() nothrow {
      return pango_font_description_get_variant(&this);
   }

   // Gets the weight field of a font description. See
   // pango_font_description_set_weight().
   // 
   // pango_font_description_get_set_fields() to find out if
   // the field was explicitly set or not.
   // RETURNS: the weight field for the font description. Use
   Weight get_weight()() nothrow {
      return pango_font_description_get_weight(&this);
   }

   // Computes a hash of a #PangoFontDescription structure suitable
   // to be used, for example, as an argument to g_hash_table_new().
   // The hash value is independent of @desc->mask.
   // RETURNS: the hash value.
   uint hash()() nothrow {
      return pango_font_description_hash(&this);
   }

   // Merges the fields that are set in @desc_to_merge into the fields in
   // @desc.  If @replace_existing is %FALSE, only fields in @desc that
   // are not already set are affected. If %TRUE, then fields that are
   // already set will be replaced as well.
   // 
   // If @desc_to_merge is %NULL, this function performs nothing.
   // <desc_to_merge>: the #PangoFontDescription to merge from, or %NULL
   // <replace_existing>: if %TRUE, replace fields in @desc with the corresponding values from @desc_to_merge, even if they are already exist.
   void merge(AT0)(AT0 /*FontDescription*/ desc_to_merge, int replace_existing) nothrow {
      pango_font_description_merge(&this, UpCast!(FontDescription*)(desc_to_merge), replace_existing);
   }

   // Like pango_font_description_merge(), but only a shallow copy is made
   // of the family name and other allocated fields. @desc can only be
   // used until @desc_to_merge is modified or freed. This is meant
   // to be used when the merged font description is only needed temporarily.
   // <desc_to_merge>: the #PangoFontDescription to merge from
   // <replace_existing>: if %TRUE, replace fields in @desc with the corresponding values from @desc_to_merge, even if they are already exist.
   void merge_static(AT0)(AT0 /*FontDescription*/ desc_to_merge, int replace_existing) nothrow {
      pango_font_description_merge_static(&this, UpCast!(FontDescription*)(desc_to_merge), replace_existing);
   }

   // VERSION: 1.8
   // Sets the size field of a font description, in device units. This is mutually
   // exclusive with pango_font_description_set_size() which sets the font size
   // in points.
   // <size>: the new size, in Pango units. There are %PANGO_SCALE Pango units in one device unit. For an output backend where a device unit is a pixel, a @size value of 10 * PANGO_SCALE gives a 10 pixel font.
   void set_absolute_size()(double size) nothrow {
      pango_font_description_set_absolute_size(&this, size);
   }

   // Sets the family name field of a font description. The family
   // name represents a family of related font styles, and will
   // resolve to a particular #PangoFontFamily. In some uses of
   // #PangoFontDescription, it is also possible to use a comma
   // separated list of family names for this field.
   // <family>: a string representing the family name.
   void set_family(AT0)(AT0 /*char*/ family) nothrow {
      pango_font_description_set_family(&this, toCString!(char*)(family));
   }

   // Like pango_font_description_set_family(), except that no
   // copy of @family is made. The caller must make sure that the
   // string passed in stays around until @desc has been freed
   // or the name is set again. This function can be used if
   // @family is a static string such as a C string literal, or
   // if @desc is only needed temporarily.
   // <family>: a string representing the family name.
   void set_family_static(AT0)(AT0 /*char*/ family) nothrow {
      pango_font_description_set_family_static(&this, toCString!(char*)(family));
   }

   // VERSION: 1.16
   // Sets the gravity field of a font description. The gravity field
   // specifies how the glyphs should be rotated.  If @gravity is
   // %PANGO_GRAVITY_AUTO, this actually unsets the gravity mask on
   // the font description.
   // 
   // This function is seldom useful to the user.  Gravity should normally
   // be set on a #PangoContext.
   // <gravity>: the gravity for the font description.
   void set_gravity()(Gravity gravity) nothrow {
      pango_font_description_set_gravity(&this, gravity);
   }

   // Sets the size field of a font description in fractional points. This is mutually
   // exclusive with pango_font_description_set_absolute_size().
   // <size>: the size of the font in points, scaled by PANGO_SCALE. (That is, a @size value of 10 * PANGO_SCALE is a 10 point font. The conversion factor between points and device units depends on system configuration and the output device. For screen display, a logical DPI of 96 is common, in which case a 10 point font corresponds to a 10 * (96 / 72) = 13.3 pixel font. Use pango_font_description_set_absolute_size() if you need a particular size in device units.
   void set_size()(int size) nothrow {
      pango_font_description_set_size(&this, size);
   }

   // Sets the stretch field of a font description. The stretch field
   // specifies how narrow or wide the font should be.
   // <stretch>: the stretch for the font description
   void set_stretch()(Stretch stretch) nothrow {
      pango_font_description_set_stretch(&this, stretch);
   }

   // Sets the style field of a #PangoFontDescription. The
   // #PangoStyle enumeration describes whether the font is slanted and
   // the manner in which it is slanted; it can be either
   // #PANGO_STYLE_NORMAL, #PANGO_STYLE_ITALIC, or #PANGO_STYLE_OBLIQUE.
   // Most fonts will either have a italic style or an oblique
   // style, but not both, and font matching in Pango will
   // match italic specifications with oblique fonts and vice-versa
   // if an exact match is not found.
   // <style>: the style for the font description
   void set_style()(Style style) nothrow {
      pango_font_description_set_style(&this, style);
   }

   // Sets the variant field of a font description. The #PangoVariant
   // can either be %PANGO_VARIANT_NORMAL or %PANGO_VARIANT_SMALL_CAPS.
   // <variant>: the variant type for the font description.
   void set_variant()(Variant variant) nothrow {
      pango_font_description_set_variant(&this, variant);
   }

   // Sets the weight field of a font description. The weight field
   // specifies how bold or light the font should be. In addition
   // to the values of the #PangoWeight enumeration, other intermediate
   // numeric values are possible.
   // <weight>: the weight for the font description.
   void set_weight()(Weight weight) nothrow {
      pango_font_description_set_weight(&this, weight);
   }

   // Creates a filename representation of a font description. The
   // filename is identical to the result from calling
   // pango_font_description_to_string(), but with underscores instead of
   // characters that are untypical in filenames, and in lower case only.
   // RETURNS: a new string that must be freed with g_free().
   char* /*new*/ to_filename()() nothrow {
      return pango_font_description_to_filename(&this);
   }

   // Creates a string representation of a font description. See
   // pango_font_description_from_string() for a description of the
   // format of the string representation. The family list in the
   // string description will only have a terminating comma if the
   // last word of the list is a valid style option.
   // RETURNS: a new string that must be freed with g_free().
   char* /*new*/ to_string()() nothrow {
      return pango_font_description_to_string(&this);
   }

   // Unsets some of the fields in a #PangoFontDescription.  The unset
   // fields will get back to their default values.
   // <to_unset>: bitmask of fields in the @desc to unset.
   void unset_fields()(FontMask to_unset) nothrow {
      pango_font_description_unset_fields(&this, to_unset);
   }

   // Creates a new font description from a string representation in the
   // form "[FAMILY-LIST] [STYLE-OPTIONS] [SIZE]", where FAMILY-LIST is a
   // comma separated list of families optionally terminated by a comma,
   // STYLE_OPTIONS is a whitespace separated list of words where each WORD
   // describes one of style, variant, weight, stretch, or gravity, and SIZE
   // is a decimal number (size in points) or optionally followed by the
   // unit modifier "px" for absolute size. Any one of the options may
   // be absent.  If FAMILY-LIST is absent, then the family_name field of
   // the resulting font description will be initialized to %NULL.  If
   // STYLE-OPTIONS is missing, then all style options will be set to the
   // default values. If SIZE is missing, the size in the resulting font
   // description will be set to 0.
   // RETURNS: a new #PangoFontDescription.
   // <str>: string representation of a font description.
   static FontDescription* /*new*/ from_string(AT0)(AT0 /*char*/ str) nothrow {
      return pango_font_description_from_string(toCString!(char*)(str));
   }
}

struct FontFace /* : GObject.Object */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // Returns the family, style, variant, weight and stretch of
   // a #PangoFontFace. The size field of the resulting font description
   // will be unset.
   // 
   // holding the description of the face. Use pango_font_description_free()
   // to free the result.
   // RETURNS: a newly-created #PangoFontDescription structure
   FontDescription* /*new*/ describe()() nothrow {
      return pango_font_face_describe(&this);
   }

   // Gets a name representing the style of this face among the
   // different faces in the #PangoFontFamily for the face. This
   // name is unique among all faces in the family and is suitable
   // for displaying to users.
   // 
   // owned by the face object and must not be modified or freed.
   // RETURNS: the face name for the face. This string is
   char* get_face_name()() nothrow {
      return pango_font_face_get_face_name(&this);
   }

   // VERSION: 1.18
   // Returns whether a #PangoFontFace is synthesized by the underlying
   // font rendering engine from another face, perhaps by shearing, emboldening,
   // or lightening it.
   // RETURNS: whether @face is synthesized.
   int is_synthesized()() nothrow {
      return pango_font_face_is_synthesized(&this);
   }

   // VERSION: 1.4
   // List the available sizes for a font. This is only applicable to bitmap
   // fonts. For scalable fonts, stores %NULL at the location pointed to by
   // @sizes and 0 at the location pointed to by @n_sizes. The sizes returned
   // are in Pango units and are sorted in ascending order.
   // <sizes>: location to store a pointer to an array of int. This array should be freed with g_free().
   // <n_sizes>: location to store the number of elements in @sizes
   void list_sizes()(int** sizes, int* n_sizes) nothrow {
      pango_font_face_list_sizes(&this, sizes, n_sizes);
   }
}

struct FontFamily /* : GObject.Object */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // Gets the name of the family. The name is unique among all
   // fonts for the font backend and can be used in a #PangoFontDescription
   // to specify that a face from this family is desired.
   // 
   // by the family object and must not be modified or freed.
   // RETURNS: the name of the family. This string is owned
   char* get_name()() nothrow {
      return pango_font_family_get_name(&this);
   }

   // VERSION: 1.4
   // A monospace font is a font designed for text display where the the
   // characters form a regular grid. For Western languages this would
   // mean that the advance width of all characters are the same, but
   // this categorization also includes Asian fonts which include
   // double-width characters: characters that occupy two grid cells.
   // g_unichar_iswide() returns a result that indicates whether a
   // character is typically double-width in a monospace font.
   // 
   // The best way to find out the grid-cell size is to call
   // pango_font_metrics_get_approximate_digit_width(), since the results
   // of pango_font_metrics_get_approximate_char_width() may be affected
   // by double-width characters.
   // RETURNS: %TRUE if the family is monospace.
   int is_monospace()() nothrow {
      return pango_font_family_is_monospace(&this);
   }

   // Lists the different font faces that make up @family. The faces
   // in a family share a common design, but differ in slant, weight,
   // width and other aspects.
   // <faces>: location to store an array of pointers to #PangoFontFace objects, or %NULL. This array should be freed with g_free() when it is no longer needed.
   // <n_faces>: location to store number of elements in @faces.
   void list_faces(AT0)(AT0 /*FontFace***/ faces, int* n_faces) nothrow {
      pango_font_family_list_faces(&this, UpCast!(FontFace***)(faces), n_faces);
   }
}

struct FontMap /* : GObject.Object */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // Unintrospectable method: create_context() / pango_font_map_create_context()
   // VERSION: 1.22
   // Creates a #PangoContext connected to @fontmap.  This is equivalent
   // to pango_context_new() followed by pango_context_set_font_map().
   // 
   // If you are using Pango as part of a higher-level system,
   // that system may have it's own way of create a #PangoContext.
   // For instance, the GTK+ toolkit has, among others,
   // gdk_pango_context_get_for_screen(), and
   // gtk_widget_get_pango_context().  Use those instead.
   // 
   // be freed with g_object_unref().
   // RETURNS: the newly allocated #PangoContext, which should
   Context* create_context()() nothrow {
      return pango_font_map_create_context(&this);
   }

   // List all families for a fontmap.
   // <families>: location to store a pointer to an array of #PangoFontFamily *. This array should be freed with g_free().
   // <n_families>: location to store the number of elements in @families
   void list_families(AT0)(AT0 /*FontFamily***/ families, int* n_families) nothrow {
      pango_font_map_list_families(&this, UpCast!(FontFamily***)(families), n_families);
   }

   // Unintrospectable method: load_font() / pango_font_map_load_font()
   // Load the font in the fontmap that is the closest match for @desc.
   // RETURNS: the font loaded, or %NULL if no font matched.
   // <context>: the #PangoContext the font will be used with
   // <desc>: a #PangoFontDescription describing the font to load
   Font* load_font(AT0, AT1)(AT0 /*Context*/ context, AT1 /*FontDescription*/ desc) nothrow {
      return pango_font_map_load_font(&this, UpCast!(Context*)(context), UpCast!(FontDescription*)(desc));
   }

   // Unintrospectable method: load_fontset() / pango_font_map_load_fontset()
   // Load a set of fonts in the fontmap that can be used to render
   // a font matching @desc.
   // RETURNS: the fontset, or %NULL if no font matched.
   // <context>: the #PangoContext the font will be used with
   // <desc>: a #PangoFontDescription describing the font to load
   // <language>: a #PangoLanguage the fonts will be used for
   Fontset* load_fontset(AT0, AT1, AT2)(AT0 /*Context*/ context, AT1 /*FontDescription*/ desc, AT2 /*Language*/ language) nothrow {
      return pango_font_map_load_fontset(&this, UpCast!(Context*)(context), UpCast!(FontDescription*)(desc), UpCast!(Language*)(language));
   }
}

enum FontMask {
   FAMILY = 1,
   STYLE = 2,
   VARIANT = 4,
   WEIGHT = 8,
   STRETCH = 16,
   SIZE = 32,
   GRAVITY = 64
}
struct FontMetrics {

   // Gets the approximate character width for a font metrics structure.
   // This is merely a representative value useful, for example, for
   // determining the initial size for a window. Actual characters in
   // text will be wider and narrower than this.
   // RETURNS: the character width, in Pango units.
   int get_approximate_char_width()() nothrow {
      return pango_font_metrics_get_approximate_char_width(&this);
   }

   // Gets the approximate digit width for a font metrics structure.
   // This is merely a representative value useful, for example, for
   // determining the initial size for a window. Actual digits in
   // text can be wider or narrower than this, though this value
   // is generally somewhat more accurate than the result of
   // pango_font_metrics_get_approximate_char_width() for digits.
   // RETURNS: the digit width, in Pango units.
   int get_approximate_digit_width()() nothrow {
      return pango_font_metrics_get_approximate_digit_width(&this);
   }

   // Gets the ascent from a font metrics structure. The ascent is
   // the distance from the baseline to the logical top of a line
   // of text. (The logical top may be above or below the top of the
   // actual drawn ink. It is necessary to lay out the text to figure
   // where the ink will be.)
   // RETURNS: the ascent, in Pango units.
   int get_ascent()() nothrow {
      return pango_font_metrics_get_ascent(&this);
   }

   // Gets the descent from a font metrics structure. The descent is
   // the distance from the baseline to the logical bottom of a line
   // of text. (The logical bottom may be above or below the bottom of the
   // actual drawn ink. It is necessary to lay out the text to figure
   // where the ink will be.)
   // RETURNS: the descent, in Pango units.
   int get_descent()() nothrow {
      return pango_font_metrics_get_descent(&this);
   }

   // VERSION: 1.6
   // Gets the suggested position to draw the strikethrough.
   // The value returned is the distance <emphasis>above</emphasis> the
   // baseline of the top of the strikethrough.
   // RETURNS: the suggested strikethrough position, in Pango units.
   int get_strikethrough_position()() nothrow {
      return pango_font_metrics_get_strikethrough_position(&this);
   }

   // VERSION: 1.6
   // Gets the suggested thickness to draw for the strikethrough.
   // RETURNS: the suggested strikethrough thickness, in Pango units.
   int get_strikethrough_thickness()() nothrow {
      return pango_font_metrics_get_strikethrough_thickness(&this);
   }

   // VERSION: 1.6
   // Gets the suggested position to draw the underline.
   // The value returned is the distance <emphasis>above</emphasis> the
   // baseline of the top of the underline. Since most fonts have
   // underline positions beneath the baseline, this value is typically
   // negative.
   // RETURNS: the suggested underline position, in Pango units.
   int get_underline_position()() nothrow {
      return pango_font_metrics_get_underline_position(&this);
   }

   // VERSION: 1.6
   // Gets the suggested thickness to draw for the underline.
   // RETURNS: the suggested underline thickness, in Pango units.
   int get_underline_thickness()() nothrow {
      return pango_font_metrics_get_underline_thickness(&this);
   }

   // Increase the reference count of a font metrics structure by one.
   // RETURNS: @metrics
   FontMetrics* /*new*/ ref_()() nothrow {
      return pango_font_metrics_ref(&this);
   }

   // Decrease the reference count of a font metrics structure by one. If
   // the result is zero, frees the structure and any associated
   // memory.
   void unref()() nothrow {
      pango_font_metrics_unref(&this);
   }
}

struct Fontset /* : GObject.Object */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // Unintrospectable method: foreach() / pango_fontset_foreach()
   // VERSION: 1.4
   // Iterates through all the fonts in a fontset, calling @func for
   // each one. If @func returns %TRUE, that stops the iteration.
   // <func>: Callback function
   // <data>: data to pass to the callback function
   void foreach_(AT0)(FontsetForeachFunc func, AT0 /*void*/ data) nothrow {
      pango_fontset_foreach(&this, func, UpCast!(void*)(data));
   }

   // Unintrospectable method: get_font() / pango_fontset_get_font()
   // Returns the font in the fontset that contains the best glyph for the
   // Unicode character @wc.
   // 
   // with the font.
   // RETURNS: a #PangoFont. The caller must call g_object_unref when finished
   // <wc>: a Unicode character
   Font* get_font()(uint wc) nothrow {
      return pango_fontset_get_font(&this, wc);
   }

   // Get overall metric information for the fonts in the fontset.
   // 
   // when finished using the object.
   // RETURNS: a #PangoFontMetrics object. The caller must call pango_font_metrics_unref()
   FontMetrics* /*new*/ get_metrics()() nothrow {
      return pango_fontset_get_metrics(&this);
   }
}


// VERSION: 1.4
// A callback function used by pango_fontset_foreach() when enumerating
// the fonts in a fontset.
// RETURNS: if %TRUE, stop iteration and return immediately.
// <fontset>: a #PangoFontset
// <font>: a font from @fontset
// <data>: callback data
extern (C) alias int function (Fontset* fontset, Font* font, void* data) nothrow FontsetForeachFunc;

struct GlyphGeometry {
   GlyphUnit width, x_offset, y_offset;
}

struct GlyphInfo {
   Glyph glyph;
   GlyphGeometry geometry;
   GlyphVisAttr attr;
}

struct GlyphItem {
   Item* item;
   GlyphString* glyphs;


   // Unintrospectable method: apply_attrs() / pango_glyph_item_apply_attrs()
   // VERSION: 1.2
   // Splits a shaped item (PangoGlyphItem) into multiple items based
   // on an attribute list. The idea is that if you have attributes
   // that don't affect shaping, such as color or underline, to avoid
   // affecting shaping, you filter them out (pango_attr_list_filter()),
   // apply the shaping process and then reapply them to the result using
   // this function.
   // 
   // All attributes that start or end inside a cluster are applied
   // to that cluster; for instance, if half of a cluster is underlined
   // and the other-half strikethrough, then the cluster will end
   // up with both underline and strikethrough attributes. In these
   // cases, it may happen that item->extra_attrs for some of the
   // result items can have multiple attributes of the same type.
   // 
   // This function takes ownership of @glyph_item; it will be reused
   // as one of the elements in the list.
   // 
   // @glyph_item. Free the elements using pango_glyph_item_free(),
   // the list using g_slist_free().
   // RETURNS: a list of glyph items resulting from splitting
   // <text>: text that @list applies to
   // <list>: a #PangoAttrList
   GLib2.SList* apply_attrs(AT0, AT1)(AT0 /*char*/ text, AT1 /*AttrList*/ list) nothrow {
      return pango_glyph_item_apply_attrs(&this, toCString!(char*)(text), UpCast!(AttrList*)(list));
   }

   // VERSION: 1.20
   // Make a deep copy of an existing #PangoGlyphItem structure.
   // 
   // be freed with pango_glyph_item_free(), or %NULL
   // if @orig was %NULL.
   // RETURNS: the newly allocated #PangoGlyphItem, which should
   GlyphItem* /*new*/ copy()() nothrow {
      return pango_glyph_item_copy(&this);
   }

   // VERSION: 1.6
   // Frees a #PangoGlyphItem and resources to which it points.
   void free()() nothrow {
      pango_glyph_item_free(&this);
   }

   // VERSION: 1.26
   // Given a #PangoGlyphItem and the corresponding
   // text, determine the screen width corresponding to each character. When
   // multiple characters compose a single cluster, the width of the entire
   // cluster is divided equally among the characters.
   // 
   // See also pango_glyph_string_get_logical_widths().
   // <text>: text that @glyph_item corresponds to (glyph_item->item->offset is an offset from the start of @text)
   // <logical_widths>: an array whose length is the number of characters in glyph_item (equal to glyph_item->item->num_chars) to be filled in with the resulting character widths.
   void get_logical_widths(AT0)(AT0 /*char*/ text, int* logical_widths) nothrow {
      pango_glyph_item_get_logical_widths(&this, toCString!(char*)(text), logical_widths);
   }

   // VERSION: 1.6
   // Adds spacing between the graphemes of @glyph_item to
   // give the effect of typographic letter spacing.
   // <text>: text that @glyph_item corresponds to (glyph_item->item->offset is an offset from the start of @text)
   // <log_attrs>: logical attributes for the item (the first logical attribute refers to the position before the first character in the item)
   // <letter_spacing>: amount of letter spacing to add in Pango units. May be negative, though too large negative values will give ugly results.
   void letter_space(AT0, AT1)(AT0 /*char*/ text, AT1 /*LogAttr*/ log_attrs, int letter_spacing) nothrow {
      pango_glyph_item_letter_space(&this, toCString!(char*)(text), UpCast!(LogAttr*)(log_attrs), letter_spacing);
   }

   // VERSION: 1.2
   // Modifies @orig to cover only the text after @split_index, and
   // returns a new item that covers the text before @split_index that
   // used to be in @orig. You can think of @split_index as the length of
   // the returned item. @split_index may not be 0, and it may not be
   // greater than or equal to the length of @orig (that is, there must
   // be at least one byte assigned to each item, you can't create a
   // zero-length item).
   // 
   // This function is similar in function to pango_item_split() (and uses
   // it internally.)
   // 
   // @split_index, which should be freed
   // with pango_glyph_item_free().
   // RETURNS: the newly allocated item representing text before
   // <text>: text to which positions in @orig apply
   // <split_index>: byte index of position to split item, relative to the start of the item
   GlyphItem* /*new*/ split(AT0)(AT0 /*char*/ text, int split_index) nothrow {
      return pango_glyph_item_split(&this, toCString!(char*)(text), split_index);
   }
}

struct GlyphItemIter {
   GlyphItem* glyph_item;
   char* text;
   int start_glyph, start_index, start_char, end_glyph, end_index, end_char;


   // VERSION: 1.22
   // Make a shallow copy of an existing #PangoGlyphItemIter structure.
   // 
   // be freed with pango_glyph_item_iter_free(), or %NULL
   // if @orig was %NULL.
   // RETURNS: the newly allocated #PangoGlyphItemIter, which should
   GlyphItemIter* /*new*/ copy()() nothrow {
      return pango_glyph_item_iter_copy(&this);
   }

   // VERSION: 1.22
   // Frees a #PangoGlyphItemIter created by pango_glyph_item_iter_copy().
   void free()() nothrow {
      pango_glyph_item_iter_free(&this);
   }

   // VERSION: 1.22
   // Initializes a #PangoGlyphItemIter structure to point to the
   // last cluster in a glyph item.
   // See #PangoGlyphItemIter for details of cluster orders.
   // RETURNS: %FALSE if there are no clusters in the glyph item
   // <glyph_item>: the glyph item to iterate over
   // <text>: text corresponding to the glyph item
   int init_end(AT0, AT1)(AT0 /*GlyphItem*/ glyph_item, AT1 /*char*/ text) nothrow {
      return pango_glyph_item_iter_init_end(&this, UpCast!(GlyphItem*)(glyph_item), toCString!(char*)(text));
   }

   // VERSION: 1.22
   // Initializes a #PangoGlyphItemIter structure to point to the
   // first cluster in a glyph item.
   // See #PangoGlyphItemIter for details of cluster orders.
   // RETURNS: %FALSE if there are no clusters in the glyph item
   // <glyph_item>: the glyph item to iterate over
   // <text>: text corresponding to the glyph item
   int init_start(AT0, AT1)(AT0 /*GlyphItem*/ glyph_item, AT1 /*char*/ text) nothrow {
      return pango_glyph_item_iter_init_start(&this, UpCast!(GlyphItem*)(glyph_item), toCString!(char*)(text));
   }

   // VERSION: 1.22
   // Advances the iterator to the next cluster in the glyph item.
   // See #PangoGlyphItemIter for details of cluster orders.
   // 
   // last cluster.
   // RETURNS: %TRUE if the iterator was advanced, %FALSE if we were already on the
   int next_cluster()() nothrow {
      return pango_glyph_item_iter_next_cluster(&this);
   }

   // VERSION: 1.22
   // Moves the iterator to the preceding cluster in the glyph item.
   // See #PangoGlyphItemIter for details of cluster orders.
   // 
   // first cluster.
   // RETURNS: %TRUE if the iterator was moved, %FALSE if we were already on the
   int prev_cluster()() nothrow {
      return pango_glyph_item_iter_prev_cluster(&this);
   }
}

struct GlyphString {
   int num_glyphs;
   GlyphInfo* glyphs;
   int* log_clusters;
   private int space;


   // Create a new #PangoGlyphString.
   // 
   // should be freed with pango_glyph_string_free().
   // RETURNS: the newly allocated #PangoGlyphString, which
   static GlyphString* /*new*/ new_()() nothrow {
      return pango_glyph_string_new();
   }
   static auto opCall()() {
      return pango_glyph_string_new();
   }

   // Copy a glyph string and associated storage.
   // 
   // should be freed with pango_glyph_string_free(),
   // or %NULL if @string was %NULL.
   // RETURNS: the newly allocated #PangoGlyphString, which
   GlyphString* /*new*/ copy()() nothrow {
      return pango_glyph_string_copy(&this);
   }

   // Compute the logical and ink extents of a glyph string. See the documentation
   // for pango_font_get_glyph_extents() for details about the interpretation
   // of the rectangles.
   // <font>: a #PangoFont
   // <ink_rect>: rectangle used to store the extents of the glyph string as drawn or %NULL to indicate that the result is not needed.
   // <logical_rect>: rectangle used to store the logical extents of the glyph string or %NULL to indicate that the result is not needed.
   void extents(AT0, AT1, AT2)(AT0 /*Font*/ font, AT1 /*Rectangle*/ ink_rect, AT2 /*Rectangle*/ logical_rect) nothrow {
      pango_glyph_string_extents(&this, UpCast!(Font*)(font), UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect));
   }

   // Computes the extents of a sub-portion of a glyph string. The extents are
   // relative to the start of the glyph string range (the origin of their
   // coordinate system is at the start of the range, not at the start of the entire
   // glyph string).
   // <start>: start index
   // <end>: end index (the range is the set of bytes with
   // <font>: a #PangoFont
   // <ink_rect>: rectangle used to store the extents of the glyph string range as drawn or %NULL to indicate that the result is not needed.
   // <logical_rect>: rectangle used to store the logical extents of the glyph string range or %NULL to indicate that the result is not needed.
   void extents_range(AT0, AT1, AT2)(int start, int end, AT0 /*Font*/ font, AT1 /*Rectangle*/ ink_rect, AT2 /*Rectangle*/ logical_rect) nothrow {
      pango_glyph_string_extents_range(&this, start, end, UpCast!(Font*)(font), UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect));
   }
   // Free a glyph string and associated storage.
   void free()() nothrow {
      pango_glyph_string_free(&this);
   }

   // Given a #PangoGlyphString resulting from pango_shape() and the corresponding
   // text, determine the screen width corresponding to each character. When
   // multiple characters compose a single cluster, the width of the entire
   // cluster is divided equally among the characters.
   // 
   // See also pango_glyph_item_get_logical_widths().
   // <text>: the text corresponding to the glyphs
   // <length>: the length of @text, in bytes
   // <embedding_level>: the embedding level of the string
   // <logical_widths>: an array whose length is the number of characters in text (equal to g_utf8_strlen (text, length) unless text has NUL bytes) to be filled in with the resulting character widths.
   void get_logical_widths(AT0)(AT0 /*char*/ text, int length, int embedding_level, int* logical_widths) nothrow {
      pango_glyph_string_get_logical_widths(&this, toCString!(char*)(text), length, embedding_level, logical_widths);
   }

   // VERSION: 1.14
   // Computes the logical width of the glyph string as can also be computed
   // using pango_glyph_string_extents().  However, since this only computes the
   // width, it's much faster.  This is in fact only a convenience function that
   // computes the sum of geometry.width for each glyph in the @glyphs.
   // RETURNS: the logical width of the glyph string.
   int get_width()() nothrow {
      return pango_glyph_string_get_width(&this);
   }

   // Converts from character position to x position. (X position
   // is measured from the left edge of the run). Character positions
   // are computed by dividing up each cluster into equal portions.
   // <text>: the text for the run
   // <length>: the number of bytes (not characters) in @text.
   // <analysis>: the analysis information return from pango_itemize()
   // <index_>: the byte index within @text
   // <trailing>: whether we should compute the result for the beginning (%FALSE) or end (%TRUE) of the character.
   // <x_pos>: location to store result
   void index_to_x(AT0, AT1)(AT0 /*char*/ text, int length, AT1 /*Analysis*/ analysis, int index_, int trailing, int* x_pos) nothrow {
      pango_glyph_string_index_to_x(&this, toCString!(char*)(text), length, UpCast!(Analysis*)(analysis), index_, trailing, x_pos);
   }

   // Resize a glyph string to the given length.
   // <new_len>: the new length of the string.
   void set_size()(int new_len) nothrow {
      pango_glyph_string_set_size(&this, new_len);
   }

   // Convert from x offset to character position. Character positions
   // are computed by dividing up each cluster into equal portions.
   // In scripts where positioning within a cluster is not allowed
   // (such as Thai), the returned value may not be a valid cursor
   // position; the caller must combine the result with the logical
   // attributes for the text to compute the valid cursor position.
   // <text>: the text for the run
   // <length>: the number of bytes (not characters) in text.
   // <analysis>: the analysis information return from pango_itemize()
   // <x_pos>: the x offset (in Pango units)
   // <index_>: location to store calculated byte index within @text
   // <trailing>: location to store a boolean indicating whether the user clicked on the leading or trailing edge of the character.
   void x_to_index(AT0, AT1)(AT0 /*char*/ text, int length, AT1 /*Analysis*/ analysis, int x_pos, int* index_, int* trailing) nothrow {
      pango_glyph_string_x_to_index(&this, toCString!(char*)(text), length, UpCast!(Analysis*)(analysis), x_pos, index_, trailing);
   }
}

struct GlyphVisAttr {
    static import std.bitmanip; mixin(std.bitmanip.bitfields!(
   uint, "is_cluster_start", 1,
    uint, "__dummy32A", 31));
}


// The #PangoGravity type represents the orientation of glyphs in a segment
// of text.  This is useful when rendering vertical text layouts.  In
// those situations, the layout is rotated using a non-identity PangoMatrix,
// and then glyph orientation is controlled using #PangoGravity.
// Not every value in this enumeration makes sense for every usage of
// #PangoGravity; for example, %PANGO_GRAVITY_AUTO only can be passed to
// pango_context_set_base_gravity() and can only be returned by
// pango_context_get_base_gravity().
// 
// See also: #PangoGravityHint
enum Gravity /* Version 1.16 */ {
   SOUTH = 0,
   EAST = 1,
   NORTH = 2,
   WEST = 3,
   AUTO = 4
}

// The #PangoGravityHint defines how horizontal scripts should behave in a
// vertical context.  That is, English excerpt in a vertical paragraph for
// example.
// 
// See #PangoGravity.
enum GravityHint /* Version 1.16 */ {
   NATURAL = 0,
   STRONG = 1,
   LINE = 2
}
struct Item {
   int offset, length, num_chars;
   Analysis analysis;


   // Creates a new #PangoItem structure initialized to default values.
   // 
   // be freed with pango_item_free().
   // RETURNS: the newly allocated #PangoItem, which should
   static Item* /*new*/ new_()() nothrow {
      return pango_item_new();
   }
   static auto opCall()() {
      return pango_item_new();
   }

   // Copy an existing #PangoItem structure.
   // 
   // be freed with pango_item_free(), or %NULL if
   // @item was NULL.
   // RETURNS: the newly allocated #PangoItem, which should
   Item* /*new*/ copy()() nothrow {
      return pango_item_copy(&this);
   }
   // Free a #PangoItem and all associated memory.
   void free()() nothrow {
      pango_item_free(&this);
   }

   // Modifies @orig to cover only the text after @split_index, and
   // returns a new item that covers the text before @split_index that
   // used to be in @orig. You can think of @split_index as the length of
   // the returned item. @split_index may not be 0, and it may not be
   // greater than or equal to the length of @orig (that is, there must
   // be at least one byte assigned to each item, you can't create a
   // zero-length item). @split_offset is the length of the first item in
   // chars, and must be provided because the text used to generate the
   // item isn't available, so pango_item_split() can't count the char
   // length of the split items itself.
   // 
   // should be freed with pango_item_free().
   // RETURNS: new item representing text before @split_index, which
   // <split_index>: byte index of position to split item, relative to the start of the item
   // <split_offset>: number of chars between start of @orig and @split_index
   Item* /*new*/ split()(int split_index, int split_offset) nothrow {
      return pango_item_split(&this, split_index, split_offset);
   }
}

struct Language {

   // Get a string that is representative of the characters needed to
   // render a particular language.
   // 
   // The sample text may be a pangram, but is not necessarily.  It is chosen to
   // be demonstrative of normal text in the language, as well as exposing font
   // feature requirements unique to the language.  It is suitable for use
   // as sample text in a font selection dialog.
   // 
   // If @language is %NULL, the default language as found by
   // pango_language_get_default() is used.
   // 
   // If Pango does not have a sample string for @language, the classic
   // "The quick brown fox..." is returned.  This can be detected by
   // comparing the returned pointer value to that returned for (non-existent)
   // language code "xx".  That is, compare to:
   // <informalexample><programlisting>
   // pango_language_get_sample_string (pango_language_from_string ("xx"))
   // </programlisting></informalexample>
   // 
   // and should not be freed.
   // RETURNS: the sample string. This value is owned by Pango
   char* get_sample_string()() nothrow {
      return pango_language_get_sample_string(&this);
   }

   // VERSION: 1.22
   // Determines the scripts used to to write @language.
   // If nothing is known about the language tag @language,
   // or if @language is %NULL, then %NULL is returned.
   // The list of scripts returned starts with the script that the
   // language uses most and continues to the one it uses least.
   // 
   // The value @num_script points at will be set to the number
   // of scripts in the returned array (or zero if %NULL is returned).
   // 
   // Most languages use only one script for writing, but there are
   // some that use two (Latin and Cyrillic for example), and a few
   // use three (Japanese for example).  Applications should not make
   // any assumptions on the maximum number of scripts returned
   // though, except that it is positive if the return value is not
   // %NULL, and it is a small number.
   // 
   // The pango_language_includes_script() function uses this function
   // internally.
   // 
   // number of entries in the array stored in @num_scripts, or
   // %NULL if Pango does not have any information about this
   // particular language tag (also the case if @language is %NULL).
   // The returned array is owned by Pango and should not be modified
   // or freed.
   // RETURNS: An array of #PangoScript values, with the
   // <num_scripts>: location to return number of scripts, or %NULL
   Script* get_scripts()(int* num_scripts) nothrow {
      return pango_language_get_scripts(&this, num_scripts);
   }

   // VERSION: 1.4
   // Determines if @script is one of the scripts used to
   // write @language. The returned value is conservative;
   // if nothing is known about the language tag @language,
   // %TRUE will be returned, since, as far as Pango knows,
   // @script might be used to write @language.
   // 
   // This routine is used in Pango's itemization process when
   // determining if a supplied language tag is relevant to
   // a particular section of text. It probably is not useful for
   // applications in most circumstances.
   // 
   // This function uses pango_language_get_scripts() internally.
   // 
   // to write @language or if nothing is known about @language
   // (including the case that @language is %NULL),
   // %FALSE otherwise.
   // RETURNS: %TRUE if @script is one of the scripts used
   // <script>: a #PangoScript
   int includes_script()(Script script) nothrow {
      return pango_language_includes_script(&this, script);
   }

   // Checks if a language tag matches one of the elements in a list of
   // language ranges. A language tag is considered to match a range
   // in the list if the range is '*', the range is exactly the tag,
   // or the range is a prefix of the tag, and the character after it
   // in the tag is '-'.
   // RETURNS: %TRUE if a match was found.
   // <range_list>: a list of language ranges, separated by ';', ':', ',', or space characters. Each element must either be '*', or a RFC 3066 language range canonicalized as by pango_language_from_string()
   int matches(AT0)(AT0 /*char*/ range_list) nothrow {
      return pango_language_matches(&this, toCString!(char*)(range_list));
   }

   // Gets the RFC-3066 format string representing the given language tag. 
   // 
   // Pango and should not be freed.
   // RETURNS: a string representing the language tag.  This is owned by
   char* to_string()() nothrow {
      return pango_language_to_string(&this);
   }

   // Take a RFC-3066 format language tag as a string and convert it to a
   // #PangoLanguage pointer that can be efficiently copied (copy the
   // pointer) and compared with other language tags (compare the
   // pointer.)
   // 
   // This function first canonicalizes the string by converting it to
   // lowercase, mapping '_' to '-', and stripping all characters other
   // than letters and '-'.
   // 
   // Use pango_language_get_default() if you want to get the #PangoLanguage for
   // the current locale of the process.
   // 
   // if @language was %NULL.  The returned pointer will be valid
   // forever after, and should not be freed.
   // RETURNS: an opaque pointer to a #PangoLanguage structure, or %NULL
   // <language>: a string representing a language tag, or %NULL
   static Language* /*new*/ from_string(AT0)(AT0 /*char*/ language) nothrow {
      return pango_language_from_string(toCString!(char*)(language));
   }

   // VERSION: 1.16
   // Returns the #PangoLanguage for the current locale of the process.
   // Note that this can change over the life of an application.
   // 
   // On Unix systems, this is the return value is derived from
   // <literal>setlocale(LC_CTYPE, NULL)</literal>, and the user can
   // affect this through the environment variables LC_ALL, LC_CTYPE or
   // LANG (checked in that order). The locale string typically is in
   // the form lang_COUNTRY, where lang is an ISO-639 language code, and
   // COUNTRY is an ISO-3166 country code. For instance, sv_FI for
   // Swedish as written in Finland or pt_BR for Portuguese as written in
   // Brazil.
   // 
   // On Windows, the C library does not use any such environment
   // variables, and setting them won't affect the behavior of functions
   // like ctime(). The user sets the locale through the Regional Options
   // in the Control Panel. The C library (in the setlocale() function)
   // does not use country and language codes, but country and language
   // names spelled out in English.
   // However, this function does check the above environment
   // variables, and does return a Unix-style locale string based on
   // either said environment variables or the thread's current locale.
   // 
   // Your application should call <literal>setlocale(LC_ALL, "");</literal>
   // for the user settings to take effect.  Gtk+ does this in its initialization
   // functions automatically (by calling gtk_set_locale()).
   // See <literal>man setlocale</literal> for more details.
   // 
   // freed.
   // RETURNS: the default language as a #PangoLanguage, must not be
   static Language* /*new*/ get_default()() nothrow {
      return pango_language_get_default();
   }
}

struct Layout /* : GObject.Object */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent object;
   GObject2.Object method_parent;


   // Create a new #PangoLayout object with attributes initialized to
   // default values for a particular #PangoContext.
   // 
   // count of one, which should be freed with
   // g_object_unref().
   // RETURNS: the newly allocated #PangoLayout, with a reference
   // <context>: a #PangoContext
   static Layout* /*new*/ new_(AT0)(AT0 /*Context*/ context) nothrow {
      return pango_layout_new(UpCast!(Context*)(context));
   }
   static auto opCall(AT0)(AT0 /*Context*/ context) {
      return pango_layout_new(UpCast!(Context*)(context));
   }

   // Forces recomputation of any state in the #PangoLayout that
   // might depend on the layout's context. This function should
   // be called if you make changes to the context subsequent
   // to creating the layout.
   void context_changed()() nothrow {
      pango_layout_context_changed(&this);
   }

   // Unintrospectable method: copy() / pango_layout_copy()
   // Does a deep copy-by-value of the @src layout. The attribute list,
   // tab array, and text from the original layout are all copied by
   // value.
   // 
   // count of one, which should be freed with
   // g_object_unref().
   // RETURNS: the newly allocated #PangoLayout, with a reference
   Layout* copy()() nothrow {
      return pango_layout_copy(&this);
   }

   // Gets the alignment for the layout: how partial lines are
   // positioned within the horizontal space available.
   // RETURNS: the alignment.
   Alignment get_alignment()() nothrow {
      return pango_layout_get_alignment(&this);
   }

   // Gets the attribute list for the layout, if any.
   // RETURNS: a #PangoAttrList.
   AttrList* /*new*/ get_attributes()() nothrow {
      return pango_layout_get_attributes(&this);
   }

   // VERSION: 1.4
   // Gets whether to calculate the bidirectional base direction
   // for the layout according to the contents of the layout.
   // See pango_layout_set_auto_dir().
   // 
   // is computed from the layout's contents, %FALSE otherwise.
   // RETURNS: %TRUE if the bidirectional base direction
   int get_auto_dir()() nothrow {
      return pango_layout_get_auto_dir(&this);
   }

   // VERSION: 1.22
   // Gets the Y position of baseline of the first line in @layout.
   // RETURNS: baseline of first line, from top of @layout.
   int get_baseline()() nothrow {
      return pango_layout_get_baseline(&this);
   }

   // Retrieves the #PangoContext used for this layout.
   // 
   // This does not have an additional refcount added, so if you want to
   // keep a copy of this around, you must reference it yourself.
   // RETURNS: the #PangoContext for the layout.
   Context* get_context()() nothrow {
      return pango_layout_get_context(&this);
   }

   // Given an index within a layout, determines the positions that of the
   // strong and weak cursors if the insertion point is at that
   // index. The position of each cursor is stored as a zero-width
   // rectangle. The strong cursor location is the location where
   // characters of the directionality equal to the base direction of the
   // layout are inserted.  The weak cursor location is the location
   // where characters of the directionality opposite to the base
   // direction of the layout are inserted.
   // <index_>: the byte index of the cursor
   // <strong_pos>: location to store the strong cursor position (may be %NULL)
   // <weak_pos>: location to store the weak cursor position (may be %NULL)
   void get_cursor_pos(AT0, AT1)(int index_, /*out*/ AT0 /*Rectangle*/ strong_pos, /*out*/ AT1 /*Rectangle*/ weak_pos) nothrow {
      pango_layout_get_cursor_pos(&this, index_, UpCast!(Rectangle*)(strong_pos), UpCast!(Rectangle*)(weak_pos));
   }

   // VERSION: 1.6
   // Gets the type of ellipsization being performed for @layout.
   // See pango_layout_set_ellipsize()
   // 
   // 
   // Use pango_layout_is_ellipsized() to query whether any paragraphs
   // were actually ellipsized.
   // RETURNS: the current ellipsization mode for @layout.
   EllipsizeMode get_ellipsize()() nothrow {
      return pango_layout_get_ellipsize(&this);
   }

   // Computes the logical and ink extents of @layout. Logical extents
   // are usually what you want for positioning things.  Note that both extents
   // may have non-zero x and y.  You may want to use those to offset where you
   // render the layout.  Not doing that is a very typical bug that shows up as
   // right-to-left layouts not being correctly positioned in a layout with
   // a set width.
   // 
   // The extents are given in layout coordinates and in Pango units; layout
   // coordinates begin at the top left corner of the layout.
   // <ink_rect>: rectangle used to store the extents of the layout as drawn or %NULL to indicate that the result is not needed.
   // <logical_rect>: rectangle used to store the logical extents of the layout or %NULL to indicate that the result is not needed.
   void get_extents(AT0, AT1)(/*out*/ AT0 /*Rectangle*/ ink_rect, /*out*/ AT1 /*Rectangle*/ logical_rect) nothrow {
      pango_layout_get_extents(&this, UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect));
   }

   // VERSION: 1.8
   // Gets the font description for the layout, if any.
   // 
   // or %NULL if the font description from the layout's
   // context is inherited. This value is owned by the layout
   // and must not be modified or freed.
   // RETURNS: a pointer to the layout's font description,
   FontDescription* get_font_description()() nothrow {
      return pango_layout_get_font_description(&this);
   }

   // VERSION: 1.20
   // Gets the height of layout used for ellipsization.  See
   // pango_layout_set_height() for details.
   // 
   // number of lines if negative.
   // RETURNS: the height, in Pango units if positive, or
   int get_height()() nothrow {
      return pango_layout_get_height(&this);
   }

   // Gets the paragraph indent width in Pango units. A negative value
   // indicates a hanging indentation.
   // RETURNS: the indent in Pango units.
   int get_indent()() nothrow {
      return pango_layout_get_indent(&this);
   }

   // Returns an iterator to iterate over the visual extents of the layout.
   // 
   // pango_layout_iter_free().
   // RETURNS: the new #PangoLayoutIter that should be freed using
   LayoutIter* /*new*/ get_iter()() nothrow {
      return pango_layout_get_iter(&this);
   }

   // Gets whether each complete line should be stretched to fill the entire
   // width of the layout.
   // RETURNS: the justify.
   int get_justify()() nothrow {
      return pango_layout_get_justify(&this);
   }

   // Retrieves a particular line from a #PangoLayout.
   // 
   // Use the faster pango_layout_get_line_readonly() if you do not plan
   // to modify the contents of the line (glyphs, glyph widths, etc.).
   // 
   // index is out of range. This layout line can
   // be ref'ed and retained, but will become invalid
   // if changes are made to the #PangoLayout.
   // RETURNS: the requested #PangoLayoutLine, or %NULL if the
   // <line>: the index of a line, which must be between 0 and <literal>pango_layout_get_line_count(layout) - 1</literal>, inclusive.
   LayoutLine* /*new*/ get_line()(int line) nothrow {
      return pango_layout_get_line(&this, line);
   }

   // Retrieves the count of lines for the @layout.
   // RETURNS: the line count.
   int get_line_count()() nothrow {
      return pango_layout_get_line_count(&this);
   }

   // VERSION: 1.16
   // Retrieves a particular line from a #PangoLayout.
   // 
   // This is a faster alternative to pango_layout_get_line(),
   // but the user is not expected
   // to modify the contents of the line (glyphs, glyph widths, etc.).
   // 
   // index is out of range. This layout line can
   // be ref'ed and retained, but will become invalid
   // if changes are made to the #PangoLayout.
   // No changes should be made to the line.
   // RETURNS: the requested #PangoLayoutLine, or %NULL if the
   // <line>: the index of a line, which must be between 0 and <literal>pango_layout_get_line_count(layout) - 1</literal>, inclusive.
   LayoutLine* /*new*/ get_line_readonly()(int line) nothrow {
      return pango_layout_get_line_readonly(&this, line);
   }

   // Returns the lines of the @layout as a list.
   // 
   // Use the faster pango_layout_get_lines_readonly() if you do not plan
   // to modify the contents of the lines (glyphs, glyph widths, etc.).
   // 
   // the lines in the layout. This points to internal data of the #PangoLayout
   // and must be used with care. It will become invalid on any change to the layout's
   // text or properties.
   // RETURNS: a #GSList containing
   GLib2.SList* get_lines()() nothrow {
      return pango_layout_get_lines(&this);
   }

   // VERSION: 1.16
   // Returns the lines of the @layout as a list.
   // 
   // This is a faster alternative to pango_layout_get_lines(),
   // but the user is not expected
   // to modify the contents of the lines (glyphs, glyph widths, etc.).
   // 
   // the lines in the layout. This points to internal data of the #PangoLayout and
   // must be used with care. It will become invalid on any change to the layout's
   // text or properties.  No changes should be made to the lines.
   // RETURNS: a #GSList containing
   GLib2.SList* get_lines_readonly()() nothrow {
      return pango_layout_get_lines_readonly(&this);
   }

   // Retrieves an array of logical attributes for each character in
   // the @layout.
   // <attrs>:  location to store a pointer to an array of logical attributes This value must be freed with g_free().
   // <n_attrs>: location to store the number of the attributes in the array. (The stored value will be one more than the total number of characters in the layout, since there need to be attributes corresponding to both the position before the first character and the position after the last character.)
   void get_log_attrs(AT0)(/*out*/ AT0 /*LogAttr**/ attrs, /*out*/ int* n_attrs) nothrow {
      pango_layout_get_log_attrs(&this, UpCast!(LogAttr**)(attrs), n_attrs);
   }

   // Computes the logical and ink extents of @layout in device units.
   // This function just calls pango_layout_get_extents() followed by
   // two pango_extents_to_pixels() calls, rounding @ink_rect and @logical_rect
   // such that the rounded rectangles fully contain the unrounded one (that is,
   // passes them as first argument to pango_extents_to_pixels()).
   // <ink_rect>: rectangle used to store the extents of the layout as drawn or %NULL to indicate that the result is not needed.
   // <logical_rect>: rectangle used to store the logical extents of the layout or %NULL to indicate that the result is not needed.
   void get_pixel_extents(AT0, AT1)(/*out*/ AT0 /*Rectangle*/ ink_rect, /*out*/ AT1 /*Rectangle*/ logical_rect) nothrow {
      pango_layout_get_pixel_extents(&this, UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect));
   }

   // Determines the logical width and height of a #PangoLayout
   // in device units. (pango_layout_get_size() returns the width
   // and height scaled by %PANGO_SCALE.) This
   // is simply a convenience function around
   // pango_layout_get_pixel_extents().
   // <width>: location to store the logical width, or %NULL
   // <height>: location to store the logical height, or %NULL
   void get_pixel_size()(/*out*/ int* width, /*out*/ int* height) nothrow {
      pango_layout_get_pixel_size(&this, width, height);
   }

   // Obtains the value set by pango_layout_set_single_paragraph_mode().
   // 
   // paragraph separator characters, %FALSE otherwise.
   // RETURNS: %TRUE if the layout does not break paragraphs at
   int get_single_paragraph_mode()() nothrow {
      return pango_layout_get_single_paragraph_mode(&this);
   }

   // Determines the logical width and height of a #PangoLayout
   // in Pango units (device units scaled by %PANGO_SCALE). This
   // is simply a convenience function around pango_layout_get_extents().
   // <width>: location to store the logical width, or %NULL
   // <height>: location to store the logical height, or %NULL
   void get_size()(/*out*/ int* width, /*out*/ int* height) nothrow {
      pango_layout_get_size(&this, width, height);
   }

   // Gets the amount of spacing between the lines of the layout.
   // RETURNS: the spacing in Pango units.
   int get_spacing()() nothrow {
      return pango_layout_get_spacing(&this);
   }

   // Gets the current #PangoTabArray used by this layout. If no
   // #PangoTabArray has been set, then the default tabs are in use
   // and %NULL is returned. Default tabs are every 8 spaces.
   // The return value should be freed with pango_tab_array_free().
   // RETURNS: a copy of the tabs for this layout, or %NULL.
   TabArray* /*new*/ get_tabs()() nothrow {
      return pango_layout_get_tabs(&this);
   }

   // Gets the text in the layout. The returned text should not
   // be freed or modified.
   // RETURNS: the text in the @layout.
   char* get_text()() nothrow {
      return pango_layout_get_text(&this);
   }

   // VERSION: 1.16
   // Counts the number unknown glyphs in @layout.  That is, zero if
   // glyphs for all characters in the layout text were found, or more
   // than zero otherwise.
   // 
   // This function can be used to determine if there are any fonts
   // available to render all characters in a certain string, or when
   // used in combination with %PANGO_ATTR_FALLBACK, to check if a
   // certain font supports all the characters in the string.
   // RETURNS: The number of unknown glyphs in @layout.
   int get_unknown_glyphs_count()() nothrow {
      return pango_layout_get_unknown_glyphs_count(&this);
   }

   // Gets the width to which the lines of the #PangoLayout should wrap.
   // RETURNS: the width in Pango units, or -1 if no width set.
   int get_width()() nothrow {
      return pango_layout_get_width(&this);
   }

   // Gets the wrap mode for the layout.
   // 
   // Use pango_layout_is_wrapped() to query whether any paragraphs
   // were actually wrapped.
   // RETURNS: active wrap mode.
   WrapMode get_wrap()() nothrow {
      return pango_layout_get_wrap(&this);
   }

   // Converts from byte @index_ within the @layout to line and X position.
   // (X position is measured from the left edge of the line)
   // <index_>: the byte index of a grapheme within the layout.
   // <trailing>: an integer indicating the edge of the grapheme to retrieve the position of. If 0, the trailing edge of the grapheme, if > 0, the leading of the grapheme.
   // <line>: location to store resulting line index. (which will between 0 and pango_layout_get_line_count(layout) - 1)
   void index_to_line_x()(int index_, int trailing, /*out*/ int* line, int* x_pos) nothrow {
      pango_layout_index_to_line_x(&this, index_, trailing, line, x_pos);
   }

   // Converts from an index within a #PangoLayout to the onscreen position
   // corresponding to the grapheme at that index, which is represented
   // as rectangle.  Note that <literal>pos->x</literal> is always the leading
   // edge of the grapheme and <literal>pos->x + pos->width</literal> the trailing
   // edge of the grapheme. If the directionality of the grapheme is right-to-left,
   // then <literal>pos->width</literal> will be negative.
   // <index_>: byte index within @layout
   // <pos>: rectangle in which to store the position of the grapheme
   void index_to_pos(AT0)(int index_, /*out*/ AT0 /*Rectangle*/ pos) nothrow {
      pango_layout_index_to_pos(&this, index_, UpCast!(Rectangle*)(pos));
   }

   // VERSION: 1.16
   // Queries whether the layout had to ellipsize any paragraphs.
   // 
   // This returns %TRUE if the ellipsization mode for @layout
   // is not %PANGO_ELLIPSIZE_NONE, a positive width is set on @layout,
   // and there are paragraphs exceeding that width that have to be
   // ellipsized.
   // 
   // otherwise.
   // RETURNS: %TRUE if any paragraphs had to be ellipsized, %FALSE
   int is_ellipsized()() nothrow {
      return pango_layout_is_ellipsized(&this);
   }

   // VERSION: 1.16
   // Queries whether the layout had to wrap any paragraphs.
   // 
   // This returns %TRUE if a positive width is set on @layout,
   // ellipsization mode of @layout is set to %PANGO_ELLIPSIZE_NONE,
   // and there are paragraphs exceeding the layout width that have
   // to be wrapped.
   // 
   // otherwise.
   // RETURNS: %TRUE if any paragraphs had to be wrapped, %FALSE
   int is_wrapped()() nothrow {
      return pango_layout_is_wrapped(&this);
   }

   // Computes a new cursor position from an old position and
   // a count of positions to move visually. If @direction is positive,
   // then the new strong cursor position will be one position
   // to the right of the old cursor position. If @direction is negative,
   // then the new strong cursor position will be one position
   // to the left of the old cursor position.
   // 
   // In the presence of bidirectional text, the correspondence
   // between logical and visual order will depend on the direction
   // of the current run, and there may be jumps when the cursor
   // is moved off of the end of a run.
   // 
   // Motion here is in cursor positions, not in characters, so a
   // single call to pango_layout_move_cursor_visually() may move the
   // cursor over multiple characters when multiple characters combine
   // to form a single grapheme.
   // <strong>: whether the moving cursor is the strong cursor or the weak cursor. The strong cursor is the cursor corresponding to text insertion in the base direction for the layout.
   // <old_index>: the byte index of the grapheme for the old index
   // <old_trailing>: if 0, the cursor was at the trailing edge of the grapheme indicated by @old_index, if > 0, the cursor was at the leading edge.
   // <direction>: direction to move cursor. A negative value indicates motion to the left.
   // <new_index>: location to store the new cursor byte index. A value of -1 indicates that the cursor has been moved off the beginning of the layout. A value of %G_MAXINT indicates that the cursor has been moved off the end of the layout.
   // <new_trailing>: number of characters to move forward from the location returned for @new_index to get the position where the cursor should be displayed. This allows distinguishing the position at the beginning of one line from the position at the end of the preceding line. @new_index is always on the line where the cursor should be displayed.
   void move_cursor_visually()(int strong, int old_index, int old_trailing, int direction, /*out*/ int* new_index, int* new_trailing) nothrow {
      pango_layout_move_cursor_visually(&this, strong, old_index, old_trailing, direction, new_index, new_trailing);
   }

   // Sets the alignment for the layout: how partial lines are
   // positioned within the horizontal space available.
   // <alignment>: the alignment
   void set_alignment()(Alignment alignment) nothrow {
      pango_layout_set_alignment(&this, alignment);
   }

   // Sets the text attributes for a layout object.
   // References @attrs, so the caller can unref its reference.
   // <attrs>: a #PangoAttrList, can be %NULL
   void set_attributes(AT0)(AT0 /*AttrList*/ attrs) nothrow {
      pango_layout_set_attributes(&this, UpCast!(AttrList*)(attrs));
   }

   // VERSION: 1.4
   // Sets whether to calculate the bidirectional base direction
   // for the layout according to the contents of the layout;
   // when this flag is on (the default), then paragraphs in
   // (Arabic and Hebrew principally), will have right-to-left
   // layout, paragraphs with letters from other scripts will
   // have left-to-right layout. Paragraphs with only neutral
   // characters get their direction from the surrounding paragraphs.
   // 
   // When %FALSE, the choice between left-to-right and
   // right-to-left layout is done according to the base direction
   // of the layout's #PangoContext. (See pango_context_set_base_dir()).
   // 
   // When the auto-computed direction of a paragraph differs from the
   // base direction of the context, the interpretation of
   // %PANGO_ALIGN_LEFT and %PANGO_ALIGN_RIGHT are swapped.
   // <auto_dir>: if %TRUE, compute the bidirectional base direction from the layout's contents.
   void set_auto_dir()(int auto_dir) nothrow {
      pango_layout_set_auto_dir(&this, auto_dir);
   }

   // VERSION: 1.6
   // Sets the type of ellipsization being performed for @layout.
   // Depending on the ellipsization mode @ellipsize text is
   // removed from the start, middle, or end of text so they
   // fit within the width and height of layout set with
   // pango_layout_set_width() and pango_layout_set_height().
   // 
   // If the layout contains characters such as newlines that
   // force it to be layed out in multiple paragraphs, then whether
   // each paragraph is ellipsized separately or the entire layout
   // is ellipsized as a whole depends on the set height of the layout.
   // See pango_layout_set_height() for details.
   // <ellipsize>: the new ellipsization mode for @layout
   void set_ellipsize()(EllipsizeMode ellipsize) nothrow {
      pango_layout_set_ellipsize(&this, ellipsize);
   }

   // Sets the default font description for the layout. If no font
   // description is set on the layout, the font description from
   // the layout's context is used.
   // <desc>: the new #PangoFontDescription, or %NULL to unset the current font description
   void set_font_description(AT0)(AT0 /*FontDescription*/ desc) nothrow {
      pango_layout_set_font_description(&this, UpCast!(FontDescription*)(desc));
   }

   // VERSION: 1.20
   // Sets the height to which the #PangoLayout should be ellipsized at.  There
   // are two different behaviors, based on whether @height is positive or
   // negative.
   // 
   // If @height is positive, it will be the maximum height of the layout.  Only
   // lines would be shown that would fit, and if there is any text omitted,
   // an ellipsis added.  At least one line is included in each paragraph regardless
   // of how small the height value is.  A value of zero will render exactly one
   // line for the entire layout.
   // 
   // If @height is negative, it will be the (negative of) maximum number of lines per
   // paragraph.  That is, the total number of lines shown may well be more than
   // this value if the layout contains multiple paragraphs of text.
   // The default value of -1 means that first line of each paragraph is ellipsized.
   // This behvaior may be changed in the future to act per layout instead of per
   // paragraph.  File a bug against pango at <ulink
   // url="http://bugzilla.gnome.org/">http://bugzilla.gnome.org/</ulink> if your
   // code relies on this behavior.
   // 
   // Height setting only has effect if a positive width is set on
   // @layout and ellipsization mode of @layout is not %PANGO_ELLIPSIZE_NONE.
   // The behavior is undefined if a height other than -1 is set and
   // ellipsization mode is set to %PANGO_ELLIPSIZE_NONE, and may change in the
   // future.
   // <height>: the desired height of the layout in Pango units if positive, or desired number of lines if negative.
   void set_height()(int height) nothrow {
      pango_layout_set_height(&this, height);
   }

   // Sets the width in Pango units to indent each paragraph. A negative value
   // of @indent will produce a hanging indentation. That is, the first line will
   // have the full width, and subsequent lines will be indented by the
   // absolute value of @indent.
   // 
   // The indent setting is ignored if layout alignment is set to
   // %PANGO_ALIGN_CENTER.
   // <indent>: the amount by which to indent.
   void set_indent()(int indent) nothrow {
      pango_layout_set_indent(&this, indent);
   }

   // Sets whether each complete line should be stretched to
   // fill the entire width of the layout. This stretching is typically
   // done by adding whitespace, but for some scripts (such as Arabic),
   // the justification may be done in more complex ways, like extending
   // the characters.
   // 
   // Note that this setting is not implemented and so is ignored in Pango
   // older than 1.18.
   // <justify>: whether the lines in the layout should be justified.
   void set_justify()(int justify) nothrow {
      pango_layout_set_justify(&this, justify);
   }

   // Same as pango_layout_set_markup_with_accel(), but
   // the markup text isn't scanned for accelerators.
   // <markup>: marked-up text
   // <length>: length of marked-up text in bytes, or -1 if @markup is nul-terminated
   void set_markup(AT0)(AT0 /*char*/ markup, int length) nothrow {
      pango_layout_set_markup(&this, toCString!(char*)(markup), length);
   }

   // Sets the layout text and attribute list from marked-up text (see
   // <link linkend="PangoMarkupFormat">markup format</link>). Replaces
   // the current text and attribute list.
   // 
   // If @accel_marker is nonzero, the given character will mark the
   // character following it as an accelerator. For example, @accel_marker
   // might be an ampersand or underscore. All characters marked
   // as an accelerator will receive a %PANGO_UNDERLINE_LOW attribute,
   // and the first character so marked will be returned in @accel_char.
   // Two @accel_marker characters following each other produce a single
   // literal @accel_marker character.
   // <markup>: marked-up text (see <link linkend="PangoMarkupFormat">markup format</link>)
   // <length>: length of marked-up text in bytes, or -1 if @markup is nul-terminated
   // <accel_marker>: marker for accelerators in the text
   // <accel_char>: return location for first located accelerator, or %NULL
   void set_markup_with_accel(AT0, AT1)(AT0 /*char*/ markup, int length, dchar accel_marker, AT1 /*dchar*/ accel_char) nothrow {
      pango_layout_set_markup_with_accel(&this, toCString!(char*)(markup), length, accel_marker, UpCast!(dchar*)(accel_char));
   }

   // If @setting is %TRUE, do not treat newlines and similar characters
   // as paragraph separators; instead, keep all text in a single paragraph,
   // and display a glyph for paragraph separator characters. Used when
   // you want to allow editing of newlines on a single text line.
   // <setting>: new setting
   void set_single_paragraph_mode()(int setting) nothrow {
      pango_layout_set_single_paragraph_mode(&this, setting);
   }

   // Sets the amount of spacing in Pango unit between the lines of the
   // layout.
   // <spacing>: the amount of spacing
   void set_spacing()(int spacing) nothrow {
      pango_layout_set_spacing(&this, spacing);
   }

   // Sets the tabs to use for @layout, overriding the default tabs
   // (by default, tabs are every 8 spaces). If @tabs is %NULL, the default
   // tabs are reinstated. @tabs is copied into the layout; you must
   // free your copy of @tabs yourself.
   // <tabs>: a #PangoTabArray, or %NULL
   void set_tabs(AT0)(AT0 /*TabArray*/ tabs) nothrow {
      pango_layout_set_tabs(&this, UpCast!(TabArray*)(tabs));
   }

   // Sets the text of the layout.
   // 
   // Note that if you have used
   // pango_layout_set_markup() or pango_layout_set_markup_with_accel() on
   // @layout before, you may want to call pango_layout_set_attributes() to clear
   // the attributes set on the layout from the markup as this function does not
   // clear attributes.
   // <text>: a valid UTF-8 string
   // <length>: maximum length of @text, in bytes. -1 indicates that the string is nul-terminated and the length should be calculated.  The text will also be truncated on encountering a nul-termination even when @length is positive.
   void set_text(AT0)(AT0 /*char*/ text, int length) nothrow {
      pango_layout_set_text(&this, toCString!(char*)(text), length);
   }

   // Sets the width to which the lines of the #PangoLayout should wrap or
   // ellipsized.  The default value is -1: no width set.
   // <width>: the desired width in Pango units, or -1 to indicate that no wrapping or ellipsization should be performed.
   void set_width()(int width) nothrow {
      pango_layout_set_width(&this, width);
   }

   // Sets the wrap mode; the wrap mode only has effect if a width
   // is set on the layout with pango_layout_set_width().
   // To turn off wrapping, set the width to -1.
   // <wrap>: the wrap mode
   void set_wrap()(WrapMode wrap) nothrow {
      pango_layout_set_wrap(&this, wrap);
   }

   // Converts from X and Y position within a layout to the byte
   // index to the character at that logical position. If the
   // Y position is not inside the layout, the closest position is chosen
   // (the position will be clamped inside the layout). If the
   // X position is not within the layout, then the start or the
   // end of the line is chosen as  described for pango_layout_x_to_index().
   // If either the X or Y positions were not inside the layout, then the
   // function returns %FALSE; on an exact hit, it returns %TRUE.
   // RETURNS: %TRUE if the coordinates were inside text, %FALSE otherwise.
   // <x>: the X offset (in Pango units) from the left edge of the layout.
   // <y>: the Y offset (in Pango units) from the top edge of the layout
   // <index_>: location to store calculated byte index
   // <trailing>: location to store a integer indicating where in the grapheme the user clicked. It will either be zero, or the number of characters in the grapheme. 0 represents the trailing edge of the grapheme.
   int xy_to_index()(int x, int y, /*out*/ int* index_, /*out*/ int* trailing) nothrow {
      return pango_layout_xy_to_index(&this, x, y, index_, trailing);
   }
}

struct LayoutClass {
}

struct LayoutIter {

   // Determines whether @iter is on the last line of the layout.
   // RETURNS: %TRUE if @iter is on the last line.
   int at_last_line()() nothrow {
      return pango_layout_iter_at_last_line(&this);
   }

   // VERSION: 1.20
   // Copies a #PangLayoutIter.
   // 
   // be freed with pango_layout_iter_free(), or %NULL if
   // @iter was %NULL.
   // RETURNS: the newly allocated #PangoLayoutIter, which should
   LayoutIter* /*new*/ copy()() nothrow {
      return pango_layout_iter_copy(&this);
   }
   // Frees an iterator that's no longer in use.
   void free()() nothrow {
      pango_layout_iter_free(&this);
   }

   // Gets the Y position of the current line's baseline, in layout
   // coordinates (origin at top left of the entire layout).
   // RETURNS: baseline of current line.
   int get_baseline()() nothrow {
      return pango_layout_iter_get_baseline(&this);
   }

   // Gets the extents of the current character, in layout coordinates
   // (origin is the top left of the entire layout). Only logical extents
   // can sensibly be obtained for characters; ink extents make sense only
   // down to the level of clusters.
   // <logical_rect>: rectangle to fill with logical extents
   void get_char_extents(AT0)(AT0 /*Rectangle*/ logical_rect) nothrow {
      pango_layout_iter_get_char_extents(&this, UpCast!(Rectangle*)(logical_rect));
   }

   // Gets the extents of the current cluster, in layout coordinates
   // (origin is the top left of the entire layout).
   // <ink_rect>: rectangle to fill with ink extents, or %NULL
   // <logical_rect>: rectangle to fill with logical extents, or %NULL
   void get_cluster_extents(AT0, AT1)(AT0 /*Rectangle*/ ink_rect, AT1 /*Rectangle*/ logical_rect) nothrow {
      pango_layout_iter_get_cluster_extents(&this, UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect));
   }

   // Gets the current byte index. Note that iterating forward by char
   // moves in visual order, not logical order, so indexes may not be
   // sequential. Also, the index may be equal to the length of the text
   // in the layout, if on the %NULL run (see pango_layout_iter_get_run()).
   // RETURNS: current byte index.
   int get_index()() nothrow {
      return pango_layout_iter_get_index(&this);
   }

   // Unintrospectable method: get_layout() / pango_layout_iter_get_layout()
   // VERSION: 1.20
   // Gets the layout associated with a #PangoLayoutIter.
   // RETURNS: the layout associated with @iter.
   Layout* get_layout()() nothrow {
      return pango_layout_iter_get_layout(&this);
   }

   // Obtains the extents of the #PangoLayout being iterated
   // over. @ink_rect or @logical_rect can be %NULL if you
   // aren't interested in them.
   // <ink_rect>: rectangle to fill with ink extents, or %NULL
   // <logical_rect>: rectangle to fill with logical extents, or %NULL
   void get_layout_extents(AT0, AT1)(AT0 /*Rectangle*/ ink_rect, AT1 /*Rectangle*/ logical_rect) nothrow {
      pango_layout_iter_get_layout_extents(&this, UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect));
   }

   // Gets the current line.
   // 
   // Use the faster pango_layout_iter_get_line_readonly() if you do not plan
   // to modify the contents of the line (glyphs, glyph widths, etc.).
   // RETURNS: the current line.
   LayoutLine* /*new*/ get_line()() nothrow {
      return pango_layout_iter_get_line(&this);
   }

   // Obtains the extents of the current line. @ink_rect or @logical_rect
   // can be %NULL if you aren't interested in them. Extents are in layout
   // coordinates (origin is the top-left corner of the entire
   // #PangoLayout).  Thus the extents returned by this function will be
   // the same width/height but not at the same x/y as the extents
   // returned from pango_layout_line_get_extents().
   // <ink_rect>: rectangle to fill with ink extents, or %NULL
   // <logical_rect>: rectangle to fill with logical extents, or %NULL
   void get_line_extents(AT0, AT1)(AT0 /*Rectangle*/ ink_rect, AT1 /*Rectangle*/ logical_rect) nothrow {
      pango_layout_iter_get_line_extents(&this, UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect));
   }

   // VERSION: 1.16
   // Gets the current line for read-only access.
   // 
   // This is a faster alternative to pango_layout_iter_get_line(),
   // but the user is not expected
   // to modify the contents of the line (glyphs, glyph widths, etc.).
   // RETURNS: the current line, that should not be modified.
   LayoutLine* /*new*/ get_line_readonly()() nothrow {
      return pango_layout_iter_get_line_readonly(&this);
   }

   // Divides the vertical space in the #PangoLayout being iterated over
   // between the lines in the layout, and returns the space belonging to
   // the current line.  A line's range includes the line's logical
   // extents, plus half of the spacing above and below the line, if
   // pango_layout_set_spacing() has been called to set layout spacing.
   // The Y positions are in layout coordinates (origin at top left of the
   // entire layout).
   // <y0_>: start of line
   // <y1_>: end of line
   void get_line_yrange()(int* y0_, int* y1_) nothrow {
      pango_layout_iter_get_line_yrange(&this, y0_, y1_);
   }

   // Unintrospectable method: get_run() / pango_layout_iter_get_run()
   // Gets the current run. When iterating by run, at the end of each
   // line, there's a position with a %NULL run, so this function can return
   // %NULL. The %NULL run at the end of each line ensures that all lines have
   // at least one run, even lines consisting of only a newline.
   // 
   // Use the faster pango_layout_iter_get_run_readonly() if you do not plan
   // to modify the contents of the run (glyphs, glyph widths, etc.).
   // RETURNS: the current run.
   LayoutRun* get_run()() nothrow {
      return pango_layout_iter_get_run(&this);
   }

   // Gets the extents of the current run in layout coordinates
   // (origin is the top left of the entire layout).
   // <ink_rect>: rectangle to fill with ink extents, or %NULL
   // <logical_rect>: rectangle to fill with logical extents, or %NULL
   void get_run_extents(AT0, AT1)(AT0 /*Rectangle*/ ink_rect, AT1 /*Rectangle*/ logical_rect) nothrow {
      pango_layout_iter_get_run_extents(&this, UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect));
   }

   // Unintrospectable method: get_run_readonly() / pango_layout_iter_get_run_readonly()
   // VERSION: 1.16
   // Gets the current run. When iterating by run, at the end of each
   // line, there's a position with a %NULL run, so this function can return
   // %NULL. The %NULL run at the end of each line ensures that all lines have
   // at least one run, even lines consisting of only a newline.
   // 
   // This is a faster alternative to pango_layout_iter_get_run(),
   // but the user is not expected
   // to modify the contents of the run (glyphs, glyph widths, etc.).
   // RETURNS: the current run, that should not be modified.
   LayoutRun* get_run_readonly()() nothrow {
      return pango_layout_iter_get_run_readonly(&this);
   }

   // Moves @iter forward to the next character in visual order. If @iter was already at
   // the end of the layout, returns %FALSE.
   // RETURNS: whether motion was possible.
   int next_char()() nothrow {
      return pango_layout_iter_next_char(&this);
   }

   // Moves @iter forward to the next cluster in visual order. If @iter
   // was already at the end of the layout, returns %FALSE.
   // RETURNS: whether motion was possible.
   int next_cluster()() nothrow {
      return pango_layout_iter_next_cluster(&this);
   }

   // Moves @iter forward to the start of the next line. If @iter is
   // already on the last line, returns %FALSE.
   // RETURNS: whether motion was possible.
   int next_line()() nothrow {
      return pango_layout_iter_next_line(&this);
   }

   // Moves @iter forward to the next run in visual order. If @iter was
   // already at the end of the layout, returns %FALSE.
   // RETURNS: whether motion was possible.
   int next_run()() nothrow {
      return pango_layout_iter_next_run(&this);
   }
}

struct LayoutLine {
   Layout* layout;
   int start_index, length;
   GLib2.SList* runs;
    static import std.bitmanip; mixin(std.bitmanip.bitfields!(
   uint, "is_paragraph_start", 1,
   uint, "resolved_dir", 3,
    uint, "__dummy32A", 28));


   // Computes the logical and ink extents of a layout line. See
   // pango_font_get_glyph_extents() for details about the interpretation
   // of the rectangles.
   // <ink_rect>: rectangle used to store the extents of the glyph string as drawn, or %NULL
   // <logical_rect>: rectangle used to store the logical extents of the glyph string, or %NULL
   void get_extents(AT0, AT1)(/*out*/ AT0 /*Rectangle*/ ink_rect, /*out*/ AT1 /*Rectangle*/ logical_rect) nothrow {
      pango_layout_line_get_extents(&this, UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect));
   }

   // Computes the logical and ink extents of @layout_line in device units.
   // This function just calls pango_layout_line_get_extents() followed by
   // two pango_extents_to_pixels() calls, rounding @ink_rect and @logical_rect
   // such that the rounded rectangles fully contain the unrounded one (that is,
   // passes them as first argument to pango_extents_to_pixels()).
   // <ink_rect>: rectangle used to store the extents of the glyph string as drawn, or %NULL
   // <logical_rect>: rectangle used to store the logical extents of the glyph string, or %NULL
   void get_pixel_extents(AT0, AT1)(/*out*/ AT0 /*Rectangle*/ ink_rect, /*out*/ AT1 /*Rectangle*/ logical_rect) nothrow {
      pango_layout_line_get_pixel_extents(&this, UpCast!(Rectangle*)(ink_rect), UpCast!(Rectangle*)(logical_rect));
   }

   // Gets a list of visual ranges corresponding to a given logical range.
   // This list is not necessarily minimal - there may be consecutive
   // ranges which are adjacent. The ranges will be sorted from left to
   // right. The ranges are with respect to the left edge of the entire
   // layout, not with respect to the line.
   // <start_index>: Start byte index of the logical range. If this value is less than the start index for the line, then the first range will extend all the way to the leading edge of the layout. Otherwise it will start at the leading edge of the first character.
   // <end_index>: Ending byte index of the logical range. If this value is greater than the end index for the line, then the last range will extend all the way to the trailing edge of the layout. Otherwise, it will end at the trailing edge of the last character.
   // <ranges>:  location to store a pointer to an array of ranges. The array will be of length <literal>2*n_ranges</literal>, with each range starting at <literal>(*ranges)[2*n]</literal> and of width <literal>(*ranges)[2*n + 1] - (*ranges)[2*n]</literal>. This array must be freed with g_free(). The coordinates are relative to the layout and are in Pango units.
   // <n_ranges>: The number of ranges stored in @ranges.
   void get_x_ranges()(int start_index, int end_index, /*out*/ int** ranges, /*out*/ int* n_ranges) nothrow {
      pango_layout_line_get_x_ranges(&this, start_index, end_index, ranges, n_ranges);
   }

   // Converts an index within a line to a X position.
   // <index_>: byte offset of a grapheme within the layout
   // <trailing>: an integer indicating the edge of the grapheme to retrieve the position of. If > 0, the trailing edge of the grapheme, if 0, the leading of the grapheme.
   // <x_pos>: location to store the x_offset (in Pango unit)
   void index_to_x()(int index_, int trailing, /*out*/ int* x_pos) nothrow {
      pango_layout_line_index_to_x(&this, index_, trailing, x_pos);
   }

   // VERSION: 1.10
   // Increase the reference count of a #PangoLayoutLine by one.
   // RETURNS: the line passed in.
   LayoutLine* /*new*/ ref_()() nothrow {
      return pango_layout_line_ref(&this);
   }

   // Decrease the reference count of a #PangoLayoutLine by one.
   // If the result is zero, the line and all associated memory
   // will be freed.
   void unref()() nothrow {
      pango_layout_line_unref(&this);
   }

   // Converts from x offset to the byte index of the corresponding
   // character within the text of the layout. If @x_pos is outside the line,
   // @index_ and @trailing will point to the very first or very last position
   // in the line. This determination is based on the resolved direction
   // of the paragraph; for example, if the resolved direction is
   // right-to-left, then an X position to the right of the line (after it)
   // results in 0 being stored in @index_ and @trailing. An X position to the
   // left of the line results in @index_ pointing to the (logical) last
   // grapheme in the line and @trailing being set to the number of characters
   // in that grapheme. The reverse is true for a left-to-right line.
   // RETURNS: %FALSE if @x_pos was outside the line, %TRUE if inside
   // <x_pos>: the X offset (in Pango units) from the left edge of the line.
   // <index_>: location to store calculated byte index for the grapheme in which the user clicked.
   // <trailing>: location to store an integer indicating where in the grapheme the user clicked. It will either be zero, or the number of characters in the grapheme. 0 represents the leading edge of the grapheme.
   int x_to_index()(int x_pos, /*out*/ int* index_, /*out*/ int* trailing) nothrow {
      return pango_layout_line_x_to_index(&this, x_pos, index_, trailing);
   }
}

struct LogAttr {
    static import std.bitmanip; mixin(std.bitmanip.bitfields!(
   uint, "is_line_break", 1,
   uint, "is_mandatory_break", 1,
   uint, "is_char_break", 1,
   uint, "is_white", 1,
   uint, "is_cursor_position", 1,
   uint, "is_word_start", 1,
   uint, "is_word_end", 1,
   uint, "is_sentence_boundary", 1,
   uint, "is_sentence_start", 1,
   uint, "is_sentence_end", 1,
   uint, "backspace_deletes_character", 1,
   uint, "is_expandable_space", 1,
   uint, "is_word_boundary", 1,
    uint, "__dummy32A", 19));
}


// A structure specifying a transformation between user-space
// coordinates and device coordinates. The transformation
// is given by
// 
// <programlisting>
// x_device = x_user * matrix->xx + y_user * matrix->xy + matrix->x0;
// y_device = x_user * matrix->yx + y_user * matrix->yy + matrix->y0;
// </programlisting>
struct Matrix /* Version 1.6 */ {
   double xx, xy, yx, yy, x0, y0;


   // VERSION: 1.6
   // Changes the transformation represented by @matrix to be the
   // transformation given by first applying transformation
   // given by @new_matrix then applying the original transformation.
   // <new_matrix>: a #PangoMatrix
   void concat(AT0)(AT0 /*Matrix*/ new_matrix) nothrow {
      pango_matrix_concat(&this, UpCast!(Matrix*)(new_matrix));
   }

   // VERSION: 1.6
   // Copies a #PangoMatrix.
   // 
   // be freed with pango_matrix_free(), or %NULL if
   // @matrix was %NULL.
   // RETURNS: the newly allocated #PangoMatrix, which should
   Matrix* /*new*/ copy()() nothrow {
      return pango_matrix_copy(&this);
   }

   // VERSION: 1.6
   // Free a #PangoMatrix created with pango_matrix_copy().
   void free()() nothrow {
      pango_matrix_free(&this);
   }

   // VERSION: 1.12
   // Returns the scale factor of a matrix on the height of the font.
   // That is, the scale factor in the direction perpendicular to the
   // vector that the X coordinate is mapped to.
   // 
   // or 1.0 if @matrix is %NULL.
   // RETURNS: the scale factor of @matrix on the height of the font,
   double get_font_scale_factor()() nothrow {
      return pango_matrix_get_font_scale_factor(&this);
   }

   // VERSION: 1.6
   // Changes the transformation represented by @matrix to be the
   // transformation given by first rotating by @degrees degrees
   // counter-clockwise then applying the original transformation.
   // <degrees>: degrees to rotate counter-clockwise
   void rotate()(double degrees) nothrow {
      pango_matrix_rotate(&this, degrees);
   }

   // VERSION: 1.6
   // Changes the transformation represented by @matrix to be the
   // transformation given by first scaling by @sx in the X direction
   // and @sy in the Y direction then applying the original
   // transformation.
   // <scale_x>: amount to scale by in X direction
   // <scale_y>: amount to scale by in Y direction
   void scale()(double scale_x, double scale_y) nothrow {
      pango_matrix_scale(&this, scale_x, scale_y);
   }

   // VERSION: 1.16
   // Transforms the distance vector (@dx,@dy) by @matrix. This is
   // similar to pango_matrix_transform_point() except that the translation
   // components of the transformation are ignored. The calculation of
   // the returned vector is as follows:
   // 
   // <programlisting>
   // dx2 = dx1 * xx + dy1 * xy;
   // dy2 = dx1 * yx + dy1 * yy;
   // </programlisting>
   // 
   // Affine transformations are position invariant, so the same vector
   // always transforms to the same vector. If (@x1,@y1) transforms
   // to (@x2,@y2) then (@x1+@dx1,@y1+@dy1) will transform to
   // (@x1+@dx2,@y1+@dy2) for all values of @x1 and @x2.
   // <dx>: in/out X component of a distance vector
   // <dy>: yn/out Y component of a distance vector
   void transform_distance(AT0, AT1)(AT0 /*double*/ dx, AT1 /*double*/ dy) nothrow {
      pango_matrix_transform_distance(&this, UpCast!(double*)(dx), UpCast!(double*)(dy));
   }

   // VERSION: 1.16
   // First transforms the @rect using @matrix, then calculates the bounding box
   // of the transformed rectangle.  The rectangle should be in device units
   // (pixels).
   // 
   // This function is useful for example when you want to draw a rotated
   // @PangoLayout to an image buffer, and want to know how large the image
   // should be and how much you should shift the layout when rendering.
   // 
   // For better accuracy, you should use pango_matrix_transform_rectangle() on
   // original rectangle in Pango units and convert to pixels afterward
   // using pango_extents_to_pixels()'s first argument.
   // <rect>: in/out bounding box in device units, or %NULL
   void transform_pixel_rectangle(AT0)(AT0 /*Rectangle*/ rect) nothrow {
      pango_matrix_transform_pixel_rectangle(&this, UpCast!(Rectangle*)(rect));
   }

   // VERSION: 1.16
   // Transforms the point (@x, @y) by @matrix.
   // <x>: in/out X position
   // <y>: in/out Y position
   void transform_point(AT0, AT1)(AT0 /*double*/ x, AT1 /*double*/ y) nothrow {
      pango_matrix_transform_point(&this, UpCast!(double*)(x), UpCast!(double*)(y));
   }

   // VERSION: 1.16
   // First transforms @rect using @matrix, then calculates the bounding box
   // of the transformed rectangle.  The rectangle should be in Pango units.
   // 
   // This function is useful for example when you want to draw a rotated
   // @PangoLayout to an image buffer, and want to know how large the image
   // should be and how much you should shift the layout when rendering.
   // 
   // If you have a rectangle in device units (pixels), use
   // pango_matrix_transform_pixel_rectangle().
   // 
   // If you have the rectangle in Pango units and want to convert to
   // transformed pixel bounding box, it is more accurate to transform it first
   // (using this function) and pass the result to pango_extents_to_pixels(),
   // first argument, for an inclusive rounded rectangle.
   // However, there are valid reasons that you may want to convert
   // to pixels first and then transform, for example when the transformed
   // coordinates may overflow in Pango units (large matrix translation for
   // example).
   // <rect>: in/out bounding box in Pango units, or %NULL
   void transform_rectangle(AT0)(AT0 /*Rectangle*/ rect) nothrow {
      pango_matrix_transform_rectangle(&this, UpCast!(Rectangle*)(rect));
   }

   // VERSION: 1.6
   // Changes the transformation represented by @matrix to be the
   // transformation given by first translating by (@tx, @ty)
   // then applying the original transformation.
   // <tx>: amount to translate in the X direction
   // <ty>: amount to translate in the Y direction
   void translate()(double tx, double ty) nothrow {
      pango_matrix_translate(&this, tx, ty);
   }
}

enum RENDER_TYPE_NONE = "PangoRenderNone";
struct Rectangle {
   int x, y, width, height;
}


// #PangoRenderPart defines different items to render for such
// purposes as setting colors.
enum RenderPart /* Version 1.8 */ {
   FOREGROUND = 0,
   BACKGROUND = 1,
   UNDERLINE = 2,
   STRIKETHROUGH = 3
}

// #PangoRenderer is a base class for objects that are used to
// render Pango objects such as #PangoGlyphString and
// #PangoLayout.
struct Renderer /* : GObject.Object */ /* Version 1.8 */ {
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance object;
   GObject2.Object parent_instance;
   private Underline underline;
   private int strikethrough;
   private int active_count;
   Matrix* matrix;
   private RendererPrivate* priv;


   // VERSION: 1.8
   // Does initial setup before rendering operations on @renderer.
   // pango_renderer_deactivate() should be called when done drawing.
   // Calls such as pango_renderer_draw_layout() automatically
   // activate the layout before drawing on it. Calls to
   // pango_renderer_activate() and pango_renderer_deactivate() can
   // be nested and the renderer will only be initialized and
   // deinitialized once.
   void activate()() nothrow {
      pango_renderer_activate(&this);
   }

   // VERSION: 1.8
   // Cleans up after rendering operations on @renderer. See
   // docs for pango_renderer_activate().
   void deactivate()() nothrow {
      pango_renderer_deactivate(&this);
   }

   // VERSION: 1.8
   // Draw a squiggly line that approximately covers the given rectangle
   // in the style of an underline used to indicate a spelling error.
   // (The width of the underline is rounded to an integer number
   // of up/down segments and the resulting rectangle is centered
   // in the original rectangle)
   // 
   // This should be called while @renderer is already active.  Use
   // pango_renderer_activate() to activate a renderer.
   // <x>: X coordinate of underline, in Pango units in user coordinate system
   // <y>: Y coordinate of underline, in Pango units in user coordinate system
   // <width>: width of underline, in Pango units in user coordinate system
   // <height>: height of underline, in Pango units in user coordinate system
   void draw_error_underline()(int x, int y, int width, int height) nothrow {
      pango_renderer_draw_error_underline(&this, x, y, width, height);
   }

   // VERSION: 1.8
   // Draws a single glyph with coordinates in device space.
   // <font>: a #PangoFont
   // <glyph>: the glyph index of a single glyph
   // <x>: X coordinate of left edge of baseline of glyph
   // <y>: Y coordinate of left edge of baseline of glyph
   void draw_glyph(AT0)(AT0 /*Font*/ font, Glyph glyph, double x, double y) nothrow {
      pango_renderer_draw_glyph(&this, UpCast!(Font*)(font), glyph, x, y);
   }

   // VERSION: 1.22
   // Draws the glyphs in @glyph_item with the specified #PangoRenderer,
   // embedding the text associated with the glyphs in the output if the
   // output format supports it (PDF for example).
   // 
   // Note that @text is the start of the text for layout, which is then
   // indexed by <literal>@glyph_item->item->offset</literal>.
   // 
   // If @text is %NULL, this simply calls pango_renderer_draw_glyphs().
   // 
   // The default implementation of this method simply falls back to
   // pango_renderer_draw_glyphs().
   // <text>: the UTF-8 text that @glyph_item refers to, or %NULL
   // <glyph_item>: a #PangoGlyphItem
   // <x>: X position of left edge of baseline, in user space coordinates in Pango units.
   // <y>: Y position of left edge of baseline, in user space coordinates in Pango units.
   void draw_glyph_item(AT0, AT1)(AT0 /*char*/ text, AT1 /*GlyphItem*/ glyph_item, int x, int y) nothrow {
      pango_renderer_draw_glyph_item(&this, toCString!(char*)(text), UpCast!(GlyphItem*)(glyph_item), x, y);
   }

   // VERSION: 1.8
   // Draws the glyphs in @glyphs with the specified #PangoRenderer.
   // <font>: a #PangoFont
   // <glyphs>: a #PangoGlyphString
   // <x>: X position of left edge of baseline, in user space coordinates in Pango units.
   // <y>: Y position of left edge of baseline, in user space coordinates in Pango units.
   void draw_glyphs(AT0, AT1)(AT0 /*Font*/ font, AT1 /*GlyphString*/ glyphs, int x, int y) nothrow {
      pango_renderer_draw_glyphs(&this, UpCast!(Font*)(font), UpCast!(GlyphString*)(glyphs), x, y);
   }

   // VERSION: 1.8
   // Draws @layout with the specified #PangoRenderer.
   // <layout>: a #PangoLayout
   // <x>: X position of left edge of baseline, in user space coordinates in Pango units.
   // <y>: Y position of left edge of baseline, in user space coordinates in Pango units.
   void draw_layout(AT0)(AT0 /*Layout*/ layout, int x, int y) nothrow {
      pango_renderer_draw_layout(&this, UpCast!(Layout*)(layout), x, y);
   }

   // VERSION: 1.8
   // Draws @line with the specified #PangoRenderer.
   // <line>: a #PangoLayoutLine
   // <x>: X position of left edge of baseline, in user space coordinates in Pango units.
   // <y>: Y position of left edge of baseline, in user space coordinates in Pango units.
   void draw_layout_line(AT0)(AT0 /*LayoutLine*/ line, int x, int y) nothrow {
      pango_renderer_draw_layout_line(&this, UpCast!(LayoutLine*)(line), x, y);
   }

   // VERSION: 1.8
   // Draws an axis-aligned rectangle in user space coordinates with the
   // specified #PangoRenderer.
   // 
   // This should be called while @renderer is already active.  Use
   // pango_renderer_activate() to activate a renderer.
   // <part>: type of object this rectangle is part of
   // <x>: X position at which to draw rectangle, in user space coordinates in Pango units
   // <y>: Y position at which to draw rectangle, in user space coordinates in Pango units
   // <width>: width of rectangle in Pango units in user space coordinates
   // <height>: height of rectangle in Pango units in user space coordinates
   void draw_rectangle()(RenderPart part, int x, int y, int width, int height) nothrow {
      pango_renderer_draw_rectangle(&this, part, x, y, width, height);
   }

   // VERSION: 1.8
   // Draws a trapezoid with the parallel sides aligned with the X axis
   // using the given #PangoRenderer; coordinates are in device space.
   // <part>: type of object this trapezoid is part of
   // <y1_>: Y coordinate of top of trapezoid
   // <x11>: X coordinate of left end of top of trapezoid
   // <x21>: X coordinate of right end of top of trapezoid
   // <y2>: Y coordinate of bottom of trapezoid
   // <x12>: X coordinate of left end of bottom of trapezoid
   // <x22>: X coordinate of right end of bottom of trapezoid
   void draw_trapezoid()(RenderPart part, double y1_, double x11, double x21, double y2, double x12, double x22) nothrow {
      pango_renderer_draw_trapezoid(&this, part, y1_, x11, x21, y2, x12, x22);
   }

   // VERSION: 1.8
   // Gets the current rendering color for the specified part.
   // 
   // if it hasn't been set and should be inherited from the
   // environment.
   // RETURNS: the color for the specified part, or %NULL
   // <part>: the part to get the color for
   Color* /*new*/ get_color()(RenderPart part) nothrow {
      return pango_renderer_get_color(&this, part);
   }

   // Unintrospectable method: get_layout() / pango_renderer_get_layout()
   // VERSION: 1.20
   // Gets the layout currently being rendered using @renderer.
   // Calling this function only makes sense from inside a subclass's
   // methods, like in its draw_shape<!---->() for example.
   // 
   // The returned layout should not be modified while still being
   // rendered.
   // 
   // rendered using @renderer at this time.
   // RETURNS: the layout, or %NULL if no layout is being
   Layout* get_layout()() nothrow {
      return pango_renderer_get_layout(&this);
   }

   // VERSION: 1.20
   // Gets the layout line currently being rendered using @renderer.
   // Calling this function only makes sense from inside a subclass's
   // methods, like in its draw_shape<!---->() for example.
   // 
   // The returned layout line should not be modified while still being
   // rendered.
   // 
   // rendered using @renderer at this time.
   // RETURNS: the layout line, or %NULL if no layout line is being
   LayoutLine* /*new*/ get_layout_line()() nothrow {
      return pango_renderer_get_layout_line(&this);
   }

   // VERSION: 1.8
   // Gets the transformation matrix that will be applied when
   // rendering. See pango_renderer_set_matrix().
   // 
   // (which is the same as the identity matrix). The returned
   // matrix is owned by Pango and must not be modified or
   // freed.
   // RETURNS: the matrix, or %NULL if no matrix has been set
   Matrix* get_matrix()() nothrow {
      return pango_renderer_get_matrix(&this);
   }

   // VERSION: 1.8
   // Informs Pango that the way that the rendering is done
   // for @part has changed in a way that would prevent multiple
   // pieces being joined together into one drawing call. For
   // instance, if a subclass of #PangoRenderer was to add a stipple
   // option for drawing underlines, it needs to call
   // 
   // <informalexample><programlisting>
   // pango_renderer_part_changed (render, PANGO_RENDER_PART_UNDERLINE);
   // </programlisting></informalexample>
   // 
   // When the stipple changes or underlines with different stipples
   // might be joined together. Pango automatically calls this for
   // changes to colors. (See pango_renderer_set_color())
   // <part>: the part for which rendering has changed.
   void part_changed()(RenderPart part) nothrow {
      pango_renderer_part_changed(&this, part);
   }

   // VERSION: 1.8
   // Sets the color for part of the rendering.
   // <part>: the part to change the color of
   // <color>: the new color or %NULL to unset the current color
   void set_color(AT0)(RenderPart part, AT0 /*Color*/ color) nothrow {
      pango_renderer_set_color(&this, part, UpCast!(Color*)(color));
   }

   // VERSION: 1.8
   // Sets the transformation matrix that will be applied when rendering.
   // <matrix>: a #PangoMatrix, or %NULL to unset any existing matrix. (No matrix set is the same as setting the identity matrix.)
   void set_matrix(AT0)(AT0 /*Matrix*/ matrix) nothrow {
      pango_renderer_set_matrix(&this, UpCast!(Matrix*)(matrix));
   }
}

// Class structure for #PangoRenderer.
struct RendererClass /* Version 1.8 */ {
   private GObject2.ObjectClass parent_class;

   // <font>: a #PangoFont
   // <glyphs>: a #PangoGlyphString
   // <x>: X position of left edge of baseline, in user space coordinates in Pango units.
   // <y>: Y position of left edge of baseline, in user space coordinates in Pango units.
   extern (C) void function (Renderer* renderer, Font* font, GlyphString* glyphs, int x, int y) nothrow draw_glyphs;

   // <part>: type of object this rectangle is part of
   // <x>: X position at which to draw rectangle, in user space coordinates in Pango units
   // <y>: Y position at which to draw rectangle, in user space coordinates in Pango units
   // <width>: width of rectangle in Pango units in user space coordinates
   // <height>: height of rectangle in Pango units in user space coordinates
   extern (C) void function (Renderer* renderer, RenderPart part, int x, int y, int width, int height) nothrow draw_rectangle;

   // <x>: X coordinate of underline, in Pango units in user coordinate system
   // <y>: Y coordinate of underline, in Pango units in user coordinate system
   // <width>: width of underline, in Pango units in user coordinate system
   // <height>: height of underline, in Pango units in user coordinate system
   extern (C) void function (Renderer* renderer, int x, int y, int width, int height) nothrow draw_error_underline;
   extern (C) void function (Renderer* renderer, AttrShape* attr, int x, int y) nothrow draw_shape;

   // <part>: type of object this trapezoid is part of
   // <y1_>: Y coordinate of top of trapezoid
   // <x11>: X coordinate of left end of top of trapezoid
   // <x21>: X coordinate of right end of top of trapezoid
   // <y2>: Y coordinate of bottom of trapezoid
   // <x12>: X coordinate of left end of bottom of trapezoid
   // <x22>: X coordinate of right end of bottom of trapezoid
   extern (C) void function (Renderer* renderer, RenderPart part, double y1_, double x11, double x21, double y2, double x12, double x22) nothrow draw_trapezoid;

   // <font>: a #PangoFont
   // <glyph>: the glyph index of a single glyph
   // <x>: X coordinate of left edge of baseline of glyph
   // <y>: Y coordinate of left edge of baseline of glyph
   extern (C) void function (Renderer* renderer, Font* font, Glyph glyph, double x, double y) nothrow draw_glyph;
   // <part>: the part for which rendering has changed.
   extern (C) void function (Renderer* renderer, RenderPart part) nothrow part_changed;
   extern (C) void function (Renderer* renderer) nothrow begin;
   extern (C) void function (Renderer* renderer) nothrow end;
   extern (C) void function (Renderer* renderer, LayoutRun* run) nothrow prepare_run;

   // <text>: the UTF-8 text that @glyph_item refers to, or %NULL
   // <glyph_item>: a #PangoGlyphItem
   // <x>: X position of left edge of baseline, in user space coordinates in Pango units.
   // <y>: Y position of left edge of baseline, in user space coordinates in Pango units.
   extern (C) void function (Renderer* renderer, char* text, GlyphItem* glyph_item, int x, int y) nothrow draw_glyph_item;
   extern (C) void function () nothrow _pango_reserved2;
   extern (C) void function () nothrow _pango_reserved3;
   extern (C) void function () nothrow _pango_reserved4;
}

struct RendererPrivate {
}

enum int SCALE = 1024;
enum Script {
   INVALID_CODE = -1,
   COMMON = 0,
   INHERITED = 1,
   ARABIC = 2,
   ARMENIAN = 3,
   BENGALI = 4,
   BOPOMOFO = 5,
   CHEROKEE = 6,
   COPTIC = 7,
   CYRILLIC = 8,
   DESERET = 9,
   DEVANAGARI = 10,
   ETHIOPIC = 11,
   GEORGIAN = 12,
   GOTHIC = 13,
   GREEK = 14,
   GUJARATI = 15,
   GURMUKHI = 16,
   HAN = 17,
   HANGUL = 18,
   HEBREW = 19,
   HIRAGANA = 20,
   KANNADA = 21,
   KATAKANA = 22,
   KHMER = 23,
   LAO = 24,
   LATIN = 25,
   MALAYALAM = 26,
   MONGOLIAN = 27,
   MYANMAR = 28,
   OGHAM = 29,
   OLD_ITALIC = 30,
   ORIYA = 31,
   RUNIC = 32,
   SINHALA = 33,
   SYRIAC = 34,
   TAMIL = 35,
   TELUGU = 36,
   THAANA = 37,
   THAI = 38,
   TIBETAN = 39,
   CANADIAN_ABORIGINAL = 40,
   YI = 41,
   TAGALOG = 42,
   HANUNOO = 43,
   BUHID = 44,
   TAGBANWA = 45,
   BRAILLE = 46,
   CYPRIOT = 47,
   LIMBU = 48,
   OSMANYA = 49,
   SHAVIAN = 50,
   LINEAR_B = 51,
   TAI_LE = 52,
   UGARITIC = 53,
   NEW_TAI_LUE = 54,
   BUGINESE = 55,
   GLAGOLITIC = 56,
   TIFINAGH = 57,
   SYLOTI_NAGRI = 58,
   OLD_PERSIAN = 59,
   KHAROSHTHI = 60,
   UNKNOWN = 61,
   BALINESE = 62,
   CUNEIFORM = 63,
   PHOENICIAN = 64,
   PHAGS_PA = 65,
   NKO = 66,
   KAYAH_LI = 67,
   LEPCHA = 68,
   REJANG = 69,
   SUNDANESE = 70,
   SAURASHTRA = 71,
   CHAM = 72,
   OL_CHIKI = 73,
   VAI = 74,
   CARIAN = 75,
   LYCIAN = 76,
   LYDIAN = 77
}

// A #PangoScriptIter is used to iterate through a string
// and identify ranges in different scripts.
struct ScriptIter {

   // VERSION: 1.4
   // Frees a #PangoScriptIter created with pango_script_iter_new().
   void free()() nothrow {
      pango_script_iter_free(&this);
   }

   // VERSION: 1.4
   // Gets information about the range to which @iter currently points.
   // The range is the set of locations p where *start <= p < *end.
   // (That is, it doesn't include the character stored at *end)
   // <start>: location to store start position of the range, or %NULL
   // <end>: location to store end position of the range, or %NULL
   // <script>: location to store script for range, or %NULL
   void get_range(AT0, AT1, AT2)(AT0 /*char**/ start, AT1 /*char**/ end, AT2 /*Script*/ script) nothrow {
      pango_script_iter_get_range(&this, toCString!(char**)(start), toCString!(char**)(end), UpCast!(Script*)(script));
   }

   // VERSION: 1.4
   // Advances a #PangoScriptIter to the next range. If @iter
   // is already at the end, it is left unchanged and %FALSE
   // is returned.
   // RETURNS: %TRUE if @iter was successfully advanced.
   int next()() nothrow {
      return pango_script_iter_next(&this);
   }

   // Unintrospectable function: new() / pango_script_iter_new()
   // VERSION: 1.4
   // Create a new #PangoScriptIter, used to break a string of
   // Unicode into runs by text. No copy is made of @text, so
   // the caller needs to make sure it remains valid until
   // the iterator is freed with pango_script_iter_free().
   // 
   // to point at the first range in the text, which should be
   // freed with pango_script_iter_free(). If the string is
   // empty, it will point at an empty range.
   // RETURNS: the new script iterator, initialized
   // <text>: a UTF-8 string
   // <length>: length of @text, or -1 if @text is nul-terminated.
   static ScriptIter* new_(AT0)(AT0 /*char*/ text, int length) nothrow {
      return pango_script_iter_new(toCString!(char*)(text), length);
   }
}

enum Stretch {
   ULTRA_CONDENSED = 0,
   EXTRA_CONDENSED = 1,
   CONDENSED = 2,
   SEMI_CONDENSED = 3,
   NORMAL = 4,
   SEMI_EXPANDED = 5,
   EXPANDED = 6,
   EXTRA_EXPANDED = 7,
   ULTRA_EXPANDED = 8
}
// An enumeration specifying the various slant styles possible for a font.
enum Style {
   NORMAL = 0,
   OBLIQUE = 1,
   ITALIC = 2
}
enum TabAlign {
   LEFT = 0
}
struct TabArray {

   // Creates an array of @initial_size tab stops. Tab stops are specified in
   // pixel units if @positions_in_pixels is %TRUE, otherwise in Pango
   // units. All stops are initially at position 0.
   // 
   // be freed with pango_tab_array_free().
   // RETURNS: the newly allocated #PangoTabArray, which should
   // <initial_size>: Initial number of tab stops to allocate, can be 0
   // <positions_in_pixels>: whether positions are in pixel units
   static TabArray* /*new*/ new_()(int initial_size, int positions_in_pixels) nothrow {
      return pango_tab_array_new(initial_size, positions_in_pixels);
   }
   static auto opCall()(int initial_size, int positions_in_pixels) {
      return pango_tab_array_new(initial_size, positions_in_pixels);
   }

   // Unintrospectable constructor: new_with_positions() / pango_tab_array_new_with_positions()
   // This is a convenience function that creates a #PangoTabArray
   // and allows you to specify the alignment and position of each
   // tab stop. You <emphasis>must</emphasis> provide an alignment
   // and position for @size tab stops.
   // 
   // be freed with pango_tab_array_free().
   // RETURNS: the newly allocated #PangoTabArray, which should
   // <size>: number of tab stops in the array
   // <positions_in_pixels>: whether positions are in pixel units
   // <first_alignment>: alignment of first tab stop
   // <first_position>: position of first tab stop
   alias pango_tab_array_new_with_positions new_with_positions; // Variadic

   // Copies a #PangoTabArray
   // 
   // be freed with pango_tab_array_free().
   // RETURNS: the newly allocated #PangoTabArray, which should
   TabArray* /*new*/ copy()() nothrow {
      return pango_tab_array_copy(&this);
   }
   // Frees a tab array and associated resources.
   void free()() nothrow {
      pango_tab_array_free(&this);
   }

   // Returns %TRUE if the tab positions are in pixels, %FALSE if they are
   // in Pango units.
   // RETURNS: whether positions are in pixels.
   int get_positions_in_pixels()() nothrow {
      return pango_tab_array_get_positions_in_pixels(&this);
   }

   // Gets the number of tab stops in @tab_array.
   // RETURNS: the number of tab stops in the array.
   int get_size()() nothrow {
      return pango_tab_array_get_size(&this);
   }

   // Gets the alignment and position of a tab stop.
   // <tab_index>: tab stop index
   // <alignment>: location to store alignment, or %NULL
   // <location>: location to store tab position, or %NULL
   void get_tab(AT0)(int tab_index, AT0 /*TabAlign*/ alignment, int* location) nothrow {
      pango_tab_array_get_tab(&this, tab_index, UpCast!(TabAlign*)(alignment), location);
   }

   // If non-%NULL, @alignments and @locations are filled with allocated
   // arrays of length pango_tab_array_get_size(). You must free the
   // returned array.
   // <alignments>: location to store an array of tab stop alignments, or %NULL
   // <locations>: location to store an array of tab positions, or %NULL
   void get_tabs(AT0)(AT0 /*TabAlign**/ alignments, int** locations) nothrow {
      pango_tab_array_get_tabs(&this, UpCast!(TabAlign**)(alignments), locations);
   }

   // Resizes a tab array. You must subsequently initialize any tabs that
   // were added as a result of growing the array.
   // <new_size>: new size of the array
   void resize()(int new_size) nothrow {
      pango_tab_array_resize(&this, new_size);
   }

   // Sets the alignment and location of a tab stop.
   // @alignment must always be #PANGO_TAB_LEFT in the current
   // implementation.
   // <tab_index>: the index of a tab stop
   // <alignment>: tab alignment
   // <location>: tab location in Pango units
   void set_tab()(int tab_index, TabAlign alignment, int location) nothrow {
      pango_tab_array_set_tab(&this, tab_index, alignment, location);
   }
}

enum int UNKNOWN_GLYPH_HEIGHT = 14;
enum int UNKNOWN_GLYPH_WIDTH = 10;
enum Underline {
   NONE = 0,
   SINGLE = 1,
   DOUBLE = 2,
   LOW = 3,
   ERROR = 4
}
enum Variant {
   NORMAL = 0,
   SMALL_CAPS = 1
}
enum Weight {
   THIN = 100,
   ULTRALIGHT = 200,
   LIGHT = 300,
   BOOK = 380,
   NORMAL = 400,
   MEDIUM = 500,
   SEMIBOLD = 600,
   BOLD = 700,
   ULTRABOLD = 800,
   HEAVY = 900,
   ULTRAHEAVY = 1000
}
enum WrapMode {
   WORD = 0,
   CHAR = 1,
   WORD_CHAR = 2
}
struct _ScriptForLang {
   char[7] lang;
   Script[3] scripts;
}


// Unintrospectable function: attr_background_new() / pango_attr_background_new()
// Create a new background color attribute.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <red>: the red value (ranging from 0 to 65535)
// <green>: the green value
// <blue>: the blue value
static Attribute* attr_background_new()(ushort red, ushort green, ushort blue) nothrow {
   return pango_attr_background_new(red, green, blue);
}


// Unintrospectable function: attr_fallback_new() / pango_attr_fallback_new()
// VERSION: 1.4
// Create a new font fallback attribute.
// 
// If fallback is disabled, characters will only be used from the
// closest matching font on the system. No fallback will be done to
// other fonts on the system that might contain the characters in the
// text.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <enable_fallback>: %TRUE if we should fall back on other fonts for characters the active font is missing.
static Attribute* attr_fallback_new()(int enable_fallback) nothrow {
   return pango_attr_fallback_new(enable_fallback);
}


// Unintrospectable function: attr_family_new() / pango_attr_family_new()
// Create a new font family attribute.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <family>: the family or comma separated list of families
static Attribute* attr_family_new(AT0)(AT0 /*char*/ family) nothrow {
   return pango_attr_family_new(toCString!(char*)(family));
}


// Unintrospectable function: attr_foreground_new() / pango_attr_foreground_new()
// Create a new foreground color attribute.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <red>: the red value (ranging from 0 to 65535)
// <green>: the green value
// <blue>: the blue value
static Attribute* attr_foreground_new()(ushort red, ushort green, ushort blue) nothrow {
   return pango_attr_foreground_new(red, green, blue);
}


// Unintrospectable function: attr_gravity_hint_new() / pango_attr_gravity_hint_new()
// VERSION: 1.16
// Create a new gravity hint attribute.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <hint>: the gravity hint value.
static Attribute* attr_gravity_hint_new()(GravityHint hint) nothrow {
   return pango_attr_gravity_hint_new(hint);
}


// Unintrospectable function: attr_gravity_new() / pango_attr_gravity_new()
// VERSION: 1.16
// Create a new gravity attribute.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <gravity>: the gravity value; should not be %PANGO_GRAVITY_AUTO.
static Attribute* attr_gravity_new()(Gravity gravity) nothrow {
   return pango_attr_gravity_new(gravity);
}


// Unintrospectable function: attr_letter_spacing_new() / pango_attr_letter_spacing_new()
// VERSION: 1.6
// Create a new letter-spacing attribute.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <letter_spacing>: amount of extra space to add between graphemes of the text, in Pango units.
static Attribute* attr_letter_spacing_new()(int letter_spacing) nothrow {
   return pango_attr_letter_spacing_new(letter_spacing);
}


// Unintrospectable function: attr_rise_new() / pango_attr_rise_new()
// Create a new baseline displacement attribute.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <rise>: the amount that the text should be displaced vertically, in Pango units. Positive values displace the text upwards.
static Attribute* attr_rise_new()(int rise) nothrow {
   return pango_attr_rise_new(rise);
}


// Unintrospectable function: attr_scale_new() / pango_attr_scale_new()
// Create a new font size scale attribute. The base font for the
// affected text will have its size multiplied by @scale_factor.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <scale_factor>: factor to scale the font
static Attribute* attr_scale_new()(double scale_factor) nothrow {
   return pango_attr_scale_new(scale_factor);
}


// Unintrospectable function: attr_stretch_new() / pango_attr_stretch_new()
// Create a new font stretch attribute
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <stretch>: the stretch
static Attribute* attr_stretch_new()(Stretch stretch) nothrow {
   return pango_attr_stretch_new(stretch);
}


// Unintrospectable function: attr_strikethrough_color_new() / pango_attr_strikethrough_color_new()
// VERSION: 1.8
// Create a new strikethrough color attribute. This attribute
// modifies the color of strikethrough lines. If not set, strikethrough
// lines will use the foreground color.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <red>: the red value (ranging from 0 to 65535)
// <green>: the green value
// <blue>: the blue value
static Attribute* attr_strikethrough_color_new()(ushort red, ushort green, ushort blue) nothrow {
   return pango_attr_strikethrough_color_new(red, green, blue);
}


// Unintrospectable function: attr_strikethrough_new() / pango_attr_strikethrough_new()
// Create a new strike-through attribute.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <strikethrough>: %TRUE if the text should be struck-through.
static Attribute* attr_strikethrough_new()(int strikethrough) nothrow {
   return pango_attr_strikethrough_new(strikethrough);
}


// Unintrospectable function: attr_style_new() / pango_attr_style_new()
// Create a new font slant style attribute.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <style>: the slant style
static Attribute* attr_style_new()(Style style) nothrow {
   return pango_attr_style_new(style);
}


// VERSION: 1.22
// MOVED TO: AttrType.get_name
// Fetches the attribute type name passed in when registering the type using
// pango_attr_type_register().
// 
// The returned value is an interned string (see g_intern_string() for what
// that means) that should not be modified or freed.
// 
// a built-in Pango attribute type or invalid.
// RETURNS: the type ID name (which may be %NULL), or %NULL if @type is
// <type>: an attribute type ID to fetch the name for
static char* attr_type_get_name()(AttrType type) nothrow {
   return pango_attr_type_get_name(type);
}


// MOVED TO: AttrType.register
// Allocate a new attribute type ID.  The attribute type name can be accessed
// later by using pango_attr_type_get_name().
// RETURNS: the new type ID.
// <name>: an identifier for the type
static AttrType attr_type_register(AT0)(AT0 /*char*/ name) nothrow {
   return pango_attr_type_register(toCString!(char*)(name));
}


// Unintrospectable function: attr_underline_color_new() / pango_attr_underline_color_new()
// VERSION: 1.8
// Create a new underline color attribute. This attribute
// modifies the color of underlines. If not set, underlines
// will use the foreground color.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <red>: the red value (ranging from 0 to 65535)
// <green>: the green value
// <blue>: the blue value
static Attribute* attr_underline_color_new()(ushort red, ushort green, ushort blue) nothrow {
   return pango_attr_underline_color_new(red, green, blue);
}


// Unintrospectable function: attr_underline_new() / pango_attr_underline_new()
// Create a new underline-style attribute.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <underline>: the underline style.
static Attribute* attr_underline_new()(Underline underline) nothrow {
   return pango_attr_underline_new(underline);
}


// Unintrospectable function: attr_variant_new() / pango_attr_variant_new()
// Create a new font variant attribute (normal or small caps)
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <variant>: the variant
static Attribute* attr_variant_new()(Variant variant) nothrow {
   return pango_attr_variant_new(variant);
}


// Unintrospectable function: attr_weight_new() / pango_attr_weight_new()
// Create a new font weight attribute.
// 
// freed with pango_attribute_destroy().
// RETURNS: the newly allocated #PangoAttribute, which should be
// <weight>: the weight
static Attribute* attr_weight_new()(Weight weight) nothrow {
   return pango_attr_weight_new(weight);
}


// VERSION: 1.22
// MOVED TO: BidiType.for_unichar
// Determines the normative bidirectional character type of a
// character, as specified in the Unicode Character Database.
// 
// A simplified version of this function is available as
// pango_unichar_get_direction().
// 
// Unicode bidirectional algorithm.
// RETURNS: the bidirectional character type, as used in the
// <ch>: a Unicode character
static BidiType bidi_type_for_unichar()(dchar ch) nothrow {
   return pango_bidi_type_for_unichar(ch);
}


// Determines possible line, word, and character breaks
// for a string of Unicode text with a single analysis.  For most
// purposes you may want to use pango_get_log_attrs().
// <text>: the text to process
// <length>: length of @text in bytes (may be -1 if @text is nul-terminated)
// <analysis>: #PangoAnalysis structure from pango_itemize()
// <attrs>: an array to store character information in
// <attrs_len>: size of the array passed as @attrs
static void break_(AT0, AT1, AT2)(AT0 /*char*/ text, int length, AT1 /*Analysis*/ analysis, AT2 /*LogAttr*/ attrs, int attrs_len) nothrow {
   pango_break(toCString!(char*)(text), length, UpCast!(Analysis*)(analysis), UpCast!(LogAttr*)(attrs), attrs_len);
}


// VERSION: 1.16
// Converts extents from Pango units to device units, dividing by the
// %PANGO_SCALE factor and performing rounding.
// 
// The @inclusive rectangle is converted by flooring the x/y coordinates and extending
// width/height, such that the final rectangle completely includes the original
// rectangle.
// 
// The @nearest rectangle is converted by rounding the coordinates
// of the rectangle to the nearest device unit (pixel).
// 
// The rule to which argument to use is: if you want the resulting device-space
// rectangle to completely contain the original rectangle, pass it in as @inclusive.
// If you want two touching-but-not-overlapping rectangles stay
// touching-but-not-overlapping after rounding to device units, pass them in
// as @nearest.
// <inclusive>: rectangle to round to pixels inclusively, or %NULL.
// <nearest>: rectangle to round to nearest pixels, or %NULL.
static void extents_to_pixels(AT0, AT1)(AT0 /*Rectangle*/ inclusive, AT1 /*Rectangle*/ nearest) nothrow {
   pango_extents_to_pixels(UpCast!(Rectangle*)(inclusive), UpCast!(Rectangle*)(nearest));
}


// VERSION: 1.4
// Searches a string the first character that has a strong
// direction, according to the Unicode bidirectional algorithm.
// 
// If no such character is found, then %PANGO_DIRECTION_NEUTRAL is returned.
// RETURNS: The direction corresponding to the first strong character.
// <text>: the text to process
// <length>: length of @text in bytes (may be -1 if @text is nul-terminated)
static Direction find_base_dir(AT0)(AT0 /*char*/ text, int length) nothrow {
   return pango_find_base_dir(toCString!(char*)(text), length);
}


// Locates a paragraph boundary in @text. A boundary is caused by
// delimiter characters, such as a newline, carriage return, carriage
// return-newline pair, or Unicode paragraph separator character.  The
// index of the run of delimiters is returned in
// @paragraph_delimiter_index. The index of the start of the paragraph
// (index after all delimiters) is stored in @next_paragraph_start.
// 
// If no delimiters are found, both @paragraph_delimiter_index and
// @next_paragraph_start are filled with the length of @text (an index one
// off the end).
// <text>: UTF-8 text
// <length>: length of @text in bytes, or -1 if nul-terminated
// <paragraph_delimiter_index>: return location for index of delimiter
// <next_paragraph_start>: return location for start of next paragraph
static void find_paragraph_boundary(AT0)(AT0 /*char*/ text, int length, int* paragraph_delimiter_index, int* next_paragraph_start) nothrow {
   pango_find_paragraph_boundary(toCString!(char*)(text), length, paragraph_delimiter_index, next_paragraph_start);
}


// MOVED TO: FontDescription.from_string
// Creates a new font description from a string representation in the
// form "[FAMILY-LIST] [STYLE-OPTIONS] [SIZE]", where FAMILY-LIST is a
// comma separated list of families optionally terminated by a comma,
// STYLE_OPTIONS is a whitespace separated list of words where each WORD
// describes one of style, variant, weight, stretch, or gravity, and SIZE
// is a decimal number (size in points) or optionally followed by the
// unit modifier "px" for absolute size. Any one of the options may
// be absent.  If FAMILY-LIST is absent, then the family_name field of
// the resulting font description will be initialized to %NULL.  If
// STYLE-OPTIONS is missing, then all style options will be set to the
// default values. If SIZE is missing, the size in the resulting font
// description will be set to 0.
// RETURNS: a new #PangoFontDescription.
// <str>: string representation of a font description.
static FontDescription* /*new*/ font_description_from_string(AT0)(AT0 /*char*/ str) nothrow {
   return pango_font_description_from_string(toCString!(char*)(str));
}


// Computes a #PangoLogAttr for each character in @text. The @log_attrs
// array must have one #PangoLogAttr for each position in @text; if
// @text contains N characters, it has N+1 positions, including the
// last position at the end of the text. @text should be an entire
// paragraph; logical attributes can't be computed without context
// (for example you need to see spaces on either side of a word to know
// the word is a word).
// <text>: text to process
// <length>: length in bytes of @text
// <level>: embedding level, or -1 if unknown
// <language>: language tag
// <log_attrs>: array with one #PangoLogAttr per character in @text, plus one extra, to be filled in
// <attrs_len>: length of @log_attrs array
static void get_log_attrs(AT0, AT1, AT2)(AT0 /*char*/ text, int length, int level, AT1 /*Language*/ language, AT2 /*LogAttr*/ log_attrs, int attrs_len) nothrow {
   pango_get_log_attrs(toCString!(char*)(text), length, level, UpCast!(Language*)(language), UpCast!(LogAttr*)(log_attrs), attrs_len);
}


// If @ch has the Unicode mirrored property and there is another Unicode
// character that typically has a glyph that is the mirror image of @ch's
// glyph, puts that character in the address pointed to by @mirrored_ch.
// 
// Use g_unichar_get_mirror_char() instead; the docs for that function
// provide full details.
// 
// filled in, %FALSE otherwise
// RETURNS: %TRUE if @ch has a mirrored character and @mirrored_ch is
// <ch>: a Unicode character
// <mirrored_ch>: location to store the mirrored character
static int get_mirror_char(AT0)(dchar ch, AT0 /*dchar*/ mirrored_ch) nothrow {
   return pango_get_mirror_char(ch, UpCast!(dchar*)(mirrored_ch));
}


// VERSION: 1.16
// MOVED TO: Gravity.get_for_matrix
// Finds the gravity that best matches the rotation component
// in a #PangoMatrix.
// 
// %PANGO_GRAVITY_AUTO, or %PANGO_GRAVITY_SOUTH if @matrix is %NULL
// RETURNS: the gravity of @matrix, which will never be
// <matrix>: a #PangoMatrix
static Gravity gravity_get_for_matrix(AT0)(AT0 /*Matrix*/ matrix) nothrow {
   return pango_gravity_get_for_matrix(UpCast!(Matrix*)(matrix));
}


// VERSION: 1.16
// MOVED TO: Gravity.get_for_script
// Based on the script, base gravity, and hint, returns actual gravity
// to use in laying out a single #PangoItem.
// 
// If @base_gravity is %PANGO_GRAVITY_AUTO, it is first replaced with the
// preferred gravity of @script.  To get the preferred gravity of a script,
// pass %PANGO_GRAVITY_AUTO and %PANGO_GRAVITY_HINT_STRONG in.
// 
// with @script.
// RETURNS: resolved gravity suitable to use for a run of text
// <script>: #PangoScript to query
// <base_gravity>: base gravity of the paragraph
// <hint>: orientation hint
static Gravity gravity_get_for_script()(Script script, Gravity base_gravity, GravityHint hint) nothrow {
   return pango_gravity_get_for_script(script, base_gravity, hint);
}


// VERSION: 1.26
// MOVED TO: Gravity.get_for_script_and_width
// Based on the script, East Asian width, base gravity, and hint,
// returns actual gravity to use in laying out a single character
// or #PangoItem.
// 
// This function is similar to pango_gravity_get_for_script() except
// that this function makes a distinction between narrow/half-width and
// wide/full-width characters also.  Wide/full-width characters always
// stand <emph>upright</emph>, that is, they always take the base gravity,
// whereas narrow/full-width characters are always rotated in vertical
// context.
// 
// If @base_gravity is %PANGO_GRAVITY_AUTO, it is first replaced with the
// preferred gravity of @script.
// 
// with @script and @wide.
// RETURNS: resolved gravity suitable to use for a run of text
// <script>: #PangoScript to query
// <wide>: %TRUE for wide characters as returned by g_unichar_iswide()
// <base_gravity>: base gravity of the paragraph
// <hint>: orientation hint
static Gravity gravity_get_for_script_and_width()(Script script, int wide, Gravity base_gravity, GravityHint hint) nothrow {
   return pango_gravity_get_for_script_and_width(script, wide, base_gravity, hint);
}


// VERSION: 1.16
// MOVED TO: Gravity.to_rotation
// Converts a #PangoGravity value to its natural rotation in radians.
// @gravity should not be %PANGO_GRAVITY_AUTO.
// 
// Note that pango_matrix_rotate() takes angle in degrees, not radians.
// So, to call pango_matrix_rotate() with the output of this function
// you should multiply it by (180. / G_PI).
// RETURNS: the rotation value corresponding to @gravity.
// <gravity>: gravity to query
static double gravity_to_rotation()(Gravity gravity) nothrow {
   return pango_gravity_to_rotation(gravity);
}


// VERSION: 1.10
// Checks @ch to see if it is a character that should not be
// normally rendered on the screen.  This includes all Unicode characters
// with "ZERO WIDTH" in their name, as well as <firstterm>bidi</firstterm> formatting characters, and
// a few other ones.  This is totally different from g_unichar_iszerowidth()
// and is at best misnamed.
// RETURNS: %TRUE if @ch is a zero-width character, %FALSE otherwise
// <ch>: a Unicode character
static int is_zero_width()(dchar ch) nothrow {
   return pango_is_zero_width(ch);
}


// Unintrospectable function: itemize() / pango_itemize()
// Breaks a piece of text into segments with consistent
// directional level and shaping engine. Each byte of @text will
// be contained in exactly one of the items in the returned list;
// the generated list of items will be in logical order (the start
// offsets of the items are ascending).
// 
// @cached_iter should be an iterator over @attrs currently positioned at a
// range before or containing @start_index; @cached_iter will be advanced to
// the range covering the position just after @start_index + @length.
// (i.e. if itemizing in a loop, just keep passing in the same @cached_iter).
// RETURNS: a #GList of #PangoItem structures.
// <context>: a structure holding information that affects
// <text>: the text to itemize.
// <start_index>: first byte in @text to process
// <length>: the number of bytes (not characters) to process after @start_index. This must be >= 0.
// <attrs>: the set of attributes that apply to @text.
// <cached_iter>: Cached attribute iterator, or %NULL
static GLib2.List* itemize(AT0, AT1, AT2, AT3)(AT0 /*Context*/ context, AT1 /*char*/ text, int start_index, int length, AT2 /*AttrList*/ attrs, AT3 /*AttrIterator*/ cached_iter) nothrow {
   return pango_itemize(UpCast!(Context*)(context), toCString!(char*)(text), start_index, length, UpCast!(AttrList*)(attrs), UpCast!(AttrIterator*)(cached_iter));
}


// Unintrospectable function: itemize_with_base_dir() / pango_itemize_with_base_dir()
// VERSION: 1.4
// Like pango_itemize(), but the base direction to use when
// computing bidirectional levels (see pango_context_set_base_dir ()),
// is specified explicitly rather than gotten from the #PangoContext.
// 
// freed using pango_item_free() probably in combination with g_list_foreach(),
// and the list itself using g_list_free().
// RETURNS: a #GList of #PangoItem structures.  The items should be
// <context>: a structure holding information that affects
// <base_dir>: base direction to use for bidirectional processing
// <text>: the text to itemize.
// <start_index>: first byte in @text to process
// <length>: the number of bytes (not characters) to process after @start_index. This must be >= 0.
// <attrs>: the set of attributes that apply to @text.
// <cached_iter>: Cached attribute iterator, or %NULL
static GLib2.List* itemize_with_base_dir(AT0, AT1, AT2, AT3)(AT0 /*Context*/ context, Direction base_dir, AT1 /*char*/ text, int start_index, int length, AT2 /*AttrList*/ attrs, AT3 /*AttrIterator*/ cached_iter) nothrow {
   return pango_itemize_with_base_dir(UpCast!(Context*)(context), base_dir, toCString!(char*)(text), start_index, length, UpCast!(AttrList*)(attrs), UpCast!(AttrIterator*)(cached_iter));
}


// MOVED TO: Language.from_string
// Take a RFC-3066 format language tag as a string and convert it to a
// #PangoLanguage pointer that can be efficiently copied (copy the
// pointer) and compared with other language tags (compare the
// pointer.)
// 
// This function first canonicalizes the string by converting it to
// lowercase, mapping '_' to '-', and stripping all characters other
// than letters and '-'.
// 
// Use pango_language_get_default() if you want to get the #PangoLanguage for
// the current locale of the process.
// 
// if @language was %NULL.  The returned pointer will be valid
// forever after, and should not be freed.
// RETURNS: an opaque pointer to a #PangoLanguage structure, or %NULL
// <language>: a string representing a language tag, or %NULL
static Language* /*new*/ language_from_string(AT0)(AT0 /*char*/ language) nothrow {
   return pango_language_from_string(toCString!(char*)(language));
}


// VERSION: 1.16
// MOVED TO: Language.get_default
// Returns the #PangoLanguage for the current locale of the process.
// Note that this can change over the life of an application.
// 
// On Unix systems, this is the return value is derived from
// <literal>setlocale(LC_CTYPE, NULL)</literal>, and the user can
// affect this through the environment variables LC_ALL, LC_CTYPE or
// LANG (checked in that order). The locale string typically is in
// the form lang_COUNTRY, where lang is an ISO-639 language code, and
// COUNTRY is an ISO-3166 country code. For instance, sv_FI for
// Swedish as written in Finland or pt_BR for Portuguese as written in
// Brazil.
// 
// On Windows, the C library does not use any such environment
// variables, and setting them won't affect the behavior of functions
// like ctime(). The user sets the locale through the Regional Options
// in the Control Panel. The C library (in the setlocale() function)
// does not use country and language codes, but country and language
// names spelled out in English.
// However, this function does check the above environment
// variables, and does return a Unix-style locale string based on
// either said environment variables or the thread's current locale.
// 
// Your application should call <literal>setlocale(LC_ALL, "");</literal>
// for the user settings to take effect.  Gtk+ does this in its initialization
// functions automatically (by calling gtk_set_locale()).
// See <literal>man setlocale</literal> for more details.
// 
// freed.
// RETURNS: the default language as a #PangoLanguage, must not be
static Language* /*new*/ language_get_default()() nothrow {
   return pango_language_get_default();
}


// VERSION: 1.4
// This will return the bidirectional embedding levels of the input paragraph
// as defined by the Unicode Bidirectional Algorithm available at:
// 
// http://www.unicode.org/reports/tr9/
// 
// If the input base direction is a weak direction, the direction of the
// characters in the text will determine the final resolved direction.
// 
// character (not byte), that should be freed using g_free.
// RETURNS: a newly allocated array of embedding levels, one item per
// <text>: the text to itemize.
// <length>: the number of bytes (not characters) to process, or -1 if @text is nul-terminated and the length should be calculated.
// <pbase_dir>: input base direction, and output resolved direction.
static ubyte* log2vis_get_embedding_levels(AT0, AT1)(AT0 /*char*/ text, int length, AT1 /*Direction*/ pbase_dir) nothrow {
   return pango_log2vis_get_embedding_levels(toCString!(char*)(text), length, UpCast!(Direction*)(pbase_dir));
}


// VERSION: 1.16
// Parses an enum type and stores the result in @value.
// 
// If @str does not match the nick name of any of the possible values for the
// enum and is not an integer, %FALSE is returned, a warning is issued
// if @warn is %TRUE, and a
// string representing the list of possible values is stored in
// @possible_values.  The list is slash-separated, eg.
// "none/start/middle/end".  If failed and @possible_values is not %NULL,
// returned string should be freed using g_free().
// RETURNS: %TRUE if @str was successfully parsed.
// <type>: enum type to parse, eg. %PANGO_TYPE_ELLIPSIZE_MODE.
// <str>: string to parse.  May be %NULL.
// <value>: integer to store the result in, or %NULL.
// <warn>: if %TRUE, issue a g_warning() on bad input.
// <possible_values>: place to store list of possible values on failure, or %NULL.
static int parse_enum(AT0, AT1)(Type type, AT0 /*char*/ str, int* value, int warn, AT1 /*char**/ possible_values) nothrow {
   return pango_parse_enum(type, toCString!(char*)(str), value, warn, toCString!(char**)(possible_values));
}


// Parses marked-up text (see
// <link linkend="PangoMarkupFormat">markup format</link>) to create
// a plain-text string and an attribute list.
// 
// If @accel_marker is nonzero, the given character will mark the
// character following it as an accelerator. For example, @accel_marker
// might be an ampersand or underscore. All characters marked
// as an accelerator will receive a %PANGO_UNDERLINE_LOW attribute,
// and the first character so marked will be returned in @accel_char.
// Two @accel_marker characters following each other produce a single
// literal @accel_marker character.
// 
// If any error happens, none of the output arguments are touched except
// for @error.
// RETURNS: %FALSE if @error is set, otherwise %TRUE
// <markup_text>: markup to parse (see <link linkend="PangoMarkupFormat">markup format</link>)
// <length>: length of @markup_text, or -1 if nul-terminated
// <accel_marker>: character that precedes an accelerator, or 0 for none
// <attr_list>: address of return location for a #PangoAttrList, or %NULL
// <text>: address of return location for text with tags stripped, or %NULL
// <accel_char>: address of return location for accelerator char, or %NULL
static int parse_markup(AT0, AT1, AT2, AT3, AT4)(AT0 /*char*/ markup_text, int length, dchar accel_marker, /*out*/ AT1 /*AttrList**/ attr_list, /*out*/ AT2 /*char**/ text, /*out*/ AT3 /*dchar*/ accel_char, AT4 /*GLib2.Error**/ error=null) nothrow {
   return pango_parse_markup(toCString!(char*)(markup_text), length, accel_marker, UpCast!(AttrList**)(attr_list), toCString!(char**)(text), UpCast!(dchar*)(accel_char), UpCast!(GLib2.Error**)(error));
}


// Parses a font stretch. The allowed values are
// "ultra_condensed", "extra_condensed", "condensed",
// "semi_condensed", "normal", "semi_expanded", "expanded",
// "extra_expanded" and "ultra_expanded". Case variations are
// ignored and the '_' characters may be omitted.
// RETURNS: %TRUE if @str was successfully parsed.
// <str>: a string to parse.
// <stretch>: a #PangoStretch to store the result in.
// <warn>: if %TRUE, issue a g_warning() on bad input.
static int parse_stretch(AT0, AT1)(AT0 /*char*/ str, AT1 /*Stretch*/ stretch, int warn) nothrow {
   return pango_parse_stretch(toCString!(char*)(str), UpCast!(Stretch*)(stretch), warn);
}


// Parses a font style. The allowed values are "normal",
// "italic" and "oblique", case variations being
// ignored.
// RETURNS: %TRUE if @str was successfully parsed.
// <str>: a string to parse.
// <style>: a #PangoStyle to store the result in.
// <warn>: if %TRUE, issue a g_warning() on bad input.
static int parse_style(AT0, AT1)(AT0 /*char*/ str, AT1 /*Style*/ style, int warn) nothrow {
   return pango_parse_style(toCString!(char*)(str), UpCast!(Style*)(style), warn);
}


// Parses a font variant. The allowed values are "normal"
// and "smallcaps" or "small_caps", case variations being
// ignored.
// RETURNS: %TRUE if @str was successfully parsed.
// <str>: a string to parse.
// <variant>: a #PangoVariant to store the result in.
// <warn>: if %TRUE, issue a g_warning() on bad input.
static int parse_variant(AT0, AT1)(AT0 /*char*/ str, AT1 /*Variant*/ variant, int warn) nothrow {
   return pango_parse_variant(toCString!(char*)(str), UpCast!(Variant*)(variant), warn);
}


// Parses a font weight. The allowed values are "heavy",
// "ultrabold", "bold", "normal", "light", "ultraleight"
// and integers. Case variations are ignored.
// RETURNS: %TRUE if @str was successfully parsed.
// <str>: a string to parse.
// <weight>: a #PangoWeight to store the result in.
// <warn>: if %TRUE, issue a g_warning() on bad input.
static int parse_weight(AT0, AT1)(AT0 /*char*/ str, AT1 /*Weight*/ weight, int warn) nothrow {
   return pango_parse_weight(toCString!(char*)(str), UpCast!(Weight*)(weight), warn);
}


// VERSION: 1.12
// Quantizes the thickness and position of a line, typically an
// underline or strikethrough, to whole device pixels, that is integer
// multiples of %PANGO_SCALE. The purpose of this function is to avoid
// such lines looking blurry.
// 
// Care is taken to make sure @thickness is at least one pixel when this
// function returns, but returned @position may become zero as a result
// of rounding.
// <thickness>: pointer to the thickness of a line, in Pango units
// <position>: corresponding position
static void quantize_line_geometry()(int* thickness, int* position) nothrow {
   pango_quantize_line_geometry(thickness, position);
}


// Reads an entire line from a file into a buffer. Lines may
// be delimited with '\n', '\r', '\n\r', or '\r\n'. The delimiter
// is not written into the buffer. Text after a '#' character is treated as
// a comment and skipped. '\' can be used to escape a # character.
// '\' proceeding a line delimiter combines adjacent lines. A '\' proceeding
// any other character is ignored and written into the output buffer
// unmodified.
// 
// the number of lines read (this is useful for maintaining
// a line number counter which doesn't combine lines with '\')
// RETURNS: 0 if the stream was already at an %EOF character, otherwise
// <stream>: a stdio stream
// <str>: #GString buffer into which to write the result
static int read_line(AT0, AT1)(AT0 /*FILE*/ stream, AT1 /*GLib2.String*/ str) nothrow {
   return pango_read_line(UpCast!(FILE*)(stream), UpCast!(GLib2.String*)(str));
}


// Unintrospectable function: reorder_items() / pango_reorder_items()
// From a list of items in logical order and the associated
// directional levels, produce a list in visual order.
// The original list is unmodified.
// 
// 
// (Please open a bug if you use this function.
// It is not a particularly convenient interface, and the code
// is duplicated elsewhere in Pango for that reason.)
// RETURNS: a #GList of #PangoItem structures in visual order.
// <logical_items>: a #GList of #PangoItem in logical order.
static GLib2.List* reorder_items(AT0)(AT0 /*GLib2.List*/ logical_items) nothrow {
   return pango_reorder_items(UpCast!(GLib2.List*)(logical_items));
}


// Scans an integer.
// Leading white space is skipped.
// RETURNS: %FALSE if a parse error occurred.
// <pos>: in/out string position
// <out>: an int into which to write the result
static int scan_int(AT0)(AT0 /*char**/ pos, int* out_) nothrow {
   return pango_scan_int(toCString!(char**)(pos), out_);
}


// Scans a string into a #GString buffer. The string may either
// be a sequence of non-white-space characters, or a quoted
// string with '"'. Instead a quoted string, '\"' represents
// a literal quote. Leading white space outside of quotes is skipped.
// RETURNS: %FALSE if a parse error occurred.
// <pos>: in/out string position
// <out>: a #GString into which to write the result
static int scan_string(AT0, AT1)(AT0 /*char**/ pos, AT1 /*GLib2.String*/ out_) nothrow {
   return pango_scan_string(toCString!(char**)(pos), UpCast!(GLib2.String*)(out_));
}


// Scans a word into a #GString buffer. A word consists
// of [A-Za-z_] followed by zero or more [A-Za-z_0-9]
// Leading white space is skipped.
// RETURNS: %FALSE if a parse error occurred.
// <pos>: in/out string position
// <out>: a #GString into which to write the result
static int scan_word(AT0, AT1)(AT0 /*char**/ pos, AT1 /*GLib2.String*/ out_) nothrow {
   return pango_scan_word(toCString!(char**)(pos), UpCast!(GLib2.String*)(out_));
}


// VERSION: 1.4
// MOVED TO: Script.for_unichar
// Looks up the #PangoScript for a particular character (as defined by
// Unicode Standard Annex #24). No check is made for @ch being a
// valid Unicode character; if you pass in invalid character, the
// result is undefined.
// 
// As of Pango 1.18, this function simply returns the return value of
// g_unichar_get_script().
// RETURNS: the #PangoScript for the character.
// <ch>: a Unicode character
static Script script_for_unichar()(dchar ch) nothrow {
   return pango_script_for_unichar(ch);
}


// VERSION: 1.4
// MOVED TO: Script.get_sample_language
// Given a script, finds a language tag that is reasonably
// representative of that script. This will usually be the
// most widely spoken or used language written in that script:
// for instance, the sample language for %PANGO_SCRIPT_CYRILLIC
// is <literal>ru</literal> (Russian), the sample language
// for %PANGO_SCRIPT_ARABIC is <literal>ar</literal>.
// 
// For some
// scripts, no sample language will be returned because there
// is no language that is sufficiently representative. The best
// example of this is %PANGO_SCRIPT_HAN, where various different
// variants of written Chinese, Japanese, and Korean all use
// significantly different sets of Han characters and forms
// of shared characters. No sample language can be provided
// for many historical scripts as well.
// 
// As of 1.18, this function checks the environment variables
// PANGO_LANGUAGE and LANGUAGE (checked in that order) first.
// If one of them is set, it is parsed as a list of language tags
// separated by colons or other separators.  This function
// will return the first language in the parsed list that Pango
// believes may use @script for writing.  This last predicate
// is tested using pango_language_includes_script().  This can
// be used to control Pango's font selection for non-primary
// languages.  For example, a PANGO_LANGUAGE enviroment variable
// set to "en:fa" makes Pango choose fonts suitable for Persian (fa) 
// instead of Arabic (ar) when a segment of Arabic text is found
// in an otherwise non-Arabic text.  The same trick can be used to
// choose a default language for %PANGO_SCRIPT_HAN when setting
// context language is not feasible.
// 
// of the script, or %NULL if no such language exists.
// RETURNS: a #PangoLanguage that is representative
// <script>: a #PangoScript
static Language* /*new*/ script_get_sample_language()(Script script) nothrow {
   return pango_script_get_sample_language(script);
}


// Given a segment of text and the corresponding
// #PangoAnalysis structure returned from pango_itemize(),
// convert the characters into glyphs. You may also pass
// in only a substring of the item from pango_itemize().
// <text>: the text to process
// <length>: the length (in bytes) of @text
// <analysis>: #PangoAnalysis structure from pango_itemize()
// <glyphs>: glyph string in which to store results
static void shape(AT0, AT1, AT2)(AT0 /*char*/ text, int length, AT1 /*Analysis*/ analysis, AT2 /*GlyphString*/ glyphs) nothrow {
   pango_shape(toCString!(char*)(text), length, UpCast!(Analysis*)(analysis), UpCast!(GlyphString*)(glyphs));
}


// Skips 0 or more characters of white space.
// 
// the position at a '\0' character.
// RETURNS: %FALSE if skipping the white space leaves
// <pos>: in/out string position
static int skip_space(AT0)(AT0 /*char**/ pos) nothrow {
   return pango_skip_space(toCString!(char**)(pos));
}


// Unintrospectable function: split_file_list() / pango_split_file_list()
// Splits a %G_SEARCHPATH_SEPARATOR-separated list of files, stripping
// white space and substituting ~/ with $HOME/.
// RETURNS: a list of strings to be freed with g_strfreev()
// <str>: a %G_SEARCHPATH_SEPARATOR separated list of filenames
static char** split_file_list(AT0)(AT0 /*char*/ str) nothrow {
   return pango_split_file_list(toCString!(char*)(str));
}


// Trims leading and trailing whitespace from a string.
// RETURNS: A newly-allocated string that must be freed with g_free()
// <str>: a string
static char* /*new*/ trim_string(AT0)(AT0 /*char*/ str) nothrow {
   return pango_trim_string(toCString!(char*)(str));
}


// Determines the inherent direction of a character; either
// %PANGO_DIRECTION_LTR, %PANGO_DIRECTION_RTL, or
// %PANGO_DIRECTION_NEUTRAL.
// 
// This function is useful to categorize characters into left-to-right
// letters, right-to-left letters, and everything else.  If full
// Unicode bidirectional type of a character is needed,
// pango_bidi_type_for_gunichar() can be used instead.
// RETURNS: the direction of the character.
// <ch>: a Unicode character
static Direction unichar_direction()(dchar ch) nothrow {
   return pango_unichar_direction(ch);
}


// VERSION: 1.16
// Converts a floating-point number to Pango units: multiplies
// it by %PANGO_SCALE and rounds to nearest integer.
// RETURNS: the value in Pango units.
// <d>: double floating-point value
static int units_from_double()(double d) nothrow {
   return pango_units_from_double(d);
}


// VERSION: 1.16
// Converts a number in Pango units to floating-point: divides
// it by %PANGO_SCALE.
// RETURNS: the double value.
// <i>: value in Pango units
static double units_to_double()(int i) nothrow {
   return pango_units_to_double(i);
}


// VERSION: 1.16
// This is similar to the macro %PANGO_VERSION except that
// it returns the encoded version of Pango available at run-time,
// as opposed to the version available at compile-time.
// 
// A version number can be encoded into an integer using
// PANGO_VERSION_ENCODE().
// 
// available at run time.
// RETURNS: The encoded version of Pango library
static int version_()() nothrow {
   return pango_version();
}


// VERSION: 1.16
// Checks that the Pango library in use is compatible with the
// given version. Generally you would pass in the constants
// %PANGO_VERSION_MAJOR, %PANGO_VERSION_MINOR, %PANGO_VERSION_MICRO
// as the three arguments to this function; that produces
// a check that the library in use at run-time is compatible with
// the version of Pango the application or module was compiled against.
// 
// Compatibility is defined by two things: first the version
// of the running library is newer than the version
// @required_major.required_minor.@required_micro. Second
// the running library must be binary compatible with the
// version @required_major.required_minor.@required_micro
// (same major version.)
// 
// For compile-time version checking use PANGO_VERSION_CHECK().
// 
// given version, or a string describing the version mismatch.
// The returned string is owned by Pango and should not be modified
// or freed.
// RETURNS: %NULL if the Pango library is compatible with the
// <required_major>: the required major version.
// <required_minor>: the required minor version.
// <required_micro>: the required major version.
static char* version_check()(int required_major, int required_minor, int required_micro) nothrow {
   return pango_version_check(required_major, required_minor, required_micro);
}


// VERSION: 1.16
// This is similar to the macro %PANGO_VERSION_STRING except that
// it returns the version of Pango available at run-time, as opposed to
// the version available at compile-time.
// 
// available at run time.
// The returned string is owned by Pango and should not be modified
// or freed.
// RETURNS: A string containing the version of Pango library
static char* version_string()() nothrow {
   return pango_version_string();
}


// C prototypes:

extern (C) {
Attribute* pango_attr_font_desc_new(FontDescription* desc) nothrow;
AttrIterator* pango_attr_iterator_copy(AttrIterator* this_) nothrow;
void pango_attr_iterator_destroy(AttrIterator* this_) nothrow;
Attribute* pango_attr_iterator_get(AttrIterator* this_, AttrType type) nothrow;
GLib2.SList* /*new*/ pango_attr_iterator_get_attrs(AttrIterator* this_) nothrow;
void pango_attr_iterator_get_font(AttrIterator* this_, FontDescription* desc, Language** language, GLib2.SList** extra_attrs) nothrow;
int pango_attr_iterator_next(AttrIterator* this_) nothrow;
void pango_attr_iterator_range(AttrIterator* this_, int* start, int* end) nothrow;
Attribute* pango_attr_language_new(Language* language) nothrow;
AttrList* /*new*/ pango_attr_list_new() nothrow;
void pango_attr_list_change(AttrList* this_, Attribute* attr) nothrow;
AttrList* /*new*/ pango_attr_list_copy(AttrList* this_) nothrow;
AttrList* /*new*/ pango_attr_list_filter(AttrList* this_, AttrFilterFunc func, void* data) nothrow;
AttrIterator* pango_attr_list_get_iterator(AttrList* this_) nothrow;
void pango_attr_list_insert(AttrList* this_, Attribute* attr) nothrow;
void pango_attr_list_insert_before(AttrList* this_, Attribute* attr) nothrow;
AttrList* /*new*/ pango_attr_list_ref(AttrList* this_) nothrow;
void pango_attr_list_splice(AttrList* this_, AttrList* other, int pos, int len) nothrow;
void pango_attr_list_unref(AttrList* this_) nothrow;
Attribute* pango_attr_shape_new(Rectangle* ink_rect, Rectangle* logical_rect) nothrow;
Attribute* pango_attr_shape_new_with_data(Rectangle* ink_rect, Rectangle* logical_rect, void* data, AttrDataCopyFunc copy_func, GLib2.DestroyNotify destroy_func) nothrow;
Attribute* pango_attr_size_new(int size) nothrow;
Attribute* pango_attr_size_new_absolute(int size) nothrow;
Attribute* pango_attribute_copy(Attribute* this_) nothrow;
void pango_attribute_destroy(Attribute* this_) nothrow;
int pango_attribute_equal(Attribute* this_, Attribute* attr2) nothrow;
void pango_attribute_init(Attribute* this_, AttrClass* klass) nothrow;
Color* /*new*/ pango_color_copy(Color* this_) nothrow;
void pango_color_free(Color* this_) nothrow;
int pango_color_parse(Color* this_, char* spec) nothrow;
char* /*new*/ pango_color_to_string(Color* this_) nothrow;
Context* /*new*/ pango_context_new() nothrow;
Direction pango_context_get_base_dir(Context* this_) nothrow;
Gravity pango_context_get_base_gravity(Context* this_) nothrow;
FontDescription* /*new*/ pango_context_get_font_description(Context* this_) nothrow;
FontMap* pango_context_get_font_map(Context* this_) nothrow;
Gravity pango_context_get_gravity(Context* this_) nothrow;
GravityHint pango_context_get_gravity_hint(Context* this_) nothrow;
Language* /*new*/ pango_context_get_language(Context* this_) nothrow;
Matrix* pango_context_get_matrix(Context* this_) nothrow;
FontMetrics* /*new*/ pango_context_get_metrics(Context* this_, FontDescription* desc, Language* language) nothrow;
void pango_context_list_families(Context* this_, FontFamily*** families, int* n_families) nothrow;
Font* pango_context_load_font(Context* this_, FontDescription* desc) nothrow;
Fontset* pango_context_load_fontset(Context* this_, FontDescription* desc, Language* language) nothrow;
void pango_context_set_base_dir(Context* this_, Direction direction) nothrow;
void pango_context_set_base_gravity(Context* this_, Gravity gravity) nothrow;
void pango_context_set_font_description(Context* this_, FontDescription* desc) nothrow;
void pango_context_set_font_map(Context* this_, FontMap* font_map) nothrow;
void pango_context_set_gravity_hint(Context* this_, GravityHint hint) nothrow;
void pango_context_set_language(Context* this_, Language* language) nothrow;
void pango_context_set_matrix(Context* this_, Matrix* matrix) nothrow;
Coverage* pango_coverage_copy(Coverage* this_) nothrow;
CoverageLevel pango_coverage_get(Coverage* this_, int index_) nothrow;
void pango_coverage_max(Coverage* this_, Coverage* other) nothrow;
Coverage* pango_coverage_ref(Coverage* this_) nothrow;
void pango_coverage_set(Coverage* this_, int index_, CoverageLevel level) nothrow;
void pango_coverage_to_bytes(Coverage* this_, ubyte** bytes, int* n_bytes) nothrow;
void pango_coverage_unref(Coverage* this_) nothrow;
Coverage* pango_coverage_from_bytes(ubyte* bytes, int n_bytes) nothrow;
Coverage* pango_coverage_new() nothrow;
void pango_font_descriptions_free(FontDescription** descs, int n_descs) nothrow;
FontDescription* /*new*/ pango_font_describe(Font* this_) nothrow;
FontDescription* /*new*/ pango_font_describe_with_absolute_size(Font* this_) nothrow;
EngineShape* pango_font_find_shaper(Font* this_, Language* language, uint ch) nothrow;
Coverage* pango_font_get_coverage(Font* this_, Language* language) nothrow;
FontMap* pango_font_get_font_map(Font* this_) nothrow;
void pango_font_get_glyph_extents(Font* this_, Glyph glyph, Rectangle* ink_rect, Rectangle* logical_rect) nothrow;
FontMetrics* /*new*/ pango_font_get_metrics(Font* this_, Language* language) nothrow;
FontDescription* /*new*/ pango_font_description_new() nothrow;
int pango_font_description_better_match(FontDescription* this_, FontDescription* old_match, FontDescription* new_match) nothrow;
FontDescription* /*new*/ pango_font_description_copy(FontDescription* this_) nothrow;
FontDescription* /*new*/ pango_font_description_copy_static(FontDescription* this_) nothrow;
int pango_font_description_equal(FontDescription* this_, FontDescription* desc2) nothrow;
void pango_font_description_free(FontDescription* this_) nothrow;
char* pango_font_description_get_family(FontDescription* this_) nothrow;
Gravity pango_font_description_get_gravity(FontDescription* this_) nothrow;
FontMask pango_font_description_get_set_fields(FontDescription* this_) nothrow;
int pango_font_description_get_size(FontDescription* this_) nothrow;
int pango_font_description_get_size_is_absolute(FontDescription* this_) nothrow;
Stretch pango_font_description_get_stretch(FontDescription* this_) nothrow;
Style pango_font_description_get_style(FontDescription* this_) nothrow;
Variant pango_font_description_get_variant(FontDescription* this_) nothrow;
Weight pango_font_description_get_weight(FontDescription* this_) nothrow;
uint pango_font_description_hash(FontDescription* this_) nothrow;
void pango_font_description_merge(FontDescription* this_, FontDescription* desc_to_merge, int replace_existing) nothrow;
void pango_font_description_merge_static(FontDescription* this_, FontDescription* desc_to_merge, int replace_existing) nothrow;
void pango_font_description_set_absolute_size(FontDescription* this_, double size) nothrow;
void pango_font_description_set_family(FontDescription* this_, char* family) nothrow;
void pango_font_description_set_family_static(FontDescription* this_, char* family) nothrow;
void pango_font_description_set_gravity(FontDescription* this_, Gravity gravity) nothrow;
void pango_font_description_set_size(FontDescription* this_, int size) nothrow;
void pango_font_description_set_stretch(FontDescription* this_, Stretch stretch) nothrow;
void pango_font_description_set_style(FontDescription* this_, Style style) nothrow;
void pango_font_description_set_variant(FontDescription* this_, Variant variant) nothrow;
void pango_font_description_set_weight(FontDescription* this_, Weight weight) nothrow;
char* /*new*/ pango_font_description_to_filename(FontDescription* this_) nothrow;
char* /*new*/ pango_font_description_to_string(FontDescription* this_) nothrow;
void pango_font_description_unset_fields(FontDescription* this_, FontMask to_unset) nothrow;
FontDescription* /*new*/ pango_font_description_from_string(char* str) nothrow;
FontDescription* /*new*/ pango_font_face_describe(FontFace* this_) nothrow;
char* pango_font_face_get_face_name(FontFace* this_) nothrow;
int pango_font_face_is_synthesized(FontFace* this_) nothrow;
void pango_font_face_list_sizes(FontFace* this_, int** sizes, int* n_sizes) nothrow;
char* pango_font_family_get_name(FontFamily* this_) nothrow;
int pango_font_family_is_monospace(FontFamily* this_) nothrow;
void pango_font_family_list_faces(FontFamily* this_, FontFace*** faces, int* n_faces) nothrow;
Context* pango_font_map_create_context(FontMap* this_) nothrow;
void pango_font_map_list_families(FontMap* this_, FontFamily*** families, int* n_families) nothrow;
Font* pango_font_map_load_font(FontMap* this_, Context* context, FontDescription* desc) nothrow;
Fontset* pango_font_map_load_fontset(FontMap* this_, Context* context, FontDescription* desc, Language* language) nothrow;
int pango_font_metrics_get_approximate_char_width(FontMetrics* this_) nothrow;
int pango_font_metrics_get_approximate_digit_width(FontMetrics* this_) nothrow;
int pango_font_metrics_get_ascent(FontMetrics* this_) nothrow;
int pango_font_metrics_get_descent(FontMetrics* this_) nothrow;
int pango_font_metrics_get_strikethrough_position(FontMetrics* this_) nothrow;
int pango_font_metrics_get_strikethrough_thickness(FontMetrics* this_) nothrow;
int pango_font_metrics_get_underline_position(FontMetrics* this_) nothrow;
int pango_font_metrics_get_underline_thickness(FontMetrics* this_) nothrow;
FontMetrics* /*new*/ pango_font_metrics_ref(FontMetrics* this_) nothrow;
void pango_font_metrics_unref(FontMetrics* this_) nothrow;
void pango_fontset_foreach(Fontset* this_, FontsetForeachFunc func, void* data) nothrow;
Font* pango_fontset_get_font(Fontset* this_, uint wc) nothrow;
FontMetrics* /*new*/ pango_fontset_get_metrics(Fontset* this_) nothrow;
GLib2.SList* pango_glyph_item_apply_attrs(GlyphItem* this_, char* text, AttrList* list) nothrow;
GlyphItem* /*new*/ pango_glyph_item_copy(GlyphItem* this_) nothrow;
void pango_glyph_item_free(GlyphItem* this_) nothrow;
void pango_glyph_item_get_logical_widths(GlyphItem* this_, char* text, int* logical_widths) nothrow;
void pango_glyph_item_letter_space(GlyphItem* this_, char* text, LogAttr* log_attrs, int letter_spacing) nothrow;
GlyphItem* /*new*/ pango_glyph_item_split(GlyphItem* this_, char* text, int split_index) nothrow;
GlyphItemIter* /*new*/ pango_glyph_item_iter_copy(GlyphItemIter* this_) nothrow;
void pango_glyph_item_iter_free(GlyphItemIter* this_) nothrow;
int pango_glyph_item_iter_init_end(GlyphItemIter* this_, GlyphItem* glyph_item, char* text) nothrow;
int pango_glyph_item_iter_init_start(GlyphItemIter* this_, GlyphItem* glyph_item, char* text) nothrow;
int pango_glyph_item_iter_next_cluster(GlyphItemIter* this_) nothrow;
int pango_glyph_item_iter_prev_cluster(GlyphItemIter* this_) nothrow;
GlyphString* /*new*/ pango_glyph_string_new() nothrow;
GlyphString* /*new*/ pango_glyph_string_copy(GlyphString* this_) nothrow;
void pango_glyph_string_extents(GlyphString* this_, Font* font, Rectangle* ink_rect, Rectangle* logical_rect) nothrow;
void pango_glyph_string_extents_range(GlyphString* this_, int start, int end, Font* font, Rectangle* ink_rect, Rectangle* logical_rect) nothrow;
void pango_glyph_string_free(GlyphString* this_) nothrow;
void pango_glyph_string_get_logical_widths(GlyphString* this_, char* text, int length, int embedding_level, int* logical_widths) nothrow;
int pango_glyph_string_get_width(GlyphString* this_) nothrow;
void pango_glyph_string_index_to_x(GlyphString* this_, char* text, int length, Analysis* analysis, int index_, int trailing, int* x_pos) nothrow;
void pango_glyph_string_set_size(GlyphString* this_, int new_len) nothrow;
void pango_glyph_string_x_to_index(GlyphString* this_, char* text, int length, Analysis* analysis, int x_pos, int* index_, int* trailing) nothrow;
Item* /*new*/ pango_item_new() nothrow;
Item* /*new*/ pango_item_copy(Item* this_) nothrow;
void pango_item_free(Item* this_) nothrow;
Item* /*new*/ pango_item_split(Item* this_, int split_index, int split_offset) nothrow;
char* pango_language_get_sample_string(Language* this_) nothrow;
Script* pango_language_get_scripts(Language* this_, int* num_scripts) nothrow;
int pango_language_includes_script(Language* this_, Script script) nothrow;
int pango_language_matches(Language* this_, char* range_list) nothrow;
char* pango_language_to_string(Language* this_) nothrow;
Language* /*new*/ pango_language_from_string(char* language) nothrow;
Language* /*new*/ pango_language_get_default() nothrow;
Layout* /*new*/ pango_layout_new(Context* context) nothrow;
void pango_layout_context_changed(Layout* this_) nothrow;
Layout* pango_layout_copy(Layout* this_) nothrow;
Alignment pango_layout_get_alignment(Layout* this_) nothrow;
AttrList* /*new*/ pango_layout_get_attributes(Layout* this_) nothrow;
int pango_layout_get_auto_dir(Layout* this_) nothrow;
int pango_layout_get_baseline(Layout* this_) nothrow;
Context* pango_layout_get_context(Layout* this_) nothrow;
void pango_layout_get_cursor_pos(Layout* this_, int index_, /*out*/ Rectangle* strong_pos, /*out*/ Rectangle* weak_pos) nothrow;
EllipsizeMode pango_layout_get_ellipsize(Layout* this_) nothrow;
void pango_layout_get_extents(Layout* this_, /*out*/ Rectangle* ink_rect, /*out*/ Rectangle* logical_rect) nothrow;
FontDescription* pango_layout_get_font_description(Layout* this_) nothrow;
int pango_layout_get_height(Layout* this_) nothrow;
int pango_layout_get_indent(Layout* this_) nothrow;
LayoutIter* /*new*/ pango_layout_get_iter(Layout* this_) nothrow;
int pango_layout_get_justify(Layout* this_) nothrow;
LayoutLine* /*new*/ pango_layout_get_line(Layout* this_, int line) nothrow;
int pango_layout_get_line_count(Layout* this_) nothrow;
LayoutLine* /*new*/ pango_layout_get_line_readonly(Layout* this_, int line) nothrow;
GLib2.SList* pango_layout_get_lines(Layout* this_) nothrow;
GLib2.SList* pango_layout_get_lines_readonly(Layout* this_) nothrow;
void pango_layout_get_log_attrs(Layout* this_, /*out*/ LogAttr** attrs, /*out*/ int* n_attrs) nothrow;
void pango_layout_get_pixel_extents(Layout* this_, /*out*/ Rectangle* ink_rect, /*out*/ Rectangle* logical_rect) nothrow;
void pango_layout_get_pixel_size(Layout* this_, /*out*/ int* width, /*out*/ int* height) nothrow;
int pango_layout_get_single_paragraph_mode(Layout* this_) nothrow;
void pango_layout_get_size(Layout* this_, /*out*/ int* width, /*out*/ int* height) nothrow;
int pango_layout_get_spacing(Layout* this_) nothrow;
TabArray* /*new*/ pango_layout_get_tabs(Layout* this_) nothrow;
char* pango_layout_get_text(Layout* this_) nothrow;
int pango_layout_get_unknown_glyphs_count(Layout* this_) nothrow;
int pango_layout_get_width(Layout* this_) nothrow;
WrapMode pango_layout_get_wrap(Layout* this_) nothrow;
void pango_layout_index_to_line_x(Layout* this_, int index_, int trailing, /*out*/ int* line, int* x_pos) nothrow;
void pango_layout_index_to_pos(Layout* this_, int index_, /*out*/ Rectangle* pos) nothrow;
int pango_layout_is_ellipsized(Layout* this_) nothrow;
int pango_layout_is_wrapped(Layout* this_) nothrow;
void pango_layout_move_cursor_visually(Layout* this_, int strong, int old_index, int old_trailing, int direction, /*out*/ int* new_index, int* new_trailing) nothrow;
void pango_layout_set_alignment(Layout* this_, Alignment alignment) nothrow;
void pango_layout_set_attributes(Layout* this_, AttrList* attrs) nothrow;
void pango_layout_set_auto_dir(Layout* this_, int auto_dir) nothrow;
void pango_layout_set_ellipsize(Layout* this_, EllipsizeMode ellipsize) nothrow;
void pango_layout_set_font_description(Layout* this_, FontDescription* desc) nothrow;
void pango_layout_set_height(Layout* this_, int height) nothrow;
void pango_layout_set_indent(Layout* this_, int indent) nothrow;
void pango_layout_set_justify(Layout* this_, int justify) nothrow;
void pango_layout_set_markup(Layout* this_, char* markup, int length) nothrow;
void pango_layout_set_markup_with_accel(Layout* this_, char* markup, int length, dchar accel_marker, dchar* accel_char) nothrow;
void pango_layout_set_single_paragraph_mode(Layout* this_, int setting) nothrow;
void pango_layout_set_spacing(Layout* this_, int spacing) nothrow;
void pango_layout_set_tabs(Layout* this_, TabArray* tabs) nothrow;
void pango_layout_set_text(Layout* this_, char* text, int length) nothrow;
void pango_layout_set_width(Layout* this_, int width) nothrow;
void pango_layout_set_wrap(Layout* this_, WrapMode wrap) nothrow;
int pango_layout_xy_to_index(Layout* this_, int x, int y, /*out*/ int* index_, /*out*/ int* trailing) nothrow;
int pango_layout_iter_at_last_line(LayoutIter* this_) nothrow;
LayoutIter* /*new*/ pango_layout_iter_copy(LayoutIter* this_) nothrow;
void pango_layout_iter_free(LayoutIter* this_) nothrow;
int pango_layout_iter_get_baseline(LayoutIter* this_) nothrow;
void pango_layout_iter_get_char_extents(LayoutIter* this_, Rectangle* logical_rect) nothrow;
void pango_layout_iter_get_cluster_extents(LayoutIter* this_, Rectangle* ink_rect, Rectangle* logical_rect) nothrow;
int pango_layout_iter_get_index(LayoutIter* this_) nothrow;
Layout* pango_layout_iter_get_layout(LayoutIter* this_) nothrow;
void pango_layout_iter_get_layout_extents(LayoutIter* this_, Rectangle* ink_rect, Rectangle* logical_rect) nothrow;
LayoutLine* /*new*/ pango_layout_iter_get_line(LayoutIter* this_) nothrow;
void pango_layout_iter_get_line_extents(LayoutIter* this_, Rectangle* ink_rect, Rectangle* logical_rect) nothrow;
LayoutLine* /*new*/ pango_layout_iter_get_line_readonly(LayoutIter* this_) nothrow;
void pango_layout_iter_get_line_yrange(LayoutIter* this_, int* y0_, int* y1_) nothrow;
LayoutRun* pango_layout_iter_get_run(LayoutIter* this_) nothrow;
void pango_layout_iter_get_run_extents(LayoutIter* this_, Rectangle* ink_rect, Rectangle* logical_rect) nothrow;
LayoutRun* pango_layout_iter_get_run_readonly(LayoutIter* this_) nothrow;
int pango_layout_iter_next_char(LayoutIter* this_) nothrow;
int pango_layout_iter_next_cluster(LayoutIter* this_) nothrow;
int pango_layout_iter_next_line(LayoutIter* this_) nothrow;
int pango_layout_iter_next_run(LayoutIter* this_) nothrow;
void pango_layout_line_get_extents(LayoutLine* this_, /*out*/ Rectangle* ink_rect, /*out*/ Rectangle* logical_rect) nothrow;
void pango_layout_line_get_pixel_extents(LayoutLine* this_, /*out*/ Rectangle* ink_rect, /*out*/ Rectangle* logical_rect) nothrow;
void pango_layout_line_get_x_ranges(LayoutLine* this_, int start_index, int end_index, /*out*/ int** ranges, /*out*/ int* n_ranges) nothrow;
void pango_layout_line_index_to_x(LayoutLine* this_, int index_, int trailing, /*out*/ int* x_pos) nothrow;
LayoutLine* /*new*/ pango_layout_line_ref(LayoutLine* this_) nothrow;
void pango_layout_line_unref(LayoutLine* this_) nothrow;
int pango_layout_line_x_to_index(LayoutLine* this_, int x_pos, /*out*/ int* index_, /*out*/ int* trailing) nothrow;
void pango_matrix_concat(Matrix* this_, Matrix* new_matrix) nothrow;
Matrix* /*new*/ pango_matrix_copy(Matrix* this_) nothrow;
void pango_matrix_free(Matrix* this_) nothrow;
double pango_matrix_get_font_scale_factor(Matrix* this_) nothrow;
void pango_matrix_rotate(Matrix* this_, double degrees) nothrow;
void pango_matrix_scale(Matrix* this_, double scale_x, double scale_y) nothrow;
void pango_matrix_transform_distance(Matrix* this_, double* dx, double* dy) nothrow;
void pango_matrix_transform_pixel_rectangle(Matrix* this_, Rectangle* rect) nothrow;
void pango_matrix_transform_point(Matrix* this_, double* x, double* y) nothrow;
void pango_matrix_transform_rectangle(Matrix* this_, Rectangle* rect) nothrow;
void pango_matrix_translate(Matrix* this_, double tx, double ty) nothrow;
void pango_renderer_activate(Renderer* this_) nothrow;
void pango_renderer_deactivate(Renderer* this_) nothrow;
void pango_renderer_draw_error_underline(Renderer* this_, int x, int y, int width, int height) nothrow;
void pango_renderer_draw_glyph(Renderer* this_, Font* font, Glyph glyph, double x, double y) nothrow;
void pango_renderer_draw_glyph_item(Renderer* this_, char* text, GlyphItem* glyph_item, int x, int y) nothrow;
void pango_renderer_draw_glyphs(Renderer* this_, Font* font, GlyphString* glyphs, int x, int y) nothrow;
void pango_renderer_draw_layout(Renderer* this_, Layout* layout, int x, int y) nothrow;
void pango_renderer_draw_layout_line(Renderer* this_, LayoutLine* line, int x, int y) nothrow;
void pango_renderer_draw_rectangle(Renderer* this_, RenderPart part, int x, int y, int width, int height) nothrow;
void pango_renderer_draw_trapezoid(Renderer* this_, RenderPart part, double y1_, double x11, double x21, double y2, double x12, double x22) nothrow;
Color* /*new*/ pango_renderer_get_color(Renderer* this_, RenderPart part) nothrow;
Layout* pango_renderer_get_layout(Renderer* this_) nothrow;
LayoutLine* /*new*/ pango_renderer_get_layout_line(Renderer* this_) nothrow;
Matrix* pango_renderer_get_matrix(Renderer* this_) nothrow;
void pango_renderer_part_changed(Renderer* this_, RenderPart part) nothrow;
void pango_renderer_set_color(Renderer* this_, RenderPart part, Color* color) nothrow;
void pango_renderer_set_matrix(Renderer* this_, Matrix* matrix) nothrow;
void pango_script_iter_free(ScriptIter* this_) nothrow;
void pango_script_iter_get_range(ScriptIter* this_, char** start, char** end, Script* script) nothrow;
int pango_script_iter_next(ScriptIter* this_) nothrow;
ScriptIter* pango_script_iter_new(char* text, int length) nothrow;
TabArray* /*new*/ pango_tab_array_new(int initial_size, int positions_in_pixels) nothrow;
TabArray* /*new*/ pango_tab_array_new_with_positions(int size, int positions_in_pixels, TabAlign first_alignment, int first_position, ...) nothrow;
TabArray* /*new*/ pango_tab_array_copy(TabArray* this_) nothrow;
void pango_tab_array_free(TabArray* this_) nothrow;
int pango_tab_array_get_positions_in_pixels(TabArray* this_) nothrow;
int pango_tab_array_get_size(TabArray* this_) nothrow;
void pango_tab_array_get_tab(TabArray* this_, int tab_index, TabAlign* alignment, int* location) nothrow;
void pango_tab_array_get_tabs(TabArray* this_, TabAlign** alignments, int** locations) nothrow;
void pango_tab_array_resize(TabArray* this_, int new_size) nothrow;
void pango_tab_array_set_tab(TabArray* this_, int tab_index, TabAlign alignment, int location) nothrow;
Attribute* pango_attr_background_new(ushort red, ushort green, ushort blue) nothrow;
Attribute* pango_attr_fallback_new(int enable_fallback) nothrow;
Attribute* pango_attr_family_new(char* family) nothrow;
Attribute* pango_attr_foreground_new(ushort red, ushort green, ushort blue) nothrow;
Attribute* pango_attr_gravity_hint_new(GravityHint hint) nothrow;
Attribute* pango_attr_gravity_new(Gravity gravity) nothrow;
Attribute* pango_attr_letter_spacing_new(int letter_spacing) nothrow;
Attribute* pango_attr_rise_new(int rise) nothrow;
Attribute* pango_attr_scale_new(double scale_factor) nothrow;
Attribute* pango_attr_stretch_new(Stretch stretch) nothrow;
Attribute* pango_attr_strikethrough_color_new(ushort red, ushort green, ushort blue) nothrow;
Attribute* pango_attr_strikethrough_new(int strikethrough) nothrow;
Attribute* pango_attr_style_new(Style style) nothrow;
char* pango_attr_type_get_name(AttrType type) nothrow;
AttrType pango_attr_type_register(char* name) nothrow;
Attribute* pango_attr_underline_color_new(ushort red, ushort green, ushort blue) nothrow;
Attribute* pango_attr_underline_new(Underline underline) nothrow;
Attribute* pango_attr_variant_new(Variant variant) nothrow;
Attribute* pango_attr_weight_new(Weight weight) nothrow;
BidiType pango_bidi_type_for_unichar(dchar ch) nothrow;
void pango_break(char* text, int length, Analysis* analysis, LogAttr* attrs, int attrs_len) nothrow;
void pango_extents_to_pixels(Rectangle* inclusive, Rectangle* nearest) nothrow;
Direction pango_find_base_dir(char* text, int length) nothrow;
void pango_find_paragraph_boundary(char* text, int length, int* paragraph_delimiter_index, int* next_paragraph_start) nothrow;
void pango_get_log_attrs(char* text, int length, int level, Language* language, LogAttr* log_attrs, int attrs_len) nothrow;
int pango_get_mirror_char(dchar ch, dchar* mirrored_ch) nothrow;
Gravity pango_gravity_get_for_matrix(Matrix* matrix) nothrow;
Gravity pango_gravity_get_for_script(Script script, Gravity base_gravity, GravityHint hint) nothrow;
Gravity pango_gravity_get_for_script_and_width(Script script, int wide, Gravity base_gravity, GravityHint hint) nothrow;
double pango_gravity_to_rotation(Gravity gravity) nothrow;
int pango_is_zero_width(dchar ch) nothrow;
GLib2.List* pango_itemize(Context* context, char* text, int start_index, int length, AttrList* attrs, AttrIterator* cached_iter) nothrow;
GLib2.List* pango_itemize_with_base_dir(Context* context, Direction base_dir, char* text, int start_index, int length, AttrList* attrs, AttrIterator* cached_iter) nothrow;
ubyte* pango_log2vis_get_embedding_levels(char* text, int length, Direction* pbase_dir) nothrow;
int pango_parse_enum(Type type, char* str, int* value, int warn, char** possible_values) nothrow;
int pango_parse_markup(char* markup_text, int length, dchar accel_marker, /*out*/ AttrList** attr_list, /*out*/ char** text, /*out*/ dchar* accel_char, GLib2.Error** error) nothrow;
int pango_parse_stretch(char* str, Stretch* stretch, int warn) nothrow;
int pango_parse_style(char* str, Style* style, int warn) nothrow;
int pango_parse_variant(char* str, Variant* variant, int warn) nothrow;
int pango_parse_weight(char* str, Weight* weight, int warn) nothrow;
void pango_quantize_line_geometry(int* thickness, int* position) nothrow;
int pango_read_line(FILE* stream, GLib2.String* str) nothrow;
GLib2.List* pango_reorder_items(GLib2.List* logical_items) nothrow;
int pango_scan_int(char** pos, int* out_) nothrow;
int pango_scan_string(char** pos, GLib2.String* out_) nothrow;
int pango_scan_word(char** pos, GLib2.String* out_) nothrow;
Script pango_script_for_unichar(dchar ch) nothrow;
Language* /*new*/ pango_script_get_sample_language(Script script) nothrow;
void pango_shape(char* text, int length, Analysis* analysis, GlyphString* glyphs) nothrow;
int pango_skip_space(char** pos) nothrow;
char** pango_split_file_list(char* str) nothrow;
char* /*new*/ pango_trim_string(char* str) nothrow;
Direction pango_unichar_direction(dchar ch) nothrow;
int pango_units_from_double(double d) nothrow;
double pango_units_to_double(int i) nothrow;
int pango_version() nothrow;
char* pango_version_check(int required_major, int required_minor, int required_micro) nothrow;
char* pango_version_string() nothrow;
}

// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/PangoCairo-1.0.gir"

module PangoCairo;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;
public import gtk2.pango;
alias gtk2.pango Pango;
public import gtk2.pangoft2;
alias gtk2.pangoft2 PangoFT2;
public import gtk2.cairo;
alias gtk2.cairo cairo;
public import gtk2.fontconfig2;
alias gtk2.fontconfig2 fontconfig2;
public import gtk2.freetype2;
alias gtk2.freetype2 freetype2;

// package: "cairo";

// package: "gobject-2.0";

// c:symbol-prefixes: ["pango_cairo"]
// c:identifier-prefixes: ["PangoCairo"]

// module PangoCairo;

struct FcFontMap /* : Pango.FontMap */ {
   mixin FontMap.__interface__;
   alias parent_instance this;
   alias parent_instance super_;
   alias parent_instance fontmap;
   Pango.FontMap parent_instance;
   double dpi;
   freetype2.Library library;
}


// #PangoCairoFont is an interface exported by fonts for
// use with Cairo. The actual type of the font will depend
// on the particular font technology Cairo was compiled to use.
struct Font /* Interface */ /* Version 1.18 */ {
   mixin template __interface__() {
      // VERSION: 1.18
      // Gets the #cairo_scaled_font_t used by @font.
      // The scaled font can be referenced and kept using
      // cairo_scaled_font_reference().
      // 
      // or %NULL if @font is %NULL.
      // RETURNS: the #cairo_scaled_font_t used by @font,
      cairo.ScaledFont* /*new*/ get_scaled_font()() nothrow {
         return pango_cairo_font_get_scaled_font(cast(Font*)&this);
      }
   }
   mixin __interface__;
}


// #PangoCairoFontMap is an interface exported by font maps for
// use with Cairo. The actual type of the font map will depend
// on the particular font technology Cairo was compiled to use.
struct FontMap /* Interface */ /* Version 1.10 */ {
   mixin template __interface__() {
      // Unintrospectable function: get_default() / pango_cairo_font_map_get_default()
      // VERSION: 1.10
      // Gets a default #PangoCairoFontMap to use with Cairo.
      // 
      // Note that the type of the returned object will depend
      // on the particular font backend Cairo was compiled to use;
      // You generally should only use the #PangoFontMap and
      // #PangoCairoFontMap interfaces on the returned object.
      // 
      // The default Cairo fontmap can be changed by using
      // pango_cairo_font_map_set_default().  This can be used to
      // change the Cairo font backend that the default fontmap
      // uses for example.
      // 
      // object is owned by Pango and must not be freed.
      // RETURNS: the default Cairo fontmap for Pango. This
      static Pango.FontMap* get_default()() nothrow {
         return pango_cairo_font_map_get_default();
      }

      // Unintrospectable function: new() / pango_cairo_font_map_new()
      // VERSION: 1.10
      // Creates a new #PangoCairoFontMap object; a fontmap is used
      // to cache information about available fonts, and holds
      // certain global parameters such as the resolution.
      // In most cases, you can use pango_cairo_font_map_get_default()
      // instead.
      // 
      // Note that the type of the returned object will depend
      // on the particular font backend Cairo was compiled to use;
      // You generally should only use the #PangoFontMap and
      // #PangoCairoFontMap interfaces on the returned object.
      // 
      // be freed with g_object_unref().
      // RETURNS: the newly allocated #PangoFontMap, which should
      static Pango.FontMap* new_()() nothrow {
         return pango_cairo_font_map_new();
      }

      // Unintrospectable function: new_for_font_type() / pango_cairo_font_map_new_for_font_type()
      // VERSION: 1.18
      // Creates a new #PangoCairoFontMap object of the type suitable
      // to be used with cairo font backend of type @fonttype.
      // 
      // In most cases one should simply use @pango_cairo_font_map_new(),
      // or in fact in most of those cases, just use
      // @pango_cairo_font_map_get_default().
      // 
      // which should be freed with g_object_unref(),
      // or %NULL if the requested cairo font backend is
      // not supported / compiled in.
      // RETURNS: the newly allocated #PangoFontMap of suitable type
      // <fonttype>: desired #cairo_font_type_t
      static Pango.FontMap* new_for_font_type()(cairo.FontType fonttype) nothrow {
         return pango_cairo_font_map_new_for_font_type(fonttype);
      }

      // Unintrospectable method: create_context() / pango_cairo_font_map_create_context()
      // VERSION: 1.10
      // DEPRECATED (v1.22) method: create_context - Use pango_font_map_create_context() instead.
      // Create a #PangoContext for the given fontmap.
      // RETURNS: the newly created context; free with g_object_unref().
      Pango.Context* create_context()() nothrow {
         return pango_cairo_font_map_create_context(cast(FontMap*)&this);
      }

      // VERSION: 1.18
      // Gets the type of Cairo font backend that @fontmap uses.
      // RETURNS: the #cairo_font_type_t cairo font backend type
      cairo.FontType /*new*/ get_font_type()() nothrow {
         return pango_cairo_font_map_get_font_type(cast(FontMap*)&this);
      }

      // VERSION: 1.10
      // Gets the resolution for the fontmap. See pango_cairo_font_map_set_resolution()
      // RETURNS: the resolution in "dots per inch"
      double get_resolution()() nothrow {
         return pango_cairo_font_map_get_resolution(cast(FontMap*)&this);
      }

      // VERSION: 1.22
      // Sets a default #PangoCairoFontMap to use with Cairo.
      // 
      // This can be used to change the Cairo font backend that the
      // default fontmap uses for example.  The old default font map
      // is unreffed and the new font map referenced.
      // 
      // A value of %NULL for @fontmap will cause the current default
      // font map to be released and a new default font
      // map to be created on demand, using pango_cairo_font_map_new().
      void set_default()() nothrow {
         pango_cairo_font_map_set_default(cast(FontMap*)&this);
      }

      // VERSION: 1.10
      // Sets the resolution for the fontmap. This is a scale factor between
      // points specified in a #PangoFontDescription and Cairo units. The
      // default value is 96, meaning that a 10 point font will be 13
      // units high. (10 * 96. / 72. = 13.3).
      // <dpi>: the resolution in "dots per inch". (Physical inches aren't actually involved; the terminology is conventional.)
      void set_resolution()(double dpi) nothrow {
         pango_cairo_font_map_set_resolution(cast(FontMap*)&this, dpi);
      }
   }
   mixin __interface__;
}

extern (C) alias void function (cairo.Context* cr, Pango.AttrShape* attr, int do_path, void* data) nothrow ShapeRendererFunc;


// VERSION: 1.10
// Retrieves any font rendering options previously set with
// pango_cairo_font_map_set_font_options(). This function does not report options
// that are derived from the target surface by pango_cairo_update_context()
// 
// if no options have been set. This value is owned by the context
// and must not be modified or freed.
// RETURNS: the font options previously set on the context, or %NULL
// <context>: a #PangoContext, from a pangocairo font map
static cairo.FontOptions* context_get_font_options(AT0)(AT0 /*Pango.Context*/ context) nothrow {
   return pango_cairo_context_get_font_options(UpCast!(Pango.Context*)(context));
}


// VERSION: 1.10
// Gets the resolution for the context. See pango_cairo_context_set_resolution()
// 
// be returned if no resolution has previously been set.
// RETURNS: the resolution in "dots per inch". A negative value will
// <context>: a #PangoContext, from a pangocairo font map
static double context_get_resolution(AT0)(AT0 /*Pango.Context*/ context) nothrow {
   return pango_cairo_context_get_resolution(UpCast!(Pango.Context*)(context));
}


// Unintrospectable function: context_get_shape_renderer() / pango_cairo_context_get_shape_renderer()
// VERSION: 1.18
// Sets callback function for context to use for rendering attributes
// of type %PANGO_ATTR_SHAPE.  See #PangoCairoShapeRendererFunc for
// details.
// 
// Retrieves callback function and associated user data for rendering
// attributes of type %PANGO_ATTR_SHAPE as set by
// pango_cairo_context_set_shape_renderer(), if any.
// 
// if no shape rendering callback have been set.
// RETURNS: the shape rendering callback previously set on the context, or %NULL
// <context>: a #PangoContext, from a pangocairo font map
// <data>: Pointer to #gpointer to return user data
static ShapeRendererFunc context_get_shape_renderer(AT0, AT1)(AT0 /*Pango.Context*/ context, AT1 /*void**/ data) nothrow {
   return pango_cairo_context_get_shape_renderer(UpCast!(Pango.Context*)(context), UpCast!(void**)(data));
}


// VERSION: 1.10
// Sets the font options used when rendering text with this context.
// These options override any options that pango_cairo_update_context()
// derives from the target surface.
// <context>: a #PangoContext, from a pangocairo font map
// <options>: a #cairo_font_options_t, or %NULL to unset any previously set options. A copy is made.
static void context_set_font_options(AT0, AT1)(AT0 /*Pango.Context*/ context, AT1 /*cairo.FontOptions*/ options) nothrow {
   pango_cairo_context_set_font_options(UpCast!(Pango.Context*)(context), UpCast!(cairo.FontOptions*)(options));
}


// VERSION: 1.10
// Sets the resolution for the context. This is a scale factor between
// points specified in a #PangoFontDescription and Cairo units. The
// default value is 96, meaning that a 10 point font will be 13
// units high. (10 * 96. / 72. = 13.3).
// <context>: a #PangoContext, from a pangocairo font map
// <dpi>: the resolution in "dots per inch". (Physical inches aren't actually involved; the terminology is conventional.) A 0 or negative value means to use the resolution from the font map.
static void context_set_resolution(AT0)(AT0 /*Pango.Context*/ context, double dpi) nothrow {
   pango_cairo_context_set_resolution(UpCast!(Pango.Context*)(context), dpi);
}


// VERSION: 1.18
// Sets callback function for context to use for rendering attributes
// of type %PANGO_ATTR_SHAPE.  See #PangoCairoShapeRendererFunc for
// details.
// <context>: a #PangoContext, from a pangocairo font map
// <func>: Callback function for rendering attributes of type %PANGO_ATTR_SHAPE, or %NULL to disable shape rendering.
// <data>: User data that will be passed to @func.
// <dnotify>: Callback that will be called when the context is freed to release @data, or %NULL.
static void context_set_shape_renderer(AT0, AT1)(AT0 /*Pango.Context*/ context, ShapeRendererFunc func, AT1 /*void*/ data, GLib2.DestroyNotify dnotify) nothrow {
   pango_cairo_context_set_shape_renderer(UpCast!(Pango.Context*)(context), func, UpCast!(void*)(data), dnotify);
}


// Unintrospectable function: create_context() / pango_cairo_create_context()
// VERSION: 1.22
// Creates a context object set up to match the current transformation
// and target surface of the Cairo context.  This context can then be
// used to create a layout using pango_layout_new().
// 
// This function is a convenience function that creates a context using
// the default font map, then updates it to @cr.  If you just need to
// create a layout for use with @cr and do not need to access #PangoContext
// directly, you can use pango_cairo_create_layout() instead.
// 
// g_object_unref().
// RETURNS: the newly created #PangoContext. Free with
// <cr>: a Cairo context
static Pango.Context* create_context(AT0)(AT0 /*cairo.Context*/ cr) nothrow {
   return pango_cairo_create_context(UpCast!(cairo.Context*)(cr));
}


// Unintrospectable function: create_layout() / pango_cairo_create_layout()
// VERSION: 1.10
// Creates a layout object set up to match the current transformation
// and target surface of the Cairo context.  This layout can then be
// used for text measurement with functions like
// pango_layout_get_size() or drawing with functions like
// pango_cairo_show_layout(). If you change the transformation
// or target surface for @cr, you need to call pango_cairo_update_layout()
// 
// This function is the most convenient way to use Cairo with Pango,
// however it is slightly inefficient since it creates a separate
// #PangoContext object for each layout. This might matter in an
// application that was laying out large amounts of text.
// 
// g_object_unref().
// RETURNS: the newly created #PangoLayout. Free with
// <cr>: a Cairo context
static Pango.Layout* create_layout(AT0)(AT0 /*cairo.Context*/ cr) nothrow {
   return pango_cairo_create_layout(UpCast!(cairo.Context*)(cr));
}


// VERSION: 1.14
// Add a squiggly line to the current path in the specified cairo context that
// approximately covers the given rectangle in the style of an underline used
// to indicate a spelling error.  (The width of the underline is rounded to an
// integer number of up/down segments and the resulting rectangle is centered
// in the original rectangle)
// <cr>: a Cairo context
// <x>: The X coordinate of one corner of the rectangle
// <y>: The Y coordinate of one corner of the rectangle
// <width>: Non-negative width of the rectangle
// <height>: Non-negative height of the rectangle
static void error_underline_path(AT0)(AT0 /*cairo.Context*/ cr, double x, double y, double width, double height) nothrow {
   pango_cairo_error_underline_path(UpCast!(cairo.Context*)(cr), x, y, width, height);
}


// VERSION: 1.10
// Adds the glyphs in @glyphs to the current path in the specified
// cairo context. The origin of the glyphs (the left edge of the baseline)
// will be at the current point of the cairo context.
// <cr>: a Cairo context
// <font>: a #PangoFont from a #PangoCairoFontMap
// <glyphs>: a #PangoGlyphString
static void glyph_string_path(AT0, AT1, AT2)(AT0 /*cairo.Context*/ cr, AT1 /*Pango.Font*/ font, AT2 /*Pango.GlyphString*/ glyphs) nothrow {
   pango_cairo_glyph_string_path(UpCast!(cairo.Context*)(cr), UpCast!(Pango.Font*)(font), UpCast!(Pango.GlyphString*)(glyphs));
}


// VERSION: 1.10
// Adds the text in #PangoLayoutLine to the current path in the
// specified cairo context.  The origin of the glyphs (the left edge
// of the line) will be at the current point of the cairo context.
// <cr>: a Cairo context
// <line>: a #PangoLayoutLine
static void layout_line_path(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Pango.LayoutLine*/ line) nothrow {
   pango_cairo_layout_line_path(UpCast!(cairo.Context*)(cr), UpCast!(Pango.LayoutLine*)(line));
}


// VERSION: 1.10
// Adds the text in a #PangoLayout to the current path in the
// specified cairo context.  The top-left corner of the #PangoLayout
// will be at the current point of the cairo context.
// <cr>: a Cairo context
// <layout>: a Pango layout
static void layout_path(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Pango.Layout*/ layout) nothrow {
   pango_cairo_layout_path(UpCast!(cairo.Context*)(cr), UpCast!(Pango.Layout*)(layout));
}


// VERSION: 1.14
// Draw a squiggly line in the specified cairo context that approximately
// covers the given rectangle in the style of an underline used to indicate a
// spelling error.  (The width of the underline is rounded to an integer
// number of up/down segments and the resulting rectangle is centered in the
// original rectangle)
// <cr>: a Cairo context
// <x>: The X coordinate of one corner of the rectangle
// <y>: The Y coordinate of one corner of the rectangle
// <width>: Non-negative width of the rectangle
// <height>: Non-negative height of the rectangle
static void show_error_underline(AT0)(AT0 /*cairo.Context*/ cr, double x, double y, double width, double height) nothrow {
   pango_cairo_show_error_underline(UpCast!(cairo.Context*)(cr), x, y, width, height);
}


// VERSION: 1.22
// Draws the glyphs in @glyph_item in the specified cairo context,
// embedding the text associated with the glyphs in the output if the
// output format supports it (PDF for example), otherwise it acts
// similar to pango_cairo_show_glyph_string().
// 
// The origin of the glyphs (the left edge of the baseline) will
// be drawn at the current point of the cairo context.
// 
// Note that @text is the start of the text for layout, which is then
// indexed by <literal>@glyph_item->item->offset</literal>.
// <cr>: a Cairo context
// <text>: the UTF-8 text that @glyph_item refers to
// <glyph_item>: a #PangoGlyphItem
static void show_glyph_item(AT0, AT1, AT2)(AT0 /*cairo.Context*/ cr, AT1 /*char*/ text, AT2 /*Pango.GlyphItem*/ glyph_item) nothrow {
   pango_cairo_show_glyph_item(UpCast!(cairo.Context*)(cr), toCString!(char*)(text), UpCast!(Pango.GlyphItem*)(glyph_item));
}


// VERSION: 1.10
// Draws the glyphs in @glyphs in the specified cairo context.
// The origin of the glyphs (the left edge of the baseline) will
// be drawn at the current point of the cairo context.
// <cr>: a Cairo context
// <font>: a #PangoFont from a #PangoCairoFontMap
// <glyphs>: a #PangoGlyphString
static void show_glyph_string(AT0, AT1, AT2)(AT0 /*cairo.Context*/ cr, AT1 /*Pango.Font*/ font, AT2 /*Pango.GlyphString*/ glyphs) nothrow {
   pango_cairo_show_glyph_string(UpCast!(cairo.Context*)(cr), UpCast!(Pango.Font*)(font), UpCast!(Pango.GlyphString*)(glyphs));
}


// VERSION: 1.10
// Draws a #PangoLayout in the specified cairo context.
// The top-left corner of the #PangoLayout will be drawn
// at the current point of the cairo context.
// <cr>: a Cairo context
// <layout>: a Pango layout
static void show_layout(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Pango.Layout*/ layout) nothrow {
   pango_cairo_show_layout(UpCast!(cairo.Context*)(cr), UpCast!(Pango.Layout*)(layout));
}


// VERSION: 1.10
// Draws a #PangoLayoutLine in the specified cairo context.
// The origin of the glyphs (the left edge of the line) will
// be drawn at the current point of the cairo context.
// <cr>: a Cairo context
// <line>: a #PangoLayoutLine
static void show_layout_line(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Pango.LayoutLine*/ line) nothrow {
   pango_cairo_show_layout_line(UpCast!(cairo.Context*)(cr), UpCast!(Pango.LayoutLine*)(line));
}


// VERSION: 1.10
// Updates a #PangoContext previously created for use with Cairo to
// match the current transformation and target surface of a Cairo
// context. If any layouts have been created for the context,
// it's necessary to call pango_layout_context_changed() on those
// layouts.
// <cr>: a Cairo context
// <context>: a #PangoContext, from a pangocairo font map
static void update_context(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Pango.Context*/ context) nothrow {
   pango_cairo_update_context(UpCast!(cairo.Context*)(cr), UpCast!(Pango.Context*)(context));
}


// VERSION: 1.10
// Updates the private #PangoContext of a #PangoLayout created with
// pango_cairo_create_layout() to match the current transformation
// and target surface of a Cairo context.
// <cr>: a Cairo context
// <layout>: a #PangoLayout, from pango_cairo_create_layout()
static void update_layout(AT0, AT1)(AT0 /*cairo.Context*/ cr, AT1 /*Pango.Layout*/ layout) nothrow {
   pango_cairo_update_layout(UpCast!(cairo.Context*)(cr), UpCast!(Pango.Layout*)(layout));
}


// C prototypes:

extern (C) {
cairo.ScaledFont* /*new*/ pango_cairo_font_get_scaled_font(Font* this_) nothrow;
Pango.FontMap* pango_cairo_font_map_get_default() nothrow;
Pango.FontMap* pango_cairo_font_map_new() nothrow;
Pango.FontMap* pango_cairo_font_map_new_for_font_type(cairo.FontType fonttype) nothrow;
Pango.Context* pango_cairo_font_map_create_context(FontMap* this_) nothrow;
cairo.FontType /*new*/ pango_cairo_font_map_get_font_type(FontMap* this_) nothrow;
double pango_cairo_font_map_get_resolution(FontMap* this_) nothrow;
void pango_cairo_font_map_set_default(FontMap* this_) nothrow;
void pango_cairo_font_map_set_resolution(FontMap* this_, double dpi) nothrow;
cairo.FontOptions* pango_cairo_context_get_font_options(Pango.Context* context) nothrow;
double pango_cairo_context_get_resolution(Pango.Context* context) nothrow;
ShapeRendererFunc pango_cairo_context_get_shape_renderer(Pango.Context* context, void** data) nothrow;
void pango_cairo_context_set_font_options(Pango.Context* context, cairo.FontOptions* options) nothrow;
void pango_cairo_context_set_resolution(Pango.Context* context, double dpi) nothrow;
void pango_cairo_context_set_shape_renderer(Pango.Context* context, ShapeRendererFunc func, void* data, GLib2.DestroyNotify dnotify) nothrow;
Pango.Context* pango_cairo_create_context(cairo.Context* cr) nothrow;
Pango.Layout* pango_cairo_create_layout(cairo.Context* cr) nothrow;
void pango_cairo_error_underline_path(cairo.Context* cr, double x, double y, double width, double height) nothrow;
void pango_cairo_glyph_string_path(cairo.Context* cr, Pango.Font* font, Pango.GlyphString* glyphs) nothrow;
void pango_cairo_layout_line_path(cairo.Context* cr, Pango.LayoutLine* line) nothrow;
void pango_cairo_layout_path(cairo.Context* cr, Pango.Layout* layout) nothrow;
void pango_cairo_show_error_underline(cairo.Context* cr, double x, double y, double width, double height) nothrow;
void pango_cairo_show_glyph_item(cairo.Context* cr, char* text, Pango.GlyphItem* glyph_item) nothrow;
void pango_cairo_show_glyph_string(cairo.Context* cr, Pango.Font* font, Pango.GlyphString* glyphs) nothrow;
void pango_cairo_show_layout(cairo.Context* cr, Pango.Layout* layout) nothrow;
void pango_cairo_show_layout_line(cairo.Context* cr, Pango.LayoutLine* line) nothrow;
void pango_cairo_update_context(cairo.Context* cr, Pango.Context* context) nothrow;
void pango_cairo_update_layout(cairo.Context* cr, Pango.Layout* layout) nothrow;
}

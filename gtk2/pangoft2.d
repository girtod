// *** DO NOT EDIT ***
// Automatically generated from "/usr/share/gir-1.0/PangoFT2-1.0.gir"

module PangoFT2;
public import gtk2.glib2;
alias gtk2.glib2 GLib2;
public import gtk2.gobject2;
alias gtk2.gobject2 GObject2;
public import gtk2.pango;
alias gtk2.pango Pango;
public import gtk2.cairo;
alias gtk2.cairo cairo;
public import gtk2.fontconfig2;
alias gtk2.fontconfig2 fontconfig2;
public import gtk2.freetype2;
alias gtk2.freetype2 freetype2;

// package: "freetype2";

// c:symbol-prefixes: ["pango_ft2"]
// c:identifier-prefixes: ["PangoFT2"]

// module PangoFT2;

struct FontMap /* : Pango.FontMap */ {
   alias method_parent this;
   alias method_parent super_;
   alias method_parent fontmap;
   Pango.FontMap method_parent;

   static FontMap* /*new*/ new_()() nothrow {
      return pango_ft2_font_map_new();
   }
   static auto opCall()() {
      return pango_ft2_font_map_new();
   }
   // Unintrospectable function: for_display() / pango_ft2_font_map_for_display()
   static Pango.FontMap* for_display()() nothrow {
      return pango_ft2_font_map_for_display();
   }
   // Unintrospectable method: create_context() / pango_ft2_font_map_create_context()
   Pango.Context* create_context()() nothrow {
      return pango_ft2_font_map_create_context(&this);
   }
   void set_default_substitute(AT0)(SubstituteFunc func, AT0 /*void*/ data, GLib2.DestroyNotify notify) nothrow {
      pango_ft2_font_map_set_default_substitute(&this, func, UpCast!(void*)(data), notify);
   }
   void set_resolution()(double dpi_x, double dpi_y) nothrow {
      pango_ft2_font_map_set_resolution(&this, dpi_x, dpi_y);
   }
   void substitute_changed()() nothrow {
      pango_ft2_font_map_substitute_changed(&this);
   }
}

extern (C) alias void function (fontconfig2.Pattern* pattern, void* data) nothrow SubstituteFunc;


// Unintrospectable function: font_get_coverage() / pango_ft2_font_get_coverage()
// Gets the #PangoCoverage for a #PangoFT2Font. Use
// pango_font_get_coverage() instead.
// RETURNS: a #PangoCoverage.
// <font>: a #PangoFT2Font.
// <language>: a language tag.
static Pango.Coverage* font_get_coverage(AT0, AT1)(AT0 /*Pango.Font*/ font, AT1 /*Pango.Language*/ language) nothrow {
   return pango_ft2_font_get_coverage(UpCast!(Pango.Font*)(font), UpCast!(Pango.Language*)(language));
}


// Unintrospectable function: font_get_face() / pango_ft2_font_get_face()
// Returns the native FreeType2 <type>FT_Face</type> structure used for this #PangoFont.
// This may be useful if you want to use FreeType2 functions directly.
// 
// Use pango_fc_font_lock_face() instead; when you are done with a
// face from pango_fc_font_lock_face() you must call
// pango_fc_font_unlock_face().
// 
// or %NULL if @font is %NULL.
// RETURNS: a pointer to a <type>FT_Face</type> structure, with the size set correctly,
// <font>: a #PangoFont
static freetype2.Face font_get_face(AT0)(AT0 /*Pango.Font*/ font) nothrow {
   return pango_ft2_font_get_face(UpCast!(Pango.Font*)(font));
}


// Retrieves kerning information for a combination of two glyphs.
// 
// Use pango_fc_font_kern_glyphs() instead.
// 
// the given combination of glyphs.
// RETURNS: The amount of kerning (in Pango units) to apply for
// <font>: a #PangoFont
// <left>: the left #PangoGlyph
// <right>: the right #PangoGlyph
static int font_get_kerning(AT0)(AT0 /*Pango.Font*/ font, Pango.Glyph left, Pango.Glyph right) nothrow {
   return pango_ft2_font_get_kerning(UpCast!(Pango.Font*)(font), left, right);
}

// Unintrospectable function: get_context() / pango_ft2_get_context()
static Pango.Context* get_context()(double dpi_x, double dpi_y) nothrow {
   return pango_ft2_get_context(dpi_x, dpi_y);
}


// Return the index of a glyph suitable for drawing unknown characters with
// @font, or %PANGO_GLYPH_EMPTY if no suitable glyph found.
// 
// If you want to draw an unknown-box for a character that is not covered
// by the font,
// use PANGO_GET_UNKNOWN_GLYPH() instead.
// RETURNS: a glyph index into @font, or %PANGO_GLYPH_EMPTY
// <font>: a #PangoFont
static Pango.Glyph get_unknown_glyph(AT0)(AT0 /*Pango.Font*/ font) nothrow {
   return pango_ft2_get_unknown_glyph(UpCast!(Pango.Font*)(font));
}

static void render(AT0, AT1, AT2)(AT0 /*freetype2.Bitmap*/ bitmap, AT1 /*Pango.Font*/ font, AT2 /*Pango.GlyphString*/ glyphs, int x, int y) nothrow {
   pango_ft2_render(UpCast!(freetype2.Bitmap*)(bitmap), UpCast!(Pango.Font*)(font), UpCast!(Pango.GlyphString*)(glyphs), x, y);
}

static void render_layout(AT0, AT1)(AT0 /*freetype2.Bitmap*/ bitmap, AT1 /*Pango.Layout*/ layout, int x, int y) nothrow {
   pango_ft2_render_layout(UpCast!(freetype2.Bitmap*)(bitmap), UpCast!(Pango.Layout*)(layout), x, y);
}

static void render_layout_line(AT0, AT1)(AT0 /*freetype2.Bitmap*/ bitmap, AT1 /*Pango.LayoutLine*/ line, int x, int y) nothrow {
   pango_ft2_render_layout_line(UpCast!(freetype2.Bitmap*)(bitmap), UpCast!(Pango.LayoutLine*)(line), x, y);
}

static void render_layout_line_subpixel(AT0, AT1)(AT0 /*freetype2.Bitmap*/ bitmap, AT1 /*Pango.LayoutLine*/ line, int x, int y) nothrow {
   pango_ft2_render_layout_line_subpixel(UpCast!(freetype2.Bitmap*)(bitmap), UpCast!(Pango.LayoutLine*)(line), x, y);
}

static void render_layout_subpixel(AT0, AT1)(AT0 /*freetype2.Bitmap*/ bitmap, AT1 /*Pango.Layout*/ layout, int x, int y) nothrow {
   pango_ft2_render_layout_subpixel(UpCast!(freetype2.Bitmap*)(bitmap), UpCast!(Pango.Layout*)(layout), x, y);
}

static void render_transformed(AT0, AT1, AT2, AT3)(AT0 /*freetype2.Bitmap*/ bitmap, AT1 /*Pango.Matrix*/ matrix, AT2 /*Pango.Font*/ font, AT3 /*Pango.GlyphString*/ glyphs, int x, int y) nothrow {
   pango_ft2_render_transformed(UpCast!(freetype2.Bitmap*)(bitmap), UpCast!(Pango.Matrix*)(matrix), UpCast!(Pango.Font*)(font), UpCast!(Pango.GlyphString*)(glyphs), x, y);
}

static void shutdown_display()() nothrow {
   pango_ft2_shutdown_display();
}


// C prototypes:

extern (C) {
FontMap* /*new*/ pango_ft2_font_map_new() nothrow;
Pango.FontMap* pango_ft2_font_map_for_display() nothrow;
Pango.Context* pango_ft2_font_map_create_context(FontMap* this_) nothrow;
void pango_ft2_font_map_set_default_substitute(FontMap* this_, SubstituteFunc func, void* data, GLib2.DestroyNotify notify) nothrow;
void pango_ft2_font_map_set_resolution(FontMap* this_, double dpi_x, double dpi_y) nothrow;
void pango_ft2_font_map_substitute_changed(FontMap* this_) nothrow;
Pango.Coverage* pango_ft2_font_get_coverage(Pango.Font* font, Pango.Language* language) nothrow;
freetype2.Face pango_ft2_font_get_face(Pango.Font* font) nothrow;
int pango_ft2_font_get_kerning(Pango.Font* font, Pango.Glyph left, Pango.Glyph right) nothrow;
Pango.Context* pango_ft2_get_context(double dpi_x, double dpi_y) nothrow;
Pango.Glyph pango_ft2_get_unknown_glyph(Pango.Font* font) nothrow;
void pango_ft2_render(freetype2.Bitmap* bitmap, Pango.Font* font, Pango.GlyphString* glyphs, int x, int y) nothrow;
void pango_ft2_render_layout(freetype2.Bitmap* bitmap, Pango.Layout* layout, int x, int y) nothrow;
void pango_ft2_render_layout_line(freetype2.Bitmap* bitmap, Pango.LayoutLine* line, int x, int y) nothrow;
void pango_ft2_render_layout_line_subpixel(freetype2.Bitmap* bitmap, Pango.LayoutLine* line, int x, int y) nothrow;
void pango_ft2_render_layout_subpixel(freetype2.Bitmap* bitmap, Pango.Layout* layout, int x, int y) nothrow;
void pango_ft2_render_transformed(freetype2.Bitmap* bitmap, Pango.Matrix* matrix, Pango.Font* font, Pango.GlyphString* glyphs, int x, int y) nothrow;
void pango_ft2_shutdown_display() nothrow;
}
